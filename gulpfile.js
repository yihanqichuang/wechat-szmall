const fs = require('fs');
const path = require('path');

const gulp = require('gulp');
const gulpLoadPlugins = require('gulp-load-plugins');
const del = require('del');
const runSequence = require('run-sequence');
const inquirer = require('inquirer');
const generatePage = require('generate-add-page');
const sass = require('gulp-sass');
const replace = require('gulp-replace');
const plumber = require('gulp-plumber');
const gutil = require('gulp-util');

// 服务器图片地址
const imgUrl = 'http://yqqtest.shanghaisz.com/images/icon';

// load all gulp plugins
const plugins = gulpLoadPlugins();
const env = process.env.NODE_ENV || 'development';
const isProduction = () => env === 'production' || env === 'release' || env === 'test';
// const hostname = require('./config/hostname');
const app = require('./appId/appId');

// utils functions
function generateFile(options) {
  const files = generatePage({
    root: path.resolve(__dirname, './src/pages/'),
    name: options.pageName,
    less: options.styleType === 'less',
    scss: options.styleType === 'scss',
    css: options.styleType === 'css',
    json: options.needConfig
  });
  files.forEach && files.forEach(file => plugins.util.log('[generate]', file));
  return files
}

function generateJson(options) {
  const filename = path.resolve(__dirname, 'src/app.json');
  const now = fs.readFileSync(filename, 'utf8');
  const temp = now.split('\n    // Dont remove this comment');
  if (temp.length !== 2) {
    return plugins.util.log('[generate]', 'Append json failed')
  }
  const result = `${temp[0].trim()},
    "pages/${options.pageName}/${options.pageName}"
    // Dont remove this comment
  ${temp[1].trim()}
`;
  fs.writeFileSync(filename, result)
}

/**
 * Clean distribution directory
 */
gulp.task('clean', del.bind(null, ['dist/*']));

/**
 * Lint source code
 */
// gulp.task('lint', () => {
//   return gulp.src(['*.js', '**/*.js', '**/*.wxs', '!node_modules/**', '!build/**', '!config/**', '!dist/**', '!**/bluebird.js', '!gulpfile.js', '!src/wxParser/**', '!src/utils/lib/**',])
//       .pipe(plugins.eslint())
//       .pipe(plugins.eslint.format('node_modules/eslint-friendly-formatter'))
//       .pipe(plugins.eslint.failAfterError())
// });

/**
 * Compile js source to distribution directory
 */
gulp.task('compile-js', () => {
  let appId = app.appId;
  let hostName = app.hostName;
  let hostNameNew = app.hostNameNew;
  let fxHostName = app.fxHostName;
  let shareDomain = app.shareDomain;
  let hasProvinceDefault = Boolean(app.provinceDefault);
  let hasMemberCardPath = Boolean(app.memberCardPath);
  let hasParkingProject = Boolean(app.parkingProject);
  let hasParkingPay = Boolean(app.parkingPay);
  // 是否需要注册信息
  let hasRegisterMessage = !Boolean(app.registerMessage);
  // 是否需要订单中心
  let hasShowOrderCenter = Boolean(app.showOrderCenter);
  // 是否需要积分明细
  let hasShowPointDetails = Boolean(app.showPointDetails);
  let provinceDefault = hasProvinceDefault ? app.provinceDefault : ''
  let memberCardPath = hasMemberCardPath ? app.memberCardPath : '';
  let parkingProject = hasParkingProject ? app.parkingProject : '';
  let parkingPay = hasParkingPay ? app.parkingPay : '';
  let registerMessage = hasRegisterMessage ? 1 : 0;
  let showOrderCenter = hasShowOrderCenter ? 1 : 0;
  let showPointDetails = hasShowPointDetails ? 1 : 0;
  return gulp.src(['src/**/*.js', 'src/**/*.wxs', '!src/utils.uploadImg.js'])
      .pipe(plumber({errorHandler: function (err) {
        gutil.log(gutil.colors.red('[Error]'), err.toString());
      }}))
      .pipe(plugins.changed('dist', {extension: '.js'}))
      .pipe(plugins.sourcemaps.init())
      .pipe(plugins.babel({
        presets: ['es2015']
      }))
      .pipe(replace(/\${hostname}/g, hostName))
      .pipe(replace(/\${hostnameNew}/g, hostNameNew))
      .pipe(replace(/\${fxHostName}/g, fxHostName))
      .pipe(replace(/\${shareDomain}/g, shareDomain))
      .pipe(replace(/\${appId}/g, appId))
      .pipe(replace(/\${provinceDefault}/g, provinceDefault))
      .pipe(replace(/\${memberCardPath}/g, memberCardPath))
      .pipe(replace(/\${parkingProject}/g, parkingProject))
      .pipe(replace(/\${parkingPay}/g, parkingPay))
      .pipe(replace(/\${registerMessage}/g, registerMessage))
      .pipe(replace(/\${showOrderCenter}/g, showOrderCenter))
      .pipe(replace(/\${showPointDetails}/g, showPointDetails))
      .pipe(plugins.if(isProduction, plugins.terser({
        compress: {
          drop_console: true
        }
      }), plugins.sourcemaps.write('.')))
      .pipe(plugins.if(isProduction, plugins.replace(/(\.*\/*)*images\/icon/g, imgUrl)))
      .pipe(plugins.debug({title: '编译js'}))
      .pipe(gulp.dest('dist'))
});

/**
 * Compile wxs source to distribution directory
 */
gulp.task('compile-wxs', () => {
  return gulp.src(['src/**/*.wxs'])
      .pipe(gulp.dest('dist'))
});

/**
 * Compile xml source to distribution directory
 */
gulp.task('compile-xml', () => {
  return gulp.src(['src/**/*.xml'])
      .pipe(plugins.changed('dist', {extension: '.wxml'}))
      .pipe(plugins.sourcemaps.init())
      .pipe(plugins.if(isProduction, plugins.htmlmin({
        collapseWhitespace: true,
        // collapseBooleanAttributes: true,
        // removeAttributeQuotes: true,
        caseSensitive: true,
        keepClosingSlash: true, // xml
        removeComments: true,
        removeEmptyAttributes: true,
        removeScriptTypeAttributes: true,
        removeStyleLinkTypeAttributes: true
      })))
      .pipe(plugins.rename({extname: '.wxml'}))
      .pipe(plugins.if(!isProduction, plugins.sourcemaps.write('.')))
      .pipe(plugins.if(isProduction, plugins.replace(/(\.*\/*)*images\/icon/g, imgUrl)))
      .pipe(plugins.debug({title: '编译wxml'}))
      .pipe(gulp.dest('dist'))
});

/**
 * Compile scss source to distribution directory
 */
gulp.task('compile-scss', () => {
  return gulp.src(['src/**/*.scss'])
      .pipe(plumber({errorHandler: function (err) {
        gutil.log(gutil.colors.red('[Error]'), err.toString());
      }}))
      .pipe(plugins.changed('dist', {extension: '.wxss'}))
      .pipe(plugins.sourcemaps.init())
      .pipe(plugins.sass({outputStyle: 'compressed'}))
      .pipe(plugins.if(isProduction, plugins.cssnano({compatibility: '*', zindex: false})))
      .pipe(plugins.rename({extname: '.wxss'}))
      .pipe(plugins.if(!isProduction, plugins.sourcemaps.write('.')))
      .pipe(plugins.debug({title: '编译wxss'}))
      .pipe(gulp.dest('dist'))
});

/**
 * Compile json source to distribution directory
 */
gulp.task('compile-json', () => {
  return gulp.src(['src/**/*.json'])
      .pipe(plugins.jsonminify())
      .pipe(gulp.dest('dist'))
});

/**
 * Compile hostname to distribution directory
 * 替换公共请求接口hostName
 * 例如：https://wisu.wmalle.com
 */
gulp.task('compile-hostname', () => {
  let hostName = app.hostName;
  let hostNameNew = app.hostNameNew
  let fxHostName = app.fxHostName
  let shareDomain = app.shareDomain;
  let appId = app.appId;
  return gulp.src(['src/utils/fetch.js', 'src/utils/uploadImg.js'])
      .pipe(plumber({errorHandler: function (err) {
        gutil.log(gutil.colors.red('[Error]'), err.toString());
      }}))
      .pipe(replace(/\${hostname}/g, hostName))
      .pipe(replace(/\${hostnameNew}/g, hostNameNew))
      .pipe(replace(/\${fxHostName}/g, fxHostName))
      .pipe(replace(/\${shareDomain}/g, shareDomain))
      .pipe(replace(/\${appId}/g, appId))
      .pipe(plugins.babel({
        presets: ['es2015']
      }))
      .pipe(plugins.if(isProduction, plugins.terser()))
      .pipe(gulp.dest('dist/utils/'));
});

/**
 * Compile img source to distribution directory
 * 图片处理
 * watch 使用本地图片
 * build 使用服务器图片
 */
gulp.task('compile-img', () => {
  if (isProduction()) {
    return gulp.src(['src/images/tabbar/**/*.{jpg,jpeg,png,gif}'])
        .pipe(plugins.imagemin())
        .pipe(gulp.dest('dist/images/tabbar'))
  } else {
    return gulp.src(['src/**/*.{jpg,jpeg,png,gif}'])
        .pipe(plugins.imagemin())
        .pipe(gulp.dest('dist'))
  }
});

// config 文件读取
gulp.task('compile-cofig', () => {
  let appId = app.appId;
  let projectName = app.projectName;
  return gulp.src(['config/project.config.json'])
      .pipe(replace(/\${appId}/g, appId))
      .pipe(replace(/\${projectName}/g, projectName))
      // .pipe(plugins.rename('project.config.json'))
      .pipe(gulp.dest('dist'))
});

/**
 * Compile source to distribution directory
 */
gulp.task('compile', ['clean'], next => {
  runSequence([
    'compile-js',
    'compile-wxs',
    'compile-xml',
    'compile-scss',
    'compile-json',
    'compile-img',
    'compile-cofig'
  ], 'compile-hostname', next)
});

/**
 * Copy extras to distribution directory
 */
gulp.task('extras', [], () => {
  return gulp.src([
    'src/**/*.*',
    '!src/**/*.js',
    '!src/**/*.xml',
    '!src/**/*.scss',
    '!src/**/*.json',
    '!src/utils/fetch.js',
    '!src/utils/uploadImg.js',
    '!src/**/*.{jpg,jpeg,png,gif}'
  ])
      .pipe(gulp.dest('dist'))
});

/**
 * Build
 */
// gulp.task('build', ['lint'], next => runSequence(['compile', 'extras', 'compile-hostname'], next))
gulp.task('build', ['clean'], next => runSequence(['compile', 'extras'], next));

/**
 * Watch source change
 */
gulp.task('watch', ['build'], () => {
  gulp.watch('src/**/*.js', ['compile-js']);
  gulp.watch('src/**/*.wxs', ['compile-wxs']);
  gulp.watch('src/**/*.xml', ['compile-xml']);
  gulp.watch('src/**/*.scss', ['compile-scss']);
  gulp.watch('src/**/*.json', ['compile-json']);
  gulp.watch('src/**/*.{jpe?g,png,gif}', ['compile-img']);
  gulp.watch('src/**/*.*', ['extras'])
});

/**
 * Generate new page
 */
gulp.task('generate', next => {
  inquirer.prompt([
    {
      type: 'input',
      name: 'pageName',
      message: 'Input the page name',
      default: 'index'
    },
    {
      type: 'confirm',
      name: 'needConfig',
      message: 'Do you need a configuration file',
      default: false
    },
    {
      type: 'list',
      name: 'styleType',
      message: 'Select a style framework',
      // choices: ['less', 'scss', 'css'],
      choices: ['scss'],
      default: 'scss'
    }
  ])
      .then(options => {
        const res = generateFile(options)
        if (res) generateJson(options)
        next
      })
      .catch(err => {
        throw new plugins.util.PluginError('generate', err)
      })
});

/**
 * Default task
 */
gulp.task('default', ['watch']);
