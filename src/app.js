/**
 * WeChat API 模块
 * @type {Object}
 * 用于将微信官方`API`封装为`Promise`方式
 * > 小程序支持以`CommonJS`规范组织代码结构
 */
import wechat from './utils/wechat.js';
import api from './api/login.js';
import Tip from './utils/tip';
import query from './utils/query.js';
import Touches from './utils/Touches.js';
import PubSub from './utils/pubsub';
import { jdFetch } from './utils/fetch.js'
import { bindParent } from './utils/bindParent.js';

App({
  /**
   * Global shared
   * 可以定义任何成员，用于在整个应用中共享
   */
  data: {
    userInfo: null,
    hash: '',
    memberType: '',
    organizeId: '',
    organizeId_m: '',
    hostName: ''
  },

  // 获取地理位置(微信接口)
  getLocation() {
    wechat.getLocation().then((res) => {
      let lng = res.longitude + '';
      let lat = res.latitude + '';
      this.getNearestOrganize({ lng, lat, industryType: 'GeneralMerchandise', organizeId: 0 });
    }).catch(() => {
      this.getNearestOrganize({ industryType: 'GeneralMerchandise', organizeId: 0 });
    });
  },

  // 根据经纬度获取最近商场
  getNearestOrganize(param) {
    api.nearestOrganiz(param).then(res => {
      if (res.data.flag) {
        this.handleOrganizeInfo(res.data.data);
        const redirectPath = wx.getStorageSync('redirectPath') + '?loginRedirect=1' || '/pages/homeMarket/home?loginRedirect=1';
        wx.reLaunch({
          url: redirectPath
        });
      }
    })
  },

  // 处理商场信息
  handleOrganizeInfo(info) {
    if (info) {
      wx.setStorageSync('organizeId', info.organizeId);
      wx.setStorageSync('organizeName', info.organizeName);
      wx.setStorageSync('organizePhone', info.organizePhone);
      wx.setStorageSync('organizeAddress', info.organizeAddress);
      let organizeType = info.industryType || info.organizeType;
      wx.setStorageSync('organizeType', organizeType);
      this.globalData.organizePhone = info.organizePhone;
    }
  },

  /**
   * 生命周期函数--监听小程序初始化
   * 当小程序初始化完成时，会触发 onLaunch（全局只触发一次）
   */
  onLaunch(option) {
    if (!this.globalData.defaultConfig) {
      if (wx.getStorageSync('defaultConfig')) {
        this.globalData.defaultConfig = JSON.parse(wx.getStorageSync('defaultConfig'))
      }
    }
    // 获取屏幕参数
    try {
      const res = wx.getSystemInfoSync()
      if (res.platform == 'ios') {
        this.globalData.platform = 'ios'
      } else if (res.platform == 'android') {
        this.globalData.platform = 'android'
      }
      // 导航高度
      let navHeight = res.statusBarHeight
      // 屏幕宽度/高度，单位px
      this.globalData.screenWidth = res.screenWidth
      this.globalData.screenHeight = res.screenHeight
      // 状态栏的高度，单位px
      this.globalData.statusBarHeight = res.statusBarHeight
      // 设备像素比
      this.globalData.pixelRatio = res.pixelRatio
      // 可使用窗口宽度，单位px
      this.globalData.winWidth = res.windowWidth
      // 安卓时，胶囊距离状态栏8px，iOS距离4px
      if (res.system.indexOf('Android') !== -1) {
        this.globalData.navHeight = 48
        this.globalData.navTitleTop = navHeight + 8
        // 视窗高度 顶部有占位栏时
        this.globalData.winHeight = res.screenHeight - 48
        // tab主页视窗高度
        this.globalData.winHeightTab = res.windowHeight - 48
      } else {
        this.globalData.navHeight = 44
        this.globalData.navTitleTop = navHeight + 4
        // 视窗高度 顶部有占位栏时
        this.globalData.winHeight = res.screenHeight - 44
        // tab主页视窗高度
        this.globalData.winHeightTab = res.windowHeight - 44
      }
      // 胶囊按钮位置信息
      const menuButtonInfo = wx.getMenuButtonBoundingClientRect();
      // 导航栏高度 = 状态栏到胶囊的间距（胶囊距上距离-状态栏高度） * 2 + 胶囊高度 + 状态栏高度
      this.globalData.navBarHeight = (menuButtonInfo.top - res.statusBarHeight) * 2 + menuButtonInfo.height + res.statusBarHeight;
      this.globalData.menuRight = res.screenWidth - menuButtonInfo.right;
      this.globalData.menuBotton = menuButtonInfo.top - res.statusBarHeight;
      this.globalData.menuHeight = menuButtonInfo.height;
    } catch (e) {
      console.error(e);
    }
  },

  async login() {
    const loginRes = await wechat.login();
    if (!loginRes.code) {
      Tip.toast('获取code失败', null, 'none');
      return;
    }
    let url = 'szmall/oauth/token/' + loginRes.code;
    let param = {
      appId: '${appId}',
      appType: 'smallApp'
    };
    wx.setStorageSync('code', loginRes.code);
    wx.setStorageSync('appId', param.appId);
    // 调用登陆
    const tokenRes = await api.login(url, param);
    if ("00000" === tokenRes.data.code) {
      wx.setStorageSync('token', tokenRes.data.data.access_token);
      // 获取用户信息
      const userRes = await api.getUserInfo();
      wx.setStorageSync('userInfo', userRes.data);
      wx.setStorageSync('memberType', userRes.data.memberType);
      // 获取当前最近的组织(赞不获取，去首页获取)
      // this.getLocation();
    } else {
      Tip.toast(tokenRes.data.msg, null, 'none');
    }
  },

  onShow(options) {
    if ('pages/newPackage/orderRefund/orderRefund' === options.path) {
      // 因为退款页面，会调用到wx.chooseImage,会触发此方法，所以当触发时，直接return，防止页面刷新
      return;
    }
    // 小程序启动，本地无登录凭证hash，进入过度页面进行登录
    // 保存目标跳转地址，登录完成后，重定向到目标地址
    let redirectPath = `/${options.path}`;
    if (JSON.stringify(options.query) !== '{}') {
      const arr = [];
      Object.keys(options.query).forEach(key => {
        arr.push(`${key}=${options.query[key]}`);
      });
      if (arr.length) {
        redirectPath = `${redirectPath}?${arr.join('&')}`;
      }
    }

    // 将跳转的页面路径存入Storage，等登陆成功后跳转
    wx.setStorageSync('redirectPath', redirectPath);

    // 登陆操作
    this.login();

    // 如果支持新版本更新API
    if (wx.canIUse('getUpdateManager')) {
      // 小程序热启动时 新版本更新
      const updateManager = wx.getUpdateManager();
      updateManager.onCheckForUpdate(function (res) {
        // 请求完新版本信息的回调
        console.log(res)
      });

      // 强制小程序重启并使用新版本
      updateManager.onUpdateReady(function () {
        wx.showModal({
          title: '更新提示',
          content: '新版本已经准备好，是否重启应用？',
          success(res) {
            if (res.confirm) {
              // 新的版本已经下载好，调用 applyUpdate 应用新版本并重启
              updateManager.applyUpdate()
            }
          }
        })
      });

      // 新版本下载失败
      updateManager.onUpdateFailed(function () {
        // 新版本下载失败
      });
    }
  },
  // 判断组织机构ID是否和当前本地组织机构一样，否则根据新的组织机构
  // ID获取组织机构信息，并更新本地信息
  async updateLocalOrganize(organizeId, organizeName = wx.getStorageSync('organizeName')) {
    console.log('对比的组织结构：', organizeId);
    const _localOrganizeId = wx.getStorageSync('organizeId');
    if (!organizeId || organizeId === _localOrganizeId) return
    // 先存入机构基本信息，保证后续请求机构ID正确
    // 后续补充所有机构信息
    wx.setStorageSync('organizeId', organizeId);
    wx.setStorageSync('organizeName', organizeName);
    const orgRes = await api.fetchOrganizeInfo({ organizeId });
    console.log('组织机构信息：', orgRes);
    if (orgRes.data.flag) {
      this.handleOrganizeInfo(orgRes.data.data);
    }
  },
  // 改变initDiscover的值
  changeInitDiscover(data) {
    this.globalData.initDiscover = data
  },

  globalData: {
    organizeList: null, // 组织机构列表
    tabBar: null, // 下方tabBar数据
    isFirst: false,
    memberPhone: null,    // 会员手机号
    userInfo: null,
    qrOrderData: null,    // 付款码被扫后获取到的订单信息
    cardNumber: null,     // 会员卡号,
    couponNumber: 0,      // 可使用票券的数量
    initDiscover: 0,      // 重新获取潮品汇列表
    cartOrderData: null,  // 购物车结算数据
    defaultConfig: null,  // 小程序默认数据
    platform: 'ios',
    pixelRatio: 2,
    statusBarHeight: 20,
    navHeight: 64,
    navTitleTop: 26,
    winHeight: 655,
    winWidth: 750,
    screenWidth: 375,
    screenHeight: 812,
    organizePhone: '',
    pop: 2,
    num: 0,
    navBarHeight: 0, // 导航栏高度
    menuRight: 0, // 胶囊距右方间距（方保持左、右间距一致）
    menuBotton: 0, // 胶囊距底部间距（保持底部间距一致）
    menuHeight: 0, // 胶囊高度（自定义内容可与胶囊高度保证一致）
  },
  Touches: new Touches(),
  pubSub: new PubSub()
});
