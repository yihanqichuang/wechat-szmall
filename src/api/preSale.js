import { newFetch } from '../utils/fetch.js';

// 查询预售详情
const fetchQueryPreSale = (params) => newFetch('wmallerWechat/queryPreSale', params, 'POST')

// 查询提货门店列表
const fetchQueryOrganizeInfo = (params) => newFetch('wmallerWechat/queryOrganizeInfo', params, 'POST')

module.exports = {
  fetchQueryPreSale,
  fetchQueryOrganizeInfo
}
