/**
 * Created by yu tao on 2018/6/1.
 * 新悦城停车接口
 */
import { oldFetch } from '../utils/fetch.js';

// 查询车牌信息列表
const listMemberCarNum = (params) => oldFetch('wechat/memberCenter/listMemberCarNum.json', params, 'POST');
// 保存车牌
const saveMemberCarNum = (params) => oldFetch('wechat/memberCenter/saveMemberCarNum.json', params, 'POST');
// 移除车牌
const removeMemberCarNum = (params) => oldFetch('wechat/memberCenter/removeMemberCarNum.json', params, 'POST');
// 车牌列表和空车位
const listCarNum = (params) => oldFetch('wechat/parkingXYC/topartingfree.json', params, 'POST');
// 车牌账单
const parkingInfo = (params) => oldFetch('wechat/parkingXYC/getParkingBillInfo.json', params, 'POST');
// 查询会员优惠信息
const parkingDesc = (params) => oldFetch('wechat/parkingXYC/getParkingDesc.json', params, 'POST');
// 确认缴费
const getPayInfo = (params) => oldFetch('wechat/parkingXYC/wxpay/getPayInfoXYC.json', params, 'POST');
// 缴费记录
const listMemberParkingRecord = (params) => oldFetch('wechat/memberCenter/listMemberParkingRecord.json', params, 'POST');
// 缴费记录明细
const parkingRecordDetail = (params) => oldFetch('wechat/memberCenter/MemberParkingRecordInfo.json', params, 'POST');


module.exports = {
  listMemberCarNum,
  saveMemberCarNum,
  removeMemberCarNum,
  listCarNum,
  parkingInfo,
  parkingDesc,
  getPayInfo,
  listMemberParkingRecord,
  parkingRecordDetail
};
