import { oldFetch, newFetch } from '../utils/fetch.js';

// 获取 店铺列表
const fetchShopList = (params) => oldFetch('szmall/mall/wechat/shop/getShopList.json', params, 'POST')

// 获取店铺详情
const fetchShopData = (params) => oldFetch('szmall/mall/wechat/shop/shopDetail.json', params, 'POST')

// 收藏店铺或者商品
const fetchDoCollect = (params) => oldFetch('wechat/collectMember/add.json', params, 'POST')

// 删除收藏店铺或者商品
const fetchCollectRemove = (params) => oldFetch('wechat/collectMember/remove.json', params, 'POST')

// 收藏店铺或者商品列表
const fetchCollectList = (params) => oldFetch('wechat/collectMember/list.json', params, 'POST')

// 获取店铺或者商品收藏状态
const fetchCollectStatus = (params) => oldFetch('wechat/collectMember/isCollect.json', params, 'POST')

// 获取店铺优惠券列表
const fetchShopCoupons = (params) => oldFetch('szmall/coupon/wechat/shopUseCouponList', params, 'POST')

module.exports = {
  fetchShopList,
  fetchShopData,
  fetchDoCollect,
  fetchCollectRemove,
  fetchCollectList,
  fetchCollectStatus,
  fetchShopCoupons
}
