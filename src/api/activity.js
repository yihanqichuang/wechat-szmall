import { oldFetch } from '../utils/fetch.js';

// 获取活动列表
const activityList = (params) => oldFetch('szmall/market/wechat/signup_activity/list.json', params, 'POST');

// 获取活动详情
const activityDetails = (params) => oldFetch('szmall/market/wechat/signup_activity/detail.json', params, 'POST');

// 活动报名详情
const activitySignUp = (params) => oldFetch('szmall/market/wechat/signup_activity/signup_form.json', params, 'POST');

// 活动报名
const signUpSubmit = (params) => oldFetch('szmall/market/wechat/signup_activity/signup_submit.json', params, 'POST');

// 微信支付签名
const getWxConfig = (params) => oldFetch('wechat/wechat/signup_activity/getPayData.json', params, 'POST');

// 生成活动订单
const createActivityOrder = (params) => oldFetch('wechat/wechat/signup_activity/getPayData.json', params, 'POST');

// 微信支付签名2
const getWxConfig2 = (params) => oldFetch('wechat/order/getPayData.json', params, 'POST');

// 活动详情
const activityCode = (params) => oldFetch('szmall/market/wechat/signup_activity/verify_codes.json', params, 'GET');

// 报名记录
const signUpRecord = (params) => oldFetch('szmall/market/wechat/signup_activity/signup_record.json', params, 'POST');

// 长益预付卡预付款
const preparePay = (params) => oldFetch('wechat/wechat/signup_activity/preparePay.json', params, 'POST');

// 支付完成回调函数(活动)
const getPayDataSuccess = (params) => oldFetch('wechat/wechat/signup_activity/getPayDataSuccess.json', params, 'POST');

// 会员活动
const listSignupActivity = (params) => oldFetch('szmall/market/wechat/signup_activity/listSignupActivity.json', params, 'POST')

module.exports = {
  activityList,
  activityDetails,
  activitySignUp,
  signUpSubmit,
  getWxConfig,
  getWxConfig2,
  createActivityOrder,
  activityCode,
  getPayDataSuccess,
  signUpRecord,
  preparePay,
  listSignupActivity
}
