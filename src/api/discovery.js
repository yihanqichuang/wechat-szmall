import { oldFetch } from '../utils/fetch.js';

// 发现banner列表
const advertList = (params) => oldFetch('wechat/discovery/advert/list.json', params, 'POST');
// 发现店铺列表
const list = (params) => oldFetch('wechat/discovery/activity/list.json', params, 'POST');
// 获取店铺详情
const shopDetail = (params) => oldFetch('wechat/discovery/activity/detail.json', params, 'POST');
// 点赞
const praise = (params) => oldFetch('wechat/discovery/activity/praise.json', params, 'POST');
// 保存品论
const saveEvaluation = (params) => oldFetch('wechat/discovery/activity/saveEvaluation.json', params, 'POST');
// 评价列表
const evaluationList = (params) => oldFetch('wechat/discovery/activity/evaluationList.json', params, 'POST');
// 评价列表详情
const evaluationDetailList = (params) => oldFetch('wechat/discovery/activity/evaluationDetailList.json', params, 'POST');
// 删除品论
const deleteEvaluation = (params) => oldFetch('wechat/discovery/activity/deleteEvaluation.json', params, 'POST');
// 回复评论
const saveEvaluationReply = (params) => oldFetch('wechat/discovery/activity/saveEvaluationReply.json', params, 'POST');

module.exports = {
  advertList,
  shopDetail,
  saveEvaluation,
  evaluationList,
  evaluationDetailList,
  deleteEvaluation,
  saveEvaluationReply,
  praise,
  list
};
