import { oldFetch } from '../utils/fetch.js';

// 获取预约订单列表
const getAppointmentList = (params) => oldFetch('szmall/order/orderAppointment/list', params, 'POST');

// 获取待预约订单列表
const getServiceTemplate = (params) => oldFetch('szmall/product/wechat/product/getServiceTemplate', params, 'POST');

// 预约订单
const reserveAppointmentOrder = (params) => oldFetch('szmall/order/orderAppointment/reserve', params, 'POST');

// 取消预约单
const cancelAppointmentOrder = (params) => oldFetch('szmall/order/orderAppointment/cancel', params, 'POST');

// 获取预约单详情
const getAppointmentOrder = (params) => oldFetch('szmall/order/orderAppointment/serviceDetail', params, 'POST');

// 会员确认完成
const finishAppointmentOrder = (params) => oldFetch('szmall/order/orderAppointment/finish', params, 'POST');

module.exports = {
  getAppointmentList,
  getServiceTemplate,
  reserveAppointmentOrder,
  cancelAppointmentOrder,
  getAppointmentOrder,
  finishAppointmentOrder
}
