import { oldFetch } from '../utils/fetch.js';

// 获取信息
const getData = (params) => oldFetch('wechat/memberInfo/getMemberInfo.json', params, 'POST');

module.exports = {
  getData
};
