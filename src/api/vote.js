/**
 * Created by yu tao on 2018/8/29.
 * 活动投票相关API接口
 */
import { oldFetch } from '../utils/fetch.js';

// 获取 投票列表
const voteActivityList = (params) => oldFetch('wechat/vote/voteActivityList.json', params, 'POST');

// 获取 投票详情
const voteActivityDetail = (params) => oldFetch('wechat/vote/voteActivityFirstPage.json', params, 'POST');

// 报名人员列表
const getMoreVoteUser = (params) => oldFetch('wechat/vote/getMoreVoteUser.json', params, 'POST');

// 报名人员详情（别人的详情、自己的详情），说明：用户不能给自己投票（member.openId=voteUser.openId时是自己）
const voteUserDetail = (params) => oldFetch('wechat/vote/voteUserDetail.json', params, 'POST');

// 我要报名页面数据
const toVoteEnroll = (params) => oldFetch('wechat/vote/toVoteEnroll.json', params, 'POST');

// 保存报名人员信息
const saveVoteEnroll = (params) => oldFetch('wechat/vote/saveVoteEnroll.json', params, 'POST');

// 投票（说明：自己不能给自己投票）
const voteForUser = (params) => oldFetch('wechat/vote/voteForUser.json', params, 'POST');

// 我的报名列表
const voteEnrollList = (params) => oldFetch('wechat/vote/voteEnrollList.json', params, 'POST');

module.exports = {
  voteActivityList,
  voteActivityDetail,
  getMoreVoteUser,
  voteUserDetail,
  toVoteEnroll,
  saveVoteEnroll,
  voteForUser,
  voteEnrollList
};
