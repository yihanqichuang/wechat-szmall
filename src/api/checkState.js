/**
 * Created by yu tao on 2018/11/26.
 */
import { oldFetch } from '../utils/fetch.js';
import Tips from '../utils/tip.js';
export default class CheckStatus {
  constructor() {
    this.timer = null;
  }

  // 请求检查状态接口
  static verify(params) {
    return new Promise((resolve, reject) => {
      oldFetch('wechat/verify/checkState.json', params, 'POST').then(res => {
        console.log(res)
        if (res.data.flag) {
          resolve(res);
        } else {
          reject(res)
        }
      }).catch(err => {
        console.log(err)
        reject(err)
      })
    })
  }

  // 定时检查转态
  static verifyInterVal(params, duration = 1000) {
    return new Promise((resolve) => {
      this.timer = setInterval(() => {
        CheckStatus.verify(params).then(res => {
          if (res.data.flag && res.data.data) {
            Tips.confirm(res.data.msg, { data: res.data.data }, '温馨提示', '', '知道了').then(r => {
              console.log(r)
              resolve(r);
            })
            this.clearVerifyInterValInterval();
          }
        })
      }, duration)
    })
  }

  // 清除定时器
  static clearVerifyInterValInterval() {
    console.log(this.timer)
    if (this.timer != null) clearInterval(this.timer)
  }
}

CheckStatus.timer = null;

