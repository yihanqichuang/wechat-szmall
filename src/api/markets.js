import { oldFetch } from '../utils/fetch.js';

// 获取商场列表
const fetchOrganizeList = (params) => oldFetch('szmall/mall/cityConfig/cityList.json', params, 'POST')

module.exports = {
  fetchOrganizeList
}
