import { newFetch } from '../utils/fetch.js'

// 拼团成员列表
const memberGroupActivityList = params => newFetch('wmallerWechat/memberGroupActivityList', params, 'POST')

// 拼团活动列表
const groupActivityPage = params => newFetch('wmallerWechat/groupActivityPage', params, 'POST')

// 获取拼团商品详情信息
const getGroupByProductCode = params => newFetch('wmallerWechat/getGroupByProductCode', params, 'POST')

// 拼团活动-判断当前登陆用户是否是新用户
const isGroupMarketNewMember = params => newFetch('wmallerWechat/isGroupMarketNewMember', params, 'POST')

// 根据订单号获取团编码
const getGroupInfoCodeByOrderNo = params => newFetch('wmallerWechat/getGroupInfoCodeByOrderNo', params, 'POST')

// 根据订单号获取团信息
const getGroupInfoByOrderNo = params => newFetch('wmallerWechat/getGroupInfoByOrderNo', params, 'POST')

// 我的拼团列表
const myGroupInfoList = params => newFetch('wmallerWechat/myGroupInfoList', params, 'POST')

// 删除拼团成员信息
const delGroupMemberById = params => newFetch('wmallerWechat/delGroupMemberById', params, 'POST')

module.exports = {
  memberGroupActivityList,
  groupActivityPage,
  getGroupByProductCode,
  isGroupMarketNewMember,
  getGroupInfoCodeByOrderNo,
  getGroupInfoByOrderNo,
  myGroupInfoList,
  delGroupMemberById
}
