/**
 * 地址管理接口
 */
import { oldFetch,fxFetch } from '../utils/fetch.js';

// 地址信息列表
const fetchAddressList = (params) => oldFetch('szmall/vip/memberHarvestAddress/list.json', params, 'POST')

// 新增地址信息
const fetchSaveAddress = (params) => oldFetch('szmall/vip/memberHarvestAddress/save.json', params, 'POST')

// 编辑地址信息
const fetchUpdateAddress = (params) => oldFetch('szmall/vip/memberHarvestAddress/update.json', params, 'POST')

// 删除地址信息
const fetchDeleteAddress = (params) => oldFetch('szmall/vip/memberHarvestAddress/delete.json', params, 'POST')

// 获取省市区列表
const getAreaList = (params) => fxFetch('my/regionList', params, 'GET')

module.exports = {
  fetchAddressList,
  fetchSaveAddress,
  fetchUpdateAddress,
  fetchDeleteAddress,
  getAreaList
}
