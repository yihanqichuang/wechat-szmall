/**
 * Created by AklanFan on 2020/3/15.
 */
import { oldFetch } from '../utils/fetch.js';

// 获取直播列表
const fetchLiveList = (params) => oldFetch('wechat/miniLive/liveList.json', params, 'POST')
// 根据上级分类获取子级分类列表
const fetchUpdateLiveStatus = (params) => oldFetch('wechat/miniLive/batchUpdateLiveSatus.json', params, 'POST')
// 保存推广记录
const fetchSavePromotionRecord = (params) => oldFetch('wechat/promotion/saveRecord.json', params, 'POST')

const getCustomerConfig = (params) => oldFetch('wechat/miniLive/getCustomerConfig.json', params, 'POST')

module.exports = {
  fetchLiveList,
  fetchUpdateLiveStatus,
  fetchSavePromotionRecord,
  getCustomerConfig
}
