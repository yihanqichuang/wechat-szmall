import { oldFetch } from '../utils/fetch.js';

// 循环获取被扫后的 订单信息
const fetchQueryOrder = (params) => oldFetch('wechat/scanCodePay/queryOrder.json', params, 'POST')

// 提交订单
const fetchConfirmOrder = (params) => oldFetch('wechat/scanCodePay/confirmOrder.json', params, 'POST')

module.exports = {
  fetchQueryOrder,
  fetchConfirmOrder
}
