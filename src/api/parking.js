/**
 * Created by yu tao on 2018/6/1.
 */
import { oldFetch } from '../utils/fetch.js';
import { PARKING_PROJECT } from '../vendor.js';
// 查询车牌信息列表
const listMemberCarNum = (params) => oldFetch('wechat/memberCenter/listMemberCarNum.json', params, 'POST');
// 保存车牌
const saveMemberCarNum = (params) => oldFetch('wechat/memberCenter/saveMemberCarNum.json', params, 'POST');
// 移除车牌
const removeMemberCarNum = (params) => oldFetch('wechat/memberCenter/removeMemberCarNum.json', params, 'POST');
// 缴费记录
const listMemberParkingRecord = (params) => oldFetch('wechat/memberCenter/listMemberParkingRecord.json', params, 'POST');
// 缴费记录明细
const parkingRecordDetail = (params) => oldFetch('wechat/memberCenter/MemberParkingRecordInfo.json', params, 'POST');
// 车牌列表和空车位
const listCarNum = (params) => oldFetch(`wechat/parking/${PARKING_PROJECT}/topartingfree.json`, params, 'POST');
// 车牌账单
const parkingInfo = (params) => oldFetch(`wechat/parking/${PARKING_PROJECT}/getParkingBillInfo.json`, params, 'POST');
// 查询会员优惠信息
const parkingDesc = (params) => oldFetch(`wechat/parking/${PARKING_PROJECT}/getParkingDesc.json`, params, 'POST');
// 确认缴费
const getPayInfo = (params) => oldFetch(`wechat/parking/wxpay/${PARKING_PROJECT}/getPayInfo.json`, params, 'POST');
// 获取缴费信息
const getPayAmount = (params) => oldFetch(`wechat/parking/wxpay/${PARKING_PROJECT}/getPayAmount.json`, params, 'POST');

module.exports = {
  listMemberCarNum,
  saveMemberCarNum,
  removeMemberCarNum,
  listCarNum,
  parkingInfo,
  parkingDesc,
  getPayInfo,
  listMemberParkingRecord,
  parkingRecordDetail,
  getPayAmount
};
