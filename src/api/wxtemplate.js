import { oldFetch } from '../utils/fetch.js';

// 推送模板消息
const sendTemplet = (params) => oldFetch('wechat/member/sendTemplet.json', Object.assign(params, { appId: wx.getStorageSync('appId') }), 'POST');

// 提交表单获取formId
const formSubmit = (e) => {
  console.log('form发生了submit事件，携带数据为：', e)
  return new Promise((resolve, reject) => {
    if (e.detail.formId) return resolve(e.detail.formId)
    reject()
  })
}

// 优惠券
const couponTemplate = function(formId, data) {
  if (data.accessType === 'FREE') return
  let page = data.couponTypeKind === 'VOUCHER'  // 满减券
    ? `pages/exchangeCode/exchangeCode?gainId=${data.gainId}&couponType=${data.couponType}`
    : 'pages/newPackage/myCouponNew/myCoupon'
  sendTemplet({
    formId,
    sendType: 'COUPON',
    eventId: data.couponId || data.objectId,
    page
  })
}

// 活动
const activityTemplate = function(formId, eventId) {
  sendTemplet({
    formId,
    eventId,
    sendType: 'ACTIVITY',
    page: `pages/activityCode/activityCode?activityId=${eventId}&type=1`
  })
}

// 积分兑礼
const giftTemplate = function(formId, eventId) {
  sendTemplet({
    formId,
    eventId,
    sendType: 'COUPON',
    page: `pages/exchange/exchange?gainId=${eventId}`
  })
}

// 被扫
const bsTemplate = function(formId, eventId, payType) {
  sendTemplet({
    formId,
    eventId,
    sendType: 'PAY',
    orderType: '会员码被扫',
    payType,
    page: 'pages/memberCard/shoppingSheet/shoppingSheet'
  })
}

// 被扫
const zsTemplate = function(formId, eventId, payType) {
  sendTemplet({
    formId,
    eventId,
    sendType: 'PAY',
    orderType: '自助购买',
    payType,
    page: 'pages/memberCard/shoppingSheet/shoppingSheet'
  })
}

// 判断支付方式
const payType = function(balance, total) {
  return balance == total ? '余额支付' : balance > 0 ? '微信+余额支付' : '微信支付'
}

module.exports = {
  sendTemplet,
  formSubmit,
  couponTemplate,
  activityTemplate,
  giftTemplate,
  bsTemplate,
  zsTemplate,
  payType
}
