/**
 * Created by AklanFun on 2018/9/13.
 */
import { oldFetch } from '../utils/fetch.js';

// 预约配置信息
const reserveQueryOne = (params) => oldFetch('wechat/parkingreserve/queryOne.json', params, 'POST')

// 马上预约
const reserveSpace = (params) => oldFetch('wechat/parkingreserve/reserveSpace.json', params, 'POST')

// 我的预约（查询当前会员的预约信息）提前进场字段如果是Y的话表示不能再提前进场。
const reserveList = (params) => oldFetch('wechat/parkingreserve/list.json', params, 'POST')

// 取消预约（id参数从我的预约接口中获取）
const reserveCancel = (params) => oldFetch('wechat/parkingreserve/cancel.json', params, 'POST')

// 提前进场
const reserveInadvance = (params) => oldFetch('wechat/parkingreserve/parkinginadvance.json', params, 'POST')

module.exports = {
  reserveQueryOne,
  reserveSpace,
  reserveList,
  reserveCancel,
  reserveInadvance
}
