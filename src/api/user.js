import { oldFetch, newFetch } from '../utils/fetch.js';
import handlePassword from '../utils/handlePassword'


// 领取会员卡-获取手机验证码
const getSecurityCode = (params) => oldFetch('szmall/vip/register/getSecurityCode.json', params, 'POST');

// 领取会员卡-保存注册
const register = (params) => oldFetch('szmall/vip/register/register.json', params, 'POST');

// 获取线下CRM会员信息
const getCrmMember = (params) => oldFetch('wechat/register/getCrmMember.json', params, 'POST');

// 重新绑定会员手机号码
const againBindMemberPhone = (params) => oldFetch('wechat/register/againBindMemberPhone.json', params, 'POST');

// 会员-资金明细
const accountChangeRecord = (params) => oldFetch('wechat/member/accountChangeRecord.json', params, 'POST');

// 会员信息-会员的余额，积分，票券数量等，会员卡号等
const queryMember = (params) => oldFetch('szmall/vip/member/queryMember.json', params, 'POST');

//  杉杉新 会员信息-会员的余额，积分，票券数量等，会员卡号等
const queryMemberCardInfo = (params) => oldFetch('wechat/memberCenter/memberCardInfo.json', params, 'POST');
// 会员-积分明细
const pointsRecord = (params) => oldFetch('szmall/vip/member/pointsRecord.json', params, 'POST');

// 会员中心-保存储值卡
const savePrePaidCard1 = (params) => oldFetch('wechat/member/savePrePaidCard.json', params, 'POST');

// 会员中心-保存储值卡(新)
const savePrePaidCard = (params) => oldFetch('wechat/member/savePrePaidCards.json', params, 'POST');

// 是否绑定支付密码
const isBindPassword = (params) => oldFetch('wechat/member/isBindPassword.json', params, 'POST');

// 绑定支付密码
const bindPassword1 = (params) => oldFetch('wechat/member/bindPassword.json', params, 'POST');

// 绑定支付密码(新，加密)
const bindPassword2 = (params) => oldFetch('wechat/member/bindPasswords.json', params, 'POST');

// 重置支付密码
const resetPassword1 = (params) => oldFetch('wechat/member/resetPassword.json', params, 'POST');

// 重置支付密码(新，加密)
const resetPassword2 = (params) => oldFetch('wechat/member/resetPasswords.json', params, 'POST');

// 身份验证
const validatePhone = (params) => oldFetch('wechat/member/validatePhone.json', params, 'POST');

// 验证支付密码
const validatePassword1 = (params) => oldFetch('wechat/member/validatePassword.json', params, 'POST');

// 验证支付密码(新)
const validatePassword2 = (params) => oldFetch('wechat/member/validatePasswords.json', params, 'POST');

// 预付卡列表
const prePaidCardList = (params) => oldFetch('wechat/member/prePaidCardList.json', params, 'POST');

// 删除预付卡
const deleteCard = (params) => oldFetch('wechat/member/deleteCard.json', params, 'POST');

// 会员卡获取说明
const getCardDesc = (params) => oldFetch('wechat/memberCard/getCardDesc.json', params, 'POST');

// 获取手机号
const getPhone = (params) => oldFetch('szmall/vip/getPhone.json', params, 'POST');

// 获取用户余额
const queryAmount = (params) => oldFetch('wechat/member/queryAmount.json', params, 'POST');

// 获取会员卡等级列表
const listCardLevel = (params) => oldFetch('szmall/vip/memberCard/listCardLevel.json', params, 'POST');

// 获取会员等级
const queryCardLevel = (params) => oldFetch('wechat/member/queryCardLevel.json', params, 'POST');

// 积分说明(用的3.0的接口)
const getMemberCardDesc = (params) => oldFetch('wechat/memberCard/getMemberCardDesc.json', params, 'POST');

// 消费记录
const saleIntems = (params) => oldFetch('wechat/member/saleIntems.json', params, 'POST');

// 是否可以使用签到功能
const isCanUseSign = (params) => oldFetch('wechat/homepage/listPersonalWechat.json', params, 'POST');

// 点击签到按钮
const saveMemberSign = (params) => oldFetch('wechat/memberSign/saveMemberSign.json', params, 'POST');

// 签到页面接口
const signConfig = (params) => oldFetch('wechat/memberSign/signConfig.json', params, 'POST');

// 领卡到微信卡包
const fetchGetAddCardParam = (params) => oldFetch('wechat/wechatCommon/getAddCardParam.json', params, 'POST');

// 验证支付密码（加密）
const validatePassword = (params) => {
  let _params = {
    payPassword: handlePassword.encrypt(params.payPassword)
  };
  return new Promise((resolve, reject) => {
    validatePassword2(_params).then((res) => {
      console.log(params);
      console.log(res);
      resolve(res);
    }).catch((err) => {
      reject(err);
      console.log(err);
    })
  })
};

// 重置支付密码（加密）
const resetPassword = (params) => {
  let _params = {
    payPassword: handlePassword.encrypt(params.payPassword),
    confirmPayPassword: handlePassword.encrypt(params.confirmPayPassword)
  };
  return new Promise((resolve, reject) => {
    resetPassword2(_params).then((res) => {
      console.log(params);
      console.log(res);
      resolve(res);
    }).catch((err) => {
      reject(err);
      console.log(err);
    })
  })
};

// 设置支付密码（加密）
const bindPassword = (params) => {
  let _params = {
    payPassword: handlePassword.encrypt(params.payPassword),
    confirmPayPassword: handlePassword.encrypt(params.confirmPayPassword)
  };
  return new Promise((resolve, reject) => {
    bindPassword2(_params).then((res) => {
      console.log(params);
      console.log(res);
      resolve(res);
    }).catch((err) => {
      reject(err);
      console.log(err);
    })
  })
};
//  memberCenter/memberCardInfo.json
// 获取会员个人资料配置信息
const getListMemberDataConfig = (params) => oldFetch('wechat/memberCenter/listMemberDataConfig.json', params, 'POST');
// 获取会员资料
const getMemberInformation = (params) => oldFetch('wechat/memberCenter/getMember.json', params, 'POST');
// 修改会员资料
const editMemberInformation = (params) => oldFetch('wechat/memberCenter/editMember.json', params, 'POST');
// 超市积分配置
const queryConfig = (params) => oldFetch('wechat/pointsExchange/queryConfig.json', params, 'POST');
// 超市积分马上兑换
const getQrcode = (params) => oldFetch('wechat/pointsExchange/getQrcode.json', params, 'POST');

// 获取卡券数量
const getCouponCount = (params) => oldFetch('szmall/coupon/wechat/getMemberCouponCount', params, 'POST');

// 获取收藏数量
const getCollectCount = (params) => oldFetch('szmall/vip/collectMember/getCollectNum.json', params, 'POST');

// 获取订单相应状态的数量
const getCountOrder = (params) => oldFetch('szmall/order/countOrder', params, 'POST');

// 分享海报个人信息
const getShareBaseData = (params) => oldFetch('szmall/product/wechatCommon/getBillMaQrcode.json', params, 'POST');

// 获取纪念日营销图标是否显示
const showLevelPowerByGradeId = (params) => newFetch('wmallerWechat/showLevelPowerByGradeId', params, 'POST');

// 获取某会员等级的会员权益
const getLevelPowerByGradeId = (params) => newFetch('wmallerWechat/getLevelPowerByGradeId', params, 'POST');


module.exports = {
  getShareBaseData,
  getCouponCount,
  getCollectCount,
  getCountOrder,
  getSecurityCode,
  register,
  accountChangeRecord,
  queryMember,
  againBindMemberPhone,
  pointsRecord,
  savePrePaidCard,
  savePrePaidCard1,
  isBindPassword,
  bindPassword,
  bindPassword1,
  bindPassword2,
  validatePhone,
  validatePassword,
  validatePassword1,
  validatePassword2,
  prePaidCardList,
  deleteCard,
  getCardDesc,
  getPhone,
  queryAmount,
  listCardLevel,
  queryCardLevel,
  resetPassword,
  resetPassword1,
  resetPassword2,
  getMemberCardDesc,
  saleIntems,
  isCanUseSign,
  saveMemberSign,
  signConfig,
  fetchGetAddCardParam,
  getListMemberDataConfig,
  getMemberInformation,
  queryConfig,
  getQrcode,
  editMemberInformation,
  queryMemberCardInfo,
  getCrmMember,
  showLevelPowerByGradeId,
  getLevelPowerByGradeId
};
