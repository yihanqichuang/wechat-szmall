/**
 * Created by Administrator on 2019/4/23.
 */
import { oldFetch } from '../utils/fetch.js';

// 拍照送积分入口功能页
const getHomePage = (params) => oldFetch('wechat/ownIntegral/ownIntegralHomePage.json', params, 'POST');

// 保存记录
const saveOwnIntegralRecord = (params) => oldFetch('wechat/ownIntegral/saveOwnIntegralRecord.json', params, 'POST');

// 获取拍照积分记录
const getIntegralRecordList = (params) => oldFetch('wechat/ownIntegral/ownIntegralRecordList.json', params, 'POST');

const shopList = (params) => oldFetch('wechat/ownIntegral/shopList.json', params, 'POST');

// 扫码积分
const scanCode = (params) => oldFetch('wechat/scanCode/saveVerificationZhengDa.json', params, 'POST');

module.exports = {
  getHomePage,
  saveOwnIntegralRecord,
  shopList,
  scanCode,
  getIntegralRecordList
};
