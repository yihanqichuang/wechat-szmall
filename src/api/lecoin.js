import { oldFetch } from '../utils/fetch.js';

// 获取乐币兑换配置
const getExchangeConfig = (params) => oldFetch('wechat/lecoin/getExchangeConfig.json', params, 'POST');

// 立即兑换
const exchangeByPoints = (params) => oldFetch('wechat/lecoin/exchangeByPoints.json', params, 'POST');


module.exports = {
  getExchangeConfig,
  exchangeByPoints
};
