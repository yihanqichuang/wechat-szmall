import { oldFetch } from '../utils/fetch.js';

// 获取标签信息
const getLabel = params =>
  oldFetch('wechat/memberInfo/getLabelInfo.json', params, 'POST');
// 获取用户标签
const getUserLabel = params =>
  oldFetch('wechat/memberInfo/getMemLabelList.json', params, 'POST');
// 修改用户标签
const editUserLabel = params =>
  oldFetch('wechat/memberInfo/updateMemLabel.json', params, 'POST');
  
module.exports = {
  getLabel,
  getUserLabel,
  editUserLabel
};
