/*
 * @Author: your name
 * @Date: 2020-10-19 14:09:29
 * @LastEditTime: 2020-11-27 15:01:08
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: /WangMao-yidong/wxml-nbss/src/api/coupon.js
 */
/**
 * Created by AklanFun on 2018/3/27.
 */
import { oldFetch, newFetch } from '../utils/fetch.js';

// 获取 优惠券列表
const fetchCouponList = (params) => oldFetch('wechat/webcoupon/listCouponDefault.json', params, 'POST')

// @new 获取优惠券列表
const fetchCouponListNew = (params) => oldFetch('szmall/coupon/wechat/couponSaleList', params, 'POST')

// 获取 优惠券详情
const fetchCouponDetail = (params) => oldFetch('wechat/webcoupon/getCouponDesc.json', params, 'POST')

// 提交优惠券 积分兑换或者免费
const fetchCouponExchange = (params) => oldFetch('wechat/webcoupon/exchangeByPoints.json', params, 'POST')

// 提交优惠券 现金购买
const fetchCouponExchangeBuy = (params) => oldFetch('wechat/webcoupon/exchangeByBuy.json', params, 'POST')

// 优惠券购买支付参数获取
const toPayPage = (params) => oldFetch('wechat/webcoupon/toPayPage.json', params, 'POST')

// 退券
const refundCoupon = (params) => oldFetch('wechat/memberCoupon/refundCoupon.json', params, 'POST')

// 获取 我的票券 列表
const fetchMemberCouponList = (params) => oldFetch('wechat/memberCoupon/listMemberCoupon.json', params, 'POST')

// 获取 我的票券 列表 from cy
const fetchMemberCouponListCy = (params) => oldFetch('wechat/memberCoupon/listMemberCoupons.json', params, 'POST')

// 票券列表 新
const fetchMyCouponList = (params) => oldFetch('wechat/memberCoupon/memberCoupon.json', params, 'POST')

// 券核销码刷新
const refreshCouponCode = (params) => oldFetch('wechat/webcoupon/refreshCouponCode.json', params, 'POST')
// 优惠券列表 新
const couponSaleList = (params) => oldFetch('szmall/coupon/wechat/couponSaleList', params, 'POST');
// 领优惠券 新
const memberCouponGain = (params) => newFetch('wmallerWechat/memberCouponGain', params, 'POST');
// 领优惠券详情 新
const couponDetail = (params) => oldFetch('szmall/coupon/wechat/couponDetail', params, 'POST');
// 领优惠下单 新
const createGiftAndCouponOrder = (params) => oldFetch('szmall/coupon/wechat/buyCoupon', params, 'POST');
// 查询结果 新
const queryOrderCache = (params) => oldFetch('szmall/coupon/wechat/trackerCouponBuyState', params, 'POST');
// 我的优惠券 新
const userCouponList = (params) => oldFetch('szmall/coupon/wechat/userCouponList', params, 'POST');
// 退券 新
const applyRefund = (params) => newFetch('wmallerWechat/applyRefund', params, 'POST');
module.exports = {
  fetchCouponList,
  fetchCouponDetail,
  fetchCouponExchange,
  fetchMemberCouponList,
  fetchMemberCouponListCy,
  fetchCouponExchangeBuy,
  toPayPage,
  refundCoupon,
  refreshCouponCode,
  fetchMyCouponList,
  fetchCouponListNew,
  couponSaleList,
  memberCouponGain,
  couponDetail,
  createGiftAndCouponOrder,
  queryOrderCache,
  userCouponList,
  applyRefund
};
