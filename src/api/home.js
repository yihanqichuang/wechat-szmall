import { oldFetch, newFetch } from '../utils/fetch.js';

// 获取首页显示的配置模块
const getHomeModel = (params) => oldFetch('szmall/mall/wechat/homepage/homePage.json', params, 'POST');
// 获取超市页面的配置模块
const getMarketHomeModel = (params) => oldFetch('szmall/mall/wechat/homepage/homePage.json', params, 'POST');
// 获取单个模块数据   
const find = (params) => oldFetch('szmall/mall/wechat/homepage/queryByCode.json', params, 'POST');
// 会员升级
const memberCardUpgradeInfo = (params) => oldFetch('szmall/mall/wechat/memberCenter/memberCardUpgradeInfo.json', params, 'POST');
// 首页弹窗
const windowConfig = (params) => oldFetch('szmall/market/popupWindow/wechat/popupWindow.json', params, 'POST');
//  保存点击记录
const saveClickLog = (params) => oldFetch('szmall/market/popupWindow/saveClickLog.json', params, 'POST');
// 获取业态分类列表
const fetchIndustryList = (params) => oldFetch('szmall/mall/wechat/commodity/queryIndustryList.json', params, 'POST');
// 首页单品
const queryByCommodity = (params) => oldFetch('szmall/mall/wechat/homepage/queryByCommodity.json', params, 'POST');
// 获取首页一级分类
const queryCategoryList = (params) => oldFetch('szmall/mall/wechat/commodity/queryCategoryList.json', params, 'POST');

// 获取首页信息
const getHomeList = (params) => oldFetch('szmall/mall/queryIndexData', params, 'POST');

// 获取限时折扣展示模块
const fetchTimerModule = (params) => oldFetch('szmall/market/wechat/currentMarketTimer', params, 'POST');

// 获取满减展示模块
const fetchFullModule = (params) => newFetch('szmall/mall/wechat/currentMarketFull', params, 'POST');

// 获取第2件活动展示模块
const fetchSecondModule = (params) => newFetch('szmall/mall/wechat/currentMarketSecond', params, 'POST');

// 获取优惠套餐活动展示模块
const fetchPackageModule = (params) => newFetch('szmall/mall/wechat/currentMarketPackage', params, 'POST');

// 拼团活动展示模块
const fetchgroupActivity = params => newFetch('szmall/mall/wechat/currentMarketGroup', params, 'POST')

module.exports = {
  getHomeList,
  getHomeModel,
  find,
  memberCardUpgradeInfo,
  windowConfig,
  saveClickLog,
  fetchIndustryList,
  queryByCommodity,
  queryCategoryList,
  fetchTimerModule,
  fetchFullModule,
  fetchSecondModule,
  getMarketHomeModel,
  fetchPackageModule,
  fetchgroupActivity,
  getMarketHomeModel
}
