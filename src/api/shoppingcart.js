import { oldFetch } from '../utils/fetch.js';

// 查询购物车商品数量
const fetchShoppingCartCount = (params) => oldFetch('szmall/vip/memberCart/cartSkuTotalCount', params, 'POST')

// 购物车内加入商品
const fetchUpdateCartCount = (params) => oldFetch('szmall/vip/memberCart/addOrUpdateCart', params, 'POST')

// 商品详情内加入商品到购物车
const fetchAddProductToCart = (params) => oldFetch('szmall/vip/memberCart/addOrUpdateCartOffset', params, 'POST')

// 购物车商品列表
const fetchShoppingCartProducts = (params) => oldFetch('szmall/vip/memberCart/queryCartList', params, 'POST')

// 删除
const fetchCartDelete = (params) => oldFetch('szmall/vip/memberCart/deleteCart', params, 'POST')

module.exports = {
  fetchShoppingCartCount,
  fetchUpdateCartCount,
  fetchAddProductToCart,
  fetchCartDelete,
  fetchShoppingCartProducts
}
