/**
 * Created by AklanFan on 2020/3/4.
 */
import { oldFetch, newFetch } from '../utils/fetch.js';

// 获取 一级分类列表
const fetchCategoryList = (params) => oldFetch('szmall/product/wechat/goods/categoryFore/getLevelList.json', params, 'POST')
// 根据上级分类获取子级分类列表
const fetchSubCategory = (params) => oldFetch('szmall/product/wechat/goods/categoryFore/listByTreeAndFore.json', params, 'POST')
// 根据上级分类获取子级分类列表
const fetchGoodsByCategory = (params) => oldFetch('szmall/product/wechat/goods/categoryFore/getForeByMallCommodityList.json', params, 'POST')
// 获取主题列表
const fetchThemeList = (params) => oldFetch('wechat/homepageTheme/homePageThemeList.json', params, 'POST')
// 获取主题列表
const fetchThemeDetail = (params) => oldFetch('wechat/homepageTheme/queryByThemeId.json', params, 'POST')
// 获取主题列表
const fetchCategoryByShop = (params) => oldFetch('wechat/commodity/queryShopByCategoryList.json', params, 'POST')


module.exports = {
  fetchCategoryList,
  fetchSubCategory,
  fetchGoodsByCategory,
  fetchThemeList,
  fetchThemeDetail,
  fetchCategoryByShop
}
