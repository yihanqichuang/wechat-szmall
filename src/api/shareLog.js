/**
 * Created by yu tao on 2018/8/2.
 */

import { oldFetch } from '../utils/fetch.js';

// 分享日志
const shareLog = (params) => oldFetch('wechat/sharePageLog/add.json', params, 'POST');

const shareLogCommon = (relationId, relationType) => {
  let params = {
    relationId: relationId,
    relationType: relationType
  };
  shareLog(params).then(res => {
    console.log(res)
  }).catch(err => {
    console.log(err)
  })
};

module.exports = {
  shareLog,
  shareLogCommon
}
