/*
 * @Author: your name
 * @Date: 2020-09-28 10:24:56
 * @LastEditTime: 2020-10-12 15:13:46
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: /WangMao-yidong/wxml-nbss/src/api/search.js
 */
import { newFetch, oldFetch } from '../utils/fetch.js';
// 满减
const currentFullMarketList = (params) => newFetch('wmallerWechat/currentFullMarketList', params, 'POST');
const fullMarketDetail = (params) => newFetch('wmallerWechat/fullMarketDetail', params, 'POST');
const fullMarketSpuList = (params) => newFetch('wmallerWechat/fullMarketSpuList', params, 'POST');
// 限时折扣
const currentTimerMarketBatchesList = (params) => oldFetch('szmall/market/wechat/currentTimerMarketBatchesList', params, 'POST');
const timerMarketSpuList = (params) => oldFetch('szmall/market/wechat/timerMarketSpuList', params, 'POST');

// 第2件活动
const currentSecondMarketList = (params) => newFetch('wmallerWechat/currentSecondMarketList', params, 'POST');

// 优惠套餐活动
const currentPackageMarketList = (params) => newFetch('wmallerWechat/currentPackageMarketList', params, 'POST');


module.exports = {
  currentFullMarketList,
  fullMarketDetail,
  fullMarketSpuList,
  currentTimerMarketBatchesList,
  timerMarketSpuList,
  currentSecondMarketList,
  currentPackageMarketList
}
