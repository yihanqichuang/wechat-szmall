/*
 * @Author: your name
 * @Date: 2020-09-28 10:24:56
 * @LastEditTime: 2020-09-29 15:55:04
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: /WangMao-yidong/wxml-nbss/src/api/search.js
 */
import { oldFetch, newFetch } from '../utils/fetch.js';

// 模糊搜索
const searchKeyword = (params) => newFetch('wmallerWechat/searchKeyword', params, 'POST');

// 热门商品
const hotSearchKeyword = (params) => newFetch('wmallerWechat/hotSearchKeyword', params, 'POST');
// 商铺列表
const getShopListNew = (params) => oldFetch('wechat/shop/getShopListNew.json', params, 'POST');
// 商品列表
const productList = (params) => oldFetch('szmall/product/wechat/product/productList', params, 'POST');
module.exports = {
  searchKeyword,
  hotSearchKeyword,
  getShopListNew,
  productList
}
