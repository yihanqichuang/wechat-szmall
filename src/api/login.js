import { oldFetch, newFetch } from '../utils/fetch.js';

// 登陆
const login = (url, params) => oldFetch(url, params, 'POST');

// 获取用户信息
const getUserInfo = (params) => oldFetch('szmall/vip/wechatLoginInfo.json', params, 'POST');

// 获取商场ID
const nearestOrganiz = (params) => oldFetch('szmall/mall/cityConfig/nearestOrganize.json', params, 'POST');

// 获取小程序默认值
const defaultConfig = (params) => oldFetch('wechat/miniLogin/defaultConfig.json', params, 'POST');

// 获取tabbar-新
const getTabbar = (params) => oldFetch('szmall/mall/queryIndexTabbar', params, 'POST');

// 根据organizeId获取组织机构信息
const fetchOrganizeInfo = (params) => oldFetch('szmall/user/getOrganizeById', params, 'POST')

// 保存分销分享人id
const fetchSaveShareId = (params) => newFetch('wmallerWechat/saveRelationMemberDistribution', params, 'POST')

// 获取拼团分享后参数数据
const getQrcodeParamById = params => newFetch('wmallerWechat/getQrcodeParamById', params, 'POST')

// 获取会员权益说明
const getMemberDesc = params => oldFetch('szmall/vip/memberCardSs/getCardDesc.json', params, 'POST');

module.exports = {
  login,
  getTabbar,
  nearestOrganiz,
  defaultConfig,
  fetchOrganizeInfo,
  fetchSaveShareId,
  getQrcodeParamById,
  getUserInfo,
  getMemberDesc
}
