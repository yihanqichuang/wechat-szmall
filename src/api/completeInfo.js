import { oldFetch } from '../utils/fetch.js';

// 获取用户信息
const getUserInfo = (params) => oldFetch('szmall/vip/memberInfo/getMemberInfoForUpdate.json', params, 'POST');
// 发送二维码
const sendCode = (params) => oldFetch('wechat/memberInfo/getSecurityCode.json', params, 'POST');
// 编辑用户信息
const editUserInfo = (params) => oldFetch('szmall/vip/memberInfo/saveMemberInfo.json', params, 'POST');

module.exports = {
  getUserInfo,
  sendCode,
  editUserInfo
};
