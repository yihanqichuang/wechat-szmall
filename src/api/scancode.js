import { oldFetch } from '../utils/fetch.js';
// 主扫相关接口

// 获取订单单据数据
const getMemberPromotion = (params) => oldFetch('wechat/scanCodePay/getMemberPromotion.json', params, 'POST');

// 重新计算会员优惠
const recountMoney = (params) => oldFetch('wechat/scanCodePay/recountMoney.json', params, 'POST');

// 订单确认提交
const confirmPay = (params) => oldFetch('wechat/scanCodePay/confirmPay.json', params, 'POST');

// 积分兑换
const getVipScoreMoney = (params) => oldFetch('wechat/scanCodePay/getVipScoreMoney.json', params, 'POST');

module.exports = {
  getMemberPromotion,
  recountMoney,
  confirmPay,
  getVipScoreMoney
};
