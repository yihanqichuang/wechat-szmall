/**
 * 电子发票接口
 */
import { oldFetch } from '../utils/fetch.js';

// 获取 未开票订单列表
const fetchOrderList = (params) => oldFetch('wechat/einvoice/listOrder.json', params, 'POST')

// 获取 已开票订单列表
const fetchEinvoiceOrderList = (params) => oldFetch('szmall/order/selectEinvociePage', params, 'POST')

// 模糊查询企业名称
const fetchCompany = (params) => oldFetch('wechat/einvoice/listQyxx.json', params, 'POST')

// 发票抬头列表
const fetchEleTile = (params) => oldFetch('szmall/order/einvoiceTitle/list.json', params, 'POST')

// 新增发票抬头
const fetchSaveTitle = (params) => oldFetch('szmall/order/einvoiceTitle/save.json', params, 'POST')

// 编辑抬头
const fetchUpdateTitle = (params) => oldFetch('szmall/order/einvoiceTitle/update.json', params, 'POST')

// 发票抬头详情
const fetchTitleDetail = (params) => oldFetch('szmall/order/einvoiceTitle/detail.json', params, 'POST')

// 获取pdf地址
const fetchPdfUrl = (params) => oldFetch('wechat/einvoice/getDownloadUrl.json', params, 'POST')

// 发送pdf至邮箱
const fetchSendEmail = (params) => oldFetch('wechat/einvoice/sendEmail.json', params, 'POST')

// 开具发票
const fetchBillingInvoice = (params) => oldFetch('szmall/order/billingInvoice', params, 'POST')

// 默认抬头
const fetchDefaultTitle = (params) => oldFetch('szmall/order/einvoiceTitle/defaultTitle.json', params, 'POST')

// 订单详情
const fetchOrderDetail = (params) => oldFetch('wechat/einvoice/orderDetail.json', params, 'POST')

// 发票详情
const selectEinvocieDetail = (params) => oldFetch('szmall/order/selectEinvocieDetail', params, 'POST')

// 是否已开票
const fetchIsOpenedEinvocie = (params) => oldFetch('wechat/einvoice/isOpenedEinvocie.json', params, 'POST')

// 删除发票抬头
const fetchDelete = (params) => oldFetch('szmall/order/einvoiceTitle/delete.json', params, 'POST')

module.exports = {
  fetchOrderList,
  fetchCompany,
  fetchEleTile,
  fetchSaveTitle,
  fetchUpdateTitle,
  fetchTitleDetail,
  fetchPdfUrl,
  fetchSendEmail,
  fetchBillingInvoice,
  fetchDefaultTitle,
  fetchOrderDetail,
  selectEinvocieDetail,
  fetchDelete,
  fetchEinvoiceOrderList,
  fetchIsOpenedEinvocie
}
