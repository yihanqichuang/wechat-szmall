/**
 * Created by AklanFun on 2018/3/27.
 */
import { oldFetch } from '../utils/fetch.js';

// 获取 礼品列表
const fetchPointsList = (params) => oldFetch('szmall/coupon/wechat/pointsReward/list.json', params, 'POST')

// 获取 礼品详情
const fetchPointsDetail = (params) => oldFetch('szmall/coupon/wechat/pointsReward/info.json', params, 'POST')

// 提交礼品兑换
const fetchPointsExchange = (params) => oldFetch('szmall/coupon/wechat/pointsReward/exchange.json', params, 'POST')

module.exports = {
  fetchPointsList,
  fetchPointsDetail,
  fetchPointsExchange
}
