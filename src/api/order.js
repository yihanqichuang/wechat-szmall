import { oldFetch, newFetch } from '../utils/fetch.js';

// 物流公司常量列表
const fetchDeliveryList = (params) => oldFetch('szmall/mall/wechat/homepage/getConstantsByKey.json', params, 'POST');

// 获取商场订单，小订单全状态
const fetchOrderList = (params) => oldFetch('szmall/order/memberShopOrderList', params, 'POST')

// 获取待支付大订单
const fetchUnpayOrderList = (params) => oldFetch('szmall/order/memberOrderList', params, 'POST')

// 获取售后订单列表
const fetchRefundList = (params) => oldFetch('szmall/order/refundPage', params, 'POST')

// 获取大订单详情
const fetchUnpayOrderDetail = (params) => oldFetch('szmall/order/getOrderDetail', params, 'POST')

// 获取小订单详情
const fetchOrderDetail = (params) => oldFetch('szmall/order/getShopOrderDetail', params, 'POST')

// 取消订单
const fetchCancelOrder = (params) => oldFetch('szmall/order/cancelOrder', params, 'POST')

// 删除订单(大订单)
const fetchDeleteMainOrder = (params) => oldFetch('szmall/order/deleteOrder', params, 'POST')

// 删除订单
const fetchDeleteOrder = (params) => oldFetch('szmall/order/deleteShopOrder', params, 'POST')

// 超市订单申请售后（大订单）
const fetchMainRefundApply = (params) => oldFetch('szmall/order/applyMainRefund', params, 'POST')

// 售后申请
const fetchRefundApply = (params) => oldFetch('szmall/order/applyRefund', params, 'POST')

// 售后详情
const fetchRefundDetail = (params) => oldFetch('szmall/order/refundDetail', params, 'POST')
const fetchRefundDetail2 = (params) => oldFetch('szmall/order/refundDetailBySku', params, 'POST')

// 撤销售后申请
const fetchRevertRefundApply = (params) => oldFetch('szmall/order/cancelRefund', params, 'POST')

// 售后申请退款信息补全
const fetchRefundInfoComplete = (params) => oldFetch('szmall/order/refundFillExpress', params, 'POST')

// 确认收货
const fetchConfirmReceive = (params) => oldFetch('szmall/order/batchConfirmReceive', params, 'POST')
// 超市确认收货
const fetchMarketConfirmReceive = (params) => oldFetch('szmall/order/marketConfirmReceive', params, 'POST')

// 查看物流
const fetchLogisticsInfo = (params) => oldFetch('szmall/order/queryCourierTag', params, 'POST')

// 删除售后订单
const fetchDeleteRefundOrder = (params) => oldFetch('szmall/order/deleteRefund', params, 'POST')

// 获取会员包邮权益信息
const fetchGetFreeShipInfo = (params) => oldFetch('szmall/order/getMemberFreeShipLevelPower', params, 'POST')

// 获取会员折扣权益信息
const fetchGetDiscountInfo = (params) => oldFetch('szmall/order/getMemberDiscountLevelPower', params, 'POST')

module.exports = {
  fetchOrderList,
  fetchUnpayOrderList,
  fetchRefundList,
  fetchUnpayOrderDetail,
  fetchOrderDetail,
  fetchCancelOrder,
  fetchDeleteMainOrder,
  fetchDeleteOrder,
  fetchMainRefundApply,
  fetchRefundApply,
  fetchRefundDetail,
  fetchRevertRefundApply,
  fetchRefundInfoComplete,
  fetchDeliveryList,
  fetchRefundDetail2,
  fetchConfirmReceive,
  fetchDeleteRefundOrder,
  fetchLogisticsInfo,
  fetchMarketConfirmReceive,
  fetchGetFreeShipInfo,
  fetchGetDiscountInfo
}

