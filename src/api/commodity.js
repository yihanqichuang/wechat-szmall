/**
 * Created by AklanFun on 2018/4/3.
 */
import { oldFetch, newFetch } from '../utils/fetch.js';

// 获取 商品 列表
const fetchCommodityList = (params) => oldFetch('wechat/commodity/listCommodity.json', params, 'POST')

// 确认订单页面信息
const fetchOrderView = (params) => oldFetch('wechat/order/orderView.json', params, 'POST')

// 提交订单
const fetchSubmitOrder = (params) => oldFetch('wechat/memberOrder/createOrder.json', params, 'POST');

// 购物车结算
const fetchSettlement = (params) => oldFetch('wechat/memberOrderShop/settlement.json', params, 'POST')

// 获取业态分类列表
const fetchIndustryList = (params) => oldFetch('wechat/commodity/queryIndustryList.json', params, 'POST');
// 获取可用优惠券列表
const fetchUseCouponList = (params) => oldFetch('wechat/memberOrder/queryUsableCoupon.json', params, 'POST');
// 获取当前订单运费信息
const fetchFreightInfo = (params) => oldFetch('wechat/freight/getFreight.json', params, 'POST');


// 获取 商品 详情----------------
const fetchCommodityDetail = (params) => oldFetch('szmall/product/wechat/product/productDetail', params, 'POST')

// 获取商品相关优惠券
const fetchProductCoupons = (params) => oldFetch('szmall/coupon/wechat/productUseCouponList', params, 'POST')

// 获取商铺相关满减阶梯信息
const fetchProductFullMarket = (params) => newFetch('wmallerWechat/fullMarketLadderByMarketCodeList', params, 'POST')

// 获取商品第二件活动信息
const secondMarketByCode = (params) => newFetch('wmallerWechat/secondMarketByCode', params, 'POST')

// 获取当前订单可用优惠券列表
const fetchOrderAvailableCoupons = (params) => oldFetch('szmall/coupon/wechat/memberGoodsAvailCoupons', params, 'POST')

// 预下单
const fetchPreCreateOrder = (params) => oldFetch('szmall/order/preCheckOrder', params, 'POST')

// 正式下单(百货)
const fetchCreateOrder = (params) => oldFetch('szmall/order/createGoodsOrder', params, 'POST')

// 正式下单(超市)
const fetchCreateSupperOrder = (params) => newFetch('wmallerWechat/createSupperGoodsOrder', params, 'POST')

// 轮询查询下单结果
const fetchCheckCreateOrderRes = (params) => oldFetch('szmall/order/queryOrderCache', params, 'POST')

// 获取支付参数
const fetchOrderPayParams = (params) => oldFetch('szmall/order/payOrder', params, 'POST')

// 获取储值卡信息
const fetchValueCardInfo = (params) => newFetch('wmallerWechat/memberPrepaidCard', params, 'POST')

// 校验储值卡密码
const fetchValidateCardPassword = (params) => newFetch('wmallerWechat/checkPrepaidCardPassword', params, 'POST')

// 校验是否储值卡支付成功
const fetchIsCardPaySuccess = (params) => newFetch('wmallerWechat/checkPrepaidPaid', params, 'POST')

// 检查赠品数量是否已经发放完成
const fetchPreCheckGiveawayStock = (params) => newFetch('wmallerWechat/preCheckGiveawayStock', params, 'POST')

// 获取分销佣金详情
const fetchGetDetailDistributionPrice = (params) => newFetch('wmallerWechat/getDetailDistributionPrice', params, 'POST')

// 获取优惠套餐详情
const fetchCurrentMarketPackageDetail = (params) => newFetch('wmallerWechat/currentMarketPackageDetail', params, 'POST')

// 获取商品相关联优惠套餐
const selectMarketPackageBySpuCode = (params) => newFetch('wmallerWechat/selectMarketPackageBySpuCode', params, 'POST')

// 查询下单未支付倒计时时间
const getCloseOrderTime = (params) => oldFetch('szmall/order/getCloseOrderTime', params, 'POST')

module.exports = {
  fetchCommodityList,
  fetchOrderView,
  fetchSubmitOrder,
  fetchSettlement,
  fetchIndustryList,
  fetchUseCouponList,
  fetchFreightInfo,
  fetchCommodityDetail,
  fetchProductCoupons,
  fetchProductFullMarket,
  secondMarketByCode,
  fetchPreCreateOrder,
  fetchCreateOrder,
  fetchCreateSupperOrder,
  fetchCheckCreateOrderRes,
  fetchOrderPayParams,
  fetchOrderAvailableCoupons,
  fetchValueCardInfo,
  fetchValidateCardPassword,
  fetchIsCardPaySuccess,
  fetchPreCheckGiveawayStock,
  fetchGetDetailDistributionPrice,
  fetchCurrentMarketPackageDetail,
  selectMarketPackageBySpuCode,
  getCloseOrderTime
}
