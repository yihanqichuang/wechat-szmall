/**
 * Created by AklanFun on 2018/3/27.
 */
import { oldFetch } from '../utils/fetch.js';

// 获取 礼品列表
const fetchExchangeList = (params) => oldFetch('szmall/coupon/memberCoupon/listMemberCoupon.json', params, 'POST')

// 获取 礼品核销信息
const fetchExchangeInfo = (params) => oldFetch('szmall/coupon/memberCoupon/getVerifyCoupon.json', params, 'POST')

// 退礼品
const refundGift = (params) => oldFetch('wechat/pointsReward/refundGift.json', params, 'POST')

module.exports = {
  fetchExchangeList,
  fetchExchangeInfo,
  refundGift,
};
