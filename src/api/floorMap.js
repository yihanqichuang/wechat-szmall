import { oldFetch } from '../utils/fetch.js';

// 获取首页显示的配置模块
const getFloorMap = (params) => oldFetch('wechat/mallFloor/floorList.json', params, 'POST');
// 获取单个模块数据
module.exports = {
  getFloorMap
};
