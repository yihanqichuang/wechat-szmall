import { newFetch } from '../utils/fetch.js';

// 查询邀请有礼详情
const queryInviteCourteous = (params) => newFetch('wmallerWechat/queryInviteCourteous', params, 'POST')
const toInvite = (params) => newFetch('wmallerWechat/toInvite', params, 'POST')
const beInvited = (params) => newFetch('wmallerWechat/beInvited', params, 'POST')
const giveRewards = (params) => newFetch('wmallerWechat/giveRewards', params, 'POST')


module.exports = {
  queryInviteCourteous,
  toInvite,
  beInvited,
  giveRewards
}
