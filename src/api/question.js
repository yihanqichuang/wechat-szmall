/**
 * Created by yu tao on 2019/1/30.
 * 活动问卷相关API接口
 */
import { oldFetch } from '../utils/fetch.js';

// 获取 问卷列表
const questionList = (params) => oldFetch('szmall/market/wechat/questionnaire/list.json', params, 'POST');

// 获取 问卷详情
const questionDetail = (params) => oldFetch('szmall/market/wechat/questionnaire/detail.json', params, 'POST');

// 提交 问卷表单
const questionSubmit = (params) => oldFetch('szmall/market/wechat/questionnaire/submit.json', params, 'POST');


module.exports = {
  questionList,
  questionDetail,
  questionSubmit
};
