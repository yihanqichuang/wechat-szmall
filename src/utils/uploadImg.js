/* eslint-disable */

/**
 *
 * @param count
 * @param sizeType
 * @param sourceType
 * @returns {*|i}
 */
let api = '${hostname}';
module.exports = function (count, sizeType, sourceType) {
  return new Promise((resolve, reject) => {
    wx.chooseImage({
      count: count, // 默认9
      sizeType: sizeType, // 可以指定是原图还是压缩图，默认二者都有
      sourceType: sourceType, // 可以指定来源是相册还是相机，默认二者都有
      success: function (res) {
        let imgList = [];
        res.tempFilePaths.forEach((item) => {
          wx.uploadFile({
            url: `${api}/wechat/wechatCommon/saveImgToOSS.json`,
            filePath: item,
            name: 'image',
            header: {'Content-Type': 'multipart/form-data'},
            formData: {
              'appType': 'smallApp',
              'hash': wx.getStorageSync('hash')
            },
            success: function (res) {
              console.log(res);
              var ret = JSON.parse(res.data);
              if (ret.flag) {
                let data = ret.data.substr(0, ret.data.length - 1);
                imgList.push(data);
                resolve(data);
              } else {
                wx.showModal({
                  title: '提示',
                  content: '上传失败',
                  showCancel: false
                })
              }
            },
            fail: function () {
              wx.showModal({
                title: '提示',
                content: '上传失败',
                showCancel: false
              })
              reject;
            },
            complete: function () {
              wx.hideToast();
            }
          })
        })
      }
    })
  })
};


