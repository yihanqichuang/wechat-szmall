/* eslint-disable */
var CryptoJS = require('./crypto-js');
import { PUBKEY } from './pubKey'
/*
* AES 加密 返回加密之後的字符串
* */
function encrypt(word){
  var key = CryptoJS.enc.Utf8.parse(PUBKEY);
  var srcs = CryptoJS.enc.Utf8.parse(word);
  var encrypted = CryptoJS.AES.encrypt(srcs, key, {mode:CryptoJS.mode.ECB,padding: CryptoJS.pad.Pkcs7});
  return encrypted.toString();
}

/*
 * AES 解密 返回解密之後的字符串
 * */
function decrypt(word){
  var key = CryptoJS.enc.Utf8.parse(PUBKEY);
  var decrypt = CryptoJS.AES.decrypt(word, key, {mode:CryptoJS.mode.ECB,padding: CryptoJS.pad.Pkcs7});
  return CryptoJS.enc.Utf8.stringify(decrypt).toString();
}

module.exports = {
  encrypt,
  decrypt
};


