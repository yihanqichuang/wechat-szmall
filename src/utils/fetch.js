import getCurrentPageUrl from './getCurrentPageUrl.js';
/*
@param  {String} api     请求地址
**@param {String} path   请求路径
**@param {Object} params 请求参数
**@param {String} method type: GET || POST 需大写1
*/
/* eslint-disable */
let api = '${hostname}';
let fxApi = '${fxHostName}';
let apiNew = '${hostnameNew}';
let appID = '${appId}';
console.log('${shareDomain}')
let shareDomains = '${shareDomain}'; //分校模块的域名
const getFetchData = function () {
  let hash = wx.getStorageSync('hash');
  let pages = getCurrentPages();
  let currentPage = pages[pages.length - 1];
  let options = currentPage ? currentPage.options : {};
  let organizeId = wx.getStorageSync('organizeId');
  let currentPageUrl = getCurrentPageUrl.getCurrentPageUrl();
  let scene = wx.getStorageSync('scene');
  let appid = wx.getStorageSync('appId');
  let appId = appID;
  let roomId = (options && options.room_id && options.room_id !== 'undefined') ? options.room_id : '';
  let openId = (options && options.openid && options.openid !== 'undefined') ? options.openid : '';
  let shareOpenId = (options && options.share_openid && options.share_openid !== 'undefined') ? options.share_openid : '';
  let customParams = (options && options.custom_params && options.custom_params !== 'undefined') ? options.custom_params : '';
  if (!hash) {
    return false;
  }
  let requestData = {};
  requestData.hash = hash;
  requestData.organizeId = organizeId || 0;
  requestData.appType = 'smallApp';
  requestData.pageUrl = currentPageUrl;
  requestData.scene = scene;
  requestData.appid = appid;
  requestData.appId = appId;
  if (roomId) {
    requestData.roomId = roomId;
    requestData.openId = openId;
    requestData.shareOpenId = shareOpenId;
    requestData.customParams = customParams;
  }
  return requestData;
};
// 老系统请求
const oldFetch = function (path, params, type) {
  return new Promise((resolve, reject) => {
    let headerObj = {
      'Content-type': 'application/json;charset=UTF-8',
      'appType': 'wechat'
    }
    if (wx.getStorageSync('token') && path.indexOf('szmall/oauth/token/') == -1) {
      headerObj.Authorization = 'bearer ' + wx.getStorageSync('token');
    }
    if (!params) {
      params = {};
    }
    params.appid = appID;
    if (params.organizeId === null || params.organizeId === undefined || params.organizeId === '') {
      params.organizeId = wx.getStorageSync('organizeId') || 0;
      headerObj.organizeId = wx.getStorageSync('organizeId') || 0;
    } else {
      headerObj.organizeId = params.organizeId;
    }
    return handleRequest(path, params, headerObj, type, resolve, reject);
  })
};

// 分销系统请求
const fxFetch = function (path, params, type) {
    return new Promise((resolve, reject) => {
      let headerObj = {
        'Content-type': 'application/json;charset=UTF-8',
        'appType': 'wechat'
      }
      if (!params) {
        params = {};
      }
      params.appid = appID;
      if (params.organizeId === null || params.organizeId === undefined || params.organizeId === '') {
        params.organizeId = wx.getStorageSync('organizeId') || 0;
        headerObj.organizeId = wx.getStorageSync('organizeId') || 0;
      } else {
        headerObj.organizeId = params.organizeId;
      }
      return fxHandleRequest(path, params, headerObj, type, resolve, reject);
    })
  };

  // 发请求
const fxHandleRequest = function (path, params, headerObj, type, resolve, reject) {
    wx.request({
      url: `${fxApi}/${path}`,
      data: params,
      header: headerObj,
      method: type || 'GET',
      success: function (e) {
        resolve(e);
      },
      fail: reject
    })
  };

// 发请求
const handleRequest = function (path, params, headerObj, type, resolve, reject) {
  wx.request({
    url: `${api}/${path}`,
    data: params,
    header: headerObj,
    method: type || 'GET',
    success: function (e) {
      if (e.statusCode == 401) {
        setTimeout(() => {
          headerObj.Authorization = 'bearer ' + wx.getStorageSync('token');
          handleRequest(path, params, headerObj, type, resolve, reject);
        }, 500);
      } else {
        resolve(e);
      }
    },
    fail: reject
  })
};

// 新请求
const newFetch = function (path, params, type) {
  return new Promise((resolve, reject) => {
    const commonData = getFetchData();
    wx.request({
      url: `${apiNew}/${path}`,
      data: { params: JSON.stringify(params) },
      header: {
        'Content-Type': type === 'POST' ? 'application/x-www-form-urlencoded' : 'json',
        'organizeId': commonData.organizeId,
        'hash': commonData.hash,
        'appId': commonData.appId
      },
      method: type || 'GET',
      success: (response) => {
        const data = groupNewResponse(response.data)
        resolve(data)
      },
      fail: resolve
    })
  })
}

/// 新接口响应结果集重构为老报文
const groupNewResponse = (data) => {
  // 如果存在success属性，表示这是新系统的响应结构
  // 转换为老系统的响应报文格式，方便统一处理
  if (data.result && data.result.hasOwnProperty('totalCount')) {
    // 存在分页
    data = {
      code: data.errorCode || 0,
      flag: data.success,
      data: data.result.data || [],
      page: {
        curPage: data.result.curPage,
        totalCount: data.result.totalCount,
        pages: data.result.pages
      }
    };
  } else {
    // 没有分页
    data = { code: data.errorCode || 0, flag: data.success, data: data.result };
  }
  return data;
};


/*
  分销模块的请求方法
*/

//DAJc6dW8KRf3tkhSGEDTnZzTxu2B62kV

let needData = {
  _acid: 365,
  _version: '2.8.9',
  _platform: 'wx',
  r: ''
}

const jdFetch = function (param = {}) {
  console.log('域名', shareDomains)
  let organize = wx.getStorageSync("fxOrganize");
  console.log(param)
  if (param.showLoading) {
    wx.showLoading()
  }
  param.data = param.data ? param.data : {};
  needData.r = 'api/' + param.url;
  Object.assign(param.data, needData)
  param.data.access_token = wx.getStorageSync("jdShareInfo").access_token;
  if (!param.data.organizeId) {
    console.log("原来没传organizeId")
    param.data.organizeId = organize.organizeId;
  }
  return new Promise((resolve, reject) => {
    wx.request({
      url: shareDomains,
      data: param.data,
      header: {
        "content-type": param.method == 'get' ? "application/x-www-form-urlencoded" : "multipart/form-data",
        access_token: param.data.access_token
      },
      method: param.method || "get",
      success: (res) => {
        wx.hideLoading()
        resolve(res.data);
      },
      fail: (err) => {
        wx.hideLoading()
        reject(err)
      }
    })
  })
}

module.exports = {
  oldFetch,
  newFetch,
  jdFetch,
  fxFetch
}
