/* eslint-disable */
import BenMath from './BenMath.js'

function isInteger(obj){
  return typeof obj === 'number' && obj%1 === 0;      //是整数，则返回true，否则返回false
}

// 去除小数点部分
function floatFilter(value) {
  return parseFloat(value)
}

// 保留2位小数点
function priceFilter(value) {
  var _value = value * 1;
  var valTemp = _value.toFixed(3);
  return valTemp.substring(0, valTemp.lastIndexOf('.') + 3)
}

// 分转元(保留2位小数点)
function Fen2Yuan(value) {
  return priceFilter(parseFloat(value) / 100)
}

// 分转元（整数不保留2位小数）
function Fen2YuanInt(value) {
  let tempValue = parseFloat(value) / 100;
  if (isInteger(tempValue)) {
    return parseFloat(value) / 100
  } else {
    return priceFilter(parseFloat(value) / 100)
  }
}

// 元转分
function Yuan2Fen(value) {
  return Math.floor(BenMath.accMul(value,100))
}

module.exports = {
  Fen2Yuan: Fen2Yuan,
  Yuan2Fen: Yuan2Fen,
  priceFilter: priceFilter,
  floatFilter: floatFilter,
  Fen2YuanInt: Fen2YuanInt
};

