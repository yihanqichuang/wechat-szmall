/*
 * Created by yu tao on 2019/5/27.
 * 节流方法，不同场景选择不同的方法；
 */

/**
 * 节流方法 （时间戳）时间戳版的函数触发是在时间段内开始的时候
 * @param func 需要包裹的方法
 * @param wait 需要等待的时间
 * @returns {Function}
 */
function throttleTimeStrap(func, wait = 500) {
  let previous = 0;
  return function() {
    let context = this;
    let args = arguments;
    let now = Date.now();
    if (now - previous > wait) {
      func.apply(context, args);
      previous = Date.now();
    }
  }
}

/**
 * 节流方法 （定时器）定时器版的函数触发是在时间段内结束的时候
 * @param func 需要包裹的方法
 * @param wait 需要等待的时间
 * @returns {Function}
 */
function throttleTimer(func, wait = 500) {
  let timeout = null;
  return function() {
    let context = this;
    let args = arguments;
    if (!timeout) {
      timeout = setTimeout(() => {
        func.apply(context, args);
        timeout = null
      }, wait)
    }
  }
}


/**
 * 节流方法 （定时器+时间戳）当第一次触发事件时马上执行事件处理函数，
 * 最后一次触发事件后也还会执行一次事件处理函数
 * @param func 需要包裹的方法
 * @param wait 需要等待的时间
 * @returns {Function}
 */
function throttle(func, wait = 500) {
  let timer = null;
  let startTime = Date.now();
  return function() {
    let curTime = Date.now();
    let remaining = wait - (curTime - startTime);
    let context = this;
    let args = arguments;
    clearTimeout(timer);
    if (remaining <= 0) {
      func.apply(context, args);
      startTime = Date.now();
    } else {
      timer = setTimeout(func, remaining);
    }
  }
}

module.exports = {
  throttleTimeStrap,
  throttleTimer,
  throttle
};
