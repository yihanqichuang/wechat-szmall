
import api from '../api/activity.js'
import ProductApi from '../api/commodity'
// import wxtemplate from '../api/wxtemplate'
import nBack from './wx_navigateback.js'
import Tips from './tip.js'

// 老系统获取微信支付参数 调起微信支付
function getWxConfig(orderNo, totalFee, payType) {
  return new Promise((resolve, reject) => {
    api.getWxConfig2({
      orderNo,
      totalFee,
      payType,
      appId: wx.getStorageSync('appId'),
      organizeId: wx.getStorageSync('organizeId')
    }).then(res => {
      if (res.data.flag) {
        let ret = res.data.data;
        let { timeStamp, nonceStr, package: packages, paySign, signType } = ret;
        pay(timeStamp, nonceStr, packages, paySign, signType).then(ress => {
          resolve(ress);
          // 商品支付回调
          if (payType === 'COMMODITY') {
            Tips.success('支付成功', 1000).then(() => {
              wx.setStorageSync('myOrderList', 'myOrderList');
              nBack.setNavigateBackData({ paySuccess: true }, 1)
            })
          } else if (payType === 'GIFT') {
            Tips.success('支付成功', 1000).then(() => {
              wx.setStorageSync('exchangeList', 'exchangeList');
              nBack.paySuccess('gift')
            })
          }
        }).catch(errs => {
          reject(errs.err_desc || '取消支付')
        })
      } else {
        reject(res.data.msg || '获取支付参数异常')
      }
    }).catch(err => {
      reject(err || '获取支付参数异常')
    })
  })
}
// 新系统微信支付逻辑  绑定分销上下级
function getNewWxPayConfig(orderNo, prepaidPwdToken) {
  let params = { orderNo };
  if (prepaidPwdToken) {
    params['prepaidPwdToken'] = prepaidPwdToken;
  }
  return new Promise((resolve, reject) => {
    ProductApi.fetchOrderPayParams(params).then(res => {
      if (res.data.flag) {
        if (res.data.data.failType === 'DIRECT_SUCCESS') {
          // 积分或者纯储值卡支付,直接支付完成
          resolve(true);
        } else {
          let { timeStamp, nonceStr, package: packages, paySign, signType } = res.data.data.payParams;
          pay(timeStamp, nonceStr, packages, paySign, signType)
            .then(payRes => {
              resolve(payRes);
            })
            .catch(err => {
              reject(err || '支付失败');
            })
        }
      } else {
        reject(res.data.data || '获取支付参数异常');
      }
    })
  })
}

// 拉起微信支付
function pay(timeStamp, nonceStr, packages, paySign, signType = 'MD5') {
  return new Promise((resolve, reject) => {
    wx.requestPayment({
      'timeStamp': timeStamp,
      'nonceStr': nonceStr,
      'package': packages,
      'signType': signType,
      'paySign': paySign,
      'success': function (res) {
        resolve(res)
      },
      'fail': function (res) {
        reject(res)
      },
      'complete': function (res) {
        reject(res)
      }
    })
  })
}
module.exports = {
  getWxConfig: getWxConfig,
  pay: pay,
  getNewWxPayConfig: getNewWxPayConfig
};
