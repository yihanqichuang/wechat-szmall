/**
 * Created by yu tao on 2019/7/24.
 */
function getBytesLength(content) {
  var totalLength = 0;     
  var charCode;  
  for (var i = 0; i < content.length; i++) {  
    charCode = content.charCodeAt(i);  
    if (charCode < 0x007f) {
      totalLength++;     
    } else if ((charCode >= 0x0080) && (charCode <= 0x07ff)) {     
      totalLength += 2;     
    } else if ((charCode >= 0x0800) && (charCode <= 0xffff)) {     
      totalLength += 3;   
    } else {  
      totalLength += 4;   
    }          
  }
  console.log(`${content}长度：`, totalLength);
  return totalLength;
}

// 长图（商品，优惠券）
function langCard(that, data) {
  console.log('绘制分享海报：', data);
  // 元素到左边的距离
  const TITLETOP = 34;
  const LEFT = 30;
  const TOP = 126;
  const RECTBANNER = {
    width: 510,
    height: 510
  };
  that.setData({
    palette: {
      width: '570rpx',
      height: '866rpx',
      background: '',
      position: 'relative',
      borderRadius: '8rpx',
      views: [
        {
          type: 'image',
          url: data.memberHead,
          css: {
            top: `${TITLETOP}rpx`,
            left: `${LEFT}rpx`,
            width: `72rpx`,
            height: `72rpx`
          }
        },
        {
          type: 'text',
          text: data.memberNickName,
          css: {
            top: `${TITLETOP}rpx`,
            left: `${LEFT + 90}rpx`,
            fontSize: '26rpx',
            color: '#333',
            height: '36rpx',
            lineHeight: '36rpx'
          }
        },
        {
          type: 'text',
          text: '心水好物一起用',
          css: {
            top: `${TITLETOP + 36}rpx`,
            left: `${LEFT + 90}rpx`,
            fontSize: '20rpx',
            color: '#999999',
            height: '36rpx',
            lineHeight: '36rpx'
          }
        },
        {
          type: 'image',
          url: data.picturePath,
          css: {
            top: `${TOP}rpx`,
            left: `${LEFT}rpx`,
            width: `${RECTBANNER.width}rpx`,
            height: `${RECTBANNER.height}rpx`,
            borderRadius: `8rpx`
          }
        },
        {
          type: 'text',
          text: data.commodityName,
          css: {
            top: `${TOP + RECTBANNER.height + 22}rpx`,
            left: `${LEFT}rpx`,
            fontSize: '26rpx',
            color: '#333',
            width: '380rpx',
            height: '64rpx',
            lineHeight: '32rpx',
            maxLines: '2'
          }
        },
        {
          type: 'text',
          text: `${data.price}`,
          css: {
            top: `${TOP + RECTBANNER.height + 106}rpx`,
            left: `${LEFT}rpx`,
            fontSize: '36rpx',
            color: '#A32C41',
            width: '320rpx',
            maxLines: '1'
          }
        },
        {
          type: 'text',
          text: `${data.oldPrice ? data.oldPrice : ''}`,
          css: {
            top: `${TOP + RECTBANNER.height + 116}rpx`,
            left: `${LEFT + getBytesLength(data.price) * 16 + 8}rpx`,
            fontSize: '24rpx',
            color: '#777777',
            maxLines: '1',
            textDecoration: 'line-through'
          }
        },
        {
          type: 'image',
          url: data.qrcodeUrl,
          css: {
            top: `${TOP + RECTBANNER.height + 22}rpx`,
            right: `${LEFT}rpx`,
            width: `100rpx`,
            height: `100rpx`
          }
        },
        {
          type: 'text',
          text: '长按立即前往',
          css: {
            top: `${TOP + RECTBANNER.height + 132}rpx`,
            right: `${LEFT}rpx`,
            fontSize: '16rpx',
            color: '#777777',
            maxLines: '1'
          }
        },
        {
          type: 'text',
          text: ' ',
          css: {
            top: `${TOP + RECTBANNER.height + 168}rpx`,
            left: `${LEFT}rpx`,
            background: '#F5F5F5',
            color: '#F5F5F5',
            width: `${RECTBANNER.width}rpx`,
            height: '2rpx',
            fontSize: '2rpx'
          }
        },
        // {
        //   type: 'image',
        //   url: data.logoUrl,
        //   css: {
        //     top: `${TOP + RECTBANNER.height + 179}rpx`,
        //     left: `${LEFT}rpx`,
        //     width: `40rpx`,
        //     height: `40rpx`,
        //     borderRadius: `40rpx`
        //   }
        // },
        {
          type: 'text',
          text: data.organizeName,
          css: {
            left: `${LEFT}rpx`,
            top: `${TOP + RECTBANNER.height + 187}rpx`,
            fontSize: '24rpx',
            color: '#333',
            width: '360rpx',
            maxLines: '1'
          }
        }
      ]
    }
  });
}

// 礼品
function cardGift(that, data) {
  // 元素到左边的距离
  const TITLETOP = 34;
  const LEFT = 30;
  const TOP = 126;
  const RECTBANNER = {
    width: 510,
    height: 510
  };
  that.setData({
    palette: {
      width: '570rpx',
      height: '866rpx',
      background: '',
      position: 'relative',
      borderRadius: `8rpx`,
      views: [
        {
          type: 'image',
          url: data.memberHead,
          css: {
            top: `${TITLETOP}rpx`,
            left: `${LEFT}rpx`,
            width: `72rpx`,
            height: `72rpx`
          }
        },
        {
          type: 'text',
          text: data.memberNickName,
          css: {
            top: `${TITLETOP}rpx`,
            left: `${LEFT + 90}rpx`,
            fontSize: '26rpx',
            color: '#333',
            height: '36rpx',
            lineHeight: '36rpx'
          }
        },
        {
          type: 'text',
          text: '心水好物一起用',
          css: {
            top: `${TITLETOP + 36}rpx`,
            left: `${LEFT + 90}rpx`,
            fontSize: '20rpx',
            color: '#999999',
            height: '36rpx',
            lineHeight: '36rpx'
          }
        },
        {
          type: 'image',
          url: data.picturePath,
          css: {
            top: `${TOP}rpx`,
            left: `${LEFT}rpx`,
            width: `${RECTBANNER.width}rpx`,
            height: `${RECTBANNER.height}rpx`,
            borderRadius: `8rpx`
          }
        },
        {
          type: 'text',
          text: data.commodityName,
          css: {
            top: `${TOP + RECTBANNER.height + 22}rpx`,
            left: `${LEFT}rpx`,
            fontSize: '26rpx',
            color: '#333',
            width: '380rpx',
            height: '64rpx',
            lineHeight: '32rpx',
            maxLines: '2'
          }
        },
        {
          type: 'text',
          text: `惊喜价`,
          css: {
            top: `${TOP + RECTBANNER.height + 116}rpx`,
            left: `${LEFT}rpx`,
            fontSize: '20rpx',
            color: '#777777'
          }
        },
        {
          type: 'text',
          text: `${data.price}`,
          css: {
            top: `${TOP + RECTBANNER.height + 100}rpx`,
            left: `${LEFT + 70}rpx`,
            fontSize: '36rpx',
            color: '#A32C41',
            width: '300rpx',
            maxLines: '1'
          }
        },
        {
          type: 'image',
          url: data.qrcodeUrl,
          css: {
            top: `${TOP + RECTBANNER.height + 22}rpx`,
            right: `${LEFT}rpx`,
            width: `100rpx`,
            height: `100rpx`
          }
        },
        {
          type: 'text',
          text: '长按立即前往',
          css: {
            top: `${TOP + RECTBANNER.height + 132}rpx`,
            right: `${LEFT}rpx`,
            fontSize: '16rpx',
            color: '#777777',
            maxLines: '1'
          }
        },
        {
          type: 'text',
          text: ' ',
          css: {
            top: `${TOP + RECTBANNER.height + 168}rpx`,
            left: `${LEFT}rpx`,
            background: '#F5F5F5',
            color: '#F5F5F5',
            width: `${RECTBANNER.width}rpx`,
            height: '2rpx',
            fontSize: '2rpx'
          }
        },
        // {
        //   type: 'image',
        //   url: data.logoUrl,
        //   css: {
        //     top: `${TOP + RECTBANNER.height + 179}rpx`,
        //     left: `${LEFT}rpx`,
        //     width: `40rpx`,
        //     height: `40rpx`,
        //     borderRadius: `40rpx`
        //   }
        // },
        {
          type: 'text',
          text: data.organizeName,
          css: {
            left: `${LEFT}rpx`,
            top: `${TOP + RECTBANNER.height + 187}rpx`,
            fontSize: '24rpx',
            color: '#333',
            width: '360rpx',
            maxLines: '1'
          }
        }
      ]
    }
  });
}

// 活动报名
function card(that, data) {
  // 元素到左边的距离
  const TITLETOP = 34;
  const LEFT = 30;
  const TOP = 126;
  const RECTBANNER = {
    width: 510,
    height: 255
  };
  that.setData({
    palette: {
      width: '570rpx',
      height: '610rpx',
      background: '',
      position: 'relative',
      borderRadius: `8rpx`,
      views: [
        {
          type: 'image',
          url: data.memberHead,
          css: {
            top: `${TITLETOP}rpx`,
            left: `${LEFT}rpx`,
            width: `72rpx`,
            height: `72rpx`
          }
        },
        {
          type: 'text',
          text: data.memberNickName,
          css: {
            top: `${TITLETOP}rpx`,
            left: `${LEFT + 90}rpx`,
            fontSize: '26rpx',
            color: '#333',
            height: '36rpx',
            lineHeight: '36rpx'
          }
        },
        {
          type: 'text',
          text: '心水好物一起用',
          css: {
            top: `${TITLETOP + 36}rpx`,
            left: `${LEFT + 90}rpx`,
            fontSize: '20rpx',
            color: '#999999',
            height: '36rpx',
            lineHeight: '36rpx'
          }
        },
        {
          type: 'image',
          url: data.picturePath,
          css: {
            top: `${TOP}rpx`,
            left: `${LEFT}rpx`,
            width: `${RECTBANNER.width}rpx`,
            height: `${RECTBANNER.height}rpx`,
            borderRadius: `8rpx`
          }
        },
        {
          type: 'text',
          text: data.commodityName,
          css: {
            top: `${TOP + RECTBANNER.height + 22}rpx`,
            left: `${LEFT}rpx`,
            fontSize: '26rpx',
            color: '#333',
            width: '380rpx',
            height: '64rpx',
            lineHeight: '32rpx',
            maxLines: '2'
          }
        },
        {
          type: 'text',
          text: `报名费`,
          css: {
            top: `${TOP + RECTBANNER.height + 116}rpx`,
            left: `${LEFT}rpx`,
            fontSize: '20rpx',
            color: '#666'
          }
        },
        {
          type: 'text',
          text: `${data.price}`,
          css: {
            top: `${TOP + RECTBANNER.height + 100}rpx`,
            left: `${LEFT + 70}rpx`,
            fontSize: '36rpx',
            color: '#A32C41',
            width: '300rpx',
            maxLines: '1'
          }
        },
        {
          type: 'image',
          url: data.qrcodeUrl,
          css: {
            top: `${TOP + RECTBANNER.height + 22}rpx`,
            right: `${LEFT}rpx`,
            width: `100rpx`,
            height: `100rpx`
          }
        },
        {
          type: 'text',
          text: '长按立即前往',
          css: {
            top: `${TOP + RECTBANNER.height + 132}rpx`,
            right: `${LEFT}rpx`,
            fontSize: '16rpx',
            color: '#777777',
            maxLines: '1'
          }
        },
        {
          type: 'text',
          text: ' ',
          css: {
            top: `${TOP + RECTBANNER.height + 168}rpx`,
            left: `${LEFT}rpx`,
            background: '#F5F5F5',
            color: '#F5F5F5',
            width: `${RECTBANNER.width}rpx`,
            height: '2rpx',
            fontSize: '2rpx'
          }
        },
        // {
        //   type: 'image',
        //   url: data.logoUrl,
        //   css: {
        //     top: `${TOP + RECTBANNER.height + 179}rpx`,
        //     left: `${LEFT}rpx`,
        //     width: `40rpx`,
        //     height: `40rpx`,
        //     borderRadius: `40rpx`
        //   }
        // },
        {
          type: 'text',
          text: data.organizeName,
          css: {
            left: `${LEFT}rpx`,
            top: `${TOP + RECTBANNER.height + 187}rpx`,
            fontSize: '24rpx',
            color: '#333',
            width: '360rpx',
            maxLines: '1'
          }
        }
      ]
    }
  });
}

// 活动投票
function cardVote(that, data) {
  // 元素到左边的距离
  const TITLETOP = 34;
  const LEFT = 30;
  const TOP = 126;
  const RECTBANNER = {
    width: 510,
    height: 255
  };
  that.setData({
    palette: {
      width: '570rpx',
      height: '610rpx',
      background: '',
      position: 'relative',
      borderRadius: `8rpx`,
      views: [
        {
          type: 'image',
          url: data.memberHead,
          css: {
            top: `${TITLETOP}rpx`,
            left: `${LEFT}rpx`,
            width: `72rpx`,
            height: `72rpx`
          }
        },
        {
          type: 'text',
          text: data.memberNickName,
          css: {
            top: `${TITLETOP}rpx`,
            left: `${LEFT + 90}rpx`,
            fontSize: '26rpx',
            color: '#333',
            height: '36rpx',
            lineHeight: '36rpx'
          }
        },
        {
          type: 'text',
          text: '心水好物一起用',
          css: {
            top: `${TITLETOP + 36}rpx`,
            left: `${LEFT + 90}rpx`,
            fontSize: '20rpx',
            color: '#999999',
            height: '36rpx',
            lineHeight: '36rpx'
          }
        },
        {
          type: 'image',
          url: data.picturePath,
          css: {
            top: `${TOP}rpx`,
            left: `${LEFT}rpx`,
            width: `${RECTBANNER.width}rpx`,
            height: `${RECTBANNER.height}rpx`,
            borderRadius: `8rpx`
          }
        },
        {
          type: 'text',
          text: data.name,
          css: {
            top: `${TOP + RECTBANNER.height + 29}rpx`,
            left: `${LEFT}rpx`,
            fontSize: '26rpx',
            color: '#333',
            lineHeight: '36rpx',
            width: '380rpx',
            maxLines: '2'
          }
        },
        {
          type: 'image',
          url: '/images/icon/time.png',
          css: {
            top: `${TOP + RECTBANNER.height + 131}rpx`,
            left: `${LEFT}rpx`,
            width: `24rpx`,
            height: `24rpx`
          }
        },
        {
          type: 'text',
          text: `${data.time}`,
          css: {
            top: `${TOP + RECTBANNER.height + 131}rpx`,
            left: `${LEFT + 34}rpx`,
            fontSize: '20rpx',
            color: '#666',
            width: '280rpx',
            maxLines: '1'
          }
        },
        {
          type: 'image',
          url: data.qrcodeUrl,
          css: {
            top: `${TOP + RECTBANNER.height + 22}rpx`,
            right: `${LEFT}rpx`,
            width: `100rpx`,
            height: `100rpx`
          }
        },
        {
          type: 'text',
          text: '长按立即前往',
          css: {
            top: `${TOP + RECTBANNER.height + 132}rpx`,
            right: `${LEFT}rpx`,
            fontSize: '16rpx',
            color: '#777777',
            maxLines: '1'
          }
        },
        {
          type: 'text',
          text: ' ',
          css: {
            top: `${TOP + RECTBANNER.height + 168}rpx`,
            left: `${LEFT}rpx`,
            background: '#F5F5F5',
            color: '#F5F5F5',
            width: `${RECTBANNER.width}rpx`,
            height: '2rpx',
            fontSize: '2rpx'
          }
        },
        // {
        //   type: 'image',
        //   url: data.logoUrl,
        //   css: {
        //     top: `${TOP + RECTBANNER.height + 179}rpx`,
        //     left: `${LEFT}rpx`,
        //     width: `40rpx`,
        //     height: `40rpx`,
        //     borderRadius: `40rpx`
        //   }
        // },
        {
          type: 'text',
          text: data.organizeName,
          css: {
            left: `${LEFT}rpx`,
            top: `${TOP + RECTBANNER.height + 187}rpx`,
            fontSize: '24rpx',
            color: '#333',
            width: '360rpx',
            maxLines: '1'
          }
        }
      ]
    }
  });
}

// 活动投票
function cardVoteShare(that, data) {
  // 元素到左边的距离
  const TITLETOP = 34;
  const LEFT = 30;
  const TOP = 126;
  const RECTBANNER = {
    width: 510,
    height: 255
  };
  that.setData({
    palette: {
      width: '570rpx',
      height: '610rpx',
      background: '',
      position: 'relative',
      borderRadius: `8rpx`,
      views: [
        {
          type: 'image',
          url: data.memberHead,
          css: {
            top: `${TITLETOP}rpx`,
            left: `${LEFT}rpx`,
            width: `72rpx`,
            height: `72rpx`
          }
        },
        {
          type: 'text',
          text: data.memberNickName,
          css: {
            top: `${TITLETOP}rpx`,
            left: `${LEFT + 90}rpx`,
            fontSize: '26rpx',
            color: '#333',
            height: '36rpx',
            lineHeight: '36rpx'
          }
        },
        {
          type: 'text',
          text: '心水好物一起用',
          css: {
            top: `${TITLETOP + 36}rpx`,
            left: `${LEFT + 90}rpx`,
            fontSize: '20rpx',
            color: '#999999',
            height: '36rpx',
            lineHeight: '36rpx'
          }
        },
        {
          type: 'image',
          url: data.picturePath,
          css: {
            top: `${TOP}rpx`,
            left: `${LEFT}rpx`,
            width: `${RECTBANNER.width}rpx`,
            height: `${RECTBANNER.height}rpx`,
            borderRadius: `8rpx`
          }
        },
        {
          type: 'text',
          text: data.title,
          css: {
            top: `${TOP + RECTBANNER.height + 29}rpx`,
            left: `${LEFT}rpx`,
            fontSize: '26rpx',
            color: '#666',
            width: '380rpx',
            maxLines: '1'
          }
        },
        {
          type: 'text',
          text: data.name,
          css: {
            top: `${TOP + RECTBANNER.height + 68}rpx`,
            left: `${LEFT}rpx`,
            fontSize: '26rpx',
            color: '#333',
            width: '380rpx',
            maxLines: '1'
          }
        },
        {
          type: 'image',
          url: '/images/icon/time.png',
          css: {
            top: `${TOP + RECTBANNER.height + 131}rpx`,
            left: `${LEFT}rpx`,
            width: `24rpx`,
            height: `24rpx`
          }
        },
        {
          type: 'text',
          text: `${data.time}`,
          css: {
            top: `${TOP + RECTBANNER.height + 131}rpx`,
            left: `${LEFT + 34}rpx`,
            fontSize: '20rpx',
            color: '#666',
            width: '280rpx',
            maxLines: '1'
          }
        },
        {
          type: 'image',
          url: data.qrcodeUrl,
          css: {
            top: `${TOP + RECTBANNER.height + 22}rpx`,
            right: `${LEFT}rpx`,
            width: `100rpx`,
            height: `100rpx`
          }
        },
        {
          type: 'text',
          text: '长按立即前往',
          css: {
            top: `${TOP + RECTBANNER.height + 132}rpx`,
            right: `${LEFT}rpx`,
            fontSize: '16rpx',
            color: '#777777',
            maxLines: '1'
          }
        },
        {
          type: 'text',
          text: ' ',
          css: {
            top: `${TOP + RECTBANNER.height + 168}rpx`,
            left: `${LEFT}rpx`,
            background: '#F5F5F5',
            color: '#F5F5F5',
            width: `${RECTBANNER.width}rpx`,
            height: '2rpx',
            fontSize: '2rpx'
          }
        },
        // {
        //   type: 'image',
        //   url: data.logoUrl,
        //   css: {
        //     top: `${TOP + RECTBANNER.height + 179}rpx`,
        //     left: `${LEFT}rpx`,
        //     width: `40rpx`,
        //     height: `40rpx`,
        //     borderRadius: `40rpx`
        //   }
        // },
        {
          type: 'text',
          text: data.organizeName,
          css: {
            left: `${LEFT}rpx`,
            top: `${TOP + RECTBANNER.height + 187}rpx`,
            fontSize: '24rpx',
            color: '#333',
            width: '360rpx',
            maxLines: '1'
          }
        }
      ]
    }
  });
}

// 邀请有礼
function invite(that, data) {
  // 元素到左边的距离
  const TITLETOP = 24;
  const LEFT = 30;
  const TOP = 126;
  const RECTBANNER = {
    width: 510,
    height: 255
  };
  that.setData({
    palette: {
      width: '570rpx',
      height: '650rpx',
      background: '',
      position: 'relative',
      borderRadius: `8rpx`,
      views: [
        {
          type: 'image',
          url: data.memberHead,
          css: {
            top: `${TITLETOP}rpx`,
            left: `${LEFT}rpx`,
            width: `72rpx`,
            height: `72rpx`
          }
        },
        {
          type: 'text',
          text: data.memberNickName,
          css: {
            top: `${TITLETOP + 24}rpx`,
            left: `${LEFT + 90}rpx`,
            fontSize: '26rpx',
            color: '#333',
            height: '36rpx',
            lineHeight: '36rpx'
          }
        },
        {
          type: 'text',
          text: data.commodityName,
          css: {
            top: `${TITLETOP + 104}rpx`,
            left: `${LEFT}rpx`,
            fontSize: '26rpx',
            color: '#333',
            width: '380rpx',
            height: '64rpx',
            lineHeight: '32rpx',
            maxLines: '2'
          }
        },
        {
          type: 'image',
          url: data.picturePath,
          css: {
            top: `${TOP + 50}rpx`,
            left: `${LEFT}rpx`,
            width: `${RECTBANNER.width}rpx`,
            height: `${RECTBANNER.height}rpx`,
          }
        },
        {
          type: 'image',
          url: data.qrcodeUrl,
          css: {
            top: `${TOP + RECTBANNER.height + 62}rpx`,
            right: `${LEFT}rpx`,
            width: `100rpx`,
            height: `100rpx`
          }
        },
        {
          type: 'text',
          text: '长按立即前往',
          css: {
            top: `${TOP + RECTBANNER.height + 172}rpx`,
            right: `${LEFT}rpx`,
            fontSize: '16rpx',
            color: '#777777',
            maxLines: '1'
          }
        },
        {
          type: 'text',
          text: ' ',
          css: {
            top: `${TOP + RECTBANNER.height + 208}rpx`,
            left: `${LEFT}rpx`,
            background: '#F5F5F5',
            color: '#F5F5F5',
            width: `${RECTBANNER.width}rpx`,
            height: '2rpx',
            fontSize: '2rpx'
          }
        },
        // {
        //   type: 'image',
        //   url: data.logoUrl,
        //   css: {
        //     top: `${TOP + RECTBANNER.height + 219}rpx`,
        //     left: `${LEFT}rpx`,
        //     width: `40rpx`,
        //     height: `40rpx`,
        //     borderRadius: `40rpx`
        //   }
        // },
        {
          type: 'text',
          text: data.organizeName,
          css: {
            left: `${LEFT}rpx`,
            top: `${TOP + RECTBANNER.height + 227}rpx`,
            fontSize: '24rpx',
            color: '#333',
            width: '360rpx',
            maxLines: '1'
          }
        }
      ]
    }
  });
}

module.exports = {
  langCard,
  cardGift,
  card,
  cardVote,
  cardVoteShare,
  invite
};
