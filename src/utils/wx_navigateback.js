/**
 * Created by AklanFun on 2018/5/7.
 */
import tip from './tip'

function setNavigateBackData(data = {}, n = 1) {
  let pages = getCurrentPages()
  let targetPage = pages[pages.length - 1 - n]
  targetPage.setData({
    navigateBackData: data
  })
  wx.navigateBack({ delta: n })
}
function getNavigateBackData() {
  let pages = getCurrentPages()
  let targetPage = pages[pages.length - 1]
  return targetPage.data.navigateBackData
}
function clearNavigateBackData() {
  let pages = getCurrentPages()
  let targetPage = pages[pages.length - 1]
  targetPage.setData({
    navigateBackData: null
  })
}
function paySuccess(type) {
  clearNavigateBackData()
  let title, content, url
  if (type === 'special' || type === 'secKill') {
    wx.setStorageSync('myOrderList', 'myOrderList')
    title = '购买成功'
    content = '商品已放置于‘我的-订单’'
    url = '/pages/orderList/orderList'
  } else if (type === 'group') {
    wx.setStorageSync('myOrderList', 'myOrderList')
    title = '参团成功'
    content = '您可到‘我的-订单’中查看拼团状态'
    url = '/pages/orderList/orderList'
  } else if (type === 'activity') {
    wx.setStorageSync('activityList', 'activityList')
    title = '报名成功'
    content = '活动入场码已放置于‘我的-活动’，记得到场参加活动哦!'
    url = '/pages/activityList/activityList'
  } else if (type === 'gift') {
    wx.setStorageSync('exchangeList', 'exchangeList');
    title = '购买成功'
    content = '商品已放置与“我的-兑换” ，记得到店核销兑换哟～'
    url = '/pages/exchangeList/exchangeList?isFirst=1'
  }
  tip.confirm(content, {}, title, '取消', '去查看').then(() => {
    wx.navigateTo({ url })
  }).catch(() => {})
}
module.exports = {
  setNavigateBackData,
  getNavigateBackData,
  paySuccess,
  clearNavigateBackData
}

