/*
 * Created by yu tao on 2019/5/27.
 * 防抖函方法，不同的场景选择不同的方法
 */

/*
 * 防抖函数，非立即执行；
 * 使用场景：按钮防止多次点击...
 * @param func 需要包装的方法
 * @param wait 需要等待的时间
 * @returns {Function}
 */
function debounceDelay(func, wait = 500) {
  let timeout;
  return function() {
    let context = this;
    let args = arguments;
    if (timeout) clearTimeout(timeout); // 清楚定时器
    timeout = setTimeout(() => {
      func.apply(context, args)
    }, wait)
  }
}

/*
 * 防抖函数，立即执行一次，后面会在执行一次
 * @param func 需要包装的方法
 * @param wait 需要等待的时间
 * @returns {Function}
 */
function debounceNotDelay(func, wait) {
  let timeout;
  return function() {
    let context = this;
    let args = arguments;
    if (timeout) clearTimeout(timeout); // 清楚定时器
    let callNow = !timeout;
    timeout = setTimeout(() => {
      timeout = null;
    }, wait);
    if (callNow) func.apply(context, args)
  }
}

/*
 * 防抖函数
 * @param func 需要包装的方法
 * @param wait 需要等待的时间
 * @param immediate true为立即执行一次，false则为延迟执行
 * @returns {Function}
 */
function debounce(func, wait = 500, immediate = false) {
  let timeout;
  return function() {
    let context = this;
    let args = arguments;
    if (timeout) clearTimeout(timeout); // 清楚定时器
    if (immediate) {
      let callNow = !timeout;
      timeout = setTimeout(() => {
        timeout = null;
      }, wait);
      if (callNow) func.apply(context, args)
    } else {
      timeout = setTimeout(() => {
        func.apply(context, args)
      }, wait)
    }
  }
}

module.exports = {
  debounceDelay,
  debounceNotDelay,
  debounce
};
