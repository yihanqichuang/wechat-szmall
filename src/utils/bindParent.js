import { jdFetch } from '/fetch.js';
function bindParent(parent_id,condition) {
    console.log('parent_id',parent_id,condition)
    if ("undefined" != parent_id && 0 != parent_id) {
        if (0 < wx.getStorageSync('SHARE_SETTING').level) {
            jdFetch({
                url:'share/bind-parent',
                data: {
                    parent_id: parent_id,
                    condition: condition,
                    user_id:wx.getStorageSync("jdShareInfo").user_id
                }
            }).then(res => {
                console.log("绑定成功:",res);
            }).catch(err => {
                console.log("绑定失败:",err);
            })
        }
    }
}

module.exports = {
    bindParent
};