/*
 * @Author: your name
 * @Date: 2020-10-10 14:23:29
 * @LastEditTime: 2020-10-23 11:37:07
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: /WangMao-yidong/wxml-nbss/src/utils/timeOut.js
 */
// util.js
 
// 取倒计时（天时分秒）
function getTimeLeft(datetime) {
  // 计算目标与现在时间差（毫秒）
  let time1 = new Date().getTime();
  let time2 = new Date(datetime).getTime();
  let mss = time2 - time1;
  // 将时间差（毫秒）格式为：天时分秒
  let days = parseInt(mss / (1000 * 60 * 60 * 24));
  let hours = parseInt((mss % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
  let minutes = parseInt((mss % (1000 * 60 * 60)) / (1000 * 60));
  let seconds = parseInt((mss % (1000 * 60)) / 1000);
  // console.log(days)
  if (days > 0) return  days + '天' + hours + ':' + minutes + ':' + seconds
  else return  hours + ':' + minutes + ':' + seconds
}
 // 取活动开始时间
function getTime(tiem) {
  // let time = tiem / 1000
  // console.log(tiem)
  // 计算目标与现在时间差（毫秒）
  // let time1 = new Date(tiem).getTime();
  // 将时间差（毫秒）格式为：天时分秒
  let days = parseInt(tiem / (1000 * 60 * 60 * 24));
  let hours = parseInt((tiem % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60) + 8);
  let minutes = parseInt((tiem % (1000 * 60 * 60)) / (1000 * 60));
  // let seconds = (tiem % (1000 * 60)) / 1000;
  if (minutes == '0') minutes = '00'
  if (days) return  [hours + ':' + minutes, '次日']
  else return   [hours + ':' + minutes]
}
 // 取倒计时（天时分秒）
 function getOver(datetimeTo) {
  // let time = datetimeTo / 1000
  // 计算目标与现在时间差（毫秒）
  let time1 = new Date(datetimeTo).getTime();
  let time2 = new Date().getTime();
  console.log(time1)
  console.log(time2)
  let mss = time1 - time2;
  return  mss
}
module.exports = {
  getTimeLeft: getTimeLeft,
  getTime: getTime,
  getOver: getOver
}