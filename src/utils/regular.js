/* eslint-disable */
export const WHOLENUMBER = /^\d+$/;    // 整数
export const PRICE = /(?!^0*(\.0{1,2})?$)^\d{1,13}(\.\d{1,2})?$/; // 价格
export const PRICE2 = /(^[1-9]([0-9]+)?(\.[0-9]{1,2})?$)|(^(0){1}$)|(^[0-9]\.[0-9]([0-9])?$)/; // 价格
export const PRICE3 = /(?!^0*(\.0{1,2})?$)^\d{1,13}(\.\d{1,2})?$/; // 价格
export const EMAIL = /^\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/; // 邮箱
export const PHONE = /^13[0-9]\d{8}$|^14[0-9]\d{8}$|^15[0-9]\d{8}$|^16[0-9]\d{8}$|^17[0-9]\d{8}$|^18[\d]{9}$|^19[0-9]\d{8}$/; // 手机号码
export const IDCARD = /^(^[1-9]\d{7}((0\d)|(1[0-2]))(([0|1|2]\d)|3[0-1])\d{3}$)|(^[1-9]\d{5}[1-9]\d{3}((0\d)|(1[0-2]))(([0|1|2]\d)|3[0-1])((\d{4})|\d{3}[Xx])$)$/; // 身份证
export const BANKCARD = /^(\d{16}|\d{19})$/; // 银行卡
export const IDENTIFYINGCODE = /^\d{6}$/; // 6位验证码
export const ZHENGSHU = /^[1-9]\d*$/; // 非0整
export const AGE = /^(\d\d{0,1}|100)$/; // 0-100年龄
export const CARNUMBER = /^[A-Za-z]{1}[A-Z0-9a-z]{4,5}[A-Z0-9a-z挂学警港澳]{1}$/; // 车牌
export const CARNUMBER2 = /^[京津沪渝冀豫云辽黑湘皖鲁新苏浙赣鄂桂甘晋蒙陕吉闽贵粤青藏川宁琼使领A-Z]{1}[A-Za-z]{1}[A-Z0-9a-z]{4,5}[A-Z0-9a-z挂学警港澳]{1}$/; // 车牌


