/* eslint-disable */
/**
 * 时间处理
 */
export default class timeFormat {
  constructor() {
  }

  /**
   * 日期格式
   *
   * @param format { date } 需要传入的格式
   * @param value: 时间戳
   *
   * format格式如下:
   *             1.'yyyy-MM-dd hh:mm:ss'
   *             2.'yyyy年MM月dd日'
   *             3.'MM/dd/yyyy'
   *             4.'yyyyMMdd'
   *             5.'hh:mm:ss'
   */

  static dateFormatFilter(value, format) {
    if (typeof value == 'string') value = Number(value);
    var date = new Date(value);
    var o = {
      'M+': date.getMonth() + 1, //month
      'd+': date.getDate(), //day
      'h+': date.getHours(), //hour
      'm+': date.getMinutes(), //minute
      's+': date.getSeconds(), //second
      'q+': Math.floor((date.getMonth() + 3) / 3), //quarter
      'S': date.getMilliseconds() //millisecond
    }
    if (/(y+)/.test(format)) {
      format = format.replace(RegExp.$1, (date.getFullYear() + '').substr(4 - RegExp.$1.length));
    }
    for (var k in o) {
      if (new RegExp('(' + k + ')').test(format)) {
        format = format.replace(RegExp.$1, RegExp.$1.length == 1 ? o[k] : ('00' + o[k]).substr(('' + o[k]).length));
      }
    }
    return format;
  }

  // 时间转时间戳
  static timestampFilter(value) {
    let time = value.replace(/-/g, '/');
    return Date.parse(time)
  }

  // 转时间
  static timeStr(value, format) {
    let res = this.timestampFilter(value);
    let ret = this.dateFormatFilter(res, format);
    return ret
  }

  // 转时间
  static timeToDay(time) {
    if (!time) return ''
    return time.substring(0, 10)
  }

  /**
   * 时间秒数格式化
   * @param s 时间戳（单位：秒）
   * @returns {*} 格式化后的时分秒
   */
  static secToTime(s) {
    let t;
    if (s > -1) {
      let hour = Math.floor(s / 3600);
      let min = Math.floor(s / 60) % 60;
      let sec = s % 60;
      if (hour < 10) {
        t = '0' + hour + ':';
      } else {
        t = hour + '小时';
      }
      if (min < 10) {
        t += '0';
      }
      t += min + '分';
      if (sec < 10) {
        t += '0';
      }
      t += sec + '秒';
    }
    return t;
  }

  // 时间显示
  static showtime(that) {
    // 格式化月份
    function dateformat(t) {
      //三目运算
      return t >= 10 ? t : '0' + t;
    }
    let d = new Date();
    // 因为默认month是从0-11的
    let t = d.getFullYear() + '.' + dateformat((d.getMonth() + 1)) + '.' + dateformat(d.getDate()) + ' ' + dateformat(d.getHours()) + ':' + dateformat(d.getMinutes()) + ':' + dateformat(d.getSeconds());
    that.setData({ showtime: t })
  }
}

