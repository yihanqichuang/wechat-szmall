import BenMath from './BenMath.js';
// 银行卡4位空格
const codeFormat = (value) => {
  return value.replace(/\s/g, '').replace(/(\d{4})(?=\d)/g, '$1 ');
};

// 根据会员卡等级 改变优惠券面值
const cy_0_CouponValue = (points, grade) => {
  switch (grade) {
    // 普通卡
    case 3:
      return BenMath.accMul(points, 0.015);
    // 三星卡
    case 2:
      return BenMath.accMul(points, 0.02);
    // 五星卡
    case 1:
      return BenMath.accMul(points, 0.03);
    default:
      return BenMath.accMul(points, 0.015);
  }
};

const cy_exchangePrice = (grade) => {
  switch (grade) {
    // 普通卡
    case 3:
      return 15;
    // 三星卡
    case 2:
      return 20;
    // 五星卡
    case 1:
      return 30;
    default:
      return 15;
  }
};

module.exports = {
  codeFormat,
  cy_0_CouponValue,
  cy_exchangePrice
};
