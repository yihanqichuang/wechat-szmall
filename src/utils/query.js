/**
 * Created by yu tao on 2019/7/20.
 */

/**
 * 获取url上的参数
 * @param name       要获取的参数的key值
 * @param url        url路径
 * @returns {null}   参数value值
 * example  url: id=18&shopId=1  getQueryString('id', url) 返回18
 */
function getQueryString(name, url) {
  let reg = new RegExp('(^|&)' + name + '=([^&]*)(&|$)', 'i');
  let r = null;
  if (!url) {
    return null;
  } else {
    r = url.match(reg);
    if (r != null) {
      return unescape(r[2]);
    } else {
      return null;
    }
  }
}

module.exports = {
  getQueryString
};
