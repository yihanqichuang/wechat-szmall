Component({
  properties: {},
  data: {
    isShow: false,
    organizeList: [],// 可选门店列表
    selectedOrganize: null,// 选中的门店信息
  },
  lifetimes: {
    attached() {
    }
  },
  methods: {
    // 关闭送达时间解释说明弹框
    closeDesDialog() {
      this.setData({
        showDes: false
      })
    },
    // 打开picker
    showPicker(organizeList,selectedOrganize) {
      this.setData({
        isShow: true,
        organizeList: organizeList,
        selectedOrganize: selectedOrganize
      });
    },
    // 关闭picker
    hidePicker() {
      this.setData({ isShow: false });
    },
    // 选中门店
    clickOrganize(e) {
      console.log("选中1",e.currentTarget.dataset)
      this.setData({
        selectedOrganize: e.currentTarget.dataset.organize.organizeId
      })
      this.triggerEvent('setOrganizeInfo', e.currentTarget.dataset.organize)
    }
  }
});