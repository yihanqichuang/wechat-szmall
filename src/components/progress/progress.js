/**
 * 页面到底组件
 * @message     String  message文字提示
 */
Component({
  properties: {
    percent: {
      type: Number
    },
    width: {
      type: Number,
      value: 230
    },
    number: {
      type: Number,
      value: 0
    }
  },
  data: {
  
  },
  methods: {
  
  }
})
