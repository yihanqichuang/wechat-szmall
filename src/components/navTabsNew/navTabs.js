Component({
  externalClasses: ['nav-tabs', 'flex-grows', 'sliders', 'actives'],
  properties: {
    tabs: {
      type: Array
    },
    activeIndex: {
      type: Number,
      value: 0
    },
    slider: {
      type: Object,
      value: {
        width: '40rpx',
        left: '0'
      }
    }
  },
  data: {},
  methods: {
    // 切换 tab
    tap(event) {
      let index = event.currentTarget.dataset.index;
      if (index === this.properties.activeIndex) return;
      this.setData({
        activeIndex: index
      })
      this.triggerEvent('changeIndex', index)
    }
  }
});
