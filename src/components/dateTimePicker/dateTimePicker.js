/**
 * 时间选择组件
 *
 */
import timeFormat from '../../utils/timeFormat'
Component({
  externalClasses: ['my-class'],
  properties: {
    date: {
      type: String
    },
    disabled: {
      type: Boolean
    }
  },
  data: {
    pickerRange: [[1, 2], [1, 2, 3], [0, 1, 2, 3]],
    pickerValue: [0, 0, 0],
    selectedDate: ''
  },
  methods: {
    setPickerRange() {
      let pickerRange = [[], [], []]
      let date = timeFormat.dateFormatFilter(new Date(), 'yyyy-MM-dd hh:mm:ss')
      let yms = date.split(' ')[0], hms = date.split(' ')[1]
      console.log(yms, hms)
      let h = hms.split(':')[0], hourRange = []
      hourRange = h < 10 ? [10, 12] : h < 14 ? [10, 15] : h < 16 ? [14, 18] : h < 21 ? [16, 22] : [10, 12]
      h >= 21 && pickerRange[0].push({ label: '明天', value: timeFormat.dateFormatFilter(new Date().getTime() + 8.64e7, 'yyyy-MM-dd') }) || pickerRange[0].push({ label: '今天', value: timeFormat.dateFormatFilter(new Date(), 'yyyy-MM-dd') })
      for (let i = hourRange[0]; i <= hourRange[1]; i++) {
        pickerRange[1].push({ label: i, value: this.addZero(i) })
      }
      for (let i = 0; i < 60; i++) {
        pickerRange[2].push({ label: i, value: this.addZero(i) })
      }
      this.setData({ pickerRange: pickerRange })
    },
    pickerChange(e) {
      console.log('picker发送选择改变，携带值为', e.detail.value)
      let value = e.detail.value
      let { pickerRange } = this.data
      let selectedDate = `${pickerRange[0][value[0]].value} ${pickerRange[1][value[1]].value}:${pickerRange[2][value[2]].value}:00`
      this.setData({ selectedDate })
      this.triggerEvent('change', selectedDate)
    },
    columnChange(e) {
      console.log('修改的列为', e.detail.column, '，值为', e.detail.value)
      let data = {
        pickerRange: this.data.pickerRange,
        pickerValue: this.data.pickerValue
      }
      if (e.detail.column === 1 && e.detail.value === data.pickerRange[1].length - 1) {
        data.pickerValue[e.detail.column] = e.detail.value
        data.pickerValue[2] = 0
      } else if (e.detail.column === 2 && data.pickerValue[1] === data.pickerRange[1].length - 1) {
        data.pickerValue[2] = 0
      } else {
        data.pickerValue[e.detail.column] = e.detail.value
      }
      this.setData({ pickerValue: data.pickerValue })
    },
    cancel() {
      console.log('cancel pick dataTime')
    },
    // 小时或分钟 一位数字加0
    addZero(n) {
      return new Array(3 - n.toString().length).join('0') + n
    }
  },
  attached() {
    this.setPickerRange()
  }
})
