/**
 * 活动报名 item
 */
Component({
  properties: {
    item: {
      type: Object
    },
    marketingActivitySignupId: {
      type: Number || String
    },

    // 活动名称
    name: {
      type: String
    },

    // 开始时间
    startTime: {
      type: String
    },

    // 结束时间
    endTime: {
      type: String
    },

    // 积分
    score: {
      type: Number || String
    },

    // 金额
    value: {
      type: Number || String
    },

    activityPicture: {
      type: String
    },

    noPadding: {
      type: Boolean,
      value: false
    }
  },
  data: {},
  methods: {}
})
