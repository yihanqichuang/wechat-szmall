/*
 * @Author: your name
 * @Date: 2020-09-21 09:54:28
 * @LastEditTime: 2020-10-21 16:45:17
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: /WangMao-yidong/wxml-nbss/src/components/paginationNew/pagination.js
 */
/**
 * 分页组件
 */
Component({
  properties: {
    page: {
      type: Object,
      value: {
        pageNo: 1,
        pageSize: 10,
        pages: 1,
        total: 0
      },
      observer: function(val) {
        console.log(val)
        if (val.total <= 0) {
          this.setData({
            emptyPage: true,
            bottomTipsFlag: true,
            loadMore: false
          })
        } else if (val.pageNo === val.pages && val.pageNo) {
          this.setData({
            emptyPage: false,
            bottomTipsFlag: false,
            bottomTipsText: '没有更多了~',
            loadMore: false
          })
        } else if (!val.pages || val.pageNo < val.pages) {
          this.setData({
            emptyPage: false,
            bottomTipsFlag: true,
            bottomTipsText: '上拉加载更多...',
            loadMore: true
          })
        } else {
          this.setData({
            emptyPage: false,
            bottomTipsFlag: true,
            bottomTipsText: '上拉加载更多...',
            loadMore: true
          })
        }
        this.setData({
          pending: false
        })
      }
    },
    scroll: {
      type: Boolean,
      value: true
    },
    emptyType: {
      type: String,
      value: ''
    },
    customClass: {
      type: String
    }
  },
  data: {
    emptyPage: false,
    bottomTipsFlag: true,
    bottomTipsText: '',
    loadMore: true,
    pending: false
  },
  methods: {

    // 滚动到底部触发
    scrolltolower() {
      console.log(this.properties.page, this.data.pending, this.data.loadMore)
      if (this.data.pending || !this.data.loadMore) return
      this.setData({
        pending: true
      })
      if (!this.data.loadMore) return
      let pageNo = this.properties.page.pageNo + 1
      this.triggerEvent('loadmore', pageNo)
    },

    // 滚动
    scroll(e) {
      this.triggerEvent('scroll', { scrollTop: e.detail.scrollTop })
    },

    // 滚动到顶部触发
    scrolltoupper() {
      console.log('下拉');
    }
  }
})
