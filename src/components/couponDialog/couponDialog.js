/**
 * Created by yu tao on 2019/7/10.
 */

Component({
  properties: {
    showCenterDialog: {
      type: Boolean,
      value: false
    },
    useCouponList: {
      type: Array,
      value: []
    },
    memberCouponId: {
      type: Number,
      value: 0
    },
    discountFee: {
      type: Number,
      value: 0
    }
  },
  observers: {
    memberCouponId: function() {
      this.setData({
        selectCouponId: this.properties.memberCouponId
      });
    },
    discountFee: function() {
      this.setData({
        discountsAmt: this.properties.discountFee
      });
    },
    useCouponList: function() {
      let couponList = this.properties.useCouponList;
      couponList.forEach(item => {
        item.startTime = item.startTime.substring(0, 16);
        item.endTime = item.endTime.substring(0, 16);
      })
      this.setData({
        couponList: couponList
      });
    }
  },
  data: {
    messageComponent: '',
    selectCouponId: '',
    discountsAmt: 0,
    couponList: []
  },
  lifetimes: {
    attached() {
      this.messageComponent = this.selectComponent('.messageTips');
    }
  },
  pageLifetimes: {
    hide() {
      this.setData({
        skuName: '请选择规格'
      })
    }
  },
  methods: {
    // 点击优惠券事件
    tapCoupon(e) {
      let item = e.currentTarget.dataset.item;
      if (!item.usableFlag) { // 不可使用券过滤
        return;
      }
      this.setData({ 
        unSubmitCouponId: this.data.selectCouponId, 
        unSubmitDiscountAmt: this.data.discountsAmt,
        selectCouponId: item.memberCouponId == this.data.selectCouponId ? 0 : item.memberCouponId,
        couponTypeKind: item.memberCouponId == this.data.selectCouponId ? '' : item.couponTypeKind,
        couponValue: item.memberCouponId == this.data.selectCouponId ? 0 : item.couponValue,
        discountsAmt: item.memberCouponId == this.data.selectCouponId ? 0 : item.discountsAmt
      });
    },
    // 提交事件
    handleSubmit() {
      this.triggerEvent('submitCoupon', {
        memberCouponId: this.data.selectCouponId,
        couponTypeKind: this.data.couponTypeKind,
        couponValue: this.data.couponValue,
        discountsAmt: this.data.discountsAmt
      });
    },
    // 关闭事件
    handleColse() {
      this.setData({ 
        selectCouponId: this.data.unSubmitCouponId ? this.data.unSubmitCouponId : this.data.selectCouponId,
        discountsAmt: this.data.unSubmitDiscountAmt ? this.data.unSubmitDiscountAmt : this.data.discountsAmt
      })
      this.triggerEvent('closeCoupon', {});
    }
  }
});
