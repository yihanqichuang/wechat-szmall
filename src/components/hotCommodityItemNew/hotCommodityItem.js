import ProductApi from '../../api/commodity';
import Tip from '../../utils/tip';
import ShopApi from '../../api/findshop';
import CartApi from '../../api/shoppingcart';
/**
 * 秒杀列表 item
 */
Component({
  properties: {
    item: {
      type: Object,
      default: {}
    }
  },
  observers: {
    item: function (nVal, oVal) {
      console.log('nValnVal', nVal)
      let spu = {};
      if (nVal.commodityId) {
        // 老接口返回的商品数据，改造成新的商品数据结构
        if (nVal.picturePath) {
          if (nVal.picturePath.indexOf(',') > 0) {
            spu['productPic'] = nVal.picturePath.split(',')[0];
          } else {
            spu['productPic'] = nVal.picturePath;
          }
        }
        spu['productName'] = nVal.commodityName;
        spu['minSalePrice'] = nVal.nowPrice;
        spu['minPoints'] = nVal.points;
        spu['minLinePrice'] = nVal.oldPrice;
        spu['stock'] = nVal.commodityNum;
        spu['productCode'] = nVal.productCode;
        spu['marketState'] = nVal.marketState;
        spu['marketType'] = nVal.marketType;
      } else {
        spu = nVal;
      }
      this.setData({ spu });
    }
  },
  data: {
    spu: null,
    defaultItems: [{
        name: '到店自提',
        value: 'PICK_UP',
        disabled: true,
        checked: false
      },
      {
        name: '快递配送',
        value: 'EXPRESS',
        disabled: true,
        checked: false
      },
      {
        name: '同城配送',
        value: 'CITY_EXPRESS',
        disabled: true,
        checked: false
      }
    ],
    items: [],
    attributeList: [],
    selectedSku: ''
  },
  methods: {
    navigateToDetail() {
      wx.navigateTo({
        url: `/pages/newProductDetail/newProductDetail?productCode=${this.data.spu.productCode}`
      })
    },
    async addCart() {
       Tip.loading();
       // 查询商品sku列表默认选着第一个
       // 获取商品详情
      const res = await ProductApi.fetchCommodityDetail({
       productCode: this.data.spu.productCode
      });
      console.log('商品详情', res)
      // 初始化配送方式
      let dispatchItems = this.data.defaultItems;
      let useItems = [];
      res.data.dispatchTypeList.forEach(type => {
        let item = dispatchItems.find(ele => ele.value === type.dispatchType);
        item.disabled = false;
        if (res.data.dispatchTypeList.length <= 1) {
          item.checked = true;
        }
        useItems.unshift(item)
      });
      if (res.flag) {
        // 预售商品不允许加入购物车
        if (res.data.marketType === 'PRESALE') {
            Tip.loaded();
            Tip.toast('预售商品无法加入购物车', null, 'none')
            return;
        }

        // 预售商品不允许加入购物车
        if (res.data.marketType === 'GROUP'){
          Tip.loaded();
          Tip.toast("团购商品无法加入购物车", null, 'none')
          return;
        }

         const selectedSku = this.handleSkuListToAttribute(res.data,res.data.skuExtList);
         let selectedDispatchWay = useItems[0];
         // 超市商品从 确认订单 选择配送方式
         if (res.data.organizeType === 'SuperMarket') {
          // 超市商品默认同城配送(处理添加购物车时过校验)
          selectedDispatchWay = {
            name: '同城配送',
            value: 'CITY_EXPRESS'
          }
        } 
        if (res.data.organizeType === 'SuperMarket') {
          // 判断该商品是否只能自提，只能自提，则不能加入购物车
          let findDispatchTypePickUp =  res.data.dispatchTypeList.find(item => item.dispatchType == 'PICK_UP')
          if (findDispatchTypePickUp && res.data.dispatchTypeList.length == 1) {
              Tip.loaded();
              Tip.toast('商品只允许自提，无法加入购物车', null, 'none')
              return;
          } else {
            wx.setStorageSync('isPickUp', false);// 在createMarkOrder页面用于判断该商品是否只能自提
          }
        }
        
        const shopDataRes = await ShopApi.fetchShopData({
          shopId: res.data.shopId
        });
        let shopData = {};
        if (shopDataRes.data.flag) {
          shopData = shopDataRes.data.shop;
        }

        console.log('获取商铺详情：', shopData, selectedDispatchWay);

        const selectedSkuData = {
          origin: 'detail',
          shopId: res.data.shopId,
          shopName: shopData.shopName,
          shopLogo: shopData.shopLogo,
          shopPhone: shopData.shopPhone,
          dispatchType: selectedDispatchWay,
          skuCount: 1,
          skuInfo: selectedSku
        };
        console.log('选择的商品11：', selectedSkuData);
        // if (res.data.organizeType === 'SuperMarket') {
        //   selectedSkuData.shopId = res.data.cabinetGroupId
        //   selectedSkuData.shopName = res.data.cabinetGroupName
        // }
        wx.setStorageSync('createOrderSkus', selectedSkuData);
        // 加入购物车
        this.addToCart(selectedSkuData, res.data);
         console.log('选中SKU', selectedSku, useItems, res.data.dispatchTypeList)
      }
    },
      // 将商品加入购物车
    addToCart(selectedSkuData, data) {
      console.log('添加商品到购物车：', selectedSkuData);
      if (!selectedSkuData) return
      const params = {
        productCode: selectedSkuData.skuInfo.productCode,
        productSkuCode: selectedSkuData.skuInfo.productSkuCode,
        shopId: selectedSkuData.shopId,
        shopName: selectedSkuData.shopName,
        shopLogo: selectedSkuData.shopLogo,
        shopPhone: selectedSkuData.shopPhone,
        skuCount: selectedSkuData.skuCount,
        skuPrice: selectedSkuData.skuInfo.salePrice,
        skuPoints: selectedSkuData.skuInfo.points,
        dispatchType: selectedSkuData.dispatchType.value,
        organizeType: data.organizeType,
        organizeName: wx.getStorageSync('organizeName') || ''
      }
      console.log('添加购物车参数', params)
      CartApi.fetchAddProductToCart({
        memberCartVO: params
      })
        .then(res => {
          Tip.loaded();
          console.log('添加到购物车结果：', res);
          if (res.flag) {
            Tip.toast('添加到购物车成功', null, 'none');
          } else {
            Tip.toast(res.data, null, 'none');
          }
        })
    },
    // 将skuList转为规格列表形式
    handleSkuListToAttribute(spu, skuList) {
      // 商品无sku
      // 仅存在一个sku,并且该sku的规格属性为空
      if (skuList.length === 1 && (!skuList[0].attributeValueList.length || skuList[0].attributeValueList.length <= 0)) {
        return skuList[0];
      }
      let list = [];
      let obj = {};
      skuList.forEach(ele => {
        list.push(...ele.attributeValueList);
      });
      list.forEach(ele => {
        let attribute = obj[ele.attributeId];
        if (attribute) {
          const isHave = attribute.valueList.find(val => val.attributeValueId === ele.attributeValueId);
          if (!isHave) {
            attribute.valueList.push({
              checked: false,
              attributeValueId: ele.attributeValueId,
              attributeValue: ele.attributeValue,
              attributeValuePic: ele.attributeValuePic
            });
          }
        } else {
          obj[ele.attributeId] = {
            attributeId: ele.attributeId,
            attributeName: ele.attributeName,
            valueList: [{
              checked: false,
              attributeValueId: ele.attributeValueId,
              attributeValue: ele.attributeValue,
              attributeValuePic: ele.attributeValuePic
            }]
          }
        }
      });
      // 对规格进行排序，例如尺码从小到大排序
      const attrList = Object.keys(obj).map(key => {
        obj[key].valueList.sort((ele1, ele2) => ele1.attributeValueId - ele2.attributeValueId);
        return obj[key];
      });
      let selectedSku = null;
      this.setData({
        attributeList: attrList
      });
      attrList.map((item, index) => {
        selectedSku = this.pickAttribute(index, 0, spu);
      })
      console.log('规格列表', attrList, selectedSku);
      return selectedSku;
    },

    // 选择属性
    pickAttribute(defaultAttrIndex, defaultAttrValueIndex, spu) {
      const attributeList = this.data.attributeList;
      const currentAttrIndex = defaultAttrIndex;
      const currentAttrValueIndex = defaultAttrValueIndex;
      const currentAttrValueList = attributeList[currentAttrIndex].valueList;
      const currentAttrValue = currentAttrValueList[currentAttrValueIndex];
      currentAttrValueList.forEach((val, index) => {
        val.checked = val.attributeValueId === currentAttrValue.attributeValueId;
      });
      this.setData({
        [`attributeList[${currentAttrIndex}].valueList`]: currentAttrValueList
      });
      const selectedSku = this.groupSelectedSku(attributeList, spu);
      console.log('选择的sku：', selectedSku);
      if (selectedSku) {
        let stock = selectedSku.saleStock;
        // 判断活动库存是否存在
        if (selectedSku.marketStock && selectedSku.marketStock > 0) {
          stock = selectedSku.marketStock;
        }
        return selectedSku;
      } else {
        return null;
      }
      console.log('数据----：', this.data);
    },
    // 计算选择的sku
    groupSelectedSku(list, spu) {
      const maxValueLength = list.length;
      const selectedValueId = [];
      list.forEach(attr => {
        attr.valueList.forEach(val => {
          if (val.checked) {
            selectedValueId.push(val.attributeValueId);
          }
        });
      });
      if (selectedValueId.length < maxValueLength) return
      selectedValueId.sort((a, b) => a - b);
      const idStr = `ATTRIB_${selectedValueId.join('_')}_`;
      console.log('sku-key：', idStr);
      return spu.skuExtList.find(sku => sku.attributeValueIdList === idStr);
    }
  }
})
