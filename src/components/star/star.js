/**
 * bindCar              绑定车牌弹窗
 * @isClick             Boolean  点击背景色是否关闭弹窗
 * @showDialog          Boolean  是否显示（默认不显示）
 * @province            NUMBER   省份数组index值（默认11 皖）
 */
const LENGTH = 5;
const CLS_ON = 'on';
const CLS_HALF = 'half';
const CLS_OFF = 'off';

Component({
  properties: {
    isClick: {
      type: Boolean,
      value: false
    },
    score: {
      type: Number,
      observer: function(newVal, oldVal) {
        if (newVal != oldVal) {
          this.init()
        }
      }
    }
  },

  // 生命周期函数 组件生命周期函数，在组件实例进入页面节点树时执行
  attached: function() {
    this.init();
  },

  data: {
    itemClasses: []
  },

  methods: {
    // 初始化数据
    init() {
      let result = [];
      let score = Math.floor(this.properties.score * 2) / 2;
      let hasDecimal = score % 1 !== 0;
      let integer = Math.floor(score);
      for (let i = 0; i < integer; i++) {
        result.push(CLS_ON);
      }
      if (hasDecimal) {
        result.push(CLS_HALF);
      }
      while (result.length < LENGTH) {
        result.push(CLS_OFF);
      }
      this.setData({
        itemClasses: result
      });
    },

    // 点击星星
    handleClick(e) {
      if (!this.properties.isClick) return false;
      let dataset = e.currentTarget.dataset;
      let index = dataset.index;
      console.log(index)
      this.triggerEvent('SelectValue', { value: index }, {})
    }
  }
});
