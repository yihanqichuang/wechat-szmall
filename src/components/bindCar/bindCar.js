/**
 * bindCar              绑定车牌弹窗
 * @isClick             Boolean  点击背景色是否关闭弹窗
 * @showDialog          Boolean  是否显示（默认不显示）
 * @province            NUMBER   省份数组index值（默认11 皖）
 */
import { CARNUMBER } from '../../utils/regular';
import { ProvinceCode } from '../../utils/provinceCode';
Component({
  properties: {
    isClick: {
      type: Boolean,
      value: true
    },
    showDialog: {
      type: Boolean,
      value: false,
      observer: function(newVal) {
        let that = this;
        if (newVal == false) {
          that.setData({
            carNumber: ''
          });
        }
      }
    },
    province: {
      type: Number,
      value: 11
    }
  },
  data: {
    // 这里是一些组件内部数据
    carNumber: '',
    carNumber1: '',
    carNumber2: '',
    carNumber3: '',
    carNumber4: '',
    carNumber5: '',
    carNumber6: '',
    carNumber7: '新',
    inputBoxCss1: 'inputBox',
    inputBoxCss2: 'inputBox',
    inputBoxCss3: 'inputBox',
    inputBoxCss4: 'inputBox',
    inputBoxCss5: 'inputBox',
    inputBoxCss6: 'inputBox',
    inputBoxCss7: 'inputBox',
    provinceCss: 'inputBox',
    index: 0,

    messageComponent: '',
    errTips: '',
    provinceCode: ProvinceCode,
    provinceDefault: '${provinceDefault}' || '浙',
    keyBoardType: 1,
    isShowKey: false,
    isShowEnsure: true
  },
  methods: {
    // 背景是否能关闭
    onClickdiaCenterView() {
      if (this.properties.isClick) {
        return false;
      }
      this.setData({
        showDialog: !this.data.showDialog
      });
    },

    // 关闭弹窗
    handleCloseDialog() {
      this.setData({
        showDialog: !this.data.showDialog,
        carNumber: '',
        carNumber1: '',
        carNumber2: '',
        carNumber3: '',
        carNumber4: '',
        carNumber5: '',
        carNumber6: '',
        carNumber7: '新',
        errTips: '',
        keyBoardType: 1,
        isShowKey: false
      });
      this.triggerEvent('handleClose', {}, {});
    },

    // 关闭所有
    handleInputCarClose() {
      this.setData({
        inputBoxCss1: 'inputBox',
        inputBoxCss2: 'inputBox',
        inputBoxCss3: 'inputBox',
        inputBoxCss4: 'inputBox',
        inputBoxCss5: 'inputBox',
        inputBoxCss6: 'inputBox',
        inputBoxCss7: 'inputBox',
        provinceCss: 'inputBox'
      });
    },

    // 输入的车牌号
    handleInputCar(index) {
      if (index >= 0) {
        this.setData({
          index: index
        });
      }

      this.handleInputCarClose()
      if (index == 0) {
        this.setData({
          inputBoxCss1: 'inputBox1'
        });
      }
      if (index == 1) {
        this.setData({
          inputBoxCss2: 'inputBox1'
        });
      }
      if (index == 2) {
        this.setData({
          inputBoxCss3: 'inputBox1'
        });
      }
      if (index == 3) {
        this.setData({
          inputBoxCss4: 'inputBox1'
        });
      }
      if (index == 4) {
        this.setData({
          inputBoxCss5: 'inputBox1'
        });
      }
      if (index == 5) {
        this.setData({
          inputBoxCss6: 'inputBox1'
        });
      }
      if (index == 6) {
        this.setData({
          inputBoxCss7: 'inputBox1'
        });
      }
    },

    // 输入的车牌号
    handleInputCar1() {
      this.handleInputCar(0)
    },
    // 输入的车牌号
    handleInputCar2() {
      this.handleInputCar(1)
    },
    // 输入的车牌号
    handleInputCar3() {
      this.handleInputCar(2)
    },
    // 输入的车牌号
    handleInputCar4() {
      this.handleInputCar(3)
    },
    // 输入的车牌号
    handleInputCar5() {
      this.handleInputCar(4)
    },
    // 输入的车牌号
    handleInputCar6() {
      this.handleInputCar(5)
    },
    // 输入的车牌号
    handleInputCar7() {
      this.handleInputCar(6)
    },

    // 选择
    bindPickerChange: function(e) {
      console.log('picker发送选择改变，携带值为', e.detail.value);
      this.setData({
        province: e.detail.value
      });
    },

    // 点击省份
    chooseProvice() {
      this.setData({
        isShowKey: true,
        keyBoardType: 1
      });
      this.handleInputCarClose()
      this.setData({
        provinceCss: 'inputBox1'
      });
    },

    // 选择省份
    provinceNumber(e) {
      this.setData({
        provinceDefault: e.detail
      });
    },

    // 点击输入车牌
    chooseCarNumber() {
      this.setData({
        isShowKey: true,
        keyBoardType: 2
      });
    },

    // 更新车牌
    handleNumberSet(index, value) {
      if (index == 0) {
        this.setData({
          carNumber1: value
        });
      }
      if (index == 1) {
        this.setData({
          carNumber2: value
        });
      }
      if (index == 2) {
        this.setData({
          carNumber3: value
        });
      }
      if (index == 3) {
        this.setData({
          carNumber4: value
        });
      }
      if (index == 4) {
        this.setData({
          carNumber5: value
        });
      }
      if (index == 5) {
        this.setData({
          carNumber6: value
        });
      }
      if (index == 6) {
        this.setData({
          carNumber7: value
        });
      }
    },

    // 点击删除按钮
    handleNumberDel() {
      let index = this.data.index;
      this.handleNumberSet(index, '')
    },

    // 收起键盘
    handleEnsure() {
      this.setData({
        isShowKey: false
      });
    },

    // 选择车牌
    vehicleTapNumber(e) {
      // 判断是选择的内容
      let value = e.detail;
      let index = this.data.index;
      // 更新车牌
      this.handleNumberSet(index, value)
      // 更新下标
      index = (index + 1) > 6 ? 6 : (index + 1)
      this.setData({
        index: index
      });
      this.handleInputCar(index)
    },

    // 点击确定（抛出省份和车牌号）
    handleSubmit() {
      let carNumber = this.data.carNumber1 + this.data.carNumber2 + this.data.carNumber3 + this.data.carNumber4 + this.data.carNumber5 + this.data.carNumber6 
      if (!carNumber) {
        this.setData({
          errTips: '请输入车牌号码'
        });
        return false;
      } else if (!CARNUMBER.test(carNumber)) {
        this.setData({
          errTips: '请输入正确的车牌号码'
        });
        return false;
      } else {
        this.setData({
          errTips: '',
          keyBoardType: 1,
          isShowKey: false
        });
        this.triggerEvent(
          'handleSure',
          {
            province: this.data.provinceDefault,
            carNumber: carNumber + (this.data.carNumber7 == '新' ? '' : this.data.carNumber7)
          },
          {}
        );
        this.handleCloseDialog()
      }
    }
  }
});
