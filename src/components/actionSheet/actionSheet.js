/**
 * 页面底部弹窗组件
 * @isClick             Boolean  点击背景色是否关闭弹窗
 * @showCenterDialog    Boolean  是否显示（默认不显示）
 * @isShowImage         Boolean  是否显示image slot（默认不显示）
 */
Component({
  options: {
    multipleSlots: true // 在组件定义时的选项中启用多slot支持
  },
  properties: {
    isClick: {
      type: Boolean,
      value: false
    },
    showCenterDialog: {
      type: Boolean,
      value: false
    },
    isShowImage: {
      type: Boolean,
      value: false
    }
  },
  data: {
    // 这里是一些组件内部数据
  },
  methods: {
    onClickdiaCenterView() {
      if (this.properties.isClick) {
        return false
      }
      this.setData({
        showCenterDialog: false
      });
    }
  }
});
