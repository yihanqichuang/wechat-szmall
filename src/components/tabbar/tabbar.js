// navbar.js
const app = getApp()
import GetPage from '../../utils/getCurrentPageUrl'
import LoginApi from '../../api/login.js';

Component({

  /**
   * 组件的初始数据
   */
  data: {
    tabBar: [],
    color: '#9E9E9E',
    selectedColor: '#f00',
    backgroundColor: '#fff',
    borderStyle: '#ccc'
  },
  lifetimes: {
    attached() {
      this.initTabBar();
    }
  },
  /**
   * 组件的方法列表
   */
  methods: {
    // 初始化tabBar数据
    async initTabBar() {
      let tabBar = app.globalData.tabBar || [];
      if (tabBar.length === 0) {
        const res = await LoginApi.getTabbar();
        if (res.data.flag) {
          tabBar = res.data.data.map(tab => {
            tab.active = false;
            return tab;
          })
          app.globalData.tabBar = tabBar; // 保存到全局数据，别的页面不需要在此请求
        }
      }
      this.initActive(tabBar);
    },
    // 高亮当前tab
    initActive(list) {
      const currentPages = GetPage.getCurrentPageUrl();
      const path = `/${currentPages}`;
      list.forEach(tab => {
        tab.active = false;
        tab.active = path === tab.pathUrl;
      });
      this.setData({ tabBar: list });
    },

    // 跳转
    switchTab(e) {
      let url = e.currentTarget.dataset.url;
      wx.switchTab({ url });
    }
  }
})
