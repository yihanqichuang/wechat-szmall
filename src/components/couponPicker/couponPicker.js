import coupon from '../../api/coupon'
import Tip from '../../utils/tip'
Component({
  externalClasses: ['icon-close'],
  properties: {
    couponList: {
      type: Array,
      default: []
    }
  },
  data: {
    isShow: false,
    maxCount: 20
  },
  lifetimes: {
    attached () {
    }
  },

  methods: {
    // 打开picker
    showPicker() {
      this.setData({ isShow: true });
    },
    // 关闭picker
    hidePicker() {
      this.setData({ isShow: false });
    },
    // 领取
    getCoupon(e) {
      this.data.maxCount = 20;
      console.log('当前卡券：', e);
      const _index = e.currentTarget.dataset.index;
      const _currentCoupon = this.properties.couponList[_index];
      let param = {
        couponId: _currentCoupon.couponId
      };
      this.setData({
        [`couponList[${_index}].loading`]: true
      });
      coupon.createGiftAndCouponOrder(param).then(res => {
        console.log('下单结果：', res);
        if (res.flag) {
          this.getFailType(res.data, _index);
        } else {
          Tip.toast(res.data, null, 'none');
          this.setData({
            [`couponList[${_index}].loading`]: false
          });
        }
      })
    },
    getFailType(orderNo, couponIndex) {
      coupon.queryOrderCache({}).then(res => {
        console.log('检查订单结果：', res);
        if (res.data.flag) {
          if (res.data.data.state === 'HANDING') {
            let timer = setTimeout(() => {
              this.getFailType(orderNo, couponIndex);
              clearTimeout(timer);
            }, 500);
          } else if (res.data.data.state === 'GAIN_SUCCESS') {
            Tip.toast('领取成功', null, 'none');
            this.setData({
              [`couponList[${couponIndex}].loading`]: false,
              [`couponList[${couponIndex}].memberHaveFlag`]: 'N'
            });
          } else if (res.data.data.state === 'FAIL') {
            Tip.toast('领取失败', null, 'none');
            this.setData({
              [`couponList[${couponIndex}].loading`]: false
            });
          } else {
            Tip.toast('领取成功', null, 'none');
            this.setData({
              [`couponList[${couponIndex}].loading`]: false,
              [`couponList[${couponIndex}].memberHaveFlag`]: 'N'
            });
          }
        } else {
          Tip.toast(res.data.data.errMsg, null, 'none');
          this.setData({
            [`couponList[${couponIndex}].loading`]: false
          });
        }
      })
    }
  }
});