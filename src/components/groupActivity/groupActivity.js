/**
 * dialog
 * @isClick             Boolean  点击背景色是否关闭弹窗
 * @showCenterDialog    Boolean  是否显示（默认不显示）
 */
Component({
  properties: {
    deliveryObj: {
      type: Object,
      value: null
    },
    showDialog: {
      type: Boolean,
      value: false
    }
  },
  data: {
    // 这里是一些组件内部数据
    show: false,// 页面是否显示
    groupInfoList: [],
  },
  methods: {
    // 打开dialog
    showDialog(groupInfoList) {
      this.setData({
        show: true,
        groupInfoList: groupInfoList
      });
    },
    // 选中某个团
    chooseOneGroup(e) {
      this.triggerEvent('chooseOneGroup', {
        groupinfocode: e.currentTarget.dataset.groupinfocode,
        chooseway: "joinGroup"
      });
      this.toggleDialog();
    },
    // 关闭dialog
    toggleDialog() {
      this.setData({
        show: false
      });
    }
  }
})
