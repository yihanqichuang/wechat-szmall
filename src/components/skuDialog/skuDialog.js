/**
 * Created by yu tao on 2019/7/10.
 */
import Tips from '../../utils/tip';
Component({
  externalClasses: ['icon-close', 'icon-minus', 'icon-add'],
  properties: {
    spu: {
      type: Object,
      value: {}
    },
    chooseWay: {
      type: String,
      value: {}
    },
    groupInfo: {
      type: Object
    },
    shopInfo: {
      type: Object,
      value: {}
    },
  },
  observers: {
    spu: function (nv, ov) {
      if (nv) {
        this.handleSkuListToAttribute(nv.skuExtList);
        this.initDispatchWays();
        this.setData({
          organizeType: nv.organizeType,
          selectedSkuPicture: nv.pictureList[0],
          saleType: nv.saleType,
          showPrice: nv.minSalePrice,
          showPoints: nv.minPoints,
          showStock: nv.sumSaleStock,
          limitBuyNum: nv.limitBuyNum,
          memberBuyCount: nv.memberBuyCount
        });
      }
    },
    shopInfo: function (nv, ov) {
      if (nv) {
        if ('Y' === nv.isVirtual) {
          this.initDispatchWays(true);
        }
      }
    }
  },
  data: {
    showSkuPicker: false,
    selectedSkuPicture: '',
    saleType: 'ALLMONEY',
    showPrice: null,
    showPoints: null,
    showStock: null,
    limitBuyNum: null,
    memberBuyCount: null,
    defaultItems: [
      {
        name: '快递配送',
        value: 'EXPRESS',
        disabled: false,
        checked: true
      },
      {
        name: '到店自提',
        value: 'PICK_UP',
        disabled: false,
        checked: false
      },
      // {
      //   name: '同城配送',
      //   value: 'CITY_EXPRESS',
      //   disabled: false,
      //   checked: false
      // }
    ],
    items: [],
    attributeList: [],
    selectedSku: '',
    productCount: 1,
    organizeType: '', // 商品类型： SuperMarket超市商品
    buyOriginNum: 1 // 起售数量，默认为1
  },
  methods: {
    // 打开
    showPicker() {
      // 处理初期显示数据
      let showPrice = null;
      let showPoints = null;
      if ('openGroup' === this.properties.chooseWay || 'joinGroup' === this.properties.chooseWay) {
        // 显示拼团价格
        showPrice = this.properties.groupInfo.groupActivityVO.displayPrice;
        showPoints = this.properties.groupInfo.groupActivityVO.displayPoint;
      } else {
        // 显示正常的售卖价格
        showPrice = this.properties.spu.minSalePrice;
        showPoints = this.properties.spu.minPoints;
      }
      this.setData({
        organizeType: this.properties.spu.organizeType,
        selectedSkuPicture: this.properties.spu.pictureList[0],
        saleType: this.properties.spu.saleType,
        showPrice: showPrice,
        showPoints: showPoints,
        showStock: this.properties.spu.sumSaleStock,
        limitBuyNum: this.properties.spu.limitBuyNum,
        memberBuyCount: this.properties.spu.memberBuyCount
      });
      // 处理规格list
      this.handleSkuListToAttribute(this.properties.spu.skuExtList);
      this.setData({
        showSkuPicker: true
      });
    },
    // 关闭
    hidePicker() {
      this.setData({
        showSkuPicker: false
      });
    },
    // 初始化配送方式
    initDispatchWays(isVirtual) {
      let dispatchItems = this.data.defaultItems;
      if (isVirtual) {
        // 虚拟商户只能选择快递配送
        dispatchItems = [
          {
            name: '快递配送',
            value: 'EXPRESS',
            disabled: false,
            checked: true
          }
        ]
      }
      this.setData({
        items: dispatchItems
      });
    },
    // 配送方式选择方式
    radioChange(e) {
      let dispatchItems = this.data.items;
      dispatchItems.forEach(ele => {
        ele.checked = e.detail.value === ele.value;
      });
      this.setData({
        items: dispatchItems
      });
    },

    // 将skuList转为规格列表形式
    handleSkuListToAttribute(skuList) {
      // 商品无sku
      // 仅存在一个sku,并且该sku的规格属性为空
      if (skuList.length === 1 && (!skuList[0].attributeValueList.length || skuList[0].attributeValueList.length <= 0)) {
        this.setData({
          selectedSku: skuList[0]
        });
        return false;
      }
      let list = [];
      let obj = {};
      skuList.forEach(ele => {
        if ('openGroup' === this.properties.chooseWay || 'joinGroup' === this.properties.chooseWay) {
          // 拼团活动时
          if (ele.marketType == 'GROUP') {
            // 只有参加拼团活动的sku才加入list
            list.push(...ele.attributeValueList);
          }
        } else {
          // 正常的情况下，将sku加入list
          list.push(...ele.attributeValueList);
        }
      });
      list.forEach(ele => {
        let attribute = obj[ele.attributeId];
        if (attribute) {
          const isHave = attribute.valueList.find(val => val.attributeValueId === ele.attributeValueId);
          if (!isHave) {
            attribute.valueList.push({
              checked: false,
              attributeValueId: ele.attributeValueId,
              attributeValue: ele.attributeValue,
              attributeValuePic: ele.attributeValuePic
            });
          }
        } else {
          obj[ele.attributeId] = {
            attributeId: ele.attributeId,
            attributeName: ele.attributeName,
            valueList: [{
              checked: false,
              attributeValueId: ele.attributeValueId,
              attributeValue: ele.attributeValue,
              attributeValuePic: ele.attributeValuePic
            }]
          }
        }
      });
      // 对规格进行排序，例如尺码从小到大排序
      const attrList = Object.keys(obj).map(key => {
        obj[key].valueList.sort((ele1, ele2) => ele1.attributeValueId - ele2.attributeValueId);
        return obj[key];
      });
      this.setData({
        attributeList: attrList
      }, () => {
        attrList.map((item, index) => {
          this.pickAttribute(null, index, 0);
        })
      });
    },

    // 选择属性
    pickAttribute(e, defaultAttrIndex, defaultAttrValueIndex) {
      const attributeList = this.data.attributeList;
      const currentAttrIndex = e ? e.target.dataset.attrIndex : defaultAttrIndex;
      const currentAttrValueIndex = e ? e.target.dataset.attrValueIndex : defaultAttrValueIndex;
      const currentAttrValueList = attributeList[currentAttrIndex].valueList;
      const currentAttrValue = currentAttrValueList[currentAttrValueIndex];
      currentAttrValueList.forEach((val, index) => {
        val.checked = val.attributeValueId === currentAttrValue.attributeValueId;
      });
      this.setData({
        [`attributeList[${currentAttrIndex}].valueList`]: currentAttrValueList
      });
      if (currentAttrValue.attributeValuePic) {
        this.setData({
          selectedSkuPicture: currentAttrValue.attributeValuePic
        });
      }
      const selectedSku = this.groupSelectedSku(attributeList);
      if (selectedSku) {
        let stock = selectedSku.saleStock;
        // 判断活动库存是否存在
        if (selectedSku.marketStock && selectedSku.marketStock > 0) {
          stock = selectedSku.marketStock;
        }
        // 处理显示价格
        let showPrice = null;
        let showPoints = null;
        if ('openGroup' === this.properties.chooseWay) {
          // 显示团长价格
          showPrice = selectedSku.otherMarketPrice;
          showPoints = selectedSku.otherMarketPoint;
        } else if ('joinGroup' === this.properties.chooseWay) {
          // 显示拼团价格
          showPrice = selectedSku.marketPrice;
          showPoints = selectedSku.marketPoint;
        } else {
          // 显示正常的售卖价格
          showPrice = selectedSku.salePrice;
          showPoints = selectedSku.points;
        }
        this.setData({
          selectedSku,
          showPrice: showPrice,
          showPoints: showPoints,
          showStock: stock,
          saleType: selectedSku.saleType
        });
      }
    },
    // 计算选择的sku
    groupSelectedSku(list) {
      const maxValueLength = list.length;
      const selectedValueId = [];
      list.forEach(attr => {
        attr.valueList.forEach(val => {
          if (val.checked) {
            selectedValueId.push(val.attributeValueId);
          }
        });
      });
      if (selectedValueId.length < maxValueLength) return
      selectedValueId.sort((a, b) => a - b);
      const idStr = `ATTRIB_${selectedValueId.join('_')}_`;
      return this.properties.spu.skuExtList.find(sku => sku.attributeValueIdList === idStr);
    },
    // 加减数量
    changeNum(event) {
      let n = Number(event.currentTarget.dataset.var);
      let productCount = this.data.productCount + n;
      this.setData({
        productCount
      });
    },

    // 确定提交
    confirmSubmit() {
      let selectedDispatchWay = this.data.items.find(way => way.checked);
      // 超市商品从 确认订单 选择配送方式
      if (this.data.organizeType === 'SuperMarket') {
        // 超市商品默认同城配送(处理添加购物车时过校验)
        selectedDispatchWay = {
          name: '同城配送',
          value: 'CITY_EXPRESS'
        }

      } else {
        if (!selectedDispatchWay) {
          wx.showToast({
            title: '请选择配送方式',
            icon: 'none'
          });
          return false;
        }
      }
      if (!this.data.selectedSku) {
        wx.showToast({
          title: '请选择商品规格',
          icon: 'none'
        });
        return false;
      }
      console.log('选择的商品：', this.data.selectedSku);
      if (('openGroup' === this.properties.chooseWay || 'joinGroup' === this.properties.chooseWay) && this.data.selectedSku.marketStock === 0) {
        wx.showModal({
          title: '温馨提示',
          content: '活动库存已售罄，将以正常价格进行购买，是否确定？',
          confirmText: '确定购买',
          confirmColor: '#FB5B41',
          success: res => {
            if (res.confirm) {
              this.handleSubmit(selectedDispatchWay, this.data.selectedSku.marketStock === 0);
            }
          }
        });
      } else {
        this.handleSubmit(selectedDispatchWay, false);
      }
    },
    handleSubmit(selectedDispatchWay, noMarketStock) {
      this.triggerEvent('submitSku', {
        dispatchWay: selectedDispatchWay,
        productCount: this.data.productCount,
        selectedSku: this.data.selectedSku,
        noMarketStock: noMarketStock
      });
      this.hidePicker();
    }
  }
});