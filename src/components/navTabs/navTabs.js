Component({
  properties: {
    tabs: {
      type: Array
    },
    isFixed: {
      type: Boolean,
      value: true
    },
    isChangeOrganize: {
      type: Boolean,
      observer: function(val) {
        console.log('isChangeOrganize', val)
        if(val){
          this.setData({
            isChangeOrganize: val
          })
        }
      }
    },
    activeIndex: {
      type: Number,
      observer: function(val) {
        console.log('activeIndex', val)
        if(val){
          this.setData({
            activeIndex: val
          })
        }
      }
    }
  },
  data: {
    activeIndex: 0,
    isChangeOrganize: false
  },
  methods: {
    // 切换 tab
    tap(event) {
      let index = event.currentTarget.dataset.index;
      if (index === this.data.activeIndex) return;
      this.setData({ activeIndex: index });
      this.triggerEvent('changeIndex', index);
    }
  }
});
