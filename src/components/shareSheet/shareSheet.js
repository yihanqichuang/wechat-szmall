/**
 * Created by yu tao on 2019/7/10.
 */

Component({
  properties: {
    showCenterDialog: {
      type: Boolean,
      value: false
    },
    imgUrl: {
      type: String,
      value: ''
    },
    width: {
      type: String,
      value: '570rpx'
    },
    height: {
      type: String,
      value: '624rpx'
    },
    top: {
      type: String,
      value: '50%'
    }
  },
  data: {},
  methods: {
    // 分享给朋友
    handleShareFriends() {
      this.setData({
        showCenterDialog: false
      })
    },

    // 下载
    handleDownload() {
      this.setData({
        showCenterDialog: false
      });
      this.triggerEvent('handleCreate', {}, {})
    }
  }
});
