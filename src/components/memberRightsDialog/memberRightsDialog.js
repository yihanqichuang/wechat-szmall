import OrderApi from '../../api/order';
import TimeFormat from '../../utils/timeFormat.js';
Component({
  properties: {},
  data: {
    showDialog: false, // dialog显示
    rightsType: null, // 显示会员权益类型 FREE_SHIP:包邮 DISCOUNT:折扣
    rightsDetail: null, // 会员权益对象内容
    marketTime: '',
    freeShipMsg: null, // 包邮优惠信息
    memberDiscountMsg: null // 会员折扣优惠信息
  },
  methods: {
    // dialog显示
    showDialog(rightsType, extraInfo) {
      let ext = "";
      if (extraInfo) {
        ext = JSON.parse(extraInfo);
      }
      this.setData({
        showDialog: true,
        rightsType: rightsType,
        freeShipMsg: ext.freeShipMsg ? ext.freeShipMsg : "",
        memberDiscountMsg: ext.memberDiscountMsg ? ext.memberDiscountMsg : ""
      })
      this.getRightsDetail();
    },
    // 通过会员id获取会员权益详情
    getRightsDetail() {
      if ('FREE_SHIP' === this.data.rightsType) {
        OrderApi.fetchGetFreeShipInfo().then(res => {
          if (res.flag) {
            this.setData({
              rightsDetail: res.data
            })
            if(this.data.rightsDetail){
              this.handleMarketTime();
            }
          } else {
            Tip.toast(res.data, null, 'none');
          }
        })
      } else {
        OrderApi.fetchGetDiscountInfo().then(res => {
          if (res.flag) {
            this.setData({
              rightsDetail: res.data
            })
            this.handleMarketTime();
          } else {
            Tip.toast(res.data, null, 'none');
          }
        })
      }
    },
    // 处理权益有效期显示
  handleMarketTime() {
    if ('FESTIVAL' === this.data.rightsDetail.marketType) {
      let startDate = TimeFormat.dateFormatFilter(this.data.rightsDetail.startDate, 'yyyy-MM-dd hh:mm:ss');
      let endDate = TimeFormat.dateFormatFilter(this.data.rightsDetail.endDate, 'yyyy-MM-dd hh:mm:ss');
      this.setData({
        marketTime: startDate + '至' + endDate
      });
    } else if ('MEMBER_DAY' === this.data.rightsDetail.marketType) {
      if ('DAY' === this.data.rightsDetail.repeatType) {
        this.setData({
          marketTime: '每天' + this.data.rightsDetail.memberDayStart + '至' + this.data.rightsDetail.memberDayEnd + '日'
        });
      } else if ('WEEK' === this.data.rightsDetail.repeatType) {
        let weekStr = '';
        for (let i = 0; i < this.data.rightsDetail.memberWeekList.length; i++) {
          if ('1' === this.data.rightsDetail.memberWeekList[i] || 1 === this.data.rightsDetail.memberWeekList[i]) {
            weekStr = weekStr + '周一 '
          } else if ('2' === this.data.rightsDetail.memberWeekList[i] || 2 === this.data.rightsDetail.memberWeekList[i]) {
            weekStr = weekStr + '周二 '
          } else if ('3' === this.data.rightsDetail.memberWeekList[i] || 3 === this.data.rightsDetail.memberWeekList[i]) {
            weekStr = weekStr + '周三 '
          } else if ('4' === this.data.rightsDetail.memberWeekList[i] || 4 === this.data.rightsDetail.memberWeekList[i]) {
            weekStr = weekStr + '周四 '
          } else if ('5' === this.data.rightsDetail.memberWeekList[i] || 5 === this.data.rightsDetail.memberWeekList[i]) {
            weekStr = weekStr + '周五 '
          } else if ('6' === this.data.rightsDetail.memberWeekList[i] || 6 === this.data.rightsDetail.memberWeekList[i]) {
            weekStr = weekStr + '周六 '
          } else if ('7' === this.data.rightsDetail.memberWeekList[i] || 7 === this.data.rightsDetail.memberWeekList[i]) {
            weekStr = weekStr + '周日 '
          }
        }
        this.setData({
          marketTime: weekStr
        });
      } else if ('MONTH' === this.data.rightsDetail.repeatType) {
        this.setData({
          marketTime: '每月' + this.data.rightsDetail.memberMonthStart + '日至' + this.data.rightsDetail.memberMonthEnd + '日'
        });
      }
    } else if ('BIRTHDAY' === this.data.rightsDetail.marketType) {
      if ('DAY' === this.data.rightsDetail.repeatType) {
        this.setData({
          marketTime: '生日当天'
        });
      } else if ('WEEK' === this.data.rightsDetail.repeatType) {
        this.setData({
          marketTime: '生日当周'
        });
      } else if ('MONTH' === this.data.rightsDetail.repeatType) {
        this.setData({
          marketTime: '生日当月'
        });
      }
    }
  },
    // 关闭弹窗
    toggleDialog() {
      this.setData({
        showDialog: false
      })
    }
  }
})
