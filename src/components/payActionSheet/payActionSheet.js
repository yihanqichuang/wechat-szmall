/**
 * 页面底部弹窗支付组件
 * @isClick               Boolean  点击背景色是否关闭弹窗
 * @showCenterDialog      Boolean  是否显示（默认不显示）
 * @handlePayPassWord     function 输入完密码
 * @handlePayClose        function 点击关闭按钮
 * @handleForgetPassword  function 点击忘记密码
 * 需要优化的地方：根据不同手机软键盘高度调整页面弹框高度
 */

Component({
  properties: {
    isShowForget: {
      type: Boolean,
      value: false
    },
    isClick: {
      type: Boolean,
      value: false
    },
    showCenterDialog: {
      type: Boolean,
      value: false,
      observer: function(newVal) {
        let that = this;
        if (newVal == true) {
          that.setData({
            getFocus: true
          });
        }
      }
    },
    getFocus: {
      type: Boolean,
      value: false
    },
    url: {
      type: String,
      value: ''
    },
    payAmount: {
      type: Number,
      value: 0
    }
  },
  data: {
    // 这里是一些组件内部数据
    isBindPassword: false
  },
  methods: {
    // 点击遮罩层
    onClickdiaCenterView() {
      if (this.properties.isClick) {
        return false
      }
      this.setData({
        showCenterDialog: !this.data.showCenterDialog
      });
    },

    // 输入密码
    valueSix(e) {
      let value = e.detail.value;
      this.triggerEvent('handlePayPassWord', { value: value }, {})
    },

    // 点击关闭按钮
    handleClose() {
      this.setData({
        showCenterDialog: !this.data.showCenterDialog
      });
      this.triggerEvent('handlePayClose')
    },

    // 点击忘记密码
    handleForgetPassword() {
      wx.navigateTo({
        url: '/pages/checkIdentify/checkIdentify'
      });
      this.triggerEvent('handleForgetPassword')
    }
  }
});
