/*
 * @Author: your name
 * @Date: 2020-09-21 09:54:28
 * @LastEditTime: 2020-10-13 11:37:02
 * @LastEditors: your name
 * @Description: In User Settings Edit
 * @FilePath: /WangMao-yidong/wxml-nbss/src/components/moneyItemMin/moneyItem.js
 */
/**
 * 秒杀列表 item
 */
Component({
  properties: {
    item: {
      type: Object
    },
    index: {
      type: Number || String
    },
    pathClass: {
      type: String,
      value: '../'
    },
    commodityId: {
      type: Number || String
    },
    margin0: {
      type: Boolean,
      value: false
    }
  },
  data: {
  
  },
  methods: {
  
  }
})
