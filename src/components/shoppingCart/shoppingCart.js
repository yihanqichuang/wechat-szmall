const app = getApp()
import cart from '../../api/shoppingcart'
import benMath from '../../utils/BenMath'
import tip from '../../utils/tip'

Component({
  externalClasses: ['checkbox-label'],
  properties: {
    roomId: {
      type: Number
    }
  },
  data: {
    list: [],
    pending: true,
    totalMoney: 0, // 选中的总金额
    totalPoint: 0, // 选中的总积分
    totalCount: 0,  // 选中的总数量
    isSelectAll: false, // 是否全选
    isDisabledAll: false, // 是否全部禁用
    isDeleteAll: false, // 是否全部删除
    shoppingCartCount: null, // 购物车全部的总数量
    ids: [], // 选中的id
    startX: 0,
    startY: 0
  },
  methods: {
    // 获取 列表数据
    getList(isCompute) {
      this.setData({ pending: true })
      cart.fetchShoppingCart().then(res => {
        if (!res.data.flag) {
          this.setData({ pending: false })
          return this.messageComponent.applyActive(res.data.msg || '获取购物车数据异常')
        }
        this.setData({
          list: [...this.handleList(res.data.data)]
        })
        wx.nextTick(() => isCompute && this.compute())
        this.setData({ pending: false })
      }).catch(() => {
        this.setData({ pending: false })
        this.messageComponent.applyActive('获取购物车数据异常!')
      })
    },
    // 判断 是否禁用勾选
    handleList(list) {
      list.forEach(item1 => {
        item1.shopCarList.forEach((item2) => {
          let disabled = this.handleItem(item2)
          item2.disabled = disabled
          item2.isSelected = disabled ? 'N' : item2.isSelected
        })
        item1.disabled = item1.shopCarList.every(item => item.disabled)
        item1.isSelected = item1.shopCarList.every(item => item.isSelected === 'Y') ? 'Y' : 'N'
      })
      this.setData({
        isDisabledAll: list.every(item => item.disabled),
        isSelectAll: list.filter(item => !item.disabled).every(item => item.isSelected === 'Y')
      })
      console.log(list)
      return list
    },
    handleItem(item) {
      return (item.commodityState !== 'ONLINE') ||
        (item.commodityNum <= 0) ||
        (item.buyLimitNum === 0 ? false : item.buyLimitNum <= item.buyNum)
    },
    // checkbox - 商品
    handleCheckBoxL3(e) {
      let value = Boolean(e.detail.value[0])
      let { idx1, idx2 } = e.currentTarget.dataset
      this.changeCheckBoxL3(value, idx1, idx2)
      wx.nextTick(() => {
        this.changeCheckBoxL2(this.data.list[idx1].shopCarList.every(item => item.isSelected === 'Y' || item.disabled), idx1)
      })
      wx.nextTick(() => {
        this.changeCheckBoxL1(this.data.list.every(item => item.isSelected === 'Y' || item.disabled))
      })
      this.compute()
    },
    // checkbox - 商户
    handleCheckBoxL2(e) {
      let value = Boolean(e.detail.value[0])
      let idx1 = e.currentTarget.dataset.idx1
      this.changeCheckBoxL2(value, idx1)
      wx.nextTick(() => {
        this.changeCheckBoxL1(this.data.list.every(item => item.isSelected === 'Y'))
        this.data.list[idx1].shopCarList.forEach((item, idx2) => {
          if (!item.disabled) this.changeCheckBoxL3(value, idx1, idx2)
        })
      })
      this.compute()
    },
    // checkbox 全选
    handleCheckBoxL1(e) {
      console.log(e)
      let value = e.detail.value[0]
      this.changeCheckBoxL1(value)
      this.data.list.forEach((item1, idx1) => {
        if (!item1.disabled) this.changeCheckBoxL2(value, idx1)
        this.data.list[idx1].shopCarList.forEach((item2, idx2) => {
          if (!item2.disabled) this.changeCheckBoxL3(value, idx1, idx2)
        })
      })
      this.compute()
    },
    changeCheckBoxL3(value, idx1, idx2) {
      console.log(value, idx1, idx2)
      value = value ? 'Y' : 'N'
      this.setData({
        [`list[${idx1}].shopCarList[${idx2}].isSelected`]: value
      })
    },
    changeCheckBoxL2(value, idx1) {
      value = value ? 'Y' : 'N'
      this.setData({
        [`list[${idx1}].isSelected`]: value
      })
    },
    changeCheckBoxL1(value) {
      this.setData({
        'isSelectAll': Boolean(value)
      })
    },

    // 计算总数 和 总金额
    compute() {
      let totalCount = 0, totalMoney = 0, ids = [], totalPoint = 0;
      wx.nextTick(() => {
        this.data.list.forEach(item1 => {
          if (item1.isDelete) return;
          item1.shopCarList.forEach(item2 => {
            console.log(item2);
            if (item2.isDelete) return;
            if (item2.isSelected === 'Y' && !item2.disabled) {
              totalCount = totalCount + item2.commodityCount;
              totalMoney = benMath.accAdd(totalMoney, benMath.accMul(item2.commodityCount, item2.nowPrice));
              totalPoint = benMath.accAdd(totalPoint, benMath.accMul(item2.commodityCount, item2.points));
              ids.push(item2.memberShoppingchartId)
            }
          })
        });
        this.setData({ totalCount, totalMoney, ids, totalPoint })
      })
    },
    // 加减商品数量
    changeCount(e) {
      console.log(e)
      let dataset = e.currentTarget.dataset
      let variable = Number(dataset.var)
      if (variable === 1) {
        let shoppingCartCount = this.data.shoppingCartCount
        if (!shoppingCartCount) return this.messageComponent.applyActive('未获取到购物车总数，稍等')
        if (shoppingCartCount >= 200) return this.messageComponent.applyActive('购物车总数上限为200')
      }
      let { item, idx1, idx2 } = dataset
      let { commodityId, shopId, memberShoppingchartId, commodityCount } = item
      commodityCount = commodityCount + variable
      if (commodityCount > item.commodityNum) {
        // 大于剩余数量
        return variable === 1
          ? this.messageComponent.applyActive('宝贝只有这么多啦')
          : this.fetchUpdate({ commodityId, shopId, memberShoppingchartId, commodityCount: item.commodityNum }, idx1, idx2)
      } else if (item.limitPerDayNum && commodityCount > item.limitPerDayNum - item.buyNumToday) {
        // 大于限制数量
        return variable === 1
          ? this.messageComponent.applyActive('已达到每人每天限购数量')
          : this.fetchUpdate({ commodityId, shopId, memberShoppingchartId, commodityCount: item.limitPerDayNum - item.buyNumToday }, idx1, idx2)
      } else if (item.buyLimitNum && commodityCount > item.buyLimitNum - item.buyNum) {
        // 大于限制数量
        return variable === 1
          ? this.messageComponent.applyActive('已达到每人限购数量')
          : this.fetchUpdate({ commodityId, shopId, memberShoppingchartId, commodityCount: item.buyLimitNum - item.buyNum }, idx1, idx2)
      } else if (commodityCount < 1) {
        // 减少 至少为 1
        return this.messageComponent.applyActive('商品数量需大于0')
      } else {
        this.fetchUpdate({ commodityId, shopId, memberShoppingchartId, commodityCount }, idx1, idx2)
      }
    },
    fetchUpdate(params, idx1, idx2) {
      cart.fetchUpdate(params).then(res => {
        console.log(res)
        if (!res.data.flag) return this.messageComponent.applyActive(res.data.msg || '数量更新失败')
        this.setData({
          shoppingCartCount: res.data.data,
          [`list[${idx1}].shopCarList[${idx2}].commodityCount`]: params.commodityCount
        })
        this.compute()
      }).catch(() => {
        this.messageComponent.applyActive('数量更新失败!')
      })
    },
    // 获取 全部购物车总数
    fetchShoppingCartCount() {
      cart.fetchShoppingCartCount().then(res => {
        this.setData({ shoppingCartCount: res.data.data })
      })
    },
    // 删除 多选
    deletes() {
      if (!this.data.ids.length) return this.messageComponent.applyActive('请选择要删除的商品')
      tip.confirm(`确定要删除吗？`, '', '提示', '取消', '确认').then(() => {
        this.confirmDelete(this.data.ids.toString())
      }).catch(() => {
        console.log('取消删除')
      })
    },
    // 删除 左滑
    deleteItem(e) {
      let { id, idx1, idx2 } = e.currentTarget.dataset
      this.confirmDelete(id, idx1, idx2)
    },
    confirmDelete(dataIds, idx1, idx2) {
      cart.fetchDelete({ dataIds }).then(res => {
        if (!res.data.flag) return this.messageComponent.applyActive(res.data.msg || '删除失败')
        this.compute()
        tip.success('删除成功')
        console.log(dataIds, idx1, idx2)
        idx1 || idx1 === 0 ? this.softDelete(idx1, idx2) : this.getList(true)
      }).catch(() => {
        this.messageComponent.applyActive('删除失败!')
      })
    },
    softDelete(idx1, idx2) {
      this.setData({
        [`list[${idx1}].shopCarList[${idx2}].isDelete`]: true,
        [`list[${idx1}].shopCarList[${idx2}].isSelected`]: 'N'
      })
      wx.nextTick(() => {
        this.setData({
          [`list[${idx1}].isDelete`]: this.data.list[idx1].shopCarList.every(item => item.isDelete)
        })
      })
      wx.nextTick(() => {
        this.setData({
          isDeleteAll: this.data.list.every(item => item.isDelete)
        })
      })
    },

    // 去结算
    settlement() {
      if (!this.data.ids.length) return this.messageComponent.applyActive('请选择要结算的商品');
      let data = [];
      this.data.list.forEach(item1 => {
        if (item1.shopCarList.some(item => item.isSelected === 'Y')) {
          let obj = {
            shopId: item1.shopId,
            shopName: item1.shopName,
            orderDtls: []
          };
          item1.shopCarList.forEach(item2 => {
            if (item2.isSelected === 'Y') {
              obj.orderDtls.push({
                goodsId: item2.commodityId,
                count: item2.commodityCount,
                memberShoppingchartId: item2.memberShoppingchartId,
                commodityName: item2.commodityName,
                nowPrice: item2.nowPrice,
                picturePath: item2.picturePath,
                payType: item2.payType,
                points: item2.points,
                dispatchingWay: item2.dispatchingWay,
                skuSpecs: item2.skuSpecs
              });
            }
          });
          data.push(obj)
        }
      });
      app.globalData.cartOrderData = data;
      wx.navigateTo({
        url: '/pages/discover/submitOrder/submitOrder?source=cart&room_id=' + this.properties.roomId
      })
    },
    // touch事件
    touchstart(e) {
      this.setData({
        startX: e.touches[0].clientX,
        startY: e.touches[0].clientY
      })
    },
    touchmove(e) {
      if (e) return
      let { idx1, idx2, open } = e.currentTarget.dataset
      let { clientX } = e.touches[0]
      let distance = this.data.startX - clientX // 右滑 小于0    左滑 大于0
      let right = open ? 44 : 0  // open: true 删除按钮显示   false 隐藏
      distance = (open ? distance > 0 ? 0 : distance < -43 ? -43 : distance : distance < 0 ? 0 : distance > 43 ? 43 : distance) + right
      /*
       * 距离 = 按钮显示 ? 左滑 ? 0 ? 滑动距离大于按钮宽度 ? 按钮宽度 : 滑动距离 :
       *        按钮隐藏 右滑 ? 0 :滑动距离大于按钮宽度 ? 按钮宽度 : 滑动距离
       * */
      this.setData({
        [`list[${idx1}].shopCarList[${idx2}].right`]: distance
      })
    },
    touchend(e) {
      let { idx1, idx2 } = e.currentTarget.dataset
      let { clientX, clientY } = e.changedTouches[0]
      let distanceX = this.data.startX - clientX
      let distanceY = Math.abs(this.data.startY - clientY)
      let isOpen = distanceX > 30 && distanceY < 100
      this.setData({
        [`list[${idx1}].shopCarList[${idx2}].open`]: isOpen,
        [`list[${idx1}].shopCarList[${idx2}].right`]: isOpen ? '44' : '0'
      })
    },
    doNothing() {
      // 点击加减按钮附近区域，捕获点击事件不进行跳转
    },
    // 离开购物车时，记录勾选的商品
    fetchUpdateSelected(dataIds) {
      cart.fetchUpdateSelected({ dataIds }).then((res) => {
        if (res.data.flag) {
          app.globalData.memberBalanceScore = res.data.data;
        }
      })
    }
  },
  
  /**
   * 生命周期函数--监听页面加载
   */
  lifetimes: {
    detached() {
      this.fetchUpdateSelected(this.data.ids.toString())
    }
  },
  pageLifetimes: {
    show() {
      let memberType = wx.getStorageSync('memberType');
      if (memberType === 'MEMBER') {
        this.fetchShoppingCartCount()
        this.getList(true)
      } else {
        this.setData({ pending: false })
      }
      this.messageComponent = this.selectComponent('.messageTips')
    },
    hide() {
      this.fetchUpdateSelected(this.data.ids.toString())
      this.setData({
        isDeleteAll: false,
        isSelectAll: false
      })
    }
  },
  onLoad() {

  },
  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {
  
  },
  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {
  
  },
  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {
  
  }
})
