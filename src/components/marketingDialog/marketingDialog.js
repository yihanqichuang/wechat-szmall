/**
 * Created by yu tao on 2019/7/10.
 */
import Tips from '../../utils/tip';
var timer = null;
Component({
    data: {
        showDialog: false,
        homeWindowData: {}
    },
    methods: {
         //  营销跳转
    MarketingNavigator(e) {
        let moduleType = e.currentTarget.dataset.moduletype;
        let id = e.currentTarget.dataset.id;
        //  营销弹窗
        if (moduleType === 'COUPON') {
            wx.navigateTo({
                url: '/pages/newPackage/couponNew/detail/detail?id=' + id
            })
        } else if (moduleType === 'SIGNUP') {
            wx.navigateTo({
                url: '/pages/activeCenter/activityDetail/activityDetail?id=' + id
            })
        } else if (moduleType === 'QUESTION') {
            wx.navigateTo({
                url: '/pages/questionDetail/questionDetail?id=' + id
            })
        } else if (moduleType === 'TIMER') {
            wx.navigateTo({
                url: '/pages/newPackage/discount/discount'
            })
        }
        clearTimeout(timer);
        this.setData({
            showDialog: false
        })
        if (this.data.homeWindowData.timedClose) {
            clearInterval(timer);
        }
        this.setData({
            showDialog: false
        });
        clearInterval(this.data.timer);
        // if (this.data.datalist[1] && !this.data.datalist[1].checked) {
        //     this.windowList(this.data.datalist[1]);
        //     this.clickCount(this.data.datalist[1].id);
        //     this.data.datalist[1] = ''
        // }
    },
        // 打开营销弹窗
        showDialog(data) {
            this.setData(data);
            if (data.homeWindowData.timedClose) {
                this.countDown()
            }
            this.countDownDate();
        },

        // 关闭营销弹窗
        hideDialog() {
            if (this.data.timer) {
                clearInterval(this.data.timer);
                clearTimeout(timer);
            }
            this.setData({
                showDialog: false,
                countDownNum: 0
            }, () => {
                setTimeout(() => {
                    this.triggerEvent('showNext', { popupWindowsIndex: this.data.popupWindowsIndex })
                }, 500)
            });

        },
        //  倒计时
        countDown() {
            let that = this;
            let countDownNum = this.data.homeWindowData.timedClose;
            that.setData({
                timer: setInterval(function () {
                    countDownNum--;
                    that.setData({
                        countDownNum: countDownNum
                    });

                    if (countDownNum == 0) {
                        that.hideDialog();
                        // clearInterval(that.data.timer);
                        // that.setData({
                        //     showDialog: false
                        // });
                    }
                }, 1000)
            })
        },
        timeFormat(param) {
            return param < 10 ? '0' + param : param;
        },

        timeFormatSuB(time) {
            return time.substr(0, 10)
        },

        // 活动倒计时  两个定时器 都倒计时 会有问题
        countDownDate() {
            let newTime = new Date().getTime();
            let startTimeList = this.data.startTimeList;
            let endTimeList = this.data.endTimeList;
            let countDownArr = [];
            // 对结束时间进行处理渲染到页面
            let startT = startTimeList.replace(/-/g, '/');
            let startE = endTimeList.replace(/-/g, '/');

            let startTime = new Date(startT).getTime();
            let endTime = new Date(startE).getTime();
            let obj = null;
            if (startTime - newTime > 0) {
                let time = (startTime - newTime) / 1000;
                let day = parseInt(time / (60 * 60 * 24));
                let hou = parseInt(time % (60 * 60 * 24) / 3600);
                let min = parseInt(time % (60 * 60 * 24) % 3600 / 60);
                let sec = parseInt(time % (60 * 60 * 24) % 3600 % 60);
                obj = {
                    day: this.timeFormat(day),
                    hou: this.timeFormat(hou),
                    min: this.timeFormat(min),
                    sec: this.timeFormat(sec)
                };
                this.setData({
                    startDesc: '距离活动开始倒计时',
                    activeStart: true
                })
            } else if (endTime - newTime > 0) {
                let time = (endTime - newTime) / 1000;
                // 获取天、时、分、秒
                let day = parseInt(time / (60 * 60 * 24));
                let hou = parseInt(time % (60 * 60 * 24) / 3600);
                let min = parseInt(time % (60 * 60 * 24) % 3600 / 60);
                let sec = parseInt(time % (60 * 60 * 24) % 3600 % 60);
                obj = {
                    day: this.timeFormat(day),
                    hou: this.timeFormat(hou),
                    min: this.timeFormat(min),
                    sec: this.timeFormat(sec)
                };
                this.setData({
                    activeStart: true,
                    startDesc: '距离活动结束倒计时'
                })
            } else { // 活动已结束，全部设置为'00'
                setTimeout(() => {
                    clearTimeout(timer);
                }, 1000);
                this.setData({
                    activeStart: false
                })
            }
            countDownArr.push(obj);
            // 渲染，然后每隔一秒执行一次倒计时函数
            this.setData({ countDownList: countDownArr });
            timer = setTimeout(this.countDownDate.bind(this), 1000);
        },
    },
});