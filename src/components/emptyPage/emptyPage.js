/**
 * 空页面组件
 */
Component({
  externalClasses: ['empty-pages'],
  properties: {
    type: {
      type: String,
      observer: function(val) {
        let data = this.switchType(val);
        this.setData({
          src: data.src,
          text: data.text
        })
      }
    },
    src: {
      type: String,
      value: '../../images/icon/blank/blank_page_a@2x.png'
    },
    text: {
      type: String,
      text: '暂无内容'
    }
  },
  data: {
    src: '../../images/icon/blank/blank_page_a@2x.png',
    text: '暂无内容'
  },
  methods: {
    switchType(type) {
      switch (type) {
        // 积分明细
        case 'points':
          return {
            src: '../../images/icon/blank/blank_page_b@2x.png',
            text: '暂无积分明细'
          }
          // 预付卡明细
        case 'balance':
          return {
            src: '../../images/icon/blank/blank_page_b@2x.png',
            text: '暂无预付卡明细'
          }
          // 投票列表
        case 'voteList':
          return {
            src: '../../images/icon/blank/blank_page_a@2x.png',
            text: '暂无投票报名'
          }
          // 暂无缴费记录
        case 'paymentRecord':
          return {
            src: '../../images/icon/blank/blank_page_b@2x.png',
            text: '暂无缴费记录'
          }
          // 暂无消费记录
        case 'shoppingSheet':
          return {
            src: '../../images/icon/blank/blank_page_b@2x.png',
            text: '暂无消费记录'
          }
          // 暂无申请记录
        case 'applyList':
          return {
            src: '../../images/icon/blank/blank_page_b@2x.png',
            text: '暂无申请记录'
          }
          // 室内地图
        case 'map':
          return {
            src: '../../images/icon/blank/blank_page_a@2x.png',
            text: '暂无室内导航'
          }
        // 预付卡
        case 'prepaid':
          return {
            src: '../../images/icon/blank/blank_page_c@2x.png',
            text: '暂无预付卡'
          }
        // 我的订单
        case 'order':
          return {
            src: '../../images/icon/blank/blank_page_b@2x.png',
            text: '暂无订单信息'
          }
        // 兑换
        case 'exchange':
          return {
            src: '../../images/icon/blank/blank_page_e@2x.png',
            text: '暂无兑换信息'
          }
        // 活动
        case 'activity':
          return {
            src: '../../images/icon/blank/blank_page_f@2x.png',
            text: '暂无活动'
          }
        // 商品
        case 'commodity':
          return {
            src: '../../images/icon/blank/blank_page_g@2x.png',
            text: '暂无商品'
          }
        // 网络异常
        case 'networkError':
          return {
            src: '../../images/icon/blank/blank_page_h@2x.png',
            text: '网络异常！'
          }
        // 优惠券
        case 'coupon':
          return {
            src: '../../images/icon/blank/blank_page_d@2x.png',
            text: '暂无优惠券'
          }
        case 'pointsList':
          return {
            src: '../../images/icon/blank/blank_page_e@2x.png',
            text: '暂无礼品'
          }
        case 'eleInvoice':
          return {
            src: '../../images/icon/empty_record.png',
            text: '空空如也'
          }
        case 'liveList':
          return {
            src: '../../images/icon/blank/blank_page_b@2x.png',
            text: '暂无直播'
          }
        // 暂无内容
        default:
          return {
            src: '../../images/icon/blank/blank_page_a@2x.png',
            text: '暂无内容'
          }
      }
    }
  }
})
