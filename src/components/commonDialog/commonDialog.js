/**
 * Created by yu tao on 2019/7/10.
 */
import Tips from '../../utils/tip';
Component({
    data: {
        showDialog: false,
        homeWindowData: {}
    },
    methods: {
        // 打开通用弹窗
        showDialog(data) {
            this.setData(data);
            // 倒计时开始，如果存在定时器则先清除
            if (this.data.timer) {
                clearInterval(this.data.timer);
            }
            if (data.homeWindowData.timedClose) {
                this.countDown()
            }
        },

        // 关闭通用弹窗
        hideDialog() {
            if (this.data.timer) {
                clearInterval(this.data.timer);
            }
            this.setData({
                showDialog: false,
                countDownNum: 0
            }, () => {
                setTimeout(() => {
                    this.triggerEvent('showNext', { popupWindowsIndex: this.data.popupWindowsIndex })
                }, 500)
            });

        },
        //  通用 按钮指定跳转
        handleNavigator(e) {
            let navigate = e.currentTarget.dataset.navigate;
            if (navigate.indexOf('isSwitchTab=Y') != -1) {
                wx.switchTab({
                    url: navigate
                });
                this.hideDialog()
            } else {
                wx.navigateTo({
                    url: navigate
                })
            }
            this.hideDialog()
        },
        //  倒计时
        countDown() {
            let that = this;
            let countDownNum = that.data.homeWindowData.timedClose;
            that.setData({
                timer: setInterval(function () {
                    countDownNum--;
                    that.setData({
                        countDownNum: countDownNum
                    });

                    if (countDownNum == 0) {
                        that.hideDialog();
                        // clearInterval(that.data.timer);
                        // that.setData({
                        //     showDialog: false
                        // });
                    }
                }, 1000)
            })
        },
        //  营销跳转
        MarketingNavigator(e) {
            let moduleType = e.currentTarget.dataset.moduletype;
            let id = e.currentTarget.dataset.id;
            //  营销弹窗
            if (moduleType === 'COUPON') {
                wx.navigateTo({
                    url: '/pages/newPackage/couponNew/detail/detail?id=' + id
                })
            } else if (moduleType === 'SIGNUP') {
                wx.navigateTo({
                    url: '/pages/activeCenter/activityDetail/activityDetail?id=' + id
                })
            } else if (moduleType === 'QUESTION') {
                wx.navigateTo({
                    url: '/pages/questionDetail/questionDetail?id=' + id
                })
            } else if (moduleType === 'TIMER') {
                wx.navigateTo({
                    url: '/pages/newPackage/discount/discount'
                })
            }
            this.hideDialog();
            // if (this.data.datalist[1] && !this.data.datalist[1].checked) {
            //     this.windowList(this.data.datalist[1]);
            //     this.clickCount(this.data.datalist[1].id);
            //     this.data.datalist[1] = ''
            // }
        },
    }
});