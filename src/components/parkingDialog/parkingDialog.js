/**
 * Created by yu tao on 2019/7/10.
 */

Component({
  properties: {
    showCenterDialog: {
      type: Boolean,
      value: false
    },
    info: {
      type: Object,
      value: {}
    }
  },
  observers: {
    info: function() {
    }
  },
  data: {
    messageComponent: ''
  },
  lifetimes: {
    attached() {
      this.messageComponent = this.selectComponent('.messageTips');
    }
  },
  pageLifetimes: {
    hide() {
      
    }
  },
  methods: {

    handleSubmit() {
      this.triggerEvent('submitPay', {});
    },

    handleColse() {
      this.triggerEvent('closeDialog', {});
    }
  }
});
