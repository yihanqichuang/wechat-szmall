/**
 * Created by yu tao on 2019/7/10.
 */
import Tips from '../../utils/tip';
Component({
    externalClasses: ['icon-close', 'icon-minus', 'icon-add'],
    properties: {
        spu: {
            type: Object,
            value: {}
        },
        chooseWay: {
            type: String,
            value: {}
        },
        groupInfo: {
            type: Object
        },
    },
    observers: {
        spu: function (nv, ov) {
            if (nv) {
                this.handleSkuListToAttribute(nv.skuExtList);
            }
        }
    },
    data: {
        showSkuPicker: false,
        skuList: [],
        productCount: 1,
        selectedSku: null,
    },
    methods: {

        // 打开Picker
        showPicker() {
            // 处理规格list
            this.handleSkuListToAttribute(this.properties.spu.skuExtList);
            this.setData({
                showSkuPicker: true
            });
        },

        // 关闭Picker
        hidePicker() {
            this.setData({
                showSkuPicker: false
            });
        },

        // 将skuList转为规格列表形式
        handleSkuListToAttribute(skuList) {
            // 对规格进行排序，例如价格从小到大排序
            const attrList = skuList.sort(function (a, b) {
                const x = a['salePrice'];
                const y = b['salePrice'];
                return x - y < 0;
            });
            this.setData({
                skuList: attrList
            });
            console.log('规格列表', attrList);
        },

        // 选择属性
        pickAttribute(e) {
            let sku = e.currentTarget.dataset.sku;
            this.setData({
                selectedSku: sku
            })
        },

        // 计算选择的sku
        groupSelectedSku(list) {
            const maxValueLength = list.length;
            const selectedValueId = [];
            list.forEach(attr => {
                attr.valueList.forEach(val => {
                    if (val.checked) {
                        selectedValueId.push(val.attributeValueId);
                    }
                });
            });
            if (selectedValueId.length < maxValueLength) return
            selectedValueId.sort((a, b) => a - b);
            const idStr = `ATTRIB_${selectedValueId.join('_')}_`;
            console.log('sku-key：', idStr);
            return this.properties.spu.skuExtList.find(sku => sku.attributeValueIdList === idStr);
        },

        // 加减数量
        changeNum(event) {
            let n = Number(event.currentTarget.dataset.var);
            let productCount = this.data.productCount + n;
            this.setData({
                productCount
            });
        },

        // 确定提交
        confirmSubmit() {
            // 菜场默认配送方式为同城配送 CITY_EXPRESS
            let selectedDispatchWay = {
                name: '同城配送',
                value: 'CITY_EXPRESS',
                disabled: false,
                checked: false
            }
            if (!this.data.selectedSku) {
                wx.showToast({
                    title: '请选择菜品',
                    icon: 'none'
                });
                return false;
            }
            this.setData({
                selectedSku: this.data.selectedSku
            })
            if (this.data.selectedSku.marketStock === 0) {
                wx.showModal({
                    title: '温馨提示',
                    content: '活动库存已售罄，将以正常价格进行购买，是否确定？',
                    confirmText: '确定购买',
                    confirmColor: '#FB5B41',
                    success: res => {
                        if (res.confirm) {
                            this.handleSubmit(selectedDispatchWay, this.data.selectedSku.marketStock === 0);
                        }
                    }
                });
            } else {
                this.handleSubmit(selectedDispatchWay, false);
            }
        },
        handleSubmit(selectedDispatchWay, noMarketStock) {
            this.triggerEvent('submitSku', {
                dispatchWay: selectedDispatchWay,
                productCount: this.data.productCount,
                selectedSku: this.data.selectedSku,
                noMarketStock: noMarketStock
            });
            this.hidePicker();
        }
    }
});