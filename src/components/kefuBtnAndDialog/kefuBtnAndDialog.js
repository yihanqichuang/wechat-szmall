/**
 * 客服按钮和弹窗
 */
import wechat from '../../utils/wechat.js';
Component({
  properties: {

  },
  data: {
    messageComponent: '',
    serverPhone: '',
    organizeName: '',
    isKeFuShow: false
  },
  methods: {
    // 打开客服弹窗
    kefuDialog() {
      let organizePhone = wx.getStorageSync('organizePhone');
      let organizeName = wx.getStorageSync('organizeName');
      this.setData({
        isKeFuShow: true,
        organizeName: organizeName,
        serverPhone: organizePhone.trim()
      })
    },

    // 客服消息触发事件
    handleContact(e) {
      console.log(e.detail.path)
      console.log(e.detail.query)
    },

    // 拨打电话方法
    handleCall() {
      wechat.makePhoneCall(this.data.serverPhone).then(() => {
        console.log('拨打成功')
      }).catch(() => {
        console.log('拨打失败')
      })
    },

    // 关闭弹窗
    handleDialog() {
      this.setData({
        isKeFuShow: false
      });
    }
  }
});
