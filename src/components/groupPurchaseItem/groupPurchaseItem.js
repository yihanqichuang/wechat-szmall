/**
 * 秒杀列表 item
 */
Component({
  externalClasses: ['my-class'],
  properties: {
    item: {
      type: Object,
      observer(val) {
        if (val) this.initData()
      }
    },
    isLast: {
      type: Boolean,
      value: false
    },
    index: {
      type: Number || String
    },
    pathClass: {
      type: String,
      value: '../'
    },
    commodityId: {
      type: Number || String
    }
  },
  data: {
  
  },
  /* lifetimes: {
    attached() {
      this.initData()
    }
  },
  pageLifetimes: {
    show() {
      this.initData()
    }
  }, */
  methods: {
    initData() {
      let item = this.data.item
      item.cState = item.isJoin === 'Y' ? 'join' : item.commodityNum <= 0 ? 'sellout' : ''
      item.state = this.decideState(item.startTime, item.endTime)
      this.setData({
        item: item
      })
    },
    // 团购状态
    decideState(startTime, endTime) {
      let now = Date.now()
      startTime = new Date(startTime.replace(/-/g, '/')).getTime()
      endTime = new Date(endTime.replace(/-/g, '/')).getTime()
      if (now < startTime) {  // 未开始
        return 'un'
      } else if (now < endTime) {
        return 'ing'
      } else {
        return 'ed'
      }
    }
  }
})
