const app = getApp()
import WxPay from '../../utils/wxPay';
import OrderApi from '../../api/order';
import ProductApi from '../../api/commodity';
import Tip from '../../utils/tip';

Component({
  properties: {
    order: {
      type: Object,
      default: {}
    },
    flag: {
      type: String,
      default: ''
    }
  },
  observers: {
    order(nVal, oVal) {
      if (nVal) {
        this.groupCurrentOrder(nVal);
        if (nVal.orderState === 'UNPAY') {
          this.getOrderTime(nVal.orderTime);
        } else if (nVal.orderState === 'BALANCE_UNPAY') {
          if (nVal.orderDetail) {
            this.setData({
              countDownTime: nVal.orderDetail && nVal.orderDetail.length > 0 ? nVal.orderDetail[0].balanceEndTime : new Date().getTime()
            })
          } else {
            this.setData({
              countDownTime: nVal.details && nVal.details.length > 0 ? nVal.details[0].balanceEndTime : new Date().getTime()
            })
          }
        }
      }
    }
  },
  data: {
    currentOrder: null,
    showCenterDialog: false,
    countDownTime: 0,
    valueCard: null,
    prepaidPwdToken: null,
    stateList: {
      CLOSE: '已关闭',
      UNPAY: '待付款',
      PAID: '待发货',
      WAIT_RECEIVE: '待收货',
      FINISH: '已完成',
      MERCHANT_AUDIT: '退款中'
    },
    currentTime: new Date().getTime()
  },
  methods: {
    // 去开票
    makeInvoice(e) {
      let industryType = this.properties.order.organizeType;
      let orderNo = this.properties.order.organizeType === 'SuperMarket' ? this.data.currentOrder.orderNo : this.data.currentOrder.shopOrderNo;
      let orderTime = this.data.currentOrder.orderTime;
      let orderFee = this.data.currentOrder.actualPayPrice;
      let orderType = this.data.currentOrder.orderType;
      let goodsName = this.data.currentOrder.shopName;
      wx.navigateTo({
        url: `/pages/eleInvoice/makeInvoices/makeInvoices?orderNo=${orderNo}&orderFee=${orderFee}&orderTime=${orderTime}&orderType=${orderType}&goodsName=${goodsName}&industryType=${industryType}`
      })
    },
    // 重组订单
    groupCurrentOrder(order) {
      let orderTemp = order;
      let organizeList = app.globalData.organizeList;
      if (organizeList && organizeList.length > 0) {
        organizeList.forEach(organize => {
          if (organize.organizeId === order.organizeId) {
            orderTemp['organizeName'] = organize.organizeName;
            orderTemp['organizeLogo'] = organize.organizePicturePath;
          }
        })
      }
      if (order.orderDetail) {
        order.orderDetail[0].skuPic = order.orderDetail[0].skuPic ? order.orderDetail[0].skuPic : app.globalData.defaultConfig.defaultPic
        // 待支付订单结构
        orderTemp['details'] = order.orderDetail;
      } else {
        // 小订单结构
        orderTemp['details'] = order.details;
      }
      // 设置定金预售信息
      if (orderTemp.details && orderTemp.details.length > 0 && orderTemp.details[0].balanceEndTime) {
        orderTemp.balanceEndTime = orderTemp.details[0].balanceEndTime;
        orderTemp.balanceStartTime = orderTemp.details[0].balanceStartTime;
        orderTemp.deposit = orderTemp.details[0].deposit;
        orderTemp.balance = orderTemp.details[0].balance;
      }
      this.setData({
        currentOrder: orderTemp,
        currentTime: new Date().getTime()
      });
    },
    // 是否是超市
    isSuperMarket() {
      return this.properties.order.organizeType === 'SuperMarket'
    },

    // 刷新
    doRefresh() {
      this.triggerEvent('refresh');
    },

    // 计算订单剩余时间
    getOrderTime(startTime) {
      startTime = new Date(startTime.replace(/-/g, '/')).getTime();
      let closeOrderTime = wx.getStorageSync("closeOrderTime");
      const endTime = startTime + Number(closeOrderTime);
      this.setData({
        countDownTime: endTime
      });
    },

    // 跳转确认预约页面
    confirmAppointment(e) {
      let item = e.currentTarget.dataset.item;
      wx.setStorageSync('appointmentOrder', item);
      wx.navigateTo({
        url: '/pages/newPackage/confirmAppointment/confirmAppointment'
      })
    },
    // 删除订单
    deleteOrder() {
      wx.showModal({
        title: '确定删除订单？',
        success: (res) => {
          if (res.confirm) {
            if (this.isSuperMarket()) {
              // 是超市订单
              this.delMainOrder();
            } else {
              this.doDelete();
            }
          } else if (res.cancel) {
          }
        }
      })
    },
    // 删除大订单（删除超市订单）
    async delMainOrder() {
      Tip.loading()
      try {
        const result = await OrderApi.fetchDeleteMainOrder({
          orderNo: this.properties.order.orderNo
        })
        Tip.loaded()
        if (result.data.flag) {
          this.doRefresh()
        } else {
          Tip.toast(result.data.msg, null, 'none')
        }
      } catch (error) {
      }
    },

    doDelete() {
      Tip.loading();
      OrderApi.fetchDeleteOrder({
        shopOrderNo: this.properties.order.shopOrderNo
      }).then(res => {
        Tip.loaded();
        if (res.data.flag) {
          this.doRefresh();
        } else {
          Tip.toast(res.data.msg, null, 'none');
        }
      })
    },

    // 取消订单
    cancelOrder() {
      let title = '确定取消订单？';
      // 定金预售
      if (this.data.currentOrder.orderState === 'BALANCE_UNPAY' && this.data.currentOrder.details[0].balance) {
        title = '预售商品，取消订单，定金不退还，是否取消订单？';
      }
      wx.showModal({
        title: title,
        success: (res) => {
          if (res.confirm) {
            this.doCancel();
          } else if (res.cancel) {
          }
        }
      })
    },
    doCancel() {
      Tip.loading();
      OrderApi.fetchCancelOrder({
        orderNo: this.properties.order.orderNo
      }).then(res => {
        Tip.loaded();
        if (res.data.flag) {
          this.doRefresh();
        } else {
          Tip.toast(res.data.msg, null, 'none');
        }
      })
    },

    // 去支付
    async continuePay() {
      // 是否存在储值卡支付
      if (this.properties.order.prepaidPayPrice && this.properties.order.prepaidPayPrice > 0) {
        const res = await ProductApi.fetchIsCardPaySuccess({
          orderNo: this.properties.order.orderNo
        });
        if (res.flag && res.data) {
          // 储值卡已经支付完成，直接进行微信支付
          this.pullWechatPay();
        } else {
          const cardRes = await ProductApi.fetchValueCardInfo();
          if (cardRes.flag) {
            if (this.properties.order.prepaidPayPrice > cardRes.data.amount) {
              Tip.toast('储值卡余额不足', null, 'none');
            } else {
              this.setData({
                valueCard: cardRes.data,
                showCenterDialog: true
              });
            }
          }
        }
      } else {
        this.pullWechatPay();
      }
    },

    payment() {
      if (this.data.currentTime < this.data.currentOrder.balanceStartTime || this.data.currentOrder.balanceEndTime < this.data.currentTime) {
        return;
      }
      let url = '/pages/newPackage/balancePayment/balancePayment'
      wx.navigateTo({
        url: `${url}?orderNo=${this.properties.order.orderNo}`
      })
    },

    // 输入完密码,校验密码正确性
    handlePayPassWord(e) {
      const params = {
        cardNo: this.data.valueCard.cid,
        password: e.detail.value
      }
      ProductApi.fetchValidateCardPassword(params)
        .then(res => {
          if (res.flag) {
            if (res.data.ispwd === 'Y') {
              this.setData({
                prepaidPwdToken: res.data.token
              });
              this.pullWechatPay();
            } else {
              wx.showModal({
                title: '密码未设置',
                content: '您还未设置储值卡密码，是否立即前往设置？',
                confirmText: '立即前往',
                confirmColor: '#FB5B41',
                success: res => {
                  if (res.confirm) {
                    wx.navigateTo({
                      url: '/pages/newPackage/setPassword/setPassword'
                    })
                  }
                }
              })
            }
          } else {
            Tip.toast(res.data, null, 'none');
          }
        })
    },
    // 获取支付参数
    pullWechatPay() {
      Tip.loading();
      WxPay.getNewWxPayConfig(this.properties.order.orderNo, this.data.prepaidPwdToken)
        .then(res => {
          Tip.loaded();
          Tip.toast('支付完成', null);
          this.closePassword();
          this.doRefresh();
        })
        .catch(err => {
          Tip.loaded();
          if (typeof err === 'string') {
            Tip.toast(err, null, 'none');
          }
          this.closePassword();
        })
    },

    // 关闭密码框
    closePassword() {
      if (this.data.showCenterDialog) {
        this.setData({
          showCenterDialog: false
        });
      }
    },

    // 前往订单详情
    toOrderDetail() {
      let url = '/pages/newPackage/mallOrderDetail/mallOrderDetail'
      if (this.data.order.organizeType === "SuperMarket") {
        // 如果该订单是超市商品订单
        url = '/pages/newPackage/markOrderDetail/markOrderDetail'
      }
      if (this.data.order.orderState === 'UNPAY' || this.data.order.orderState === 'BALANCE_UNPAY') {
        wx.navigateTo({
          url: `${url}?type=${this.properties.order.orderState}&orderNo=${this.properties.order.orderNo}&flag=${this.properties.flag}`
        })
      } else {
        if (this.data.order.organizeType === 'SuperMarket') {
          wx.navigateTo({
            url: `${url}?type=${this.properties.order.orderState}&orderNo=${this.properties.order.orderNo}&flag=${this.properties.flag}`
          })
        } else {
          wx.navigateTo({
            url: `${url}?type=${this.properties.order.orderState}&orderNo=${this.properties.order.details[0].shopOrderNo}&flag=${this.properties.flag}`
          })
        }
      }
    }
  }
});