/**
 * Created by yu tao on 2019/7/10.
 */

Component({
  properties: {
    showCenterDialog: {
      type: Boolean,
      value: false
    },
    info: {
      type: Object,
      value: {}
    }
  },
  observers: {
    info: function() {
      if (this.properties.info && this.properties.info.perPoints) {
        this.setData({
          points: this.properties.info.perPoints,
          exchangeLeCoin: this.properties.info.perExchangeLeCoin,
          maxExchangeLeCoinNum: this.properties.info.maxExchangeLeCoinNum
        })
      }
    }
  },
  data: {
    messageComponent: '',
    points: 0,
    exchangeNum: 1,
    exchangeLeCoin: 0,
    maxExchangeLeCoinNum: 0
  },
  lifetimes: {
    attached() {
      this.messageComponent = this.selectComponent('.messageTips');
    }
  },
  pageLifetimes: {
    hide() {
      
    }
  },
  methods: {
    
    // 增加或减少 兑换数量
    changeNum(event) {
      if (this.data.maxExchangeLeCoinNum < this.data.exchangeNum) {
        return this.messageComponent.applyActive('您当前积分不足, 请重新输入');
      }
      let val = Number(event.currentTarget.dataset.val);  // plus-增加   minus-减少
      let exchangeNum = parseInt(this.data.exchangeNum) + parseInt(val);
      if (exchangeNum < 1) {
        // 减少 至少为 1
        return this.messageComponent.applyActive('兑换数量需大于0')
      } else {
        this.setData({ 
          exchangeNum: exchangeNum
        });
        this.sum()
      }
    },

    // 增加或减少 兑换数量
    changeInputNum(e) {
      var exchangeNum = e.detail.value;
      if (this.data.maxExchangeLeCoinNum < exchangeNum) {
        this.messageComponent.applyActive('您当前积分不足, 请重新输入')
        return {
          value: this.data.exchangeNum
        };
      }
      if (exchangeNum < 1) {
        // 减少 至少为 1
        return;
      } else {
        this.setData({ 
          exchangeNum: exchangeNum
        });
        this.sum()
      }
    },

    changeBlurNum(e) {
      var exchangeNum = e.detail.value;
      if (exchangeNum < 1) {
        this.setData({ 
          exchangeNum: 1
        });
        this.sum();
        return {
          value: this.data.exchangeNum
        };
      }
    },

    sum() {
      this.setData({
        points: this.data.info.perPoints * this.data.exchangeNum,
        exchangeLeCoin: this.data.info.perExchangeLeCoin * this.data.exchangeNum
      })
    },

    handleSubmit() {
      if (this.data.maxExchangeLeCoinNum < this.data.exchangeNum) {
        this.messageComponent.applyActive('您当前积分不足, 请重新输入')
        return;
      }
      this.triggerEvent('exchangeLeCoin', {
        exchangeNum: this.data.exchangeNum,
        perPoints: this.data.info.perPoints,
        perExchangeLeCoin: this.data.info.perExchangeLeCoin,
        points: this.data.points,
        exchangeLeCoin: this.data.exchangeLeCoin
      });
    },

    handleColse() {
      this.triggerEvent('closeLeCoin', {});
    }
  }
});
