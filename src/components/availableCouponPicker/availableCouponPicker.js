import ProductApi from '../../api/commodity'

Component({
  externalClasses: ['icon-close'],
  properties: {},
  data: {
    isShow: false,
    couponList: null,
    pickedCoupon: null,
    totalPrice: null
  },
  lifetimes: {
    attached() {
    }
  },
  methods: {
    // 打开优惠券选择
    showPicker(params, totalPrice) {
      this.setData({ isShow: true, totalPrice: totalPrice });
      this.getOrderAvailableCoupons(params)
    },

    // 关闭优惠券选择
    hidePicker() {
      this.setData({ isShow: false, couponList: null });
    },

    // 选择优惠券
    pickCoupon(e) {
      const coupon = e.currentTarget.dataset.coupon;
      this.setData({ pickedCoupon: coupon });
      this.triggerEvent('pickedCoupon', coupon);
      this.hidePicker();
    },

    // 不使用优惠券
    noUseCoupon() {
      this.setData({ pickedCoupon: null });
      this.triggerEvent('pickedCoupon', null);
      this.hidePicker();
    },

    // 获取当前订单可用券
    getOrderAvailableCoupons(params) {
      ProductApi.fetchOrderAvailableCoupons(params).then(res => {
        if (res.data.flag) {
          let filterCouponList = res.data.data;
          if (this.data.totalPrice && this.data.totalPrice != 0) {
            filterCouponList = res.data.data.filter(coupon => coupon.serviceAmount <= this.data.totalPrice);
          }
          this.setData({ couponList: filterCouponList });
        }
      })
    }
  }
});