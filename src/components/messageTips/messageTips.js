/**
 * 页面到底组件
 * @message     String  message文字提示
 */
// let Message = null
Component({
  properties: {
  
  },
  data: {
    Message: null,
    active: false,
    message: ''
  },
  methods: {
    applyActive(message, timeout = 2500) {
      if (this.data.Message) {
        clearTimeout(this.data.Message)
        this.setData({
          message: '',
          active: false
        })
      }
      this.setData({
        message,
        active: true
      })
      this.data.Message = setTimeout(() => this.setData({
        message: '',
        active: false
      }), timeout)
    }
  }
})
