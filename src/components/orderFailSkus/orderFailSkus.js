Component({
  externalClasses: ['icon-close'],
  data: {
    isShow: false,
    skuList: [],
    failType: 'STOCK_FAIL',
    marketType: ''
  },
  methods: {
    showDialog(skuList, type, marketType) {
      this.setData({ skuList, isShow: true, failType: type ,marketType});
    },
    hideDialog() {
      this.setData({ skuList: [], isShow: false });
    },
    // 知道了,继续支付
    continuePay() {
      this.hideDialog();
      this.triggerEvent('continuePay');
    },
    // 返回上一页
    goBack() {
      this.hideDialog();
      wx.navigateBack({ delta: 1 });
    },
    // 移除无库存商品
    removeNoStockSkus() {
      this.hideDialog();
      this.triggerEvent('removeAndPay');
    }
  }
});