/* eslint-disable */
/**
 * Created by yu tao on 2018/9/11.
 */

import {ProvinceCode, NumberCode} from  '../../utils/provinceCode';
const Number = ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9"];
Component({
  properties: {
    isShow: {
      type: Boolean,
      value: false,
    },
    isShowEnsure: {
      type: Boolean,
      value: false
    },
    buttonBorder: {
      type: String,
      value: "1px solid #ccc"
    },
    backgroundColor: {
      type: String,
      value: "#fff"
    },
    //1为省份键盘，其它为英文键盘
    keyBoardType: {
      type: Number,
      value: 1,
    },
    // 默认省份
    activeValue: {
      type: String,
      value: "鄂"
    }
  },
  data: {
    keyVehicle: ProvinceCode,
    keyNumber: NumberCode,
    carNumber: '',
    disabledNumber: false
  },
  methods: {
    // 点击数字
    vehicleTapNumber(e) {
      let value = e.currentTarget.dataset.value;
      this.triggerEvent('numberClick', value);
    },

    // 点击省份
    provinceNumber(e) {
      let value = e.currentTarget.dataset.value;
      this.triggerEvent('provinceClick', value);
    },

    // 点击删除按钮
    vehicleTapDelete() {
      this.triggerEvent('numberDelete', {});
    },
    
    // 点击确定按钮
    vehicleTapEnsure(e) {
      let value = e.currentTarget.dataset.value;
      this.triggerEvent('ensure', value);
    }
  }
});
