/**
 * 秒杀列表 item
 */
Component({
  properties: {
    item: {
      type: Object
    },
    index: {
      type: Number || String
    },
    pathClass: {
      type: String,
      value: '../'
    },
    commodityId: {
      type: Number || String
    },
    margin0: {
      type: Boolean,
      value: false
    }
  },
  data: {
  
  },
  methods: {
  
  }
})
