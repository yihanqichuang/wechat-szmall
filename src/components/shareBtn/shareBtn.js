/**
 * 页面到底组件
 * @homePage   Boolean   首页
 * @userPage   Boolean   我的
 * @sharePage  Boolean   转发
 */
import tip from '../../utils/tip.js';
import card from '../../utils/painterCard.js'
Component({
  imagePath: '',
  externalClasses: ['groups'],
  properties: {
    homePage: {
      type: Boolean,
      value: true
    },
    userPage: {
      type: Boolean,
      value: true
    },
    sharePage: {
      type: Boolean,
      value: true
    },
    height: {
      type: String,
      value: '100rpx'
    },
    bgHeight: {
      type: String,
      value: '854rpx'
    },
    bgWidth: {
      type: String,
      value: '570rpx'
    },
    bgTop: {
      type: String || Number,
      value: '40%'
    },
    info: {
      type: Object,
      value: {}
    },
    isShowPainter: {
      type: Boolean,
      value: false
    },
    type: {
      type: String,
      value: 'longCard'
    }
  },
  data: {
    messageComponent: '',
    showCenterDialog: false
  },
  methods: {
    // 转发
    goTransmit() {
      let memberType = wx.getStorageSync('memberType');
      if (memberType != 'MEMBER') {
        wx.navigateTo({
          url: `/pages/memberSign/memberSign`
        })
        return false
      }
      tip.loading('海报生成中...');
      let data = this.properties.info;
      if (!Object.keys(data).length) {
        tip.loaded();
        this.messageComponent.applyActive('生成海报失败,请重新生成~');
        return false;
      }
      this.createPainter(data).then(() => {
        console.log('绘制中...');
        this.setData({
          isShowPainter: true
        })
      });
    },

    // 生成海报
    createPainter(data) {
      return new Promise((resolve) => {
        if (this.properties.type == 'longCard') {
          card.langCard(this, data);
        } else if (this.properties.type == 'card') {
          card.card(this, data);
        } else if (this.properties.type == 'cardVote') {
          card.cardVote(this, data);
        } else if (this.properties.type == 'cardVoteShare') {
          card.cardVoteShare(this, data);
        }
        resolve();
      });
    },

    // 海报绘制成功
    onImgOK(e) {
      this.imagePath = e.detail.path;
      tip.loaded();
      this.setData({
        imagePath: e.detail.path,
        showCenterDialog: true
      })
    },

    // 海报绘制失败
    onImgErr(e) {
      console.log(e);
      tip.loaded();
      this.messageComponent.applyActive('生成海报失败,请重新生成~');
    },

    // 下载海报
    handleCreate() {
      let that = this;
      wx.saveImageToPhotosAlbum({
        filePath: this.imagePath,
        success() {
          that.messageComponent.applyActive('图片已保存到本地相册')
        }
      });
    }
  },
  pageLifetimes: {
    show() {
      this.messageComponent = this.selectComponent('.messageTips')
    }
  }
});
