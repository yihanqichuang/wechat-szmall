Component({
  properties: {},
  data: {
    isShow: false,
    selectedType: 'TODAY', // 选择日期的类型：'TODAY'、'TOMORROW'
    typeList: [{ typeName: '今天', type: 'TODAY' }, { typeName: '明天', type: 'TOMORROW' }],
    todayTimeList: [],// 今日可选时间列表
    tomorrowTimeList: [], // 明日可选时间列表
    selectedTime: null,// 选中的日期
    showDes: null,// 送达时间解释说明弹框显示flag
  },
  lifetimes: {
    attached() {
    }
  },
  methods: {
    // 打开送达时间解释说明弹框
    showDesDialog() {
      this.setData({
        showDes: true
      })
    },
    // 关闭送达时间解释说明弹框
    closeDesDialog() {
      this.setData({
        showDes: false
      })
    },
    // 打开picker
    showPicker(todayTimeList, tomorrowTimeList) {
      this.setData({
        isShow: true,
        todayTimeList: todayTimeList,
        tomorrowTimeList: tomorrowTimeList,
        selectedType: 'TODAY',
        selectedTime: null
      });
      if(!todayTimeList || (todayTimeList && todayTimeList.length == 0)) {
        // 判断今日时间列表是否为空
        this.setData({
          typeList: [{ typeName: '明天', type: 'TOMORROW' }],
          selectedType: 'TOMORROW',
        });
      } else if(!tomorrowTimeList || (tomorrowTimeList && tomorrowTimeList.length == 0)) {
        // 判断明日时间列表是否为空
        this.setData({
          typeList: [{ typeName: '今天', type: 'TODAY' }],
          selectedType: 'TODAY',
        });
      } else if (todayTimeList && todayTimeList.length > 0 && tomorrowTimeList && tomorrowTimeList.length > 0) {
        this.setData({
          typeList: [{ typeName: '今天', type: 'TODAY' }, { typeName: '明天', type: 'TOMORROW' }],
          selectedType: 'TODAY',
        });
      }
    },
    // 关闭picker
    hidePicker() {
      this.setData({ isShow: false });
    },
    // 选中类型
    clickType(e) {
      this.setData({
        selectedType: e.currentTarget.dataset.currenttype
      })
    },
    // 选中时间
    clickTime(e) {
      this.setData({
        selectedTime: e.currentTarget.dataset.currenttime
      })
      this.triggerEvent('setDeliveryTime', this.data.selectedTime)
    }
  }
});