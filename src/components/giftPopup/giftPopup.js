Component({
  properties: {
    giftsList: {
      type: Array,
      default: []
    }
  },
  data: {
    isShow: false
  },
  lifetimes: {
    attached () {
    }
  },
  methods: {
    // 打开picker
    showPicker() {
      this.setData({ isShow: true });
    },
    // 关闭picker
    hidePicker() {
      this.setData({ isShow: false });
    }
  }
});