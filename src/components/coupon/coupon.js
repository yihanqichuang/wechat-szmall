// 获取全局应用程序实例对象
const app = getApp();

Component({
  externalClasses: ['arrow-enter', 'arrow-down'],
  properties: {
    checked: {
      type: Boolean,
      default: false
    },
    showBtn: {
      type: Boolean,
      default: false
    },
    coupon: {
      type: Object,
      default: null
    }
  },
  data: {
    showMemo: false,
    logoImg: ''
  },
  // 生命周期函数 组件生命周期函数，在组件实例进入页面节点树时执行
  attached: function() {
    this.init();
  },
  methods: {
    // 显示备注
    toggleMemo() {
      this.setData({ showMemo: !this.data.showMemo });
    },
    init() {
      // this.setData({ logoImg: app.globalData.defaultConfig.defaultPic});
    }
  }
});