/**
 * Created by yutao on 2020/4/8.
 */

let lastY = 0 // 上一次滚动的位置
let scale = 750 / wx.getSystemInfoSync().windowWidth // rpx转化比例
Component({
  externalClasses: ['scroll-wrap'],
  options: {
    multipleSlots: true
  },
  data: {
    scrollTop: 0,
    translateHeight: 0, // 平移距离
    state: -1,
    emptyPage: false,
    bottomTipsFlag: true,
    bottomTipsText: '',
    loadMore: true,
    pending: false
  },
  properties: {
    // 触发下拉刷新的距离
    upperDistance: {
      type: Number,
      value: 50
    },
    page: {
      type: Object,
      value: {
        pageNum: 1,
        pageSize: 10,
        pages: 1,
        total: 0
      },
      observer: function(val) {
        if (val.total <= 0) {
          this.setData({
            emptyPage: true,
            bottomTipsFlag: true,
            loadMore: false
          })
        } else if (val.pageNum === val.pages && val.pageNum) {
          this.setData({
            emptyPage: false,
            bottomTipsFlag: false,
            bottomTipsText: '没有更多了~',
            loadMore: false
          })
        } else if (!val.pages || val.pageNum < val.pages) {
          this.setData({
            emptyPage: false,
            bottomTipsFlag: true,
            bottomTipsText: '',
            loadMore: true
          })
        } else {
          this.setData({
            emptyPage: false,
            bottomTipsFlag: true,
            bottomTipsText: '',
            loadMore: true
          })
        }
        this.setData({
          pending: false
        })
      }
    },
    scroll: {
      type: Boolean,
      value: true
    },
    emptyType: {
      type: String,
      value: ''
    },
    customClass: {
      type: String
    },
    // 是否开启下拉刷新
    isUpper: {
      type: Boolean,
      value: false
    },
    // 是否开启上拉加载
    isDown: {
      type: Boolean,
      value: true
    }
  },
  methods: {
    // 监听滚动，获取scrollTop
    onPageScroll(e) {
      this.data.scrollTop = e.scrollTop
    },

    touchStart(e) {
      lastY = e.touches[0].clientY
    },

    touchMove(e) {
      let clientY = e.touches[0].clientY
      let offset = clientY - lastY
      console.log(clientY);
      console.log(offset);
      if (this.data.scrollTop > 0 || offset < 0 || !this.data.isUpper) return
      this.data.translateHeight += offset
      this.data.state = 0
      lastY = e.touches[0].clientY
      if (this.data.translateHeight - this.data.scrollTop * scale > this.data.upperDistance) {
        this.data.state = 1
      }
      this.setData({
        translateHeight: this.data.translateHeight,
        state: this.data.state
      })
    },

    touchEnd() {
      if (this.data.translateHeight - this.data.scrollTop * scale > this.data.upperDistance) {
        this.setData({
          translateHeight: 50
        })
        this.triggerEvent('scrolltoupper')
        this.setData({
          state: 2
        })
      } else if (this.data.scrollTop <= 0) {
        this.stopRefresh()
      }
    },

    //  停止刷新
    stopRefresh() {
      this.setData({
        translateHeight: 0,
        state: -1
      }, () => {
        wx.pageScrollTo({
          scrollTop: 0,
          duration: 0
        })
      })
    },

    // 页面滚动到底部
    onReachBottom() {
      console.log('页面到底了')
      console.log(this.properties.page, this.data.pending, this.data.loadMore)
      if (this.data.pending || !this.data.loadMore) return
      this.setData({
        pending: true
      })
      if (!this.data.loadMore) return
      let pageNum = this.properties.page.pageNum + 1
      this.triggerEvent('loadmore', pageNum)
    }
  }
})
