/**
 * 空页面组件
 */
Component({
  // externalClasses: ['empty-pages'],
  properties: {
    organizeList: {
      type: Array,
      observer: function(val) {
        console.log(val)
        // let data = this.switchType(val);
        this.setData({
          organizeList: val
        })
      }
    },
    defaultChooseId: {
      type: Number || String,
      observer: function(val) {
        console.log("defaultChooseId",val)
        // let data = this.switchType(val);
        if (val) {
          this.setData({
            chooseId: val,
            theOrganizeId: val,
          });
        }

      }
    },
    defaultChooseName: {
      type:  String,
      observer: function(val) {
        console.log("defaultChooseName",val)
        // let data = this.switchType(val);
        if (val) {
          this.setData({
            chooseName: val,
          });
        }

      }
    }
    
  },
  data: {
    isshowChoose:false,
    chooseId:0,
    chooseName:"全部门店",
    organizeList:[]
  },
  methods: {
    openChoose() {
      this.setData({
        isshowChoose: !this.data.isshowChoose
      });
    },
    chooseOrganize(e) {
      console.log('e', e);
      let theOrganizeId= e.currentTarget.dataset.organizeid;
      if(theOrganizeId == 0) {
        theOrganizeId = null;
      }
      this.setData({
        chooseId: e.currentTarget.dataset.organizeid,
        chooseName: e.currentTarget.dataset.organizename,
        theOrganizeId: theOrganizeId,
        isshowChoose: false
      });
      let param= {
        pageNum: 1,
        organizeId: theOrganizeId
      }
      console.log(param);
      this.triggerEvent('chooseChange', param)
      // this.getList(1, this.data.theOrganizeId);
    },
  }
})
