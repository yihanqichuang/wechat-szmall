
Component({
  properties: {
    isClick: {
      type: Boolean,
      value: true
    },
    showDialog: {
      type: Boolean,
      value: false
    },
    title: {
      type: String,
      value: '标题'
    },
    content: {
      type: String,
      value: '内容'
    },
    btnSureText: {
      type: String,
      value: '确定',
      observer: function(val) {
        console.log(val);
        this.setData({
          btnSureText: val
        })
      }
    },
    btnCancelText: {
      type: String,
      value: '取消'
    },
    btnLength: {
      type: Number,
      observer: function(val) {
        console.log(val);
        this.setData({
          btnLength: val
        })
      }
    }
  },
  data: {
    btnLength: 1,
    btnSureText: '',
    inputVal: ''
  },
  methods: {
    // 背景是否能关闭
    onClickdiaCenterView() {
      if (this.properties.isClick) {
        return false
      }
      this.setData({
        showDialog: !this.data.showDialog
      });
    },

    // 关闭弹窗
    handleCloseDialog() {
      this.setData({
        showDialog: !this.data.showDialog,
        carNumber: ''
      });
      this.triggerEvent('handleClose', {}, {})
    },

    // 点击确定
    handleSubmit() {
      this.triggerEvent('handleSure', {}, {})
    },

    // 点击取消
    handleCancel() {
      this.triggerEvent('handleCancel', {}, {})
    }
  }
});
