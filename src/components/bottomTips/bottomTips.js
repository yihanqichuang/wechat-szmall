/**
 * 页面到底组件
 * @innerText     String  页面到底的文字提示
 * @modalHidden   Boolean 是否显示（默认不显示）
 */
Component({
  properties: {
    innerText: {
      type: String,
      value: 'OH 到底啦！'
    },
    modalHidden: {
      type: Boolean,
      value: true
    }
  },
  data: {
    // 这里是一些组件内部数据
  },
  methods: {}
})
