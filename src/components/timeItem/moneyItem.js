/**
 * 秒杀列表 item
 */
Component({
  properties: {
    item: {
      type: Object
    },
    marketType: {
      type: String
    },
    state: {
      type: String
    }
  },
  data: {
  
  },
  methods: {
  
  }
})
