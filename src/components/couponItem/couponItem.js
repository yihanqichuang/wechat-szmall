/**
 * 优惠券列表 item
 */
/* eslint-disable */
// 获取全局应用程序实例对象
const app = getApp();

Component({
  externalClasses: ['my-class'],
  properties: {
    // 优惠券每一列
    item: {
      type: Object,
      observer: function(newData, oldData) {
        if (newData != oldData) {
          this.init()
        }
      }
    },
    // 优惠券id
    couponId: {
      type: Number || String
    },
    // 优惠券cyId
    cyId: {
      type: Number || String
    },
    activityStartTime: {
      type: String
    }
  },
  data: {
    pending: false,
    messageComponent: ''
  },

  // 生命周期函数 组件生命周期函数，在组件实例进入页面节点树时执行
  attached: function() {
    this.init();
  },

  methods: {
    // init
    init() {
      this.messageComponent = this.selectComponent('.messageTips');
      let c = this.properties.item;
      c.name = c.couponTypeKind == 'PARKING'
        ? `${c.couponValue}小时-${c.couponName}` : c.couponTypeKind == 'DISCOUNT'
          ? `${c.couponValue}折-${c.couponName}` : c.couponTypeKind == 'GIFT'
          ? `${c.couponName}`
          : `${c.couponValue}元-${c.couponName}`;
      c.state = this.decideCouponState(c);
      c.couponImage = c.couponImage ? c.couponImage : app.globalData.defaultConfig.defaultPic;
      // c.useRange = this.useRange(c.couponTypeKind, c.voucherType, c.useRange, c.couponValue, c.serviceAmount, c.listCouponShop, c.limitType);
      if (c.activityStartTime) {
        this.activityStartTime = c.activityStartTime;
      }
      this.setData({
        item: c,
        activityStartTime: this.activityStartTime
       });
    },

    // 优惠券领取状态
    decideCouponState(data) {
      // 计算时间
      let state = this.checkStartTime(data)
      // 未开始
      if (state==1) return 'UNSTART';
      // 已结束
      if (state==2) return 'OVER';
      // 剩余数量小于等于0 则为‘已抢光’  balanceNum 剩余数量
      if (data.balanceNum <= 0) return 'rob';
      // 已领取
      // 限领数量>=会员领取的数量 || 每天领取的数量>= 每天限领的数量 || 领取状态为RECEIVE
      if (data.memberHaveFlag && data.memberHaveFlag === 'N') return 'received';
      return 'get'
    },

    // 优惠券使用说明
    // useRange(kind, type, useRange, couponValue, serviceAmount, listCouponShop, limitType) {
    //   let shop = limitType == 'ONE'
    //     ? `限${listCouponShop[listCouponShop.length - 1].shopName}`
    //     : limitType == 'ALL'
    //       ? `全部商户通用`
    //       : limitType == 'MAIN' ? '限' + wx.getStorageSync('organizeName') : `限指定商户`;
    //   if (kind == 'PARKING') {
    //     return `享受${couponValue}小时免费停车,${shop}`
    //   } else if (kind == 'DISCOUNT') {
    //     return `享受${couponValue}折优惠,${shop}`
    //   } else {
    //     return (serviceAmount === 0 ? '无门槛，' : serviceAmount ? '满' + serviceAmount + '元可用，' : '') + shop
    //   }
    // },

    // 活动时间判断
    checkStartTime(item){
      let now = Date.now()
      let startTime = item.activityStartTime == null ? '' : new Date(item.activityStartTime).getTime()
      if (item.activityStartTime && now < startTime) {  // 未开始
        return 1
      }
      let endTime = item.activityEndTime == null ? '' : new Date(item.activityEndTime).getTime()
      if (item.activityEndTime && now > endTime) {  // 已结束
        return 2
      }
      return 0
    }
  }
});
