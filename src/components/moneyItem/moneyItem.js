/**
 * 秒杀列表 item
 */
Component({
  properties: {
    item: {
      type: Object
    },
    state: {
      type: String,
      default: 'notice'
    }
  },
  data: {
  
  },
  methods: {
  
  }
})
