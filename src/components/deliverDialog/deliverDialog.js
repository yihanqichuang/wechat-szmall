/**
 * dialog
 * @isClick             Boolean  点击背景色是否关闭弹窗
 * @showCenterDialog    Boolean  是否显示（默认不显示）
 */
Component({
  properties: {
    deliveryObj: {
      type: Object,
      value: null
    },
    showDialog: {
      type: Boolean,
      value: false
    }
  },
  data: {
    // 这里是一些组件内部数据
  },
  methods: {
    toggleDialog() {
      this.setData({
        showDialog: !this.data.showDialog
      });
    }
  }
})
