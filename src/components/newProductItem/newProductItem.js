/**
 * 秒杀列表 item
 */
Component({
  externalClasses: ['my-class'],
  properties: {
    item: {
      type: Object
    },
    index: {
      type: Number || String
    },
    pathClass: {
      type: String,
      value: '../'
    },
    commodityId: {
      type: Number || String
    },
    mt0: {
      type: Boolean,
      value: false
    }
  },
  data: {
  
  },
  methods: {
  
  }
})
