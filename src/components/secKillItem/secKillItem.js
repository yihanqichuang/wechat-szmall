/**
 * 秒杀列表 item
 */
Component({
  externalClasses: ['my-class'],
  properties: {
    item: {
      type: Object
    },
    isLast: {
      type: Boolean,
      value: false
    },
    index: {
      type: Number || String
    },
    pathClass: {
      type: String,
      value: '../'
    },
    commodityId: {
      type: Number || String
    }
  },
  data: {
  
  },
  methods: {
  
  }
})
