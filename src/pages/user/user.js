// 获取全局应用程序实例对象
const app = getApp();
import {
	PHONE
} from '../../utils/regular.js';
import Tip from '../../utils/tip';
import wechat from '../../utils/wechat.js';
import userApi from '../../api/user.js';
import ProductApi from '../../api/commodity';
import { SHOWORDERCENTER } from '../../vendor.js';
import {
	fetchOrganizeList
} from '../../api/markets'

import {
	jdFetch
} from '../../utils/fetch.js'


// 创建页面实例对象
Page({
	/**
	 * 页面的初始数据
	 */
	data: {
		orderCount: {
			paidNum: 0,
			refundNum: 0,
			unPayNum: 0,
			waitReceiveNum: 0,
		},
		listRights: [],// 会员权限list
		userInfo: {},
		memberType: wx.getStorageSync('memberType'),
		memberInfo: {},
		messageComponent: '',
		couponNumber: 0,// 可使用票券的数量
		collectNumber: 0,// 收藏数量
		// 是否显示赚积分功能
		isShowSign: false,
		bindDialog: false,
		isError: 'N',
		codeBtnText: '获取验证码',
		isDisabled: false,
		time: 60,
		timer: '',
		memberPhone: '',
		phoneValue: '',
		codeValue: '',
		isCanBind: false,
		isLoading: false,
		isServiceShow: false,
		serverPhone: '',
		organizeName: '',
		organizeList: [],
		organizeModel: 0,
		showOrderCenter: false
	},

	// 联系客服
	toggleService() {
		this.setData({
			isServiceShow: !this.data.isServiceShow
		})
	},
	// 拨打电话方法
	handleCall() {
		wechat.makePhoneCall(this.data.serverPhone).then(() => {
			console.log('拨打成功')
		}).catch(() => {
			console.log('拨打失败')
		})
	},

	// 获取卡券数量
	getCouponCount() {
		userApi.getCouponCount({
			memberCouponVO: {}
		}).then(res => {
			if (res.flag) {
				this.setData({
					couponNumber: res.data
				});
			}
		})
	},
	// 获取收藏数量
	getCollectCount() {
		userApi.getCollectCount().then(res => {
			if (res.data.flag) {
				this.setData({
					collectNumber: res.data.data
				});
			}
		})
	},
	// 获取订单数量
	getCountOrder() {
		userApi.getCountOrder().then(res => {
			if (res.data.flag) {
				this.setData({
					orderCount: res.data.data
				});
			}
		})
	},

	// 敬请期待
	waitComplete() {
		Tip.toast('功能开发中，敬请期待', null, 'none');
	},

	// 进入卡券
	handleCouponEnter() {
		this.isLogin(function () {
			wx.navigateTo({
				url: '/pages/newPackage/myCouponNew/myCoupon'
			})
		});
	},

	// 进入我的兑换列表
	handleJumpExchangeList() {
		this.isLogin(function() {
			wx.setStorageSync('exchangeList', 'exchangeList');
			wx.navigateTo({
				url: '/pages/exchangeList/exchangeList'
			})
		});
	},

	// 进入我的收藏
	handleCollectEnter() {
		this.isLogin(function () {
			wx.navigateTo({
				url: '/pages/newPackage/collectList/collectList'
			})
		});
	},

	// 进入签到功能
	handleJumpSigned() {
		if (!this.data.isShowSign) {
			this.messageComponent.applyActive('该功能暂未开放~')
		} else {
			this.isLogin(function () {
				wx.navigateTo({
					url: '../signIn/signIn'
				})
			});
		}
	},

	// 进入会员信息
	handleJumpMemberInfo() {
		this.isLogin(function () {
			wx.navigateTo({
				url: '../menberInfo/menberInfo'
			})
		});
	},
	// 进入开票记录
	handleJumpInvoice() {
		this.isLogin(function () {
			wx.navigateTo({
				url: '../invoicedRecord/invoicedRecord'
			})
		});
	},

	// 进入完善个人信息
	handleJumpCompleteInfo() {
		this.isLogin(function () {
			wx.navigateTo({
				url: '../completeInfo/completeInfo'
			})
		});
	},
	// 进入我的标签
	handleJumpMyLabel() {
		this.isLogin(function () {
			wx.navigateTo({
				url: '../myLabel/myLabel'
			})
		});
	},

	// 进入我的消费记录
	handleJumpShopping() {
		this.waitComplete();
	},

	// 进入会与卡页面
	handleJumpCard() {
		this.isLogin(function () {
			wx.navigateTo({
				url: '/pages/memberCardRights/memberCardRights'
			})
		});
	},

	// 进入个人信息页面
	toUserInformation() {
		this.isLogin(function () {
			wx.navigateTo({
				url: '../userInformation/userInformation'
			})
		});
	},

	// 会员卡二维码
	handleMemberCardQrCode() {
		this.isLogin(function () {
			wx.navigateTo({
				url: '../memberCard/memberCardQrCode/memberCardQrCode'
			})
		});
	},

	// 进入余额
	handleBalance() {
		this.isLogin(function () {
			wx.navigateTo({
				url: '../balance/balance'
			})
		});
	},

	// 进入积分
	handleIntegral() {
		this.isLogin(function () {
			wx.navigateTo({
				url: '../integral/integral'
			})
		});
	},

	// 进入我的投票
	handleJumpVote() {
		this.isLogin(function () {
			wx.navigateTo({
				url: '../voteList/voteList'
			})
		});
	},

	// 进入我的报名
	handleJumpActivity() {
		this.isLogin(function () {
			wx.navigateTo({
				url: '../activityList/activityList'
			})
		});
	},

	// 进入我的报名/投票
	handleJumpActivityCenter() {
		this.isLogin(function () {
			wx.navigateTo({
				url: '../activityLists/activityLists'
			})
		});
	},

	// 进入预约订单列表
	handleAppoinementOrderList() {
		this.isLogin(function () {
			wx.navigateTo({
				url: '/pages/newPackage/appointmentOrders/appointmentOrders'
			})
		});
	},

	// 进入我的商场订单
	handleJumpOrderList(e) {
		let orderStatus = e.currentTarget.dataset.orderstatus;
		let url = '/pages/newPackage/mallOrders/mallOrders';
		if (orderStatus) {
			url = url + '?orderStatus=' + orderStatus
		}
		this.isLogin(function () {
			wx.navigateTo({
				url: url
			})
		});
	},

	// 进入我的超市订单
	handleJumpMarkOrderList() {
		this.isLogin(function () {
			wx.navigateTo({
				url: '/pages/newPackage/markOrders/markOrders'
			})
		});
	},

	// 进入我的兑换列表
	handleJumpExchangeList() {
		this.isLogin(function () {
			wx.setStorageSync('exchangeList', 'exchangeList');
			wx.navigateTo({
				url: '../exchangeList/exchangeList'
			})
		});
	},

	// 进入意见反馈
	handleJumpFeedBack() {
		this.waitComplete();
		// this.isLogin(function() {
		//   wx.navigateTo({
		//     url: '../feedBack/feedBack'
		//   })
		// })
	},

	// 进入电子发票
	handleJumpInvoice() {
		// this.waitComplete();
		this.isLogin(function () {
			wx.navigateTo({
				url: '../eleInvoice/eleInvoice'
			})
		});
	},

	// 进入邀请有礼列表
	inviteCourtesy() {
		this.isLogin(function () {
			wx.navigateTo({
				url: '/pages/newPackage/inviteCourtesy/inviteCourtesy'
			})
		});
	},

	// 领卡至微信卡包
	getWechatCard() {
		this.isLogin(function () {
			this.getAddCardParam().then(res => {
				this.wxAddCard(res)
			})
		});
	},

	// 获取微信卡券参数
	getAddCardParam() {
		return new Promise((resolve, reject) => {
			userApi.fetchGetAddCardParam().then(res => {
				res.data.flag && resolve(res.data);
				res.data.flag || reject(res.data)
			}).catch(() => reject())
		})
	},

	// 会员权益说明
	handleJumpMemberLevel() {
		this.isLogin(function () {
			wx.navigateTo({
				url: '../memberLevel/memberLevel'
			})
		});
	},

	// 进入地址管理
	handleJumpAddress() {
		this.isLogin(function () {
			wx.navigateTo({
				url: '/pages/newPackage/addressList/addressList'
			})
		});
	},


	// 进入停车记录
	handleJumpParkingRecords() {
		this.waitComplete();
		// this.isLogin(function () {
		// 	wx.navigateTo({
		// 		url: '/pages/parkingPayment/paymentRecord/paymentRecord'
		// 	})
		// });
	},

	// 获取所有的组织列表
	getOrganizeList() {
		let params = {}
		fetchOrganizeList(params).then(res => {
			console.log("所有组织列表:", res);
			let organizeList = [];
			for (let i = 0; i < res.data.data.length; i++) {
				let item = res.data.data[i];
				console.log(item)
				organizeList = organizeList.concat(item.organizeList)
			}

			this.setData({
				organizeList: organizeList
			})
			console.log(organizeList)
		})
	},

	// 进入分销商页面
	handleJumpAddShare() {
		if (wx.getStorageSync('memberType') !== 'MEMBER') {
			wx.navigateTo({
				url: '/pages/memberSign/memberSign'
			});
			return
		}
		// 判断之前有没有选择过商城
		let fxOrganize = wx.getStorageSync("fxOrganize");
		if (fxOrganize) {
			console.log(fxOrganize)
			let url = '';
			if (fxOrganize.is_distributor == '1') {
				url = '/pages/share/index/index';
			} else {
				this.getCurOrgInfo()
			}
			this.isLogin(function () {
				console.log(url)
				wx.navigateTo({
					url: url
				})
			});
		} else {
			console.log("之前没有分销的组织")
			this.showOrganizeModel()
		}
	},
	showOrganizeModel() {
		this.setData({
			organizeModel: !0
		})
	},
	closeOrganizeModel() {
		this.setData({
			organizeModel: 0
		})
	},
	getCurOrgInfo(organize) {
		if (organize) {
			wx.setStorageSync("fxOrganize", organize);
		}
		let jdShareInfo = wx.getStorageSync("jdShareInfo");
		jdFetch({
			url: 'passport/share',
			data: {
				user_id: jdShareInfo.user_id
			},
			showLoading: true
		}).then(res => {
			console.log("当前商城的分销信息:", res)
			let fxOrganize = organize || wx.getStorageSync("fxOrganize")
			wx.setStorageSync("organizeId" + fxOrganize.organizeId, res.data.is_distributor);
			wx.setStorageSync("parent_name", res.data.parent_name);

			console.log('res.code == 0 ?', res.code == 0)
			if (res.code == 0) {
				let url = '';
				if (res.data.is_level == 0) {
					wx.showToast({
						title: "该商城还未开启!",
						icon: "none",
						duration: 1500
					})
					return false;
				} else if (res.data.is_distributor == 0 || res.data.is_distributor == 2) {
					console.log("当前不是分销商或在审核中")
					url = '/pages/share/add-share/index';
				} else if (res.data.is_distributor == 1) {
					console.log("已经是分销商了")
					url = '/pages/share/index/index';
				}
				wx.navigateTo({
					url: url
				})
				this.closeOrganizeModel()
			} else {
				console.log(res.code)
			}
		}).catch(err => {
			wx.removeStorageSync("fxOrganize")
		})
	},
	chooseOrganize(e) {
		console.log(e)
		let organize = e.currentTarget.dataset.item;
		// let is_distributor = wx.getStorageSync("organizeId" + organize.id);

		// console.log("分销商??:", is_distributor)
		// // 判断之前是否存在过这个商城
		// if (is_distributor || is_distributor == 0 && is_distributor !== '') {
		// 	let url = '';
		// 	if (is_distributor == 0) {
		// 		url = '/pages/share/add-share/index';
		// 	} else if (is_distributor == 1) {
		// 		url = '/pages/share/index/index';
		// 	} else if (is_distributor == 2) {
		// 		console.log('之前缓存的分销信息是未通过,获取当前的组织分销信息')
		// 		this.getCurOrgInfo(organize)
		// 	}
		// } else {
		// 	console.log("之前并没有缓存过分销信息")
		// 	this.getCurOrgInfo(organize)
		// }
		this.getCurOrgInfo(organize)
	},
	wxAddCard(data) {
		console.log(data)
		wx.addCard({
			cardList: [{
				cardId: data.card_id,
				cardExt: JSON.stringify(data.cardExt)
			}],
			success(res) {
				console.log(res.cardList); // 卡券添加结果
			},
			fail(res) {
				console.log(res)
			}
		})
	},

	// getQueryMember 获取会员信息
	getQueryMember() {
		userApi.queryMember().then(res => {
			if (res.data.flag) {
				this.setData({
					memberInfo: res.data.data,
					isError: res.data.data.isError,
					memberPhone: res.data.data.memberPhone
				});
				// 设置会员权益list
				if (res.data.data.memberCardGradeDTO.listPrivilege && res.data.data.memberCardGradeDTO.listPrivilege.length > 0) {
					this.setData({
						listRights: res.data.data.memberCardGradeDTO.listPrivilege
					});
				}
				wx.setStorageSync('memberId', res.data.data.memberId);
				app.globalData.cardNumber = res.data.data.cardNumber;
				app.globalData.memberPhone = res.data.data.memberPhone;
			} else {
				let msg = res.data.msg || '请求数据失败';
				this.messageComponent.applyActive(msg)
			}
		}).catch();
	},
	// 是否登录
	isLogin(callback) {
		if (this.data.memberType !== 'MEMBER') {
			wx.navigateTo({
				url: `../memberSign/memberSign`
			})
		} else {
			callback();
		}
	},
	// 手机号是否匹配
	isNumberMatch() {
		if (this.data.isError === 'Y') {
			wx.hideTabBar();
			this.setData({
				bindDialog: true
			})
			return false;
		}
	},

	handleCloseBindDialog() {
		wx.showTabBar();
		this.setData({
			bindDialog: false,
			isLoading: false
		});
	},

	// 手机号码输入
	handlePhoneInput(e) {
		this.setData({
			phoneValue: e.detail.value
		});
		if (PHONE.test(this.data.phoneValue) && this.data.codeValue) {
			this.setData({
				isCanBind: true
			})
		} else {
			this.setData({
				isCanBind: false
			})
		}
	},

	// 验证码输入
	handleCodeInput(e) {
		this.setData({
			codeValue: e.detail.value
		});
		if (PHONE.test(this.data.phoneValue) && this.data.codeValue) {
			this.setData({
				isCanBind: true
			})
		} else {
			this.setData({
				isCanBind: false
			})
		}
	},

	// 获取验证码
	handleSendCode() {
		// 验证手机号
		if (!this.data.phoneValue) {
			this.messageComponent.applyActive('请输入手机号码');
			return false;
		} else if (!PHONE.test(this.data.phoneValue)) {
			this.messageComponent.applyActive('手机格式不正确');
			return false;
		} else if (this.data.phoneValue == this.data.memberPhone) {
			this.messageComponent.applyActive('请勿输入原有绑定的手机号码');
			return false;
		} else {
			this.getCode()
		}
	},

	// 发送验证码
	getCode() {
		let params = {
			memberPhone: this.data.phoneValue,
			code: wx.getStorageSync('hash')
		};
		userApi.getSecurityCode(params).then(res => {
			if (res.data.flag) {
				this.handleSuccess()
			} else {
				this.messageComponent.applyActive(res.data.msg);
				this.setData({
					isDisabled: false
				})
			}
		}).catch(err => {
			this.setData({
				isDisabled: false
			});
			this.messageComponent.applyActive(err)
		})
	},

	// 发送成功需要的倒计时
	handleSuccess() {
		let time = this.data.time;
		let timer;
		this.setData({
			isDisabled: true,
			codeBtnText: `${time}S后重新获取`
		});
		timer = setInterval(() => {
			if (time === 1) {
				this.setData({
					isDisabled: false,
					codeBtnText: '获取验证码'
				});
				clearInterval(timer);
				return
			}
			time = time - 1;
			this.setData({
				codeBtnText: `${time}S后重新获取`
			})
		}, 1000)
	},

	// 绑定手机号
	handleBindPhone() {
		// 验证手机号
		if (!this.data.phoneValue) {
			this.messageComponent.applyActive('请输入手机号码');
			return false;
		} else if (!PHONE.test(this.data.phoneValue)) {
			this.messageComponent.applyActive('手机格式不正确');
			return false;
		}

		// 验证码校验
		if (!this.data.codeValue) {
			this.messageComponent.applyActive('请输入验证码');
			return false;
		}

		let params = {
			memberPhone: this.data.phoneValue,
			validateCode: this.data.codeValue
		};
		this.setData({
			isLoading: true,
			isCanBind: false
		});
		userApi.againBindMemberPhone(params).then(res => {
			if (res.data.flag) {
				app.globalData.memberPhone = this.data.phoneValue;
				wx.setStorageSync('memberPhone', this.data.phoneValue);
				this.handleCloseBindDialog();
				this.getQueryMember()
				this.messageComponent.applyActive('重新绑定成功！');
			} else {
				this.messageComponent.applyActive(res.data.msg || '重新绑定失败');
				this.setData({
					isLoading: false,
					isCanBind: true
				})
			}
		}).catch(err => {
			console.log(err);
			this.setData({
				isLoading: false,
				isCanBind: true
			})
		})
	},

	// 进入我的拼团
	handleJumpMyGroup() {
		this.isLogin(() => {
			wx.navigateTo({
				url: '../newPackage/group/myGroup/myGroup'
			})
		});
	},

	onLoad() {
		// this.getOrganizeList()
		this.messageComponent = this.selectComponent('.messageTips')
	},

	onShow() {
		this.setData({
			memberType: wx.getStorageSync('memberType'),
			serverPhone: wx.getStorageSync('organizePhone'),
			organizeName: wx.getStorageSync('organizeName'),
			showOrderCenter: Boolean(SHOWORDERCENTER * 1)
		})
		this.getQueryMember()
		// app.changeInitDiscover(0)
		this.getCouponCount();
		this.getCollectCount();
		this.getCountOrder();
		let fxOrganize = wx.getStorageSync("fxOrganize");
		if (fxOrganize && fxOrganize !== '') {
			let is_distributor = wx.getStorageSync("organizeId" + fxOrganize.Id)
			if (is_distributor !== 1) {
				wx.removeStorageSync("fxOrganize");
				wx.removeStorageSync("organizeId" + fxOrganize.Id)
			}
		}
	}
});
