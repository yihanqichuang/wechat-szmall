// 获取全局应用程序实例对象
// const app = getApp()
import userApi from '../../api/user.js'
import timeFormat from '../../utils/timeFormat.js'
import Tips from '../../utils/tip'

// 创建页面实例对象
Page({
  /**
   * 页面的初始数据
   */
  data: {
    processList: [],
    title: 'integral',
    choosesFlag: false,
    recordListPage: [],
    recordListPages: [],
    balanceScore: 0,
    chooseList: [
      {
        label: '全部',
        changeType: ''
      },
      {
        label: '获取',
        changeType: 'INCREASE'
      },
      {
        label: '消费',
        changeType: 'DECREASE'
      }
    ],
    changeType: '',
    isDown: true,
    date: '',
    messageComponent: '',
    _date: '',
    label: '全部',
    time: '近半年',
    emptyType: 'points',
    showPointDetails: true,
    page: {
      pageNum: 1,
      pageSize: 20,
      total: 1
    }
  },
  // 获取所有会员等级积分list，并处理成长积分进度条
  getListCardLevel() {
    userApi.listCardLevel().then(res => {
      if (res.data.flag) {
        let listCardLevel = res.data.data;
        let tempProcessList = [];
        if (listCardLevel) {
          let progressWidth = (100 / listCardLevel.length).toFixed(2);
          listCardLevel.forEach(card => {
            let point0bj = {};
            point0bj.points = card.minScore;
            if (this.data.balanceScore >= card.minScore && this.data.balanceScore <= card.maxScore) {
              point0bj.percent = ((this.data.balanceScore - card.minScore) / (card.maxScore - card.minScore)).toFixed(2);
              point0bj.percent = point0bj.percent * 100;
              if (point0bj.percent > 100) {
                // 如果计算出来的百分比超过100则设置为100
                point0bj.percent = 100;
              }
              if (point0bj.percent < 0) {
                // 如果计算出来的百分比小于0则设置为0
                point0bj.percent = 0;
              }
            } else {
              if (this.data.balanceScore >= card.maxScore) {
                point0bj.percent = 100
              } else {
                point0bj.percent = 0
              }
            }
            point0bj.width = progressWidth + '%';
            tempProcessList.push(point0bj);
          });
          this.setData({
            processList: tempProcessList
          })
        }
      }
    }).catch(err => {
      console.log(err)
    })
  },



  // 选择筛选条件
  handleChoose() {
    this.data.choosesFlag = !this.data.choosesFlag;
    this.data.isDown = !this.data.isDown;
    this.setData({
      choosesFlag: this.data.choosesFlag,
      isDown: this.data.isDown
    })
  },

  // 时间选择
  bindDateChange(e) {
    this.setData({
      date: e.detail.value,
      _date: e.detail.value + '-01',
      time: timeFormat.dateFormatFilter(new Date(), 'yyyy-MM')
    });
    this.getIntegralData(this.data.changeType, this.data._date, 1)
  },

  // 获取/消费切换
  handleSwitchChoose(e) {
    this.setData({
      changeType: e.target.dataset.changetype,
      label: e.target.dataset.label,
      choosesFlag: false,
      isDown: !this.data.isDown
    });
    this.getIntegralData(this.data.changeType, this.data._date, 1)
  },

  // 获得积分列表
  getIntegralData(changeType, date, pageNum, isLoading = true) {
    let params = {
      changeType: changeType,
      pageNum: pageNum,
      pageSize: 20,
      isNeedPage: true,
      createTimeStr: date
    };
    if (isLoading) Tips.loading();
    userApi.pointsRecord(params).then(res => {
      if (isLoading) Tips.loaded();
      // 每次有新的请求清空需要渲染的数组
      if (res.data.flag) {
        this.setData({
          recordListPages: []
        });
        if (pageNum === 1) this.setData({ recordList: [] });
        this.setData({
          recordList: [...this.data.recordList, ...res.data.data.pointsChangeList],
          balanceScore: res.data.data.balanceScore ? res.data.data.balanceScore : 0,//res.data.data.balanceScore ? res.data.data.balanceScore : 0,
          csScore: res.data.data.csScore ? res.data.data.csScore : 0,
          bhScore: res.data.data.bhScore ? res.data.data.bhScore : 0,
          page: res.data.page
        })
        this.getListCardLevel();
      } else {
        this.messageComponent.applyActive(res.data.msg)
      }
    }).catch((err) => {
      if (isLoading) Tips.loaded();
      console.log(err)
      this.messageComponent.applyActive('系统繁忙，稍后再试~')
    })
  },

  // 加载更多
  loadMore(e) {
    this.getIntegralData(this.data.changeType, this.data._date, e.detail, false)
  },

  // 获取月份如果是本月
  forMatMonth(date) {
    if (!date) return;
    let now = timeFormat.dateFormatFilter(new Date(), 'yyyy-MM');
    let _date = timeFormat.dateFormatFilter(new Date(date), 'yyyy-MM');
    if (now == _date) {
      return '本月'
    } else {
      return date
    }
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad() {
    this.messageComponent = this.selectComponent('.messageTips')
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {
    let now = new Date();
    let date = timeFormat.dateFormatFilter(now, 'yyyy-MM');
    let time = timeFormat.dateFormatFilter(now, 'yyyy-MM');
    this.setData({
      date: date,
      time: time,
      _date: '',
    });
    this.getIntegralData(this.data.changeType, '', this.data.page.pageNum)
  }
});
