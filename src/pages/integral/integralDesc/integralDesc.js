// 获取全局应用程序实例对象
// const app = getApp()
import WxParse from '../../../wxParse/wxParse.js'
import userApi from '../../../api/user.js'
// 创建页面实例对象
Page({
  /**
   * 页面的初始数据
   */
  data: {
    title: 'memberLevel',
    cardDescPoints: ''
  },

  // 获取积分说明
  getMemberCardDesc() {
    userApi.getMemberCardDesc().then(res => {
      if (res.data.flag) {
        this.setData({
          cardDescPoints: res.data.data.cardDescPoints
        });
        let _this = this;
        let article = res.data.data.cardDescPoints;
        WxParse.wxParse('article', 'html', article, _this, 15);
      } else {
        this.messageComponent.applyActive(res.data.msg)
      }
    })
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad() {
    // TODO: onLoad
    this.messageComponent = this.selectComponent('.messageTips');
    this.getMemberCardDesc();
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {
    // TODO: onReady
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {
    // TODO: onShow
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {
    // TODO: onHide
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {
    // TODO: onUnload
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {
    // TODO: onPullDownRefresh
  }
})
