// 获取全局应用程序实例对象
// const app = getApp()
import userApi from '../../../api/user.js'
import QR from '../../../utils/wxqrcode.min'
import wxtemplate from '../../../api/wxtemplate'
import Tips from '../../../utils/tip'

// 创建页面实例对象
Page({
  /**
   * 页面的初始数据
   */
  data: {
    title: 'memberLevel',
    messageComponent: '',
    showDialog: false,
    info: {}
  },

  // 获取配置
  queryConfig() {
    userApi.queryConfig().then(res => {
      if (res.data.flag) {
        this.setData({
          info: res.data.data
        })
      } else {
        this.messageComponent.applyActive(res.data.msg)
      }
    }).catch(() => {
      this.messageComponent.applyActive('系统繁忙')
    })
  },

  // 提交表单获取formId
  formSubmit(e) {
    this.setData({ formId: e.detail.formId });
    wxtemplate.formSubmit(e).then(res => {
      this.setData({ formId: res });
      this.handleClick()
    }).catch(() => {
      console.log('formId undefined');
      this.handleClick()
    })
  },

  // 点击按钮
  handleClick() {
    let info = this.data.info;
    let memberType = wx.getStorageSync('memberType');
    if (memberType !== 'MEMBER') {
      wx.navigateTo({
        url: `/pages/memberSign/memberSign`
      });
      return false;
    }
    if (info.clickEventsType == 'QRCODE') {
      // 获取要生成二维码数据
      this.getQrcode();
    } else {
      // 获取要生成二维码数据
      this.getQrcode();
    }
  },

  // 获取生成二维码接口
  getQrcode() {
    let params = {
      formId: this.data.formId
    };
    Tips.loading();
    userApi.getQrcode(params).then(res => {
      Tips.loaded();
      if (res.data.flag) {
        this.creatQrCode(res.data.data.serialNumber);
        this.setData({
          showDialog: true,
          codeInfo: res.data.data
        });
      } else {
        this.messageComponent.applyActive(res.data.msg)
      }
    }).catch(() => {
      this.messageComponent.applyActive('系统繁忙')
    })
  },

  // 生成二维码
  creatQrCode(pickupCode) {
    let codeImage = QR.createQrCodeImg(pickupCode);
    this.setData({
      codeImage: codeImage
    });
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad() {
    this.messageComponent = this.selectComponent('.messageTips');
    this.queryConfig();
  }
});
