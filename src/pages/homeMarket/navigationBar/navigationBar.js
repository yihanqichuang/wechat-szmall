const app = getApp()
Component({
    properties: {
        // defaultData（父页面传递的数据）
        defaultData: {
            type: Object,
            value: {
                title: "我是默认标题"
            },
            observer: function(newVal, oldVal) {}
        },
        organizeName: {
            type: String,
            value: '',
            observer: function (val) {
                console.log(val)
                // let data = this.switchType(val);
                this.setData({
                    organizeName: val || wx.getStorageSync('organizeName')
                })
            }
        }
    },
    data: {
        navBarHeight: app.globalData.navBarHeight,
        menuRight: app.globalData.menuRight,
        menuBotton: app.globalData.menuBotton,
        menuHeight: app.globalData.menuHeight,
        organizeName: wx.getStorageSync('organizeName'),
        industryType: 'GeneralMerchandise',
    },
    attached: function() {

    },
    methods: {
        
    }
})