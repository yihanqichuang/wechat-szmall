// 获取全局应用程序实例对象
const app = getApp();
import api from '../../api/home.js';
import loginApi from '../../api/login.js';
import tip from '../../utils/tip';
const Wechat = require('../../utils/wechat.js');
// import commodity from '../../api/commodity'
let pageDataLoading = false; // 页面数据是否加载中...

// 创建页面实例对象
var timer = null;
Page({
    data: {
        industryType: 'GeneralMerchandise',
        params: {
            page: {
                pageNum: 1,
                pageSize: 10,
            },
            moduleCode: 'SPECIAL'
        },
        page: {
            pageNum: 1,
            pageSize: 10,
            total: 1
        },
        lists: [],
        list: [],
        BANNER: [],
        FUNCTION: [],
        COUPON: [],
        GIFT: [],
        ACTIVITY: [],
        SECKILL: [],
        GROUPON: [],
        SHOP: [],       // 品牌推荐模块数据
        CATEGORY: [],   // 品类推荐模块数据
        THEME: [],      // 主题模块列表数据
        ADVERT_ONE: [],
        ADVERT_TWO: [],
        TIMER: null,
        FULL: [],
        SECOND: [],// 第二件数组
        GROUP_BARGAIN: [],  // 拼团模块列表数据
        PACKAGE_DISCOUNT: [],// 优惠套餐数组
        pending: false,
        formId: '',
        templateData: {},
        pagePath: '',
        showDialog: false,
        memberCardUpgradeInfo: [],
        couponNumStatus: false,
        giftNumStauts: false,
        couponGiftAll: false,
        memberCardName: '',
        couponNum: '',
        giftNum: '',
        countDownNum: '',
        windowDialog: false,
        currencyDialog: false,
        homeWindowData: [],
        countDownList: [],
        startTimeList: [],
        endTimeList: [],
        PopulState: false,
        couponState: false,
        sigupState: false,
        voteState: false,
        grouponState: false,
        seckillState: false,
        activeStart: true,
        startDesc: '',
        timer: null,
        datalist: [],
        listState: false,
        showPriceTxt: '',
        interval: 3000,  // 轮播时间间隔
        categoryList: [],
        activeIndex: 0,
        emptyPage: false,
        bottomTipsFlag: true,
        bottomTipsText: '',
        loadMore: true,
        isSpecial: false,
        specialName: '爆款专区',
        returnTopShow: false,
        isTipShow: true,
        lastPullDownTime: 0,
        organizeName: wx.getStorageSync('organizeName'),
        organizeAddress: wx.getStorageSync('organizeAddress'),
        defaultLogo: '',
        oneFixed: "",
        organizeId: ''
    },

    // 跳转页面(功能入口控制以及会员非会员控制)
    handleJumpToPage(e) {
        let memberType = wx.getStorageSync('memberType');
        let item = e.currentTarget.dataset.item;
        // 类型（如会员码、停车缴费）
        let type = item.functionCode;
        // 伪组织id (可2个组织共用一个停车缴费)
        let theOrganizeId = item.theOrganizeId;
        // 页面路径
        let path = item.page
        let useType = item.useType
        let isSwitchTab = this.getQueryString('isSwitchTab', item.page);
        let tabbar = this.getQueryString('tabbar', item.page);
        // 页面路径为空代表功能尚未开发完成
        if (!path) {
            tip.toast('功能开发中，敬请期待', null, 'none');
            return
        }
        // 是否需要会员才能进入
        let isMember = item.isMember;
        if (isMember == 'Y' && memberType != 'MEMBER') {
            path = '/pages/memberSign/memberSign'
            wx.navigateTo({ url: path })
        } else {
            if (useType == 'COMMON') {
                if (path.indexOf('/') != -1) {
                    if (isSwitchTab == 'Y') wx.switchTab({ url: path.split('?')[0] })
                    if (tabbar) app.changeInitDiscover(1)
                    if (type == 'PARKING') path = `${path}?theOrganizeId=${theOrganizeId}`
                    wx.navigateTo({ url: path })
                } else {
                    wx.navigateToMiniProgram({
                        appId: path,
                        envVersion: 'release',
                        success(res) {
                            // 打开成功
                            console.log(res)
                        }
                    })
                }
            } else if (useType == 'CATEGORY') {
                if (isSwitchTab == 'Y') wx.switchTab({ url: path.split('?')[0] })
                if (tabbar) app.changeInitDiscover(1)
                if (type == 'PARKING') path = `${path}?theOrganizeId=${theOrganizeId}`
                if (path.indexOf('/pages/') == -1) { path = '/' + path }
                wx.navigateTo({ url: path })
            } else {
                if (item.appId) {
                    wx.navigateToMiniProgram({
                        appId: item.appId,
                        path: path,
                        success(res) {
                            // 打开成功
                            console.log(res)
                        }
                    })
                } else {
                    if (isSwitchTab == 'Y') wx.switchTab({ url: path.split('?')[0] })
                    if (tabbar) app.changeInitDiscover(1)
                    if (type == 'PARKING') path = `${path}?theOrganizeId=${theOrganizeId}`
                    wx.navigateTo({ url: path })
                }
            }
        }
    },
    // banner跳转页面(功能入口控制)
    handleBannerJumpToPage(e) {
        let item = e.currentTarget.dataset.item;
        // 页面路径
        let advertUrl = item.advertUrl
        let appId = item.appid
        let pagePath = item.pagepath
        if (advertUrl) {
            wx.navigateTo({ url: advertUrl })
        } else {
            if (appId) {
                wx.navigateToMiniProgram({
                    appId: appId,
                    path: pagePath,
                    envVersion: 'release',
                    success(res) {
                        // 打开成功
                        console.log(res)
                    }
                })
            }
        }
    },
    // 获取路径参数
    getQueryString: function (name, url) {
        let reg = new RegExp('(^|&)' + name + '=([^&]*)(&|$)', 'i');
        let r = null;
        if (url && url.indexOf('?') != -1) {
            let query = url.split('?')[1];
            r = query.match(reg);
            if (r != null) {
                return unescape(r[2]);
            } else {
                return null;
            }
        } else {
            return null;
        }
    },

    // 获取首页弹窗配置
    getDialogConfig() {
        api.memberCardUpgradeInfo().then(res => {
            if (res.data.data && res.data.data.length > 0) {
                this.setData({
                    memberCardName: res.data.data[0].memberCardName,
                    couponNum: res.data.data[0].couponNum,
                    giftNum: res.data.data[0].giftNum
                });
                if (this.data.couponNum) {
                    if (this.data.giftNum) {
                        this.setData({
                            couponGiftAll: true,
                            couponNumStatus: false,
                            giftNumStauts: false,
                            showDialog: true
                        });
                    } else {
                        this.setData({
                            couponNumStatus: true,
                            couponGiftAll: false,
                            giftNumStauts: false,
                            showDialog: true
                        });
                    }
                } else if (this.data.giftNum) {
                    this.setData({
                        couponNumStatus: false,
                        couponGiftAll: false,
                        giftNumStauts: true,
                        showDialog: true
                    });
                } else if (!this.data.giftNum && this.data.memberCardName) {
                    this.setData({
                        couponNumStatus: false,
                        couponGiftAll: false,
                        giftNumStauts: false,
                        showDialog: true
                    });
                }
            } else { // degradeStatus 有值就说明有降级提示
                res.data.degradeStatus && tip.confirm('您的卡等级发生变动，请留意权益变更！', {}, '温馨提示', '', '确定').then(() => { })
            }
        })
    },
    // 关闭注册成功弹窗
    handleCloseDialog() {
        this.setData({
            showDialog: false
        });
    },

    // 跳转
    toExchangeList() {
        if (this.data.couponNum && this.data.giftNum) {
            wx.switchTab({
                url: '/pages/user/user'
            });
            this.setData({
                showDialog: false
            });
        } else if (this.data.giftNum) {
            wx.navigateTo({
                url: '/pages/exchangeList/exchangeList'
            });
            this.setData({
                showDialog: false
            });
        } else if (this.data.couponNum) {
            wx.navigateTo({
                url: '/pages/newPackage/myCouponNew/myCoupon'
            });
            this.setData({
                showDialog: false
            });
        } else if (this.data.memberCardName) {
            wx.navigateTo({
                // url: '/pages/memberCardBinBin/memberCardBinBin'
                // url: '/pages/memberCardMuDu/memberCard'
                url: '${memberCardPath}' || '/pages/memberCard/memberCard'
            });
            this.setData({
                showDialog: false
            });
        }
    },

    // 首页弹窗
    homeWindow() {
        let eventActionId = wx.getStorageSync('memberType') !== 'MEMBER' ? 2 : 3
        api.windowConfig({
            eventActionId: eventActionId
        }).then(res => {
            console.log('首页弹窗数据：', res);
            if (res.data.data) {
                this.setData({
                    datalist: res.data.data
                });
                // this.data.datalist.forEach(item => {
                //     item.checked = false
                // });
                if (this.data.datalist.length) {
                    this.windowList(this.data.datalist[0], 0);
                    this.clickCount(this.data.datalist[0].id)
                };
                // if (!this.data.listState) {
                //     if (this.data.datalist[2]) {
                //         this.windowList(this.data.datalist[2]);
                //         this.clickCount(this.data.datalist[2].id)
                //     }
                // } else {
                //     if (this.data.datalist[1]) {
                //         this.windowList(this.data.datalist[1]);
                //         this.clickCount(this.data.datalist[1].id)
                //     }
                // }
            }
        })
    },

    windowList(datalist, index) {
        if (datalist) {
            this.selectComponent('#commonDialog').showDialog({
                homeWindowData: datalist,
                countDownNum: datalist.timedClose,
                showDialog: true,
                startTimeList: '',
                endTimeList: '',
                popupWindowsIndex: index
            });
            //  通用
            // if (datalist.popupType == 'COMMON' || datalist.popupType == 'SPECIFY') {
            //     this.selectComponent('#commonDialog').showDialog({
            //         homeWindowData: datalist,
            //         countDownNum: datalist.timedClose,
            //         showDialog: true,
            //         startTimeList: '',
            //         endTimeList: '',
            //         popupWindowsIndex: index
            //     });
            //     //  优惠券
            // } else if (datalist.moduleType == 'COUPON') {
            //     this.selectComponent('#marketingDialog').showDialog({
            //         homeWindowData: datalist,
            //         showDialog: true,
            //         countDownNum: datalist.timedClose,
            //         sigupState: false,
            //         couponState: true,
            //         // PopulState: false,
            //         // voteState: false,
            //         startTimeList: '',
            //         endTimeList: '',
            //         // seckillState: false,
            //         // grouponState: false,
            //         popupWindowsIndex: index
            //     });
            //     // 活动报名
            // } else if (datalist.moduleType == 'SIGNUP') {
            //     this.selectComponent('#marketingDialog').showDialog({
            //         homeWindowData: datalist,
            //         startTime: this.timeFormatSuB(datalist.object.startTime),
            //         endTime: this.timeFormatSuB(datalist.object.endTime),
            //         showDialog: true,
            //         startTimeList: datalist.object.startTime,
            //         endTimeList: datalist.object.endTime,
            //         countDownNum: datalist.timedClose,
            //         sigupState: true,
            //         couponState: false,
            //         popupWindowsIndex: index
            //     });
            //     // 问卷调查
            // } else if (datalist.moduleType == 'QUESTION') {
            //     this.selectComponent('#marketingDialog').showDialog({
            //         homeWindowData: datalist,
            //         startTime: this.timeFormatSuB(datalist.object.startTime),
            //         endTime: this.timeFormatSuB(datalist.object.endTime),
            //         showDialog: true,
            //         startTimeList: datalist.object.startTime,
            //         endTimeList: datalist.object.endTime,
            //         countDownNum: datalist.timedClose,
            //         sigupState: false,
            //         couponState: false,
            //         questionState: true,
            //         popupWindowsIndex: index
            //     });
            //     // 限时折扣
            // } else if (datalist.moduleType == 'TIMER') {
            //     this.selectComponent('#marketingDialog').showDialog({
            //         homeWindowData: datalist,
            //         startTime: this.timeFormatSuB(datalist.object.startDate),
            //         endTime: this.timeFormatSuB(datalist.object.endDate),
            //         showDialog: true,
            //         startTimeList: datalist.object.startDate,
            //         endTimeList: datalist.object.endDate,
            //         countDownNum: datalist.timedClose,
            //         sigupState: false,
            //         couponState: false,
            //         questionState: false,
            //         timerState: true,
            //         popupWindowsIndex: index
            //     });

            // }
        }
    },
    timeFormatSuB(time) {
        return time.substr(0, 10)
    },
    showNext(e) {
        // let popupWindowsIndex = e.detail.popupWindowsIndex;
        // if (popupWindowsIndex < (this.data.datalist.length - 1)) {
        //     popupWindowsIndex++;
        //     this.windowList(this.data.datalist[popupWindowsIndex], popupWindowsIndex);
        //     this.clickCount(this.data.datalist[popupWindowsIndex].id);
        // }
    },
    // 判断商品的价格显示
    judgePrice(listObj) {
        let obj = listObj.object
        this.data.showPriceTxt = obj.payType == 'MIXED'
            ? obj.points + '积分+' + obj.nowPrice + '元'
            : obj.payType == 'ALLPOINTS'
                ? obj.points + '积分'
                : '¥' + obj.nowPrice;
        this.setData({
            showPriceTxt: this.data.showPriceTxt
        })
    },
    //  通用 按钮指定跳转
    handleNavigator(e) {
        let navigate = e.currentTarget.dataset.navigate;
        let clickEventType = e.currentTarget.dataset.clickeventtype;
        let externalMiniappAppid = e.currentTarget.dataset.externalminiappappid;
        let externalMiniappUrl = e.currentTarget.dataset.externalminiappurl;
        if (clickEventType === 'CLOSE') {
            this.setData({
                currencyDialog: false
            })
        } else if (clickEventType === 'INNERMINIAPP') {
            if (navigate.indexOf('isSwitchTab=Y') != -1) {
                wx.switchTab({
                    url: navigate
                });
                this.setData({
                    currencyDialog: false
                })
            } else {
                wx.navigateTo({
                    url: navigate
                })
            }
        } else if (clickEventType === 'TOOTHERMINIAPP') {
            wx.navigateToMiniProgram({
                appId: externalMiniappAppid,
                path: externalMiniappUrl,
                extraData: {
                    foo: 'bar'
                },
                envVersion: 'develop',
                success() { }
            })
        }
        if (this.data.datalist[0].timedClose) {
            clearTimeout(this.data.timer);
        }
        this.setData({
            currencyDialog: false
        });
        setTimeout(() => {
            if (this.data.datalist[1] && !this.data.datalist[1].checked) {
                this.windowList(this.data.datalist[1])
                this.clickCount(this.data.datalist[1].id)
                this.data.datalist[1] = ''
            }
        }, 500);
    },
    clickCount(id) {
        let param = {
            organizeId: wx.getStorageSync('organizeId'),
            popupId: id,
        };
        api.saveClickLog(param).then(() => { })
    },


    // 初始化业态分类
    initIndustryType() {
        let params = {
            commodityType: 'SPECIAL'
        }
        let categoryList = [];
        categoryList.push({
            categoryId: 0,
            categoryName: '全部'
        });
        api.queryCategoryList(params).then(res => {
            if (!res.data.flag) return;
            this.setData({
                categoryList: [...categoryList, ...res.data.data],
                activeIndex: 0
            });
        });
    },

    // 获取 列表数据
    getList(params, type) {
        let commodityList = [];
        api.find(params).then(res => {
            console.log('人气单品列表：', res);
            if (!res.data.flag) {
                return
            }
            if (type === 'list') {
                commodityList = [...this.data.list, ...res.data.data];
            } else {
                commodityList = [...res.data.data];
            }

            this.setData({
                list: commodityList,
                page: res.data.page,
                loadMore: false
            })
        })
    },

    // 选择标签
    chooseCategory(e) {
        let categoryId = e.currentTarget.dataset.item.categoryId;
        let activeIndex = this.data.activeIndex;
        if (activeIndex === categoryId) return
        let params = this.data.params;
        params.pageNum = 1;
        params.categoryId = categoryId;
        this.setData({
            activeIndex: categoryId,
            params,
            'params.page.pageNum': 1,
            emptyPage: false,
            bottomTipsFlag: true,
            bottomTipsText: '',
            loadMore: true,
            pending: false
        })
        this.getList(this.data.params, 'industry');
    },

    // 初始化页面
    initPage() {
        this.setData({
            list: [],
            page: {},
            params: {
                page: {
                    pageNum: 1,
                    pageSize: 10
                },
                moduleCode: 'SPECIAL'
            }
        })
        this.getList(this.data.params, '')
    },

    // 获取滚动条当前位置
    onPageScroll(e) {
        if (e.scrollTop > 1200) {
            if (!this.data.returnTopShow) {
                this.setData({
                    returnTopShow: true
                });
            }
        }
        if (e.scrollTop == 0 && this.data.returnTopShow) {
            this.setData({
                returnTopShow: false
            });
        }
        let that = this;
        //获取元素节点的位置信息
        const query = wx.createSelectorQuery().in(this)
        //小程序BUG不加延时算出来的高度部分机型不准确，目前官方没有给更好的解决方案
        setTimeout(() => {
            query.select('#goodsList').boundingClientRect(function (res) {

                if (res.top < 45) {
                    that.setData({ oneFixed: "fixed" })
                } else {
                    that.setData({ oneFixed: '' })
                }
            }).exec();
        }, 0);

    },

    // 返回顶部方法
    returnTop() {
        if (wx.pageScrollTo) {
            wx.pageScrollTo({
                scrollTop: 0,
                duration: 150
            })
        } else {
            tip.toast('当前微信版本过低，无法使用该功能', null, 'none');
        }
        this.setData({ returnTopShow: false });
    },

    // 关闭顶部提示栏
    closeTip() {
        wx.setStorageSync('isTipShow', false);
        this.setData({ isTipShow: false })
    },

    // 常规模块展示信息获取
    getNormalModule(item) {
        api.find({ moduleId: item.id, moduleCode: item.moduleCode }).then(r => {
            console.log(`${item.moduleCode}模块数据：`, r);
            let obj = {};
            let name = item.moduleCode;
            let tokenKey = r.data.submit_tokens_name;
            let tokenValue = r.data[tokenKey];
            let interval = r.data.interval ? r.data.interval : 3000
            obj[name] = r.data.data ? r.data.data : [];
            if (item.moduleCode === 'THEME') { // 主题数据放在一起
                let themeObj = r.data.data;
                if (themeObj) {
                    themeObj[0].moduleId = item.id;
                }
                obj[name] = [...this.data.THEME, ...themeObj];
            }
            if (item.moduleCode === 'SPECIAL') {
                this.setData({
                    isSpecial: true,
                    specialName: item.moduleName
                });
            }
            if (item.moduleCode === 'COUPON' && r.data.data && r.data.data.length > 0) {
                r.data.data.forEach(list => {
                    list.tokenKey = tokenKey;
                    list.tokenValue = tokenValue;
                    this.setData(obj);
                })
            } else {
                this.setData(obj);
                this.setData({ interval })
            }
        })
    },

    // 获取限时折扣会场
    getTimerModule() {
        api.fetchTimerModule().then(res => {
            console.log('限时数据：', res);
            if (res.data.flag) {
                if (res.data.data && res.data.data.spuList && res.data.data.spuList.length > 5) {
                    res.data.data.spuList = res.data.data.spuList.slice(0, 5);
                }
                if (res.data.data) res.data.data.endDate = new Date(res.data.data.endDate.replace(/-/g, '/')).getTime()
                this.setData({
                    TIMER: res.data.data
                });
            }
        })
    },

    // 获取满减会场
    getFullModule() {
        api.fetchFullModule().then(res => {
            console.log('满减数据：', res);
            if (res.flag) {
                if (res.data && res.data.spuList && res.data.spuList.length > 5) {
                    res.data.spuList = res.data.spuList.slice(0, 5);
                }
                this.setData({
                    FULL: res.data
                });
            }
        })
    },

    // 获取第二件活动数据
    getSecondModule() {
        api.fetchSecondModule().then(res => {
            console.log('第二件活动数据:', res);
            if (res.flag) {
                if (res.data && res.data.spuList && res.data.spuList.length > 5) {
                    res.data.spuList = res.data.spuList.slice(0, 5);
                }
                this.setData({
                    SECOND: res.data
                });
            }
        })
    },

    // 获取拼团会场
    getGroupBarGainModule() {
        api.fetchgroupActivity().then(res => {
            console.log('拼团数据：', res);
            if (res.flag) {
                if (res.data && res.data.length > 5) {
                    res.data = res.data.slice(0, 5);
                }
                this.setData({
                    GROUP_BARGAIN: res.data
                });
            }
        })
    },

    // 获取优惠套餐活动数据
    getPackageModule() {
        api.fetchPackageModule().then(res => {
            console.log('优惠套餐活动数据:', res);
            if (res.flag) {
                if (res.data && res.data && res.data.length > 5) {
                    res.data = res.data.slice(0, 5);
                }
                this.setData({
                    PACKAGE_DISCOUNT: res.data
                });
            }
        })
    },
    // 获取地理位置(微信接口)
    getLocation() {
        Wechat.getLocation().then((res) => {
            let lng = res.longitude + '';
            let lat = res.latitude + '';
            this.getNearestOrganize({ lng, lat, industryType: 'GeneralMerchandise', organizeId: 0 });
        }).catch(() => {
            this.getNearestOrganize({ industryType: 'GeneralMerchandise', organizeId: 0 });
        });
    },
    // 根据经纬度获取最近商场
    getNearestOrganize(param) {
        loginApi.nearestOrganiz(param).then(res => {
            if (res.data.flag) {
                app.handleOrganizeInfo(res.data.data);
                this.setData({
                    organizeId: res.data.data.organizeId,                    
                    organizeName: res.data.data.organizeName,
                    organizeAddress: res.data.data.organizeAddress
                });
                this.loadPageData();
            }
        })
    },
    // 初始化页面数据
    loadPageData() {
        if (pageDataLoading) {
            return false
        }
        pageDataLoading = true;
        api.getHomeModel({}).then(res => {
            let data = res.data.data;
            this.setData({ lists: data });
            const promises = [];
            data.forEach(item => {
                if (item.moduleCode === 'TIMER') {
                    promises.push(this.getTimerModule(item));
                } else if (item.moduleCode === 'FULL') {
                    promises.push(this.getFullModule(item));
                } else if (item.moduleCode === 'SECOND') {
                    promises.push(this.getSecondModule(item));
                } else if (item.moduleCode === 'PACKAGE') {
                    promises.push(this.getPackageModule(item));
                } else if (item.moduleCode === 'GROUP_BARGAIN') {
                    promises.push(this.getGroupBarGainModule(item));
                } else {
                    promises.push(this.getNormalModule(item));
                }
            });
            Promise.all(promises);
            pageDataLoading = false;
        });
        app.changeInitDiscover(0);
        this.initIndustryType();
        // this.getDialogConfig();
        this.homeWindow();
        this.initPage();
    },

    onLoad(option) { 
        
    },

    onShow(option) {
        const _orgName = wx.getStorageSync('organizeName');
        const _organizeId = wx.getStorageSync('organizeId');
        const _orgAddress = wx.getStorageSync('organizeAddress');
        if (!this.data.lists.length || _organizeId != this.data.organizeId) {
            if (!_organizeId) {
                // 获取最近组织并且获取首页数据
                this.getLocation();
            } else {
                this.setData({
                    organizeId: _organizeId,
                    organizeName: _orgName,
                    organizeAddress: _orgAddress
                });
                // 获取当前组织首页数据
                this.loadPageData();
            }
        }
        
    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh() {
        let curTime = new Date().getTime();
        let lastPullDownTime = this.data.lastPullDownTime;
        if (lastPullDownTime && curTime - lastPullDownTime < 5000) {
            wx.hideNavigationBarLoading();  // 完成停止加载
            wx.stopPullDownRefresh();
            return
        }
        wx.showNavigationBarLoading();
        this.setData({
            lastPullDownTime: curTime,
            THEME: []   // 每次进入页面初始化
        });
        api.getHomeModel().then(res => {
            let data = res.data.data;
            this.setData({ lists: data });
            const promises = [];
            data.forEach(item => {
                if (item.moduleCode === 'TIMER') {
                    promises.push(this.getTimerModule(item));
                } else if (item.moduleCode === 'FULL') {
                    promises.push(this.getFullModule(item));
                } else if (item.moduleCode === 'SECOND') {
                    promises.push(this.getSecondModule(item));
                } else if (item.moduleCode === 'PACKAGE') {
                    promises.push(this.getPackageModule(item));
                } else if (item.moduleCode === 'GROUP_BARGAIN') {
                    promises.push(this.getGroupBarGainModule(item));
                } else {
                    promises.push(this.getNormalModule(item));
                }
            });
            Promise.all(promises);
        });
        app.changeInitDiscover(0);
        this.initIndustryType();
        this.initPage();
        wx.hideNavigationBarLoading() // 完成停止加载
        wx.stopPullDownRefresh();
    },

    // 分页
    onReachBottom() {
        if(this.data.page.pageNum == null || this.data.page.pageNum == undefined || this.data.page.pageNum == '') return;
        if (this.data.page.total <= 0) {
            this.setData({
                emptyPage: true,
                bottomTipsFlag: true,
                loadMore: false
            })
        } else if (this.data.page.pageNum === this.data.page.pages && this.data.page.pageNum) {
            this.setData({
                emptyPage: false,
                bottomTipsFlag: false,
                bottomTipsText: 'OH 到底啦~',
                loadMore: false
            })
        } else if (this.data.page.pages || this.data.page.pageNum < this.data.page.pages) {
            this.setData({
                emptyPage: false,
                bottomTipsFlag: true,
                bottomTipsText: '上拉加载更多...',
                loadMore: true
            })
        } else {
            this.setData({
                emptyPage: false,
                bottomTipsFlag: true,
                bottomTipsText: '上拉加载更多...',
                loadMore: true
            })
        }
        this.setData({
            pending: false
        })
        if (this.data.pending || !this.data.loadMore) return
        this.setData({
            pending: true
        })
        if (!this.data.loadMore) return
        let pageNum = this.data.page.pageNum + 1
        this.setData({
            'params.page.pageNum': pageNum,
            'page.pageNum': pageNum
        })
        this.getList(this.data.params, 'list')
    },

    /**
     * 页面相关分享
     */
    onShareAppMessage() {
        const organizeId = wx.getStorageSync('organizeId');
        const organizeName = wx.getStorageSync('organizeName');
        return {
            path: `/pages/homeMarket/home?organizeId=${organizeId}&organizeName=${organizeName}`
        }
    }
});
