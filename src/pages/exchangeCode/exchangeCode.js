// 获取全局应用程序实例对象
const app = getApp()
const couponApi = require('../../api/coupon.js');
const QR = require('../../utils/wxqrcode.min.js');
import timeFormat from '../../utils/timeFormat.js';
import Tips from '../../utils/tip.js';
import CheckStatus from '../../api/checkState.js';

// 创建页面实例对象
Page({
  /**
   * 页面的初始数据
   */
  data: {
    info: {},  // 礼品核销信息
    member: {},  // 会员信息
    avatarUrl: '',    // 头像
    imgErweima: '',   // 二维码
    messageComponent: '',
    gainId: '',
    isStart: false,
    signStateText: '',
    // 券信息
    detail: {},
    // 店铺信息
    store: {},
    verifyCode: '',
    showtime: '',
    shopName: '',
    defaultPic: '' // 默认图片
  },
  // 获取券信息
  getInfo(params) {
    couponApi.fetchCouponDetail(params).then(res => {
      Tips.loading();
      if (!res.data.flag) {
        let msg = res.data.msg || '请求数据失败';
        this.messageComponent.applyActive(msg);
        Tips.loaded();
        return
      }
      let t = res.data.data;
      t.startTime = timeFormat.timeStr(t.startTime, 'yyyy.MM.dd hh:mm');
      t.endTime = timeFormat.timeStr(t.endTime, 'yyyy.MM.dd hh:mm');
      let shop = t.limitType == 'ONE'
        ? `限${t.listCouponShop[t.listCouponShop.length - 1].shopName}`
        : t.limitType == 'ALL'
          ? `全部商户通用`
          : t.limitType == 'MAIN' ? '限' + wx.getStorageSync('organizeName') : `限指定商户`;
      this.setData({
        detail: res.data.data,
        store: res.data.data.listCouponShop && res.data.data.listCouponShop.length && res.data.data.listCouponShop[res.data.data.listCouponShop.length - 1],
        verifyCode: res.data.data.verifyCode + '1',
        shopName: shop
      });
      let imgErweima = QR.createQrCodeImg(this.data.verifyCode);
      this.setData({
        imgErweima: imgErweima
      });
      Tips.loaded();

      // 检查核销状态
      if (res.data.data.useState === 'UNUSE') {
        CheckStatus.verifyInterVal({ verifyCode: this.data.verifyCode }).then((r) => {
          if (r) {
            wx.setStorageSync('refundCoupon', 'refundCoupon');
            wx.setStorageSync('refundCouponIndex', this.data.index);
            this.getInfo(params)
          }
        }).catch(err => {
          console.log(err)
        })
      }
    })
  },
  // 获取 用户信息
  getUserInfo() {
    let _this = this;
    if (wx.getUserProfile) {
      wx.getUserProfile({
        success: function(res) {
          let userInfo = res.userInfo;
          _this.setData({
            avatarUrl: userInfo.avatarUrl
          })
        }
      })
    } else {
      wx.getUserInfo({
        success: function(res) {
          let userInfo = res.userInfo;
          _this.setData({
            avatarUrl: userInfo.avatarUrl
          })
        }
      })
    }
  },

  // 使用说明
  handleJumpDetail(e) {
    let gainId = e.currentTarget.dataset.id;
    wx.navigateTo({
      url: `../coupon/detail/detail?id=${gainId}`
    })
  },

  // 退券
  handleBackCoupon() {
    Tips.confirm(`确认退券？`, '', '提示', '取消', '确认').then(() => {
      let param = {
        memberCouponId: this.data.memberCouponId,
        appId: wx.getStorageSync('appId')
      };
      CheckStatus.clearVerifyInterValInterval();
      couponApi.refundCoupon(param).then(res => {
        if (res.data.flag) {
          Tips.success('退券成功');
          wx.setStorageSync('refundCoupon', 'refundCoupon');
          wx.setStorageSync('refundCouponIndex', this.data.index);
          wx.navigateBack({
            delta: 1
          })
        } else {
          this.messageComponent.applyActive(res.data.msg || '退券失败');
        }
      })
    }).catch(() => {
      console.log('取消退券')
    })
  },

  // 重新刷新二维码
  refreshCouponCode() {
    let param = {
      memberCouponId: this.data.memberCouponId
    };
    return new Promise((resolve, reject) => {
      couponApi.refreshCouponCode(param).then(res => {
        if (res.data.flag) {
          this.setData({
            verifyCode: res.data.data + '1'
          });
          resolve(res.data.data + '1')
        } else {
          reject(this.messageComponent.applyActive(res.data.msg || '刷新失败！'));
        }
      })
    })
  },

  // 倒计时
  timeCountDown() {
    // 每隔一秒执行一次
    this.timer = setInterval(() => timeFormat.showtime(this), 1000);
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(option) {
    this.messageComponent = this.selectComponent('.messageTips');
    this.setData({
      couponId: option.couponType,
      memberCouponId: option.gainId,
      couponKind: option.couponKind,
      index: option.index,
      defaultPic: app.globalData.defaultConfig.defaultPic
    })
    let that = this
    wx.onUserCaptureScreen((res) => {
      console.log(this);
      console.log(res);
      that.refreshCouponCode().then(() => {
        let imgErweima = QR.createQrCodeImg(that.data.verifyCode);
        that.setData({
          imgErweima: imgErweima
        });
      })
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {
    // TODO: onReady
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {
    // TODO: onShow
    let title = this.data.couponKind == 'PARKING2' || this.data.couponKind == 'PARKING' ? '停车券说明' : '优惠券核销码'
    wx.setNavigationBarTitle({
      title: title
    });
    let params = {
      couponId: this.data.couponKind != 'PARKING2' ? this.data.couponId : '',
      memberCouponId: this.data.couponKind != 'PARKING2' ? this.data.memberCouponId : '',
      memberCouponParkingId: this.data.couponKind == 'PARKING2' ? this.data.memberCouponId : ''
    };
    this.getInfo(params)
    this.timeCountDown();
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {
    // TODO: onHide
    CheckStatus.clearVerifyInterValInterval();
    clearInterval(this.timer)
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {
    // TODO: onUnload
    // 清除定时器
    CheckStatus.clearVerifyInterValInterval();
    clearInterval(this.timer)
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {
    // TODO: onPullDownRefresh
  }
});
