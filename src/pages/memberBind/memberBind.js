// 获取全局应用程序实例对象
const app = getApp();
import { PHONE, IDCARD } from '../../utils/regular.js';
import Tips from '../../utils/tip.js';
import userApi from '../../api/user.js'

// 创建页面实例对象
Page({
  /**
   * 页面的初始数据
   */
  data: {
    title: 'bindCard',
    phoneValue: '',
    codeValue: '',
    cardValue: '',
    codeBtnText: '发送验证码',
    isDisabled: false,
    time: 60,
    timer: '',
    sysCode: '',
    isAuthorize: false,
    isLoading: false,
    memberPhone: '',
    messageComponent: '',
    checked: true,
    btnDisabled: false,
    // 注册送的积分
    points: 0,
    couponListPoints: 0,
    showDialog: false,
    // 终端类型，包括iPhone, android,其他
    platform: '',
    // 机型，如苹果iPhone6, OPPO R9等
    device: ''
  },

  // 手机号码输入
  handlePhoneInput(e) {
    this.setData({
      phoneValue: e.detail.value
    });
    console.log(this.data.phoneValue)
  },

  // 身份证号码输入
  handleCardInput(e) {
    this.setData({
      cardValue: e.detail.value
    });
    console.log(this.data.cardValue)
  },

  // 验证码输入
  handleCodeInput(e) {
    this.setData({
      codeValue: e.detail.value
    });
    console.log(this.data.codeValue)
  },

  // 获取验证码
  handleSendCode() {
    // 验证手机号
    if (!this.data.phoneValue) {
      this.messageComponent.applyActive('请输入手机号码');
      return false;
    } else if (!PHONE.test(this.data.phoneValue)) {
      this.messageComponent.applyActive('手机格式不正确');
      return false;
    } else {
      this.getCode()
    }
  },

  // 发送验证码
  getCode() {
    let params = {
      memberPhone: this.data.phoneValue,
      code: wx.getStorageSync('hash')
    };
    this.setData({
      isDisabled: true
    });
    userApi.getSecurityCode(params).then(res => {
      if (res.data.flag) {
        this.handleSuccess()
      } else {
        this.messageComponent.applyActive(res.data.msg);
        this.setData({
          isDisabled: false
        })
      }
    }).catch(err => {
      this.setData({
        isDisabled: false
      });
      this.messageComponent.applyActive(err)
    })
  },

  // 发送成功需要的倒计时
  handleSuccess() {
    let time = this.data.time;
    let timer;
    this.setData({
      codeBtnText: `${time}S后重试`
    });
    timer = setInterval(() => {
      if (time === 1) {
        this.setData({
          isDisabled: false,
          codeBtnText: '发送验证码'
        });
        clearInterval(timer);
        return
      }
      time = time - 1;
      this.setData({
        codeBtnText: `${time}S后重试`
      })
    }, 1000)
  },

  // 绑定银行卡
  handleBindCard() {
    // 验证手机号
    if (!this.data.phoneValue) {
      this.messageComponent.applyActive('请输入手机号码');
      return false;
    } else if (!PHONE.test(this.data.phoneValue)) {
      this.messageComponent.applyActive('手机格式不正确');
      return false;
    }

    // 身份证校验
    if (!this.data.cardValue) {
      this.messageComponent.applyActive('请输入身份证号码');
      return false;
    } else if (!IDCARD.test(this.data.cardValue)) {
      this.messageComponent.applyActive('身份证号码不正确');
      return false;
    }

    // 验证码校验
    if (!this.data.codeValue) {
      this.messageComponent.applyActive('请输入验证码');
      return false;
    }

    let params = {
      memberPhone: this.data.phoneValue,
      isValidate: this.data.isAuthorize ? 'Y' : 'N',
      vircode: this.data.isAuthorize ? '' : this.data.codeValue
    };
    this.setData({
      isLoading: true,
      btnDisabled: true
    });
    userApi.register(params).then(res => {
      if (res.data.flag) {
        app.data.memberType = 'MEMBER';
        app.globalData.memberPhone = this.data.phoneValue;
        wx.setStorageSync('memberPhone', this.data.phoneValue);
        if (res.data.couponList) {
          let couponList = res.data.couponList;
          let couponListPoints = 0;
          if (couponList && couponList.length != 0) {
            couponList.forEach((item) => {
              couponListPoints = couponListPoints + item.couponValue
            })
          }
          this.setData({
            couponListPoints: couponListPoints
          });
        }
        if (res.data.points) {
          let points = res.data.points;
          this.setData({
            points: points && points != '' ? points : 0
          })
        }
        // 如果没有送积分和券
        if (!res.data.points && !res.data.couponList) {
          Tips.success('注册成功', 1000).then(() => {
            wx.navigateBack({
              delta: 2
            })
          })
        } else {
          this.setData({
            showDialog: true
          });
        }
      } else {
        this.messageComponent.applyActive(res.data.msg || '注册失败');
        this.setData({
          isLoading: false,
          btnDisabled: false
        })
      }
    }).catch(err => {
      console.log(err);
      this.setData({
        isLoading: false,
        btnDisabled: false
      })
    })
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad() {
    // TODO: onLoad
    this.messageComponent = this.selectComponent('.messageTips');
    let memberPhone = wx.getStorageSync('memberPhone');
    if (memberPhone) {
      this.setData({
        memberPhone: memberPhone,
        phoneValue: memberPhone
      })
    }
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {
    // TODO: onReady
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {
    // TODO: onShow
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {
    // TODO: onHide
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {
    // TODO: onUnload
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {
    // TODO: onPullDownRefresh
  }
});
