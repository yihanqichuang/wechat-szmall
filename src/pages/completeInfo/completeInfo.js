// 获取全局应用程序实例对象
const app = getApp();
import completeInfoApi from '../../api/completeInfo.js';
import Tips from '../../utils/tip.js';
// import { PHONE, EMAIL } from '../../utils/regular.js';
import { PHONE, IDCARD } from '../../utils/regular.js';

// 创建页面实例对象
Page({
  /**
   * 页面的初始数据
   */
  data: {
    messageComponent: '',
    sexArray: ['男', '女'],
    // 表单页数据
    memberName: '',
    memberSex: '',
    memberPhone: '',
    occupation: '',
    email: '',
    region: [],
    regionStr: '',
    memberProvince: '',
    memberCity: '',
    memberDistrict: '',
    address: '',
    payPwd1: '',
    payPwd2: '',
    verificationCode: '',
    codeBtnText: '发送验证码',
    isDisabled: false,
    time: 60,
    timer: '',
    memberBirthday: '',
    memberBirthdayStr: '',
    idCardNumber: null,
    isLoading: false, // 保存按钮
    btnDisabled: false,
    occupationList: [], // 职业列表
    occupationListAll: [],
    family: '',
    familyStatus: [],
    familyStatusArr: [],
    education: '',
    educationArr: [],
    educationAll: [],
    showDialog: false,
    pointsCount: 0, // 赠送的积分
    birthdayDisabled: false, // 是否可以编辑生日
    idCardNumberDisabled: false // 是否可以编辑身份证
  },
  // 姓名输入
  handleNameInput(e) {
    this.setData({
      memberName: e.detail.value
    });
  },
  /**
   * 性别切换
   */
  bindSexChange(e) {
    this.setData({
      memberSex: e.detail.value
    });
  },

  // 选择生日
  bindBirthChange(e) {
    this.setData({
      memberBirthday: e.detail.value
    });
  },

  /**
   * 邮箱输入
   */
  handleEmailInput(e) {
    this.setData({
      email: e.detail.value
    });
  },
  /**
   * 手机号输入
   */
  handlePhoneInput(e) {
    this.setData({
      memberPhone: e.detail.value
    });
  },
  /**
   * 身份证输入
   */
  handleIdInput(e) {
    this.setData({
      idCardNumber: e.detail.value
    });
  },

  /**
   * 地区改变
   */
  bindRegionChange(e) {
    this.setData({
      region: e.detail.value,
      regionStr:
        e.detail.value[0] + '/' + e.detail.value[1] + '/' + e.detail.value[2],
      memberProvince: e.detail.value[0],
      memberCity: e.detail.value[1],
      memberDistrict: e.detail.value[2]
    });
  },

  /**
   * 详细地址输入
   */
  handleDetailAddressInput(e) {
    this.setData({
      address: e.detail.value
    });
  },
  /**
   * 密码设置输入
   */
  handlePasswordInput(e) {
    this.setData({
      payPwd1: e.detail.value
    });
  },
  /**
   * 确认密码输入
   */
  handlePasswordAgainInput(e) {
    this.setData({
      payPwd2: e.detail.value
    });
  },
  /**
   * 验证码输入
   * @param {*} 
   */
  handleCodeInput(e) {
    this.setData({
      verificationCode: e.detail.value
    });
  },
  //  职业
  occupationChange(e) {
    this.setData({
      occupation: e.detail.value
    });
  },
  familyChange(e) {
    this.setData({
      family: e.detail.value
    });
  },
  educationChange(e) {
    this.setData({
      education: e.detail.value
    });
  },
  /**
   * 获取验证码
   */
  handleSendCode() {
    // 验证手机号
    if (!this.data.memberPhone) {
      this.messageComponent.applyActive('请输入手机号码');
      return false;
    } else if (!PHONE.test(this.data.memberPhone)) {
      this.messageComponent.applyActive('手机格式不正确');
      return false;
    } else {
      this.getCode();
    }
  },

  /**
   * 发送验证码
   */
  getCode() {
    let params = {
      memberPhone: this.data.memberPhone,
      code: wx.getStorageSync('hash')
    };
    this.setData({
      isDisabled: true
    });
    completeInfoApi
      .sendCode(params)
      .then(res => {
        if (res.data.flag) {
          this.handleSuccess();
        } else {
          this.messageComponent.applyActive(res.data.msg);
          this.setData({
            isDisabled: false
          });
        }
      })
      .catch(err => {
        this.setData({
          isDisabled: false
        });
        this.messageComponent.applyActive(err);
      });
  },

  /**
   * 发送成功需要的倒计时
   */
  handleSuccess() {
    let time = this.data.time;
    let timer;
    this.setData({
      codeBtnText: `${time}S后重试`
    });
    timer = setInterval(() => {
      if (time === 1) {
        this.setData({
          isDisabled: false,
          codeBtnText: '发送验证码'
        });
        clearInterval(timer);
        return;
      }
      time = time - 1;
      this.setData({
        codeBtnText: `${time}S后重试`
      });
    }, 1000);
  },

  /**
   * 表单保存
   */
  handleBindCard() {
    // 验证手机号
    if (!this.data.memberPhone) {
      this.messageComponent.applyActive('请输入手机号码');
      return false;
    } else if (!PHONE.test(this.data.memberPhone)) {
      this.messageComponent.applyActive('手机格式不正确');
      return false;
    }

    // 身份证校验
    if (this.data.idCardNumber) {
      if (!IDCARD.test(this.data.idCardNumber)) {
        this.messageComponent.applyActive('身份证不正确');
        return false;
      }
    }

    // 姓名校验
    if (!this.data.memberName) {
      this.messageComponent.applyActive('请输入姓名');
      return false;
    }
    // 密码校验
    // if (this.data.payPwd1) {
    //   if (!WHOLENUMBER.test(this.data.payPwd1)) {
    //     this.messageComponent.applyActive('密码只能为数字');
    //     return false;
    //   }
    // }
    // // 密码校验
    // if (this.data.payPwd1 !== this.data.payPwd2) {
    //   this.messageComponent.applyActive('两次密码不一致');
    //   return false;
    // }
    // // 密码校验
    // if (this.data.payPwd1 !== this.data.payPwd2) {
    //   this.messageComponent.applyActive('两次密码不一致');
    //   return false;
    // }
    // // 验证码校验
    // if (!this.data.verificationCode) {
    //   this.messageComponent.applyActive('请输入验证码');
    //   return false;
    // } else if (!WHOLENUMBER.test(this.data.verificationCode)) {
    //   this.messageComponent.applyActive('验证码只能为数字');
    //   return false;
    // }
    let sex;
    if (this.data.memberSex == 0) {
      sex = 'MAN';
    } else if (this.data.memberSex == 1) {
      sex = 'WOMAN';
    } else {
      sex = 'UNKNOW';
    }
    let listMemberExtraInfo = [
      { type: 'PROFESSION', value: this.data.occupationListAll[this.data.occupation] },
      { type: 'FAMILYINFO', value: this.data.familyStatusArr[this.data.family] },
      { type: 'EDUCATION', value: this.data.educationAll[this.data.education] }
    ];
    let params = {
      memberName: this.data.memberName,
      memberSex: sex,
      mobileno: this.data.memberPhone,
      memberBirthday: this.data.memberBirthday,
      idCardNumber: this.data.idCardNumber,
      email: this.data.email,
      region: this.data.region.length > 0 ? this.data.region : [],
      regionStr: this.data.memberProvince
        ? this.data.region[0] +
        '/' +
        this.data.region[1] +
        '/' +
        this.data.region[2]
        : '',
      memberProvince: this.data.region[0],
      memberCity: this.data.region[1],
      memberDistrict: this.data.region[2],
      address: this.data.address,
      listMemberExtraInfo: listMemberExtraInfo
    };
    this.setData({
      isLoading: true,
      btnDisabled: true
    });
    completeInfoApi
      .editUserInfo(params)
      .then(res => {
        if (res.data.flag) {
          app.data.memberType = 'MEMBER';
          app.globalData.memberPhone = this.data.memberPhone;
          wx.setStorageSync('memberPhone', this.data.memberPhone);
          if (res.data.data > 0) {
            this.setData({
              pointsCount: res.data.data,
              showDialog: true
            })
          } else {
            Tips.success('修改成功', 1000).then(() => {
              wx.navigateBack({
                delta: 2
              });
            });
          }
          this.setData({
            isLoading: false,
            btnDisabled: true
          });
        } else {
          this.messageComponent.applyActive(res.data.msg || '修改失败');
          this.setData({
            isLoading: false,
            btnDisabled: false
          });
        }
      })
      .catch(err => {
        console.log(err);
        this.setData({
          isLoading: false,
          btnDisabled: false
        });
      });
  },

  // 跳转我的积分
  toIntegralPage() {
    let url = '/pages/integral/integral'
    wx.navigateTo({
      url
    })
  },

  // 关闭送积分弹窗
  handleCloseDialog() {
    this.setData({
      showDialog: false
    })
    wx.navigateBack({
      delta: 2
    });
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad() {
    wx.showLoading({
      title: '加载中'
    })
    // TODO: onLoad
    this.messageComponent = this.selectComponent('.messageTips');
    // 获取用户信息
    let params = {
      memberId: wx.getStorageSync('memberId')
    };
    completeInfoApi.getUserInfo(params).then(res => {
      let sex;
      if (res.data.data.memberSex == 'MAN') {
        sex = 0;
      } else if (res.data.data.memberSex == 'WOMAN') {
        sex = 1;
      } else {
        sex = '';
      }
      let configList = res.data.data.configList;
      configList.forEach(el => {
        if (el.showType == 'PROFESSION') {
          this.setData({
            occupationList: el.listMemberDataConfigDetail
          })
        }
        if (el.showType == 'FAMILYINFO') {
          this.setData({
            familyStatus: el.listMemberDataConfigDetail
          })
        }
        if (el.showType == 'EDUCATION') {
          this.setData({
            educationArr: el.listMemberDataConfigDetail
          })
        }
      })
      this.data.occupationList.forEach(el => {
        this.data.occupationListAll.push(el.value)
      })
      this.data.familyStatus.forEach(el => {
        this.data.familyStatusArr.push(el.value)
      })
      this.data.educationArr.forEach(el => {
        this.data.educationAll.push(el.value)
      })
      this.setData({
        occupationListAll: this.data.occupationListAll,
        familyStatusArr: this.data.familyStatusArr,
        educationAll: this.data.educationAll
      })
      //  数据回显 传 索引值  occupation  family  education
      let listMemberExtraInfos = res.data.data.listMemberExtraInfos;
      let occupationName = '';
      let occupationNum = -1;
      let familyName = '';
      let familyNum = -1;
      let educationName = '';
      let educationNum = -1;
      if (listMemberExtraInfos) {
        listMemberExtraInfos.forEach(el => {
          if (el.type == 'PROFESSION') {
            occupationName = el.value
          }
          if (el.type == 'FAMILYINFO') {
            familyName = el.value
          }
          if (el.type == 'EDUCATION') {
            educationName = el.value
          }
        })
      }
      this.data.occupationListAll.forEach((el, index) => {
        if (el == occupationName) {
          occupationNum = index
        }
      })
      this.data.familyStatusArr.forEach((el, index) => {
        if (el == familyName) {
          familyNum = index
        }
      })
      this.data.educationAll.forEach((el, index) => {
        if (el == educationName) {
          educationNum = index
        }
      })
      this.setData({
        occupation: occupationNum || occupationNum == 0 ? occupationNum : '',
        family: familyNum || familyNum == 0 ? familyNum : '',
        education: educationNum || educationNum == 0 ? educationNum : '',
        memberName: res.data.data.memberName || '',
        memberSex: sex,
        memberPhone: res.data.data.memberPhone,
        email: res.data.data.email,
        region: res.data.data.memberProvince
          ? [
            res.data.data.memberProvince,
            res.data.data.memberCity,
            res.data.data.memberDistrict
          ]
          : [],
        regionStr: res.data.data.memberProvince
          ? res.data.data.memberProvince +
          '/' +
          res.data.data.memberCity +
          '/' +
          res.data.data.memberDistrict
          : '',
        memberProvince: res.data.data.memberProvince,
        memberCity: res.data.data.memberCity,
        memberDistrict: res.data.data.memberDistrict,
        address: res.data.data.address,
        memberBirthday: res.data.data.memberBirthdayStr || '',
        birthdayDisabled: Boolean(res.data.data.memberBirthdayStr),
        memberBirthdayStr: res.data.data.memberBirthdayStr,
        idCardNumber: res.data.data.idCardNumber || '',
        idCardNumberDisabled: Boolean(res.data.data.idCardNumber),
        payPwd1: res.data.data.payPwd || '',
        payPwd2: res.data.data.payPwd || ''
      });
      wx.hideLoading()
    }).catch(err => {
      console.log(err);
      wx.hideLoading()
    });
  }
});