// 获取全局应用程序实例对象
// const app = getApp()
import activityApi from '../../api/activity.js';
import timeFormat from '../../utils/timeFormat.js';
// 创建页面实例对象
Page({
  /**
   * 页面的初始数据
   */
  data: {
    bottomTipsFlag: true,
    tabs: [
      {
        name: '热门活动'
      },
      {
        name: '积分课堂'
      }
    ],
    activeIndex: 0,
    navSlider: {
      width: '40rpx',
      left: '170rpx',
      position: ['170rpx', '540rpx']
    },
    activityList: [],
    activityList_class: [],
    page: {
      pageNum: 1,
      pageSize: 10,
      total: 1
    },
    pageScore: {
      pageNum: 1,
      pageSize: 10,
      total: 1
    },
    activityType: 'ACTIVITY' // 活动分类（ACTIVITY 热门活动，CLASSROME 积分课堂）
  },
  // tab切换
  handleTabSlider(data) {
    let current = data.detail;
    this.setData({
      activeIndex: current
    });
  },
  // nav-slider 的位置
  sliderPosition(index) {
    this.setData({
      'navSlider.left': this.data.navSlider.position[index]
    })
  },
  // 改变 swiper 手势滑动
  swiperChange(event) {
    let current = event.detail.current;
    this.setData({
      activeIndex: current
    });
    this.sliderPosition(current);
    this.data.activityType = current == 0 ? 'ACTIVITY' : current == 1 ? 'CLASSROME' : '';
    this.getActivityList(this.data.activityType, 1)
  },
  // 活动上拉加载更多
  loadMore(e) {
    this.getActivityList(this.data.activityType, e.detail)
  },
  // 获取活动列表
  getActivityList(type, pageNum) {
    let params = { pageNum, type };
    if (pageNum === 1) {
      if (type == 'ACTIVITY') {
        this.setData({
          activityList: []
        });
      } else if (type == 'CLASSROME') {
        this.setData({
          activityList_class: []
        });
      }
    }
    activityApi.listSignupActivity(params).then(res => {
      if (res.data.flag) {
        let ret = res.data;
        ret.data.forEach((item) => {
          this.handleFormatData(item)
        });
        if (type == 'ACTIVITY') {
          this.setData({
            activityList: [...this.data.activityList, ...ret.data],
            page: ret.page
          });
        } else if (type == 'CLASSROME') {
          this.setData({
            activityList_class: [...this.data.activityList_class, ...ret.data],
            pageScore: ret.page
          });
        }
      } else {
        this.setData({
          activityList: [],
          activityList_class: []
        });
        this.messageComponent.applyActive(res.data.msg || '加载数据失败')
      }
    }).catch(err => {
      console.log(err)
    })
  },
  // 处理活动列表数据
  handleFormatData(item) {
    item.activityEndtimeStr = timeFormat.timeStr(item.activityEndtime, 'yyyy.MM.dd');
    item.activityStarttimeStr = timeFormat.timeStr(item.activityStarttime, 'yyyy.MM.dd');
    item.signupStartTimeStr = timeFormat.timeStr(item.startTime, 'yyyy.MM.dd hh:mm');
    item.signupEndTimeStr = timeFormat.timeStr(item.endTime, 'yyyy.MM.dd hh:mm');
    if (item.memberSignupState == '待报名') {
      item.type = '1';
      item.isDisabled = true
    } else if (item.memberSignupState == '去报名') {
      item.type = '2';
      item.isDisabled = false
    } else if (item.memberSignupState == '去参加') {
      item.type = '3';
      item.isDisabled = false
    } else if (item.memberSignupState == '已结束') {
      item.type = '4';
      item.isDisabled = true
    }
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad() {
    this.messageComponent = this.selectComponent('.messageTips');
    this.getActivityList('ACTIVITY', 1)
  },
  /**
   * 页面相关分享
   */
  onShareAppMessage() {
    const organizeId = wx.getStorageSync('organizeId');
    const organizeName = wx.getStorageSync('organizeName');
    return {
      path: `/pages/memberActivity/memberActivity?organizeId=${organizeId}&organizeName=${organizeName}`
    }
  }
})
