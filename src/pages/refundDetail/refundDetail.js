// 获取全局应用程序实例对象
// const app = getApp()
import orderApi from '../../api/order.js'

// 创建页面实例对象
Page({
  /**
   * 页面的初始数据
   */
  data: {
    title: 'refundDetail',
    messageComponent: '',
    detail: {},
    imgList: [],
    // 退款id
    memberRefundApplyId: ''
  },

  // 获取退款详情
  getRefundDetail() {
    let params = {
      dataId: this.data.memberRefundApplyId
    }
    orderApi.refundApplyDetail(params).then(res => {
      console.log(res)
      if (res.data.flag) {
        this.setData({
          detail: res.data.data
        })
        if (res.data.data.refundApplyPic) {
          let imgList = res.data.data.refundApplyPic.split(',')
          this.setData({
            imgList: imgList
          })
        }
      } else {
        this.messageComponent.applyActive(res.data.msg || '获取数据失败!')
      }
    }).catch((err) => {
      console.log(err)
    })
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(e) {
    this.messageComponent = this.selectComponent('.messageTips')
    this.setData({
      memberRefundApplyId: e.memberRefundApplyId
    })
  }
})
