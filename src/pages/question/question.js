// 获取全局应用程序实例对象
// const app = getApp()
import question from '../../api/question.js'

// 创建页面实例对象
Page({
  /**
   * 页面的初始数据
   */
  data: {
    title: 'form',
    url: ''
  },

  // 获取问卷调查
  getQuestion() {
    question.getQuestion().then(res => {
      console.log(res);
      if (res.data.flag) {
        let data = res.data.data.split(':');
        let appId = data[1];
        let id = data[0];
        let hostNames = '${hostname}';
        this.setData({
          url: `https://open.weixin.qq.com/connect/oauth2/authorize?appid=${appId}&redirect_uri=${hostNames}/mall/html/marketCenter/html/questionnaire/questionnaire_detail.html?appId=${appId}&response_type=code&scope=snsapi_userinfo&state=${id}#wechat_redirect`
        })
      } else {
        console.log(res.data.msg)
      }
    }).catch(err => {
      console.log(err)
    })
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad() {
    // TODO: onLoad
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {
    // TODO: onReady
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {
    // TODO: onShow
    this.getQuestion();
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {
    // TODO: onHide
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {
    // TODO: onUnload
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {
    // TODO: onPullDownRefresh
  }
});
