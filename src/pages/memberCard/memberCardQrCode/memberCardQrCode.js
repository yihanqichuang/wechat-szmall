// 获取全局应用程序实例对象
const app = getApp();
import user from '../../../api/user';
import scan from '../../../api/scan';
import QR from '../../../utils/wxqrcode.min.js'
import wxbarcode from '../../../utils/wxbarcode'
import coupon from '../../../api/coupon'
import Tips from '../../../utils/tip'
let QrInterval = null; // 刷新付款码的定时器
let scanInterval = null; // 被扫 定时器
// 创建页面实例对象
Page({
  /**
   * 页面的初始数据
   */
  data: {
    info: {},
    QrMember: '', // 会员码
    BarCode: '', // 条形码
    QrPay: '', // 付款码
    BarPay: '',
    payNo: '',  // 付款码
    showPayNo: false,
    showMemberNo: false,
    initScanInterval: true,
    type: 'member',
    messageComponent: '',
    back: false,
    cardLevel: '',
    couponNumber: 0
  },

  // 获取会员信息
  getInfo() {
    user.queryMember().then(res => {
      if (!res.data.flag) {
        let msg = res.data.msg || '获取会员信息失败';
        this.messageComponent.applyActive(msg);
        return
      }
      clearInterval(QrInterval);
      let cardNumber = String(res.data.data.cardNumber);
      let qrCode = res.data.data.qrCode;
      if (!qrCode) {
        qrCode = cardNumber
      }
      if (res.data.data.memberType != 'MEMBER') {
        Tips.confirm('您还不是会员,无法使用该功能', {}, '前往注册?', '回首页', '去注册').then(() => {
          Tips.loading('前往中');
          setTimeout(() => {
            wx.navigateTo({
              url: '/pages/login/login',
              success: () => {
                Tips.loaded();
              }
            })
          }, 1000);
        }).catch(() => {
          wx.switchTab({
            url: '/pages/homeMarket/home'
          })
        });
      }
      this.setData({
        info: res.data.data,
        QrMember: QR.createQrCodeImg(qrCode),
        cardLevel: res.data.data.cardLevel,
        BarCode: res.data.data.cardNumber
      });
      // this.getCouponList(res.data.data.cardNumber, res.data.data.memberId);
      app.globalData.cardNumber = res.data.data.cardNumber;
      wxbarcode.barcode('barcode_member', qrCode, 750, 200);
      this.createdQrPay(cardNumber + Date.now());
      QrInterval = setInterval(() => {
        this.createdQrPay(cardNumber + Date.now())
      }, 60000)
    })
  },
  // 生成二维码
  createdQrPay(code) {
    this.setData({
      QrPay: QR.createQrCodeImg(String(code)),
      payNo: code
    });
    wxbarcode.barcode('barcode', String(code), 512, 200);
  },
  switch(e) {
    let type = e.currentTarget.dataset.type;
    this.setData({ type });
    if (type === 'pay' && this.data.initScanInterval) {
      this.setScanInterval();
      this.setData({ initScanInterval: false })
    }
  },
  // 设置付款码被扫定时器
  setScanInterval() {
    scanInterval = setInterval(() => {
      scan.fetchQueryOrder().then(res => {
        if (res.data.flag && res.data.data && !app.globalData.qrOrderData) {
          clearInterval(scanInterval);
          app.globalData.qrOrderData = res.data.data;
          this.toOrder()
        }
      })
    }, 1000)
  },
  // 查看付款码
  checkPayNo() {
    this.setData({
      showPayNo: !this.data.showPayNo
    })
  },

  // 查看会员码
  checkMemberNo() {
    this.setData({
      showMemberNo: !this.data.showMemberNo
    })
  },
  // 跳转 确认订单页面
  toOrder() {
    wx.navigateTo({
      url: '../confirmOrder/confirmOrder'
    })
  },

  // 获取票券的使用数量
  getCouponList(cardNumber, memberId) {
    let params = {
      cardNumber: cardNumber,
      memberId: memberId,
      couponTypeKind: 'VOUCHER',
      queryType: 'UNUSE'
    };
    coupon.fetchMyCouponList(params).then(res => {
      if (res.data.flag) {
        let len = res.data.data.length;
        app.globalData.couponNumber = len;
        this.setData({
          couponNumber: len
        })
      } else {
        console.log(res.data.msg)
      }
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad() {
    this.messageComponent = this.selectComponent('.message-tips')
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {
    // TODO: onReady
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {
    this.setData({
      couponNumber: app.globalData.couponNumber
    });
    console.log(this.data.back);
    if (this.data.back) {
      this.setData({ back: false })
    }
    this.getInfo()
    app.globalData.qrOrderData = null;
    if (this.data.type === 'pay') {
      this.setScanInterval()
    }
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {
    clearInterval(scanInterval);
    clearInterval(QrInterval);
    this.setData({ back: true })
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {
    clearInterval(scanInterval);
    clearInterval(QrInterval);
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {
    // TODO: onPullDownRefresh
  }
});
