// 获取全局应用程序实例对象
// const app = getApp()
import userApi from '../../api/user.js';
import fillter from '../../utils/fillter.js';
import Tips from '../../utils/tip.js';

// 创建页面实例对象
Page({
  /**
   * 页面的初始数据
   */
  data: {
    title: '电子会员卡',
    memberInfo: {
      cardNumber: ''
    },
    festivalRights: 'N', // 纪念日营销权益显示flag
    memberRights: 'N',// 会员日营销权益显示flag
    birthdayRights: 'N',// 生日营销权益显示flag
  },

  // 获取纪念日营销图标是否显示
  showLevelPowerByGradeId() {
    let param = {gradeIdList:[this.data.memberInfo.cardLevelDO.gradeId]}
    userApi.showLevelPowerByGradeId(param).then(res => {
      if (res.flag && res.data && res.data.length > 0) {
        this.setData({
          festivalRights: res.data[0].isShowFestival,
          memberRights: res.data[0].isShowMemberDay,
          birthdayRights: res.data[0].isShowBirthday,
        })
      }
    })
  },

  // 获取会员信息
  getMemberInfo() {
    userApi.queryMember().then(res => {
      if (res.data.flag) {
        // 不是会员进入此页面
        if (res.data.data.memberType != 'MEMBER') {
          Tips.confirm('您还不是会员,无法使用该功能', {}, '前往注册?', '回首页', '去注册').then(() => {
            Tips.loading('前往中');
            setTimeout(() => {
              wx.navigateTo({
                url: '/pages/memberSign/memberSign',
                success: () => {
                  Tips.loaded();
                }
              })
            }, 1000);
          }).catch(() => {
            wx.switchTab({
              url: '/pages/homeMarket/home'
            })
          });
        }
        this.setData({
          memberInfo: res.data.data
        });
        this.setData({
          'memberInfo.cardNumber': fillter.codeFormat(res.data.data.cardNumber)
        });
        this.showLevelPowerByGradeId();
      } else {
        console.log(res.data.msg)
      }
    }).catch(err => {
      console.log(err)
    })
  },
  // 领卡至微信卡包
  getWechatCard() {
    this.getAddCardParam().then(res => {
      this.wxAddCard(res)
    })
  },
  getAddCardParam() {
    return new Promise((resolve, reject) => {
      userApi.fetchGetAddCardParam().then(res => {
        res.data.flag && resolve(res.data);
        res.data.flag || reject(res.data)
      }).catch(() => reject())
    })
  },
  wxAddCard(data) {
    wx.addCard({
      cardList: [
        {
          cardId: data.card_id,
          cardExt: JSON.stringify(data.cardExt)
        }
      ],
      success(res) {
        console.log(res.cardList); // 卡券添加结果
      },
      fail(res) {
        console.log(res)
      }
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad() {
    // TODO: onLoad
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {
    // TODO: onReady
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {
    // TODO: onShow
    this.getMemberInfo()
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {
    // TODO: onHide
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {
    // TODO: onUnload
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {
    // TODO: onPullDownRefresh
  }
});
