// 获取全局应用程序实例对象
// const app = getApp()
import userApi from '../../../api/user.js'
import Tips from '../../../utils/tip.js'
// 创建页面实例对象
Page({
  /**
   * 页面的初始数据
   */
  data: {
    title: 'balanceDetail',
    consumeType:'',
    consumeDetail: {
      organizeName: '上百万和购物中心',
      consumeType: '购物',
      consumeTime: '2020.12.23 14:29:32',
      consumePrice: '210.50'
    },
  },
  



  // 获取消费记录
  getRecordList() {
    Tips.loading();
    userApi.saleIntems().then(res => {
      console.log(res);
      // 每次有新的请求清空需要渲染的数组
      if (res.data.flag) {
        this.setData({
          recordList: []
        });
        let recordList = res.data.data;
        recordList.forEach((item) => {
          item.isShow = false;
        });
        this.setData({
          recordList: recordList
        });
        console.log(this.data.recordList);
        Tips.loaded()
      } else {
        this.messageComponent.applyActive(res.data.msg || '获取消费记录失败');
        Tips.loaded()
      }
    }).catch(err => {
      console.log(err);
      Tips.loaded()
    })
  },


  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(opt) {
    console.log('opt', opt)
    this.setData({
      consumeType:opt.consumeType
    })
    this.messageComponent = this.selectComponent('.messageTips');
    // TODO: onLoad
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {
    // TODO: onReady
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {
    // TODO: onShow
    // this.getRecordList()
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {
    // TODO: onHide
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {
    // TODO: onUnload
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {
    // TODO: onPullDownRefresh
  }
});
