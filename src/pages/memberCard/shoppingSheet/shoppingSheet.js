// 获取全局应用程序实例对象
// const app = getApp()
import userApi from '../../../api/user.js'
import Tips from '../../../utils/tip.js'
import {fetchOrganizeList} from '../../../api/markets'
// 创建页面实例对象
Page({
  /**
   * 页面的初始数据
   */
  data: {
    title: 'balanceDetail',
    recordList: [
      {
        organizeName: '上百万和购物中心',
        consumeType: '购物',
        consumeTime: '2020.12.23 14:29:32',
        consumePrice: '210.50'
      },
      {
        organizeName: '上虞一百购物中心',
        consumeType: '停车缴费',
        consumeTime: '2020.12.23 14:29:32',
        consumePrice: '8.20'
      },
      {
        organizeName: '上虞虞北超市',
        consumeType: '报名',
        consumeTime: '2020.12.23 14:29:32',
        consumePrice: '7.50'
      }
    ],
    recordListPage: [],
    recordListPages: [],
    messageComponent: '',
    page: {
      pageNum: 1,
      pageSize: 10,
      pages: '',
      total: 1
    },
    id: '',
    isShow: false,
    // 上一次点击的索引值
    preIndex: 0,
    organizeList: [],
    theOrganizeId:null
  },
  // 获取组织列表
  getOrganizeList() {
    let params = {
    };
    fetchOrganizeList(params).then(res => {
      console.log('获取组织列表', res)
      if (res.data.flag) {
        let organizeList = res.data.data;
        let list=[{
          organizeId:0,
          organizeName: '全部门店'
        }];
        organizeList.forEach(item => {
          console.log('item', item.organizeList);
          list.push(...item.organizeList);
        });
        if (organizeList && organizeList.length > 0) {
          this.setData({
            organizeList: list
          });
        }
      } else {
        this.messageComponent.applyActive(res.data.msg || '获取列表数据异常');
      }
    }).catch(err => {
      console.log(err)
    })
  },
  onChooseChanges(event) {
    console.log('获取列表',event);
    this.setData({
      theOrganizeId: event.detail.organizeId
    });
    this.getRecordList();
  },
  goDetails(e){
    console.log('e', e);
    let type=e.currentTarget.dataset.consumetype;
    let consumeType=null;
    switch(type) {
      case '购物' :
        consumeType=1;
        break;
      case '停车缴费' :
        consumeType=2;
        break;
      case '报名' :
        consumeType=3;
        break;
      default :
        break
    }
    wx.navigateTo({
      url: '../consumeDetail/consumeDetail?consumeType=' + consumeType
    })
  },





  // 获取消费记录
  getRecordList() {
    Tips.loading();
    userApi.saleIntems().then(res => {
      console.log(res);
      // 每次有新的请求清空需要渲染的数组
      if (res.data.flag) {
        this.setData({
          recordList: []
        });
        let recordList = res.data.data;
        recordList.forEach((item) => {
          item.isShow = false;
        });
        this.setData({
          recordList: recordList
        });
        console.log(this.data.recordList);
        Tips.loaded()
      } else {
        this.messageComponent.applyActive(res.data.msg || '获取消费记录失败');
        Tips.loaded()
      }
    }).catch(err => {
      console.log(err);
      Tips.loaded()
    })
  },


  // 显示当前一条消费记录的详情
  handleShowDetail(e) {
    let index = e.currentTarget.dataset.index;
    let recordList = this.data.recordList;
    let preIndex = this.data.preIndex;

    if (index == preIndex) {
      recordList[index].isShow = !recordList[index].isShow;
    } else {
      recordList[index].isShow = true;
      recordList[preIndex].isShow = false;
      preIndex = index;
    }

    this.setData({
      preIndex: preIndex,
      recordList: recordList
    });
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad() {
    this.messageComponent = this.selectComponent('.messageTips');
    this.getOrganizeList();
    // TODO: onLoad
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {
    // TODO: onReady
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {
    // TODO: onShow
    // this.getRecordList()
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {
    // TODO: onHide
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {
    // TODO: onUnload
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {
    // TODO: onPullDownRefresh
  }
});
