// 获取全局应用程序实例对象
const app = getApp();
const Wechat = require('../../utils/wechat.js');
import HomeApi from '../../api/home.js';
import LoginApi from '../../api/login.js';

Page({
    /**
     * 页面的初始数据
     */
    data: {
        organizeName: wx.getStorageSync('organizeName'),
        homeData: [],
        isTipShow: true
    },

    // 切换机构后调用刷新
    loadPageData() {
        this.getHomeSet();
    },

    // 获取首页配置
    getHomeSet() {
        HomeApi.getHomeList().then(res => {
            if (res.flag) {
                this.setData({ homeData: res.data });
            }
        })
    },

    toUrl(e) {
        let url = e.currentTarget.dataset.urls
        if (url.indexOf('isSwitchTab=Y') !== -1) {
            wx.switchTab({ url: url });
        } else {
            wx.navigateTo({ url: url })
        }
    },

    // 关闭顶部提示栏
    closeTip() {
        wx.setStorageSync('isTipShow', false);
        this.setData({ isTipShow: false })
    },

    onLoad(option) {
        // 登录授权后跳转
        if (option.loginRedirect && String(option.loginRedirect) == '1') {
            this.seleceWindow();
        }
    },
    // 弹窗
    seleceWindow() {
        let eventActionId = 1;
        HomeApi.windowConfig({
            eventActionId: eventActionId
        }).then(res => {
            console.log('首页弹窗数据：', res);
            if (res.data.data) {
                this.setData({
                    datalist: res.data.data
                });
                if (this.data.datalist.length) {
                    this.windowList(this.data.datalist[0], 0);
                    this.clickCount(this.data.datalist[0].id)
                };
            }
        })
    },
    windowList(datalist, index) {
        if (datalist) {
            //  通用
            if (datalist.popupType == 'COMMON') {
                this.selectComponent('#commonDialog').showDialog({
                    homeWindowData: datalist,
                    countDownNum: datalist.timedClose,
                    showDialog: true,
                    startTimeList: '',
                    endTimeList: '',
                    popupWindowsIndex: index
                });
                //  优惠券
            } else if (datalist.moduleType == 'COUPON') {
                this.selectComponent('#marketingDialog').showDialog({
                    homeWindowData: datalist,
                    showDialog: true,
                    countDownNum: datalist.timedClose,
                    sigupState: false,
                    couponState: true,
                    // PopulState: false,
                    // voteState: false,
                    startTimeList: '',
                    endTimeList: '',
                    // seckillState: false,
                    // grouponState: false,
                    popupWindowsIndex: index
                });
                // 活动报名
            } else if (datalist.moduleType == 'SIGNUP') {
                this.selectComponent('#marketingDialog').showDialog({
                    homeWindowData: datalist,
                    startTime: this.timeFormatSuB(datalist.object.startTime),
                    endTime: this.timeFormatSuB(datalist.object.endTime),
                    showDialog: true,
                    startTimeList: datalist.object.startTime,
                    endTimeList: datalist.object.endTime,
                    countDownNum: datalist.timedClose,
                    sigupState: true,
                    couponState: false,
                    popupWindowsIndex: index
                });
                // 问卷调查
            } else if (datalist.moduleType == 'QUESTION') {
                this.selectComponent('#marketingDialog').showDialog({
                    homeWindowData: datalist,
                    startTime: this.timeFormatSuB(datalist.object.startTime),
                    endTime: this.timeFormatSuB(datalist.object.endTime),
                    showDialog: true,
                    startTimeList: datalist.object.startTime,
                    endTimeList: datalist.object.endTime,
                    countDownNum: datalist.timedClose,
                    sigupState: false,
                    couponState: false,
                    questionState: true,
                    popupWindowsIndex: index
                });
                // 限时折扣
            } else if (datalist.moduleType == 'TIMER') {
                this.selectComponent('#marketingDialog').showDialog({
                    homeWindowData: datalist,
                    startTime: this.timeFormatSuB(datalist.object.startDate),
                    endTime: this.timeFormatSuB(datalist.object.endDate),
                    showDialog: true,
                    startTimeList: datalist.object.startDate,
                    endTimeList: datalist.object.endDate,
                    countDownNum: datalist.timedClose,
                    sigupState: false,
                    couponState: false,
                    questionState: false,
                    timerState: true,
                    popupWindowsIndex: index
                });

            }
        }
    },
    timeFormatSuB(time) {
        return time.substr(0, 10)
    },
    clickCount(id) {
        let param = {
            organizeId: wx.getStorageSync('organizeId'),
            popupId: id,
        };
        HomeApi.saveClickLog(param).then(() => { })
    },
    onShow() {
        this.setData({
            organizeName: wx.getStorageSync("organizeName")
        })
        // 获取首页数据
        this.loadPageData();
    },

    /**
     * 页面相关分享
     */
    onShareAppMessage() {
        const organizeId = wx.getStorageSync('organizeId');
        const organizeName = wx.getStorageSync('organizeName');
        const jdShareInfo = wx.getStorageSync("jdShareInfo");
        const shareId = jdShareInfo ? jdShareInfo.user_id : false;
        const shareUrl = shareId ? `/pages/homeMarket/home?organizeId=${organizeId}&organizeName=${organizeName}&shareId=${shareId}` : `/pages/homeMarket/home?organizeId=${organizeId}&organizeName=${organizeName}`;
        return {
            path: shareUrl
        }
    }
});
