/* eslint-disable */
// 获取全局应用程序实例对象
const app = getApp()
import question from '../../api/question.js';
import WxParse from '../../wxParse/wxParse.js';
import Tips from '../../utils/tip.js';
import query from '../../utils/query.js';
import timeFormat from '../../utils/timeFormat.js';
import shareLog from '../../api/shareLog.js';


// 创建页面实例对象
Page({
  /**
   * 页面的初始数据
   */
  data: {
    title: 'activityDetail',
    messageComponent: '',
    // 投票id
    id: '',
    naire: {},
    isSubmit: [],
    timeText: '',
    submitDataStr: '',
    options: [],
    awardStr: '',
    award: {}
  },

  // 获取投票详情数据
  getQuestionDetail(id) {
    let params = {
      id: id
    };
    question.questionDetail(params).then(res => {
      console.log('投票详情：', res);
      let data = res.data;
      let _this = this;
      if (data.flag) {
        let {
          naire,
          isSubmit,
          options,
          award
        } = data.data;
        let article = naire.detal;
        article && WxParse.wxParse('article', 'html', article, _this, 15);
        // 活动未开始
        if (naire.runState == 'UNSTART') {
          this.countDownTimes(naire.startTime)
        } else if (naire.runState == 'DOING') {
          this.countDownTimes(naire.endTime)
        }

        app.updateLocalOrganize(naire.organizeId);

        // 处理奖项设置
        this.handleAward(award);

        // 默认所有选项不勾选
        if (!isSubmit.length) {
          options.forEach((item) => {
            let option = item.option;
            item.disabled = false;
            item.textTips = '(多选)';
            if (item.type == 'RADIO' || item.type == 'CHECKBOX') {
              option.item_list.forEach((val) => {
                val.optionId = item.id;
                val.checked = false;
                val.disabled = false;
                val.option_image ? item.hasImage = true : item.hasImage = false;
              })
            } else {
              item.textContent = '';
              item.disabled = false;
            }
            if (item.type == 'CHECKBOX') {
              if (item.option.only_number && item.option.many_number) {
                item.textTips = `(多选,可选${item.option.only_number}~${item.option.many_number}项)`
              } else if (item.option.only_number && !item.option.many_number) {
                item.textTips = `(多选,最少选${item.option.only_number}项)`
              } else if (!item.option.only_number && item.option.many_number) {
                item.textTips = `(多选,最多选${item.option.many_number}项)`
              }
            }
          });
        } else {
          isSubmit.forEach((item) => {
            options.forEach((val) => {
              val.disabled = true;
              item.textTips = '(多选)';
              if (val.type !== 'MULTIPLE') {
                val.option.item_list.forEach((list) => {
                  list.disabled = true;
                  list.option_image ? val.hasImage = true : val.hasImage = false;
                  if (item.commonOptionId == val.id && item.optionValueId == list.id) {
                    list.checked = true
                  }
                })
              } else {
                if (item.commonOptionId == val.id) {
                  val.textContent = item.optionValue
                }
              }
              if (item.type == 'CHECKBOX') {
                if (item.option.only_number && item.option.many_number) {
                  item.textTips = `(多选,可选${item.option.only_number}~${item.option.many_number}项)`
                } else if (item.option.only_number && !item.option.many_number) {
                  item.textTips = `(多选,最少选${item.option.only_number}项)`
                } else if (!item.option.only_number && item.option.many_number) {
                  item.textTips = `(多选,最多选${item.option.many_number}项)`
                }
              }
            });
          })
        }
        this.setData({
          naire,
          isSubmit,
          award,
          options,
          // 海报图绘制需要的信息
          info: {
            picturePath: naire.picturePath,
            name: naire.title,
            time: timeFormat.timeToDay(naire.startTime) + ' ~ ' + timeFormat.timeToDay(naire.endTime),
            qrcodeUrl: naire.qrcodeUrl,
            memberHead: naire.memberHead,
            memberNickName: naire.memberNickName,
            logoUrl: '',
            organizeName: wx.getStorageSync('organizeName')
          }
        })
        // let memberType = wx.getStorageSync('memberType');
        // if (memberType !== 'MEMBER') {
        //   Tips.confirm(`您还不是会员，成为会员可享受更多权益！`, '', '提示', '返回', '前往注册').then(() => {
        //     wx.navigateTo({
        //       url: `/pages/memberSign/memberSign`
        //     });
        //   }).catch(() => {
        //     wx.navigateBack({
        //       delta: 1
        //     })
        //   })
        //   return
        // }
      } else {
        this.messageComponent.applyActive(data.msg || '请求数据失败！')
      }
    }).catch(err => {
      console.log(err)
    })
  },

  // 处理奖励设置
  handleAward(award) {
    let point = award.point;
    let gift = award.gift;
    let coupon = award.coupon;
    let awardArr = [];
    let awardStr = '';
    if (point) awardArr.push(`${point}积分`);
    if (gift.length) {
      gift.forEach(item => {
        awardArr.push(item.giftName);
      });
    }
    if (coupon.length) {
      coupon.forEach(item => {
        awardArr.push(item.couponName);
      });
    }
    if (awardArr.length) {
      awardStr = awardArr.join('、')
    }
    console.log(awardArr);
    this.setData({
      awardStr
    })
  },

  // 输入内容
  handleInput(e) {
    let index = e.currentTarget.dataset.index;
    let options = this.data.options;
    let _textContent = `options[${index}].textContent`;
    this.setData({
      [_textContent]: e.detail.value
    });
  },

  // 单选
  radioChange(e) {
    let options = this.data.options;
    let valueStr = e.detail.value;
    let valueArr = valueStr.split('|');
    let checkedId = valueArr[0];
    let optionId = valueArr[1];
    let optionCurrentLists = options.find((val) => val.id == optionId);
    let optionCurrentList = optionCurrentLists.option.item_list;
    optionCurrentList.forEach((val) => {
      val.id == checkedId ? val.checked = true : val.checked = false
    });
    this.setData({
      options
    })
  },

  // 多选
  checkboxChange(e) {
    let valueArr = e.detail.value;
    let index = e.currentTarget.dataset.index;
    let options = this.data.options;
    let optionId = '';
    let checkedId = valueArr.map((val) => {
      optionId = val.split('|')[1];
      return val.split('|')[0]
    });
    if (checkedId.length) {
      let optionCurrentLists = options.find((val) => val.id == optionId);
      let optionCurrentList = optionCurrentLists.option.item_list;
      for (let i = 0; i < optionCurrentList.length; i++) {
        if (checkedId.indexOf((optionCurrentList[i].id).toString()) !== -1) {
          optionCurrentList[i].checked = true
        } else {
          optionCurrentList[i].checked = false
        }
      }
    } else {
      let optionCurrentList = this.data.options[index].option.item_list;
      optionCurrentList.forEach(item => {
        item.checked = false
      })
    }

    this.setData({
      options
    })
  },

  // 倒计时
  countDownTimes(data) {
    let timeStamp = new Date(data.replace(/-/g, '/')).getTime();
    let now = Date.now();
    let time = (timeStamp - now) / 1000;
    if (time <= 0) {
      return false;
    } else {
      this.countDownTime(data);
    }
  },

  // 倒计时
  countDownTime(times) {
    if (!times || times < 0) {
      return
    }
    let timeStamp = new Date(times.replace(/-/g, '/')).getTime();
    let timer = null;
    timer = setInterval(() => {
      let now = Date.now();
      let time = (timeStamp - now) / 1000;
      if (time <= 0) {
        this.setData({
          timeText: ''
        });
        // 刷新页面
        this.getQuestionDetail(this.data.id);
        clearInterval(timer);
        return
      }
      let s = parseInt((time) % 60);
      let m = parseInt((time / 60) % 60);
      let h = parseInt((time / 60 / 60) % 24);
      let d = parseInt((time / 60 / 60) / 24);
      this.setData({
        timeText: `${d > 0 ? d + '天' : ''}${h < 10 ? '0' + h : h}:${m < 10 ? '0' + m : m}:${s < 10 ? '0' + s : s}`
      })
    }, 1000)
  },

  // 提交表单
  handleToSubmit() {
    if (!this.validate()) return false;
    if (!this.validateCheckboxLength()) return false;
    this.submitData();
    Tips.loading('提交中...');
    setTimeout(() => this.saveData(), 500);
  },

  // 保存数据
  saveData() {
    let params = {
      data: this.data.submitDataStr,
      id: this.data.naire.questionnaireId
    };
    console.log(params);
    question.questionSubmit(params).then(res => {
      Tips.loaded();
      let data = res.data;
      if (data.flag) {
        Tips.success('提交成功', 500).then(() => {
          //let str = !this.data.awardStr ? '答卷已提交成功，感谢您的参与。' : '答卷已提交成功，奖励已发放至“我的-卡券”，请及时使用。';
          let str = '答卷已提交成功，奖励已发放至“我的-卡券”，请及时使用。';// 客户要求不判断条件了，modify by sundc 2021/05/28
          Tips.confirm(str, {}, '温馨提示', '', '知道了').then(() => {
            this.getQuestionDetail(this.data.id);
          })
        });
      } else {
        this.messageComponent.applyActive(data.msg || '提交数据失败！')
      }
    }).catch(err => {
      console.log(err)
    })
  },

  // 表单检验
  validate() {
    let options = this.data.options;
    // 校验必填项
    for (let i = 0; i < options.length; i++) {
      if (options[i].need_answer) {
        if (options[i].type !== 'MULTIPLE') {
          let require = options[i].option.item_list.some(function (x) {
            return x.checked == true;
          });
          if (!require) {
            this.messageComponent.applyActive(`第${i + 1}个问题为必填项!`);
            return false;
          }

        } else {
          if (!options[i].textContent) {
            this.messageComponent.applyActive(`第${i + 1}个问题为必填项!`);
            return false;
          }
        }
      }
    }
    return true;
  },


  // 校验多选的个数
  validateCheckboxLength() {
    let options = this.data.options;
    // 校验必填项
    for (let i = 0; i < options.length; i++) {
      // 多选
      if (options[i].type == 'CHECKBOX') {
        let only_number = options[i].option.only_number;
        let many_number = options[i].option.many_number;
        let checkArr = options[i].option.item_list.filter(function (item) {
          return item.checked == true
        });
        console.log(checkArr);
        let checkLength = checkArr.length;
        console.log(checkLength);
        if (!checkLength) return true;
        if (only_number && many_number) {
          if (checkLength < only_number) {
            this.messageComponent.applyActive(`第${i + 1}个问题最少选${only_number}项!`);
            return false;
          } else if (checkLength > many_number) {
            this.messageComponent.applyActive(`第${i + 1}个问题最多选${many_number}项!`);
            return false;
          }
        } else if (only_number && !many_number) {
          if (checkLength < only_number) {
            this.messageComponent.applyActive(`第${i + 1}个问题最少选${only_number}项!`);
            return false;
          }
        } else if (!only_number && many_number) {
          if (checkLength > many_number) {
            this.messageComponent.applyActive(`第${i + 1}个问题最多选${many_number}项!`);
            return false;
          }
        }
      }
    }
    return true;
  },

  // 提交表单数据拼接
  submitData() {
    let options = this.data.options;
    let submitDataArr = [];
    options.forEach((item) => {
      if (item.type !== 'MULTIPLE') {
        let require = item.option.item_list.some(function (x) {
          return x.checked == true;
        });
        if (require) {
          item.option.item_list.forEach((val) => {
            if (val.checked) {
              submitDataArr.push(`${val.id}|${val.option_name}|${item.id}|${item.title}`)
            }
          })
        }
      } else {
        submitDataArr.push(`${item.id}|${item.textContent}|${item.id}|${item.title}`)
      }
    });
    let submitDataStr = submitDataArr.join(';');
    this.setData({
      submitDataStr
    })
  },

  // 分享
  onShareAppMessage() {
    // 转发成功之后记录分享的次数
    shareLog.shareLogCommon(this.data.id, 'QUESTION');
    let organizeId = wx.getStorageSync('organizeId'),
      organizeName = wx.getStorageSync('organizeName');
    return {
      path: '/pages/questionDetail/questionDetail?id=' + this.data.id + '&organizeId=' + organizeId + '&organizeName=' + organizeName + '&share=true'
    }
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(option) {
    this.messageComponent = this.selectComponent('.messageTips');
    if (option.scene) {
      let scene = decodeURIComponent(option.scene);
      let id = query.getQueryString('id', scene);
      this.setData({
        id: id
      });
    } else {
      this.setData({
        id: option.id
      });
    }
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {
    this.getQuestionDetail(this.data.id);
  }
});