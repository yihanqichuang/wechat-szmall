// 获取全局应用程序实例对象
// const app = getApp()
const activityApi = require('../../api/activity.js');
import { fetchOrganizeList } from '../../api/markets'
// 创建页面实例对象
Page({
  /**
   * 页面的初始数据
   */
  data: {
    tabs: [
      {
        name: '待参加',
        type: '0'
      },
      {
        name: '全部活动',
        type: '1'
      }
    ],
    activeIndex: 0,
    navSlider: {
      width: '40rpx',
      left: '165rpx',
      position: ['165rpx', '540rpx']
    },
    listUnuse: [],
    pageUnuse: {
      pageNum: 1,
      pageSize: 10,
      total: 1
    },
    listAll: [],
    pageAll: {
      pageNum: 1,
      pageSize: 10,
      total: 1
    },
    emptyType: 'activity',
    bottomTipsFlag: true,
    messageComponent: '',
    isMember: false,
    organizeList: [],
    theOrganizeId: ''
  },
  // 获取组织列表
  getOrganizeList() {
    let params = {
    };
    fetchOrganizeList(params).then(res => {
      console.log('获取组织列表', res)
      if (res.data.flag) {
        let organizeList = res.data.data;
        let list = [{
          organizeId: '',
          organizeName: '全部门店'
        }];
        organizeList.forEach(item => {
          console.log('item', item.organizeList);
          list.push(...item.organizeList);
        });
        if (organizeList && organizeList.length > 0) {
          this.setData({
            organizeList: list
          });
        }
      } else {
        this.messageComponent.applyActive(res.data.msg || '获取列表数据异常');
      }
    }).catch(err => {
      console.log(err)
    })
  },
  onChooseChanges(event) {
    console.log('获取列表', event);
    this.setData({
      theOrganizeId: event.detail.organizeId ? event.detail.organizeId : ''
    });
    this.getExchangeList('0', 1);
    // this.getList(1,event.detail.organizeId)
  },
  // 改变 tab 点击头部
  changeIndex(data) {
    let current = data.detail;
    this.setData({
      activeIndex: current
    });
  },
  // nav-slider 的位置
  sliderPosition(index) {
    this.setData({
      'navSlider.left': this.data.navSlider.position[index]
    })
  },
  // 改变 swiper 手势滑动
  swiperChange(event) {
    let current = event.detail.current;
    this.setData({
      activeIndex: current
    });
    this.sliderPosition(current);
    if (current == 0) {
      this.getExchangeList('0', 1)
    } else if (current == 1) {
      this.getExchangeList('1', 1)
    }
  },
  // 获取活动列表
  getExchangeList(queryType, pageNum) {
    let pageSize
    if (queryType == '0') {
      pageSize = this.data.pageUnuse.pageSize;
      if (pageNum === 1) this.setData({ listUnuse: [] })
    } else {
      pageSize = this.data.pageAll.pageSize
      if (pageNum === 1) this.setData({ listAll: [] })
    }
    let params = {
      type: Number(queryType),
      pageNum,
      pageSize,
      organizeId: this.data.theOrganizeId,
      memberId: wx.getStorageSync('memberId')
    };
    activityApi.activityList(params).then(res => {
      if (!res.data.flag) {
        let msg = res.data.msg || '请求数据失败';
        this.messageComponent.applyActive(msg);
        return
      }
      if (queryType === '0') {
        res.data.data.forEach((item) => {
          item.activityStarttimeStr = item.activityStarttime.substr(0, 10).replace(/-/g, '.');
          item.activityEndtimeStr = item.activityEndtime.substr(0, 10).replace(/-/g, '.');
          item.signupTime = item.signupTime.replace(/-/g, '.');
        });
        this.setData({
          listUnuse: [...this.data.listUnuse, ...res.data.data],
          pageUnuse: res.data.page
        })
      } else if (queryType === '1') {
        res.data.data.forEach((item) => {
          item.activityStarttimeStr = item.activityStarttime.substr(0, 10).replace(/-/g, '.');
          item.activityEndtimeStr = item.activityEndtime.substr(0, 10).replace(/-/g, '.');
          item.signupTime = item.signupTime.replace(/-/g, '.');
        });
        this.setData({
          listAll: [...this.data.listAll, ...res.data.data],
          pageAll: res.data.page
        })
      }
    }, () => {
      this.messageComponent.applyActive('请求数据失败！');
      console.log('err')
    })
  },
  // 跳转到详情
  toExchange(event) {
    let useState = event.currentTarget.dataset.usestate;
    let gainId = event.currentTarget.dataset.gainid;
    console.log(useState);
    wx.navigateTo({
      url: `../activityCode/activityCode?activityId=${gainId}`
    })
  },

  // 加载更多
  loadMore(e) {
    let queryType = e.currentTarget.dataset.type;
    if (queryType == '0') {
      this.getExchangeList('0', e.detail)
    } else if (queryType == '1') {
      this.getExchangeList('1', e.detail)
    }
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(e) {
    this.messageComponent = this.selectComponent('.messageTips');
    this.getOrganizeList();
    if (e.isFirst) {
      this.setData({
        isFirst: e.isFirst
      })
    }
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {
    // TODO: onShow
    let activityList = wx.getStorageSync('activityList');
    let isMember = wx.getStorageSync('memberType');
    if (isMember == 'MEMBER') {
      this.setData({
        isMember: true
      })
    } else {
      this.setData({
        isMember: false
      })
    }
    console.log(activityList);
    this.getExchangeList('0', 1);
    // if (activityList == 'activityList' || this.data.isFirst) {
    //   wx.removeStorageSync('activityList');
    //   this.getExchangeList('0', 1)
    // }
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {
    // TODO: onHide
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {
    // TODO: onUnload
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {
    // TODO: onPullDownRefresh
  },

  /**
   * 页面相关分享
   */
  onShareAppMessage() {

  }
})
