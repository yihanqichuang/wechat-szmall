// 获取全局应用程序实例对象
const app = getApp();
import { PHONE } from '../../utils/regular.js';
import Tips from '../../utils/tip.js';
import userApi from '../../api/user.js';
import api from '../../api/login.js';
import wechat from '../../utils/wechat';
import { REGISTERMESSAGE } from '../../vendor.js';
// 创建页面实例对象
Page({
  /**
   * 页面的初始数据
   */
  data: {
    title: 'bindCard',
    phoneValue: '',
    codeValue: '',
    cardValue: '',
    nameValue: '',
    shopValue: '',
    sexValue: '',
    sexValueType: '',
    codeBtnText: '发送验证码',
    isDisabled: false,
    time: 60,
    timer: '',
    sysCode: '',
    isAuthorize: false,
    isGetUserInfo: false, // 是否用户授权过
    isLoading: false,
    memberPhone: '',
    messageComponent: '',
    checked: true,
    btnDisabled: false,
    // 注册送的积分
    points: 0,
    couponListPoints: 0,
    showDialog: false,
    // 终端类型，包括iPhone, android,其他
    platform: '',
    // 机型，如苹果iPhone6, OPPO R9等
    device: '',
    visible: false,
    visibleSex: false,
    memberBirthday: '',
    actions: [
      {
        name: '丹尼斯购物广场'
      },
      {
        name: '枝江购物广场'
      },
      {
        name: '新天地购物广场'
      }
    ],
    actionsSex: [
      {
        name: '男',
        type: 'MAN'
      },
      {
        name: '女',
        type: 'WOMAN'
      }
    ],
    authorDialog: false,
    telephone: '', // 客服电话
    tips: '', // 温馨提示
    inviteCourteousCode: null
  },

  // 打开会员章程
  handleOpenPdf() {
    wx.navigateTo({
      url: '/pages/signAgreement/signAgreement'
    })
  },

  // 手机号码输入
  handlePhoneInput(e) {
    this.setData({
      phoneValue: e.detail.value
    });
    if (this.data.phoneValue.length == 11) {
      // this.getCrmMember()
    }
  },

  // 身份证号码输入
  handleCardInput(e) {
    this.setData({
      cardValue: e.detail.value
    });
  },

  // 姓名输入
  handleNameInput(e) {
    this.setData({
      nameValue: e.detail.value
    });
  },

  // 验证码输入
  handleCodeInput(e) {
    this.setData({
      codeValue: e.detail.value
    });
  },

  // 打开商户选项
  handleOpenAction() {
    this.setData({
      visible: true
    });
  },

  // 关闭商户选项
  handleCancel() {
    this.setData({
      visible: false
    });
  },

  // 选择商户
  handleClickItem({ detail }) {
    const index = detail.index;
    this.setData({
      shopValue: this.data.actions[index].name,
      visible: false
    })
  },

  // 打开性别选项
  handleOpenActionSex() {
    this.setData({
      visibleSex: true
    });
  },

  // 关闭性别选项
  handleCancelSex() {
    this.setData({
      visibleSex: false
    });
  },

  // 选择性别
  handleClickItemSex({ detail }) {
    const index = detail.index;
    this.setData({
      sexValue: this.data.actionsSex[index].name,
      sexValueType: this.data.actionsSex[index].type,
      visibleSex: false
    })
  },

  // 选择生日
  bindBirthChange(e) {
    this.setData({
      memberBirthday: e.detail.value
    });
  },

  // 勾选协议
  handleCheckBox() {
    this.setData({
      checked: !this.data.checked
    })
  },

  // 获取验证码
  handleSendCode() {
    // 验证手机号
    if (!this.data.phoneValue) {
      this.messageComponent.applyActive('请输入手机号码');
      return false;
    } else if (!PHONE.test(this.data.phoneValue)) {
      this.messageComponent.applyActive('手机格式不正确');
      return false;
    } else {
      this.getCode()
    }
  },

  // 发送验证码
  getCode() {
    let params = {
      memberPhone: this.data.phoneValue,
      code: wx.getStorageSync('hash')
    };
    this.setData({
      isDisabled: true
    });
    userApi.getSecurityCode(params).then(res => {
      if (res.data.flag) {
        this.handleSuccess()
      } else {
        this.messageComponent.applyActive(res.data.msg);
        this.setData({
          isDisabled: false
        })
      }
    }).catch(err => {
      this.setData({
        isDisabled: false
      });
      this.messageComponent.applyActive(err)
    })
  },

  // 发送成功需要的倒计时
  handleSuccess() {
    let time = this.data.time;
    let timer;
    this.setData({
      codeBtnText: `${time}S后重试`
    });
    timer = setInterval(() => {
      if (time === 1) {
        this.setData({
          isDisabled: false,
          codeBtnText: '发送验证码'
        });
        clearInterval(timer);
        return
      }
      time = time - 1;
      this.setData({
        codeBtnText: `${time}S后重试`
      })
    }, 1000)
  },

  // 关闭注册成功弹窗
  handleCloseDialog() {
    this.setData({
      showDialog: false,
      isLoading: false,
      btnDisabled: false
    });
    // 促销活动注册完成后跳转商品列表
    if (this.data.inviteType && this.data.inviteType === 'MARKET') {
      if (this.data.organizeType === 'SuperMarket') {
        wx.switchTab({ url: `/pages/superMarket/superMarket` })
      } else {
        wx.switchTab({ url: `/pages/homeMarket/home` })
      }
    } else {
      wx.navigateBack({
        delta: 1
      });
    }
  },

  // 关闭授权弹窗
  handleCloseAuthorDialog() {
    this.setData({
      authorDialog: false
    });
    wx.navigateBack({
      delta: 1
    });
  },

  // 身份证校验函数
  identityCodeValid(code) {
    let city = {
      11: '北京',
      12: '天津',
      13: '河北',
      14: '山西',
      15: '内蒙古',
      21: '辽宁',
      22: '吉林',
      23: '黑龙江 ',
      31: '上海',
      32: '江苏',
      33: '浙江',
      34: '安徽',
      35: '福建',
      36: '江西',
      37: '山东',
      41: '河南',
      42: '湖北 ',
      43: '湖南',
      44: '广东',
      45: '广西',
      46: '海南',
      50: '重庆',
      51: '四川',
      52: '贵州',
      53: '云南',
      54: '西藏 ',
      61: '陕西',
      62: '甘肃',
      63: '青海',
      64: '宁夏',
      65: '新疆',
      71: '台湾',
      81: '香港',
      82: '澳门',
      91: '国外 '
    };
    let tip = '';
    let pass = true;

    if (!/^\d{6}(18|19|20)?\d{2}(0[1-9]|1[0-2])(0[1-9]|[12]\d|3[01])\d{3}(\d|X)$/i.test(code)) {
      tip = '身份证号格式错误';
      pass = false;
    } else if (!city[code.substr(0, 2)]) {
      tip = '身份证地址编码错误';
      pass = false;
    } else {
      // 18位身份证需要验证最后一位校验位
      if (code.length == 18) {
        code = code.split('');
        // ∑(ai×Wi)(mod 11)
        // 加权因子
        let factor = [7, 9, 10, 5, 8, 4, 2, 1, 6, 3, 7, 9, 10, 5, 8, 4, 2];
        // 校验位
        let parity = [1, 0, 'X', 9, 8, 7, 6, 5, 4, 3, 2];
        let sum = 0;
        let ai = 0;
        let wi = 0;
        for (let i = 0; i < 17; i++) {
          ai = code[i];
          wi = factor[i];
          sum += ai * wi;
        }
        let last = parity[sum % 11];
        if (last != code[17]) {
          tip = '身份证校验位错误';
          pass = false;
        }
      }
    }
    if (!pass) this.messageComponent.applyActive(tip);
    return pass;
  },

  // 获取商场
  getNearestOrganiz() {
    return new Promise((resolve) => {
      let localId = wx.getStorageSync('organizeId');
      if (localId) {
        resolve();
        return false;
      }
      // 邀请有礼设置组织为活动组织
      if (this.data.inviteType && this.data.organizeId) {
        wx.setStorageSync('organizeId', this.data.organizeId);
        wx.setStorageSync('organizeName', this.data.organizeName);
        resolve();
        return false;
      }
      api.nearestOrganiz().then(res => {
        if (res.data.flag) {
          let id = res.data.data.organizeId;
          let name = res.data.data.organizeName;
          wx.setStorageSync('organizeId', id);
          wx.setStorageSync('organizeName', name);
          resolve();
        }
      })
    })
  },

  // 绑定银行卡
  handleBindCard() {
    // 验证手机号
    if (!this.data.phoneValue) {
      this.messageComponent.applyActive('请输入手机号码');
      return false;
    } else if (!PHONE.test(this.data.phoneValue)) {
      this.messageComponent.applyActive('手机格式不正确');
      return false;
    }

    // 身份证校验
    // if (!this.data.cardValue) {
    //   this.messageComponent.applyActive('请输入身份证号码');
    //   return false;
    // } else if (!this.identityCodeValid(this.data.cardValue)) {
    //   return false;
    // }

    // 姓名校验
    if (!this.data.nameValue && this.data.isShowBlock) {
      this.messageComponent.applyActive('请输入姓名');
      return false;
    }

    // 商户校验
    // if (!this.data.shopValue) {
    //   this.messageComponent.applyActive('请选择注册所在商城');
    //   return false;
    // }

    // 性别校验
    if (!this.data.sexValueType && this.data.isShowBlock) {
      this.messageComponent.applyActive('请选择性别');
      return false;
    }

    // 生日校验
    if (!this.data.memberBirthday && this.data.isShowBlock) {
      this.messageComponent.applyActive('请选择生日');
      return false;
    }

    // 验证码校验
    if (!this.data.isAuthorize && !this.data.codeValue) {
      this.messageComponent.applyActive('请输入验证码');
      return false;
    }

    // 协议是否勾选
    if (!this.data.checked) {
      this.messageComponent.applyActive('请勾选会员章程');
      return false;
    }

    let params = {
      memberPhone: this.data.phoneValue,
      memberSex: this.data.sexValueType,
      memberBirthday: this.data.memberBirthday,
      memberName: this.data.nameValue,
      isValidate: this.data.isAuthorize ? 'Y' : 'N',
      vircode: this.data.isAuthorize ? '' : this.data.codeValue,
      platform: this.data.platform,
      device: this.data.device
    };
    this.setData({
      isLoading: true,
      btnDisabled: true
    });
    // this.getNearestOrganiz().then(() => {
    userApi.register(params).then(async res => {
      this.setData({
        isLoading: false,
        btnDisabled: false
      })
      if (res.data.flag) {
        app.data.memberType = 'MEMBER';
        app.globalData.memberPhone = this.data.phoneValue;
        wx.setStorageSync('memberPhone', this.data.phoneValue);
        wx.setStorageSync('memberType', 'MEMBER');
        if (res.data.couponList) {
          let couponList = res.data.couponList;
          let couponListPoints = 0;
          if (couponList && couponList.length != 0) {
            // 只需要券的张数
            couponListPoints = couponList.length
          }
          this.setData({
            couponListPoints: couponListPoints
          });
        }
        if (res.data.points) {
          let points = res.data.points;
          this.setData({
            points: points && points != '' ? points : 0
          })
        }
        //重新获取用户信息
        await this.getUserInfo();
        // 如果没有送积分和券
        if (!res.data.points && !res.data.couponList) {
          Tips.success('注册成功', 1000).then(() => {
            // 促销活动注册完成后跳转商品列表
            if (this.data.inviteType && this.data.inviteType === 'MARKET') {
              if (this.data.organizeType === 'SuperMarket') {
                wx.switchTab({ url: `/pages/superMarket/superMarket` })
              } else {
                wx.switchTab({ url: `/pages/homeMarket/home` })
              }
            } else {
              wx.navigateBack({
                delta: 1
              });
            }
          })
        } else {
          this.setData({
            showDialog: true
          });
        }
      } else {
        // if (res.data.code == 4010) {
        //   Tips.confirm(`获取微信unionId失败，需要重新进行微信登录授权？`, '', '提示', '取消', '确认').then(() => {
        //     wx.navigateTo({
        //       url: '/pages/author/author'
        //     })
        //   }).catch(() => {
        //   })
        // } else {
        this.messageComponent.applyActive(res.data.msg || '注册失败');
        // }
      }
    }).catch(err => {
      this.setData({
        isLoading: false,
        btnDisabled: false
      })
    })
    // })
  },

  async getUserInfo() {
    const loginRes = await wechat.login();
    if (!loginRes.code) {
      Tip.toast('获取code失败', null, 'none');
      return;
    }
    let url = 'szmall/oauth/token/' + loginRes.code;
    let param = {
      appId: '${appId}',
      appType: 'smallApp'
    };
    wx.setStorageSync('code', loginRes.code);
    wx.setStorageSync('appId', param.appId);
    // 调用登陆
    const tokenRes = await api.login(url, param);
    if ("00000" === tokenRes.data.code) {
      wx.setStorageSync('token', tokenRes.data.data.access_token);
      // 获取用户信息
      const userRes = await api.getUserInfo();
      wx.setStorageSync('userInfo', userRes.data);
    } else {
      Tip.toast(tokenRes.data.msg, null, 'none');
    }
  },

  // 拨打电话
  handlePhoneCall(e) {
    let phone = e.currentTarget.dataset.phone;
    wx.makePhoneCall({
      phoneNumber: phone
    })
  },

  // 登陆 (调用getPhoneNumber必须要先调用login方法)
  login() {
    wechat.login().then(res => {
      this.setData({
        code: res.code
      })
    })
  },

  // 获取手机号
  getPhoneNumber(e) {
    wx.checkSession({
      success() { },
      fail() {
        this.login()
      }
    })
    let that = this;
    if (e.detail.errMsg == 'getPhoneNumber:fail user deny') {
    } else {
      if (!e.detail.encryptedData) return false;
      if (this.data.isAgree) return false;
      let params = {
        encryptedData: e.detail.encryptedData,
        ivStr: e.detail.iv,
        code: that.data.code,
        appId: wx.getStorageSync('appId')
      };
      userApi.getPhone(params).then(res => {
        if (res.data.flag) {
          wx.setStorageSync('memberPhone', res.data.data.phoneNumber);
          wx.setStorageSync('isAuthorize', true);
          this.setData({
            isAgree: true,
            phoneValue: res.data.data.phoneNumber,
            authorDialog: false,
            isAuthorize: true
          })
          if (!this.data.isShowBlock) {
            this.handleBindCard();
          } else {
            // this.getCrmMember();
          }
        } else {
          this.messageComponent.applyActive('获取手机号码失败!');
          this.setData({
            isAgree: false
          })
        }
      }).catch(err => {
        this.setData({
          isAgree: false
        })
      })
    }
  },

  // 使用其他手机号码
  getOtherMemberPhone() {
    this.setData({
      authorDialog: false,
      isAuthorize: false
    })
  },

  // 获取CRM线下会员信息
  getCrmMember() {
    wx.showLoading({
      title: '加载中'
    })
    let params = {
      memberPhone: this.data.phoneValue
    };
    userApi.getCrmMember(params).then(res => {
      if (res.data.flag) {
        this.setData({
          nameValue: res.data.data.memberName,
          sexValue: res.data.data.memberSexStr,
          sexValueType: res.data.data.memberSex,
          memberBirthday: res.data.data.memberBirthdayStr
        })
      } else {
        this.setData({
          nameValue: '',
          sexValue: '',
          sexValueType: '',
          memberBirthday: ''
        })
      }
      wx.hideLoading()
    }).catch(err => {
      wx.hideLoading()
    })
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(option) {
    this.messageComponent = this.selectComponent('.messageTips');
    this.setData({
      //   showDialog: true,
      authorDialog: true,
      inviteCourteousCode: option.inviteCourteousCode,
      inviteType: option.inviteType,
      organizeType: option.organizeType,
      organizeId: option.organizeId,
      organizeName: option.organizeName
    })
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {
    this.login();
    // if (app.globalData.defaultConfig) {
    //   this.setData({
    //     telephone: app.globalData.defaultConfig.telephone,
    //     tips: app.globalData.defaultConfig.tips
    //   })
    // }
    wechat.getSystemInfo().then(res => {
      this.setData({
        platform: res.platform,
        device: res.model,
        isShowBlock: Boolean(REGISTERMESSAGE * 1)
      })
    })
    // 通过wx.getSetting判断用户是否已经授权过了,如果授权过了则可以直接调用 getUserInfo 获取头像昵称，不会弹框
    if (app.globalData.userInfo) {
      this.setData({
        isGetUserInfo: true
      })
    } else {
      wx.navigateTo({
        url: '/pages/author/author'
      })
    }
  }
});
