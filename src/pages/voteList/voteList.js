// 获取全局应用程序实例对象
// const app = getApp()
import voteApi from '../../api/vote.js'
import {fetchOrganizeList} from '../../api/markets'
// 创建页面实例对象
Page({
  /**
   * 页面的初始数据
   */
  data: {
    tabs: [
      {
        name: '全部',
        type: ''
      },
      {
        name: '报名中',
        type: 'ENROLLING'
      },
      {
        name: '报名失败',
        type: 'FAIL'
      },
      {
        name: '报名成功',
        type: 'SUCCESS'
      }
    ],
    activeIndex: 0,
    navSlider: {
      width: '40rpx',
      left: '60rpx',
      position: ['56rpx', '220rpx', '416rpx', '630rpx']
    },
    // 全部
    listAll: [],
    pageAll: {
      pageNum: 1,
      pageSize: 10,
      total: 1
    },
    // 报名中
    listIng: [],
    pageIng: {
      pageNum: 1,
      pageSize: 10,
      total: 1
    },
    // 报名成功
    listSuccess: [],
    pageSuccess: {
      pageNum: 1,
      pageSize: 10,
      total: 1
    },
    // 报名成功
    listFail: [],
    pageFail: {
      pageNum: 1,
      pageSize: 10,
      total: 1
    },
    emptyType: 'voteList',
    organizeList: [],
    theOrganizeId: ''
  },
  // 获取组织列表
  getOrganizeList() {
    let params = {
    };
    fetchOrganizeList(params).then(res => {
      console.log('获取组织列表', res)
      if (res.data.flag) {
        let organizeList = res.data.data;
        let list = [{
          organizeId: '',
          organizeName: '全部门店'
        }];
        organizeList.forEach(item => {
          console.log('item', item.organizeList);
          list.push(...item.organizeList);
        });
        if (organizeList && organizeList.length > 0) {
          this.setData({
            organizeList: list
          });
        }
      } else {
        this.messageComponent.applyActive(res.data.msg || '获取列表数据异常');
      }
    }).catch(err => {
      console.log(err)
    })
  },
  onChooseChanges(event) {
    console.log('获取列表', event);
    this.setData({
      theOrganizeId: event.detail.organizeId ? event.detail.organizeId : ''
    });
    this.getVoteList('', 1);
    // this.getList(1,event.detail.organizeId)
  },
  // 改变 tab 点击头部
  changeIndex(data) {
    let current = data.detail;
    this.setData({
      activeIndex: current
    });
    this.sliderPosition(current)
    this.getVoteList(this.data.tabs[current].type, 1)
  },
  // nav-slider 的位置
  sliderPosition(index) {
    this.setData({
      'navSlider.left': this.data.navSlider.position[index]
    })
  },
  // 改变 swiper 手势滑动
  swiperChange(event) {
    let current = event.detail.current;
    this.setData({
      activeIndex: current
    });
    this.sliderPosition(current);
    this.getVoteList(this.data.tabs[current].type, 1)
  },

  // 获取投票活动列表
  getVoteList(queryType, pageNum) {
    if (queryType == '') {
      if (pageNum === 1) this.setData({ listAll: [] })
    } else if (queryType == 'ENROLLING') {
      if (pageNum === 1) this.setData({ listIng: [] })
    } else if (queryType == 'SUCCESS') {
      if (pageNum === 1) this.setData({ listSuccess: [] })
    } else if (queryType == 'FAIL') {
      if (pageNum === 1) this.setData({ listFail: [] })
    }

    let params = {
      enrollState: queryType,
      organizeId: this.data.theOrganizeId,
      pageNum: pageNum
    };
    console.log(params);
    voteApi.voteEnrollList(params).then(res => {
      if (!res.data.flag) {
        let msg = res.data.msg || '请求数据失败';
        this.messageComponent.applyActive(msg);
        return
      }
      res.data.data.forEach((item) => {
        item.picPath = item.picPath.split(',')[0];
      });
      if (queryType === '') {
        this.setData({
          listAll: [...this.data.listAll, ...res.data.data],
          pageAll: res.data.page
        })
      } else if (queryType === 'ENROLLING') {
        this.setData({
          listIng: [...this.data.listIng, ...res.data.data],
          pageIng: res.data.page
        })
      } else if (queryType === 'SUCCESS') {
        this.setData({
          listSuccess: [...this.data.listSuccess, ...res.data.data],
          pageSuccess: res.data.page
        })
      } else if (queryType === 'FAIL') {
        this.setData({
          listFail: [...this.data.listFail, ...res.data.data],
          pageFail: res.data.page
        })
      }
    }, () => {
      this.messageComponent.applyActive('请求数据失败！');
      console.log('err')
    })
  },

  // 加载更多
  loadMore(e) {
    let queryType = e.currentTarget.dataset.type;
    if (queryType == 'All') {
      this.getVoteList('', e.detail)
    } else if (queryType == 'Ing') {
      this.getVoteList('ENROLLING', e.detail)
    } else if (queryType == 'Success') {
      this.getVoteList('SUCCESS', e.detail)
    } else if (queryType == 'Fail') {
      this.getVoteList('FAIL', e.detail)
    }
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad() {
    this.getOrganizeList();
    this.messageComponent = this.selectComponent('.messageTips');
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {
    // TODO: onShow
    // 默认读取全部列表
    this.getVoteList('', 1);
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {
    // TODO: onHide
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {
    // TODO: onUnload
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {
    // TODO: onPullDownRefresh
  },

  /**
   * 页面相关分享
   */
  onShareAppMessage() {
    
  }
});
