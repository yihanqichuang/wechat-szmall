// 获取全局应用程序实例对象
const app = getApp()
import Tip from '../../../utils/tip';
import ShopApi from '../../../api/findshop';
import OrderApi from '../../../api/order';
import loginApi from '../../../api/login'

// 创建页面实例对象
Page({
    data: {
        showActionSheet: false,
        skuData: null,
        shopOrderNo: null,
        orderState: null,
        shopData: null,
        mallData: null,
        floorData: null,
        refundReason: '',
        refundPics: [],
        remark: '',
        groups: [],
        refundTypes: [{
            text: '退货退款',
            value: 'REFUND_ALL'
        },
        {
            text: '退款',
            value: 'REFUND_MONEY'
        }],
        showRrefundTypes:false,
        refundType: ''
    },
    onLoad(option) {
        if (option.packageProductCode) {
            this.setData({
                packageProductCode: option.packageProductCode
            })
        } else {
            this.setData({
                packageProductCode: ''
            })
        }
        const eventChannel = this.getOpenerEventChannel();
        eventChannel.on('passSku', (data) => {
            if (data.orderData) {
                // 如果是超市订单退款(整单退)
                this.setData({
                    skuData: Object.assign({}, data.orderData, {
                        refundType: 'ONLY_MONEY',
                        refundTarget: 'order' // 退款对象{订单}
                    })
                })
            } else if (data.sku.organizeType === 'SuperMarket') {
                // 超市订单退款（单商品退）
                this.setData({
                    skuData: Object.assign({}, data.sku, {
                        refundType: data.orderState === 'FINISH' ? 'GOOD_MONEY' : data.sku.refundType === 'REFUND_MONEY' ? 'ONLY_MONEY' : 'GOOD_MONEY'
                    })
                })
            } else if (data) {
                let skuData = {};
                if (data.sku.refundOrderNo) {
                    // 售后单进来
                    skuData.refundType = data.sku.refundType === 'REFUND_MONEY' ? 'ONLY_MONEY' : 'GOOD_MONEY';
                    skuData.productCount = data.sku.productCount;
                    skuData.otherPayPrice = data.sku.refundOtherPrice;
                    skuData.prepaidPayPrice = data.sku.refundPrepaidPrice;
                    skuData.actualPayPoints = data.sku.refundPoints;
                    skuData.dispatchType = data.sku.dispatchType;
                    skuData.shopOrderNo = data.sku.shopOrderNo;
                    skuData.productSkuCode = data.sku.productSkuCode;
                    skuData.shopId = data.sku.shopId;
                } else {
                    // 订单详情进来
                    skuData.refundType = (data.orderState === 'WAIT_RECEIVE' || data.orderState === 'FINISH') ? 'GOOD_MONEY' : 'ONLY_MONEY';
                    skuData.productCount = data.sku.productCount;
                    skuData.otherPayPrice = data.sku.otherPayPrice;
                    skuData.prepaidPayPrice = data.sku.prepaidPayPrice;
                    skuData.actualPayPoints = data.sku.actualPayPoints;
                    skuData.dispatchType = data.sku.dispatchType;
                    skuData.shopOrderNo = data.sku.shopOrderNo;
                    skuData.productSkuCode = data.sku.productSkuCode;
                    skuData.shopId = data.sku.shopId;
                    skuData.marketCode = data.sku.marketCode;
                    skuData.marketType = data.sku.marketType;
                    skuData.packageProductCode = data.sku.packageProductCode;
                }
                this.setData({
                    skuData
                });
            }
            // 订单已完成的情况下，申请退款要减去【退差价】金额
            if (data.orderState == 'FINISH') {
                let refundMoney = 0
                if (this.data.skuData.refundFlag == 'Y') {
                    refundMoney += this.data.skuData.refundPrice
                }
                if (this.data.skuData.otherPayPrice) {
                    this.data.skuData.otherPayPrice = (this.data.skuData.otherPayPrice - refundMoney).toFixed(2)
                } else {
                    this.data.skuData.prepaidPayPrice = (this.data.skuData.prepaidPayPrice - refundMoney).toFixed(2)
                }
                this.setData({
                    skuData: this.data.skuData
                });
            }
            this.sortRefundReasons();
            if (this.data.skuData.organizeType === 'SuperMarket') {
                // this.getSuperMarketData()
            } else {
                this.getShopData();
            }
        });
    },

    // 打开退款原因选择
    openActionSheet() {
        this.setData({
            showActionSheet: true
        });
    },
    // 打开退款类型选择
    openRefundTypeSheet() {
        this.setData({
            showRrefundTypes: true
        });
    },

    // 选择
    btnClick(e) {
        this.setData({
            refundReason: e.detail.value,
            showActionSheet: false
        });
    },
    refundTypeClick(e) {
        this.setData({
            refundType: e.detail.value,
            showRrefundTypes: false
        });
    },

    // 组织退款原因列表以及退款类型
    sortRefundReasons() {
        const reasons = {
            ONLY_MONEY_PICK_UP: [{
                text: '未提货',
                value: '未提货'
            },
            {
                text: '个人原因',
                value: '个人原因'
            },
            {
                text: '商品问题',
                value: '商品问题'
            },
            {
                text: '其它',
                value: '其它'
            }
            ],
            ONLY_MONEY_EXPRESS: [{
                text: '不喜欢/不想要了',
                value: '不喜欢/不想要了'
            },
            {
                text: '快递一直未送达',
                value: '快递一直未送达'
            },
            {
                text: '货物破损拒收',
                value: '货物破损拒收'
            },
            {
                text: '其它',
                value: '其它'
            }
            ],
            ONLY_MONEY_CITY_EXPRESS: [{
                text: '不喜欢/不想要了',
                value: '不喜欢/不想要了'
            },
            {
                text: '快递一直未送达',
                value: '快递一直未送达'
            },
            {
                text: '货物破损拒收',
                value: '货物破损拒收'
            },
            {
                text: '其它',
                value: '其它'
            }
            ],
            GOOD_MONEY_PICK_UP: [{
                text: '不喜欢/不想要了',
                value: '不喜欢/不想要了'
            },
            {
                text: '质量问题',
                value: '质量问题'
            },
            {
                text: '假冒问题',
                value: '假冒问题'
            },
            {
                text: '其它',
                value: '其它'
            }
            ],
            GOOD_MONEY_EXPRESS: [{
                text: '不喜欢/不想要了',
                value: '不喜欢/不想要了'
            },
            {
                text: '商品与描述不符',
                value: '商品与描述不符'
            },
            {
                text: '质量问题',
                value: '质量问题'
            },
            {
                text: '包装/商品破损',
                value: '包装/商品破损'
            },
            {
                text: '其它',
                value: '其它'
            }
            ],
            GOOD_MONEY_CITY_EXPRESS: [{
                text: '不喜欢/不想要了',
                value: '不喜欢/不想要了'
            },
            {
                text: '商品与描述不符',
                value: '商品与描述不符'
            },
            {
                text: '质量问题',
                value: '质量问题'
            },
            {
                text: '包装/商品破损',
                value: '包装/商品破损'
            },
            {
                text: '其它',
                value: '其它'
            }
            ]
        };
        this.setData({
            groups: reasons[`${this.data.skuData.refundType}_${this.data.skuData.dispatchType}`]
        });
    },

    // 上传图片
    handleUpLoad() {
        let that = this;
        let hostName = '${hostname}'//wx.getStorageSync('hostName') || app.data.hostName;
        let token = wx.getStorageSync('token');
        wx.chooseImage({
            count: 3 - that.data.refundPics.length, // 默认9
            sizeType: ['original', 'compressed'], // 可以指定是原图还是压缩图，默认二者都有
            sourceType: ['album', 'camera'], // 可以指定来源是相册还是相机，默认二者都有
            success: function (res) {
                // 返回选定照片的本地文件路径列表，tempFilePath可以作为img标签的src属性显示图片
                // var tempFilePaths = res.tempFilePaths[0];
                let imgList = [];
                res.tempFilePaths.forEach((item) => {
                    wx.uploadFile({
                        url: `${hostName}/szmall/user/fileupload/upload.json?filePosition=image`,
                        filePath: item,
                        name: 'image',
                        header: {
                            'Content-Type': 'multipart/form-data',
                            'Authorization': 'bearer ' + token
                        },
                        formData: {
                            'appType': 'smallApp',
                            'hash': wx.getStorageSync('hash')
                        },
                        success: function (res) {
                            let ret = JSON.parse(res.data);
                            if (ret.flag) {
                                imgList.push(ret.data.url);
                                that.setData({
                                    refundPics: imgList
                                })
                            } else {
                                wx.showModal({
                                    title: '提示',
                                    content: '上传失败',
                                    showCancel: false
                                });
                                return;
                            }
                        },
                        fail: function () {
                            wx.showModal({
                                title: '提示',
                                content: '上传失败',
                                showCancel: false
                            })
                        },
                        complete: function () {
                            // wx.hideToast();
                        }
                    })
                })
            }
        })
    },

    // 删除图片
    removePicture(e) {
        let index = e.currentTarget.dataset.index;
        let imgList = this.data.refundPics;
        imgList.splice(index, 1);
        this.setData({
            refundPics: imgList
        })
    },

    // 输入框输入的时候
    handleInputValue(e) {
        this.setData({
            remark: e.detail.value
        })
    },


    // 获取商铺详情
    getShopData() {
        ShopApi.fetchShopData({
            shopId: this.data.skuData.shopId
        }).then(res => {
            if (res.data.flag) {
                this.setData({
                    shopData: res.data.shop,
                    mallData: res.data.mall,
                    floorData: res.data.floor
                });
            }
        })
    },
    // // 获取超市详情
    // async getSuperMarketData() {
    //   try{
    //     const result = await loginApi.fetchOrganizeInfo({
    //       organizeId:this.data.skuData.organizeId
    //     })
    //     console.log('组织机构信息',result)
    //     if (result.data.flag) {

    //     } else {

    //     }
    //   } catch(err) {
    //     console.log(err)
    //   }
    // },

    // 提交申请
    async submitApply() {
        if (!this.data.refundType) {
            Tip.toast('请选择退款类型', null, 'none');
            return
        }
        if (!this.data.refundReason) {
            Tip.toast('请选择退款原因', null, 'none');
            return
        }
        // 超市的退款（整单退）
        if (this.data.skuData.organizeType === 'SuperMarket' && this.data.skuData.orderState === 'PAID') {
            try {
                Tip.loading()
                const params = {
                    orderNo: this.data.skuData.orderNo,
                    shopOrderNo: this.data.skuData.shopOrderNo,
                    productSkuCode: this.data.skuData.productSkuCode,
                    refundAddress: `${this.data.mallData ? this.data.mallData.detailAddress : ''}${this.data.floorData ? this.data.floorData.floorName : ''}`,
                    refundPhone: this.data.shopData ? this.data.shopData.shopPhone : '',
                    refundPics: this.data.refundPics.join(','),
                    refundReason: this.data.refundReason,
                    remark: this.data.remark,
                    refundType: this.data.refundType
                };
                const result = await OrderApi.fetchMainRefundApply(params);
                if (result.data.flag) {
                    Tip.toast(result.data.data.auditRemark, null, 'none');
                    const refundOrderNo = result.data.data.refundOrderNo;
                    wx.redirectTo({
                        url: `/pages/newPackage/orderRefundDetail/orderRefundDetail?refundOrderNo=${refundOrderNo}`
                    })
                } else {
                    Tip.toast(result.data.msg, null, 'none');
                }
            } catch (err) {
            } finally {
                Tip.loaded()
            }
        } else {
            // 百货的退款以及超市售后单个商品退
            const params = {
                shopOrderNo: this.data.skuData.shopOrderNo,
                productSkuCode: this.data.skuData.productSkuCode,
                refundAddress: `${this.data.mallData ? this.data.mallData.detailAddress : ''}${this.data.floorData ? this.data.floorData.floorName : ''}`,
                refundPhone: this.data.shopData ? this.data.shopData.shopPhone : '',
                refundPics: this.data.refundPics.join(','),
                refundReason: this.data.refundReason,
                remark: this.data.remark,
                marketCode: this.data.skuData.marketCode,
                marketType: this.data.skuData.marketType,
                packageProductCode: this.data.skuData.packageProductCode,
                refundType: this.data.refundType
            };
            Tip.loading();
            OrderApi.fetchRefundApply(params).then(res => {
                Tip.loaded();
                if (res.data.flag) {
                    Tip.toast(res.data.data.auditRemark, null, 'none');
                    // const refundOrderNo = res.data.refundDetail.refundOrderNo;
                    const refundOrderNo = res.data.data.refundOrderNo;
                    wx.redirectTo({
                        url: `/pages/newPackage/orderRefundDetail/orderRefundDetail?refundOrderNo=${refundOrderNo}&packageProductCode=${this.data.packageProductCode}`
                    })
                } else {
                    Tip.toast(res.data.msg, null, 'none');
                }
            })
        }
    }
});