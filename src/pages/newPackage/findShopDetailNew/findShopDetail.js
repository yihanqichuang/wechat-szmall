// 获取全局应用程序实例对象
const app = getApp()

import ShopApi from '../../../api/findshop'
import Tip from '../../../utils/tip'
import wechat from '../../../utils/wechat.js';
import SearchApi from '../../../api/search'

// 创建页面实例对象
Page({
    /**
     * 页面的初始数据
     */
    data: {
        loading: false,
        isCollect: false,
        showGifts: false,
        gifts: null,
        shopId: '',
        shopInfo: null,
        floorInfo: null,
        productList: [],
        COUPON: [],
        industryName: '',
        organizeName: '',
        bannerImg: '',
        messageComponent: '',
        shopPhone: null,
        defaultPic: '', // 默认图片
        pageNum: 1,
        pageSize: 10,
        isLoadOver: false,
        item: {
            moduleName: '优惠券'
        }
    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad(option) {
        this.setData({
            shopId: option.shopId,
            organizeName: wx.getStorageSync('organizeName'),
            //   defaultPic: app.globalData.defaultConfig.defaultPic
            defaultPic: ''
        })
        this.fetchShopDetail();
        // this.getCollectState();
        this.getProductList();
        this.getShopCoupon();
    },
    // 获取 列表数据
    getProductList() {
        let params = {
            shopId: this.data.shopId,
            pageNum: this.data.pageNum,
            pageSize: this.data.pageSize
        };
        this.setData({ loading: true });
        SearchApi.productList(params).then(res => {
            this.setData({ loading: false });
            console.log('店铺关联商品：', res);
            if (res.data.flag && res.data.data.data && res.data.data.data.length) {
                this.setData({
                    isLoadOver: (res.data.data.pages && res.data.data.pages <= this.data.pageNum),
                    [`productList[${this.data.productList.length}]`]: res.data.data.data
                });
            }
        })
    },
    loadMore() {
        if (this.data.isLoadOver) return
        this.setData({ pageNum: ++this.data.pageNum });
        this.getProductList();
    },
    // 获取收藏状态
    getCollectState() {
        const params = {
            type: 'SHOP',
            relateId: this.data.shopId
        };
        ShopApi.fetchCollectStatus(params)
            .then(res => {
                console.log('收藏状态：', res);
                if (res.data.flag) {
                    this.setData({ isCollect: res.data.data === 'Y' });
                }
            })
    },
    // 收藏/取消收藏
    toggleCollect() {
        this.setData({ isCollect: !this.data.isCollect });
        const params = {
            type: 'SHOP',
            relateId: this.data.shopId,
            isCollect: this.data.isCollect ? 'Y' : 'N'
        };
        ShopApi.fetchDoCollect(params)
            .then(res => {
                console.log('收藏结果：', res);
                if (res.data.flag) {
                    if (res.data.data.isReward === 'Y') {
                        this.setData({ gifts: res.data.data });
                        this.openGifts();
                    } else {
                        const txt = this.data.isCollect ? '已收藏' : '已取消';
                        Tip.toast(txt, null, 'none');
                    }
                } else {
                    Tip.toast(res.data.data, null, 'none');
                }
            })
    },
    // 获取店铺详情
    fetchShopDetail() {
        Tip.loaded();
        ShopApi.fetchShopData({ shopId: this.data.shopId }).then(res => {
            console.log('店铺详情：', res);
            Tip.loaded();
            if (res.data.flag) {
                this.setData({
                    industryName: res.data.shop.industryList.length ? res.data.shop.industryList[0].industryName : '',
                    shopInfo: res.data.shop,
                    floorInfo: res.data.floor,
                    bannerImg: res.data.shop.shopPicture || res.data.mall.organizePicturePath || '',
                    shopPhone: res.data.shop.shopPhone
                });
            }
        })
    },
    // 拨打商户电话
    handleCall() {
        if (!this.data.shopPhone) {
            this.messageComponent.applyActive('该商户没有联系电话~')
        } else {
            wechat.makePhoneCall(this.data.shopPhone).then(() => {
                console.log('拨打成功')
            }).catch(() => {
                console.log('拨打失败')
            })
        }
    },
    // 获取 商户关联的 优惠券
    getShopCoupon() {
        ShopApi.fetchShopCoupons({ shopId: this.data.shopId }).then(res => {
            console.log('优惠券：', res);
            if (res.flag) {
                this.setData({
                    COUPON: res.data
                });
            }
        })
    },
    // 显示收藏有礼弹窗
    openGifts() {
        this.setData({ showGifts: true });
    },
    // 关闭收藏有礼弹窗
    closeGifts() {
        this.setData({ showGifts: false });
    },

    onReachBottom() {
        this.loadMore();
    }
})
