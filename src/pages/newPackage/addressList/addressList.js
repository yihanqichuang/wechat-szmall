// 获取全局应用程序实例对象
// const app = getApp()
import AddressApi from '../../../api/addressApi'
import Tip from '../../../utils/tip'

// 创建页面实例对象
Page({
  data: {
    addressList: null
  },
  // 获取地址列表
  getAddressList() {
    Tip.loading();
    AddressApi.fetchAddressList().then(res => {
      console.log('地址列表：', res);
      Tip.loaded();
      if (res.data.flag) {
        this.setData({
          addressList: res.data.data
        });
      }
    })
  },

  // 前往编辑地址
  editAddress(e) {
    let currentAddress = e.currentTarget.dataset.address;
    if (currentAddress) {
      wx.setStorageSync('currentAddress', currentAddress);
      let url = '/pages/newPackage/addressManage/addressManage?type=edit';
      wx.navigateTo({ url });
    }
  },

  // 选择地址
  pickAddress(e) {
    const address = e.currentTarget.dataset.address;
    if (!address) return
    const eventChannel = this.getOpenerEventChannel();
    eventChannel.emit('pickedAddress', { selectedAddress: address });
    wx.navigateBack({ delta: 1 });
  },

  /**
   * 生命周期函数--监听页面加载
   */

  onLoad() {
    const eventChannel = this.getOpenerEventChannel();
    console.log('事件渠道：', eventChannel);
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {
    this.getAddressList();
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {
  }
})
