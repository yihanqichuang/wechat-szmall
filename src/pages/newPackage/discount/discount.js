// 获取全局应用程序实例对象
const app = getApp();
import MarketApi from '../../../api/moneyOff.js'
Page({
    data: {
        loading: false,
        isLoadOver: false,
        pageNum: 1,
        pageSize: 10,
        productList: [],
        marketList: [],
        currentBatch: null
    },

    // 刷新
    refresh() {
        this.setData({ pageNum: 1, productList: [] });
        this.getMarketProductList();
    },

    // 加载更多
    loadMore() {
        if (this.data.isLoadOver) return
        this.setData({ pageNum: ++this.data.pageNum });
        this.getMarketProductList();
    },

    // 切换场次
    switchMarket(e) {
        const currentBatch = e.currentTarget.dataset.batch;
        if (currentBatch.id === this.data.currentBatch.id) return
        this.setData({ currentBatch });
        this.refresh();
    },

    // 获取场次商品
    getMarketProductList() {
        const params = {
            batchCode: this.data.currentBatch.batchCode,
            pageNum: this.data.pageNum,
            pageSize: this.data.pageSize
        };
        this.setData({ loading: true });
        MarketApi.timerMarketSpuList(params)
            .then(res => {
                this.setData({ loading: false });
                console.log('场次商品：', res);
                if (res.data.data && res.data.data.length) {
                    this.setData({
                        isLoadOver: !res.data.page || (res.data.pages && res.data.pages <= this.data.pageNum),
                        [`productList[${this.data.pageNum - 1}]`]: res.data.data
                    });
                } else {
                    this.setData({ isLoadOver: true });
                }
            })
    },

    // 将日期转化到同一时间点
    dateToSameTime(timeStamp) {
        let date = new Date();
        if (timeStamp) {
            date = new Date(timeStamp);
        }
        date.setHours(0);
        date.setMinutes(0);
        date.setSeconds(0);
        return date;
    },

    // 计算场次开始时间
    groupMarketTime(list) {
        if (!list) return
        const nowTime = new Date().getTime();
        const nowDate = this.dateToSameTime().getTime();
        return list.map(ele => {
            const endDate = this.dateToSameTime(ele.startTime).getTime();
            const diffDays = Math.ceil((endDate - nowDate) / (1000 * 60 * 60 * 24));
            if (diffDays < 1) {
                // 当天
                if (nowTime >= ele.startTime && nowTime < ele.endTime) {
                    ele.mark = 'ing';
                } else {
                    ele.mark = 'notice';
                }
            } else if (diffDays >= 1 && diffDays < 2) {
                // 次日
                ele.mark = 'yesterday';
            } else {
                // 其它
                ele.mark = 'other';
            }
            return ele;
        })
    },

    // 获取场次列表
    getMarketList() {
        MarketApi.currentTimerMarketBatchesList({ batchNoticeVO: '' })
            .then(res => {
                console.log('场次列表：', res);
                if (res.data.flag) {
                    res.data.data.map(item => {
                        item.startTime = new Date(item.startTime.replace(/-/g, '/')).getTime();
                        item.endTime = new Date(item.endTime.replace(/-/g, '/')).getTime();
                    })
                    const list = this.groupMarketTime(res.data.data);
                    this.setData({
                        marketList: list,
                        currentBatch: res.data.data && res.data.data.length ? res.data.data[0] : null
                    });
                    this.refresh();
                }
            })
    },

    onLoad() {
        this.getMarketList();
    },
    onReachBottom() {
        this.loadMore();
    },
    /**
     * 页面相关分享
     */
    onShareAppMessage() {
        const organizeId = wx.getStorageSync('organizeId');
        const organizeName = wx.getStorageSync('organizeName');
        return {
            path: `/pages/newPackage/discount/discount?organizeId=${organizeId}&organizeName=${organizeName}`
        }
    }
});

