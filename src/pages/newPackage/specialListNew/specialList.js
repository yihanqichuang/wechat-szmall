// 获取全局应用程序实例对象
// const app = getApp()
import categoryApi from '../../../api/category'
import api from '../../../api/search'
// 创建页面实例对象
Page({
  /**
   * 页面的初始数据
   */
  data: {
    loading: false,
    isLoadOver: false,
    pageNo: 1,
    pageSize: 10,
    list: [],
    emptyType: 'commodity',
    parentCategoryId: null,
    categoryList: [],
    currentCategory: null,
    tabs: [  //
      {
        name: '默认排序',
        type: ''
      },
      // {
      //   name: '销量',
      //   type: 'SALENUM'
      // },
      {
        name: '价格',
        type: 'PRICE'
      }
    ],
    sortField: '',
    sortType: ''
  },
  cancel() {
    wx.navigateBack({     // 返回上一页面或多级页面
      delta: 1
    })  
  },
  // 清空按钮
  clearText() {
    this.setData({ searchKey: '' })
  },
  // 刷新
  refresh() {
    this.setData({ pageNo: 1, list: [] });
    this.getList();
  },

  // 加载更多
  loadMore() {
    if (this.data.isLoadOver) return
    this.setData({ pageNo: ++this.data.pageNo });
    this.getList();
  },
  // 获取商品列表数据
  getList() {
    let params = {
      productVO: {
        lastCategoryIdList: this.data.currentCategory.backCategoryIds ? this.data.currentCategory.backCategoryIds.split(',') : null,
        sortField: this.data.sortField,
        sortType: this.data.sortType
      },
      page: {
        pageNo: this.data.pageNo,
        pageSize: this.data.pageSize
      }
    };
    this.setData({ loading: true });
    api.productList(params).then(res => {
      this.setData({ loading: false });
      console.log('商品列表：', res);
      if (res.flag && res.data && res.data.length) {
        this.setData({
          isLoadOver: !res.page || (res.page.pages && res.page.pages <= this.data.pageNo),
          [`list[${this.data.pageNo - 1}]`]: res.data
        });
      } else {
        this.setData({ isLoadOver: true });
      }
    })
  },
  // 初始化分类
  initCategoryList() {
    let params = {
      level: 3,
      parentCategoryId: this.data.parentCategoryId
    };
    categoryApi.fetchCategoryList(params).then(res => {
      console.log('分类列表：', res);
      if (!res.data.flag) return;
      this.setData({
        categoryList: [...this.data.categoryList, ...res.data.data]
      });
    });
  },

  chooseCategory(e) {
    let item = e.currentTarget.dataset.item;
    this.setData({
      currentCategory: item
    });
    this.refresh();
  },

  // 顶部搜索栏搜索方法
  handleConfirm(e) {
    let searchKey = e.detail.value;
    this.setData({ searchKey });
    this.refresh();
  },

  // 排序选择
  handleSortType(e) {
    const type = e.currentTarget.dataset.type;
    const sortType = this.data.sortType === 'ASC' ? 'DESC' : 'ASC'
    if (type) {
      this.setData({
        sortField: type,
        sortType: sortType
      });
    } else {
      // 默认升序排列
      this.setData({
        sortField: '',
        sortType: 'ASC'
      });
    }
    this.refresh();
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(option) {
    if (option.searchKey) {
      wx.setNavigationBarTitle({ title: option.searchKey });
    }
    this.setData({
      searchKey: option.searchKey,
      parentCategoryId: option.categoryId,
      currentCategory: {
        name: '全部',
        categoryId: option.categoryId,
        backCategoryIds: option.lastCategoryIdList
      },
      categoryList: [{
        name: '全部',
        categoryId: option.categoryId,
        backCategoryIds: option.lastCategoryIdList
      }]
    });
    this.initCategoryList();
    this.refresh();
  },
  onReachBottom() {
    this.loadMore();
  }
})
