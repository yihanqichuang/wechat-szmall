// 获取全局应用程序实例对象
const app = getApp();
var dateTimePicker = require('../../../utils/lib/dateTimePicker.js');
import appointmentApi from '../../../api/appointment';
import Tip from '../../../utils/tip'


// 创建页面实例对象
Page({
  /**
   * 页面的初始数据
   */
  data: {
    appointmentOrder: null, // 预约单详情,
    appointmentInfo: null, // 预约模板对象信息
    startTimeArray: null,
    startTime: null,
    endTimeArray: null,
    endTime: null,
  },
  changeDateTime2(e) {
    this.setData({ endTime: e.detail.value });
  },
  changeDateTimeColumn2(e) {
    var arr = this.data.endTime, dateArr = this.data.endTimeArray;
    if (arr) {
      arr[e.detail.column] = e.detail.value;
      dateArr[2] = dateTimePicker.getMonthDay(dateArr[0][arr[0]], dateArr[1][arr[1]]);
      this.setData({
        endTimeArray: dateArr,
        endTime: arr
      });
    }
  },
  changeDateTime1(e) {
    this.setData({ startTime: e.detail.value });
  },
  changeDateTimeColumn1(e) {
    var arr = this.data.startTime, dateArr = this.data.startTimeArray;
    if (arr) {
      arr[e.detail.column] = e.detail.value;
      dateArr[2] = dateTimePicker.getMonthDay(dateArr[0][arr[0]], dateArr[1][arr[1]]);
      this.setData({
        startTimeArray: dateArr,
        startTime: arr
      });
    }
  },

  // 姓名输入
  handleInput(e) {
    let id = e.currentTarget.dataset.id;
    let tempObj = this.data.appointmentInfo;
    tempObj.writeList.forEach(writeInfo => {
      if (id === writeInfo.id) {
        writeInfo.contentDesc = e.detail.value;
      }
    });
    this.setData({
      appointmentInfo: tempObj
    });
  },

  onLoad() {
    this.initDatePicker();
  },

  // 初始化日期选择控件
  initDatePicker() {
    let curDate = new Date();
    let startYear = curDate.getFullYear();
    var obj1 = dateTimePicker.dateTimePicker(startYear);
    // 精确到分的处理，将数组的秒去掉
    obj1.dateTimeArray.pop();
    obj1.dateTime.pop();
    this.setData({
      startTimeArray: obj1.dateTimeArray,
      endTimeArray: obj1.dateTimeArray,
    });
  },

  onShow() {
    if (wx.getStorageSync('appointmentOrder')) {
      this.setData({
        appointmentOrder: wx.getStorageSync('appointmentOrder')
      })
      this.getServiceTemplate();
    }
  },

  getServiceTemplate() {
    let productCode = this.data.appointmentOrder.details[0].productCode;
    appointmentApi.getServiceTemplate({ productCode }).then(res => {
      if (res.data.flag) {
        this.setData({
          appointmentInfo: res.data.data
        })
      }
    })
  },
  // 跳转至预约详情
  toAppointmentDetail() {
    // 将预约状态改成待确认，并跳转至预约详情
    let tepAppointmentOrder = wx.getStorageSync('appointmentOrder');
    tepAppointmentOrder.appointmentState = 'CONFIRMING';
    wx.setStorageSync('appointmentOrder', tepAppointmentOrder);
    wx.navigateTo({
      url: '/pages/newPackage/appointmentDetail/appointmentDetail'
    })
  },
  // 预约
  saveAppointmentInfo() {
    let params = this.data.appointmentInfo;
    params.orderNo = this.data.appointmentOrder.orderNo;
    params.shopOrderNo = this.data.appointmentOrder.shopOrderNo;
    let startTimeWriteObj = {
      contentTitle: '日期开始',
      contentDesc: this.data.startTimeArray[0][this.data.startTime[0]] + '-' + this.data.startTimeArray[1][this.data.startTime[1]] + '-' + this.data.startTimeArray[2][this.data.startTime[2]] + ' ' + this.data.startTimeArray[3][this.data.startTime[3]] + ':' + this.data.startTimeArray[4][this.data.startTime[4]]
    }
    let endTimeWriteObj = {
      contentTitle: '日期结束',
      contentDesc: this.data.endTimeArray[0][this.data.endTime[0]] + '-' + this.data.endTimeArray[1][this.data.endTime[1]] + '-' + this.data.endTimeArray[2][this.data.endTime[2]] + ' ' + this.data.endTimeArray[3][this.data.endTime[3]] + ':' + this.data.endTimeArray[4][this.data.endTime[4]]
    }
    params.writeList.push(startTimeWriteObj);
    params.writeList.push(endTimeWriteObj);
    appointmentApi.reserveAppointmentOrder(params).then(res => {
      if (res.data.flag) {
        Tip.toast('预约成功');
        setTimeout(() => {
          this.toAppointmentDetail();
        }, 500)
      } else {
        Tip.toast(res.data.msg);
      }
    })
  },

  onReachBottom() {
  }

});