// 获取全局应用程序实例对象
const app = getApp()
import OrderApi from '../../../api/order'
import Tip from '../../../utils/tip'
import WxPay from '../../../utils/wxPay'
import ShopApi from '../../../api/findshop';
import PreSaleApi from '../../../api/preSale';
import eleInvoice from '../../../api/eleInvoice';
import benMath from '../../../utils/BenMath.js';
import formatMoney from '../../../utils/formatMoney.js';
import GroupApi from '../../../api/group.js'
const QR = require('../../../utils/wxqrcode.min.js');

// 创建页面实例对象
Page({
    data: {
        extraInfo: '', //会员权益对象
        type: null,
        orderNo: null,
        orderData: null,
        needAddress: false,
        countDownTime: 0,
        dispatchObj: {
            PICK_UP: '自提',
            EXPRESS: '快递'
        },
        payTypeList: {
            WECHAT: '微信',
            PREPAID: '储值卡',
            PREPAID_WECHAT: '微信+储值卡'
        },
        stateList: {
            CLOSE: {
                title: '订单已关闭',
                subTitle: '订单已关闭'
            },
            UNPAY: {
                title: '待支付',
                subTitle: '订单待付款，12分钟后将自动关闭'
            },
            BALANCE_UNPAY: {
                title: '待支付',
                subTitle: '订单待付款，12分钟后将自动关闭'
            },
            PAID: {
                title: '已付款,待发/提货',
                subTitle: '订单已付款，商家将尽快发货'
            },
            WAIT_RECEIVE: {
                title: '待收货/待提货',
                subTitle: '您的订单已确认，感谢您的选购'
            },
            FINISH: {
                title: '已完成',
                subTitle: '订单已完成，期待您的下次光临'
            }
        },
        showPickUpCode: false,
        codeStr: '',
        showDeliveryInfo: false,
        codeImage: null,
        pickupCode: null,
        preSaleData: null, //预售信息
        marketType: null,
        deposit: 0, // 定金
        balance: 0, //尾款
        currentTime: new Date().getTime(),
        balanceEndTime: null,
        balanceStartTime: null,
        depositPayTime: null,
        showInvoiceButton: true,
        flag: ''
    },

    // 打开会员权益弹窗
    openMemberRightsDialog(e) {
        let rightsType = e.currentTarget.dataset.type;
        this.selectComponent('#memberRightsDialog').showDialog(rightsType, this.data.extraInfo);
    },

    // 显示物流信息
    openDeliveryDialog() {
        this.setData({
            showDeliveryInfo: true
        });
    },
    // 关闭物流信息
    closeDeliveryInfo() {
        this.setData({
            showDeliveryInfo: false
        });
    },

    // 跳转尾款支付页面
    payment() {
        let url = '/pages/newPackage/balancePayment/balancePayment'
        wx.navigateTo({
            url: `${url}?orderNo=${this.data.orderNo}`
        })
    },

    // 复制运单号
    copyExpressNo() {
        if (!this.data.orderData.expressInfo) return
        wx.setClipboardData({
            data: this.data.orderData.expressInfo.expressNo,
            success(res) {
                Tip.toast('复制成功', null, 'none');
            }
        });
    },

    // 删除订单
    deleteOrder() {
        wx.showModal({
            title: '确定删除订单？',
            success: (res) => {
                if (res.confirm) {
                    console.log('用户点击确定')
                    this.doDelete();
                } else if (res.cancel) {
                    console.log('用户点击取消')
                }
            }
        })
    },
    doDelete() {
        Tip.loading();
        OrderApi.fetchDeleteOrder({
            shopOrderNo: this.data.orderNo
        }).then(res => {
            Tip.loaded();
            console.log('删除结果：', res);
            if (res.data.flag) {
                this.goBack();
            } else {
                Tip.toast(res.data.msg, null, 'none');
            }
        })
    },
    // 删除完成，返回上一页
    goBack() {
        const pages = getCurrentPages();
        const prevPage = pages[pages.length - 2];
        prevPage.refresh();
        wx.navigateBack({
            delta: 1
        });
    },

    // 取消订单
    cancelOrder() {
        let title = '确定取消订单？';
        // 定金预售
        if (this.data.orderData.orderState === 'BALANCE_UNPAY' && this.data.balance) {
            title = '预售商品，取消订单，定金不退还，是否取消订单？';
        }
        wx.showModal({
            title: title,
            success: (res) => {
                if (res.confirm) {
                    this.doCancel();
                } else if (res.cancel) {
                    console.log('用户点击取消')
                }
            }
        })
    },
    doCancel() {
        Tip.loading();
        OrderApi.fetchCancelOrder({
            orderNo: this.data.orderNo
        }).then(res => {
            Tip.loaded();
            console.log('取消结果：', res);
            if (res.data.flag) {
                this.initData();
                this.goBack();
            } else {
                Tip.toast(res.data.msg, null, 'none');
            }
        })
    },
    // 前往申请售后
    goOrderRefund() {
        wx.navigateTo({
            url: `/pages/newPackage/orderRefundList/orderRefundList?orderNo=${this.data.orderData.shopOrderNo}`
        })
    },

    // 获取未支付订单的时效
    getCountTime(orderTime) {
        orderTime = new Date(orderTime).getTime();
        let closeOrderTime = wx.getStorageSync("closeOrderTime");
        const endTime = orderTime + Number(closeOrderTime);
        this.setData({
            countDownTime: endTime
        });
    },

    // 显示配送方式，遍历商品下的sku列表
    // 存在自提显示自提，存在配送限时配送，都存在限时物流+快递
    showDispatchName(skus) {
        if (!skus || skus.length <= 0) return
        let dispatchTypeNames = [];
        let needAddress = false;
        skus.forEach(sku => {
            if (sku.dispatchType === 'EXPRESS') {
                needAddress = true;
            }
            dispatchTypeNames.push(this.data.dispatchObj[sku.dispatchType])
        });
        let paid_title = '';
        let wait_title = '';
        let res = Array.from(new Set(dispatchTypeNames));
        if (res.length === 2) {
            // 说明物流和自提商品都存在
            paid_title = '待发货/待提货';
            wait_title = '待收货/待提货';
        } else if (res[0] === '自提') {
            // 仅自提
            paid_title = '待提货';
            wait_title = '待提货';
        } else {
            // 仅配送
            paid_title = '待发货';
            wait_title = '待收货';
        }
        this.setData({
            needAddress,
            ['stateList.PAID.title']: paid_title,
            ['stateList.WAIT_RECEIVE.title']: wait_title
        });
        console.log('配送方式：', res);
        return res.join('+')
    },

    // 去开票
    makeInvoice() {
        let actualPayPrice = this.data.orderData.shopOrders[0].deliveryOtherPrice || 0;
        this.data.orderData.shopOrders[0].details.forEach(item => {
            // 退款成功的商品金额减去
            if (item.afterSaleState && item.afterSaleState === 'REMIT_SUCCESS') {
                actualPayPrice = actualPayPrice;
            } else {
                // 商品金额相加并减去退差价金额
                actualPayPrice = benMath.accSub(benMath.accAdd(actualPayPrice, item.otherPayPrice), (item.refundPrice || 0));
            }
        });
        console.log("实付金额", actualPayPrice)
        if (actualPayPrice <= 0) {
            Tip.toast('发票金额异常,请前往线下开票', null, 'none');
            return;
        }
        let industryType = 'GeneralMerchandise';
        let orderNo = this.data.orderData.orderNo;
        let orderTime = this.data.orderData.orderTime;
        let memberId = this.data.orderData.memberId;
        let shopOrderNo = this.data.orderData.shopOrderNo;
        let shopId = this.data.orderData.shopId;
        actualPayPrice = formatMoney.priceFilter(actualPayPrice);
        wx.navigateTo({
            url: `/pages/eleInvoice/makeInvoices/makeInvoices?orderNo=${orderNo}&orderFee=${actualPayPrice}&orderTime=${orderTime}&memberId=${memberId}&shopOrderNo=${shopOrderNo}&shopId=${shopId}&industryType=${industryType}`
        })
    },

    // 获取订单详情
    initData() {
        if (this.data.type === 'UNPAY' || this.data.type === 'BALANCE_UNPAY') {
            this.getUnpayOrderDetail();
        } else {
            let organizeType = wx.getStorageSync('organizeType');
            if ('SuperMarket' === organizeType) {
                this.getUnpayOrderDetail();
            } else {
                this.getOrderDetail();
            }
        }
    },
    goInvoiceDetail() {
        let dpddh = this.data.orderData.shopOrderNo;
        wx.navigateTo({
            url: `/pages/eleInvoice/invoicedRecordDetail/invoicedRecordDetail?dpddh=${dpddh}`
        })
    },

    // 倒计时结束，刷新信息
    doRefresh() {
        this.setData({
            orderNo: this.data.orderData.orderNo
        });
        this.getUnpayOrderDetail();
    },

    // 未支付订单详情
    getUnpayOrderDetail() {
        Tip.loading();
        OrderApi.fetchUnpayOrderDetail({
            orderNo: this.data.orderNo
        }).then(res => {
            Tip.loaded();
            if (res.data.flag) {
                res.data.data.shopOrders.forEach(shop => {
                    shop['dispatchName'] = this.showDispatchName(shop.details);
                    if (shop.orderType === 'GIFT' || shop.orderType === 'COUPON') {
                        let organizeList = app.globalData.organizeList;
                        if (organizeList && organizeList.length > 0) {
                            organizeList.forEach(organize => {
                                if (organize.organizeId === shop.organizeId) {
                                    shop.shopName = organize.organizeName;
                                    shop.shopLogo = organize.organizePicturePath;
                                }
                            })
                        }
                    }
                });
                const detail = res.data.data.shopOrders[0].details[0];
                // 设置定金支付时间
                this.setData({
                    depositPayTime: res.data.data.depositPayTime
                })
                if (detail.marketType === 'PRESALE') {
                    this.getPreSaleDetai(detail, res.data.data);
                    if (res.data.data.orderState == 'BALANCE_UNPAY' && detail.balanceEndTime) {
                        this.setData({
                            countDownTime: detail.balanceEndTime
                        })
                    } else {
                        this.getCountTime(res.data.data.orderTime);
                    }
                } else {
                    this.getCountTime(res.data.data.orderTime);
                    this.setData({
                        orderData: res.data.data
                    });
                }
                // 设置会员权益对象
                this.setData({
                    extraInfo: res.data.data.extraInfo
                })
            } else {
                Tip.toast(res.data.msg, null, 'none');
            }
        })
    },

    // 其它订单详情
    getOrderDetail() {
        Tip.loading();
        // 查询是否已经开票
        // eleInvoice.fetchIsOpenedEinvocie({ orderNo: this.data.orderNo }).then(eleInvoiceRes => {
        //   if (eleInvoiceRes.data.flag && eleInvoiceRes.data.data) {
        //     this.setData({ showInvoiceButton: false });
        //   }
        // });
        OrderApi.fetchOrderDetail({
            shopOrderNo: this.data.orderNo
        }).then(res => {
            Tip.loaded();
            if (res.data.flag) {
                let temp = Object.assign({}, res.data.data);
                temp['shopOrders'] = [res.data.data];
                delete temp.details;
                temp.shopOrders.forEach(shop => {
                    shop['dispatchName'] = this.showDispatchName(shop.details);
                });
                console.log("temp", temp)
                // 订单明细中存在退款中的订单,不允许开票
                res.data.data.details.forEach(item => {
                    if (item.afterSaleState && item.afterSaleState === 'MERCHANT_AUDIT') {
                        this.setData({ showInvoiceButton: false });
                        return;
                    }
                })
                const detail = temp.shopOrders[0].details[0];
                this.setData({
                    marketType: detail.marketType
                })
                if (detail.marketType === 'PRESALE') {
                    this.getPreSaleDetai(detail, temp);
                } else {
                    this.setData({
                        orderData: temp
                    });
                }
                OrderApi.fetchUnpayOrderDetail({
                    orderNo: temp.orderNo
                }).then(ores => {
                    Tip.loaded();
                    this.data.orderData.shopOrders.forEach((order, index) => {
                        let currentShopOrder = ores.data.data.shopOrders.filter(item => {
                            return item.shopOrderNo == order.shopOrderNo
                        })
                        this.data.orderData.shopOrders[index].preDetailsViewList = currentShopOrder[0].preDetailsViewList
                    })
                    if (ores.data.flag) {
                        // 设置定金支付时间
                        this.setData({
                            orderData: this.data.orderData,
                            depositPayTime: ores.data.data.depositPayTime
                        })
                        // 设置会员权益对象
                        this.setData({
                            extraInfo: ores.data.data.extraInfo
                        })
                    }
                })
            } else {
                Tip.toast(res.data.msg, null, 'none');
            }
        })

    },

    // 获取预售信息
    async getPreSaleDetai(detail, data) {
        const res = await PreSaleApi.fetchQueryPreSale({ productVO: { marketCode: detail.marketCode } });
        if (res.flag) {
            this.setData({
                marketType: detail.marketType,
                preSaleData: res.data.data,
                deposit: detail.deposit,
                balance: detail.balance,
                balanceEndTime: detail.balanceEndTime,
                balanceStartTime: detail.balanceStartTime,
                currentTime: new Date().getTime()
            })
        }
        this.setData({
            orderData: data
        });
    },

    // 获取支付参数
    pullWechatPay() {
        Tip.loading();
        WxPay.getNewWxPayConfig(this.data.orderData.orderNo)
            .then(res => {
                Tip.loaded();
                console.log('支付结果：', res);
                Tip.toast('支付完成', null);
                this.goBack();
            })
            .catch(err => {
                Tip.loaded();
                if (typeof err === 'string') {
                    Tip.toast(err, null, 'none');
                }
                console.log('支付失败：', err)
            })
    },

    // 确认收货
    confirmReceive() {
        wx.showModal({
            title: '确认收货',
            content: '是否确认收到货物？',
            confirmText: '确认收货',
            confirmColor: '#FB5B41',
            success: res => {
                if (res.confirm) {
                    Tip.loading();
                    OrderApi.fetchConfirmReceive({
                        shopOrderNo: this.data.orderData.shopOrderNo,
                        orderNo: this.data.orderData.orderNo
                    }).then(res => {
                        Tip.loaded();
                        console.log('确认收货结果：', res);
                        if (res.data.flag) {
                            this.initData();
                        } else {
                            Tip.toast(res.data.msg, null, 'none');
                        }
                    })
                }
            }
        })
    },

    // 打开提货码
    async showPicUpCode(e) {
        Tip.loading('生成中...');
        let sku = e.currentTarget.dataset.sku;
        const codeImage = QR.createQrCodeImg(String(sku.pickupCode) + 0);
        const shopRes = await ShopApi.fetchShopData({
            shopId: sku.shopId
        });
        console.log('店铺详情：', shopRes.data);
        if (shopRes.data.flag) {
            sku.pickupPhone = shopRes.data.shop.shopPhone
        }
        Tip.loaded();
        this.setData({
            showPickUpCode: true,
            codeStr: String(sku.pickupCode) + 0,
            currentSku: sku,
            codeImage
        })
    },
    // 关闭提货码
    closePickUpCode() {
        this.setData({
            showPickUpCode: false,
            currentSku: null,
            codeImage: null
        })
    },


    async onLoad(option) {
        let flag = option.flag

        // 若是从订单列表tab页「全部」点进来，需要判断是否为待成团订单
        // if ('WAIT_GROUP' !== flag) {
        //   await GroupApi.getGroupInfoByOrderNo({ orderNo: option.orderNo, type: null }).then(rsp => {
        //     if (rsp.data && 'ING' === rsp.data.status) {
        //       flag = 'WAIT_GROUP'
        //     }
        //   })
        // }

        this.setData({
            type: option.type,
            orderNo: option.orderNo,
            flag
        });
        this.initData();
    }
});