// 获取全局应用程序实例对象
const app = getApp();
const wechat = require('../../../utils/wechat.js');
import coupon from '../../../api/coupon';
import userApi from '../../../api/user';
// 创建页面实例对象
Page({
    /**
     * 页面的初始数据
     */
    data: {
        address: '上百',
        list: [],
        page: {
            pageNo: 1,
            pageSize: 10
        },
        query: {},
        pending: false,
        messageComponent: '',
        gradeNo: null,
        formId: '',
        templateData: {},
        pagePath: ''
    },
    // 获取优惠券列表
    getList(pageNo, key = '') {
        let params = {
            couponName: key,
            pageNum: pageNo,
            pageSize: 10
        };
        if (pageNo == 1) {
            this.setData({
                list: []
            })
        }
        Object.keys(this.data.query).forEach(item => params[item] = this.data.query[item]);
        coupon.couponSaleList(params).then(res => {
            if (!res.data) {
                let msg = res.data.message || '请求数据失败';
                this.messageComponent.applyActive(msg);
                return
            }
            console.log('优惠券列表：', res);
            let data = res.data.data;
            this.data.page.pageNo = res.data.curPage
            this.setData({
                list: [...this.data.list, ...data],
                page: {
                    pageNo: res.data.curPage,
                    pageSize: 10,
                    total: res.data.totalCount
                }
            })
        })
    },

    // 搜索
    search(e) {
        this.setData({
            list: [],
            page: {
                pageNo: 1,
                pageSize: 10,
                pages: 1,
                total: 1
            }
        });
        this.getList(this.data.page.pageNo, e.detail.value.trim())
    },

    // 加更多载
    loadMore(e) {
        this.getList(e.detail)
    },

    // 获取会员等级
    getGradeNo() {
        return new Promise((resolve, reject) => {
            userApi.queryMember().then(res => {
                if (res.data.flag) {
                    resolve(res.data.data.gradeNo);
                    return
                }
                reject()
            }, () => reject())
        })
    },

    // 获取点击的列表索引
    handleGetIndex(e) {
        let refreshIndex = e.currentTarget.dataset.index;
        console.log(refreshIndex)
        this.setData({ refreshIndex })
    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad(option) {
        this.setData({ query: option });
        this.messageComponent = this.selectComponent('.message-tips');
        this.getList(1);
        app.pubSub.on('refreshCouponItem', (state, balanceNum) => {
            let item = this.data.list[this.data.refreshIndex];
            state && (item.state = state);
            item.balanceNum = balanceNum;
            this.setData({
                [`list[${this.data.refreshIndex}]`]: item
            })
        })
    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload() {
        app.pubSub.off('refreshCouponItem', null)
    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh() {
        wx.showNavigationBarLoading();
        this.getList(1);
        wx.hideNavigationBarLoading() // 完成停止加载
        wx.stopPullDownRefresh();
    },
    /**
     * 页面相关分享
     */
    onShareAppMessage() {
        const organizeId = wx.getStorageSync('organizeId');
        const organizeName = wx.getStorageSync('organizeName');
        return {
            path: `/pages/newPackage/couponNew/coupon?organizeId=${organizeId}&organizeName=${organizeName}`
        }
    }
});
