/*
 * @Author: your name
 * @Date: 2020-11-25 14:47:33
 * @LastEditTime: 2020-11-30 17:12:17
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: /WangMao-yidong/wxml-nbss/src/pages/newPackage/couponNew/detail/detail.js
 */
// 获取全局应用程序实例对象
const app = getApp();
import couponApi from '../../../../api/coupon'
import tip from '../../../../utils/tip'
import wxtemplate from '../../../../api/wxtemplate'
import shareLog from '../../../../api/shareLog.js';
import wxPay from '../../../../utils/wxPay.js';
import query from '../../../../utils/query.js'
import GetPage from '../../../../utils/getCurrentPageUrl'
// 创建页面实例对象
Page({
  /**
   * 页面的初始数据
   */
  data: {
    couponId: '',
    cyCouponId: '',
    info: {},
    maxCount: 20,
    messageComponent: '',
    index: '',
    isShow: true,
    page: '',
    formId: '',
    orderNo: '',
    shareTitle: '', // 分享图片
    shareImg: '', // 分享标题
    transInfo: {} // 转发信息
  },

  // 获取优惠券详情
  getInfo(couponId, isBuy) {
    let params = {
      couponId
    };
    couponApi.couponDetail(params).then(res => {
      console.log('优惠券详情：', res);
      tip.loaded();
      if (!res.data.flag) {
        return
      }
      let info = res.data.data;
      let form = {
        id: info.couponId,
        picturePath: info.couponImage || app.globalData.defaultConfig.defaultPic,
        commodityName: info.couponName,
        price: '免费',
        pageUrl:`${GetPage.getCurrentPageUrl()}`
      }
      this.setData({shareTitle: res.data.data.couponName})
      this.setData({shareImg: res.data.data.couponImage})
      this.setData({transInfo: form});
      this.handleInfo(info);
      app.updateLocalOrganize(info.organizeId);
      if (isBuy) {
        app.pubSub.emit('refreshCouponItem', info.$state, info.balanceNum)
      }
    })
  },

  // 处理数据Type
  handleInfo(info) {
    console.log(info)
    info.$state = this.checkStartTime(info) === 1 ? 'UNSTART'
            : this.checkStartTime(info) === 2 ? 'OVER'
              : info.balanceNum <= 0 ? 'rob'
                : info.memberHaveFlag === 'N' ? 'received' : 'get';
    info.couponImage = info.couponImage ? info.couponImage : app.globalData.defaultConfig.defaultPic;
    this.setData({info, isShow: true})
  },

  // 到达领取数量
  received() {
    this.messageComponent.applyActive('已达到限领数量')
  },

  // 未开始或已结束
  checkStartTime(item) {
    let now = Date.now()
    let startTime = item.activityStartTime == null ? '' : new Date(item.activityStartTime).getTime()
    if (item.activityStartTime && now < startTime) {  // 未开始
      return 1
    }
    let endTime = item.activityEndTime == null ? '' : new Date(item.activityEndTime).getTime()
    if (item.activityEndTime && now > endTime) {  // 已结束
      return 2
    }
    return 0
  },


  // 兑换，领取，购买优惠券
  draw() {
    if (this.data.info.$state === 'UNSTART') {
      this.messageComponent.applyActive('活动未开始');
      return
    }
    if (this.data.info.$state === 'OVER') {
      this.messageComponent.applyActive('活动已结束');
      return
    }
    // 正常购买
    if (this.data.info.$state === 'get' || this.data.orderNo) {
        setTimeout(() => this.buyExchange(), 500)
    //   // 免费
    //   if (this.data.info.accessType === 'FREE') {
    //     setTimeout(() => this.buyExchange(), 500)
    //   }

    //   // 积分
    //   if (this.data.info.accessType === 'POINT') {
    //     tip.confirm(`确认消耗${this.data.info.accessValue}积分兑换？`, '', '提示', '取消', '确认').then(() => {
    //       setTimeout(() => this.buyExchange(), 500)
    //     }).catch(() => {
    //       console.log('取消兑换')
    //     })
    //   }

    //   // 现金购买
    //   if (this.data.info.accessType === 'BUY') {
    //     if (!this.data.orderNo) {
    //       setTimeout(() => this.buyExchange(), 500)
    //     } else {
    //       this.wxPay(this.data.orderNo, this.data.info.accessValue, 'COUPON');
    //     }
    //   }

    //   // 积分+现金
    //   if (this.data.info.accessType === 'MIXED') {
    //     if (!this.data.orderNo) {
    //       tip.confirm(`您正在使用${this.data.info.points}积分购买优惠券，请确认是否立即支付？`, {}, '温馨提示', '再想想', '立即下单').then(() => {
    //         setTimeout(() => this.buyExchange(), 500)
    //       }).catch(() => {
    //         console.log('取消');
    //       })
    //     } else {
    //       this.wxPay(this.data.orderNo, this.data.info.accessValue, 'COUPON');
    //     }
    //   }
    }
  },

  // 使用现金购买优惠券
  buyExchange() {
    this.data.maxCount = 20; // 复原计数
    wx.showLoading({ mask: true });
    let param = {
      couponId: this.data.couponId,
      organizeType: wx.getStorageSync('organizeType')
    }
    couponApi.createGiftAndCouponOrder(param).then(res => {
      console.log(res)
      if (res.data.flag) {
        this.getFailType(res.data.data)
      } else {
        wx.hideLoading();
        this.messageComponent.applyActive('领取失败!' + res.data.msg)
      }
    })
  },
  getFailType(orderNo) {
    couponApi.queryOrderCache({})
        .then(res => {
          console.log('查询结果：', res.data);
          if (res.data.flag) {
            if (res.data.data.state === 'HANDING') {
              // 最多请求20次
              if (this.data.maxCount > 0) {
                let timer = setTimeout(() => {
                  this.getFailType(orderNo);
                  clearTimeout(timer);
                  this.data.maxCount--;
                }, 500);
              } else {
                wx.hideLoading();
              }
            } else if (res.data.data.state === 'CREATE_SUCCESS') {
              wx.hideLoading();
              this.wxPay(orderNo)
            } else if (res.data.data.state === 'GAIN_SUCCESS') {
              wx.hideLoading();
              this.getInfo(this.data.couponId, false);
              this.messageComponent.applyActive('领取成功!')
            } else if (res.data.data.state === 'FAIL') {
              wx.hideLoading();
              this.messageComponent.applyActive(res.data.data.errMsg);
            } else {
              wx.hideLoading();
              this.getInfo(this.data.couponId, false);
              this.messageComponent.applyActive('领取成功!')
            }
          }
        });
  },

  // 微信支付
  wxPay(id) {
    wxPay.getNewWxPayConfig(id).then(() => {
      this.getInfo(this.data.couponId, true);
      this.setData({orderNo: ''});
      tip.toast('已购买', null, 'none');
      wx.redirectTo({
        url: '/pages/newPackage/myCouponNew/myCoupon'
      })
    }).catch(err => {
      console.log('支付错误：', err);
      let organizeType = wx.getStorageSync('organizeType')
      let url = '/pages/newPackage/markOrders/markOrders'
      if (organizeType != 'SuperMarket') {
        url = '/pages/newPackage/mallOrders/mallOrders'
      }
      wx.redirectTo({
        url: url
      })
    })
  },

  // 提交表单获取formId
  formSubmit(e) {
    console.log('点击购买-----');
    if (wx.getStorageSync('memberType') !== 'MEMBER') {
      wx.navigateTo({url: `/pages/memberSign/memberSign`});
      return
    }
    this.setData({formId: e.detail.formId});
    wxtemplate.formSubmit(e).then(res => {
      this.setData({formId: res});
      this.draw()
    }).catch(() => this.draw())
  },

  // 分享
  onShareAppMessage() {
    // 转发成功之后记录分享的次数
    shareLog.shareLogCommon(this.data.couponId, 'COUPON');
    let organizeId = wx.getStorageSync('organizeId'), organizeName = wx.getStorageSync('organizeName');
    return {
      title: this.data.shareTitle,
      path: '/pages/newPackage/couponNew/detail/detail?id=' + this.data.couponId + '&cyId=' + this.data.cyCouponId + '&organizeId=' + organizeId + '&organizeName=' + organizeName + '&share=true',
      imageUrl: this.data.shareImg
    }
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(option) {
    console.log('活动启动参数：', option);
    tip.loading();
    // 二维码扫入
    if (option.scene) {
      let scene = decodeURIComponent(option.scene);
      console.log('解析场景值：', scene);
      let id = query.getQueryString('id', scene);
      this.setData({couponId: id});
    } else {
      this.setData({
        couponId: option.id,
        cyCouponId: option.cyId || '',
        orderNo: option.orderNo || '',
        memberOrderId: option.memberOrderId || ''
      });
    }
    this.getInfo(this.data.couponId, false);
    if (this.data.orderNo) {
      wx.setNavigationBarTitle({
        title: '确认支付'
      })
    }
    this.messageComponent = this.selectComponent('.message-tips')
  }
});
