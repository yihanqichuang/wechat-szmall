import OrderApi from '../../../api/order';
Page({
  /**
   * 页面的初始数据
   */
  data: {
    markers: [],
    longitude: 120.882258,
    latitude: 30.037795,
    // scale: 16,
    polyline: [],
    setting: {
      enable3D: true
    }
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function ({orderNo}) {
    this.getData(orderNo);
  },

  getData(orderNo) {
    try {
      OrderApi.fetchLogisticsInfo({
        orderNo: orderNo || ''
      }).then((res) => {
        const points = []
        const markers = [
          {
            id: 1,
            title: '起点',
            label: '起点',
            latitude: '',
            longitude: '',
            width: '28px',
            height: '30px',
            color: '#1E9FFF',
            iconPath: '../../../images/icon/start.png'
          },
          {
            id: 2,
            title: '骑手',
            label: '骑手',
            latitude: '',
            longitude: '',
            width: '50px',
            height: '50px',
            iconPath: '../../../images/icon/rider.png'
          },
          {
            id: 3,
            title: '终点',
            label: '终点',
            latitude: '',
            longitude: '',
            width: '28px',
            height: '30px',
            color: '#1E9FFF',
            iconPath: '../../../images/icon/end.png'
          }
        ]
        if (res.flag) {
          markers[0].latitude = res.data.from.lat;
          markers[0].longitude = res.data.from.lng;

          markers[1].latitude = res.data.courierTag.courierLat;
          markers[1].longitude = res.data.courierTag.courierLng;

          markers[2].latitude = res.data.to.lat;
          markers[2].longitude = res.data.to.lng;

          markers.forEach(item => {
            points.push({
              latitude: item.latitude,
              longitude: item.longitude
            })
          })
          
          this.setData({
            markers,
            polyline: [{
              points,
              color: '#1E9FFF',
              width: 6
            }],
            longitude: markers[0].longitude,
            latitude: markers[0].latitude
          });
        }
      });
    } catch (error) {
      console.error(error);
    }
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },
  

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {},

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {},

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {},

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {},

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {},

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {}
});
