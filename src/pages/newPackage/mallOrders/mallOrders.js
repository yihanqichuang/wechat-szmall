// 获取全局应用程序实例对象
const app = getApp();
import OrderApi from '../../../api/order';
import MarketApi from '../../../api/markets';

// 创建页面实例对象
Page({
  /**
   * 页面的初始数据
   */
  data: {
    tabs: [{
      name: '全部',
      type: 'ALL'
    },
    {
      name: '待付款',
      type: 'UNPAY'
    },
    {
      name: '待发货',
      type: 'PAID'
    },
    {
      name: '待收货',
      type: 'WAIT_RECEIVE'
    },
    {
      name: '已完成',
      type: 'FINISH'
    },
    {
      name: '售后',
      type: 'AFTER_SALE'
    }
    ],
    activeIndex: 0,
    loading: false,
    isLoadOver: false,
    pageNo: 1,
    pageSize: 10,
    currentOrderState: 'ALL',
    unPayList: null,
    orderList: [],
  },

  // 初始化机构列表，待支付订单需要显示机构名和logo
  async getAllOrganizeList(callback) {
    if (!app.globalData.organizeList) {
      const res = await MarketApi.fetchOrganizeList()
      if (res.data.flag) {
        let list = [];
        res.data.data.forEach(city => {
          if (city.organizeList && city.organizeList.length > 0) {
            list.push(...city.organizeList);
          }
        });
        app.globalData.organizeList = list;
      }
    }
    callback();
  },

  navigateToRefund(e) {
    const detail = e.currentTarget.dataset.refunddetail;
    // 过滤出优惠套餐数据
    const packageList = detail.orderDetailInfoList.filter(item => item.packageProductCode);
    wx.navigateTo({
      url: `/pages/newPackage/orderRefundDetail/orderRefundDetail?refundOrderNo=${detail.refundOrderNo}&packageProductCode=${packageList.length > 0 ? packageList[0].packageProductCode : ''}`
    })
  },

  // tab切换
  changeIndex(e) {
    const type = this.data.tabs[e.detail].type;
    this.setData({
      currentOrderState: type
    });
    this.refresh();
  },

  // 刷新
  async refresh() {
    this.setData({
      pageNo: 1,
      orderList: []
    });
    await this.getList();
  },

  // 查询订单列表
  async getList() {
    let organizeType = wx.getStorageSync('organizeType')
    if ('SuperMarket' === organizeType) {
      // 菜场订单查询
      if (this.data.currentOrderState === 'AFTER_SALE') {
        this.getRefundList();
      } else {
        this.getUnPayOrders(this.data.pageSize, this.data.currentOrderState);
      }
    } else {
      // 百货订单查询
      if (this.data.currentOrderState === 'ALL') {
        await this.getUnPayOrders(200, 'UNPAYBALANCE_UNPAY');
        this.getOrderList();
      } else if (this.data.currentOrderState === 'UNPAY') {
        this.getUnPayOrders(this.data.pageSize, this.data.currentOrderState);
      } else if (this.data.currentOrderState === 'AFTER_SALE') {
        this.getRefundList();
      } else {
        this.getOrderList();
      }
    }
  },

  // 加载更多
  loadMore() {
    if (this.data.isLoadOver) return
    this.setData({
      pageNo: ++this.data.pageNo
    });
    this.getList();
  },

  // 获取订单列表
  getOrderList() {
    let params = {
      orderState: this.data.currentOrderState,
      current: this.data.pageNo,
      size: this.data.pageSize
    };
    this.setData({
      loading: true
    });
    OrderApi.fetchOrderList(params)
      .then(res => {
        this.setData({
          loading: false
        });
        this.setData({
          orderList: []
        })
        if (res.data.flag && res.data.data && res.data.data.data && res.data.data.data.length) {
          this.setData({
            isLoadOver: !res.data.data.pages || (res.data.data.pages && res.data.data.pages <= this.data.pageNo),
            [`orderList[${this.data.orderList.length}]`]: res.data.data.data
          });
        } else {
          this.setData({
            isLoadOver: true
          });
        }
      })
  },
  // 加载待支付的大订单
  getUnPayOrders(pageSize, orderState) {
    const params = {
      orderState: orderState,
      current: this.data.pageNo,
      size: pageSize
    };
    return new Promise((resolve, reject) => {
      this.setData({
        loading: true
      });
      OrderApi.fetchUnpayOrderList(params).then(res => {
        this.setData({
          loading: false
        });
        this.setData({
          orderList: []
        })
        if (res.data.flag && res.data.data && res.data.data.data && res.data.data.data.length) {
          this.setData({
            isLoadOver: !res.data.data.pages || (res.data.data.pages && res.data.data.pages <= this.data.pageNo),
            [`orderList[${this.data.orderList.length}]`]: res.data.data.data
          });
        } else {
          this.setData({
            isLoadOver: true
          });
        }
        resolve();
      })
    })
  },

  // 获取售后订单列表
  getRefundList() {
    let params = {
      current: this.data.pageNo,
      size: this.data.pageSize
    };
    this.setData({
      loading: true
    });
    OrderApi.fetchRefundList(params)
      .then(res => {
        this.setData({
          loading: false
        });
        this.setData({
          orderList: []
        })
        if (res.data.flag && res.data.data && res.data.data.data && res.data.data.data.length) {
          this.setData({
            isLoadOver: !res.data.data.pages || (res.data.data.pages && res.data.data.pages <= this.data.pageNo),
            [`orderList[${this.data.orderList.length}]`]: res.data.data.data
          });
        } else {
          this.setData({
            isLoadOver: true
          });
        }
      })
  },

  // 前往申请退款
  applyRefund(e) {
    const sku = e.currentTarget.dataset.sku;
    wx.navigateTo({
      url: '/pages/newPackage/orderRefund/orderRefund',
      success: (res) => {
        res.eventChannel.emit('passSku', {
          sku: Object.assign({}, sku, {
            dispatchType: sku.dispatchType || sku.orderDetailInfoList[0].dispatchType // 添加配送方式
          })
        });
      }
    })
  },

  onLoad(e) {
    if (e.orderStatus) {
      this.setData({
        currentOrderState: e.orderStatus,
        activeIndex: this.getActiveIndex(e.orderStatus)
      });
    }
  },

  onShow() {
    this.getAllOrganizeList(() => {
      this.refresh();
    });
  },

  getActiveIndex(val) {
    if ('ALL' === val) {
      return 0
    } else if ('UNPAY' === val) {
      return 1
    } else if ('PAID' === val) {
      return 2
    } else if ('WAIT_RECEIVE' === val) {
      return 3
    } else if ('FINISH' === val) {
      return 4
    } else if ('AFTER_SALE' === val) {
      return 5
    } else {
      return 0
    }
  },

  onReachBottom() {
    this.loadMore();
  }

});