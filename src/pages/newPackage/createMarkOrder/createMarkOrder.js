// 获取全局应用程序实例对象
// const app = getApp()
import AddressApi from '../../../api/addressApi'
import ProductApi from '../../../api/commodity'
import WxPay from '../../../utils/wxPay'
import Pages from '../../../utils/getCurrentPageUrl'
import Tip from '../../../utils/tip'
import PreSaleApi from '../../../api/preSale';

const distributionDictList = [{
  name: '同城配送',
  value: 'CITY_EXPRESS'
}, {
  name: '到店自提',
  value: 'PICK_UP'
}]

// 创建页面实例对象
Page({
  /**
   * 页面的初始数据
   */
  data: {
    isGroup:'N', // 该商品是否拼团标志
    groupInfoCode:'', // 团code
    isCityExpress: null, // 送达时间是否开启标志
    isPickUp: null, // 自提时间是否开启标志
    immDeliveryShow: false, // 是否显示【立即送达】
    deliveryTime: '', // 送达时间
    todayCityExpressTimeList: [], // 今日送达时间列表
    tomorrowCityExpressTimeList: [], // 明日送达时间列表
    todayPickUpTimeList: [], // 今日自提时间列表
    skuOrganizeId: null,
    submitDisabled: false, // 是否可提交
    showCenterDialog: false,
    checkPriceChange: true,
    isGiftsShow: false,
    selectedAddress: null,
    selectedCoupon: null,
    createOrderSkus: null,
    orderData: null, // 预下单渲染数据
    cardPay: false,
    wechatPay: true,
    needAddress: false,
    // dispatchObj: {
    //   PICK_UP: '自提',
    //   EXPRESS: '快递',
    //   CITY_EXPRESS: '同城配送'
    // },
    orderNo: null,
    failSkuList: null,
    repeatCount: 0,
    valueCard: null,
    prepaidPayPrice: 0,
    prepaidPwdToken: null,
    distributionDictList: distributionDictList, // 配送字典列表
    dispatchType: null, // 选中的配送方式
    goodList: [], // 商品列表（平铺）
    isShowAll: true, // 商品列表是否展开Flag
    isShowDeliverDialog: false, // 打开运费弹窗
    deposit: 0, // 定金
    balance: 0, // 尾款
    marketCode: null,
    preSaleData: null, // 预售信息
    marketType: null,
    secondMarketUseCoupon: true, // 第二件活动情况下可否使用优惠券判断flag
    isPickUpOnly: false, // 商品只能自提falg
    totalWeight: 0, // 商品重量
    organizeList: [], //提货门店列表
    pickupOrganize: null,
    groupActivity: null // 拼团活动信息
  },
  /**
   * 从状态树中获取配送字典列表
   */
  getDistributionDictList() {
    let firstDispatchType = null;
    let isPickUpOnly = wx.getStorageSync('isPickUp');
    if (isPickUpOnly) {
      // 默认选中自提
      firstDispatchType = this.data.distributionDictList.find(item => item.value == 'PICK_UP')
    } else {
      firstDispatchType = this.data.distributionDictList[0]
    }

    this.setData({
      dispatchType: firstDispatchType,
      needAddress: firstDispatchType.value === 'CITY_EXPRESS' // 如果是同城配送，则显示地址
    })
  },

  // 计算商品总重量
  countProductWeight(productList) {
    let totalWeight = 0;
    // 筛选出有第二件活动的商品
    productList.forEach(product => {
      for (let item of product.details) {
        totalWeight += item.skuWeight * item.productCount
      }
    })
    this.setData({
      totalWeight: totalWeight.toFixed(2)
    })
  },

  /**
   * 改变配送方式
   * @param {} e 当前点击的配送事件
   */
  async changeDistribution(e) {
    // 只能自提，则不能切换配送方式
    if (this.data.isPickUpOnly) {
      Tip.toast('当前商品仅支持自提', null, 'none');
      return
    }
    let {
      item
    } = e.currentTarget.dataset;
    if (item.value !== this.data.dispatchType) {
      this.setData({
        dispatchType: item,
        needAddress: item.value === 'CITY_EXPRESS' // 如果是同城配送，则显示地址
      })
    }

    await this.preCreateOrder()
    Tip.toast('由于配送方式变化，您的配送费也发生了变化', null, 'none');
    // 清空配送时间
    this.setData({
      deliveryTime: '',
      immDeliveryShow: false
    })
  },

  // 打开选择配送时间弹窗
  openDeliveryTimePicker() {
    if ('CITY_EXPRESS' === this.data.dispatchType.value) {
      this.selectComponent('#deliveryTimePicker').showPicker(this.data.todayCityExpressTimeList, this.data.tomorrowCityExpressTimeList);
    } else if ('PICK_UP' === this.data.dispatchType.value) {
      this.selectComponent('#deliveryTimePicker').showPicker(this.data.todayPickUpTimeList, this.data.tomorrowPickUpTimeList);
    }
  },

  // 打开提货门店弹窗
  openPickStorePicker() {
    if (this.data.marketType != 'PRESALE') {
      return;
    }
    this.selectComponent('#pickStorePicker').showPicker(this.data.organizeList, this.data.pickupOrganize);
  },

  // 获取日期类型str
  getDateStr(type) {
    let now = new Date();
    let year = '';
    let month = '';
    let date = '';
    if ('TODAY' === type) {
      now.setTime(now.getTime());
    } else if ('TOMORROW' === type) {
      now.setTime(now.getTime() + 24 * 60 * 60 * 1000);
    }
    year = now.getFullYear();
    month = now.getMonth() + 1;
    date = now.getDate()
    if (month < 10) {
      month = '0' + month;
    }
    if (date < 10) {
      date = '0' + date;
    }
    return year + '-' + month + '-' + date;
  },

  // 设定送达时间
  setDeliveryTime(e) {
    if ('TODAY' === e.detail.deliveryType) {
      let todayDeliveryTime = this.getDateStr('TODAY');
      this.setData({
        deliveryTime: todayDeliveryTime + ' ' + e.detail.startTime + '-' + e.detail.endTime
      })
    } else if ('TOMORROW' === e.detail.deliveryType) {
      let tomorrowDeliveryTime = this.getDateStr('TOMORROW');
      this.setData({
        deliveryTime: tomorrowDeliveryTime + ' ' + e.detail.startTime + '-' + e.detail.endTime
      })
    }
    // 判断是否显示【立即送达】四个字
    this.setData({
      immDeliveryShow: e.detail.immDelivery ? true : false
    })
  },
  // 设定门店
  setOrganizeInfo(e) {
    console.log("设定门店", e)
    const orderData = this.data.orderData;
    orderData.orderInfo.pickupAddress = e.detail.mallManagementConfigDO.address;
    this.setData({
      pickupOrganize: e.detail.organizeId,
      orderData: orderData
    })
  },
  // 打开优惠券选择
  openCouponPicker() {
    const skuList = [];
    this.data.createOrderSkus.forEach(shop => {
      if (shop.skuList && shop.skuList.length > 0) {
        shop.skuList.forEach(sku => {
          if (sku.cartProductType === 'PACKAGE') {
            sku.skuInfo.forEach(item => {
              skuList.push({productSkuCode: item.productSkuCode, productCount: sku.skuCount});
            });
          } else {
            skuList.push({
              productSkuCode: sku.skuInfo.productSkuCode,
              productCount: sku.skuCount
            });
          }
        });
      }
    });
    this.selectComponent('#couponPicker').showPicker({
      skuList
    });
  },

  // 接收选择优惠券
  savePickedCoupon(e) {
    this.setData({
      prepaidPayPrice: 0,
      prepaidPwdToken: null,
      selectedCoupon: e.detail
    });
    this.preCreateOrder();
  },
  showDispatchName() {

    // if (!skus || skus.length <= 0) return
    // let dispatchTypeNames = [];

    // // skus.forEach(sku => {
    // //   if ((sku.dispatchType === 'EXPRESS' || sku.dispatchType === 'CITY_EXPRESS') && !this.data.needAddress) {
    // //     this.setData({ needAddress: true });
    // //   }
    // //   dispatchTypeNames.push(this.data.dispatchObj[sku.dispatchType])
    // // });
    // let res = Array.from(new Set(dispatchTypeNames));
    // console.log('配送方式：', res);
    // return res.join('+')
  },

  // 切换赠送明细显示
  switchGiftShow() {
    this.setData({
      isGiftsShow: !this.data.isGiftsShow
    });
  },
  // 前往选择地址
  toPickAddress() {
    const _this = this;
    wx.navigateTo({
      url: '/pages/newPackage/addressList/addressList',
      events: {
        pickedAddress: function (data) {
          _this.setData({
            selectedAddress: data.selectedAddress
          });
          _this.preCreateOrder();
        }
      },
      success: function (res) {
        // 通过eventChannel向被打开页面传送数据
        // res.eventChannel.emit('acceptDataFromOpenerPage', { data: 'test' })
      }
    })
  },

  // 选择支付方式
  payWayChange(e) {
    console.log('选择的支付方式：', e);
    const cardPay = e.detail.value.indexOf('card') > -1;
    const wechatPay = e.detail.value.indexOf('wechat') > -1;
    if (!cardPay) {
      // 取消储值卡支付，清空金额
      this.setData({
        prepaidPayPrice: 0,
        prepaidPwdToken: null
      });
    }
    this.setData({
      cardPay,
      wechatPay
    });
  },

  // 初始化订单商品数据
  initOrderInfo() {
    AddressApi.fetchAddressList({
      isDefault: 'Y'
    }).then(addressRes => {
      console.log('默认地址：', addressRes);
      if (addressRes.data.flag && addressRes.data.data) {
        this.setData({
          selectedAddress: addressRes.data.data[0]
        });
      } else {
        Tip.toast(addressRes.data.msg, null, 'none');
      }
      let createOrderSkus = wx.getStorageSync('createOrderSkus');
      console.log('createOrderSkus', createOrderSkus)
      if (createOrderSkus) {
        if (createOrderSkus.origin === 'detail') {
          // 立即购买
          createOrderSkus = [{
            shopId: createOrderSkus.shopId,
            shopName: createOrderSkus.shopName,
            shopLogo: createOrderSkus.shopLogo,
            shopPhone: createOrderSkus.shopPhone,
            skuList: [{
              // dispatchType: createOrderSkus.dispatchType.value,
              skuCount: createOrderSkus.skuCount,
              skuInfo: createOrderSkus.skuInfo,

              cartProductType:createOrderSkus.cartProductType,
              marketCode:createOrderSkus.marketCode,
              packageProductCode:createOrderSkus.packageProductCode
            }]
          }];
        }
        console.log(createOrderSkus)
        this.setData({
          createOrderSkus
        });
        this.preCreateOrder();
      }
    });
  },

  // 输入店铺备注
  completeRemark(e) {
    console.log('备注：', e);
    let {
      value
    } = e.detail;

    // this.setData({
    //   [`createOrderSkus[${index}].remark`]: value
    // });
    this.setData({
      ['orderData.orderInfo.remark']: value
    })
  },

  // 获取储值卡信息
  getValueCardInfo() {
    ProductApi.fetchValueCardInfo()
      .then(res => {
        console.log('储值卡信息：', res);
        if (res.flag) {
          this.setData({
            valueCard: res.data
          });
        }
      })
  },

  // 输入储值卡支付金额
  inputPrepaidPayPrice(e) {
    let amount = e.detail.value;
    if (isNaN(Number(amount)) || amount > this.data.orderData.orderInfo.actualPayPrice) {
      Tip.toast('输入金额不合规', null, 'none');
      amount = this.data.orderData.orderInfo.actualPayPrice
    }
    this.setData({
      prepaidPayPrice: amount
    });
  },
  // 输入完密码,校验密码正确性
  handlePayPassWord(e) {
    console.log('输入完密码：', e);
    const params = {
      cardNo: this.data.valueCard.cid,
      password: e.detail.value
    }
    ProductApi.fetchValidateCardPassword(params)
      .then(res => {
        console.log('校验支付密码结果：', res);
        if (res.flag) {
          this.setData({
            prepaidPwdToken: res.data.token
          });
          this.createOrder();
        } else {
          Tip.toast(res.data, null, 'none');
        }
      })
  },
  // 组织下单参数
  groupOrderParams() {
    const selectedAddress = this.data.selectedAddress || {}
    let isPickUpOnly = wx.getStorageSync('isPickUp');
    this.setData({
      isPickUpOnly: isPickUpOnly ? isPickUpOnly : false
    })
    let groupInfoCode = wx.getStorageSync('createOrderGroupInfoCode');
    let isGroup = wx.getStorageSync('createOrderIsGroup');
    this.setData({
      groupInfoCode:groupInfoCode?groupInfoCode:'',
      isGroup:'Y' === isGroup? isGroup:'N'
    })
    let params = {
      pickupOrganize: this.data.needAddress ? null : this.data.pickupOrganize,
      prepaidPayCardNo: this.data.valueCard ? this.data.valueCard.cid : '',
      prepaidPayPrice: this.data.prepaidPayPrice,
      prepaidPwdToken: this.data.prepaidPwdToken,
      checkPriceChange: this.data.checkPriceChange, // 是否需要校验价格，价格有变化继续支付的时候不要校验价格，置为false
      verifyCode: this.data.selectedCoupon ? this.data.selectedCoupon.verifyCode : null,
      memberCouponId: this.data.selectedCoupon ? this.data.selectedCoupon.memberCouponId : null,
      receiverName: selectedAddress.name || null,
      dispatchType: isPickUpOnly ? 'PICK_UP' : this.data.dispatchType.value, // 配送方式
      receiverPhone: selectedAddress.phone || null,
      receiverProvinceName: selectedAddress.province || null,
      receiverProvinceCode: selectedAddress.provinceCode || null,
      receiverCityName: selectedAddress.city || null,
      receiverCityNameCode: selectedAddress.cityCode || null,
      receiverDistrictName: selectedAddress.county || null,
      receiverDistrictCode: selectedAddress.countyCode || null,
      receiverAddress: selectedAddress.address || null,
      remark: this.data.orderData && this.data.orderData.orderInfo.remark,
      appType: 'smallApp',
      shopDetails: this.data.createOrderSkus.map(shop => ({
        shopId: shop.shopId,
        shopName: shop.shopName,
        shopLogo: shop.shopLogo,
        pickupPhone: shop.shopPhone,
        details: shop.skuList.map(sku => {
          return {
            cartId: sku.cartId,
            productCode: sku.skuInfo?sku.skuInfo.productCode:null,
            productSkuCode: sku.skuInfo?sku.skuInfo.productSkuCode:null,
            productCount: sku.skuCount,
            dispatchType: sku.dispatchType,
            marketCode:sku.marketCode,
            cartProductType:sku.cartProductType,
            packageProductCode:sku.packageProductCode
          }
        })
      })),
      deliveryTime: this.data.deliveryTime,
      groupInfoCode: this.data.groupInfoCode,
      isGroup: this.data.isGroup
    };

    // 定金预售
    if (this.data.preSaleData && this.data.preSaleData.preSaleModel === 'DEPOSIT') {
      // params.balanceStartTime = TimeFormat.dateFormatFilter(this.data.preSaleData.balanceStartTime, 'yyyy-MM-dd hh:mm:ss');
      // params.balanceEndTime = TimeFormat.dateFormatFilter(this.data.preSaleData.balanceEndTime, 'yyyy-MM-dd hh:mm:ss');
      params.balanceStartTime = this.data.preSaleData.balanceStartTime;
      params.balanceEndTime = this.data.preSaleData.balanceEndTime;
      // if (this.data.preSaleData.deliveryModel === 'DATE') {
      //   params.deliveryDate = this.data.preSaleData.deliveryDate;
      // }else {
      //   params.deliveryDate = this.data.preSaleData.balanceEndTime + this.data.preSaleData.deliveryDay * 24 * 60 * 60 * 1000;
      // }
    }
    // console.log()
    return params;
  },

  // 处理库存不足或者价格失效的商品
  // 从预下单订单数据中筛选出库存不足或者价格失效的商品，并展示出来
  filterTroubleGoods(list, type) {
    this.setData({
      failSkuList: list
    });
    this.selectComponent('#failSkus').showDialog(this.data.failSkuList, type ,this.data.marketType);
  },

  // 移除无库存商品并重新预下单展示
  removeNoStockSkus() {
    let stockFailSkuCodes = this.data.failSkuList.map(sku => sku.productSkuCode);
    let oldData = this.data.createOrderSkus;
    for (let i = oldData.length - 1; i >= 0; i--) {
      if (oldData[i].skuList && oldData[i].skuList.length > 0) {
        for (let j = oldData[i].skuList.length - 1; j >= 0; j--) {
          if (stockFailSkuCodes.indexOf(oldData[i].skuList[j].skuInfo.productSkuCode) > -1) {
            oldData[i].skuList.splice(j, 1);
          }
        }
      }
      if (oldData[i].skuList.length <= 0) {
        oldData.splice(i, 1);
      }
    }

    if (oldData.length <= 0) {
      // 全部删除了，提示并返回
      wx.showModal({
        title: '订单失效',
        content: '订单内不包含任何商品！',
        showCancel: false,
        success(res) {
          if (res.confirm) {
            wx.navigateBack({
              delta: 1
            })
          }
        }
      })
    } else {
      // 重新拉取预下单展示
      this.setData({
        checkPriceChange: false,
        selectedCoupon: null
      }); // 需要清除之前选择的优惠券
      this.preCreateOrder();
    }
  },

  // 价格变动继续支付,重新拉取预下单展示
  continuePay() {
    this.setData({
      checkPriceChange: false,
      selectedCoupon: null
    }); // 需要清除之前选择的优惠券
    this.createOrder();
  },

  // 预下单
  preCreateOrder() {
    Tip.loading();
    const orderInfo = this.groupOrderParams();
    return ProductApi.fetchPreCreateOrder({
      orderInfo
    })
      .then(res => {
        Tip.loaded();
        console.log('预下单结果：', res);
        if (res.flag) {
          if (!res.data.orderInfo) {
            // 错误处理
            this.setData({
              submitDisabled: true
            })
            setTimeout(() => {
              Tip.toast(res.data.failMsg, null, 'none')
            }, 500);
          } else {
            const orderInfo = res.data.orderInfo


            let goodList = Array.isArray(orderInfo.shopOrders) &&
            orderInfo.shopOrders.reduce((arr, shopItem) => arr.concat(shopItem.preDetailsViewList || []), [])            
            
            // 优惠套餐类型设值
            let marketType = '';
            for (let good of goodList) {
              if (good && good.marketType === 'PACKAGE') {
                marketType = 'PACKAGE';
                break;
              }
            }
            this.setData({
              marketType: marketType
            })
            
            const detail = res.data.orderInfo.shopOrders[0].details[0];
            if (detail.marketType === 'PRESALE') this.getPreSaleDetai(detail);
            // 当商品为拼团商品时，获取拼团活动信息
            if(wx.getStorageSync('createOrderIsGroup') == 'Y') {
              let groupActivity = wx.getStorageSync("createOrderGroupActivity")
              console.log("groupActivity",groupActivity)
              this.setData({
                groupActivity: groupActivity
              })
            }

            // 合计第二件活动的商品，判断是否可用优惠券
            this.isSecondMarketUseCoupon(res.data.orderInfo.shopOrders)

            // 计算商品总重量
            this.countProductWeight(res.data.orderInfo.shopOrders)

            // 处理自提时间和送达时间列表
            this.setData({
              isCityExpress: res.data.isCityExpress,
              isPickUp: res.data.isPickUp
            })
            let mallManagementConfigDeliveryTimeDOList = res.data.mallManagementConfigDeliveryTimeDOList;
            let todayCityExpressTimeList = [];
            let tomorrowCityExpressTimeList = [];
            let todayPickUpTimeList = [];
            let tomorrowPickUpTimeList = [];
            if (mallManagementConfigDeliveryTimeDOList && mallManagementConfigDeliveryTimeDOList.length > 0) {
              mallManagementConfigDeliveryTimeDOList.forEach(deliveryTime => {
                if ('CITY_EXPRESS' === deliveryTime.dispatchType && 'TODAY' === deliveryTime.deliveryType) {
                  todayCityExpressTimeList.push(deliveryTime);
                } else if ('CITY_EXPRESS' === deliveryTime.dispatchType && 'TOMORROW' === deliveryTime.deliveryType) {
                  tomorrowCityExpressTimeList.push(deliveryTime);
                } else if ('PICK_UP' === deliveryTime.dispatchType && 'TODAY' === deliveryTime.deliveryType) {
                  todayPickUpTimeList.push(deliveryTime);
                } else if ('PICK_UP' === deliveryTime.dispatchType && 'TOMORROW' === deliveryTime.deliveryType) {
                  tomorrowPickUpTimeList.push(deliveryTime);
                }
              })
            }

            // 把同城配送的今日送达时间的第一条处理成立即送达时间
            if (todayCityExpressTimeList && todayCityExpressTimeList.length > 0 && !this.data.isPickUpOnly) {
              todayCityExpressTimeList[0].immDelivery = true;
              let todayDeliveryTime = this.getDateStr('TODAY');
              this.setData({
                deliveryTime: todayDeliveryTime + ' ' + todayCityExpressTimeList[0].startTime + '-' + todayCityExpressTimeList[0].endTime,
                immDeliveryShow: true
              })
            }

            this.setData({
              // goodList 长度大于3，商品列表折叠isShowAll=false;小于3时,商品列表平铺显示isShowAll=true
              isShowAll: goodList.length > 3 ? false : true,
              goodList,
              submitDisabled: false,
              orderData: res.data,
              todayCityExpressTimeList: todayCityExpressTimeList,
              tomorrowCityExpressTimeList: tomorrowCityExpressTimeList,
              todayPickUpTimeList: todayPickUpTimeList,
              tomorrowPickUpTimeList: tomorrowPickUpTimeList
            })
            // 只在初始化时赋值提货门店
            if (!this.data.pickupOrganize) {
              this.setData({ pickupOrganize: orderInfo.organizeId })
            }
          }
        } else {
          Tip.toast(res.data, null, 'none');
        }
      })
  },

  // 获取预售信息
  getPreSaleDetai(detail) {
    PreSaleApi.fetchQueryPreSale({ productVO: { marketCode: detail.marketCode } }).then(res => {
      if (res.flag) {
        this.setData({
          marketCode: detail.marketCode,
          marketType: detail.marketType,
          preSaleData: res.data.data,
          deposit: detail.deposit,
          balance: detail.balance
        })
      }
    })
  },

  // 合计第二件活动的商品，判断是否可用优惠券
  async isSecondMarketUseCoupon(productList) {
    let marketCodeslist = []
    // 筛选出有第二件活动的商品
    productList.forEach(product => {
      for (let item of product.details) {
        console.log(item)
        if (item.marketType == 'SECOND') {
          marketCodeslist.push({ marketCode: item.marketCode, productCount: item.productCount })
        }
      }
    })
    console.log('marketCodeslist=============')
    console.log(marketCodeslist)
    if (marketCodeslist && marketCodeslist.length > 0) {
      // 同一marketCode下商品件数》1，则按活动couponFlag判断是否可用优惠券，只有1件则都可以使用优惠券
      for (let marketCode of marketCodeslist) {
        if (marketCode.productCount > 1) {
          const res = await ProductApi.secondMarketByCode({ marketCode: marketCode.marketCode });
          console.log(res)
          if (res.flag && res.data.couponFlag == 'N') {
            marketCode.couponUseFlag = false
          } else {
            marketCode.couponUseFlag = true
          }
        } else {
          marketCode.couponUseFlag = true
        }
      }

      let findCanUseCoupon = marketCodeslist.find(item => item.couponUseFlag == true)
      this.setData({
        secondMarketUseCoupon: findCanUseCoupon ? true : false
      })
    } else {
      this.setData({
        secondMarketUseCoupon: true
      })
    }
  },
  // 超市正式下单 绑定分销上下级
  createOrder() {
    this.data.repeatCount = 0;
    Tip.loading();
    const orderInfo = this.groupOrderParams();

    ProductApi.fetchCreateSupperOrder({
      orderInfo
    }).then(res => {
      console.log('正式创建订单结果：', res);
      if (res.flag) {
        this.setData({
          orderNo: res.data.orderNo
        });
        if (res.data.failType === 'STOCK_FAIL') {
          Tip.loaded();
          this.filterTroubleGoods(res.data.failSkuList, 'STOCK_FAIL');
        }
        if (res.data.failType === 'SALE_LEAST') {
          Tip.loaded();
          this.filterTroubleGoods(res.data.failSkuList, 'SALE_LEAST');
        }
        if (res.data.failType === 'PROCESS') {
          this.checkCreateOrderResRepeat();
        }
      } else {
        Tip.loaded();
        Tip.toast(res.data, null, 'none');
      }
    })
  },

  // 轮询查询创建订单结果
  checkCreateOrderResRepeat() {
    if (!this.data.orderNo) return
    ProductApi.fetchCheckCreateOrderRes({
      orderInfo: {
        orderNo: this.data.orderNo
      }
    })
      .then(res => {
        console.log('检查订单结果：', res);
        if (res.flag) {
          this.handleCreateOrderResult(res.data);
        } else {
          Tip.loaded();
          Tip.toast(res.data, null, 'none');
        }
      })
  },

  // 判断是否在当前页面
  isCurrentPage() {
    const path = Pages.getCurrentPageUrl();
    console.log('当前页面：', path);
    return path && path === 'pages/newPackage/createMarkOrder/createMarkOrder';
  },

  // 处理下单结果各种状态
  handleCreateOrderResult(result) {
    switch (result.failType) {
      case 'PROCESS': // 下单中
        if (this.isCurrentPage()) {
          let timer = setTimeout(() => {
            this.checkCreateOrderResRepeat();
            clearTimeout(timer);
          }, 1000);
        }
        break;
      case 'FAIL': // 下单失败
        Tip.loaded();
        Tip.toast(result.failMsg, null, 'none');
        break;
      case 'STOCK_FAIL': // 部分商品库存不足
        Tip.loaded();
        this.filterTroubleGoods(result.failSkuList, 'STOCK_FAIL');
        break;
      case 'EXPIRE': // 部分商品价格失效
        Tip.loaded();
        this.filterTroubleGoods(result.failSkuList, 'EXPIRE');
        break;
      case 'GOODS_OFFLINE':
        Tip.loaded();
        this.filterTroubleGoods(result.failSkuList, 'GOODS_OFFLINE');
        break;
      case 'REQUEST_SUCCESS': // 下单成功
        this.pullWechatPay();
        break;
      case 'SALE_LEAST': // 部分商品不满足最低售卖
        Tip.loaded();
        this.filterTroubleGoods(result.failSkuList, 'SALE_LEAST');
        break;
      case 'SALE_LIMIT': // 部分商品不满足限购条件
        Tip.loaded();
        Tip.toast(result.failMsg, null, 'none');
        break;
    }
  },

  // 获取支付参数
  // 支付完成或者结束都强制跳往订单页面，防止订单重复支付
  pullWechatPay() {
    WxPay.getNewWxPayConfig(this.data.orderNo, this.data.prepaidPwdToken)
      .then(res => {
        Tip.loaded();
        console.log('支付结果：', res);
        Tip.toast('支付完成', null);
        //判断是否是拼团活动
        if ('Y' === this.data.isGroup) {
          // 将订单号传入拼团成功页面
          wx.setStorageSync('createOrderOrderNo', this.data.orderNo);
          // 将orderInfo传入拼团成功页面
          wx.setStorageSync('createOrderOrderInfo', this.data.orderData.orderInfo);
          // 将订单总金额传入拼团成功页面
          let createOrderTotalPrice = "";
          if (this.data.orderData.orderInfo.actualPayPrice && this.data.orderData.orderInfo.actualPayPoints) {
            createOrderTotalPrice = '￥'+ this.data.orderData.orderInfo.actualPayPrice + '+' + this.data.orderData.orderInfo.actualPayPoints + '积分';
          } else if (this.data.orderData.orderInfo.actualPayPrice) {
            createOrderTotalPrice = '￥'+ this.data.orderData.orderInfo.actualPayPrice;
          } else if (this.data.orderData.orderInfo.actualPayPoints) {
            createOrderTotalPrice = this.data.orderData.orderInfo.actualPayPoints + '积分';
          } else {
            createOrderTotalPrice = '￥0';
          }
          wx.setStorageSync('createOrderTotalPrice', createOrderTotalPrice);
          // 跳转至拼团成功页面
          wx.redirectTo({
            url: '/pages/newPackage/group/groupResult/groupResult'
          })
        } else {
          //跳转至超市订单列表
          wx.redirectTo({
            url: '/pages/newPackage/markOrders/markOrders'
          })
        }
      })
      .catch(err => {
        Tip.loaded();
        if (typeof err === 'string') {
          Tip.toast(err, null, 'none');
        }
        wx.redirectTo({
          url: '/pages/newPackage/markOrders/markOrders'
        })
      })
  },

  // 检查赠品是否已经发放完
  checkGiveawayStore() {
    const orderGifts = this.data.orderData.orderGifts
    ProductApi.fetchPreCheckGiveawayStock({
      orderGifts
    }).then(res => {
      if (res.data) {
        // 提交订单
        this.submitValidOrder();
      } else {
        wx.showModal({
          title: '赠品已送完，确定下单吗？',
          success: (res) => {
            if (res.confirm) {
              // 提交订单
              this.submitValidOrder();
            } else if (res.cancel) {
              // do nothing
            }
          }
        })
      }
    })
  },

  // 提交订单
  submitValidOrder() {
    console.log('提交订单');
    if (this.data.needAddress && !this.data.selectedAddress) {
      Tip.toast('请选择收货地址', null, 'none');
      return
    }
    if (!this.data.deliveryTime && this.data.marketType != 'PRESALE' && 'Y' != wx.getStorageSync('createOrderIsGroup') && 'CITY_EXPRESS' === this.data.dispatchType.value && 'Y' === this.data.isCityExpress) {
      Tip.toast('请选择送达时间', null, 'none');
      return
    }
    if (!this.data.deliveryTime && this.data.marketType != 'PRESALE' && 'Y' != wx.getStorageSync('createOrderIsGroup') && 'PICK_UP' === this.data.dispatchType.value && 'Y' === this.data.isPickUp) {
      Tip.toast('请选择自提时间', null, 'none');
      return
    }
    if (this.data.orderData && this.data.orderData.orderInfo && this.data.orderData.orderInfo.actualPayPrice > 0 && !this.data.cardPay && !this.data.wechatPay) {
      Tip.toast('请选择支付方式', null, 'none');
      return
    }
    if (this.data.preSaleData && this.data.preSaleData.preSaleModel === 'DEPOSIT') {
      wx.showModal({
        title: '温馨提示',
        content: '预售商品，定金不退，是否继续支付？',
        showCancel: true,
        cancelText: '我再想想',
        confirmColor: '#FB5B41',
        confirmText: '继续支付',
        success: res => {
          if (res.confirm) {
            this.submitOrder();
          }
        }
      })
    } else {
      this.submitOrder();
    }
  },

  // 提交订单
  submitOrder() {
    if (this.data.cardPay) {
      if (this.data.prepaidPayPrice && this.data.prepaidPayPrice > 0) {
        if (this.data.valueCard.ispwd === 'N') {
          wx.showModal({
            title: '密码未设置',
            content: '您还未设置储值卡密码，是否立即前往设置？',
            confirmText: '立即前往',
            confirmColor: '#FB5B41',
            success: res => {
              if (res.confirm) {
                wx.navigateTo({
                  url: '/pages/newPackage/setPassword/setPassword'
                })
              }
            }
          })
        } else if (this.data.prepaidPayPrice > this.data.valueCard.amount) {
          Tip.toast('储值卡余额不足', null, 'none');
        } else {
          this.setData({
            showCenterDialog: true
          });
        }
      } else {
        Tip.toast('请输入储值卡支付金额', null, 'none');
      }
    } else {
      this.createOrder();
    }
  },

  // 查看更多按钮
  showMore(e) {
    let flag = this.data.isShowAll;
    this.data.isShowAll = !flag;
    this.setData({ isShowAll: this.data.isShowAll });
  },

  // 打开运费弹窗
  openDeliverDialog() {
    if (!this.data.orderData.orderInfo.totalDeliveryPrice) {
      return false
    }
    this.setData({
      isShowDeliverDialog: true
    })
  },

  // 打开会员权益弹窗
  openMemberRightsDialog(e) {
    let rightsType = e.currentTarget.dataset.type;
    this.selectComponent('#memberRightsDialog').showDialog(rightsType,this.data.orderData.orderInfo.extraInfo);
  },

  // 获取自提门店数据
  getOrganizeList() {
    let selectedOrganize = wx.getStorageSync('selectedOrganizeIds');
    if (selectedOrganize) {
      PreSaleApi.fetchQueryOrganizeInfo({ selectedOrganize: selectedOrganize }).then(res => {
        console.log("门店数据", res)
        if (res.flag) {
          this.setData({
            organizeList: res.data,
          })
        }
      })
    }
  },

  onLoad() {
    this.getOrganizeList();
    this.getDistributionDictList()
    this.initOrderInfo();
    this.getValueCardInfo();
  }
});