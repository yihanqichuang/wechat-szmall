// 获取全局应用程序实例对象
// const app = getApp()
import Tip from '../../../utils/tip';
import OrderApi from '../../../api/order';

// 创建页面实例对象
Page({
  data: {
    refundOrderNo: null,
    shopOrderNo: null,
    productSkuCode: null,
    refundData: null,
    index: 0, // 选择的快递公司下标
    step: 0,
    expressList: [],
    refundState: {
      title: '申请中',
      subTitle: '申请已提交，请联系商家确认售后事宜。'
    },
    refundWays: {
      AUTO_REFUND: '自动退款',
      USER_REFUND: '用户退款',
      MANAGER_REFUND: '管理员退款'
    }
  },

  onLoad(option) {
    if (option.refundOrderNo) {
      this.setData({
        refundOrderNo: option.refundOrderNo,
        packageProductCode: option.packageProductCode
      });
      this.getRefundDetail();
    } else {
      this.setData({
        shopOrderNo: option.shopOrderNo,
        productSkuCode: option.productSkuCode,
        packageProductCode: option.packageProductCode
      });
      this.getRefundDetailBySku();
    }
    this.getDeliveryList();
  },

  // 预览图片
  previewImage(e) {
    const url = e.currentTarget.dataset.url;
    wx.previewImage({
      urls: this.data.refundData.refundPics,
      current: url
    })
  },

  // 删除售后订单
  deleteRefundOrder() {
    wx.showModal({
      title: '确定删除订单？',
      success: (res) => {
        if (res.confirm) {
          console.log('用户点击确定')
          this.doDelete();
        } else if (res.cancel) {
          console.log('用户点击取消')
        }
      }
    })
  },
  doDelete() {
    Tip.loading();
    OrderApi.fetchDeleteRefundOrder({
      refundOrderNo: this.data.refundData.refundOrderNo
    }).then(res => {
      Tip.loaded();
      if (res.data.flag) {
        this.goBack();
      } else {
        Tip.toast(res.data.msg, null, 'none');
      }
    })
  },
  // 删除完成，返回上一页
  goBack() {
    const pages = getCurrentPages();
    const prevPage = pages[pages.length - 2];
    prevPage.refresh();
    wx.navigateBack({
      delta: 1
    });
  },

  // 获取物流公司列表
  getDeliveryList() {
    OrderApi.fetchDeliveryList({
      key: 'express'
    }).then(res => {
      if (res.data.flag) {
        this.setData({
          expressList: res.data.data
        });
      }
    })
  },

  // 输入运单号
  handleInputDeliveryNo(e) {
    console.log('单号：', e);
    this.setData({
      ['refundData.deliveryNo']: e.detail.value
    })
    this.saveDeliveryData();
  },

  // 保存退货物流信息
  saveDeliveryData() {
    if (!this.data.refundData.deliveryName || !this.data.refundData.deliveryNo) return
    const params = {
      refundOrderNo: this.data.refundData.refundOrderNo,
      deliveryName: this.data.refundData.deliveryName,
      deliveryNo: this.data.refundData.deliveryNo
    };
    Tip.loading();
    OrderApi.fetchRefundInfoComplete(params).then(res => {
      Tip.loaded();
      if (res.data.flag) {
        Tip.toast('已保存');
      } else {
        Tip.toast(res.data.msg, null, 'none');
      }
    })
  },

  // 选择物流
  bindPickerChange(e) {
    console.log('选择的：', e);
    let _index = e.detail.value;
    let _name = null;
    if (this.data.expressList.length) {
      _name = this.data.expressList[_index].value;
    }
    this.setData({
      index: _index,
      ['refundData.deliveryName']: _name
    });
    this.saveDeliveryData();
  },

  // 处理售后状态
  handleRefundState(state) {
    if (state === 'MERCHANT_PICK' || state === 'MERCHANT_AUDIT') return

    const refundStateObj = {
      'REMIT_FAIL': {
        ['refundState.title']: '退款中',
        ['refundState.subTitle']: '退款中，请留意退款进度。',
        step: 2
      },
      'REPULSE_REMIT': {
        ['refundState.title']: '申请失败',
        ['refundState.subTitle']: '审核不通过，请联系商家。',
        step: 1
      },
      'REMIT_SUCCESS': {
        ['refundState.title']: '已完成',
        ['refundState.subTitle']: '退款完成，请注意查收。',
        step: 3
      },
      'REMIT_CLOSE': {
        ['refundState.title']: '已关闭',
        ['refundState.subTitle']: '退款申请已关闭。',
        step: 3
      },
      'REFUND_DIFF_WAIT': {
        ['refundState.title']: '待退款',
        ['refundState.subTitle']: '商家退差价，待退款。',
        step: 1
      },
      'REFUND_DIFF_FAIL': {
        ['refundState.title']: '退款失败',
        ['refundState.subTitle']: '商家退差价，打款失败/待退款。',
        step: 2
      },
      'REFUND_DIFF_SUCCESS': {
        ['refundState.title']: '已完成',
        ['refundState.subTitle']: '商家退差价，退款成功。',
        step: 3
      }
    }
    this.setData(refundStateObj[state])

    // if (state === 'REMIT_FAIL') {
    //   this.setData({
    //     ['refundState.title']: '退款中',
    //     ['refundState.subTitle']: '退款中，请留意退款进度。',
    //     step: 2
    //   });
    // }
    // if (state === 'REPULSE_REMIT') {
    //   this.setData({
    //     ['refundState.title']: '申请失败',
    //     ['refundState.subTitle']: '审核不通过，请联系商家。',
    //     step: 1
    //   });
    // }
    // if (state === 'REMIT_SUCCESS') {
    //   this.setData({
    //     ['refundState.title']: '已完成',
    //     ['refundState.subTitle']: '退款完成，请注意查收。',
    //     step: 3
    //   });
    // }
    // if (state === 'REMIT_CLOSE') {
    //   this.setData({
    //     ['refundState.title']: '已关闭',
    //     ['refundState.subTitle']: '退款申请已关闭。',
    //     step: 3
    //   });
    // }

  },

  // 撤销申请
  revertApply() {
    wx.showModal({
      title: '温馨提示',
      content: '您将撤销本次申请，如问题未解决，您还可再次发起，确定撤销？',
      showCancel: true,
      cancelText: '再想想',
      confirmColor: '#FB5B41',
      success: res => {
        if (res.confirm) {
          Tip.loading();
          OrderApi.fetchRevertRefundApply({ refundOrderNo: this.data.refundData.refundOrderNo }).then(res => {
            Tip.loaded();
            if (res.data.flag) {
              Tip.toast('已撤销');
              wx.navigateBack({
                delta: 1
              });
            } else {
              Tip.toast(res.data.msg, null, 'none');
            }
          })
        }
      }
    })
  },

  // 重新申请
  applyAgain() {
    wx.navigateBack({
      delta: 1
    });
  },
  // 重新申请
  applyRefund() {
    if (!this.data.refundData.dispatchType) {
      Tip.toast(`后端接口wmallerWechat/refundDetail，需要返回dispatchType值`, null, 'none')
      return false
    }
    wx.navigateTo({
      url: '/pages/newPackage/orderRefund/orderRefund',
      success: (res) => {
        res.eventChannel.emit('passSku', {
          sku: Object.assign({}, this.data.refundData, {
            refundOtherPrice: this.data.refundData.refundOtherPrice, // 微信支付金额
            refundPrepaidPrice: this.data.refundData.refundPrepaidPrice, // 储值卡支付金额
            refundPoints: this.data.refundData.refundPoints, // 积分
          },
            this.data.refundData.refundDetailInfoList.reduce((obj, item) => ({
              productCount: (obj.productCount || 0) + item.productCount,
            }), {})
          )
        });
      }
    })
  },

  // 获取售后详情
  getRefundDetailBySku() {
    const params = {
      shopOrderNo: this.data.shopOrderNo,
      productSkuCode: this.data.productSkuCode,
      packageProductCode: this.data.packageProductCode
    }
    Tip.loading();
    OrderApi.fetchRefundDetail2(params).then(res => {
      Tip.loaded();
      if (res.data.flag) {
        if (res.data.data.refundPics) {
          res.data.data.refundPics = res.data.data.refundPics.split(',');
        }
        this.handleRefundState(res.data.data.refundState);
        this.setData({
          refundData: res.data.data
        });
      } else {
        Tip.toast(res.data.msg, null, 'none');
      }
    })
  },
  // 获取售后详情
  getRefundDetail() {
    Tip.loading();
    OrderApi.fetchRefundDetail({
      refundOrderNo: this.data.refundOrderNo
    }).then(res => {
      Tip.loaded();
      if (res.data.flag) {
        if (res.data.data.refundPics) {
          res.data.data.refundPics = res.data.data.refundPics.split(',');
        }
        this.handleRefundState(res.data.data.refundState);
        this.setData({
          refundData: res.data.data
        });
      } else {
        Tip.toast(res.data.msg, null, 'none');
      }
    })
  }
});