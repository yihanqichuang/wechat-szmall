// 获取全局应用程序实例对象
const app = getApp();
import appointmentApi from '../../../api/appointment';
import Tip from '../../../utils/tip'

// 创建页面实例对象
Page({
  /**
   * 页面的初始数据
   */
  data: {
    appointmentOrder: null, // 预约单信息
    appointmentInfo: null, // 预约信息
    expireDate: null,//商家确认截止日期
  },

  // 取消订单
  cancelAppointment() {
    let that = this;
    wx.showModal({
      title: '提示',
      content: '确定要取消订单吗？',
      success: function (sm) {
        if (sm.confirm) {
          let params = {
            shopOrderNo: that.data.appointmentOrder.shopOrderNo
          };
          appointmentApi.cancelAppointmentOrder(params).then(res => {
            if (res.data.flag) {
              Tip.toast('取消订单成功');
              wx.navigateBack({
                delta: 1
              })
            }
          })
        } else if (sm.cancel) {
          console.log('用户点击取消')
        }
      }
    })
  },

  // 获取订单列表
  getAppointmentDetail() {
    let params = {
      shopOrderNo: this.data.appointmentOrder.shopOrderNo
    };
    appointmentApi.getAppointmentOrder(params).then(res => {
      if (res.data.flag) {
        this.setData({
          appointmentInfo: res.data.data
        })
        if (res.data.data.expireDate) {
          this.setData({
            expireDate: res.data.data.expireDate
          })
        }
      }
    })
  },

  onLoad() { },

  onShow() {
    if (wx.getStorageSync('appointmentOrder')) {
      this.setData({
        appointmentOrder: wx.getStorageSync('appointmentOrder')
      })
      this.getAppointmentDetail();
    }
  },

  onReachBottom() {

  }

});