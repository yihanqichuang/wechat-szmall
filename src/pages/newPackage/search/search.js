/*
 * @Author: your name
 * @Date: 2020-09-28 10:08:52
 * @LastEditTime: 2020-10-28 15:36:00
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: /WangMao-yidong/wxml-nbss/src/components/search/search.js
 */
// 获取全局应用程序实例对象
// const app = getApp()
import api from '../../../api/search'
// 创建页面实例对象
Page({
  /**
   * 页面的初始数据
   */
  data: {
    loading: false,
    isLoadOver: false,
    pageNo: 1,
    pageSize: 10,
    shopId: '',
    searchKey: '',
    keywordList: [],
    timer: null,
    historyData: [],
    hotDate: [],
    shopList: [],
    itemList: [],
    emptyType: 'commodity'
  },
  // 获取商品列表数据
  getProductList() {
    let params = {
      productVO: {
        productName: this.data.searchKey,
        shopId: this.data.shopId
      },
      page: {
        pageNo: this.data.pageNo,
        pageSize: this.data.pageSize
      }
    };
    this.setData({ loading: true });
    api.productList(params).then(res => {
      this.setData({ loading: false });
      console.log('商品列表:', res);
      if (res.flag && res.data && res.data.length) {
        this.setData({
          isLoadOver: !res.page || (res.page.pages && res.page.pages <= this.data.pageNo),
          [`itemList[${this.data.pageNo - 1}]`]: res.data
        });
      } else {
        this.setData({ isLoadOver: true});
      }
    })
  },

  // 刷新
  refresh() {
    this.setData({ pageNo: 1, itemList: [], shopList: [] });
    if (this.data.shopId) {
      this.getShopList();
    }
    this.getProductList();
  },

  // 加载更多
  loadMore() {
    if (this.data.isLoadOver) return
    this.setData({ pageNo: ++this.data.pageNo });
    this.getProductList();
  },

  // 根据关键字搜索店铺列表
  getShopList() {
    api.getShopListNew({shopName: this.data.searchKey}).then(res => {
      console.log('店铺列表：', res);
      if (res.data.flag) {
        this.setData({shopList: res.data.shopList})
      }
    });
  },

  // 取消按钮
  cancel() {
    this.setData({
      shopList: [],
      itemList: [],
      keywordList: [],
      searchKey: '',
      shopId: ''
    });
  },
  clearText() {
    this.setData({
      searchKey: ''
    });
  },
  // 获取热门搜索
  getHotSearch() {
    api.hotSearchKeyword({rowSize: 8}).then(res => {
      console.log('热搜关键字：', res);
      if (res.flag) {
        this.setData({hotDate: res.data})
      }
    })
  },
  goShop(e) {
    wx.navigateTo({
      url: '/pages/newPackage/findShopDetailNew/findShopDetail?shopId=' + e.target.dataset.item
    })
  },
  // 清空最近搜索
  clearNearlySearch() {
    wx.removeStorageSync('searchHistory');
    this.setData({
      historyData: []
    })
  },

  // 加载最近搜索
  loadNearlySearch() {
    let searchText = wx.getStorageSync('searchHistory');
    if (searchText) {
      this.setData({
        historyData: searchText
      });
    }
  },

  // 存储最近搜索
  saveNearlySearch(text) {
    let hisData = this.data.historyData
    if (this.data.historyData.length >= 8) {
      hisData.pop()
      hisData.unshift(text)
    } else {
      hisData.unshift(text)
    }
    let hisd = [...new Set(hisData)]
    wx.setStorageSync('searchHistory', hisd);
    this.setData({
      searchKey: text,
      historyData: hisd
    })
  },

  // 输入框回车搜索
  bindConfirm(e) {
    this.setData({ searchKey: e.detail.value });
    this.refresh();
  },
  // 点击跳转
  toSearch(e) {
    console.log('点击的关键词', e.currentTarget.dataset.item);
    let searchKey = e.currentTarget.dataset.item.keyword;
    let categoryId = e.currentTarget.dataset.item.categoryId;
    let type = e.currentTarget.dataset.item.hrefType;
    let shopId = e.currentTarget.dataset.item.shopId;
    let lastCategoryIdList = e.currentTarget.dataset.item.searchText ? e.currentTarget.dataset.item.searchText : '';
    this.setData({shopId, searchKey});
    this.saveNearlySearch(searchKey);
    if (type === 'CATEGORY') {
      wx.navigateTo({
        url: `/pages/newPackage/specialListNew/specialList?categoryId=${categoryId}&searchKey=${searchKey}&lastCategoryIdList=${lastCategoryIdList}`
      })
    } else {
      this.refresh();
    }
  },
  // 历史跳转
  historySearch(e) {
    this.setData({ searchKey: e.target.dataset.item });
    this.refresh();
  },
  // 顶部搜索栏搜索方法
  handleInput(e) {
    this.setData({searchKey: e.detail.value});
    clearTimeout(this.data.timer);
    this.data.timer = setTimeout(() => {
      this.getSearchKeywords();
    }, 300)
  },
  // 获取关键词列表
  getSearchKeywords() {
    api.searchKeyword({keyword: this.data.searchKey}).then(res => {
      console.log('关键词列表：', res.data);
      if (res.flag) {
        this.setData({keywordList: res.data})
      }
    })
  },

  onLoad(option) {
    if (option.searchKey) {
      this.setData({
        searchKey: option.searchKey
      });
    }
    this.getHotSearch();
    this.loadNearlySearch();
  },

  onReachBottom() {
    this.loadMore();
  }
})
