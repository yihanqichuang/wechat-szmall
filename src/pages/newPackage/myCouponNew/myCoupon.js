// 获取全局应用程序实例对象
// const app = getApp()
import CouponApi from '../../../api/coupon'
import QR from '../../../utils/wxqrcode.min.js';
import TimeFormat from '../../../utils/timeFormat.js';
Page({
  data: {
    Tabs: [
      { name: '线上', type: 'ONLINE' },
      { name: '线下', type: 'OFFLINE' }
    ],
    couponTypes: [
      { name: '代金券', type: 'CASH' },
      { name: '折扣券', type: 'DISCOUNT' },
      { name: '礼品券', type: 'GIFT' },
      { name: '停车券', type: 'PARKING' }
    ],
    stateList: [
      { type: 'UNUSE', name: '未使用' },
      { type: 'USE', name: '已使用' },
      { type: 'EXPIRE', name: '已失效' },
      { type: 'REFUND', name: '已退款' }
    ],
    currentTab: 'ONLINE',
    couponType: 'CASH',
    couponState: 'UNUSE',
    couponStateName: '未使用',
    couponList: [],
    loading: false,
    isLoadOver: false,
    isFilterShow: false,
    current: 1,
    size: 10,
    showCode: false,
    currentCoupon: null,
    qrcode: null,
    timer: null
  },

  // 切换filter
  toggleFilterShow() {
    this.setData({ isFilterShow: !this.data.isFilterShow });
  },

  // 切换状态
  switchState(e) {
    const state = e.currentTarget.dataset.state;
    this.setData({
      isFilterShow: false,
      couponState: state.type,
      couponStateName: state.name
    });
    this.refresh();
  },

  // 切换券类型
  switchCouponType(e) {
    const type = e.currentTarget.dataset.type;
    this.setData({
      isFilterShow: false,
      couponState: 'UNUSE',
      couponStateName: '未使用',
      couponType: type
    });
    this.refresh();
  },

  // 切换tab
  switchTab(e) {
    console.log('当前tab：', e);
    this.setData({
      isFilterShow: false,
      currentTab: e.currentTarget.dataset.type,
      couponType: 'CASH',
      couponState: 'UNUSE',
      couponStateName: '未使用'
    });
    this.refresh();
  },

  // 刷新
  refresh() {
    this.setData({ current: 1, couponList: [] });
    this.getCouponList();
  },

  // 加载更多
  loadMore() {
    if (this.data.isLoadOver) return
    this.setData({ current: ++this.data.current });
    this.getCouponList();
  },

  // 获取优惠券列表
  getCouponList() {
    let params = {
      useState: this.data.couponState,
      verifyWay: this.data.currentTab,
      couponTypeKind: this.data.couponType,
      page: {
        current: this.data.current,
        size: this.data.size
      }
    };
    this.setData({ loading: true });
    CouponApi.userCouponList(params)
      .then(res => {
        this.setData({ loading: false });
        console.log('优惠券列表：', res);
        if (res.data.flag && res.data.data.records && res.data.data.records.length) {
          this.setData({
            isLoadOver: (res.data.data.pages && res.data.data.pages <= this.data.current),
            [`couponList[${this.data.current - 1}]`]: res.data.data.records
          });
        } else {
          this.setData({
            isLoadOver: true,
            collectList: []
          });
        }
      })
  },

  // 去使用优惠券
  showUseCode(e) {
      wx.navigateTo({
          url:'/pages/discover/specialList/specialList?parentCategoryId=0&level=1'
      })
    // const coupon = e.currentTarget.dataset.coupon;
    // console.log('当前券：', coupon);
    // const codeStr = String(coupon.verifyCode) + 1;
    // const qrcode = QR.createQrCodeImg(codeStr);
    // if (coupon.couponTypeKind !== 'PARKING') {
    //   this.startTime();
    // }
    // this.setData({
    //   showCode: true,
    //   qrcode,
    //   currentCoupon: coupon
    // })
  },

  // 关闭优惠券核销码
  hideUseCode() {
    const timer = this.data.timer
    timer && clearTimeout(timer);
    this.setData({
      showCode: false,
      qrcode: null,
      currentCoupon: null
    })
  },

  // 开始时间递增
  startTime() {
    const _this = this;
    const timeStamp = new Date().getTime();
    const formatTime = TimeFormat.dateFormatFilter(timeStamp, 'yyyy-MM-dd hh:mm:ss');
    this.setData({ currentTime: formatTime });
    this.data.timer = setTimeout(_this.startTime, 1000)
  },

  // 进入详情
  goDetail(e) {
    let coupon = e.currentTarget.dataset.coupon;
    if (coupon.useState !== 'UNUSE') return
    wx.navigateTo({
      url: `/pages/newPackage/myCouponNew/detail/detail`,
      events: {
        acceptDataFromOpenedPage: (res) => {
          this.changeCouponState(res.data);
        }
      },
      success: (res) => {
        // 优惠券传送到详情页
        res.eventChannel.emit('sendCoupon', { coupon });
      }
    })
  },

  // 退款后修改优惠券状态
  changeCouponState(coupon) {
    this.data.couponList.forEach((arr, index) => {
      arr.forEach((ele, index1) => {
        if (ele.couponId === coupon.couponId) {
          this.setData({
            [`couponList[${index}][${index1}].useState`]: coupon.useState
          })
        }
      })
    })
  },

  onHide() {
    this.hideUseCode();
  },

  onLoad(e) {
    console.log(e)
    if (e.currentTab) {
      this.setData({
        currentTab: e.currentTab
      })
    }
    if (e.couponType) {
      this.setData({
        couponType: e.couponType
      })
    }
    if (e.couponState) {
      this.setData({
        couponState: e.couponState
      })
    }
    this.refresh();
  },
  onReachBottom() {
    this.loadMore();
  }
});
