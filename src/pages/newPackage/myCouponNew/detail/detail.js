// 获取全局应用程序实例对象
const app = getApp();
import TimeFormat from '../../../../utils/timeFormat.js';
import QR from '../../../../utils/wxqrcode.min.js';
import Tip from '../../../../utils/tip'
import CouponApi from '../../../../api/coupon'
// 创建页面实例对象
Page({
  /**
   * 页面的初始数据
   */
  data: {
    coupon: null,
    timer: null,
    showCode: false,
    currentTime: null,
    qrcode: null,
    alreadyRefund: false,  // 是否已退券
    logoImg: ''
  },

  onLoad(option) {
    const eventChannel = this.getOpenerEventChannel();
    eventChannel.on('sendCoupon', (data) => {
      this.setData({ 
        coupon: data.coupon,
        logoImg: ''
      });
    })
  },

  // 显示优惠券核销码
  showUseCode() {
    wx.navigateTo({
        url:'/pages/discover/specialList/specialList?parentCategoryId=0&level=1'
    })  
    // const codeStr = String(this.data.coupon.verifyCode) + '1';
    // const qrcode = QR.createQrCodeImg(codeStr);
    // if (this.data.coupon.couponTypeKind !== 'PARKING') {
    //   this.startTime();
    // }
    // this.setData({
    //   showCode: true,
    //   qrcode
    // })
  },

  // 关闭优惠券核销码
  hideUseCode() {
    const timer = this.data.timer
    timer && clearTimeout(timer);
    this.setData({
      showCode: false,
      qrcode: null
    })
  },

  // 开始时间递增
  startTime() {
    const _this = this;
    const timeStamp = new Date().getTime();
    const formatTime = TimeFormat.dateFormatFilter(timeStamp, 'yyyy-MM-dd hh:mm:ss');
    this.setData({ currentTime: formatTime });
    this.data.timer = setTimeout(_this.startTime,  1000)
  },

  // 退券
  refundCoupon() {
    Tip.loading();
    CouponApi.applyRefund({refundVo: { productSkuCode: this.data.coupon.couponId, orderNo: this.data.coupon.buyOrderNo }})
        .then(res => {
          console.log(res)
          Tip.loaded();
          if (res.flag) {
            Tip.toast('退劵成功!', null);
            this.setData({ alreadyRefund: true })
            setTimeout(() => {
              this.backToPrePage(true);
            }, 2000);
          } else {
            Tip.toast(res.data, null, 'none');
          }
        })
  },
  // 返回上一页
  backToPrePage(isReloadPrePage = false) {
    const eventChannel = this.getOpenerEventChannel();
    eventChannel.emit('acceptDataFromOpenedPage', { data: this.data.coupon });
    if (isReloadPrePage) {
      const pages = getCurrentPages(),
      prePage = pages[pages.length-2]
      prePage.refresh();
    }
    wx.navigateBack({ delta: 1 });
  }
});
