// 获取全局应用程序实例对象
const app = getApp();
import OrderApi from '../../../api/order';
import Tip from '../../../utils/tip';
import WxPay from '../../../utils/wxPay';
import ShopApi from '../../../api/findshop';
import PreSaleApi from '../../../api/preSale';
import GroupApi from '../../../api/group';
import eleInvoice from '../../../api/eleInvoice'
import benMath from '../../../utils/BenMath.js';
import formatMoney from '../../../utils/formatMoney.js';
const QR = require('../../../utils/wxqrcode.min.js');

// 创建页面实例对象
Page({
  data: {
    groupActivity: null,// 拼团活动对象
    extraInfo: '', //会员权益对象
    type: null,
    orderNo: null,
    orderData: null,
    needAddress: false,
    showActionSheet: false,
    countDownTime: 0,

    // 联系商家列表
    callStore: [{
      text: '拨打',
      value: '1'
    }],

    dispatchObj: {
      PICK_UP: '到店自提',
      CITY_EXPRESS: '同城配送'
    },
    payTypeList: {
      WECHAT: '微信',
      PREPAID: '储值卡',
      PREPAID_WECHAT: '微信+储值卡'
    },
    stateList: {
      CLOSE: {
        title: '订单已关闭',
        subTitle: '订单已关闭'
      },
      UNPAY: {
        title: '待支付',
        subTitle: '订单待付款，12分钟后将自动关闭'
      },
      BALANCE_UNPAY: {
        title: '待支付',
        subTitle: '订单待付款，12分钟后将自动关闭'
      },
      PAID: {
        title: '已付款,待发/提货',
        subTitle: '订单已付款，商家将尽快发货'
      },
      WAIT_RECEIVE: {
        title: '待收货/待提货',
        subTitle: '您的订单已确认，感谢您的选购'
      },
      FINISH: {
        title: '已完成',
        subTitle: '订单已完成，期待您的下次光临'
      },
      MERCHANT_AUDIT: {
        // 退款中
        title: '退货/退款中',
        subTitle: '订单申请退货/退款中'
      }
    },
    showPickUpCode: false,
    codeStr: '',
    showDeliveryInfo: false,
    codeImage: null,
    pickupCode: null,
    isShowAll: true, // 商品列表是否展开Flag
    isShowDeliverDialog: false,// 打开运费弹窗    preSaleData:null, //预售信息
    marketType: null,
    deposit: 0, // 定金
    balance: 0, //尾款
    currentTime: new Date().getTime(),
    balanceEndTime: null,
    balanceStartTime: null,
    totalWeight: 0, // 商品重量
    showInvoiceButton: true,
    flag: ''
  },

  // 打开会员权益弹窗
  openMemberRightsDialog(e) {
    let rightsType = e.currentTarget.dataset.type;
    this.selectComponent('#memberRightsDialog').showDialog(rightsType, this.data.extraInfo);
  },

  showDialog() {
    console.log('触发显示');
    this.setData({
      showActionSheet: true
    });
  },
  // 查看物流
  viewLogistics() {
    wx.navigateTo({
      url: '../logisticsMap/logisticsMap?orderNo=' + this.data.orderData.orderNo
    });
  },
  // 显示物流信息
  openDeliveryDialog() {
    this.setData({
      showDeliveryInfo: true
    });
  },
  // 关闭物流信息
  closeDeliveryInfo() {
    this.setData({
      showDeliveryInfo: false
    });
  },
  // 拨打电话
  makeCall(e) {
    const value = e.detail.value;
    if (value === '1') {
      wx.makePhoneCall({
        phoneNumber: this.data.orderData.pickupPhone
      })
    }
  },

  // 复制运单号
  copyExpressNo() {
    if (!this.data.orderData.expressInfo) return;
    wx.setClipboardData({
      data: this.data.orderData.expressInfo.expressNo,
      success(res) {
        Tip.toast('复制成功', null, 'none');
      }
    });
  },
  // 查看售后
  viewAales() {
    wx.redirectTo({
      url: `/pages/newPackage/orderRefundDetail/orderRefundDetail?refundOrderNo=${this.data.orderData.refundOrderNo}`
    })
  },
  bindPickerChange(e) {
    console.log(e);
  },

  // 去开票
  makeInvoice(e) {
    console.log("orderData", this.data.orderData)
    let actualPayPrice = this.data.orderData.deliveryOtherPrice || 0;
    this.data.orderData.orderDetail.forEach(item => {
      if (item.marketType === 'PRESALE') {
        actualPayPrice = benMath.accSub(this.data.orderData.actualPayPrice, (item.refundPrice || 0));
      } else {
        // 退款成功的商品金额减去
        if (item.afterSaleState && item.afterSaleState === 'REMIT_SUCCESS') {
          actualPayPrice = actualPayPrice;
        } else {
          // 商品金额相加并减去退差价金额
          actualPayPrice = benMath.accSub(benMath.accAdd(actualPayPrice, item.otherPayPrice), (item.refundPrice || 0));
        }
      }

    });
    console.log("实付金额", actualPayPrice)
    if (actualPayPrice <= 0) {
      Tip.toast('发票金额异常,请前往线下开票', null, 'none');
      return;
    }
    let industryType = 'SuperMarket';
    let orderNo = this.data.orderData.orderNo;
    let orderTime = this.data.orderData.orderTime;
    let orderType = this.data.orderData.orderType;
    let goodsName = this.data.orderData.shopName;
    actualPayPrice = formatMoney.priceFilter(actualPayPrice);
    wx.navigateTo({
      url: `/pages/eleInvoice/makeInvoices/makeInvoices?orderNo=${orderNo}&orderFee=${actualPayPrice}&orderTime=${orderTime}&orderType=${orderType}&goodsName=${goodsName}&industryType=${industryType}`
    })
  },

  // 删除订单
  deleteOrder() {
    wx.showModal({
      title: '确定删除订单？',
      success: (res) => {
        if (res.confirm) {
          console.log('用户点击确定');
          this.doDelete();
        } else if (res.cancel) {
          console.log('用户点击取消');
        }
      }
    });
  },
  doDelete() {
    Tip.loading();
    OrderApi.fetchDeleteMainOrder({
      orderNo: this.data.orderNo
    }).then(res => {
      Tip.loaded();
      console.log('删除结果：', res);
      if (res.data.flag) {
        this.goBack();
      } else {
        Tip.toast(res.data.msg, null, 'none');
      }
    })
  },
  // 跳转尾款支付页面
  payment() {
    let url = '/pages/newPackage/balancePayment/balancePayment'
    wx.navigateTo({
      url: `${url}?orderNo=${this.data.orderNo}`
    })
  },
  // 删除完成，返回上一页
  goBack() {
    const pages = getCurrentPages();
    const prevPage = pages[pages.length - 2];
    prevPage.refresh();
    wx.navigateBack({
      delta: 1
    });
  },
  // 去提货按钮（显示商家地址）
  showStoreAddress() {
    wx.showModal({
      title: '取货地址',
      showCancel: false,
      content: this.data.orderData.organizeAddress,
      success: function (res) {
        if (res.confirm) {
          console.log('用户点击确定')
        }
      }
    });
  },

  // 取消订单
  cancelOrder() {
    let title = '确定取消订单？'
    let flag = true // 是否继续退单
    if (this.data.orderData.orderState === 'UNPAY') {
      // 如果订单未支付，则可直接取消订单
      wx.showModal({
        title: title,
        success: (res) => {
          if (res.confirm) {
            this.doCancel();
          } else if (res.cancel) {
            console.log('用户点击取消')
          }
        }
      })
    }
    else if (this.data.orderData.orderState === 'BALANCE_UNPAY') {
      title = '预售商品，取消订单，定金不退还，是否取消订单？';
      // 如果订单未支付，则可直接取消订单
      wx.showModal({
        title: title,
        success: (res) => {
          if (res.confirm) {
            this.doCancel();
          } else if (res.cancel) {
            console.log('用户点击取消')
          }
        }
      })
    }
    else if (['PAID'].indexOf(this.data.orderData.orderState) > -1) {
      // 如果订单已支付，则需要校验是否有可退商品
      let sureRefundList = this.data.orderData.orderDetail.filter(item => item.beforeDispatchRefundWay !== 'NOT_REFUND')
      let orderData = this.data.orderData
      if (sureRefundList.length === 0) {
        // 全部商品都不支持退款
        flag = false
        title = '订单中所有商品为不可退商品，不支持退款！'
      } else if (sureRefundList.length === this.data.orderData.orderDetail.length) {
        // 全部商品都可退
      } else {
        // 部分商品可退
        title = `订单中存在不可退款的商品，取消订单部分商品不退款，确定要取消订单吗？`
        orderData = Object.assign({}, orderData, {
          orderDetail: sureRefundList // 可退货商品
        },
          sureRefundList.reduce((obj, item) => {
            console.log(item.productCount, item)
            return {
              productCount: (obj.productCount || 0) + item.productCount, // 可退货的商品数量
              otherPayPrice: (obj.otherPayPrice || 0) + item.otherPayPrice, // 可退货的微信金额
              prepaidPayPrice: (obj.prepaidPayPrice || 0) + item.prepaidPayPrice, // 可退货的储值卡金额
              actualPayPoints: (obj.actualPayPoints || 0) + item.actualPayPoints  // 可退货的积分
            }
          }, {}),
        )
      }
      wx.showModal({
        title: title,
        success: (res) => {
          if (res.confirm) {
            if (flag) {
              wx.navigateTo({
                url: '/pages/newPackage/orderRefund/orderRefund',
                success: (res) => {
                  res.eventChannel.emit('passSku', {
                    orderData: orderData
                  });
                }
              })
            }
          } else if (res.cancel) {
            console.log('用户点击取消')
          }
        }
      })
    }
  },
  doCancel() {
    Tip.loading();
    OrderApi.fetchCancelOrder({
      orderNo: this.data.orderNo
    }).then((res) => {
      Tip.loaded();
      console.log('取消结果：', res);
      if (res.data.flag) {
        this.initData();
        this.goBack();
      } else {
        Tip.toast(res.data.msg, null, 'none');
      }
    });
  },

  // 打开退款弹窗
  orderRefund() {
    wx.navigateTo({
      url: `/pages/newPackage/orderRefundList/orderRefundList?orderNo=${this.data.orderData.orderNo}&organizeType=${this.data.orderData.organizeType}`
    })
  },

  // 获取未支付订单的时效
  getCountTime(orderTime) {
    orderTime = new Date(orderTime).getTime();
    let closeOrderTime = wx.getStorageSync("closeOrderTime");
    const endTime = orderTime + Number(closeOrderTime);
    this.setData({
      countDownTime: endTime
    });
  },

  // 显示配送方式，遍历商品下的sku列表
  // 存在自提显示自提，存在配送限时配送，都存在限时物流+快递
  showDispatchName(skus) {
    if (!skus || skus.length <= 0) return;
    let dispatchTypeNames = [];

    console.log(this.data.orderData);
    console.log('skus', skus);

    skus.forEach((sku) => {
      dispatchTypeNames.push(this.data.dispatchObj[sku.dispatchType]);
    });
    let paid_title = '';
    let wait_title = '';
    let res = Array.from(new Set(dispatchTypeNames));
    if (res.length === 2) {
      // 说明物流和自提商品都存在
      paid_title = '待发货/待提货';
      wait_title = '待收货/待提货';
    } else if (res[0] === '到店自提') {
      // 仅自提
      paid_title = '待提货';
      wait_title = '待提货';
    } else {
      // 仅配送
      paid_title = '待发货';
      wait_title = '待收货';
    }
    this.setData({
      ['stateList.PAID.title']: paid_title,
      ['stateList.WAIT_RECEIVE.title']: wait_title
    });
    console.log('配送方式：', res);
    return res.join('+');
  },

  // 获取订单详情
  initData() {
    if (this.data.type === 'UNPAY' || this.data.type === 'BALANCE_UNPAY') {
      this.getUnpayOrderDetail();
    } else {
      this.getOrderDetail();
    }
  },

  // 倒计时结束，刷新信息
  doRefresh() {
    this.setData({
      orderNo: this.data.orderData.orderNo
    });
    this.getUnpayOrderDetail();
  },

  // 未支付订单详情
  getUnpayOrderDetail() {
    Tip.loading();
    OrderApi.fetchUnpayOrderDetail({
      orderNo: this.data.orderNo
    }).then((res) => {
      Tip.loaded();
      if (res.data.flag) {
        res.data.data.shopOrders.forEach((shop) => {
          shop['dispatchName'] = this.showDispatchName(shop.details);
          if (shop.orderType === 'GIFT' || shop.orderType === 'COUPON') {
            let organizeList = app.globalData.organizeList;
            if (organizeList && organizeList.length > 0) {
              organizeList.forEach((organize) => {
                if (organize.organizeId === shop.organizeId) {
                  shop.shopName = organize.organizeName;
                  shop.shopLogo = organize.organizePicturePath;
                }
              });
            }
          }
        });
        // 计算商品总重量
        this.countProductWeight(res.data.data.orderDetail)
        const detail = res.data.data.orderDetail[0];
        // 设定商品活动类型
        this.setData({
          marketType: detail.marketType
        })
        // 如果是拼团商品，则获取拼团活动信息
        if (detail.marketType === 'GROUP') {
          this.getGroupInfo(detail.productCode)
        }
        if (detail.marketType === 'PRESALE') {
          this.getPreSaleDetai(detail, res.data);
          if (res.data.data.orderState == 'BALANCE_UNPAY' && detail.balanceEndTime) {
            this.setData({
              countDownTime: detail.balanceEndTime
            })
          } else {
            this.getCountTime(res.data.data.orderTime);
          }
        } else {
          this.getCountTime(res.data.data.orderTime);
          this.setData({
            orderData: res.data.data,
            // 商品列表长度大于3，商品列表折叠isShowAll=false;小于3时,商品列表平铺显示isShowAll=true
            isShowAll: res.data.data.orderDetail.length > 3 ? false : true
          });
          this.setData({
            needAddress: this.data.orderData.dispatchType === 'CITY_EXPRESS'
          });
        }
        // 设置会员权益对象
        this.setData({
          extraInfo: res.data.data.extraInfo
        })
      } else {
        Tip.toast(res.data.data, null, 'none');
      }
    });
  },

  // 其它订单详情
  getOrderDetail() {
    Tip.loading();
    // 查询是否已经开票
    // eleInvoice.fetchIsOpenedEinvocie({ orderNo: this.data.orderNo }).then(eleInvoiceRes => {
    //   if (eleInvoiceRes.data.flag && eleInvoiceRes.data.data) {
    //     this.setData({ showInvoiceButton: false });
    //   }
    // });
    OrderApi.fetchUnpayOrderDetail({
      orderNo: this.data.orderNo
    }).then((res) => {
      Tip.loaded();
      if (res.data.flag) {
        let temp = Object.assign({}, res.data.data);
        // temp['shopOrders'] = [res.data];
        // delete temp.details;
        // temp.shopOrders.forEach(shop => {
        //   shop['dispatchName'] = this.showDispatchName(shop.details);
        // });
        // 订单明细中存在退款中的订单,不允许开票
        res.data.data.orderDetail.forEach(item => {
          if (item.afterSaleState && item.afterSaleState === 'MERCHANT_AUDIT') {
            this.setData({ showInvoiceButton: false });
            return;
          }
        })
        // 计算商品总重量
        this.countProductWeight(temp.orderDetail)
        const detail = temp.orderDetail[0];
        // 设定商品活动类型
        this.setData({
          marketType: detail.marketType
        })
        // 如果是拼团商品，则获取拼团活动信息
        if (detail.marketType === 'GROUP') {
          this.getGroupInfo(detail.productCode)
        }
        if (detail.marketType === 'PRESALE') {
          this.getPreSaleDetai(detail, temp);
        } else {
          this.setData({
            orderData: temp
          });
          this.setData({
            needAddress: res.data.data.dispatchType === 'CITY_EXPRESS',
            orderData: temp,
            // 商品列表长度大于3，商品列表折叠isShowAll=false;小于3时,商品列表平铺显示isShowAll=true
            isShowAll: temp.orderDetail.length > 3 ? false : true
          });
        }
        // 设置会员权益对象
        this.setData({
          extraInfo: res.data.data.extraInfo
        })
      } else {
        Tip.toast(res.data.msg, null, 'none');
      }
    });
  },
  countProductWeight(productList) {
    let totalWeight = 0;
    // 筛选出有第二件活动的商品
    productList.forEach(product => {
      totalWeight += product.skuWeight * product.productCount - product.refundWeight / 1000
    })
    this.setData({
      totalWeight: totalWeight.toFixed(2)
    })
  },
  // 获取拼团活动信息
  async getGroupInfo(productCode) {
    const groupRes = await GroupApi.getGroupByProductCode({ productCode: productCode });
    if (groupRes.flag && groupRes.data.groupActivityVO) {
      this.setData({
        groupActivity: groupRes.data.groupActivityVO
      })
    }
  },
  // 获取预售信息
  async getPreSaleDetai(detail, data) {
    const res = await PreSaleApi.fetchQueryPreSale({ productVO: { marketCode: detail.marketCode } });
    if (res.flag) {
      this.setData({
        marketType: detail.marketType,
        preSaleData: res.data.data,
        deposit: detail.deposit,
        balance: detail.balance,
        balanceEndTime: detail.balanceEndTime,
        balanceStartTime: detail.balanceStartTime,
        currentTime: new Date().getTime(),
        needAddress: data.dispatchType === 'CITY_EXPRESS',
        // 商品列表长度大于3，商品列表折叠isShowAll=false;小于3时,商品列表平铺显示isShowAll=true
        isShowAll: data.orderDetail.length > 3 ? false : true
      })
    }
    this.setData({
      orderData: data
    });
  },

  // 获取支付参数
  pullWechatPay() {
    Tip.loading();
    WxPay.getNewWxPayConfig(this.data.orderData.orderNo)
      .then((res) => {
        Tip.loaded();
        console.log('支付结果：', res);
        Tip.toast('支付完成', null);
        this.goBack();
      })
      .catch((err) => {
        Tip.loaded();
        if (typeof err === 'string') {
          Tip.toast(err, null, 'none');
        }
        console.log('支付失败：', err);
      });
  },

  // 确认收货
  confirmReceive() {
    wx.showModal({
      title: '确认收货',
      content: '是否确认收到货物？',
      confirmText: '确认收货',
      confirmColor: '#FB5B41',
      success: (res) => {
        if (res.confirm) {
          if (this.isSuperMarket()) {
            this.MarketConfirmReceive();
          } else {
            Tip.loading();
            OrderApi.fetchConfirmReceive({
              shopOrderNo: this.data.orderData.orderNo
            }).then((res) => {
              Tip.loaded();
              console.log('确认收货结果：', res);
              if (res.data.flag) {
                this.initData();
              } else {
                Tip.toast(res.data.msg, null, 'none');
              }
            });
          }
        }
      }
    });
  },
  // 超市确认收货
  MarketConfirmReceive() {
    Tip.loading();
    OrderApi.fetchMarketConfirmReceive({
      orderNo: this.data.orderData.orderNo
    }).then((res) => {
      Tip.loaded();
      console.log(res);
      if (res.data) {
        wx.navigateBack({
          delta: 1
        });
      }
    });
  },

  isSuperMarket() {
    return this.data.orderData.organizeType === 'SuperMarket';
  },

  // 打开提货码
  showPicUpCode(e) {
    Tip.loading('生成中...');
    let orderNo = '';
    if (this.data.orderData) {
      if (this.data.orderData.orderNo) {
        orderNo = this.data.orderData.orderNo;
      } else if (this.data.orderData.shopOrderNo) {
        orderNo = this.data.orderData.shopOrderNo;
      }
    }
    const codeImage = QR.createQrCodeImg(String(orderNo) + 0);
    Tip.loaded();
    this.setData({
      showPickUpCode: true,
      codeStr: String(orderNo) + 0,
      codeImage
    });
  },
  // 关闭提货码
  closePickUpCode() {
    this.setData({
      showPickUpCode: false,
      currentSku: null,
      codeImage: null
    });
    this.initData();
  },
  // 查看更多按钮
  showMore(e) {
    let flag = this.data.isShowAll;
    this.data.isShowAll = !flag;
    this.setData({ isShowAll: this.data.isShowAll });
  },
  // 打开运费弹窗
  openDeliverDialog() {
    if (!this.data.orderData.totalDeliveryPrice) {
      return false
    }
    this.setData({
      isShowDeliverDialog: true
    })
  },
  async onLoad(option) {
    let flag = option.flag
    this.setData({
      type: option.type,
      orderNo: option.orderNo,
      flag
    });
    this.initData();
  }
});