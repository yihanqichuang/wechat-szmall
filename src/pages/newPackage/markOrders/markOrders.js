// 获取全局应用程序实例对象
const app = getApp();
import OrderApi from '../../../api/order';
import MarketApi from '../../../api/markets';

// 创建页面实例对象
Page({
  /**
   * 页面的初始数据
   */
  data: {
    tabs: [{
        name: '全部',
        type: 'ALL'
      },
      {
        name: '待付款',
        type: 'UNPAYBALANCE_UNPAY'
      },
      {
        name: '待成团',
        type: 'WAIT_GROUP'
      },
      {
        name: '待收货',
        type: 'WAIT_RECEIVE'
      },
      {
        name: '已完成',
        type: 'FINISH'
      },
      {
        name: '售后',
        type: 'AFTER_SALE'
      }
    ],
    activeIndex: 0,
    loading: false,
    isLoadOver: false,
    pageNo: 1,
    pageSize: 10,
    currentOrderState: 'ALL',
    unPayList: null,
    orderList: [],
    organizeType: 'SuperMarket'
  },

  // 初始化机构列表，待支付订单需要显示机构名和logo
  async getAllOrganizeList(callback) {
    if (!app.globalData.organizeList) {
      const res = await MarketApi.fetchOrganizeList()
      if (res.data.flag) {
        let list = [];
        res.data.data.forEach(city => {
          if (city.organizeList && city.organizeList.length > 0) {
            list.push(...city.organizeList);
          }
        });
        app.globalData.organizeList = list;
      }
    }
    callback();
  },

  // tab切换
  changeIndex(e) {
    console.log('当前选项：', e);
    const type = this.data.tabs[e.detail].type;
    this.setData({
      currentOrderState: type
    });
    this.refresh();
  },

  // 刷新
  async refresh() {
    this.setData({
      pageNo: 1,
      orderList: []
    });
    if (this.data.currentOrderState === 'AFTER_SALE') {
      this.getRefundList();
    } else {
      this.getUnPayOrders();
    }
  },

  // 加载更多
  loadMore() {
    if (this.data.isLoadOver) return
    this.setData({
      pageNo: ++this.data.pageNo
    });
    if (this.data.currentOrderState === 'AFTER_SALE') {
      this.getRefundList();
    } else {
      this.getUnPayOrders();
    }
  },

  // 加载待支付的大订单
  getUnPayOrders(pageSize = 10) {
    const params = {
      page: {
        pageNo: this.data.pageNo,
        pageSize: this.data.pageSize
      },
      orderInfo: {
        orderState: this.data.currentOrderState,
        organizeType: this.data.organizeType
      }
    };
    return new Promise((resolve, reject) => {
      this.setData({
        loading: true
      });
      OrderApi.fetchUnpayOrderList(params).then(res => {
        console.log('待支付大订单列表：', res);
        this.setData({
          loading: false
        });
        if (res.flag && res.data && res.data.length) {
          this.setData({
            isLoadOver: !res.page || (res.page.pages && res.page.pages <= this.data.pageNo),
            [`orderList[${this.data.orderList.length}]`]: res.data
          });
        } else {
          this.setData({
            isLoadOver: true
          });
        }
        resolve();
      })
    })
  },

  // 获取售后订单列表
  getRefundList() {
    let params = {
      page: {
        pageNo: this.data.pageNo,
        pageSize: this.data.pageSize
      },
      refundVo: {
        organizeType: this.data.organizeType
      }
    };
    this.setData({
      loading: true
    });
    OrderApi.fetchRefundList(params)
      .then(res => {
        this.setData({
          loading: false
        });
        console.log('售后订单列表：', res);
        if (res.flag && res.data && res.data.length) {
          this.setData({
            isLoadOver: !res.page || (res.page.pages && res.page.pages <= this.data.pageNo),
            [`orderList[${this.data.orderList.length}]`]: res.data
          });
        } else {
          this.setData({
            isLoadOver: true
          });
        }
      })
  },

  // 前往申请退款
  applyRefund(e) {
    const sku = e.currentTarget.dataset.sku;
    wx.navigateTo({
      url: '/pages/newPackage/orderRefund/orderRefund',
      success: (res) => {
        res.eventChannel.emit('passSku', {
          sku: Object.assign({}, sku, {
            dispatchType: sku.dispatchType || sku.orderDetailInfoList[0].dispatchType, // 添加配送方式
            organizeType: 'SuperMarket'
          })
        });
      }
    })
  },

  onLoad() {
    this.getAllOrganizeList(() => {
      this.refresh();
    });
  },

  onReachBottom() {
    this.loadMore();
  }

});