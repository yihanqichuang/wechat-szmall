// 获取全局应用程序实例对象
// const app = getApp()
import OrderApi from '../../../api/order'
import Tip from '../../../utils/tip'
// 创建页面实例对象
Page({
  data: {
    currentOrderNo: null,
    orderData: null,
    organizeType: '' // 订单组织类型
  },

  onLoad(option) {
    this.setData({
      currentOrderNo: option.orderNo,
      organizeType: option.organizeType
    });
  },

  onShow() {
    if ('SuperMarket' === this.data.organizeType) {
      this.getMaxOrderDetail()
    } else {
      this.getOrderDetail();
    }
  },

  // 此方法为了满足售后订单详情内删除后的刷新操作
  refresh() {
    this.getOrderDetail();
  },

  // 获取大订单详情
  getMaxOrderDetail() {
    Tip.loading();
    OrderApi.fetchUnpayOrderDetail({
      orderNo: this.data.currentOrderNo
    }).then(res => {
      Tip.loaded();
      if (res.data.flag) {
        let data = res.data.data.shopOrders[0]
        data.details = res.data.data.orderDetail.map(good => Object.assign(good, {
          dispatchType: res.data.data.dispatchType
        })) // 超市订单的配送方式由订单层到商品层
        this.setData({
          orderData: data
        });
      } else {
        Tip.toast(res.data.msg, null, 'none');
      }
    })
  },

  // 其它订单详情
  getOrderDetail() {
    Tip.loading();
    OrderApi.fetchOrderDetail({
      shopOrderNo: this.data.currentOrderNo
    }).then(res => {
      Tip.loaded();
      if (res.data.flag) {
        this.setData({
          orderData: res.data.data
        });
      } else {
        Tip.toast(res.data, msg, null, 'none');
      }
    })
  },

  // 查看售后详情
  checkDetail(e) {
    const sku = e.currentTarget.dataset.sku;
    if (sku.afterSaleState) {
      wx.navigateTo({
        url: `/pages/newPackage/orderRefundDetail/orderRefundDetail?shopOrderNo=${sku.shopOrderNo}&productSkuCode=${sku.productSkuCode}&packageProductCode=${sku.packageProductCode ? sku.packageProductCode : ''}`
      })
    }
  },

  // 前往申请退款
  applyRefund(e) {
    const sku = e.currentTarget.dataset.sku;
    const orderState = e.currentTarget.dataset.orderState;
    wx.navigateTo({
      url: `/pages/newPackage/orderRefund/orderRefund?packageProductCode=${sku.packageProductCode ? sku.packageProductCode : ''}`,
      success: (res) => {
        res.eventChannel.emit('passSku', {
          sku: Object.assign({}, sku, {
            organizeType: this.data.orderData.organizeType
          }),
          orderState
        });
      }
    })
  }

});