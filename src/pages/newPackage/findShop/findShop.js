// 获取全局应用程序实例对象
const app = getApp()

import api from '../../../api/findshop.js'

// 创建页面实例对象
Page({
  /**
   * 页面的初始数据
   */
  data: {
    title: 'findShop',
    searchParams: {
      shopName: '',
      industryId: '',
      floorId: ''
    },
    shopExtList: null, // 店铺列表 shopExtList
    industryList: [], // 业态
    industryName: '全部分类',
    floorList: [], // 楼层
    floorName: '全部楼层',
    selectType: '', // 激活状态的下拉
    messageComponent: '',
    loaded: false,
    letters: ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', '#'],  // 所有字母索引
    trueLetters: [], //
    activeLetter: '',
    scrollIntoView: '',
    hasMap: false,
    defaultPic: '' // 默认图片
  },
  onChange() {
    // console.log(event.detail, 'click right menu callback data')
  },
  searchInput(e) {
    this.setData({
      'searchParams.shopName': e.detail.value
    })
  },
  
  // 查询列表
  fetchShopList(params = this.data.searchParams, is1st) {
    this.setData({
      loaded: false,
      shopExtList: []
    })
    api.fetchShopList(params).then(res => {
      let data = res.data
      if (!data.flag) return this.messageComponent.applyActive(data.msg || '获取店铺列表数据失败')
      data.shopExtList = data.shopExtList || []
      this.setData({
        loaded: true,
        shopExtList: [...data.shopExtList]
      })
      if (is1st) {
        this.setData({
          industryList: [{ industryId: '', industryName: '全部分类' }, ...data.industryList],
          floorList: [{ floorId: '', floorName: '全部楼层' }, ...data.floorList]
        })
      }
      let trueLetters = this.data.shopExtList.length ? this.data.shopExtList.map(item => item.firstLetter) : []
      let activeLetter = this.data.shopExtList.length ? this.data.shopExtList[0].firstLetter : ''
      this.setData({
        trueLetters,
        activeLetter
      })
    })
  },
  selectByName() {
    this.fetchShopList(undefined)
  },
  // 切换下拉
  changeSelect(e) {
    let dataset = e.currentTarget.dataset
    let type = dataset.type
    this.setData({
      selectType: this.data.selectType === type ? '' : type
    })
  },
  changeSelectItem(e) {
    let dataset = e.currentTarget.dataset
    let selectId = dataset.id
    let selectName = dataset.name
    let selectType = dataset.type
    let propId = selectType + 'Id'
    let propName = selectType + 'Name'
    if (this.data.selectType === selectType && this.data.searchParams[propId] === selectId) {
      this.setData({
        selectType: ''
      })
      return false
    }
    this.setData({
      [`searchParams.${propId}`]: selectId,
      [propName]: selectName
    })
    this.fetchShopList(undefined)
    this.setData({
      selectType: ''
    })
  },
  toDetailPage(e) {
    let shopId = e.currentTarget.dataset.id
    wx.navigateTo({
      url: `/pages/newPackage/findShopDetailNew/findShopDetail?shopId=${shopId}`
    })
  },
  
  // 点击右侧 字母索引
  tapIndex(e) {
    let dataset = e.currentTarget.dataset
    let idx = dataset.idx
    console.log(this.findLetter(idx, dataset.letter))
    let letter = this.findLetter(idx, dataset.letter)
    this.setData({
      activeLetter: letter,
      scrollIntoView: letter
    })
  },
  //
  findLetter(idx, letter) {
    if (!this.data.trueLetters.length) return letter
    if (letter in this.data.trueLetters) return letter
    let arr = this.data.trueLetters.map(item => {
      return Math.abs(this.data.letters.indexOf(item))
    }).sort((a, b) => Math.abs(a - idx) - Math.abs(b - idx))
    console.log(idx)
    return this.data.letters[arr[0]] === '#' ? 'other' : this.data.letters[arr[0]]
  },
  // 列表滚动
  scroll(e) {
    let { scrollTop } = e.detail
    console.log(scrollTop)
    if (scrollTop < 50) {
      this.setData({
        activeLetter: this.data.shopExtList[0].firstLetter
      })
    } else {
      this.getAllRects().then(res => {
        console.log(res)
        let length = res.length
        for (let i = 0; i < length; i++) {
          if (res[i].top - 117 - 110 > 0) {
            console.log(res[i].id)
            this.setData({
              activeLetter: res[i - 1].id
            })
            break
          }
        }
      }).catch(() => {})
    }
  },
  getAllRects: function() {
    return new Promise((resolve, reject) => {
      wx.createSelectorQuery().selectAll('.letter').boundingClientRect(function(rects) {
        /* rects.forEach(function(rect) {
         rect.id      // 节点的ID
         rect.dataset // 节点的dataset
         rect.left    // 节点的左边界坐标
         rect.right   // 节点的右边界坐标
         rect.top     // 节点的上边界坐标
         rect.bottom  // 节点的下边界坐标
         rect.width   // 节点的宽度
         rect.height  // 节点的高度
         }) */
        let ret = rects.map(rect => {
          return {
            id: rect.id,
            top: rect.top
          }
        })
        ret.length ? resolve(ret) : reject()
      }).exec()
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(option) {
    this.messageComponent = this.selectComponent('.message-tips');
    if (option && option.hasMap) {
      this.setData({
        hasMap: option.hasMap
      })
    }
    this.fetchShopList(undefined, true);
    this.setData({
      defaultPic: app.globalData.defaultConfig.defaultPic
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {
    // TODO: onReady
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {
    // TODO: onShow
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {
    // TODO: onHide
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {
    // TODO: onUnload
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {
    // TODO: onPullDownRefresh
  },

  /**
   * 页面相关分享
   */
  onShareAppMessage() {
    
  }
})
