// 获取全局应用程序实例对象
const app = getApp();
import appointmentApi from '../../../api/appointment';
import Tip from '../../../utils/tip'

// 创建页面实例对象
Page({
  /**
   * 页面的初始数据
   */
  data: {
    tabs: [{
      name: '全部',
      type: ''
    },
    {
      name: '待预约',
      type: 'PENDING'
    },
    {
      name: '待确认',
      type: 'CONFIRMING'
    },
    {
      name: '预约成功',
      type: 'SUCCESS'
    },
    {
      name: '已完成',
      type: 'FINISH'
    },
    {
      name: '已取消',
      type: 'CANCEL'
    }
    ],
    activeIndex: 0,
    loading: false,
    isLoadOver: false,
    pageNum: 1,
    pageSize: 10,
    currentOrderState: '',
    unPayList: null,
    orderList: [],
    organizeType: 'GeneralMerchandise'
  },

  // 跳转至预约详情
  goDetail(e) {
    let item = e.currentTarget.dataset.item;
    wx.setStorageSync('appointmentOrder', item);
    if ('PENDING' !== item.appointmentState) {
      wx.navigateTo({
        url: '/pages/newPackage/appointmentDetail/appointmentDetail'
      })
    }
  },

  // 跳转确认预约页面
  confirmAppointment(e) {
    let item = e.currentTarget.dataset.item;
    wx.setStorageSync('appointmentOrder', item);
    wx.navigateTo({
      url: '/pages/newPackage/confirmAppointment/confirmAppointment'
    })
  },

  // 取消订单
  cancelAppointment(e) {
    let that = this;
    wx.showModal({
      title: '提示',
      content: '确定要取消订单吗？',
      success: function (sm) {
        if (sm.confirm) {
          let shopOrderNo = e.currentTarget.dataset.shoporderno;
          appointmentApi.cancelAppointmentOrder({ shopOrderNo }).then(res => {
            if (res.data.flag) {
              that.refresh();
              Tip.toast('取消订单成功');
            }
          })
        } else if (sm.cancel) {
          console.log('用户点击取消')
        }
      }
    })
  },

  // 完成订单
  finishAppointment(e) {
    let that = this;
    wx.showModal({
      title: '提示',
      content: '确定要完成订单吗？',
      success: function (sm) {
        if (sm.confirm) {
          let shopOrderNo = e.currentTarget.dataset.shoporderno;
          appointmentApi.finishAppointmentOrder({ shopOrderNo }).then(res => {
            if (res.data.flag) {
              that.refresh();
              Tip.toast('完成订单成功');
            }
          })
        } else if (sm.cancel) {
          console.log('用户点击取消')
        }
      }
    })
  },

  // tab切换
  changeIndex(e) {
    console.log('当前选项：', e);
    const type = this.data.tabs[e.detail].type;
    this.setData({
      currentOrderState: type
    });
    this.refresh();
  },

  // 刷新
  async refresh() {
    this.setData({
      pageNum: 1,
      orderList: []
    });
    if (this.data.currentOrderState === 'PENDING') {
      this.getOrderList();
    } else {
      this.getOrderList();
    }
  },

  // 加载更多
  loadMore() {
    if (this.data.isLoadOver) return
    this.setData({
      pageNum: ++this.data.pageNum
    });
    this.getOrderList();
  },

  // 获取订单列表
  getOrderList() {
    let params = {
      pageNum: this.data.pageNum,
      pageSize: this.data.pageSize,
      appointmentState: this.data.currentOrderState
    };
    this.setData({
      loading: true
    });
    appointmentApi.getAppointmentList(params)
      .then(res => {
        this.setData({
          loading: false
        });
        if (res.data.flag && res.data.data && res.data.data.length) {
          this.setData({
            isLoadOver: !res.data.page || (res.data.page.pages && res.data.page.pages <= this.data.pageNum),
            orderList: this.data.orderList.concat(res.data.data)
          });
        } else {
          this.setData({
            isLoadOver: true
          });
        }
      })
  },

  onLoad() { },

  onShow() {
    this.refresh();
  },

  onReachBottom() {
    this.loadMore();
  }

});