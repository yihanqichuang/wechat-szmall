// 获取全局应用程序实例对象
// const app = getApp()
import AddressApi from '../../../api/addressApi'
import Tip from '../../../utils/tip'
import { PHONE } from '../../../utils/regular'

// 创建页面实例对象
Page({
    /**
     * 页面的初始数据
     */
    data: {
        operateType: 'add',
        formData: {
            addressId: null,
            name: '',
            phone: '',
            address: '',
            region: [],
            regionCodes: [],
            isDefault: true
        },
        address: {
            areaCode: 0,
        },
        canSave: true,
        deleteDialog: false,
        openSelectRegion: false,
        selectRegionList: [{
            code: 0,
            name: '省份'
        },
        {
            code: 0,
            name: '城市'
        },
        {
            code: 0,
            name: '区县'
        }
        ],
        regionType: 1,
        regionList: [],
        selectRegionDone: false
    },

    chooseRegion() {
        let that = this;
        this.setData({
            openSelectRegion: !this.data.openSelectRegion
        });

        //设置区域选择数据
        let address = this.data.address;
        if (address.areaCode > 0) {
            // let selectRegionList = this.data.selectRegionList;
            // selectRegionList[0].code = address.areaCode.slice(0, 2) + '0000';
            // selectRegionList[0].name = address.province;

            // selectRegionList[1].code = address.areaCode.slice(0, 4) + '00';
            // selectRegionList[1].name = address.city;

            // selectRegionList[2].code = address.areaCode;
            // selectRegionList[2].name = address.county;

            let regionList = this.getList('county', address.areaCode.slice(0, 4));
            regionList = regionList.map(item => {
                //标记已选择的
                if (address.areaCode === item.code) {
                    item.selected = true;
                } else {
                    item.selected = false;
                }
                return item;
            })

            this.setData({
                // selectRegionList: selectRegionList,
                regionType: 3,
                regionList: regionList
            });

        } else {
            let selectRegionList = [{
                code: 0,
                name: '省份',
            },
            {
                code: 0,
                name: '城市',
            },
            {
                code: 0,
                name: '区县',
            }
            ];

            this.setData({
                selectRegionList: selectRegionList,
                regionType: 1,
                regionList: this.getList('province')
            });
        }

        this.setRegionDoneStatus();

    },
    getConfig(type) {
        return (this.data.areaList && this.data.areaList[`${type}_list`]) || {};
    },
    getList(type, code) {
        let result = [];
        if (type !== 'province' && !code) {
            return result;
        }

        const list = this.getConfig(type);
        result = Object.keys(list).map(code => ({
            code,
            name: list[code]
        }));

        if (code) {
            // oversea code
            if (code[0] === '9' && type === 'city') {
                code = '9';
            }

            result = result.filter(item => item.code.indexOf(code) === 0);
        }

        return result;
    },
    selectRegion(event) {
        let that = this;
        let regionIndex = event.target.dataset.regionIndex;
        let regionItem = this.data.regionList[regionIndex];
        let regionType = this.data.regionType;
        let selectRegionList = this.data.selectRegionList;
        selectRegionList[regionType - 1] = regionItem;

        if (regionType == 3) {
            this.setData({
                selectRegionList: selectRegionList
            })

            let regionList = that.data.regionList.map(item => {
                //标记已选择的
                if (that.data.selectRegionList[that.data.regionType - 1].code == item.code) {
                    item.selected = true;
                } else {
                    item.selected = false;
                }
                return item;
            })

            this.setData({
                regionList: regionList
            })

            this.setRegionDoneStatus();
            return
        }

        //重置下级区域为空
        selectRegionList.map((item, index) => {
            if (index > regionType - 1) {
                item.code = 0;
                item.name = index == 1 ? '城市' : '区县';
            }
            return item;
        });

        this.setData({
            selectRegionList: selectRegionList,
            regionType: regionType + 1
        })

        let code = regionItem.code;
        let regionList = [];
        if (regionType === 1) {
            // 点击省级，取市级
            regionList = this.getList('city', code.slice(0, 2))
        }
        else {
            // 点击市级，取县级
            regionList = this.getList('county', code.slice(0, 4))
        }

        this.setData({
            regionList: regionList
        })

        this.setRegionDoneStatus();
    },
    setRegionDoneStatus() {
        let that = this;
        let doneStatus = that.data.selectRegionList.every(item => {
            return item.code != 0;
        });

        that.setData({
            selectRegionDone: doneStatus
        })

    },
    selectRegionType(event) {
        let that = this;
        let regionTypeIndex = event.target.dataset.regionTypeIndex;
        let selectRegionList = that.data.selectRegionList;

        //判断是否可点击
        if (regionTypeIndex + 1 == this.data.regionType || (regionTypeIndex - 1 >= 0 && selectRegionList[regionTypeIndex - 1].code <= 0)) {
            return false;
        }

        let selectRegionItem = selectRegionList[regionTypeIndex];
        let code = selectRegionItem.code;
        let regionList;
        if (regionTypeIndex === 0) {
            // 点击省级，取省级
            regionList = this.getList('province');
        }
        else if (regionTypeIndex === 1) {
            // 点击市级，取市级
            regionList = this.getList('city', code.slice(0, 2));
        }
        else {
            // 点击县级，取县级
            regionList = this.getList('county', code.slice(0, 4));
        }

        regionList = regionList.map(item => {
            //标记已选择的
            if (that.data.selectRegionList[regionTypeIndex].code == item.code) {
                item.selected = true;
            } else {
                item.selected = false;
            }
            return item;
        })

        this.setData({
            regionList: regionList,
            regionType: regionTypeIndex + 1
        })

        this.setRegionDoneStatus();
    },
    doneSelectRegion() {
        if (this.data.selectRegionDone === false) {
            return false;
        }
        let address = this.data.address;
        let selectRegionList = this.data.selectRegionList;
        // address.province = selectRegionList[0].name;
        // address.city = selectRegionList[1].name;
        // address.county = selectRegionList[2].name;
        address.areaCode = selectRegionList[2].code;
        var region = [];
        var regionCodes = [];
        for (let item of this.data.selectRegionList) {
            region.push(item.name);
            regionCodes.push(item.code);
        }

        this.setData({
            address: address,
            openSelectRegion: false,
            ['formData.region']: region,
            ['formData.regionCodes']: regionCodes
        });

    },
    cancelSelectRegion() {
        this.setData({
            openSelectRegion: false,
            regionType: this.data.regionDoneStatus ? 3 : 1
        });

    },
    // 验证
    validate() {
        if (!this.data.formData.name) {
            Tip.toast('请输入收货人姓名！', null, 'none');
            return false
        }

        if (!this.data.formData.phone) {
            Tip.toast('请输入收货人电话！', null, 'none');
            return false
        } else {
            if (!this.data.canSave) {
                Tip.toast('手机号格式错误！', null, 'none');
                return false
            }
        }

        if (!this.data.formData.region) {
            Tip.toast('请选择收货人地区！', null, 'none');
            return false
        }

        if (!this.data.formData.address) {
            Tip.toast('请输入详细地址！', null, 'none');
            return false
        }

        return true;
    },

    // 判断手机号格式方法
    bindblur(e) {
        let phoneVal = e.detail.value.trim()
        let flag = true;
        if (!PHONE.test(phoneVal)) {
            flag = false;
            Tip.toast('手机号格式错误！', null, 'none');
        }
        this.setData({ canSave: flag })
    },

    // 提交form
    formSubmit(e) {
        if (!this.validate()) return
        let params = e.detail.value;
        params.isDefault = this.data.formData.isDefault ? 'Y' : 'N';
        params.province = this.data.formData.region[0];
        params.city = this.data.formData.region[1];
        params.county = this.data.formData.region[2];
        params.provinceCode = this.data.formData.regionCodes[0];
        params.cityCode = this.data.formData.regionCodes[1];
        params.countyCode = this.data.formData.regionCodes[2];
        if (this.data.operateType === 'edit') {
            params.id = this.data.formData.addressId;
            this.updateAddress(params)
        } else {
            this.addAddress(params)
        }
    },

    // 新增地址抬头
    addAddress(params) {
        AddressApi.fetchSaveAddress(params).then(res => {
            if (res.data.flag) {
                Tip.toast('已更新', null, 'none');
                wx.navigateBack({ delta: 1 })
            }
        })
    },
    // 编辑地址
    updateAddress(params) {
        AddressApi.fetchUpdateAddress(params).then(res => {
            if (res.data.flag) {
                Tip.toast('已更新', null, 'none');
                wx.navigateBack({ delta: 1 })
            }
        })
    },
    handleSwitch(e) {
        this.setData({
            ['formData.isDefault']: e.detail.value
        });
    },
    handleInputVal(e) {
        let type = e.currentTarget.dataset.type
        let item = `formData.${type}`
        this.setData({
            [item]: e.detail.value
        })
    },

    // 地区选择器方法
    bindRegionChange(e) {
        console.log('已选择的地址：', e);
        this.setData({
            ['formData.region']: e.detail.value,
            ['formData.regionCodes']: e.detail.code
        })
    },

    deleteHead() {
        this.setData({
            deleteDialog: true
        })
    },
    cancelDelete() {
        this.setData({
            deleteDialog: false
        })
    },
    // 删除地址
    confirmDelete() {
        AddressApi.fetchDeleteAddress({ id: this.data.formData.addressId }).then(res => {
            console.log('删除地址：', res);
            if (res.data.flag) {
                wx.navigateBack({ delta: 1 });
            }
        })
    },
    // 获取省市区列表
    getAreaList() {
        AddressApi.getAreaList().then(res => {
            if (res.statusCode === 200) {
                if (res.data.data) {
                    this.setData({
                        areaList: res.data.data
                    })
                }
            }
        })
    },

    /**
     * 生命周期函数--监听页面加载
     */

    onLoad(option) {
        this.setData({ operateType: option.type });
        this.getAreaList();
        if (this.data.operateType === 'add') {
            wx.setNavigationBarTitle({ title: '地址新增' });
        }
        if (this.data.operateType === 'edit') {
            wx.setNavigationBarTitle({ title: '地址编辑' });
            let currentAddress = wx.getStorageSync('currentAddress');
            this.data.selectRegionList[0].code = currentAddress.provinceCode
            this.data.selectRegionList[0].name = currentAddress.province
            this.data.selectRegionList[1].code = currentAddress.cityCode
            this.data.selectRegionList[1].name = currentAddress.city
            this.data.selectRegionList[2].code = currentAddress.countyCode
            this.data.selectRegionList[2].name = currentAddress.county
            this.data.address.areaCode = currentAddress.countyCode
            this.setData({
                formData: {
                    addressId: currentAddress.id,
                    name: currentAddress.name,
                    phone: currentAddress.phone,
                    address: currentAddress.address,
                    region: [currentAddress.province, currentAddress.city, currentAddress.county],
                    regionCodes: [currentAddress.provinceCode, currentAddress.cityCode, currentAddress.countyCode],
                    isDefault: currentAddress.isDefault === 'Y'
                },
                selectRegionList: this.data.selectRegionList,
                address: this.data.address
            })
        }
    },

    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady() {
    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow() {
    },

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide() {
    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload() {
    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh() {
    }
})
