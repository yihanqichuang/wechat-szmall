// 获取全局应用程序实例对象
// const app = getApp()
const points = require('../../../api/points.js');
import Tip from '../../../utils/tip'

// 创建页面实例对象
Page({
  /**
   * 页面的初始数据
   */
  data: {
    tabs: [{
        name: '默认排序',
        type: 'DEFAULT'
      },
      {
        name: '积分最高',
        type: 'DESC'
      },
      {
        name: '积分最低',
        type: 'ASC'
      }],
    giftList: [],  // 列表数据
    loading: false,
    isLoadOver: false,
    pageNo: 1,
    pageSize: 10,
    emptyType: 'pointsList',
    sortType: 'DEFAULT' // 排序   DEFAULT-默认 ASC-从低到高 DESC-从高到低
  },
  // 积分兑换排序
  sort(event) {
    let sortType = event.currentTarget.dataset.type;
    if (sortType === this.data.sortType) return;
    this.setData({ sortType });
    this.doRefresh();
  },

  // 获取 礼品列表
  getPointsList() {
    let params = {
      pointsRewardType: 'GIFT',
      sortType: this.data.sortType,
      pageNum: this.data.pageNo,
      pageSize: this.data.pageSize
    };
    this.setData({ loading: true });
    points.fetchPointsList(params).then(res => {
      console.log('礼品列表：', res);
      this.setData({ loading: false });
      if (res.data.flag && res.data.data && res.data.data.length) {
        this.setData({
          isLoadOver: !res.data.page || (res.data.page.pages && res.data.page.pages <= this.data.pageNo),
          [`giftList[${this.data.pageNo - 1}]`]: res.data.data
        });
      } else {
        this.setData({ isLoadOver: true });
      }
    })
  },

  // 刷新
  doRefresh() {
    this.setData({ pageNo: 1, giftList: [] });
    this.getPointsList();
  },

  // 加载更多
  loadMore() {
    if (this.data.isLoadOver) return
    this.setData({ pageNo: ++this.data.pageNo });
    this.getPointsList();
  },

  onLoad() {
    this.doRefresh();
  },

  onReachBottom() {
    this.loadMore();
  },
  /**
   * 页面相关分享
   */
  onShareAppMessage() {
    const organizeId = wx.getStorageSync('organizeId');
    const organizeName = wx.getStorageSync('organizeName');
    return {
      path: `/pages/newPackage/pointsListNew/pointsList?organizeId=${organizeId}&organizeName=${organizeName}`
    }
  }
});
