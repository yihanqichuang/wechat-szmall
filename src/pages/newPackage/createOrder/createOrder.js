// 获取全局应用程序实例对象
// const app = getApp()
import AddressApi from '../../../api/addressApi'
import ProductApi from '../../../api/commodity'
import WxPay from '../../../utils/wxPay'
import Pages from '../../../utils/getCurrentPageUrl'
import Tip from '../../../utils/tip'
import PreSaleApi from '../../../api/preSale';
import TimeFormat from '../../../utils/timeFormat.js';

// 创建页面实例对象
Page({
  /**
   * 页面的初始数据
   */
  data: {
    isGroup: 'N', // 该商品是否拼团标志
    groupInfoCode: '', // 团code
    deposit: 0, // 定金
    balance: 0, // 尾款
    skuOrganizeId: null,
    showCenterDialog: false,
    checkPriceChange: true,
    isGiftsShow: false,
    selectedAddress: null,
    selectedCoupon: null,
    createOrderSkus: null,
    orderData: null, // 预下单渲染数据
    marketCode: null,
    preSaleData: null, // 预售信息
    marketType: null,
    cardPay: false,
    wechatPay: true,
    needAddress: false,
    dispatchObj: {
      PICK_UP: '自提',
      EXPRESS: '快递',
      CITY_EXPRESS: '同城配送'
    },
    orderNo: null,
    failSkuList: null,
    repeatCount: 0,
    valueCard: null,
    prepaidPayPrice: 0,
    prepaidPwdToken: null,
    secondMarketUseCoupon: true, // 第二件活动情况下可否使用优惠券判断flag
    totalWeight: 0, // 商品重量
    canPay: true
  },

  // 打开会员权益弹窗
  openMemberRightsDialog(e) {
    let rightsType = e.currentTarget.dataset.type;
    this.selectComponent('#memberRightsDialog').showDialog(rightsType, this.data.orderData.orderInfo.extraInfo);
  },

  // 打开优惠券选择
  openCouponPicker() {
    const skuList = [];
    this.data.createOrderSkus.forEach(shop => {
      if (shop.skuList && shop.skuList.length > 0) {
        shop.skuList.forEach(sku => {
          if (sku.cartProductType === 'PACKAGE') {
            sku.skuInfo.forEach(item => {
              skuList.push({ productSkuCode: item.productSkuCode, productCount: sku.skuCount });
            });
          } else {
            skuList.push({ productSkuCode: sku.skuInfo.productSkuCode, productCount: sku.skuCount });
          }
        });
      }
    });
    this.selectComponent('#couponPicker').showPicker(skuList, this.data.orderData.orderInfo.totalPrice);
  },

  // 接收选择优惠券
  savePickedCoupon(e) {
    this.setData({
      prepaidPayPrice: 0,
      prepaidPwdToken: null,
      selectedCoupon: e.detail
    });
    this.preCreateOrder();
  },

  // 显示配送方式，遍历商品下的sku列表
  // 存在自提显示自提，存在配送限时配送，都存在限时物流+配送
  showDispatchName(skus) {
    if (!skus || skus.length <= 0) return
    let dispatchTypeNames = [];
    skus.forEach(sku => {
      if (sku.dispatchType === 'EXPRESS' || sku.dispatchType === 'CITY_EXPRESS') {
        this.setData({ needAddress: true });
      }
      dispatchTypeNames.push(this.data.dispatchObj[sku.dispatchType])
    });
    let res = Array.from(new Set(dispatchTypeNames));
    return res.join('+')
  },

  // 切换赠送明细显示
  switchGiftShow() {
    this.setData({ isGiftsShow: !this.data.isGiftsShow });
  },

  // 前往选择地址
  toPickAddress() {
    const _this = this;
    wx.navigateTo({
      url: '/pages/newPackage/addressList/addressList',
      events: {
        pickedAddress: function (data) {
          _this.setData({ selectedAddress: data.selectedAddress });
          _this.preCreateOrder();
        }
      },
      success: function (res) {
        // 通过eventChannel向被打开页面传送数据
        // res.eventChannel.emit('acceptDataFromOpenerPage', { data: 'test' })
      }
    })
  },

  // 选择支付方式
  payWayChange(e) {
    const cardPay = e.detail.value.indexOf('card') > -1;
    const wechatPay = e.detail.value.indexOf('wechat') > -1;
    if (!cardPay) {
      // 取消储值卡支付，清空金额
      this.setData({
        prepaidPayPrice: 0,
        prepaidPwdToken: null
      });
    }
    this.setData({
      cardPay,
      wechatPay
    });
  },

  // 初始化订单商品数据
  initOrderInfo() {
    AddressApi.fetchAddressList({ isDefault: 'Y' }).then(addressRes => {
      if (addressRes.data.flag && addressRes.data.data) {
        this.setData({ selectedAddress: addressRes.data.data[0] });
      } else {
        Tip.toast(addressRes.data.msg, null, 'none');
      }
      let createOrderSkus = wx.getStorageSync('createOrderSkus');
      if (createOrderSkus) {
        if (createOrderSkus.origin === 'detail') {
          // 立即购买
          createOrderSkus = [{
            shopId: createOrderSkus.shopId,
            shopName: createOrderSkus.shopName,
            shopLogo: createOrderSkus.shopLogo,
            shopPhone: createOrderSkus.shopPhone,
            skuList: [{
              dispatchType: createOrderSkus.cartProductType && createOrderSkus.cartProductType == 'PACKAGE' ? createOrderSkus.dispatchType : createOrderSkus.dispatchType.value,
              skuCount: createOrderSkus.skuCount,
              skuInfo: createOrderSkus.skuInfo,
              cartProductType: createOrderSkus.cartProductType,
              marketCode: createOrderSkus.marketCode,
              packageProductCode: createOrderSkus.packageProductCode
            }]
          }];
        }
        this.setData({ createOrderSkus });
        this.preCreateOrder();
      }
    });
  },

  // 输入店铺备注
  completeRemark(e) {
    let { value } = e.detail;
    let { index } = e.currentTarget.dataset;
    this.setData({
      [`createOrderSkus[${index}].remark`]: value
    });
  },

  // 组织下单参数
  groupOrderParams() {
    let groupInfoCode = wx.getStorageSync('createOrderGroupInfoCode');
    let isGroup = wx.getStorageSync('createOrderIsGroup');
    this.setData({
      groupInfoCode: groupInfoCode ? groupInfoCode : '',
      isGroup: 'Y' === isGroup ? isGroup : 'N'
    })
    let orgType = wx.getStorageSync('organizeType');
    let dispatchType = null;
    if ('SuperMarket' === orgType) {
      // 菜场下单时，配送方式默认同城配送
      dispatchType = 'CITY_EXPRESS';
    }
    let params = {
      dispatchType: dispatchType,
      groupInfoCode: this.data.groupInfoCode,
      isGroup: this.data.isGroup,
      prepaidPayCardNo: this.data.valueCard ? this.data.valueCard.cid : '',
      prepaidPayPrice: this.data.prepaidPayPrice,
      prepaidPwdToken: this.data.prepaidPwdToken,
      checkPriceChange: this.data.checkPriceChange, // 是否需要校验价格，价格有变化继续支付的时候不要校验价格，置为false
      verifyCode: this.data.selectedCoupon ? this.data.selectedCoupon.verifyCode : null,
      memberCouponId: this.data.selectedCoupon ? this.data.selectedCoupon.memberCouponId : null,
      receiverName: this.data.selectedAddress ? this.data.selectedAddress.name : null,
      receiverPhone: this.data.selectedAddress ? this.data.selectedAddress.phone : null,
      receiverProvinceName: this.data.selectedAddress ? this.data.selectedAddress.province : null,
      receiverProvinceCode: this.data.selectedAddress ? this.data.selectedAddress.provinceCode : null,
      receiverCityName: this.data.selectedAddress ? this.data.selectedAddress.city : null,
      receiverCityNameCode: this.data.selectedAddress ? this.data.selectedAddress.cityCode : null,
      receiverDistrictName: this.data.selectedAddress ? this.data.selectedAddress.county : null,
      receiverDistrictCode: this.data.selectedAddress ? this.data.selectedAddress.countyCode : null,
      receiverAddress: this.data.selectedAddress ? this.data.selectedAddress.address : null,
      appType: 'smallApp',
      shopDetails: this.data.createOrderSkus.map(shop => ({
        shopId: shop.shopId,
        shopName: shop.shopName,
        shopLogo: shop.shopLogo,
        remark: shop.remark,
        pickupPhone: shop.shopPhone,
        details: shop.skuList.map(sku => {
          return {
            cartId: sku.cartId,
            productCode: sku.skuInfo ? sku.skuInfo.productCode : null,
            productSkuCode: sku.skuInfo ? sku.skuInfo.productSkuCode : null,
            productCount: sku.skuCount,
            dispatchType: sku.dispatchType,
            marketCode: sku.marketCode,
            cartProductType: sku.cartProductType,
            packageProductCode: sku.packageProductCode
          }
        })
      }))
    };
    return params;
  },

  // 处理库存不足或者价格失效的商品
  // 从预下单订单数据中筛选出库存不足或者价格失效的商品，并展示出来
  filterTroubleGoods(list, type) {
    this.setData({ failSkuList: list });
    this.selectComponent('#failSkus').showDialog(this.data.failSkuList, type, this.data.marketType);
  },

  // 移除无库存商品并重新预下单展示
  removeNoStockSkus() {
    let stockFailSkuCodes = this.data.failSkuList.map(sku => sku.productSkuCode);
    let oldData = this.data.createOrderSkus;
    for (let i = oldData.length - 1; i >= 0; i--) {
      if (oldData[i].skuList && oldData[i].skuList.length > 0) {
        for (let j = oldData[i].skuList.length - 1; j >= 0; j--) {
          if (stockFailSkuCodes.indexOf(oldData[i].skuList[j].skuInfo.productSkuCode) > -1) {
            oldData[i].skuList.splice(j, 1);
          }
        }
      }
      if (oldData[i].skuList.length <= 0) {
        oldData.splice(i, 1);
      }
    }

    if (oldData.length <= 0) {
      // 全部删除了，提示并返回
      wx.showModal({
        title: '订单失效',
        content: '订单内不包含任何商品！',
        showCancel: false,
        success(res) {
          if (res.confirm) {
            wx.navigateBack({ delta: 1 })
          }
        }
      })
    } else {
      // 重新拉取预下单展示
      this.setData({ checkPriceChange: false, selectedCoupon: null }); // 需要清除之前选择的优惠券
      this.preCreateOrder();
    }
  },

  // 价格变动继续支付,重新拉取预下单展示
  continuePay() {
    this.setData({
      checkPriceChange: false,
      selectedCoupon: null
    }); // 需要清除之前选择的优惠券
    this.createOrder();
  },

  // 预下单
  preCreateOrder() {
    Tip.loading();
    const orderInfo = this.groupOrderParams();
    ProductApi.fetchPreCreateOrder(orderInfo).then(res => {
      Tip.loaded();
      if (res.data.flag && res.data.data.orderInfo) {
        res.data.data.orderInfo.shopOrders.forEach(shop => {
          shop['dispatchName'] = this.showDispatchName(shop.details);
        });
        // let marketType = '';
        // for (let shopOrder of res.data.data.orderInfo.shopOrders) {
        //   if (shopOrder.preDetailsViewList && shopOrder.preDetailsViewList.length > 0) {
        //     for (let preDetail of shopOrder.preDetailsViewList) {
        //       if (preDetail.marketType === 'PACKAGE') {
        //         marketType = 'PACKAGE'
        //         break;
        //       }
        //     }
        //   }
        // }
        // // 设置活动类型
        // this.setData({
        //   marketType: marketType
        // })
        this.setData({ orderData: res.data.data, canPay: true })
      } else {
        this.setData({ canPay: false })
        if (res.data.data.failMsg) {
          Tip.toast(res.data.data.failMsg, null, 'none');
        } else {
          Tip.toast('获取订单信息异常！', null, 'none');
        }
      }
    })
  },

  // 正式下单
  createOrder() {
    this.data.repeatCount = 0;
    Tip.loading();
    const orderInfo = this.groupOrderParams();
    ProductApi.fetchCreateOrder(orderInfo).then(res => {
      if (res.data.flag) {
        this.setData({
          orderNo: res.data.data.orderNo
        });
        if (res.data.data.failType === 'STOCK_FAIL') {
          Tip.loaded();
          this.filterTroubleGoods(res.data.data.failSkuList, 'STOCK_FAIL');
        }
        if (res.data.data.failType === 'SALE_LEAST') {
          Tip.loaded();
          this.filterTroubleGoods(res.data.data.failSkuList, 'SALE_LEAST');
        }
        if (res.data.data.failType === 'PROCESS') {
          this.checkCreateOrderResRepeat();
        }
      } else {
        Tip.loaded();
        Tip.toast(res.data.msg, null, 'none');
      }
    })
  },

  // 轮询查询创建订单结果
  checkCreateOrderResRepeat() {
    if (!this.data.orderNo) return
    ProductApi.fetchCheckCreateOrderRes({ orderNo: this.data.orderNo })
      .then(res => {
        if (res.data.flag) {
          this.handleCreateOrderResult(res.data.data);
        } else {
          Tip.loaded();
          Tip.toast(res.data.msg, null, 'none');
        }
      })
  },

  // 判断是否在当前页面
  isCurrentPage() {
    const path = Pages.getCurrentPageUrl();
    return path && path === 'pages/newPackage/createOrder/createOrder';
  },

  // 处理下单结果各种状态
  handleCreateOrderResult(result) {
    switch (result.failType) {
      case 'PROCESS': // 下单中
        if (this.isCurrentPage()) {
          let timer = setTimeout(() => {
            this.checkCreateOrderResRepeat();
            clearTimeout(timer);
          }, 1000);
        }
        break;
      case 'FAIL': // 下单失败
        Tip.loaded();
        Tip.toast(result.failMsg, null, 'none');
        break;
      case 'STOCK_FAIL': // 部分商品库存不足
        Tip.loaded();
        this.filterTroubleGoods(result.failSkuList, 'STOCK_FAIL');
        break;
      case 'EXPIRE': // 部分商品价格失效
        Tip.loaded();
        this.filterTroubleGoods(result.failSkuList, 'EXPIRE');
        break;
      case 'GOODS_OFFLINE':
        Tip.loaded();
        this.filterTroubleGoods(result.failSkuList, 'GOODS_OFFLINE');
        break;
      case 'REQUEST_SUCCESS': // 下单成功
        this.pullWechatPay();
        break;
      case 'SALE_LEAST': // 部分商品不满足最低售卖
        Tip.loaded();
        this.filterTroubleGoods(result.failSkuList, 'SALE_LEAST');
        break;
      case 'SALE_LIMIT': // 部分商品不满足限购条件
        Tip.loaded();
        Tip.toast(result.failMsg, null, 'none');
        break;
    }
  },

  // 获取支付参数
  // 支付完成或者结束都强制跳往订单页面，防止订单重复支付
  pullWechatPay() {
    WxPay.getNewWxPayConfig(this.data.orderNo, this.data.prepaidPwdToken).then(res => {
      Tip.loaded();
      Tip.toast('支付完成', null);
      //跳转至订单列表
      wx.redirectTo({
        url: '/pages/newPackage/mallOrders/mallOrders'
      })
    }).catch(err => {
      Tip.loaded();
      if (typeof err === 'string') {
        Tip.toast(err, null, 'none');
      }
      wx.redirectTo({
        url: '/pages/newPackage/mallOrders/mallOrders'
      })
    })
  },

  // 提交订单
  submitValidOrder() {
    if (!this.data.canPay) return;
    if (this.data.needAddress && !this.data.selectedAddress) {
      Tip.toast('请选择收货地址', null, 'none');
      return
    }
    if (this.data.orderData && this.data.orderData.orderInfo && this.data.orderData.orderInfo.actualPayPrice > 0 && !this.data.cardPay && !this.data.wechatPay) {
      Tip.toast('请选择支付方式', null, 'none');
      return
    }
    this.createOrder();
  },
  // 获取下单未支付倒计时时间
  getCloseOrderTime() {
    ProductApi.getCloseOrderTime().then(res => {
      if (res.data.flag) {
        wx.setStorageSync("closeOrderTime", res.data.data)
      }
    })
  },
  onLoad() {
    // 初始化预下单数据
    this.initOrderInfo();
    // 获取下单未支付倒计时时间
    this.getCloseOrderTime();
  }
});
