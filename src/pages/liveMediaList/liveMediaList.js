// /* eslint-disable */
// // 获取全局应用程序实例对象
// const app = getApp()
// const livePlayer = requirePlugin('live-player-plugin')
// import liveMedia from '../../api/liveMedia.js'


// // 创建页面实例对象
// Page({
//   /**
//    * 页面的初始数据
//    */
//   data: {
//     activeIndex: '',
//     liveList: [],
//     page: {
//       pageNum: 1,
//       pageSize: 10,
//       total: 1
//     },
//     isMember: true,
//     isCustomerEnable: false,
//     liveLogo: app.globalData.defaultConfig ? app.globalData.defaultConfig.logo : ''  // 系统全局默认logo
//   },
  
//   // 首次获取立马返回直播状态，往后间隔1分钟或更慢的频率去轮询获取直播状态
//   getLiveStatusById(item) {
//     let liveStatusObj = {};
//     let liveStatus = ''
//     livePlayer.getLiveStatus({ room_id: item.roomid }).then(res => {
//       // 101: 直播中, 102: 未开始, 103: 已结束, 104: 禁播, 105: 暂停中, 106: 异常，107：已过期 
//       liveStatus = res.liveStatus
//       console.log('get live status', liveStatus)
//     }).catch(err => {
//       console.log('get live status', err)
//     })
//     item.status = liveStatus;
//     liveStatusObj['room_' + item.roomid] = item;
//     this.setData(liveStatusObj);
//   },

//   // 获取直播列表
//   getLiveList(pageNum) {
//     let params = {
//       liveSatus: this.data.activeIndex,
//       isNeedPage: true,
//       pageNum: pageNum,
//       pageSize: this.data.page.pageSize
//     };
//     liveMedia.fetchLiveList(params).then(res => {
//       if (res.data.flag) {
//         if (pageNum === 1) this.setData({ liveList: [] });
//         let statusStr = '';
//         let liveList = [...this.data.liveList, ...res.data.data];
//         liveList.forEach(item => { // 遍历一下列表中每个直播的状态
//           let goodsArr = this.switchJsonObj(item.goods);
//           item.goods = goodsArr;
//           let curTime = new Date().getTime();
//           let startTime = new Date(item.startTime.replace(/-/g, '/')).getTime();
//           let endTime = new Date(item.endTime.replace(/-/g, '/')).getTime();
//           if (curTime >= startTime && curTime <= endTime) {
//             item.status = '101';
//           } else if (curTime < startTime) {
//             item.status = '102';
//           } else if (curTime > endTime) {
//             item.status = '103';
//           }
//           // 拼装直播列表状态字符串
//           statusStr += item.id + ':' + item.status + ','
//         })
//         // 去掉末尾逗号
//         statusStr = statusStr.substring(0, statusStr.length - 1);
//         // 调用更新直播列表状态接口
//         if (statusStr != '') {
//           this.updateLiveStatus(statusStr, pageNum);
//         }

//         this.setData({ 
//           liveList: liveList,
//           page: res.data.page  
//         })
//       } else {
//         this.messageComponent.applyActive(res.data.msg || '获取banner列表异常~');
//       }
//     }).catch((err) => {
//       console.log(err)
//     })
//   },

//   // 批量更新直播列表状态
//   updateLiveStatus(statusStr, pageNum) {
//     let params = {
//       strSatus: statusStr,
//       pageNum: pageNum,
//       status: this.data.activeIndex
//     };
//     liveMedia.fetchUpdateLiveStatus(params).then(res => {
//       if (res.data.flag) {
//         console.log('更新成功')
//       } else {
//         console.log(res.data.msg)
//       }
//     }).catch((err) => {
//       console.log(err)
//     })
//   },

//   getCustomerConfig() {
//     liveMedia.getCustomerConfig().then(res => {
//       if (res.data.flag) {
//         this.setData({
//           isCustomerEnable: res.data.data
//         })
//       }
//     }).catch((err) => {
//       console.log(err)
//     })
//   },

//   // 分页
//   loadMore(e) {
//     this.getLiveList(e.detail)
//   },

//   // 顶部tab切换
//   tabSwitch(e) {
//     let curIndex = e.currentTarget.dataset.key;
//     this.setData({
//       activeIndex: curIndex
//     })
//     this.getLiveList(1);
//   },

//   // 跳转商品详情页
//   toGoodsDetail(e) {
//     let item = e.currentTarget.dataset.item;
//     wx.navigateTo({ url: '/' + item.url});
//   },

//   // 跳转直播间
//   toLiveRoom(e) {
//     let roomId = e.currentTarget.dataset.item;
//     let url = `plugin-private://wx2b03c6e691cd7370/pages/live-player-plugin?room_id=${roomId}`
//     wx.navigateTo({ url: url });
//   },

//   // 判断是否为json, 处理
//   switchJsonObj(goodObj) {
//     try {
//       if (typeof JSON.parse(goodObj) == 'object') {
//         return JSON.parse(goodObj);
//       }
//     } catch(e) {
//       return goodObj;
//     }
//   },

//   // 保存推广记录
//   savePromotionRecord(scene) {
//     let params = {
//       scene: scene
//     };
//     liveMedia.fetchSavePromotionRecord(params).then(res => {
//       if (res.data.flag) {
//         console.log(res.data.data);
//         if(res.data.data){
//           this.toAutoLiveRoom(res.data.data);
//         }
//       }
//       wx.hideLoading();
//     }).catch((err) => {
//       console.log(err)
//       wx.hideLoading();
//     })
//   },

//   // 跳转直播间
//   toAutoLiveRoom(obj) {
//     let roomId = obj.roomId;
//     let url = `plugin-private://wx2b03c6e691cd7370/pages/live-player-plugin?room_id=${roomId}`
//     wx.navigateTo({ url: url });
//   },

//   /**
//    * 生命周期函数--监听页面加载
//    */
//   onLoad(option) {
//     this.messageComponent = this.selectComponent('.messageTips');
//     if (option.scene) {
//       let organizeId = option.scene.split('_')[2];
//       wx.setStorageSync('organizeId', organizeId);
//       let memberType = wx.getStorageSync('memberType');
//       if (memberType !== 'MEMBER') {
//         wx.setStorageSync('live-scene', option.scene);
//         this.setData({
//           isMember: false
//         })
//         wx.navigateTo({
//           url: `/pages/memberSign/memberSign`
//         });
//         return
//       }
//       wx.showLoading({
//         title: '跳转直播间中...',
//       })
//       let scene = decodeURIComponent(option.scene);
//       this.savePromotionRecord(scene);
//     }
//   },

//   /**
//    * 生命周期函数--监听页面初次渲染完成
//    */
//   onReady() {
//     // TODO: onReady
//   },

//   /**
//    * 生命周期函数--监听页面显示
//    */
//   onShow() {
//     // TODO: onShow
//     let liveScene = wx.getStorageSync('live-scene');
//     this.getLiveList(1);
//     this.getCustomerConfig();
//     let memberType = wx.getStorageSync('memberType');
//     if(liveScene && !this.data.isMember && memberType == 'MEMBER'){
//       wx.showLoading({
//         title: '跳转直播间中...',
//       })
//       let scene = decodeURIComponent(liveScene);
//       this.savePromotionRecord(scene);
//       wx.removeStorageSync('live-scene');
//     }
//   },

//   /**
//    * 生命周期函数--监听页面隐藏
//    */
//   onHide() {
//     // TODO: onHide
//   },

//   /**
//    * 生命周期函数--监听页面卸载
//    */
//   onUnload() {
//     // TODO: onUnload
//   },

//   /**
//    * 页面相关事件处理函数--监听用户下拉动作
//    */
//   onPullDownRefresh() {
//     // TODO: onPullDownRefresh
//     wx.showNavigationBarLoading();
//     this.getLiveList(1);
//     this.getCustomerConfig();
//     wx.hideNavigationBarLoading() // 完成停止加载
//     wx.stopPullDownRefresh();
//   },

//   /**
//    * 页面相关分享
//    */
//   onShareAppMessage() {
    
//   }
// });
