// 获取全局应用程序实例对象
// const app = getApp()
import themeApi from '../../api/category.js'

// 创建页面实例对象
Page({
  /**
   * 页面的初始数据
   */
  data: {
    list: [],
    page: {
      pageNum: 1,
      pageSize: 10,
      total: 1
    }
  },

  // 获取banner列表
  getThemeList(pageNum) {
    let params = {
      themeId: '',
      isNeedPage: true,
      pageNum: pageNum,
      pageSize: this.data.page.pageSize
    };
    themeApi.fetchThemeList(params).then(res => {
      if (res.data.flag) {
        if (pageNum === 1) this.setData({ list: [] });
        this.setData({ 
          list: [...this.data.list, ...res.data.data],
          page: res.data.page  
        })
      } else {
        this.messageComponent.applyActive(res.data.msg || '获取banner列表异常~');
      }
    }).catch((err) => {
      console.log(err)
    })
  },

  // 分页
  loadMore(e) {
    this.getThemeList(e.detail)
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad() {
    this.messageComponent = this.selectComponent('.messageTips');
    this.getThemeList(1);
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {
    // TODO: onReady
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {
    // TODO: onShow
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {
    // TODO: onHide
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {
    // TODO: onUnload
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {
    // TODO: onPullDownRefresh
  }
});
