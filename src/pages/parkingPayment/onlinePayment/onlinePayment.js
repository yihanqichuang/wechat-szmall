// 获取全局应用程序实例对象
// const app = getApp()
import Tips from '../../../utils/tip'
import parkApi from '../../../api/parking';
import wxPay from '../../../utils/wxPay.js';
import activityApi from '../../../api/activity.js';
import benMath from '../../../utils/BenMath.js';
import formatMoney from '../../../utils/formatMoney.js';
import { PARKINGPAY } from '../../../vendor.js';

// 创建页面实例对象
Page({
  /**
   * 页面的初始数据
   */
  data: {
    isShow: false,
    pending: false,
    btnDisabled: true,
    title: 'onlinePayment',
    messageComponent: '',
    carNumber: '',
    info: {},
    showMore: false,
    limitLen: 2,
    list1: [],
    // 一小时停车券减免金额（元）
    amount: 0,
    // 半小时收费
    increaseFee: 0,
    couponList: [],
    // 微信还需支付的金额
    needFee: 0,
    // 停车券减免金额
    couponMoney: 0,
    // 停车券减免总时长
    couponHours: 0,
    // 是否使用积分兑换
    pointsUse: false,
    // 积分兑换的金额
    pointsAmount: 0,
    // 是否使用会员专享时长
    switchStatus: false,
    // 会员专享-时长
    memberParkingTime: 0,
    // 会员专享-时长(参与计算)
    memberParkingTimeCount: 0,
    // 会员专享抵扣金额（参与计算）
    memberParkingMoney: 0.00,
    // 会员专享抵扣小时（参与计算）
    memberParkingHour: 0,
    // 黄金会员抵扣的金额
    vipMoney: 0,
    // 是否使用电子券抵扣
    couponUse: false,
    // 电子券id
    couponParkingIds: '',
    // 电子券数量
    gainCounts: '1',
    // 减免时长
    freeHour: 0,
    // 减免金额
    freeMoney: 0,
    // 减免金额
    freeFee: 0,
    // 积分减免时长
    scoreTime: 0,
    // 标记是那一辆车跳转过来的
    index: 0,
    // 用户积分
    points: 0,
    // 用户等级积分兑换金额(说明)
    exchangePrice: 0,
    // 用户等级积分兑换金额
    pointsToPrice: '0.00',
    pointsToHours: 0,
    // 用户输入的积分
    pointsInput: '',
    minPoints: 5,
    // 百货积分选择的数量
    generalNumber: 0,
    generalScore: 0,
    generalTime: 0,
    generalAmount: 0,
    marketNumber: 0,
    marketScore: 0,
    marketTime: 0,
    marketAmount: 0,
    // 是否支持停车券多张使用 默认不支持
    isMoreCoupon: false,
    // 是否使用小时抵扣
    isUseHour: false,
    isHaveDiscountButton: false,
    isUseBenift: true,
    singleUseCouponNum: 0,
    // 抵扣类型（存在就是小时：duration，否则就是金额）：查询账单时传免费时长还是免费金额
    deductionType: '',
    // 抵扣总时长
    freeTime: 0,
    showParkingDialog: false,
    payInfo: {},
    isSubmit: false
  },

  // 百货积分选择 (积分当做百货积分处理)
  changeGeneralScore(event) {
    let val = Number(event.currentTarget.dataset.val);
    let generalNumber = this.data.generalNumber + val;
    let MemberVipInfo = this.data.MemberVipInfo;
    let bHpoints = MemberVipInfo.points;
    let bHpointsHours = MemberVipInfo.pointsHours;
    let bhUpperLimit = MemberVipInfo.upperLimit;
    let bhUseScore = MemberVipInfo.useScore;
    let generalScore = benMath.accMul(generalNumber, bHpoints);
    let bhUseScores = benMath.accAdd(bhUseScore, generalScore);
    // 没有百货积分
    if (!MemberVipInfo.balanceScore || MemberVipInfo.balanceScore <= 0) return this.messageComponent.applyActive('您的积分不足~');
    // 应缴金额小于等于0
    if (this.data.needFee <= 0 && val == 1 && this.data.deductionType !== 'duration') {
      return this.messageComponent.applyActive('应缴费用已经为0~')
    } else if (generalScore > MemberVipInfo.balanceScore) {
      return this.messageComponent.applyActive('您的积分不足~')
    } else if (generalNumber < 0) {
      return false;
    } else if (bhUseScores > bhUpperLimit && val == 1) {
      return this.messageComponent.applyActive(`每人每天最高抵扣${bhUpperLimit}积分哟`)
    } else {
      this.setData({
        generalNumber: generalNumber,
        // 百货抵扣的积分(数量*积分)
        generalScore: generalScore,
        // 百货抵扣的时长 (数量*抵扣的小时)
        generalTime: formatMoney.priceFilter(benMath.accMul(generalNumber, bHpointsHours)),
        // 百货抵扣的金额 (数量*1小时金额*抵扣的小时）
        generalAmount: formatMoney.priceFilter(benMath.accMul(generalNumber * this.data.amount, bHpointsHours))
      })

      this.handleCountScore();
      // 重新计算金额
      this.handleCountNeedFee();
    }
  },

  // 超市积分选择
  changeMarketScore(event) {
    let val = Number(event.currentTarget.dataset.val);
    let marketNumber = this.data.marketNumber + val;
    let MemberVipInfo = this.data.MemberVipInfo;
    let cSpoints = MemberVipInfo.cSpoints;
    let cSpointsHours = MemberVipInfo.cSpointsHours;
    let csUpperLimit = MemberVipInfo.csUpperLimit;
    let csUseScore = MemberVipInfo.csUseScore;
    let csUseScores = benMath.accAdd(csUseScore, benMath.accMul(marketNumber, cSpoints));
    // 没有超市积分
    if (!MemberVipInfo.cSbalanceScore) return this.messageComponent.applyActive('您还没有超市积分呢~')
    // 应缴金额小于等于0
    if (this.data.needFee <= 0 && val == 1 && this.data.deductionType !== 'duration') {
      return this.messageComponent.applyActive('应缴费用已经为0~')
    } else if (csUseScores > MemberVipInfo.cSbalanceScore) {
      return this.messageComponent.applyActive('您的积分不足~')
    } else if (marketNumber < 0) {
      return false;
    } else if (csUseScores > csUpperLimit && val == 1) {
      return this.messageComponent.applyActive(`超市类积分,每人每天最高抵扣${csUpperLimit}积分哟`)
    } else {
      this.setData({
        marketNumber: marketNumber,
        // 超市抵扣的积分(数量*积分)
        marketScore: benMath.accMul(marketNumber, cSpoints),
        // 超市抵扣的时长 (数量*抵扣的小时)
        marketTime: formatMoney.priceFilter(benMath.accMul(marketNumber, cSpointsHours)),
        // 超市抵扣的金额（数量*1小时金额*抵扣的小时）
        marketAmount: formatMoney.priceFilter(benMath.accMul(marketNumber * this.data.amount, cSpointsHours))
      })
      this.handleCountScore();
      // 重新计算金额
      this.handleCountNeedFee();
    }
  },

  // 计算超市和百货合计抵扣的金额
  handleCountScore() {
    this.setData({
      pointsToPrice: formatMoney.priceFilter(benMath.accAdd(this.data.generalAmount, this.data.marketAmount)),
      pointsToHours: benMath.accAdd(this.data.generalTime, this.data.marketTime)
    })
  },

  // 会员折扣switch
  handleSwitch(e) {
    console.log('switch1 发生 change 事件，携带值为', e.detail.value);
    this.setData({
      switchStatus: e.detail.value
    });
    let switchStatus = this.data.switchStatus;
    if (switchStatus) {
      this.setData({
        memberParkingMoney: this.data.vipMoney,
        memberParkingHour: this.data.memberTime,
        memberParkingTimeCount: this.data.memberParkingTime
      })
    } else {
      this.setData({
        memberParkingMoney: 0.00,
        memberParkingHour: 0,
        memberParkingTimeCount: 0
      })
    }
    // 重新计算金额
    this.handleCountNeedFee();
  },

  // 显示更多电子券
  showMoreCoupon() {
    this.setData({
      couponList: this.data.list1,
      showMore: true
    });
  },

  // 隐藏更多电子券
  hideMoreCoupon() {
    let couponArr = this.data.list1.slice(0, 2);
    this.setData({
      couponList: couponArr,
      showMore: false
    });
  },

  // 电子券减免
  checkboxElectChange(e) {
    let checkedId = e.detail.value;
    let couponList = this.data.list1;
    let errMsg = false
    // 不支持券多选
    if (!this.data.isMoreCoupon) {
      if (checkedId && checkedId.length > 1) {
        checkedId = checkedId.pop()
      }
      couponList.forEach((item) => {
        item.memberCouponParkingId == checkedId ? item.isSelected = true : item.isSelected = false
      });
    } else {
      if (this.data.singleUseCouponNum > 0 && checkedId && checkedId.length > this.data.singleUseCouponNum) {
        checkedId.pop();
        errMsg = true
      }
      for (let i = 0; i < couponList.length; i++) {
        if (checkedId.indexOf((couponList[i].memberCouponParkingId).toString()) !== -1) {
          couponList[i].isSelected = true
        } else {
          couponList[i].isSelected = false
        }
      }
    }
    this.setData({
      list1: couponList
    });
    if (errMsg) {
      return this.messageComponent.applyActive('停车券一次最多使用' + this.data.singleUseCouponNum + '张');
    }
    // 重新计算金额
    this.handleCountNeedFee();
  },

  // 获取车牌信息
  getCarInfo(organizeId, freeTime = 0) {
    let params = {
      carNum: this.data.carNumber,
      organizeId: organizeId,
      freeTime: freeTime
    };
    Tips.loading('加载中');
    parkApi.parkingInfo(params).then(res => {
      if (res.data.flag) {
        this.setData({
          info: res.data.data,
          needFee: res.data.data.serviceFee,
          orderId: res.data.data.orderId,
          isShow: true,
          isHaveDiscountButton: res.data.data.isHaveDiscountButton,
          isUseBenift: res.data.data.canMemberUse ? res.data.data.canMemberUse == 'Y' : true,
          deductionType: res.data.data.deductionType
        });
        if (this.data.info.url || this.data.deductionType == 'duration') {
          this.setData({
            isUseHour: true
          });
        }
        // 获取券列表以及会员抵扣
        // this.getMemberVipInfo(res.data.data.serviceFee, organizeId);
        this.getMemberVipInfo(this.handleFormatDate(res.data.data.serviceTime), organizeId);
      } else {
        this.messageComponent.applyActive(res.data.msg);
      }
      Tips.loaded();
    }).catch(err => {
      Tips.loaded();
      console.log(err)
    })
  },

  // 优化传入的小时数，半小时为单位
  handleFormatDate(s) {
    if (s > -1) {
      let hour = Math.floor(s / 3600);
      let min = Math.floor(s / 60) % 60;
      // return min >= 30 ?  hour + 1 : hour + 0.5
      return min > 0 ?  hour + 1 : hour
    }
  },

  // 获取会员优惠信息
  getMemberVipInfo(serviceFee, organizeId) {
    let params = {
      serviceFee: serviceFee,
      organizeId: organizeId
    };
    parkApi.parkingDesc(params).then(res => {
      if (res.data.flag) {
        this.setData({
          // 会员的可用积分数
          points: Math.floor(res.data.data.balanceScore),
          // 一小时停车券减免金额（元）基数
          amount: res.data.data.amount * 1,
          // 每半小时收费基数
          increaseFee: res.data.data.increaseFee,
          isMoreCoupon: res.data.data.isParkingCouponOverlayUse && res.data.data.isParkingCouponOverlayUse == 'Y',
          MemberVipInfo: res.data.data,
          memberTime: res.data.data.memberTime,
          memberParkingTime: res.data.data.memberTime ? res.data.data.memberTime : 0,
          vipMoney: formatMoney.priceFilter(benMath.accMul(res.data.data.amount, res.data.data.memberTime ? res.data.data.memberTime : 0)),
          singleUseCouponNum: res.data.data.singleUseCouponNum ? res.data.data.singleUseCouponNum : 0
        });

        // 会员专享可用，会员专享券未使用
        if (this.data.isUseBenift && res.data.data.isMemberUse == 'Y' && res.data.data.memberState == 'N') {
          this.setData({
            // 默认选择使用会员专享券
            switchStatus: true,
            memberParkingMoney: this.data.vipMoney,
            memberParkingHour: this.data.memberTime,
            memberParkingTimeCount: res.data.data.memberTime ? res.data.data.memberTime : 0
          })
        }

        // 会员专享可用，会员专享券已经使用
        if (res.data.data.isMemberUse == 'Y' && res.data.data.memberState == 'Y') {
          this.setData({
            // 默认选择使用会员专享券
            switchStatus: false,
            memberParkingMoney: 0.00,
            memberParkingHour: 0,
            memberParkingTimeCount: 0
          })
        }

        // 停车券的使用 isMoreCoupon 是否默认选一张或者多张
        if (res.data.data.parkingcoupon && res.data.data.parkingcoupon.length > 0) {
          res.data.data.parkingcoupon.forEach((item, i) => {
            item.isSelected = this.data.isMoreCoupon ? item.isDefault == 'Y' : item.isDefault == 'Y' && i == 0;
            item.price = benMath.accMul(item.parkingHour, res.data.data.amount);
            item.parkingHour = formatMoney.floatFilter(item.parkingHour)
          });
          this.setData({
            list1: res.data.data.parkingcoupon
          });
        }

        // 计算微信还需支付的金额
        this.handleCountNeedFee();
        // 所有数据加载完成后，才可以提交支付
        this.setData({
          btnDisabled: false
        });
      } else {
        // 计算微信还需支付的金额
        this.handleCountNeedFee();
        this.messageComponent.applyActive(res.data.msg);
      }
    }).catch(err => {
      console.log(err)
    })
  },

  // 计算微信还需支付的金额
  handleCountNeedFee() {
    // 应付金额
    let totalMoney = this.data.info.serviceFee;
    // 黄金会员抵扣的金额
    let memberParkingMoney = this.data.memberParkingMoney;
    // 会员抵扣的小时
    let memberParkingHour = this.data.memberParkingHour;
    // 停车券减免金额
    let couponMoney = 0;
    // 停车券减免时长
    let couponHours = 0;
    // 停车券列表
    let list1 = this.data.list1;
    if (list1 && list1.length > 0) {
      list1.forEach((item) => {
        // 如果券是勾选状态
        if (item.isSelected) {
          couponMoney = benMath.accAdd(couponMoney, item.price);
          couponHours = benMath.accAdds(couponHours, item.parkingHour);
        }
      });
    }
    // 超市和百货积分的金额总和
    let pointsToPrice = this.data.pointsToPrice;
    // 超市和百货积分的时长总和
    let pointsToHours = this.data.pointsToHours;
    // 减免的总金额（会员+券+积分抵扣的金额）
    let freeMoney = benMath.accAdds(memberParkingMoney, couponMoney, pointsToPrice);
    // console.log('减免的总金额' + freeMoney);
    // 减免的总小时数
    let freeHour = benMath.accAdds(memberParkingHour, couponHours, pointsToHours);
    // 减免的金额 公式 Y= 5+（X－1) ÷ 0.5 x Z   （ y：抵扣费用  X：抵扣时长  Z：每增加0.5小时收费金额）
    // 抵扣时长
    // let hour = benMath.accDiv((freeHour - 1), 0.5);
    // console.log(hour, 'hour')
    // console.log(freeHour, 'freeHour')
    // console.log(this.data.increaseFee, 'increaseFee')
    // let money = benMath.accMul(hour, this.data.increaseFee);
    // console.log(money, 111)
    // let moneys = benMath.accAdd(this.data.amount, money);
    // console.log(moneys, 222)
    // let freeFee = freeHour > 0 ? moneys : 0;
    // console.log('优惠时长' + freeHour);
    // 微信还需支付的金额
    let needFee = benMath.accSub(totalMoney, freeMoney);
    this.setData({
      list1: list1,
      couponMoney: couponMoney,
      couponHours: couponHours,
      freeFee: freeMoney,
      freeHour: freeHour,
      needFee: needFee > 0 ? needFee : 0
    });
  },

  // 缴费 处理参数
  reCountParams() {
    // 拼接券id 以及券的数量（默认都是1）,券的优惠小时数
    // ===================Start====================== //
    let couponList = this.data.list1;
    let couponParkingIds = [];
    let couponRelationIds = [];
    let gainCounts = [];
    let couponHour = 0;
    let couponMoney = 0;
    if (couponList && couponList.length) {
      couponList.forEach((item) => {
        if (item.isSelected) {
          item.couponType == 'MEMBER_PARKING' && couponParkingIds.push(item.memberCouponParkingId);
          item.couponType == 'RELATION_MEMBER' && couponRelationIds.push(item.memberCouponParkingId);
          gainCounts.push(1);
          couponHour = benMath.accAdd(couponHour, Number(item.parkingHour));
          couponMoney = benMath.accAdd(couponMoney, item.price);
        }
      });
      this.setData({
        couponUse: couponParkingIds.length > 0 || couponRelationIds.length > 0,
        couponParkingIds: couponParkingIds.join(),
        couponRelationIds: couponRelationIds.join(),
        gainCounts: gainCounts.join()
      })
    }
    // =====================End==================== //

    // 计算减免的总时长&减免的总金额
    // 会员专享减免时长
    let memberParkingTime = this.data.memberParkingTimeCount;
    // 会员专享减免金额
    let memberParkingMoney = this.data.memberParkingMoney;
    // 券优惠减免时长
    let couponHours = couponHour;
    // 券优惠减免金额
    let couponMoneys = couponMoney;
    // 百货积分减免时长
    let generalTime = this.data.generalTime;
    // 百货积分减免金额
    let generalAmount = this.data.generalAmount;
    // 百货积分减免时长
    let marketTime = this.data.marketTime;
    // 百货积分减免金额
    let marketAmount = this.data.marketAmount;
    // 减免总时长（包含会员，券，积分兑换时长）
    let freeHour = benMath.accAdds(memberParkingTime, couponHours, generalTime, marketTime);
    // 减免总金额（包含会员，券，积分兑换金额）
    let freeMoney = benMath.accAdds(memberParkingMoney, couponMoneys, generalAmount, marketAmount);
    this.setData({
      freeHour: freeHour,
      freeMoney: freeMoney
    });
  },

  // 点击抵扣优惠
  handleDiscount() {
    this.reCountParams();
    // if (this.data.needFee > 0) {
    //   Tips.confirm('小程序支付暂未开通，请前往【昆山巴黎春天百货】服务号点击菜单(非会员停车缴费)进行全额缴费，感谢您的配合', {}, '提示', '', '确定');
    //   return;
    // }
    let params = {
      // 车牌号
      carNumber: this.data.carNumber,
      // 进场时间
      entryTime: this.data.info.startTime,
      // 停放时长
      elapsedTime: this.data.info.serviceTime,
      // 停车费用
      totalMoney: this.data.info.serviceFee,
      // 是否使用会员专享时长
      memberUse: this.data.switchStatus,
      // 会员专享时长
      memberParkingTime: this.data.memberParkingTimeCount,
      // 会员会员专享金额
      // memberAmount: this.data.vipMoney,
      // 实际支付总额
      payFee: this.data.needFee,
      // 是否使用积分兑换(积分值大于0就认为用了积分抵扣)
      pointsUse: this.data.marketScore > 0 || this.data.generalScore > 0,
      // 百货兑换金额
      // BHpointsAmount: this.data.generalAmount,
      // 百货的积分数
      BHgainPoints: this.data.generalScore,
      // 百货抵扣的时长
      BHpointsHours: this.data.generalTime,
      // 超市兑换金额
      // CSpointsAmount: this.data.marketAmount,
      // 超市抵扣的小时
      CSpointsHours: this.data.marketTime,
      // 超市的积分数
      CSgainPoints: this.data.marketScore,
      // 是否使用停车券
      couponUse: this.data.couponUse,
      // 消费停车券的id, ','拼接
      couponParkingIds: this.data.couponParkingIds,
      // 兑换停车券id ','拼接
      couponRelationIds: this.data.couponRelationIds,
      // 停车券数量,默认都是1
      gainCounts: this.data.gainCounts,
      // 减免的金额（会员专享，券，积分兑换）
      freeMoney: this.data.freeFee,
      // 减免的总时长（会员专享，券，积分兑换）
      freeHour: this.data.freeHour,
      // 组织id
      organizeId: this.data.theOrganizeId,
      // 订单id
      orderId: this.data.orderId,
      // 抵扣优惠
      onlyDiscount: true
    };
    if (params.memberUse || params.pointsUse || params.couponUse) {
      if (this.data.pending) return;
      this.setData({
        btnDisabled: true
      });
      Tips.loading('订单生成中');
      parkApi.getPayInfo(params).then(res => {
        Tips.loaded();
        if (res.data.flag) {
          Tips.success('优惠抵扣成功', 1000).then(() => {
            wx.navigateBack({
              delta: 1
            })
          })
        } else {
          this.messageComponent.applyActive(res.data.msg);
          this.setData({
            btnDisabled: false
          });
        }
      }).catch(err => {
        console.log(err);
        this.setData({
          btnDisabled: false
        });
      })
    } else {
      wx.showToast({
        title: '无优惠抵扣',
        duration: 1000,
        icon: 'none'
      });
    }
  },

  checkParkingType() {
    if (this.data.deductionType == 'duration') {
      this.getPayAmount();
    } else {
      this.handleSubmit();
    }
  },

  getPayAmount() {
    this.setData({
      btnDisabled: true
    });
    Tips.loading('加载中');
    this.reCountParams();
    let params = {
      // 车牌号
      carNumber: this.data.carNumber,
      // 停车费用
      totalMoney: this.data.info.serviceFee,
      // 减免的总时长（会员专享，券，积分兑换）
      freeHour: this.data.freeHour,
      // 组织id
      organizeId: this.data.theOrganizeId,
      // 订单id
      orderId: this.data.orderId
    };
    parkApi.getPayAmount(params).then(res => {
      Tips.loaded();
      if (res.data.flag) {
        this.setData({
          payInfo: res.data.data,
          needFee: res.data.data.payAmount,
          freeFee: res.data.data.freeAmount,
          showParkingDialog: true,
          btnDisabled: false
        })
      } else {
        this.messageComponent.applyActive(res.data.msg);
        this.setData({
          btnDisabled: false
        });
      }
    }).catch(err => {
      console.log(err);
      this.setData({
        btnDisabled: false
      });
    })
  },

  // 关闭弹窗
  handleCloseDialog() {
    this.setData({
      showParkingDialog: false,
      btnDisabled: false
    })
  },

  // 点击立即缴费
  handleSubmit() {
    if (this.data.isSubmit) {
      return;
    }
    this.reCountParams();
    let params = {
      // 车牌号
      carNumber: this.data.carNumber,
      // 进场时间
      entryTime: this.data.info.startTime,
      // 停放时长
      elapsedTime: this.data.info.serviceTime,
      // 停车费用
      totalMoney: this.data.info.serviceFee,
      // 是否使用会员专享时长
      memberUse: this.data.switchStatus,
      // 会员专享时长
      memberParkingTime: this.data.memberParkingTimeCount,
      // 会员会员专享金额
      // memberAmount: this.data.vipMoney,
      // 实际支付总额
      payFee: this.data.needFee,
      // 是否使用积分兑换(积分值大于0就认为用了积分抵扣)
      pointsUse: this.data.marketScore > 0 || this.data.generalScore > 0,
      // 百货兑换金额
      // BHpointsAmount: this.data.generalAmount,
      // 百货的积分数
      BHgainPoints: this.data.generalScore,
      // 百货抵扣的时长
      BHpointsHours: this.data.generalTime,
      // 超市兑换金额
      // CSpointsAmount: this.data.marketAmount,
      // 超市抵扣的小时
      CSpointsHours: this.data.marketTime,
      // 超市的积分数
      CSgainPoints: this.data.marketScore,
      // 是否使用停车券
      couponUse: this.data.couponUse,
      // 消费停车券的id, ','拼接
      couponParkingIds: this.data.couponParkingIds,
      // 兑换停车券id ','拼接
      couponRelationIds: this.data.couponRelationIds,
      // 停车券数量,默认都是1
      gainCounts: this.data.gainCounts,
      // 减免的金额（会员专享，券，积分兑换）
      freeMoney: this.data.freeFee,
      // 减免的总时长（会员专享，券，积分兑换）
      freeHour: this.data.freeHour,
      // 组织id
      organizeId: this.data.theOrganizeId,
      // 订单id
      orderId: this.data.orderId
    };
    console.log(params);
    // 判断是否为ETCP调用，并且存在会员专享、积分、券抵扣
    if (this.data.info.url && (params.memberUse || params.pointsUse || params.couponUse)) {
      Tips.confirm('请确认是否使用抵扣（确定后即扣除相应券、积分与权益）', '', '提示', '取消', '确认').then(() => {
        if (this.data.pending) return;
        this.setData({
          btnDisabled: true,
          isSubmit: true
        });
        Tips.loading('订单生成中');

        parkApi.getPayInfo(params).then(res => {
          Tips.loaded();
          if (res.data.flag) {
            let { orderNumber: orderNo, url } = res.data.data;
            if (res.data.data.isNeedPay) {
              url ? this.ToMiniProgram(res.data.data) : this.wxPay(orderNo)
            } else {
              Tips.success('缴费成功', 1000).then(() => {
                wx.navigateBack({
                  delta: 1
                })
              })
            }
          } else {
            this.messageComponent.applyActive(res.data.msg);
            this.setData({
              btnDisabled: false,
              isSubmit: false
            });
          }
        }).catch(err => {
          console.log(err);
          this.setData({
            btnDisabled: false,
            isSubmit: false
          });
        })
      }).catch(() => {
        this.setData({
          btnDisabled: false,
          isSubmit: false
        });
        this.getCarInfo(this.data.theOrganizeId);
      })
    } else {
      if (this.data.pending) return;
      this.setData({
        btnDisabled: true,
        isSubmit: true
      });
      Tips.loading('订单生成中');

      parkApi.getPayInfo(params).then(res => {
        Tips.loaded();
        if (res.data.flag) {
          let { orderNumber: orderNo, url } = res.data.data;
          if (res.data.data.isNeedPay) {
            // 如果是时长类型，更新支付金额
            if (this.data.deductionType == 'duration') {
              if (this.data.needFee != res.data.data.needFee) {
                this.messageComponent.applyActive('您的停车费用已经发生变化请重新刷新页面！');
                this.setData({
                  showParkingDialog: false,
                  btnDisabled: false,
                  isSubmit: false
                })
                return;
              } else {
                this.setData({
                  needFee: res.data.data.needFee
                });
              }
            }
            url ? this.ToMiniProgram(res.data.data) : this.wxPay(orderNo)
          } else {
            Tips.success('缴费成功', 1000).then(() => {
              wx.navigateBack({
                delta: 1
              })
            })
          }
        } else {
          this.messageComponent.applyActive(res.data.msg);
          this.setData({
            btnDisabled: false,
            isSubmit: false
          });
        }
      }).catch(err => {
        console.log(err);
        this.setData({
          btnDisabled: false,
          isSubmit: false
        });
      })
    }
  },

  // 微信支付
  wxPay(orderNo) {
    wxPay.getNewWxPayConfig(orderNo).then(() => {
      Tips.success('缴费成功', 1000).then(() => {
        wx.navigateBack({
          delta: 1
        })
      })
      this.setData({
        btnDisabled: false,
        isSubmit: false
      });
    }).catch(err => {
      console.log(err)
      this.setData({
        btnDisabled: false,
        isSubmit: false
      });
    })
  },

  // 微信支付
  getConfig(orderNo, totalFee) {
    let params = {
      orderNo: orderNo,
      totalFee: totalFee,
      organizeId: this.data.theOrganizeId,
      appId: wx.getStorageSync('appId'),
      payType: PARKINGPAY || 'PARKINGPAY'
    };
    activityApi.getWxConfig2(params).then(res => {
      console.log(res);
      if (res.data.flag) {
        let ret = res.data.data;
        let { timeStamp, nonceStr, package: packages, paySign, signType } = ret;
        console.log('timeStamp:' + timeStamp);
        console.log('nonceStr:' + nonceStr);
        console.log('package:' + packages);
        console.log('paySign:' + paySign);
        console.log('signType:' + signType);
        wxPay.pay(timeStamp, nonceStr, packages, paySign, signType).then(ress => {
          console.log(ress);
          Tips.success('缴费成功', 1000).then(() => {
            wx.navigateBack({
              delta: 1
            })
          })
          this.setData({
            btnDisabled: false,
            isSubmit: false
          });
        }).catch(errs => {
          console.log(errs);
          console.log('取消支付');
          this.setData({
            btnDisabled: false,
            isSubmit: false
          });
        })
      } else {
        this.messageComponent.applyActive(res.data.msg);
        this.setData({
          btnDisabled: false,
          isSubmit: false
        });
      }
    }).catch(err => {
      console.log(err)
    })
  },

  // 昆山巴黎春天百货停车缴费
  ToMiniProgram(data) {
    let { token, orderId: syncId } = data;
    let path = `pages/pay/order-pay-open/main?token=${token}&syncId=${syncId}&payType=6&CarNumber=${this.data.carNumber}&actionId=1`;
    wx.navigateToMiniProgram({
      appId: 'wxc07f9d67923d676d', // 线上
      // appId: 'wx192b7d2e8dcbefd0', // 测试
      path: path,
      extraData: {
        token: token,
        syncId: syncId,
        payType: 6,
        CarNumber: this.data.carNumber,
        actionId: '1'
      },
      envVersion: 'release',
      success(res) {
        console.log(res)
        // 打开成功
      },
      fail() {
        this.getCarInfo(this.data.theOrganizeId);
      }
    })
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(e) {
    // TODO: onLoad
    this.messageComponent = this.selectComponent('.message-tips');
    this.setData({
      carNumber: e.carNo,
      index: e.index,
      theOrganizeId: e.theOrganizeId
    });
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {
    // TODO: onReady
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {
    // TODO: onShow
    this.getCarInfo(this.data.theOrganizeId);
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {
    // TODO: onHide
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {
    // TODO: onUnload
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {
    // TODO: onPullDownRefresh
  }
})
