// 获取全局应用程序实例对象
// const app = getApp()
import Tips from '../../utils/tip'
import parkApi from '../../api/parking'
import {fetchOrganizeList} from '../../api/markets'
const LIMITCAR = 3;

// 创建页面实例对象
Page({
  /**
   * 页面的初始数据
   */
  data: {
    title: 'parkingPayment',
    messageComponent: '',
    showDialog: false,
    province: '',
    carNumber: '',
    // 绑定的车辆
    bindCarList: [],
    // 是否开通了车位预定
    isPrepay: true,
    // 默认选中的车牌号
    activeCarNumber: 0,
    // 空余车位
    restSpace: 0,
    // 当前选中的车牌
    currentCarNumber: '',
    carInfo: {},
    date: {
      d: 0,
      h: '00',
      m: '00',
      s: '00'
    },
    timeText: '00:00:00',
    timeFlag: false,
    spinFlag: true,
    // 停车公告 || 停车主体内容切换
    isRule: false,
    notice: '',
    organizeList: [],
    theOrganizeId: '',
    firstTheOrganizeId: '',
  },

  // 切换规则
  handleSwitchRule() {
    this.setData({
      isRule: !this.data.isRule
    })
    // 关闭公告重新拉取一次数据
    if (!this.data.isRule) {
      this.getCarNum(this.data.theOrganizeId);
      this.setData({ spinFlag: true })
    }
  },
    // 获取组织列表
  getOrganizeList() {
    let params = {
      industryType:'ShoppingMall'
    };
    fetchOrganizeList(params).then(res => {
      console.log('获取组织列表', res)
      if (res.data.flag) {
        let organizeList = res.data.data;
        let list = [];
        organizeList.forEach(item => {
          console.log('item', item.organizeList);
          list.push(...item.organizeList);
        });
        if (organizeList && organizeList.length > 0) {
        list.forEach((item) => {
           if (item.organizeId == this.data.theOrganizeId){
              this.setData({
                chooseName:item.organizeName
              });
           }
        });
          this.setData({
            organizeList: list
          });
        }
      } else {
        this.messageComponent.applyActive(res.data.msg || '获取列表数据异常');
      }
    }).catch(err => {
      console.log(err)
    })
  },

  onChooseChanges(event) {
    console.log('获取列表', event);
    this.setData({
      theOrganizeId: event.detail.organizeId ? event.detail.organizeId : ''
    });
    this.setData({
      activeCarNumber: 0,
      spinFlag: true
    });
    this.getCarNum(event.detail.organizeId);
    // this.getList(1,event.detail.organizeId)
  },
  
  // swiper滑动
  swiperChange(event) {
    let current = event.detail.current;
    this.setData({
      activeCarNumber: current,
      spinFlag: true
    });
    // Tips.loading('获取车辆状态中');
    setTimeout(() => this.getCarInfo(this.data.bindCarList[current].carNumber, this.data.theOrganizeId), 500);
  },
  
  // 选择车牌
  handleChooseCar(e) {
    let index = e.target.dataset.index;
    this.setData({
      activeCarNumber: index
    });
    // this.getCarInfo(this.data.bindCarList[index].carNumber);
  },
  
  // 跳转到车辆管理
  handleJumpCarManagement() {
    wx.navigateTo({
      url: `/pages/parkingPayment/carManagement/carManagement?theOrganizeId=${this.data.theOrganizeId}`
    })
  },
  
  // 跳转到缴费记录
  handleJumpRecord() {
    wx.navigateTo({
      url: `/pages/parkingPayment/paymentRecord/paymentRecord?theOrganizeId=${this.data.theOrganizeId}`
    })
  },
  
  // 跳转到车位预定(需要会员才能进入)
  handleJumpPrepay() {
    let memberType = wx.getStorageSync('memberType');
    if (this.data.isPrepay) {
      if (memberType == 'MEMBER') {
        wx.navigateTo({
          url: `/pages/parkingPayment/parkingPrepay/parkingPrepay?theOrganizeId=${this.data.theOrganizeId}`
        })
      } else {
        wx.navigateTo({
          url: `/pages/memberSign/memberSign`
        })
      }
    } else {
      this.messageComponent.applyActive('正在开发,敬请期待');
    }
  },
  
  // 跳转到在线缴费
  handleJumpPayment(e) {
    let index = e.currentTarget.dataset.index;
    let bindCarList = this.data.bindCarList;
    let carNo = bindCarList[index].carNumber;
    wx.navigateTo({
      url: `/pages/parkingPayment/onlinePayment/onlinePayment?carNo=${carNo}&index=${index}&theOrganizeId=${this.data.theOrganizeId}`
    })
  },
  
  // 打开绑定车辆弹窗(限制3)
  handleOpenBind() {
    let len = this.data.bindCarList.length;
    if (len >= LIMITCAR) {
      this.messageComponent.applyActive(`最多可添加${LIMITCAR}辆车哦~`);
      return false;
    }
    this.setData({
      showDialog: true
    })
  },

  // 重新绑定车牌
  handleResetBind() {
    wx.navigateTo({
      url: `/pages/parkingPayment/carManagement/carManagement?theOrganizeId=${this.data.theOrganizeId}&type=reset`
    })
  },
  
  // 关闭绑定车辆弹窗
  handleCloseBind() {
    this.setData({
      showDialog: false
    })
  },
  
  // 点击添加车辆确定的时候
  handleSure(e) {
    let carNumber = e.detail.province + e.detail.carNumber;
    Tips.loading('添加车牌中');
    setTimeout(() => this.saveMemberCarNum(carNumber, this.data.theOrganizeId), 500)
  },
  
  // 记录车位
  handleMarkPark() {
  
  },
  
  // 添加车牌
  saveMemberCarNum(carNumber, organizeId) {
    let params = {
      carNumber: carNumber,
      organizeId: organizeId
    };
    parkApi.saveMemberCarNum(params).then(res => {
      Tips.loaded();
      this.setData({
        showDialog: false
      });
      if (res.data.flag) {
        if (res.data.data && res.data.data.length > 0) {
          Tips.confirm('首次绑定车牌成功，特赠送您' + res.data.data.length + '张停车券', {}, '成功提示', '继续缴费', '前往查看').then(() => {
            Tips.loading('前往中');
            setTimeout(() => {
              wx.navigateTo({
                url: '/pages/newPackage/myCouponNew/myCoupon?currentTab=ONLINE&couponState=UNUSE&couponType=PARKING',
                success: () => {
                  Tips.loaded();
                }
              })
            }, 50);
          }).catch(() => {
            this.getCarNum(organizeId);
            this.setData({
              activeCarNumber: 0
            })
          });
        } else {
          Tips.success('添加成功').then(() => {
            this.getCarNum(organizeId);
            this.setData({
              activeCarNumber: 0
            })
          })
        }
      } else {
        this.messageComponent.applyActive(res.data.msg);
      }
    })
  },

  // 获取车牌
  getCarNum(organizeId) {
    let params = {
      organizeId: organizeId
    };
    parkApi.listCarNum(params).then(res => {
      if (res.data.flag) {
        this.setData({
          bindCarList: res.data.data.carNumberList ? res.data.data.carNumberList : [],
          currentCarNumber: res.data.data.carNumber ? res.data.data.carNumber : '',
          restSpace: res.data.data.restSpace ? res.data.data.restSpace : 0,
          notice: res.data.data.notice ? res.data.data.notice : '暂无停车公告'
        });
        if (res.data.data.carNumber) {
          // Tips.loading('获取车辆状态中');
          setTimeout(() => this.getCarInfo(res.data.data.carNumber, organizeId), 500);
        }
      } else {
        if (res.data.code == 4004) {
          let storageOrganizeId = wx.getStorageSync('organizeId');
          if (!storageOrganizeId) {
            wx.setStorageSync('organizeId', organizeId);
          }
          // 非会员进来直接跳去注册页面
          let memberType = wx.getStorageSync('memberType');
          if (memberType != 'MEMBER') {
            Tips.confirm('您还不是会员,无法使用该功能', {}, '前往注册?', '回首页', '去注册').then(() => {
              Tips.loading('前往中');
              setTimeout(() => {
                wx.navigateTo({
                  url: '/pages/memberSign/memberSign',
                  success: () => {
                    Tips.loaded();
                  }
                })
              }, 50);
            }).catch(() => {
              wx.switchTab({
                url: '/pages/homeMarket/home'
              })
            });
            return false
          } 
        } else {
          this.messageComponent.applyActive(res.data.msg || '获取车辆信息失败');
        }
      }
    })
  },
  
  // 根据车牌查询状态
  getCarInfo(carNum, organizeId) {
    let params = {
      carNum: carNum,
      organizeId: organizeId
    };
    this.setData({
      carInfo: {}
    });
    this.setData({
      carInfo: {}
    });
    parkApi.parkingInfo(params).then(res => {
      Tips.loaded();
      if (res.data.flag) {
        this.setData({
          carInfo: res.data.data,
          spinFlag: false
        });
        let carInfo = this.data.carInfo;
        // carInfo 0：正常状态；2：未停入；9：缴费之后未离场 10：无需缴费
        // 费用为0
        if (carInfo.parkStatus == 'OVERTIME') {
          carInfo.retcode = carInfo.serviceFee == 0 ? 9 : 0;
        } else {
          carInfo.retcode = carInfo.serviceFee == 0 ? 10 : 0;
        }


        // if (carInfo.serviceFee == 0) {
        //   if (carInfo.parkStatus == 'OVERTIME') {
        //     carInfo.retcode = 9
        //   } else {
        //     carInfo.retcode = 10
        //   }
        // } else {
        //   carInfo.retcode = 0
        // }
        // carInfo.retcode = res.data.data.serviceFee == 0 ? 10 : 0;
        this.setData({
          carInfo: carInfo
        });
        // 车辆未停入
      } else {
        if (res.data.msg == '车辆未停入' || res.data.msg == `${carNum}未入场` || res.data.msg == `无该账单信息`) {
          let carInfo = this.data.carInfo;
          carInfo.retcode = 2;
          this.setData({
            carInfo: carInfo,
            spinFlag: false
          })
        } else {
          this.messageComponent.applyActive(res.data.msg);
        }
      }
    }).catch(err => {
      console.log(err)
    })
  },
  
  // 缴费 倒计时
  countDownTime(min) {
    if (!min || min < 0) {
      return
    }
    let time = min * 60;
    let timer = null;
    timer = setInterval(() => {
      if (time === 0) {
        let carInfo = this.data.carInfo;
        carInfo.retcode = 'continuePay';
        this.setData({
          timeText: '00:00:00',
          timeFlag: true,
          carInfo: carInfo
        });
        clearInterval(timer);
        return
      }
      time = time - 1;
      let s = parseInt((time) % 60);
      let m = parseInt((time / 60) % 60);
      let h = parseInt((time / 60 / 60) % 24);
      this.setData({
        timeText: `${h < 10 ? '0' + h : h}:${m < 10 ? '0' + m : m}:${s < 10 ? '0' + s : s}`
      })
    }, 1000)
  },
  
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(e) {
    // TODO: onLoad
    this.messageComponent = this.selectComponent('.message-tips');
    let organizeId = wx.getStorageSync('organizeId');
    this.setData({
      theOrganizeId: e.theOrganizeId || organizeId,
      firstTheOrganizeId: e.theOrganizeId || organizeId
    })
    this.getOrganizeList();
  },
  
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {
    // TODO: onReady
  },
  
  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {
    // TODO: onShow
    // 非会员进来直接跳去注册页面
    let memberType = wx.getStorageSync('memberType');
    wx.getSetting({
      success(res) {
        console.log(res)
        if (res.authSetting.hasOwnProperty('scope.userLocation')) {
          if (memberType != 'MEMBER') {
            Tips.confirm('您还不是会员,无法使用该功能', {}, '前往注册?', '回首页', '去注册').then(() => {
              Tips.loading('前往中');
              setTimeout(() => {
                wx.navigateTo({
                  url: '/pages/memberSign/memberSign',
                  success: () => {
                    Tips.loaded();
                  }
                })
              }, 50);
            }).catch(() => {
              wx.switchTab({
                url: '/pages/homeMarket/home'
              })
            });
            return false
          }
        } 
      }
    })
    
    this.setData({
      activeCarNumber: 0,
      spinFlag: true
    });
    this.getCarNum(this.data.theOrganizeId);
  },
  
  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {
    // TODO: onHide
  },
  
  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {
    // TODO: onUnload
  },
  
  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {
    // TODO: onPullDownRefresh
  },

  /**
   * 页面相关分享
   */
  onShareAppMessage() {
    
  }
})
