// 获取全局应用程序实例对象
// const app = getApp()
import parkApi from '../../../api/parking'
import {fetchOrganizeList} from '../../../api/markets'
// 创建页面实例对象
Page({
  /**
   * 页面的初始数据
   */
  data: {
    title: 'paymentRecord',
    messageComponent: '',
    recordList: [],
    page: {
      pageNum: 1,
      pageSize: 10,
      total: 0
    },
    organizeList: [],
    isshowChoose: false,
    chooseId: 0,
    chooseName: '全部门店',
    theOrganizeId: ''
  },
  // chooseOrganize(e) {
  //   console.log('e', e);
  //   let theOrganizeId= e.currentTarget.dataset.organizeid;
  //   if(theOrganizeId == 0) {
  //     theOrganizeId = null;
  //   }
  //   this.setData({
  //     chooseId: e.currentTarget.dataset.organizeid,
  //     chooseName: e.currentTarget.dataset.organizename,
  //     theOrganizeId: theOrganizeId,
  //     isshowChoose: false
  //   });
  //   this.getList(1, this.data.theOrganizeId);
  // },
  // openChoose() {
  //   this.setData({
  //     isshowChoose: !this.data.isshowChoose
  //   });
  // },
  // 获取组织列表
  getOrganizeList() {
    let params = {
    };
    fetchOrganizeList(params).then(res => {
      console.log('获取组织列表', res)
      if (res.data.flag) {
        let organizeList = res.data.data;
        let list = [{
          organizeId: '',
          organizeName: '全部门店'
        }];
        organizeList.forEach(item => {
          console.log('item', item.organizeList);
          list.push(...item.organizeList);
        });
        if (organizeList && organizeList.length > 0) {
          this.setData({
            organizeList: list
          });
        }
      } else {
        this.messageComponent.applyActive(res.data.msg || '获取列表数据异常');
      }
    }).catch(err => {
      console.log(err)
    })
  },
  // 跳到详情
  handleJumpDetail(e) {
    let id = e.currentTarget.dataset.id
    wx.navigateTo({
      url: `../paymentRecordDetail/paymentRecordDetail?id=${id}&theOrganizeId=${this.data.theOrganizeId}`
    })
  },
  onChooseChanges(event) {
    let organizeId = event.detail.organizeId ? event.detail.organizeId : ''
    this.setData({
      theOrganizeId: organizeId
    });
    this.getList(1, organizeId)
  },
  // 获取列表
  getList(pageNum, organizeId) {
    console.log('获取列表', pageNum, organizeId)
    let params = {
      pageNum: pageNum,
      organizeId: organizeId
    };
    if (pageNum === 1) this.setData({ recordList: [] });
    parkApi.listMemberParkingRecord(params).then(res => {
      if (res.data.flag) {
        let recordList = res.data.data;
        if (recordList && recordList.length > 0) {
          this.setData({
            recordList: [...this.data.recordList, ...recordList],
            page: res.data.page
          });
        }
      } else {
        this.messageComponent.applyActive(res.data.msg || '获取列表数据异常');
      }
    }).catch(err => {
      console.log(err)
    })
  },

  // 加载更多
  loadMore(e) {
    this.getList(e.detail, this.data.theOrganizeId);
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(e) {
    let organizeId = e.theOrganizeId ? e.theOrganizeId : this.data.theOrganizeId
    this.messageComponent = this.selectComponent('.messageTips');
    this.setData({
      theOrganizeId: organizeId
    });
    this.getOrganizeList();
    this.getList(1, this.data.theOrganizeId);
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {
    // TODO: onReady
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {
    // TODO: onShow
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {
    // TODO: onHide
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {
    // TODO: onUnload
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {
    // TODO: onPullDownRefresh
  }
})
