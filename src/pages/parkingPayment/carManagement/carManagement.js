// 获取全局应用程序实例对象
const app = getApp()
import Tips from '../../../utils/tip'
import parkApi from '../../../api/parking'
const LIMITCAR = 3;

// 创建页面实例对象
Page({
  /**
   * 页面的初始数据
   */
  data: {
    title: 'carManagement',
    messageComponent: '',
    // 添加车辆弹窗
    showDialog: false,
    // 车辆列表
    bindCarList: [],
    theOrganizeId: '',
    // 是否送积分
    isScore: null
  },

  // 打开绑定车辆弹窗(限制3)
  handleOpenBind() {
    let len = this.data.bindCarList.length;
    if (len >= LIMITCAR) {
      this.messageComponent.applyActive(`最多可添加${LIMITCAR}辆车哦~`);
      return false;
    }
    this.setData({
      showDialog: true
    })
  },

  // 关闭绑定车辆弹窗
  handleCloseBind() {
    this.setData({
      showDialog: false,
      carNumber: ''
    })
  },

  // 点击添加车辆确定的时候
  handleSure(e) {
    let carNumber = e.detail.province + e.detail.carNumber;
    Tips.loading('添加车牌中');
    if (this.data.isScore && this.data.bindCarList.length === 0) {
      console.log('pubsub')
      app.pubSub.emit('addCarNumber', carNumber)
    } else {
      this.saveMemberCarNum(carNumber, this.data.theOrganizeId);
    }
  },

  // 添加车牌
  saveMemberCarNum(carNumber, organizeId) {
    let params = {
      carNumber: carNumber,
      organizeId: organizeId
    };
    parkApi.saveMemberCarNum(params).then(res => {
      Tips.loaded();
      this.setData({
        showDialog: false
      });
      if (res.data.flag) {
        if (res.data.data && res.data.data.length > 0) {
          Tips.confirm('首次绑定车牌成功，特赠送您' + res.data.data.length + '张停车券', {}, '成功提示', '去缴费', '前往查看').then(() => {
            Tips.loading('前往中');
            setTimeout(() => {
              wx.navigateTo({
                url: '/pages/newPackage/myCouponNew/myCoupon?currentTab=ONLINE&couponState=UNUSE&couponType=PARKING',
                success: () => {
                  Tips.loaded();
                }
              })
            }, 50);
          }).catch(() => {
            this.getListMemberCarNum(organizeId)
            wx.navigateBack({
              delta: 1
            })
          });
        } else {
          Tips.success('添加成功').then(() => {
            this.getListMemberCarNum(organizeId)
            if (this.data.type == 'reset') {
              wx.navigateBack({
                delta: 1
              })
            }
          })
        }
      } else {
        this.messageComponent.applyActive(res.data.msg);
      }
    })
  },

  // 删除车辆
  handleDelCar(e) {
    let index = e.target.dataset.index;
    let id = e.target.dataset.id;
    let organizeId = this.data.theOrganizeId;
    Tips.confirm('确定删除?', { index, id, organizeId }, '提示', '取消', '确定').then(res => {
      Tips.loading('删除中...')
      setTimeout(() => this.removeMemberCarNum(res.id, res.index, res.organizeId), 500)
    });
  },

  // 删除车牌(弹窗确定)
  removeMemberCarNum(id, index, organizeId) {
    let params = {
      memberCarnumberId: id,
      organizeId: organizeId
    };
    let bindCarList = this.data.bindCarList;
    parkApi.removeMemberCarNum(params).then(res => {
      Tips.loaded();
      if (res.data.flag) {
        bindCarList.splice(index, 1);
        this.setData({ bindCarList });
        Tips.success('成功删除')
      } else {
        this.messageComponent.applyActive(res.data.msg);
      }
    })
  },

  // 获取车牌
  getListMemberCarNum(organizeId) {
    let params = {
      organizeId: organizeId
    };
    parkApi.listMemberCarNum(params).then(res => {
      if (res.data.flag) {
        this.setData({
          bindCarList: res.data.data
        });
      } else {
        this.messageComponent.applyActive(res.data.msg);
      }
    })
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(e) {
    // TODO: onLoad
    this.messageComponent = this.selectComponent('.message-tips');
    this.setData({
      theOrganizeId: e.theOrganizeId || wx.getStorageSync('organizeId'),
      isScore: e.isScore || null,
      type: e.type || null
    });
    app.pubSub.on('addCarNumberSuccess', (flag, msg) => {
      if (flag) {
        this.setData({
          showDialog: false
        });
        Tips.success('添加成功').then(() => {
          this.getListMemberCarNum(this.data.theOrganizeId)
        })
      } else {
        this.messageComponent.applyActive(msg);
      }
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {
    // TODO: onReady
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {
    // TODO: onShow
    this.getListMemberCarNum(this.data.theOrganizeId)
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {
    // TODO: onHide
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {
    app.pubSub.off('addCarNumberSuccess', null)
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {
    // TODO: onPullDownRefresh
  }
})
