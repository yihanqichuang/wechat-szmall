// 获取全局应用程序实例对象
// const app = getApp()
import parkApi from '../../../api/parking'
import eleInvoice from '../../../api/eleInvoice';

// 创建页面实例对象
Page({
  /**
   * 页面的初始数据
   */
  data: {
    title: 'paymentRecord',
    messageComponent: '',
    detail: {},
    id: '',
    payFee: 0,
    showInvoiceButton: true
  },

  // 缴费明细
  parkingRecordDetail(id, organizeId) {
    let params = {
      parkingSignInfoId: id,
      organizeId: organizeId
    };
    parkApi.parkingRecordDetail(params).then(res => {
      console.log(res);
      if (res.data.flag) {
        this.setData({
          detail: res.data.data,
          payFee: Number(res.data.data.payFee)
        })
        // 查询是否已经开票
        eleInvoice.fetchIsOpenedEinvocie({ orderNo: res.data.data.orderNumber }).then(res => {
          if (res.data.flag && res.data.data) {
            this.setData({ showInvoiceButton: false });
          }
        });
      } else {
        this.messageComponent.applyActive(res.data.msg || '获取数据异常');
      }
    }).catch(err => {
      console.log(err)
    })
  },

  // 去开票
  makeInvoice() {
      let industryType = 'GeneralMerchandise';
      let orderFee = this.data.detail.payFee;
      let orderNo = this.data.detail.orderNumber;
      let orderTime = new Date(this.data.detail.createTime.replace(/-/g, '/')).getTime();
      let orderType = 'PARKING';
      let goodsName = this.data.detail.carNumber;
      console.log("orderTime",orderTime)
      wx.navigateTo({
        url: `/pages/eleInvoice/makeInvoices/makeInvoices?orderNo=${orderNo}&orderFee=${orderFee}&orderTime=${orderTime}&orderType=${orderType}&goodsName=${goodsName}&industryType=${industryType}`
      })
    },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(e) {
    // TODO: onLoad
    this.messageComponent = this.selectComponent('.messageTips');
    this.setData({
      theOrganizeId: e.theOrganizeId,
      id: e.id
    });
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {
    // TODO: onReady
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {
    // TODO: onShow
    this.parkingRecordDetail(this.data.id, this.data.theOrganizeId);
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {
    // TODO: onHide
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {
    // TODO: onUnload
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {
    // TODO: onPullDownRefresh
  }
})
