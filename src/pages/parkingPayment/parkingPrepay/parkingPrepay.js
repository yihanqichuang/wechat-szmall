// 获取全局应用程序实例对象
// const app = getApp()
import parkingPrepayApi from '../../../api/parkingPrepay'
import Tips from '../../../utils/tip'
import { CARNUMBER } from '../../../utils/regular'
// 创建页面实例对象
Page({
  /**
   * 页面的初始数据
   */
  data: {
    title: 'parkingPrepay',
    queryOneData: {},
    form: {
      reserveStartTime: '',
      carNumber: '',
      formId: ''
    },
    reserveRules: '',  // 预约规则
    reserveData: {},  // 我的预约信息
    reserveDisabled: false, // 是否处于可预约状态
    messageComponent: null,
    isShowKey: false,  // 显示选择车牌号键盘
    provinceDefault: '${provinceDefault}' || '浙',
    carNumber: '',
    keyBoardType: 1,
    isRule: false
  },

  // 切换规则
  handleSwitchRule() {
    this.setData({
      isRule: !this.data.isRule
    })
  },

  // 预约配置信息
  reserveQueryOne() {
    Tips.loading()
    parkingPrepayApi.reserveQueryOne().then(res => {
      console.log(this.data.reserveDisabled)
      Tips.loaded()
      if (!res.data.flag) {
        this.setData({ reserveDisabled: true })
        return this.messageComponent.applyActive(res.data.msg || '获取预约配置信息错误')
      }
      this.setData({
        'form.reserveStartTime': '',
        reserveRules: res.data.result.reserveRules,
        reserveData: res.data.data[0] || {}
      })
      this.handleReserveData(res.data.data[0])
    }, () => {
      Tips.loaded()
      this.messageComponent.applyActive('获取预约配置信息错误')
    })
  },
  // 判断页面状态
  handleReserveData(data) {
    if (!data) return
    console.log(data)
    let arr = ['RESERVE', 'ENTER', 'INVALID'], provinceDefault = '', carNumber = ''
    if (data.carNumber && data.carNumber.substr) {
      provinceDefault = data.carNumber.substr(0, 1)
      carNumber = data.carNumber.substr(1)
    }
    this.setData({
      reserveDisabled: arr.some(item => data.status === item),
      provinceDefault,
      carNumber
    })
  },
  // 我的预约（查询当前会员的预约信息）提前进场字段 inAdvance 如果是Y的话表示不能再提前进场
  reserveList() {
    parkingPrepayApi.reserveList().then(res => {
      console.log(res)
      if (!res.data.flag) return this.messageComponent.applyActive(res.data.msg || '获取我的预约信息错误')
      this.setData({
        reserveList: res.data.data
      })
    }, () => {
      this.messageComponent.applyActive('获取我的预约信息错误')
    })
  },
  
  // 马上预约
  prePay(e) {
    console.log('formId: ' + e.detail.formId)
    this.setData({ 'form.formId': e.detail.formId })
    let { reserveStartTime, formId } = this.data.form, carNumber = `${this.data.provinceDefault}${this.data.carNumber}`
    console.log({ reserveStartTime, carNumber, formId })
    if (!reserveStartTime) return this.messageComponent.applyActive('请选择预约时间')
    if (new Date(reserveStartTime.replace(/-/g, '/')).getTime() - Date.now() < 0) return this.messageComponent.applyActive('预约时间不能小于当前时间')
    if (!carNumber) return this.messageComponent.applyActive('请输入车牌号码')
    if (!CARNUMBER.test(this.data.carNumber)) return this.messageComponent.applyActive('请输入正确的车牌号码')
    Tips.loading('车位预约中')
    parkingPrepayApi.reserveSpace({ reserveStartTime, carNumber, formId }).then(res => {
      console.log(res)
      Tips.loaded()
      if (!res.data.flag) return this.messageComponent.applyActive(res.data.msg || '提交预约请求失败')
      Tips.success('预约成功~').then(() => this.reserveQueryOne())
    }, () => {
      Tips.loaded()
      this.messageComponent.applyActive('提交预约请求失败')
    })
  },
  // 提前入场
  advanceIn() {
    Tips.loading('提前入场中')
    let { parkingReserveRecordId, carNumber } = this.data.reserveData
    parkingPrepayApi.reserveInadvance({ parkingReserveRecordId, carNumber }).then(res => {
      Tips.loaded()
      if (!res.data.flag) return this.messageComponent.applyActive(res.data.msg || '提前入场请求失败')
      Tips.success('入场成功~').then(() => this.reserveQueryOne())
    }, () => {
      Tips.loaded()
      this.messageComponent.applyActive('提前入场请求失败')
    })
  },
  // 取消预约
  beforeCancel() {
    Tips.confirm(`取消后当天还可预约${this.data.reserveData.count}次`, {}, '取消车位预约', '否', '是').then(() => {
      this.cancel()
    }).catch(() => {})
  },
  cancel() {
    Tips.loading('取消预约中')
    let { parkingReserveRecordId, carNumber } = this.data.reserveData
    parkingPrepayApi.reserveCancel({ parkingReserveRecordId, carNumber }).then(res => {
      Tips.loaded()
      if (!res.data.flag) return this.messageComponent.applyActive(res.data.msg || '取消预约请求失败')
      Tips.success('取消成功~').then(() => this.reserveQueryOne())
    }, () => {
      Tips.loaded()
      this.messageComponent.applyActive('取消预约请求失败')
    })
  },
  // 重新预约
  reStart() {
    this.setData({
      'form.reserveStartTime': '',
      'reserveData.status': '',
      reserveDisabled: false
    })
  },
  // 返回上一页
  goBack() {
    wx.navigateBack({ delta: 1 })
  },
  
  // 车牌号
  // 点击省份
  chooseProvice() {
    this.setData({
      isShowKey: true,
      keyBoardType: 1
    })
  },
  // 点击输入车牌
  chooseCarNumber() {
    this.setData({
      isShowKey: true,
      keyBoardType: 2
    })
  },
  // 选择省份
  provinceNumber(e) {
    console.log(e)
    this.setData({
      provinceDefault: e.detail
    })
  },
  // 选择车牌
  vehicleTapNumber(e) {
    console.log(e)
    let value = e.detail;
    let carNumber = this.data.carNumber;
    if (carNumber.length > 6) return; // 车牌长度最多为7个（新能源车牌7个）
    carNumber += value;
    this.setData({
      carNumber: carNumber
    });
    console.log(this.data.carNumber)
  },
  // 输入的车牌号
  handleInputCar(e) {
    this.setData({
      carNumber: e.detail.value.toUpperCase()
    })
    console.log(this.data.carNumber);
  },
  // 点击删除按钮
  handleNumberDel() {
    let carNumber = this.data.carNumber;
    let arr = carNumber.split('');
    arr.splice(-1, 1)
    console.log(arr)
    let str = arr.join('')
    this.setData({
      carNumber: str
    })
  },
  // 点击确定
  handleEnsure() {
    this.setData({
      isShowKey: false
    })
  },
  formReset() {
    console.log('form发生了reset事件')
  },
  // 选择时间
  dateTimePickerChange(data) {
    console.log(data)
    this.setData({ 'form.reserveStartTime': data.detail })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad() {
    this.reserveQueryOne()
    // this.reserveList()
    this.messageComponent = this.selectComponent('.messageTips')
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {
    // TODO: onReady
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {
    // TODO: onShow
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {
    // TODO: onHide
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {
    // TODO: onUnload
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {
    // TODO: onPullDownRefresh
  }
})
