// 获取全局应用程序实例对象
const app = getApp();
const wechat = require('../../utils/wechat.js');
const api = require('../../api/login.js');
import Tips from '../../utils/tip';
import WxParse from '../../wxParse/wxParse.js'

// 创建页面实例对象
Page({
  /**
   * 页面的初始数据
   */
  data: {
    title: 'login',
    code: '',
    messageComponent: '',
    isAgree: false,
    isAuthor: false,
    memberType: 'FANS',
    defaultConfig: null,
    appid: '${appId}', // appid
    canIUseGetUserProfile: false,
    cardDesc: '',
  },

  getMemberDescInfo() {
    api.getMemberDesc().then(res => {
      if (res.data.flag) {
        if (res.data.data) {
          let _this = this;
          let article = res.data.data.cardDesc;
          this.setData({
            cardDesc: article
          });
          WxParse.wxParse('article', 'html', article, _this, 15);
        }
      }
    })
  },

  // 微信用户授权
  bindGetUserInfo(e) {
    let param = {};
    param.code = this.data.code;
    param.appId = '${appId}';
    param.encryptedData = e.detail.encryptedData;
    param.ivStr = e.detail.iv;
    wx.setStorageSync('appId', param.appId);
    app.globalData.userInfo = e.detail.userInfo;
    let that = this;
    // 用户点击授权按钮
    if (e.detail.errMsg == 'getUserInfo:ok') {
      wx.checkSession({
        success() {
          // session_key 未过期，并且在本生命周期一直有效
          that.getHashs(param);
        },
        fail() {
          // session_key 已经失效，需要重新执行登录流程
          wechat.login().then(res => {
            if (!res.code) return;
            that.setData({
              code: res.code
            })
            param.code = res.code;
            that.getHashs(param);
          })
        }
      })
    } else if (e.detail.errMsg == 'getUserInfo:fail auth deny') {
      // 用户按了拒绝按钮
      Tips.confirm('您点击了拒绝授权，将无法注册成为会员', {}, '温馨提示', '返回', '继续授权').then(() => {
        console.log('用户点击了返回授权')
      }).catch(() => {
        wx.navigateBack({
          delta: 2
        });
      })
    }
  },

  // 微信用户授权(2.10.4及以上版本支持)
  getUserProfile() {
    wx.getUserProfile({
      lang: 'zh_CN',
      desc: '用于完善会员资料', // 声明获取用户个人信息后的用途，后续会展示在弹窗中，请谨慎填写
      success: (res) => {
        let param = {};
        param.code = this.data.code;
        param.appId = '${appId}';
        param.encryptedData = res.encryptedData;
        param.ivStr = res.iv;
        wx.setStorageSync('appId', param.appId);
        app.globalData.userInfo = res.userInfo;
        let that = this;
        // 用户点击授权按钮
        if (res.errMsg == 'getUserProfile:ok') {
          wx.checkSession({
            success() {
              // session_key 未过期，并且在本生命周期一直有效
              that.getHashs(param);
            },
            fail() {
              // session_key 已经失效，需要重新执行登录流程
              wechat.login().then(res => {
                if (!res.code) return;
                that.setData({
                  code: res.code
                })
                param.code = res.code;
                that.getHashs(param);
              })
            }
          })
        } else if (res.errMsg == 'getUserProfile:fail auth deny') {
          // 用户按了拒绝按钮
          Tips.confirm('您点击了拒绝授权，将无法注册成为会员', {}, '温馨提示', '返回', '继续授权').then(() => {
            console.log('用户点击了返回授权')
          }).catch(() => {
            wx.navigateBack({
              delta: 2
            });
          })
        }
      }
    })
  },

  // 获取hash（新方法）
  async getHashs(param) {
    let url = 'szmall/oauth/token/' + param.code;
    // 调用登陆
    const tokenRes = await api.login(url, param);
    if ("00000" === tokenRes.data.code) {
      wx.setStorageSync('token', tokenRes.data.data.access_token);
      // 获取用户信息
      const userRes = await api.getUserInfo();
      wx.setStorageSync('userInfo', userRes.data);
      wx.setStorageSync('isAuthor', true);
      wx.navigateBack({ delta: 1 })
    } else {
      wx.showModal({
        title: '提示',
        content: '微信授权登录失败，请重试！',
        success: function () { }
      })
      wx.setStorageSync('isAuthor', false);
    }

    // let that = this;
    // let hostName = '${hostname}';
    // let url = hostName + 'szmall/oauth/token/' + param.code;
    // wx.request({
    //   url: url,
    //   data: Object.assign({ 'appType': 'smallApp' }, param),
    //   header: { 'Content-Type': 'application/json' },
    //   method: 'POST',
    //   success: function(res) {
    //     if (!res.data.flag) {
    //       wx.showModal({
    //         title: '提示',
    //         content: '微信授权登录失败，请重试！',
    //         success: function() {}
    //       })
    //       wx.setStorageSync('isAuthor', false);
    //     } else {
    //       that.data.memberType = res.data.result;
    //       that.data.hostName = res.data.hostName;
    //       wx.setStorageSync('hash', res.data.hash);
    //       wx.setStorageSync('hostName', res.data.hostName);
    //       wx.setStorageSync('memberId', res.data.memberId);
    //       wx.setStorageSync('memberType', res.data.result);
    //       wx.setStorageSync('isAuthor', true);
    //       wx.navigateBack({ delta: 1 })
    //     }
    //   },
    //   fail: function(err) {
    //     console.log(err)
    //   }
    // })
  },

  // 获取地理位置
  getLocation() {
    wechat.getLocation().then((res) => {
      let lng = res.longitude;
      let lat = res.latitude;
      this.getNearestOrganiz({ lng, lat });
    }).catch(() => {
      this.getNearestOrganiz();
    });
  },

  // 获取最近商场
  getNearestOrganiz(param) {
    console.log('author.js:获取最近商场,被调用了')
    let localId = wx.getStorageSync('organizeId');
    api.nearestOrganiz(param).then(res => {
      if (res.data.flag) {
        let id = res.data.data.organizeId;
        let name = res.data.data.organizeName;
        wx.setStorageSync('organizeId', id);
        wx.setStorageSync('organizeName', name);
      }
      this.jumpToTargetpage(localId);
    })
  },

  // 页面跳转
  jumpToTargetpage(localId) {
    let url = wx.getStorageSync('enterPage');
    let options = JSON.parse(wx.getStorageSync('enterOptions'));
    let params = wx.getStorageSync('enterParam');
    if (options.organizeId && options.organizeId != localId) {
      wx.setStorageSync('organizeId', options.organizeId);
      api.nearestOrganiz().then(res => {
        let name = res.data.data.organizeName;
        wx.setStorageSync('organizeName', name);
      })
    }
    if (!localId) {
      wx.reLaunch({
        url: '/' + url + '?' + params
      })
    }
  },

  // 取消授权，回到原页面
  handleCancel() {
    wx.navigateBack({ delta: 2 })
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad() {
    this.messageComponent = this.selectComponent('.messageTips');
    this.getMemberDescInfo();
    if (wx.getUserProfile) {
      this.setData({
        canIUseGetUserProfile: true
      })
    }
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {
    wechat.login().then(res => {
      if (!res.code) return;
      this.setData({
        code: res.code
      })
    })
  }
});
