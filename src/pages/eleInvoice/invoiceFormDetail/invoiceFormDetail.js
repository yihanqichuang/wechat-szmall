/**
 * 页面到底组件
 * @invoiceForm    电子发票表单
 */
import eleInvoice from '../../../api/eleInvoice'
import tip from '../../../utils/tip'
import { PHONE, IDCARD, EMAIL, AGE } from '../../../utils/regular.js'
Component({
    properties: {
        invoiceDetail: {
            type: Object,
            observer: function (val) {
                this.setData({ formData: val })
            }
        },
    },
    data: {
        formData: {}, // 表单数据
    },
    methods: {
    }
})
