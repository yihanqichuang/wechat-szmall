// 获取全局应用程序实例对象
// const app = getApp()
import eleInvoice from '../../../api/eleInvoice'
import TimeFormat from '../../../utils/timeFormat.js';
// 创建页面实例对象
Page({
  /**
   * 页面的初始数据
   */
  data: {
    query: {},
    orderNo: '',
    orderFee: '',
    orderTime: null,
    orderType: '',
    goodsName: '',
    invoiceType: '',
    isOneTime: true,  // 是否初次进入开票  为了区分进入开票选择抬头再次进入开票页
    orderTimeFormat:'',
    industryType: ''
  },
  // 跳转发票预览
  invoicePreview(e) {
    console.log(e, '保存的数据', e.detail)
    // 保存表单数据接口  调用成功 跳转预览
    wx.navigateTo({
      url: '../invoicePreview/invoicePreview'
    })
  },
  // 获取订单详情
  getOrderDetail() {
    eleInvoice.fetchOrderDetail({ ddh: this.data.ddh }).then(res => {
      if (!res.data.flag) {
        let msg = res.data.msg || '调用订单详情接口失败'
        this.messageComponent.applyActive(msg)
        return 
      }
      this.setData({
        jysj: res.data.jysj,
        ddje: res.data.ddje
      })
    })
  },

 
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(option) {
    // TODO: onLoad
    this.messageComponent = this.selectComponent('.message-tips');
    
    if (option.orderNo) {
      console.log("option.orderTime",option.orderTime)
      this.setData({ 
        orderNo: option.orderNo,
        orderFee: option.orderFee,
        orderTime: option.orderTime,
        orderTimeFormat: TimeFormat.dateFormatFilter(option.orderTime, 'yyyy-MM-dd hh:mm:ss'),
        industryType: option.industryType,
        memberId: option.memberId,
        shopOrderNo: option.shopOrderNo,
        shopId: option.shopId
      })
      // this.getOrderDetail()
    }

    if (option.title) {
      this.setData({
        query: option,
        isOneTime: false
      })
    }
    if (option.titleType) this.setData({ invoiceType: option.titleType })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {
    // TODO: onReady
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {
    // TODO: onShow
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {
    // TODO: onHide
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {
    // TODO: onUnload
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {
    // TODO: onPullDownRefresh
  }
})
