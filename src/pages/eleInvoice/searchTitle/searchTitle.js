// 获取全局应用程序实例对象
// const app = getApp()
import eleInvoice from '../../../api/eleInvoice'
// 创建页面实例对象
Page({
  /**
   * 页面的初始数据
   */
  data: {
    formData: {},
    companyList: [
      {
        title: 'lalala',
        taxNumber: 121456451321545
      },
      {
        title: 'lalalababa',
        taxNumber: 1214564321545
      }
    ],
    hasResult: false,
    showEmpty: false,
    canSave: false,
    operateType: '',
    titleType: 'ENTERPRISE'
  },
  handleInputVal(e) {
    let type = e.currentTarget.dataset.type
    let item = `formData.${type}`
    this.setData({
      [item]: e.detail.value
    })
  },
  // 查询抬头
  inputTitle(e) {
    this.handleInputVal(e)
    if (this.properties.invoiceType == 'PERSONAL') return
    if (e.detail.value) this.setData({ showEmpty: true })
    let params = {
      str: this.data.formData.title,
      titleType: this.data.invoiceType,
      pageNum: 1,
      pageSize: 10
    }
    console.log('查询抬头',e);
    eleInvoice.fetchEleTile(params).then(res => {
      console.log("hhh",res)
      if (!res.data.flag) {
        let msg = res.data.msg || '请求数据失败';
        this.messageComponent.applyActive(msg);
        return
      }
      this.setData({ companyList: [] })
      let data = res.data.data
      if (data && data.length > 0) {
        this.setData({
          companyList: [...this.data.companyList, ...data],
          hasResult: true
        })
      } else {
        this.setData({
          companyList: [],
          hasResult: false
        })
      }
    })
  },
  // 清空输入内容
  emptyCont() {
    this.setData({
      formData: {},
      hasResult: false,
      showEmpty: false,
      canSave: false
    })
  },
  // 选择抬头
  // selectTitle(e) {
  //   console.log(e.currentTarget.dataset.item)
  //   let item = e.currentTarget.dataset.item
  //   if (item) this.setData({ formData: item, hasResult: false })
  // },
  // 选择抬头
  selectTitle(e) {
    let params = {}
    if (this.data.titleType == 'ENTERPRISE') {
      params = {
        title: e.currentTarget.dataset.item.title,
        taxNumber: e.currentTarget.dataset.item.taxNumber,
        bankAccount: e.currentTarget.dataset.item.bankAccount,
        address: e.currentTarget.dataset.item.address,
        bankName: e.currentTarget.dataset.item.bankName,
        enterprisePhone: e.currentTarget.dataset.item.enterprisePhone
      }
    } else if (this.data.titleType == 'PERSONAL') {
      params = {
        phone: e.currentTarget.dataset.item.phone,
        title: e.currentTarget.dataset.item.title
      }
    }
    
    params.titleType = this.data.titleType
    params.orderNo = this.data.orderNo
    params.orderFee = this.data.orderFee
    params.orderType = this.data.orderType
    params.goodsName = this.data.goodsName
    params.orderTime = this.data.orderTime

    // 有的手机报错不兼容Object.entries  
    if (!Object.entries) {
      Object.entries = function(obj) {
        var ownProps = Object.keys(obj),
          i = ownProps.length,
          resArray = new Array(i); // preallocate the Array
        while (i--) {
          resArray[i] = [ownProps[i], obj[ownProps[i]]];
        }
        return resArray;
      }
    }
    wx.redirectTo({
      url: '../makeInvoices/makeInvoices?' + Object.entries(params).map(item => `${item[0]}=${item[1]}`).join('&')
    })
  },
  // 关闭查询结果
  closeResult() {
    this.setData({
      hasResult: false,
      showEmpty: false
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(option) {
    if (option.invoiceType) this.setData({ invoiceType: option.invoiceType })
    if (option.titleType) this.setData({ titleType: option.titleType })
    if (option.orderNo) this.setData({ orderNo: option.orderNo })
    if (option.orderFee) this.setData({ orderFee: option.orderFee })
    if (option.orderType) this.setData({ orderType: option.orderType })
    if (option.orderTime) this.setData({ orderTime: option.orderTime })
    if (option.goodsName) this.setData({ goodsName: option.goodsName })
    // TODO: onLoad
    // this.messageComponent = this.selectComponent('.message-tips');
    
    // if (option.orderNo) {
    //   this.setData({ 
    //     orderNo: option.orderNo,
    //     orderFee: option.orderFee,
    //     orderTime: option.orderTime,
    //     orderType: option.orderType,
    //     goodsName: option.goodsName
    //   })
    //   // this.getOrderDetail()
    // }

    // if (option.title) {
    //   this.setData({
    //     query: option,
    //     isOneTime: false
    //   })
    // }
    // if (option.titleType) this.setData({ invoiceType: option.titleType })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {
    // TODO: onReady
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {
    // TODO: onShow
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {
    // TODO: onHide
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {
    // TODO: onUnload
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {
    // TODO: onPullDownRefresh
  }
})
