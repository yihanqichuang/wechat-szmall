// 获取全局应用程序实例对象
// const app = getApp()
import timeFormat from '../../../utils/timeFormat.js'
import eleInvoice from '../../../api/eleInvoice'
import tip from '../../../utils/tip'
// 创建页面实例对象
Page({
  /**
   * 页面的初始数据
   */
  data: {
    invoiceData:{},
    nowDate:''
  },
  // 确认开票
  confirmInvoice() {
    this.getBillingInvoice();
  },

  // 开具发票
  getBillingInvoice() {
    eleInvoice.fetchBillingInvoice(this.data.invoiceData).then(res => {
      tip.loaded()
      if (!res.data.flag) {
        let msg = res.data.msg || '请求数据失败';
        this.messageComponent.applyActive(msg);
        return
      }
      let fpqqlsh = res.data.data
      wx.navigateTo({
        url: `../invoicedRecordDetail/invoicedRecordDetail?fpqqlsh=${fpqqlsh}`
      })
    })
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(option) {
    // TODO: onLoad
    this.messageComponent = this.selectComponent('.message-tips');
    const params = JSON.parse(option.params);
    console.log("跳转参数",params)
    this.setData({ invoiceData: params })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {
    // TODO: onReady
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {
    // TODO: onShow
    let now = new Date();
    let date = timeFormat.dateFormatFilter(now, 'yyyy年MM月dd日');
    this.setData({
      nowDate: date
    })
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {
    // TODO: onHide
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {
    // TODO: onUnload
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {
    // TODO: onPullDownRefresh
  }
})
