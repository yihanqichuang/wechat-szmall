// 获取全局应用程序实例对象
// const app = getApp()
import eleInvoice from '../../../api/eleInvoice'
// 创建页面实例对象
Page({
  /**
   * 页面的初始数据
   */
  data: {
    title: 'form',
    headList: [], // 抬头列表
    canEdit: 0,
    page: {
      pageNum: 1,
      pageSize: 100,
      total: 1
    },
    titleType: '',
    ddh: '',
    isShow: false
  },
  
  // 获取抬头列表
  einvoiceTitle(pageNum, type = '') {
    let params = {
      pageNum,
      pageSize: this.data.page.pageSize,
      titleType: type || ''
    }
    eleInvoice.fetchEleTile(params).then(res => {
      if (!res.data.flag) {
        let msg = res.data.msg || '请求数据失败';
        this.messageComponent.applyActive(msg);
        return
      }
      let data = res.data.data
      if (data) {
        this.setData({
          headList: [...data],
          page: res.data.page,
          isShow: false
        })
      } else {
        this.setData({
          headList: [],
          isShow: true
        })
      }
    })
  },
  // 加载更多
  loadMore(e) {
    this.einvoiceTitle(e.detail)
  },
  // 跳转发票抬头管理
  addHead() {
    wx.navigateTo({
      url: `/pages/eleInvoice/invoiceHeadManage/invoiceHeadManage?operateType=add`
    })
  },
  // 选择抬头、编辑抬头判断
  judgeFunc(e) {
    console.log(e, this.data.canEdit)
    let einvoiceTitleId = e.currentTarget.dataset.id
    if (this.data.canEdit == 1) { // 编辑抬头
      this.editTitle(einvoiceTitleId)
    } else { // 选择抬头
      this.selectTitle(e)
    }
  },
  // 编辑抬头
  editTitle(einvoiceTitleId) {
    wx.navigateTo({
      url: `/pages/eleInvoice/invoiceHeadManage/invoiceHeadManage?operateType=edit&einvoiceTitleId=${einvoiceTitleId}`
    })
  },
  // 选择抬头
  selectTitle(e) {
    if (this.data.canEdit == 1) return
    let params = {}
    if (this.data.titleType == 'ENTERPRISE') {
      params = {
        title: e.currentTarget.dataset.item.title,
        taxNumber: e.currentTarget.dataset.item.taxNumber,
        bankAccount: e.currentTarget.dataset.item.bankAccount,
        address: e.currentTarget.dataset.item.address,
        bankName: e.currentTarget.dataset.item.bankName,
        enterprisePhone: e.currentTarget.dataset.item.enterprisePhone,
        einvoiceTitleId: e.currentTarget.dataset.item.einvoiceTitleId,
      }
    } else if (this.data.titleType == 'PERSONAL') {
      params = {
        phone: e.currentTarget.dataset.item.phone,
        title: e.currentTarget.dataset.item.title,
        einvoiceTitleId: e.currentTarget.dataset.item.einvoiceTitleId
      }
    }
    
    // params.titleType = this.data.titleType
    // params.orderNo = this.data.orderNo
    // params.orderFee = this.data.orderFee
    // params.orderType = this.data.orderType
    // params.goodsName = this.data.goodsName
    // params.orderTime = this.data.orderTime
    // params.industryType = this.data.industryType

    //获取上一个页面的js里面的pages的所有信息
    const pages = getCurrentPages();
    const prevPage = pages[pages.length - 2];
    prevPage.setData({
      query: params
    })

    // 有的手机报错不兼容Object.entries  
    if (!Object.entries) {
      Object.entries = function(obj) {
        var ownProps = Object.keys(obj),
          i = ownProps.length,
          resArray = new Array(i); // preallocate the Array
        while (i--) {
          resArray[i] = [ownProps[i], obj[ownProps[i]]];
        }
        return resArray;
      }
    }
     
    wx.navigateBack({
      delta: 1  // 返回上一级页面
    })
    // wx.redirectTo({
    //   url: '../makeInvoices/makeInvoices?' + Object.entries(params).map(item => `${item[0]}=${item[1]}`).join('&')
    // })
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(option) {
    this.setData({ canEdit: option.hasEdit })
    if (option.hasEdit == 1) {
      wx.setNavigationBarTitle({
        title: '发票抬头管理'
      });
    }
    console.log(option.hasEdit, this.data.canEdit)
    if (option.titleType) this.setData({ titleType: option.titleType })
    // if (option.orderFee) this.setData({ orderFee: option.orderFee })
    // if (option.orderType) this.setData({ orderType: option.orderType })
    // if (option.orderTime) this.setData({ orderTime: option.orderTime })
    // if (option.goodsName) this.setData({ goodsName: option.goodsName })
    // if (option.industryType) this.setData({ industryType: option.industryType })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {
    // TODO: onReady
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {
    // TODO: onShow
    this.einvoiceTitle(1, this.data.titleType)
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {
    // TODO: onHide
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {
    // TODO: onUnload
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {
    // TODO: onPullDownRefresh
  }
})
