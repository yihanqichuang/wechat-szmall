/**
 * 页面到底组件
 * @invoiceForm    电子发票表单
 */
import eleInvoice from '../../../api/eleInvoice'
import tip from '../../../utils/tip'
import { PHONE, IDCARD, EMAIL, AGE } from '../../../utils/regular.js'
Component({
    properties: {
        invoiceType: {
            type: String,
            value: '',
            observer: function (val) {
                console.log(val)
                this.checkInput()
            }
        },
        operateType: { // 引用form操作类型
            type: String,
            value: ''
        },
        einvoiceTitleId: { // 抬头id
            type: Number,
            observer: function (val) {
                if (val) this.getTitleDetail(val)
            }
        },
        orderNo: { // 订单号
            type: String
        },
        orderFee: {  // 订单金额
            type: String
        },
        shopOrderNo: { // 订单类型
            type: String
        },
        orderTime: { // 订单时间
            type: String
        },
        memberId: {
            type: String
        },
        shopId: {
            type: String
        },
        industryType: { // 组织类型
            type: String
        },
        query: {
            type: Object,
            observer: function (val) {
                this.setData({ formData: val })
                this.checkInput()
            }
        },
        isOneTime: {
            type: Boolean
        }
    },
    data: {
        hasResult: false, // 是否显示模糊查询结果
        showEmpty: false,
        companyList: [], // 企业名称
        formData: {}, // 表单数据
        canSave: false,
        isDefault: true,
        deleteDialog: false
    },
    // 组件生命周期
    lifetimes: {
        attached() {
            // 在组件实例进入页面节点树时执行
            // this.init();
            this.messageComponent = this.selectComponent('.message-tips');
        },

        ready() {
            // 在组件在视图层布局完成后执行
            // this.init();
            if (this.properties.isOneTime) this.getDefault()
        },

        detached() {
            // 在组件实例被从页面节点树移除时执行
        }
    },

    pageLifetimes: {
        show() {
            // this.init();
            console.log(this.properties.operateType)
            if (this.properties.operateType == 'add' || this.properties.operateType == 'edit') return
            // this.getDefault()
        },

        hide() {
            // clearInterval(this.data.timer);
        }
    },
    methods: {
        // 是否有默认抬头
        getDefault() {
            eleInvoice.fetchDefaultTitle().then(res => {
                if (!res.data.flag) {
                    let msg = res.data.msg || '调用默认抬头接口失败'
                    this.messageComponent.applyActive(msg)
                }
                if (res.data.data) {
                    let { titleType, title, taxNumber, address, enterprisePhone, bankName, bankAccount, isDefault, phone, einvoiceTitleId } = res.data.data
                    let editData = { titleType, title, taxNumber, address, enterprisePhone, bankName, bankAccount, isDefault, phone, einvoiceTitleId }
                    this.setData({
                        formData: editData,
                        invoiceType: titleType
                    })
                }
                this.checkInput()
            })
        },
        selectType(e) {
            console.log(e)
            let type = e.currentTarget.dataset.value
            if (type == 'PERSONAL') this.setData({ sjm: '' })
            this.setData({
                invoiceType: type
            })
            this.checkInput()
            console.log(this.properties.invoiceType)
        },
        // 跳转 发票抬头
        selectInvoice() {
            // let titleType = this.properties.invoiceType
            // let orderNo = this.properties.orderNo
            // let orderFee = this.properties.orderFee
            // let orderType = this.properties.orderType
            // let goodsName = this.properties.goodsName
            // let orderTime = this.properties.orderTime
            // let industryType = this.properties.industryType
            wx.navigateTo({
                url: `../invoiceHead/invoiceHead?titleType=${this.properties.invoiceType}`
            })
        },
        // 验证
        validate() {
            if (!this.data.formData.title) {
                wx.showToast({
                    title: '请输入抬头',
                    duration: 2000,
                    icon: 'none'
                });
                return false
            } else if (this.properties.invoiceType == 'ENTERPRISE') {
                if (!this.data.formData.taxNumber) {
                    wx.showToast({
                        title: '请输入税号',
                        duration: 2000,
                        icon: 'none'
                    });
                    return false
                } else {
                    let taxNumber = this.data.formData.taxNumber
                    if (taxNumber.length < 6 || taxNumber.length > 20) {
                        wx.showToast({
                            title: '请输入正确的税号',
                            duration: 2000,
                            icon: 'none'
                        });
                        return false
                    }
                }
            }
            return true;
        },
        // 提交form
        formSubmit(e) {
            if (!this.validate()) return
            let titleType = this.properties.invoiceType
            let params = e.detail.value
            params.titleType = titleType
            if (this.properties.operateType != 'add' && this.properties.operateType != 'edit') {
                this.getBillingInvoice(params)
                // this.toInvoicePreview()
            } else {
                let isDefault = this.data.isDefault ? 'Y' : 'N'
                params.isDefault = isDefault
                if (this.properties.einvoiceTitleId) {
                    params.einvoiceTitleId = this.properties.einvoiceTitleId
                    this.updateTitle(params)
                } else {
                    this.addTitle(params)
                }
            }
        },
        // 跳转开票预览页
        toInvoicePreview() {
            let params = {};
            params.title = this.data.formData.title
            params.titleType = this.properties.invoiceType
            params.taxNumber = params.titleType == 'ENTERPRISE' ? this.data.formData.taxNumber : ''
            params.orderNo = this.properties.orderNo
            params.orderFee = this.properties.orderFee
            params.orderType = this.properties.orderType
            // params.orderTime = this.properties.orderTime
            params.goodsName = this.properties.goodsName
            params.enterprisePhone = params.titleType == 'ENTERPRISE' ? this.data.formData.enterprisePhone : ''
            params.address = params.titleType == 'ENTERPRISE' ? this.data.formData.address : ''
            params.bankName = params.titleType == 'ENTERPRISE' ? this.data.formData.bankName : ''
            params.bankAccount = params.titleType == 'ENTERPRISE' ? this.data.formData.bankAccount : ''
            params.phone = this.data.formData.phone
            params.industryType = this.properties.industryType


            wx.navigateTo({
                url: `../invoicePreview/invoicePreview?params=${JSON.stringify(params)}`
            })
        },
        // 开具发票
        getBillingInvoice(params) {
            if (!EMAIL.test(this.data.formData.email)) {
                wx.showToast({
                    title: "请填写正确的邮箱地址",
                    icon: "none"
                })
                return false;
            }
            params.titleId = this.data.formData.einvoiceTitleId;
            params.dpddh = this.properties.shopOrderNo;
            params.ddh = this.properties.orderNo;
            params.memberId = this.properties.memberId;
            params.shopId = this.properties.shopId;
            params.einvoiceAmount = this.properties.orderFee;
            params.email = this.data.formData.email;
            console.log(params, this.data.formData)
            tip.confirm(this.data.formData.email, '', '请确认收件邮箱地址', '取消', '确认').then(() => {
                tip.loading('提交中')
                eleInvoice.fetchBillingInvoice(params).then(res => {
                    tip.loaded()
                    if (!res.data.flag) {
                        let msg = res.data.msg || '请求数据失败';
                        this.messageComponent.applyActive(msg);
                        return
                    }
                    wx.showToast({
                        title: '开票成功'
                    })
                    wx.navigateBack({
                        delta: 2
                    });
                    // let fpqqlsh = res.data.data
                    // wx.navigateTo({
                    //   url: `../invoicePreview/invoicePreview?title=${title}&taxNumber=${taxNumber}&fpqqlsh=${fpqqlsh}`
                    // })
                })
            }).catch(() => { })

        },
        // 新增保存抬头
        addTitle(params) {
            eleInvoice.fetchSaveTitle(params).then(res => {
                if (!res.data.flag) {
                    let msg = res.data.msg || '请求数据失败';
                    this.messageComponent.applyActive(msg);
                    return
                }
                wx.showToast({
                    title: '保存成功'
                })
                // 从抬头管理进来
                wx.navigateBack({
                    delta: 1 // 回退前 delta(默认为1) 页面
                })
            })
        },
        // 编辑保存抬头
        updateTitle(params) {
            eleInvoice.fetchUpdateTitle(params).then(res => {
                if (!res.data.flag) {
                    let msg = res.data.msg || '请求数据失败';
                    this.messageComponent.applyActive(msg);
                    return
                }
                wx.showToast({
                    title: '保存成功'
                })
                // 从抬头管理进来
                wx.navigateBack({
                    delta: 1 // 回退前 delta(默认为1) 页面
                })
            })
        },
        handleSwitch(e) {
            this.setData({
                isDefault: e.detail.value
            });
        },
        handleInputVal(e) {
            let type = e.currentTarget.dataset.type
            let item = `formData.${type}`
            this.setData({
                [item]: e.detail.value
            })
        },
        inputTitle1(e) {
            console.log(e, "456", this.data);
            let invoiceType = this.data.invoiceType;
            let titleType = this.properties.invoiceType
            let orderNo = this.properties.orderNo
            let orderFee = this.properties.orderFee
            let orderType = this.properties.orderType
            let goodsName = this.properties.goodsName
            let orderTime = this.properties.orderTime
            wx.navigateTo({
                url: `../searchTitle/searchTitle?invoiceType=${invoiceType}&titleType=${titleType}&hasEdit=0&orderNo=${orderNo}&orderFee=${orderFee}&orderType=${orderType}&goodsName=${goodsName}&orderTime=${orderTime}`
            })
        },
        // 查询抬头
        inputTitle(e) {
            this.handleInputVal(e)
            if (this.properties.invoiceType == 'PERSONAL') return
            if (e.detail.value) this.setData({ showEmpty: true })
            // let params = {
            //   str: this.data.formData.title,
            //   pageNum: 1,
            //   pageSize: 10
            // }
            // // if (e.detail.value.length >= 4) {
            // eleInvoice.fetchCompany(params).then(res => {
            //   if (!res.data.flag) {
            //     let msg = res.data.msg || '请求数据失败';
            //     this.messageComponent.applyActive(msg);
            //     return
            //   }
            //   this.setData({ companyList: [] })
            //   let data = res.data.data
            //   if (data && data.length > 0) {
            //     this.setData({
            //       companyList: [...this.data.companyList, ...data],
            //       hasResult: true
            //     })
            //   } else {
            //     this.setData({
            //       companyList: [],
            //       hasResult: false
            //     })
            //   }
            // })
            // }
        },
        // 选择抬头
        selectTitle(e) {
            console.log(e.currentTarget.dataset.item)
            let item = e.currentTarget.dataset.item
            if (item) this.setData({ formData: item, hasResult: false })
            this.checkInput()
        },
        // 关闭查询结果
        closeResult() {
            this.setData({
                hasResult: false,
                showEmpty: false
            })
        },
        // 清空输入内容
        emptyCont() {
            this.setData({
                formData: {},
                hasResult: false,
                showEmpty: false,
                canSave: false
            })
        },
        // 失去焦点 清空none
        handleInputBlur() {
            this.checkInput()
            setTimeout(() => {
                this.setData({
                    showEmpty: false
                })
            }, 500);
        },
        // 获取焦点
        handleInputFocus() {
            this.setData({
                showEmpty: true
            })
        },
        // 检查必填是否输完
        checkInput() {
            console.log("this.data.formData", this.data.formData)
            let companyBool = this.properties.invoiceType == 'ENTERPRISE' && this.data.formData.title && this.data.formData.taxNumber && this.data.formData.email
            let personBool = this.properties.invoiceType == 'PERSONAL' && this.data.formData.title && this.data.formData.email
            if (this.properties.operateType == 'add' || this.properties.operateType == 'edit') {
                companyBool = this.properties.invoiceType == 'ENTERPRISE' && this.data.formData.title && this.data.formData.taxNumber
                personBool = this.properties.invoiceType == 'PERSONAL' && this.data.formData.title
            }
            console.log(companyBool, personBool)
            if (companyBool || personBool) {
                this.setData({
                    canSave: true
                })
            } else {
                this.setData({
                    canSave: false
                })
            }
        },
        // 抬头详情
        getTitleDetail(id) {
            eleInvoice.fetchTitleDetail({ einvoiceTitleId: id }).then(res => {
                let { titleType, title, taxNumber, address, enterprisePhone, bankName, bankAccount, isDefault, phone, einvoiceTitleId } = res.data.data
                let editData = { titleType, title, taxNumber, address, enterprisePhone, bankName, bankAccount, isDefault, phone, einvoiceTitleId }
                this.setData({
                    formData: editData,
                    isDefault: isDefault != 'N',
                    invoiceType: titleType
                })
                this.checkInput()
            })
        },
        deleteHead() {
            this.setData({
                deleteDialog: true
            })
        },
        cancelDelete() {
            this.setData({
                deleteDialog: false
            })
        },
        confirmDelete() {
            eleInvoice.fetchDelete({ einvoiceTitleId: this.properties.einvoiceTitleId }).then(res => {
                if (!res.data.flag) {
                    let msg = res.data.msg || '调用删除抬头接口失败'
                    this.messageComponent.applyActive(msg)
                }
                wx.showToast({
                    title: '删除成功'
                })
                setTimeout(() => {
                    // 从抬头管理进来
                    wx.navigateBack({
                        delta: 1 // 回退前 delta(默认为1) 页面
                    })
                }, 500);
            })
        }
    }
})
