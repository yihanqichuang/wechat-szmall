// 获取全局应用程序实例对象
// const app = getApp()
import eleInvoice from '../../../api/eleInvoice'
// 创建页面实例对象
Page({
  /**
   * 页面的初始数据
   */
  data: {
    // collaspse: false, // 默认收起
    orderList: [
      {
        orderNo: 12315645,
        collaspse: true,
        orderTime: '2020/12/20 12:00:20',
        djmxList: [
          {
            ddh: 5451213154415612,
            spmc: '商品名称商品名称商品名称',
            spdj: 129.00,
            spsl: 10,
            dw: '斤',
            spzj: 9430.00
          },
          {
            ddh: 5451213154415612,
            spmc: '商品名称商品名称商品名称',
            spdj: 129.00,
            spsl: 10,
            dw: '斤',
            spzj: 9430.00
          },
          {
            ddh: 5451213154415612,
            spmc: '商品名称商品名称商品名称',
            spdj: 129.00,
            spsl: 10,
            dw: '斤',
            spzj: 9430.00
          }
        ],
        goodsName: '哈哈哈哈',
        orderFee: 180000.00
      }
    ],
    page: {
      pageNum: 1,
      pageSize: 10,
      total: 1,
      pages: 1 // whh临时添加
    }
  },
  // 获取订单列表
  getListOrder(pageNum) {
    let params = { ISINVOICE: '0', pageNum, pageSize: this.data.page.pageSize }
    eleInvoice.fetchOrderList(params).then(res => {
      if (!res.data.flag) {
        let msg = res.data.msg || '请求数据失败';
        this.messageComponent.applyActive(msg);
        return
      }
      if (pageNum === 1) this.setData({ orderList: [] });
      let data = res.data.data
      if (data && data.length > 0) {
        this.setData({
          orderList: [...this.data.orderList, ...data]
        })
      }
      this.setData({
        page: res.data.page
      }) 
    })
  },
  // 查询订单
  searchOrder() {
    wx.navigateTo({
      url: '../searchOrder/searchOrder'
    })
  },
  // 展开收起
  collaspseOrder(e) {
    let index = e.currentTarget.dataset.index
    let collaspse = e.currentTarget.dataset.collaspse
    let orderCollaspse = 'orderList[' + index + '].collaspse'
    this.setData({
      [orderCollaspse]: !collaspse
    })
  },
  // 去开票
  makeInvoice(e) {
    // this.messageComponent.applyActive('未对接，暂时还不能开票哦！');
    let orderNo = e.currentTarget.dataset.item.orderNo
    let orderTime = e.currentTarget.dataset.item.orderTime
    let orderFee = e.currentTarget.dataset.item.orderFee
    let orderType = e.currentTarget.dataset.item.orderType
    let goodsName = e.currentTarget.dataset.item.goodsName
    wx.navigateTo({
      url: `/pages/eleInvoice/makeInvoices/makeInvoices?orderNo=${orderNo}&orderFee=${orderFee}&orderTime=${orderTime}&orderType=${orderType}&goodsName=${goodsName}`
    })
  },
  // 加载更多
  loadMore(e) {
    console.log(e.detail)
    // this.getListOrder(e.detail)
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad() {
    // TODO: onLoad
    this.messageComponent = this.selectComponent('.message-tips');
    // this.getListOrder(1)
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {
    // TODO: onReady
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {
    // TODO: onShow
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {
    // TODO: onHide
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {
    // TODO: onUnload
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {
    // TODO: onPullDownRefresh
  }
})
