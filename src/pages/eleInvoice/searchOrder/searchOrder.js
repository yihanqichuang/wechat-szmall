// 获取全局应用程序实例对象
// const app = getApp()
import eleInvoice from '../../../api/eleInvoice'
// 创建页面实例对象
Page({
  /**
   * 页面的初始数据
   */
  data: {
    tabs: [
      {
        name: '扫码录入',
        type: 'scanCode'
      },
      {
        name: '手动录入',
        type: 'handInput'
      }
    ],
    navSlider: {
      width: '40rpx',
      left: '167.5rpx',
      position: ['167.5rpx', '542.5rpx']
    },
    activeIndex: 0,
    inputALL: false,
    page: {
      pageNum: 1,
      pageSize: 10
    },
    orderList: [
      {
        ddh: 213145612,
        ddje: 456.00
      }
    ],
    noData: false,
    formData: {}
  },
  // 扫码查询
  scanCode() {
    let memberType = wx.getStorageSync('memberType');
    if (memberType != 'MEMBER') {
      wx.navigateTo({
        url: '/pages/login/login'
      })
    } else {
      // 扫码出来的是网址 需要截取订单号和随机码
      // let str = 'http://einvoice.hfbdjt.com/Wrapper/scan/jump.do?orderno=订单号&sjm=随机码'
      // let orderno = this.str2obj(str).orderno
      // let sjm = this.str2obj(str).sjm
      // 二维码是地址   一维码是订单号
      wx.scanCode({
        scanType: ['barCode', 'qrCode'],
        success: (res) => {
          console.log(res.result)
          if (res.result) {
            if (res.result.indexOf('http') != -1) { // 地址
              let orderStr = this.str2obj(res.result).state
              let orderno = orderStr.substring(0, orderStr.length - 4)
              let sjm = orderStr.substring(orderStr.length - 4)
              this.getOrderList(orderno, sjm)
            } else { // 订单号
              let barCodeStr = res.result
              barCodeStr = barCodeStr.replace(/\b(0+)/gi, '')
              let barCodeStr1 = barCodeStr.substring(0, 7)
              let barCodeStr2 = barCodeStr.substring(7).replace(/\b(0+)/gi, '')
              this.getOrderList(barCodeStr1 + barCodeStr2)
            }
          }
        },
        fail: () => {
          wx.showToast({
            title: '扫码失败'
          })
        }
      });
    }
  },
  // 获取扫码订单和随机码
  str2obj(str) {
    let obj = {}, arr = str.split('?')[2].split('&').map(item => item.split('='))
    arr.forEach(item => obj[item[0]] = item[1])
    return obj
  },
  inputTitle(e) {
    let type = e.currentTarget.dataset.type
    let item = `formData.${type}`
    this.setData({
      [item]: e.detail.value
    })
    this.checkInput()
  },
  checkInput() {
    if (this.data.formData.orderno && this.data.formData.sjm) {
      this.setData({
        inputAll: !this.data.inputALL
      })
    }
  },
  // 获取订单信息
  getOrderList(orderno, sjm = '') {
    let params = { 
      ISINVOICE: '0', 
      pageNum: this.data.page.pageNum, 
      pageSize: this.data.page.pageSize,
      orderno,
      sjm
    }
    eleInvoice.fetchOrderList(params).then(res => {
      if (!res.data.flag) {
        let msg = res.data.msg || '请求数据失败';
        this.messageComponent.applyActive(msg);
        return
      }
      let data = res.data.data
      if (data && data.length > 0) {
        this.setData({
          orderList: [...data],
          noData: false
        })
      } else {
        this.setData({
          orderList: [],
          noData: true
        })
      }
      this.setData({
        page: res.data.page
      })
    })
  },
  // 查询订单
  searchOrder() {
    this.getOrderList(this.data.formData.orderno, this.data.formData.sjm)
  },
  // 去开票
  makeInvoice(e) {
    let ddh = e.currentTarget.dataset.item.ddh
    let sjm = e.currentTarget.dataset.item.sjm
    let jysj = e.currentTarget.dataset.item.jysj
    let ddje = e.currentTarget.dataset.item.ddje
    wx.navigateTo({
      url: `/pages/eleInvoice/makeInvoices/makeInvoices?ddh=${ddh}&sjm=${sjm}&jysj=${jysj}&ddje=${ddje}`
    })
  },
  // 改变 swiper 手势滑动
  swiperChange(event) {
    let current = event.detail.current;
    this.setData({
      activeIndex: current,
      orderList: [],
      noData: false,
      formData: {},
      inputAll: false
    })
    this.sliderPosition(current);
  },
  // 切换类型
  changeIndex(data) {
    let currentIndex = data.detail
    this.setData({
      activeIndex: currentIndex,
      orderList: [],
      noData: false,
      formData: {},
      inputAll: false
    })
    this.sliderPosition(currentIndex)
  },
  // nav-slider 的位置
  sliderPosition(index) {
    this.setData({
      'navSlider.left': this.data.navSlider.position[index]
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad() {
    this.messageComponent = this.selectComponent('.message-tips');
  }
})
