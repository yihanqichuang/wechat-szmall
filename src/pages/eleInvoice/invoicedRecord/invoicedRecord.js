// 获取全局应用程序实例对象
// const app = getApp()
import eleInvoice from '../../../api/eleInvoice'
// 创建页面实例对象
Page({
  /**
   * 页面的初始数据
   */
  data: {
    recordList: [
      // {
      //   orderTime: '2019/03/10 12:00',
      //   orderNo: 13234234987667,
      //   orderFee: 359.00
      // }
    ],
    page: {
      pageNum: 1,
      pageSize: 10,
      total: 1,
      pages: 1
    }
  },
  // 获取订单列表
  getOrderList(pageNum) {
    let params = {
      ISINVOICE: '1',
      pageNum,
      pageSize: this.data.page.pageSize
    }
    eleInvoice.fetchEinvoiceOrderList(params).then(res => {
      if (!res.data.flag) {
        let msg = res.data.msg || '请求数据失败';
        this.messageComponent.applyActive(msg);
        return
      }
      let data = res.data.data.records
      if (pageNum === 1) this.setData({ recordList: [] });
      if (data && data.length > 0) {
        // data.forEach(item => {
        //   // 开票渠道标识：0 小程序开票，1 公众号开票，2 其他
        //   item.kply = this.judgeType(item.kply)
        // })
        this.setData({
          recordList: [...this.data.recordList, ...data]
        })
      }
      this.setData({ page: {
        pageNum: res.data.data.current,
        pageSize: 10,
        total: res.data.data.total,
        pages: res.data.data.pages
      }})
    })
  },
  judgeType(type) {
    let obj = {
      '1': '小程序',
      '0': '公众号',
      '2': '其他'
    }
    return obj[type]
  },
  // 加载更多
  loadMore(e) {
    this.getOrderList(e.detail)
  },
  // 跳转开票详情
  goDetail(e) {
    console.log(e)
    let dpddh = e.currentTarget.dataset.dpddh
    wx.navigateTo({
      url: `../invoicedRecordDetail/invoicedRecordDetail?dpddh=${dpddh}`
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad() {
    // TODO: onLoad
    this.getOrderList(1)
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {
    // TODO: onReady
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {
    // TODO: onShow
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {
    // TODO: onHide
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {
    // TODO: onUnload
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {
    // TODO: onPullDownRefresh
  }
})
