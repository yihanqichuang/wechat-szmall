// 获取全局应用程序实例对象
// const app = getApp()

// 创建页面实例对象
Page({
  /**
   * 页面的初始数据
   */
  data: {
    operateType: '',
    einvoiceTitleId: ''
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(option) {
    // TODO: onLoad
    console.log(option)
    if (option.operateType) {
      this.setData({
        operateType: option.operateType
      })
      if (option.operateType == 'edit') {
        wx.setNavigationBarTitle({
          title: '编辑发票抬头'
        });
      } else if (option.operateType == 'add') {
        wx.setNavigationBarTitle({
          title: '新增发票抬头'
        });
      }
    }

    if (option.einvoiceTitleId) {
      this.setData({
        einvoiceTitleId: option.einvoiceTitleId
      })
    }
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {
    // TODO: onReady
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {
    // TODO: onShow
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {
    // TODO: onHide
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {
    // TODO: onUnload
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {
    // TODO: onPullDownRefresh
  }
})
