import categoryApi from '../../api/category.js'
import debounce from '../../utils/debounce.js'

Page({
  data: {
    isSearchPage: false,
    searchPageData: [],
    mainCategory: [],  // 一级品类列表
    subCategory: [],   // 一级分类下的二、三级品类列表
    menuHeight: '',    // 菜单高度
    currentTab: 0,     // 预设当前项的值
    scrollTop: 0       // tab标题的滚动条位置
  },
  
  // 点击标题切换当前页时改变样式
  switchNav: function(e) {
    let cur = e.currentTarget.dataset.current;
    if (this.data.currentTab == cur) {
      return false;
    } else {
      // 加载当前分类下的数据
      this.querySubCatagoryByNav(cur, null);
      // 滚动到页面顶端
      wx.pageScrollTo({
        scrollTop: 0
      })
      this.setData({
        currentTab: cur
      })
    }
  },
  // 跳转单品列表页-带分类
  toGoodsListPage(e) {
    let parentName = e.currentTarget.dataset.key;
    let item = e.currentTarget.dataset.item;
    wx.navigateTo({
      url: '../discover/specialList/specialList?categoryId=' + item.categoryId + '&parentCategoryId=' + item.parentCategoryId + '&categoryName=' + parentName
    })
  },


  handleFocus() {
    this.setData({
      isSearchPage: true
    })
  },

  /**
   * 进入页面初始化数据
   */
  initCatagoryInfo(organizeId, categoryId) {
    let params = {
      level: 1,
      parentCategoryId: 0
    };

    if (organizeId) {
      params.organizeId = organizeId;
    }
    categoryApi.fetchCategoryList(params).then(res => {
      let data = res.data
      if (!data.flag) return this.messageComponent.applyActive(data.msg || '获取品类数据失败')
      this.setData({
        mainCategory: data.data,
        currentTab: categoryId
      });

      if (!organizeId) {
        this.setData({
          currentTab: data.data[0].categoryId
        });
        // 默认加载第一个一级类目下品类数据
        this.querySubCatagoryByNav(data.data[0].categoryId, null);
      }
    })
  },

  /**
   * 根据导航分类搜索下级所有分类
   */
  querySubCatagoryByNav(catagoryId, locateCategoryId) {
    let params = {
      parentCategoryId: catagoryId
    };

    if (locateCategoryId) {
      params.locateCategoryId = locateCategoryId
    }

    categoryApi.fetchSubCategory(params).then(res => {
      let data = res.data
      if (!data.flag) return this.messageComponent.applyActive(data.msg || '获取品类数据失败')
      this.setData({
        subCategory: data.data
      });
    })
  },

  // 输入
  handleSearchPageInput(e) {
    debounce.debounceDelay(this.handleSearchPageInputApi(e.detail.value), 500)
  },

  // 输入API
  handleSearchPageInputApi(name = '') {
    let params = {
      level: 3,
      name: name
    }
    categoryApi.fetchCategoryList(params).then(res => {
      let data = res.data
      if (!data.flag) return this.messageComponent.applyActive(data.msg || '获取品类数据失败')
      this.setData({
        searchPageData: data.data
      })
    })
  },


  // 点击搜索的某个结果
  handleSearchItem(e) {
    let item = e.currentTarget.dataset.item;
    console.log(item);
    let { categoryId, parentCategoryId } = item
    wx.navigateTo({
      url: `../discover/specialList/specialList?categoryId=${categoryId}&parentCategoryId=${parentCategoryId}`
    })
  },


  // 点击取消
  handleCancelSearch() {
    this.setData({
      isSearchPage: false
    })
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(opt) {
    if (opt) {
      let organizeId = null;
      let categoryId = null;
      let locateCategoryId = null;
      if (opt.organizeId) {
        organizeId = opt.organizeId
      }

      if (opt.categoryId) {
        categoryId = opt.categoryId
      }

      if (opt.locateCategoryId) {
        locateCategoryId = opt.locateCategoryId
      }
      // 初始化加载一级分类
      this.initCatagoryInfo(organizeId, categoryId);     
      // 加载当前分类下的数据
      this.querySubCatagoryByNav(categoryId, locateCategoryId);
      // 滚动到页面顶端
      wx.pageScrollTo({
        scrollTop: 0
      })

      this.setData({
        currentTab: categoryId
      })
    }
    wx.getSystemInfo({
      success: (res) => {
        this.setData({
          menuHeight: res.windowHeight - res.windowWidth / 750 * 92
        });
      }
    });
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {
    // TODO: onReady
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {  
    this.setData({
      isSearchPage: false
    })
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {
    // TODO: onHide
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {
    // TODO: onUnload
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {
    // TODO: onPullDownRefresh
  }
})
