// 获取全局应用程序实例对象
// const app = getApp()
import discovery from '../../../api/discovery.js'

// 创建页面实例对象
Page({
  /**
   * 页面的初始数据
   */
  data: {
    imgUrls: [],
    list: [],
    page: {
      pageNum: 1,
      pageSize: 10,
      total: 1
    },
    interval: 3000  // 轮播时间间隔
  },

  // 获取banner列表
  getBannerList() {
    discovery.advertList().then(res => {
      if (res.data.flag) {
        this.setData({
          imgUrls: res.data.data,
          interval: res.data.interval ? res.data.interval : 3000
        })
      } else {
        this.messageComponent.applyActive(res.data.msg || '获取banner列表异常~');
      }
    }).catch((err) => {
      console.log(err)
    })
  },

  // 获取商户列表
  getShopList(pageNum) {
    let params = {
      pageNum: pageNum
    };
    discovery.list(params).then(res => {
      if (res.data.flag) {
        if (pageNum === 1) this.setData({ list: [] });
        res.data.data && res.data.data.length && res.data.data.forEach(item => {
          item.activityPic = item.activityPic.split(',')[0]
        });
        this.setData({
          list: [...this.data.list, ...res.data.data],
          page: res.data.page
        })
      } else {
        this.messageComponent.applyActive(res.data.msg || '获取数据异常~');
      }
    }).catch((err) => {
      console.log(err)
    })
  },

  // 分页
  loadMore(e) {
    this.getShopList(e.detail)
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad() {
    this.messageComponent = this.selectComponent('.messageTips');
    this.getBannerList();
    this.getShopList(1);
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {
    // TODO: onReady
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {
    // TODO: onShow
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {
    // TODO: onHide
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {
    // TODO: onUnload
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {
    // TODO: onPullDownRefresh
  },

  /**
   * 页面相关分享
   */
  onShareAppMessage() {
    
  }
});
