// 获取全局应用程序实例对象
// const app = getApp()
import discovery from '../../../api/discovery.js';
import Tips from '../../../utils/tip.js';

// 创建页面实例对象
Page({
  /**
   * 页面的初始数据
   */
  data: {
    id: '',
    list: [],
    item: {},
    page: {
      pageNum: 1,
      pageSize: 10,
      total: 1
    },
    actionSheetHidden: true,
    form: {},
    showCenterDialog: false,
    replyDescriptionReply: '',
    replyDescription: '',
    index: -1,
    disabled: true,
    tips: false
  },

  // 获取列表
  getList() {
    let params = {
      id: this.data.id
    };
    discovery.evaluationDetailList(params).then(res => {
      if (res.data.flag) {
        if (res.data.data.evaluation.imgPath) res.data.data.evaluation.imgUrls = res.data.data.evaluation.imgPath.split(',');
        this.setData({
          list: [...res.data.data.listReply],
          item: res.data.data.evaluation,
          disabled: false
        })
      } else {
        this.messageComponent.applyActive(res.data.msg);
        if (res.data.msg == '该条评论不存在') {
          this.setData({
            tips: true
          })
        }
      }
    })
  },

  // 分页
  loadMore(e) {
    this.getList(e.detail);
  },

  // 长按
  handleLongPress(e) {
    let item = e.currentTarget.dataset.item;
    let index = e.currentTarget.dataset.index;
    this.setData({
      actionSheetHidden: !this.data.actionSheetHidden,
      form: item,
      index: index
    })
  },

  // 监听点击事件
  listenerActionSheet() {
    this.setData({
      actionSheetHidden: !this.data.actionSheetHidden
    })
  },

  // 回复
  handleReply() {
    let item = this.data.form;
    this.setData({
      actionSheetHidden: !this.data.actionSheetHidden,
      showCenterDialog: !this.data.showCenterDialog,
      placeholder: `回复${item.nickName}`
    });
  },

  // 输入
  handleInput(e) {
    this.setData({
      replyDescription: e.detail.value
    })
  },

  // 回复评论
  handleInputReply(e) {
    this.setData({
      replyDescriptionReply: e.detail.value
    })
  },

  // 发送回复（针对某一条回复）
  handleSend() {
    let replyDescription = this.data.replyDescription;
    if (!replyDescription) {
      this.messageComponent.applyActive('请填写内容哦~');
      return false;
    }
    let item = this.data.form;
    let list = this.data.list;
    let params = {
      evaluationId: item.evaluationId,
      replyDescription: replyDescription,
      fromAccountId: item.toAccountId,
      toAccountId: item.fromAccountId,
      acceptType: item.replyType
    };
    discovery.saveEvaluationReply(params).then(res => {
      if (res.data.flag) {
        Tips.success('回复成功~').then(() => {
          list.push(res.data.data);
          this.setData({
            list: list,
            showCenterDialog: !this.data.showCenterDialog,
            replyDescription: ''
          })
        });
      } else {
        this.messageComponent.applyActive(res.data.msg)
      }
    })
  },

  // 回复（针对评价回复）
  handleSendReply() {
    let replyDescription = this.data.replyDescriptionReply;
    if (!replyDescription) {
      this.messageComponent.applyActive('请填写内容哦~');
      return false;
    }
    let item = this.data.item;
    let list = this.data.list;
    let params = {
      evaluationId: item.evaluationId,
      replyDescription: replyDescription,
      toAccountId: item.memberId,
      acceptType: 'MEMBER'
    };
    discovery.saveEvaluationReply(params).then(res => {
      if (res.data.flag) {
        Tips.success('回复成功~').then(() => {
          list.push(res.data.data);
          this.setData({
            list: list,
            replyDescriptionReply: ''
          })
        });
      } else {
        this.messageComponent.applyActive(res.data.msg)
      }
    })
  },

  // 删除某一条回复
  handleDelete() {
    this.setData({
      actionSheetHidden: !this.data.actionSheetHidden
    });
    let id = this.data.form.evaluationReplyId;
    let list = this.data.list;
    let index = this.data.index;
    let params = {
      id: id,
      type: '2'
    };
    Tips.confirm('该操作将删除此条评论，是否继续该操作？', {}, '温馨提示', '取消', '确定').then(() => {
      discovery.deleteEvaluation(params).then(res => {
        if (res.data.flag) {
          Tips.success('删除成功~').then(() => {
            list.splice(index, 1);
            this.setData({
              list: list
            })
          })
        } else {
          this.messageComponent.applyActive(res.data.msg)
        }
      })
    });
  },

  // 删除品论（整一条+回复）
  handleDeleteAll() {
    let id = this.data.item.evaluationId;
    let params = {
      id: id,
      type: '1'
    };
    Tips.confirm('该操作将删除所有评论和回复，是否继续该操作？', {}, '温馨提示', '取消', '确定').then(() => {
      discovery.deleteEvaluation(params).then(res => {
        if (res.data.flag) {
          Tips.success('删除成功~').then(() => {
            wx.navigateBack({
              delta: 1
            })
          })
        } else {
          this.messageComponent.applyActive(res.data.msg)
        }
      })
    });
  },

  // 展示图片
  handlePreviewImage(e) {
    let idx = e.target.dataset.idx;
    let that = this;
    wx.previewImage({
      current: that.data.item.imgUrls[idx], // 当前显示图片的http链接
      urls: that.data.item.imgUrls
    })
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(option) {
    this.messageComponent = this.selectComponent('.messageTips');
    this.setData({
      id: option.id
    });
    this.getList();
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {
    // TODO: onReady
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {
    // TODO: onHide
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {
    // TODO: onUnload
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {
    // TODO: onPullDownRefresh
  }
});
