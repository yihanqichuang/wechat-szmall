// 获取全局应用程序实例对象
const app = getApp();
import tip from '../../../utils/tip'
import discovery from '../../../api/discovery.js';

// 创建页面实例对象
Page({
  /**
   * 页面的初始数据
   */
  data: {
    messageComponent: '',
    picNum: 6,
    score: 0,
    content: '',
    shopId: '',
    id: '',
    imgList: [],
    disabled: false
  },

  // 打分
  handleSelectValue(e) {
    let score = e.detail.value;
    this.setData({
      score: score
    })
  },

  // 输入评论
  handleContent(e) {
    this.setData({
      content: e.detail.value
    })
  },

  handleSubmit() {
    if (!this.data.score) {
      this.messageComponent.applyActive('请打分哦~');
      return false;
    }

    if (!this.data.content) {
      this.messageComponent.applyActive('请填写评价内容~');
      return false;
    }

    tip.loading();
    setTimeout(() => this.submit(), 500)
  },

  // 保存评论接口
  submit() {
    if (this.data.disabled) return;
    let params = {
      discoveryActivityId: this.data.id,
      shopId: this.data.shopId,
      evaluationLevel: this.data.score,
      description: this.data.content,
      imgPath: this.data.imgList.join(',')
    };
    discovery.saveEvaluation(params).then(res => {
      tip.loaded();
      if (res.data.flag) {
        tip.success('保存成功~').then(() => {
          if (this.data.isList) {
            wx.setStorageSync('evaluateList', this.data.isList)
          }
          this.setData({
            disabled: true
          });
          wx.navigateBack({
            delta: 1
          })
        })
      } else {
        this.messageComponent.applyActive(res.data.msg || '保存失败~');
      }
    })
  },

  // 上传图片(保存在阿里服务器)
  handleUpLoad() {
    let that = this;
    let hostNames = wx.getStorageSync('hostName') || app.data.hostName;
    wx.chooseImage({
      count: that.data.picNum - that.data.imgList.length,
      sizeType: ['original', 'compressed'],
      sourceType: ['album', 'camera'],
      success: function(res) {
        console.log(res);
        res.tempFilePaths.forEach((item) => {
          wx.uploadFile({
            url: `${hostNames}/wechat/wechatCommon/saveImageToOSSCheck.json`,
            filePath: item,
            name: 'image',
            header: { 'Content-Type': 'multipart/form-data' },
            formData: {
              'appType': 'smallApp',
              'organizeId': wx.getStorageSync('organizeId'),
              'filePath': item,
              'hash': wx.getStorageSync('hash')
            },
            success: function(res) {
              let ret = JSON.parse(res.data);
              if (ret.flag) {
                let imgList = that.data.imgList;
                let data = ret.data.url;
                imgList.push(data);
                that.setData({
                  imgList: imgList
                });
              } else {
                wx.showModal({
                  title: '提示',
                  content: ret.msg ? ret.msg : '上传错误',
                  showCancel: false
                });
              }
            },
            fail: function() {
              wx.showModal({
                title: '提示',
                content: '上传失败',
                showCancel: false
              })
            },
            complete: function() {
              wx.hideToast();
            }
          })
        })
      }
    })
  },

  // 删除图片
  handleCancelImg(e) {
    let index = e.currentTarget.dataset.index;
    let imgList = this.data.imgList;
    imgList.splice(index, 1);
    this.setData({
      imgList: imgList
    });
  },

  // 展示图片
  handlePreviewImage(e) {
    let index = e.target.dataset.index;
    let that = this;
    wx.previewImage({
      current: that.data.imgList[index], // 当前显示图片的http链接
      urls: that.data.imgList
    })
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(option) {
    this.setData({
      shopId: option.shopId,
      id: option.id,
      isList: option.isList ? option.isList : ''
    });
    this.messageComponent = this.selectComponent('.messageTips');
    let isChooseImage = wx.canIUse('chooseImage');
    let isUploadFile = wx.canIUse('uploadFile');
    if (!isChooseImage || !isUploadFile) {
      wx.showModal({
        title: '提示',
        content: '当前微信版本过低，无法使用该功能，请升级到最新微信版本后重试。'
      })
    }
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {
    // TODO: onReady
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {
    // TODO: onShow
    this.setData({
      disabled: false
    })
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {
    // TODO: onHide
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {
    // TODO: onUnload
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {
    // TODO: onPullDownRefresh
  }
});
