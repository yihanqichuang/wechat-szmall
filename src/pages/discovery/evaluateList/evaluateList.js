// 获取全局应用程序实例对象
// const app = getApp()
import discovery from '../../../api/discovery.js';
import Tips from '../../../utils/tip.js';

// 创建页面实例对象
Page({
  /**
   * 页面的初始数据
   */
  data: {
    shopId: '',
    id: '',
    list: [],
    page: {
      pageNum: 1,
      pageSize: 10,
      total: 1
    }
  },

  // 获取列表
  getList(pageNum) {
    let params = {
      shopId: this.data.shopId,
      pageNum: pageNum,
      id: this.data.id
    };
    discovery.evaluationList(params).then(res => {
      if (res.data.flag) {
        if (pageNum === 1) this.setData({ list: [] });
        res.data.data && res.data.data.length && res.data.data.forEach(item => {
          if (item.imgPath) {
            item.imgUrls = item.imgPath.split(',');
          }
        });
        this.setData({
          list: [...this.data.list, ...res.data.data],
          page: res.data.page
        })
      } else {
        this.messageComponent.applyActive(res.data.msg)
      }
    })
  },

  // 分页
  loadMore(e) {
    this.getList(e.detail);
  },

  // 评论跳转
  handleJump(e) {
    let id = e.currentTarget.dataset.id;
    wx.navigateTo({
      url: `../evaluateListDetail/evaluateListDetail?&id=${id}`
    })
  },

  // 删除品论（整一条+回复）
  handleDelete(e) {
    let id = e.currentTarget.dataset.id;
    let index = e.currentTarget.dataset.index;
    let list = this.data.list;
    let params = {
      id: id,
      type: '1'
    };
    Tips.confirm('该操作将删除所有评论和回复，是否继续该操作？', {}, '温馨提示', '取消', '确定').then(() => {
      discovery.deleteEvaluation(params).then(res => {
        if (res.data.flag) {
          Tips.success('删除成功~').then(() => {
            list.splice(index, 1);
            this.setData({
              list: list
            })
          })
        } else {
          this.messageComponent.applyActive(res.data.msg)
        }
      })
    });
  },

  // 展示图片
  handlePreviewImage(e) {
    let index = e.target.dataset.index;
    let idx = e.target.dataset.idx;
    let that = this;
    wx.previewImage({
      current: that.data.list[index].imgUrls[idx], // 当前显示图片的http链接
      urls: that.data.list[index].imgUrls
    })
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(option) {
    this.messageComponent = this.selectComponent('.messageTips');
    this.setData({
      shopId: option.shopId,
      id: option.id
    });
    this.getList(1);
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {
    // TODO: onReady
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {
    let evaluateList = wx.getStorageSync('evaluateList');
    if (evaluateList) {
      wx.removeStorageSync('evaluateList');
      this.getList(1);
    }
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {
    // TODO: onHide
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {
    // TODO: onUnload
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {
    // TODO: onPullDownRefresh
  },

  /**
   * 页面相关分享
   */
  onShareAppMessage() {
    
  }
});
