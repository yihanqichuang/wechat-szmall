// 获取全局应用程序实例对象
const app = getApp();
import coupon from '../../../api/coupon'
import wechat from '../../../utils/wechat.js';
import discovery from '../../../api/discovery.js';
import WxParse from '../../../wxParse/wxParse.js';
import commodity from '../../../api/commodity.js';
import Tips from '../../../utils/tip.js';
import query from '../../../utils/query.js';

// 元素到左边的距离
const TITLETOP = 34;
const LEFT = 30;
const TOP = 126;
const RECTBANNER = {
  width: 510,
  height: 255
};

// 创建页面实例对象
Page({
  imagePath: '',
  /**
   * 页面的初始数据
   */
  data: {
    id: '',
    shopId: '',
    shopInfo: {},
    couponList: [],
    messageComponent: '',
    commodityTypeList: ['GROUPON', 'SPECIAL', 'FIRSTLOOK', 'SECKILL'],
    commodityLists: {
      GROUPON: [],
      SPECIAL: [],
      FIRSTLOOK: [],
      SECKILL: []
    },
    emptyCommodity: false,
    showCenterDialog: false,
    palette: {
      width: '570rpx',
      height: '610rpx',
      background: '',
      position: 'relative',
      borderRadius: '8rpx',
      views: []
    },
    isShowPainter: false
  },

  // 获取详情
  getShopDetail() {
    Tips.loading('加载中');
    let params = {
      id: this.data.id
    };
    discovery.shopDetail(params).then(res => {
      if (res.data.flag) {
        let shopInfo = res.data.data;
        let _this = this;
        let article = shopInfo.activityIntro;
        let evaluationList = shopInfo.evaluationList;
        evaluationList && evaluationList.length && evaluationList.forEach(item => {
          if (item.imgPath) {
            item.imgUrls = item.imgPath.split(',');
          }
        });
        shopInfo.activityPic = shopInfo.activityPic && shopInfo.activityPic.split(',');
        shopInfo.logoUrl = app.globalData.defaultConfig.defaultPic;
        shopInfo.organizeName = wx.getStorageSync('organizeName');
        article && WxParse.wxParse('article', 'html', article, _this, 15);
        this.setData({
          shopInfo: shopInfo,
          evaluationList: evaluationList
        });
      } else {
        this.messageComponent.applyActive(res.data.msg || '获取详情异常~')
      }
      Tips.loaded();
    })
  },


  // 获取商品信息Promise
  getCommodityData(params) {
    return new Promise((resolve, reject) => {
      commodity.fetchCommodityList(params).then(res => {
        if (!res.data.flag) {
          this.messageComponent.applyActive(res.data.msg);
          resolve(false);
          return
        }
        if (res.data.data.length <= 0) {
          resolve(false);
          return
        }
        resolve(true);
        let data = res.data.data.length <= 2 ? res.data.data : res.data.data.slice(0, 2);
        this.setData({
          [`commodityLists.${params.commodityType}`]: data
        })
      }, () => reject())
    })
  },

  // 获取商品信息4个接口
  getCommodityList() {
    let promises = this.data.commodityTypeList.map(commodityType => {
      return this.getCommodityData({ commodityType, shopId: this.data.shopId })
    });
    Promise.all([...promises]).then(res => {
      let emptyCommodity = true;
      res.forEach(item => {
        if (item) emptyCommodity = false
      });
      this.setData({ emptyCommodity })
    })
  },

  // 获取 商户关联的 优惠券
  getCoupon(shopId) {
    this.setData({
      couponList: []
    });
    coupon.fetchCouponList({
      shopId: shopId
    }).then(res => {
      let data = res.data.data;
      let tokenKey = res.data.submit_tokens_name;
      let tokenValue = res.data[tokenKey];
      if (data && data.length > 0) {
        data.forEach(item => {
          item.tokenKey = tokenKey;
          item.tokenValue = tokenValue
        })
      }
      if (Array.isArray(data)) this.setData({ couponList: data.slice(0, 2), isMoreCoupon: data.length > 2 });
    })
  },

  // 拨打商户电话
  handleCall(e) {
    let shopPhone = e.currentTarget.dataset.phone;
    if (!shopPhone) {
      this.messageComponent.applyActive('该商户没有联系电话~')
    } else {
      wechat.makePhoneCall(shopPhone).then(() => {
        console.log('拨打成功')
      }).catch(() => {
        console.log('拨打失败')
      })
    }
  },

  // 点赞
  handlePraise() {
    if (!this.isMember()) return false;
    let isPraise = this.data.shopInfo.isPraise;
    isPraise = isPraise == 'Y';
    let params = {
      id: this.data.id,
      isPraise: isPraise ? 'N' : 'Y'
    };
    discovery.praise(params).then(res => {
      if (res.data.flag) {
        this.setData({
          ['shopInfo.isPraise']: isPraise ? 'N' : 'Y'
        })
      } else {
        this.messageComponent.applyActive(res.data.msg)
      }
    })
  },

  // 跳转到品论
  handleEvaluate() {
    if (!this.isMember()) return false;
    let id = this.data.id;
    let shopId = this.data.shopId;
    wx.navigateTo({
      url: `../evaluate/evaluate?&id=${id}&shopId=${shopId}`
    });
  },

  // 是否会员
  isMember() {
    let memberType = wx.getStorageSync('memberType');
    if (memberType != 'MEMBER') {
      wx.navigateTo({
        url: `/pages/memberSign/memberSign`
      });
      return false;
    } else {
      return true;
    }
  },

  // 分享
  onShareAppMessage() {
    let organizeId = wx.getStorageSync('organizeId'), organizeName = wx.getStorageSync('organizeName');
    let path = `/pages/discovery/detail/detail?id=${this.data.id}&shopId=${this.data.shopId}&organizeId=${organizeId}&organizeName=${organizeName}&share=true`
    return {
      path: path
    }
  },

  // 评论列表详跳转
  handleJump(e) {
    if (!this.isMember()) return false;
    let id = e.currentTarget.dataset.id;
    wx.navigateTo({
      url: `../evaluateListDetail/evaluateListDetail?&id=${id}`
    })
  },

  // 评论列表跳转
  handleToEvaluateList() {
    if (!this.isMember()) return false;
    let id = this.data.id;
    let shopId = this.data.shopId;
    wx.navigateTo({
      url: `../evaluateList/evaluateList?shopId=${shopId}&id=${id}`
    })
  },

  // 删除品论（整一条+回复）
  handleDelete(e) {
    let id = e.currentTarget.dataset.id;
    let params = {
      id: id,
      type: '1'
    };
    Tips.confirm('该操作将删除所有评论和回复，是否继续该操作？', {}, '温馨提示', '取消', '确定').then(() => {
      discovery.deleteEvaluation(params).then(res => {
        if (res.data.flag) {
          Tips.success('删除成功~').then(() => {
            this.getShopDetail();
          })
        } else {
          this.messageComponent.applyActive(res.data.msg)
        }
      })
    });
  },

  // 展开分享方式
  handleShowShareType() {
    Tips.loading('海报生成中...');
    let shopInfo = this.data.shopInfo;
    if (!Object.keys(shopInfo).length) {
      Tips.loaded();
      this.messageComponent.applyActive('生成海报失败,请重新生成~');
      return false;
    }
    this.createPainter(shopInfo).then(() => {
      console.log('绘制中...');
      this.setData({
        isShowPainter: true
      })
    });
  },

  // 展示图片
  handlePreviewImage(e) {
    let index = e.target.dataset.index;
    let idx = e.target.dataset.idx;
    let that = this;
    wx.previewImage({
      current: that.data.evaluationList[index].imgUrls[idx], // 当前显示图片的http链接
      urls: that.data.evaluationList[index].imgUrls
    })
  },

  // 生成海报
  createPainter(shopInfo) {
    return new Promise((resolve) => {
      this.setData({
        palette: {
          width: '570rpx',
          height: '610rpx',
          background: '',
          position: 'relative',
          borderRadius: `8rpx`,
          views: [
            {
              type: 'image',
              url: shopInfo.memberHead,
              css: {
                top: `${TITLETOP}rpx`,
                left: `${LEFT}rpx`,
                width: `72rpx`,
                height: `72rpx`
              }
            },
            {
              type: 'text',
              text: shopInfo.memberNickName,
              css: {
                top: `${TITLETOP}rpx`,
                left: `${LEFT + 90}rpx`,
                fontSize: '26rpx',
                color: '#333',
                height: '36rpx',
                lineHeight: '36rpx'
              }
            },
            {
              type: 'text',
              text: '心水好物一起用',
              css: {
                top: `${TITLETOP + 36}rpx`,
                left: `${LEFT + 90}rpx`,
                fontSize: '20rpx',
                color: '#999999',
                height: '36rpx',
                lineHeight: '36rpx'
              }
            },
            {
              type: 'image',
              url: shopInfo.activityPic[0],
              css: {
                top: `${TOP}rpx`,
                left: `${LEFT}rpx`,
                width: `${RECTBANNER.width}rpx`,
                height: `${RECTBANNER.height}rpx`,
                borderRadius: `8rpx`
              }
            },
            {
              type: 'text',
              text: shopInfo.activityName,
              css: {
                top: `${TOP + RECTBANNER.height + 22}rpx`,
                left: `${LEFT}rpx`,
                fontSize: '32rpx',
                color: '#333333',
                width: '380rpx',
                maxLines: '1'
              }
            },
            {
              type: 'image',
              url: '/images/icon/time.png',
              css: {
                top: `${TOP + RECTBANNER.height + 74}rpx`,
                left: `${LEFT}rpx`,
                width: `24rpx`,
                height: `24rpx`
              }
            },
            {
              type: 'text',
              text: shopInfo.shopHour,
              css: {
                top: `${TOP + RECTBANNER.height + 74}rpx`,
                left: `${LEFT + 34}rpx`,
                fontSize: '20rpx',
                color: '#777777',
                width: '380rpx',
                maxLines: '1'
              }
            },
            {
              type: 'image',
              url: '/images/icon/add.png',
              css: {
                top: `${TOP + RECTBANNER.height + 112}rpx`,
                left: `${LEFT}rpx`,
                width: `24rpx`,
                height: `24rpx`
              }
            },
            {
              type: 'text',
              text: shopInfo.shopAddress,
              css: {
                top: `${TOP + RECTBANNER.height + 112}rpx`,
                left: `${LEFT + 34}rpx`,
                fontSize: '20rpx',
                color: '#777777',
                width: '380rpx',
                maxLines: '1'
              }
            },
            {
              type: 'image',
              url: shopInfo.qrcodeUrl,
              css: {
                top: `${TOP + RECTBANNER.height + 22}rpx`,
                right: `${LEFT}rpx`,
                width: `100rpx`,
                height: `100rpx`
              }
            },
            {
              type: 'text',
              text: '长按立即前往',
              css: {
                top: `${TOP + RECTBANNER.height + 132}rpx`,
                right: `${LEFT}rpx`,
                fontSize: '16rpx',
                color: '#777777',
                maxLines: '1'
              }
            },
            {
              type: 'text',
              text: ' ',
              css: {
                top: `${TOP + RECTBANNER.height + 168}rpx`,
                left: `${LEFT}rpx`,
                background: '#F5F5F5',
                color: '#F5F5F5',
                width: `${RECTBANNER.width}rpx`,
                height: '2rpx',
                fontSize: '2rpx'
              }
            },
            {
              type: 'image',
              url: shopInfo.logoUrl,
              css: {
                top: `${TOP + RECTBANNER.height + 179}rpx`,
                left: `${LEFT}rpx`,
                width: `40rpx`,
                height: `40rpx`,
                borderRadius: `40rpx`
              }
            },
            {
              type: 'text',
              text: shopInfo.organizeName,
              css: {
                left: `${LEFT + 50}rpx`,
                top: `${TOP + RECTBANNER.height + 187}rpx`,
                fontSize: '24rpx',
                color: '#333',
                width: '360rpx',
                maxLines: '1'
              }
            }
          ]
        }
      });
      resolve();
    });
  },

  // 下载海报
  handleCreate() {
    let that = this;
    wx.saveImageToPhotosAlbum({
      filePath: this.imagePath,
      success() {
        that.messageComponent.applyActive('图片已保存到本地相册')
      }
    });
  },

  // 海报绘制成功
  onImgOK(e) {
    this.imagePath = e.detail.path;
    Tips.loaded();
    this.setData({
      imagePath: e.detail.path,
      showCenterDialog: true
    })
  },

  // 海报绘制失败
  onImgErr(e) {
    console.log(e);
    Tips.loaded();
    this.messageComponent.applyActive('生成海报失败,请重新生成~');
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(option) {
    this.messageComponent = this.selectComponent('.messageTips');
    if (option.scene) {
      let scene = decodeURIComponent(option.scene);
      let id = query.getQueryString('id', scene);
      let shopId = query.getQueryString('shopId', scene);
      this.setData({
        id: id,
        shopId: shopId
      })
    } else {
      this.setData({
        id: option.id,
        shopId: option.shopId
      })
    }
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {
    // TODO: onReady
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {
    this.getShopDetail();
    this.getCommodityList();
    this.getCoupon(this.data.shopId);
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {
    // TODO: onHide
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {
    // TODO: onUnload
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {
    // TODO: onPullDownRefresh
  }
});
