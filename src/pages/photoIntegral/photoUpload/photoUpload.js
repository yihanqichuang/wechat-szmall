// 获取全局应用程序实例对象
const app = getApp();
import photoIntegral from '../../../api/pohotoIntegral.js';
import Tips from '../../../utils/tip.js';
import WxParse from '../../../wxParse/wxParse.js';
import { PRICE2, PRICE } from '../../../utils/regular.js';

// 创建页面实例对象
Page({
  /**
   * 页面的初始数据
   */
  data: {
    messageComponent: '',
    picNum: 6,
    ownerIntegralIntroduce: [],
    detailList: [],
    detailListDefault: {
      'CONSUME_TIME': {
        placeholder: '请选择消费时间',
        value: '',
        putType: 'SELECT',
        SubmitType: 'consumeTime',
        reg: /\*/
      },
      'CONSUME_SHOP': {
        placeholder: '请输入门店关键字或选择',
        value: '',
        putType: 'SEARCH',
        SubmitType: 'shopId',
        reg: /\*/
      },
      'CONSUME_MONEY': {
        placeholder: '请输入金额',
        value: '',
        putType: 'INPUT',
        SubmitType: 'amount',
        reg: PRICE2
      },
      'TICKET_NUM': {
        placeholder: '请输入小票号',
        value: '',
        putType: 'INPUT',
        SubmitType: 'ticketNum',
        reg: /\*/
      }
    },
    formData: {},
    imgList: [],
    isShowShopList: false,
    config: {},
    shopList: []
  },

  // 获取卡号和积分 传type不为空的值不查积分，后端接口优化需要
  getPhotoIntegral() {
    photoIntegral.getHomePage({ type: 'NOTSCORE' }).then(res => {
      if (res.data.flag) {
        let data = res.data.data;
        let _this = this;
        data.config && data.config.detailList.length && data.config.detailList.forEach(item => {
          item.value = '';
          item.placeholder = this.data.detailListDefault[item.showType].placeholder;
          item.putType = this.data.detailListDefault[item.showType].putType;
          item.SubmitType = this.data.detailListDefault[item.showType].SubmitType;
        });
        this.setData({
          ownerIntegralIntroduce: data.ownerIntegralIntroduce,
          detailList: data.config.detailList,
          config: data.config,
          picNum: data.config.uploadImageNum
        });
        let article = res.data.data.config.operationInstruction;
        article && WxParse.wxParse('article', 'html', article, _this, 15);
      } else {
        this.messageComponent.applyActive(res.data.msg || '获取数据失败！');
      }
    }).catch(err => {
      this.messageComponent.applyActive(err || '系统繁忙！');
    })
  },

  // 上传图片(保存在阿里服务器)
  handleUpLoad() {
    let that = this;
    let hostNames = wx.getStorageSync('hostName') || app.data.hostName;
    wx.chooseImage({
      count: that.data.picNum - that.data.imgList.length,
      sizeType: ['original', 'compressed'],
      sourceType: ['album', 'camera'],
      success: function(res) {
        console.log(res);
        res.tempFilePaths.forEach((item) => {
          wx.uploadFile({
            url: `${hostNames}/wechat/wechatCommon/saveImageToOSS.json`,
            filePath: item,
            name: 'image',
            header: { 'Content-Type': 'multipart/form-data' },
            formData: {
              'appType': 'smallApp',
              'organizeId': wx.getStorageSync('organizeId'),
              'filePath': item,
              'hash': wx.getStorageSync('hash')
            },
            success: function(res) {
              let ret = JSON.parse(res.data);
              if (ret.flag) {
                let imgList = that.data.imgList;
                let data = ret.data.url;
                imgList.push(data);
                that.setData({
                  imgList: imgList
                });
              } else {
                wx.showModal({
                  title: '提示',
                  content: ret.msg ? ret.msg : '上传失败',
                  showCancel: false
                });
                return;
              }
            },
            fail: function() {
              wx.showModal({
                title: '提示',
                content: '上传失败',
                showCancel: false
              })
            },
            complete: function() {
              wx.hideToast();
            }
          })
        })
      }
    })
  },

  // 删除图片
  handleCancelImg(e) {
    let index = e.currentTarget.dataset.index;
    let imgList = this.data.imgList;
    imgList.splice(index, 1);
    this.setData({
      imgList: imgList
    });
  },

  // 展示图片
  handlePreviewImage(e) {
    let index = e.target.dataset.index;
    let that = this;
    wx.previewImage({
      current: that.data.imgList[index], // 当前显示图片的http链接
      urls: that.data.imgList
    })
  },

  // 确认上传
  handleSaveRecord(e) {
    this.setData({ formId: e.detail.formId });
    if (!this.handleValidate()) return false;
    Tips.loading('提交中...');
    setTimeout(() => this.saveRecord(), 500);
  },

  // 表单校验
  handleValidate() {
    if (this.data.config.needFillinField == 'Y') {
      let detailList = this.data.detailList;
      for (let i = 0; i < detailList.length; i++) {
        let item = detailList[i];
        if (item.wechat == 'Y') {
          if (item.value ==  '') {
            let msg = this.data.detailListDefault[item.showType].placeholder;
            this.messageComponent.applyActive(msg);
            return false
          }

          if (item.showType == 'CONSUME_MONEY') {
            if (!PRICE.test(item.value)) {
              this.messageComponent.applyActive('请输入合法的金额~');
              return false
            }
          }
        }
      }
    }

    if (!this.data.imgList.length) {
      this.messageComponent.applyActive('请先上传小票照片呀');
      return false;
    }
    return true
  },

  // 保存图片
  saveRecord() {
    let param = {
      ticketPic: this.data.imgList.join(','),
      formId: this.data.formId
    };
    let params = Object.assign(param, this.data.formData);
    console.log(params);
    photoIntegral.saveOwnIntegralRecord(params).then(res => {
      Tips.loaded();
      if (res.data.flag) {
        Tips.confirm('我们将在3-7个工作日内完成审核，在此期间请您保留小票做好凭证！', {}, '上传成功', '申请记录', '再积一笔').then(() => {
          wx.navigateBack({
            delta: 1
          })
        }).catch(() => {
          wx.redirectTo({
            url: `../photoApplyList/photoApplyList`
          });
        })
      } else {
        this.messageComponent.applyActive(res.data.msg || '保存失败！');
      }
    }).catch(err => {
      this.messageComponent.applyActive(err || '系统繁忙');
    })
  },

  // SELECT 类型选值
  bindDateChange(e) {
    let value = e.detail.value;
    let dataset = e.currentTarget.dataset;
    let index = dataset.index;
    let type = dataset.type;
    let _value = `detailList[${index}].value`;
    let $value = `formData.${type}`;
    this.setData({
      [_value]: value,
      [$value]: value
    })
  },

  // INPUT 类型选值
  handleInputVal(e) {
    let value = e.detail.value;
    let dataset = e.currentTarget.dataset;
    let index = dataset.index;
    let type = dataset.type;
    let _value = `detailList[${index}].value`;
    let $value = `formData.${type}`;
    this.setData({
      [_value]: value,
      [$value]: value
    })
  },

  // 搜索店铺
  handleInputValSearch(e) {
    let value = e.detail.value;
    this.getShopList(value)
  },

  // 选择店铺
  handleChooseShopName(e) {
    console.log(e);
    let dataset = e.currentTarget.dataset;
    let index = dataset.index;
    let type = dataset.type;
    let value = dataset.name;
    let id = dataset.id;
    let _value = `detailList[${index}].value`;
    let $value = `formData.${type}`;
    this.setData({
      isShowShopList: false,
      [_value]: value,
      [$value]: id
    });
  },

  // 关闭店铺列表
  handleCloseShopList() {
    this.setData({
      isShowShopList: false
    })
  },

  // 搜索店铺列表
  getShopList(shopName = '') {
    let params = {
      shopName: shopName
    };
    photoIntegral.shopList(params).then(res => {
      if (res.data.flag) {
        this.setData({
          shopList: res.data.data,
          isShowShopList: res.data.data && res.data.data.length
        })
      } else {
        this.messageComponent.applyActive(res.data.msg || '保存失败！');
      }
    }).catch(err => {
      this.messageComponent.applyActive(err || '系统繁忙');
    })
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad() {
    // TODO: onLoad
    this.messageComponent = this.selectComponent('.messageTips');
    this.getPhotoIntegral();
    let isChooseImage = wx.canIUse('chooseImage');
    let isUploadFile = wx.canIUse('uploadFile');
    if (!isChooseImage || !isUploadFile) {
      wx.showModal({
        title: '提示',
        content: '当前微信版本过低，无法使用该功能，请升级到最新微信版本后重试。'
      })
    }
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {
    // TODO: onReady
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {
    // TODO: onShow
    // this.getShopList();
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {
    // TODO: onHide
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {
    // TODO: onUnload
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {
    // TODO: onPullDownRefresh
  }
});
