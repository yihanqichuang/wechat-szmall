// 获取全局应用程序实例对象
// const app = getApp()
import photoIntegral from '../../api/pohotoIntegral.js';
import Tips from '../../utils/tip';
import WxParse from '../../wxParse/wxParse.js';

// 创建页面实例对象
Page({
  /**
   * 页面的初始数据
   */
  data: {
    messageComponent: '',
    balanceScore: 0,
    memberCardNo: '',
    ownerIntegralIntroduce: [],
    // 是否开启扫码自助积分
    isScanCode: 'N'
  },

  // 获取卡号和积分
  getPhotoIntegral() {
    photoIntegral.getHomePage().then(res => {
      if (res.data.flag) {
        let data = res.data.data;
        let _this = this;
        this.setData({
          balanceScore: data.member.balanceScore,
          memberCardNo: data.member.memberCardNo,
          ownerIntegralIntroduce: data.ownerIntegralIntroduce,
          config: data.config,
          isScanCode: data.isScanCode == 'Y'
        });
        let article = res.data.data.config.operationInstruction;
        article && WxParse.wxParse('article', 'html', article, _this, 15);
      } else {
        this.messageComponent.applyActive(res.data.msg || '获取数据失败！');
      }
    }).catch(err => {
      this.messageComponent.applyActive(err || '系统繁忙！');
    })
  },

  // 调起微信扫码
  handleScanCode() {
    let that = this;
    wx.scanCode({
      success(res) {
        console.log(res)
        that.scanCodeApi(res.result)
      },
      fail(err) {
        console.log(err);
        that.messageComponent.applyActive('扫码失败~');
      }
    })
  },

  scanCodeApi(qrcode) {
    console.log(qrcode)
    let params = {
      qrcode: qrcode
    }
    photoIntegral.scanCode(params).then(res => {
      console.log(res)
      if (res.data.flag) {
        Tips.success('积分成功');
      } else {
        this.messageComponent.applyActive(res.data.msg);
      }
    }).catch(err => {
      this.messageComponent.applyActive(err || '系统繁忙！');
    })
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad() {
    // TODO: onLoad
    this.messageComponent = this.selectComponent('.messageTips')
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {
    // TODO: onReady
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {
    // TODO: onShow
    this.getPhotoIntegral();
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {
    // TODO: onHide
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {
    // TODO: onUnload
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {
    // TODO: onPullDownRefresh
  },

  /**
   * 页面相关分享
   */
  onShareAppMessage() {
    
  }
});
