// 获取全局应用程序实例对象
// const app = getApp()
import photoIntegral from '../../../api/pohotoIntegral.js';

// 创建页面实例对象
Page({
  /**
   * 页面的初始数据
   */
  data: {
    messageComponent: '',
    listPage: {
      pageNum: 1,
      pageSize: 10,
      total: 1
    },
    // 解决图片放大接口调用问题
    isShow: false,
    list: [],
    // 上一次点击的索引值
    preIndex: 0,
    emptyType: 'applyList'
  },

  // 获取列表
  getLists(pageNum) {
    if (pageNum === 1) this.setData({ list: [] });
    let params = {
      pageNum
    };
    photoIntegral.getIntegralRecordList(params).then(res => {
      if (res.data.flag) {
        let data = res.data.data;
        data.forEach(item => {
          item.imgList = item.ticketPic ? item.ticketPic.split(',') : [];
          item.createTime = item.createTime.replace(/-/g, '.');
          item.isShow = false
        });
        this.setData({
          list: [...this.data.list, ...res.data.data],
          listPage: res.data.page
        })
      } else {
        this.messageComponent.applyActive(res.data.msg || '获取数据失败！');
      }
    }).catch(err => {
      console.log(err || '系统繁忙！');
    })
  },

  // 加载更多
  loadMore(e) {
    this.getLists(e.detail);
  },

  // 显示当前详情
  handleShowDetail(e) {
    let index = e.currentTarget.dataset.index;
    let list = this.data.list;
    let preIndex = this.data.preIndex;

    if (index == preIndex) {
      list[index].isShow = !list[index].isShow;
    } else {
      list[index].isShow = true;
      list[preIndex].isShow = false;
      preIndex = index;
    }

    this.setData({
      preIndex: preIndex,
      list: list
    });
  },

  // 展示图片
  handlePreviewImage(e) {
    // 图片的在列表的位置
    let idx = e.target.dataset.idx;
    let item = e.target.dataset.item;
    wx.previewImage({
      current: item[idx], // 当前显示图片的http链接
      urls: item
    });
    this.setData({
      isShow: true
    })
  },


  /**
   * 生命周期函数--监听页面加载
   */
  onLoad() {
    // TODO: onLoad
    this.messageComponent = this.selectComponent('.messageTips')
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {
    // TODO: onReady
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {
    // TODO: onShow
    if (!this.data.isShow) {
      this.getLists(1);
    }
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {
    // TODO: onHide
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {
    // TODO: onUnload
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {
    // TODO: onPullDownRefresh
  }
});
