// 获取全局应用程序实例对象
// const app = getApp();
import Tips from '../../utils/tip.js';
import userApi from '../../api/user.js'
// 创建页面实例对象
Page({
  /**
   * 页面的初始数据
   */
  data: {
    messageComponent: '',
    // 标记今天是否已经签到
    signed: false,
    // 连续签到的天数
    day: 1,
    // 累计签到的积分
    score: 122,
    // 大礼包弹窗
    showDialog: false,
    // 签到时间轴
    signTimeList: [],
    pointsCount: 0, // 赠送的积分
    couponCount: 0, // 赠送的优惠券数量
    upperBtnTxt: '', // 按钮显示文字
    lowerBtnTxt: '',
    hasNoGift: false
  },

  // 点击签到按钮
  handleSign() {
    let signed = this.data.signed;
    if (!signed) {
      Tips.loading('签到中');
      setTimeout(() => this.sign(), 500)
    }
  },

  // 签到（按钮点击）
  sign() {
    Tips.loaded();
    userApi.saveMemberSign().then(res => {
      if (res.data.flag) {
        Tips.success('签到成功!', 500).then(() => {
          this.signConfig();
          let handselCouponList = res.data.handselCouponList && res.data.handselCouponList.length > 0
          let handselPoints = res.data.handselPoints
          let hasNoGift = res.data.hasNoGift 
          if (handselPoints || handselCouponList) {
            if (handselPoints) {
              this.data.upperBtnTxt = '我的积分'
            }
            if (handselCouponList) {
              res.data.handselCouponList.forEach(item => {
                this.data.couponCount += item.couponNum
              });
              this.data.upperBtnTxt = '我的卡券'
            }
            if (handselPoints && handselCouponList) {
              this.data.upperBtnTxt = '我的卡券'
              this.data.lowerBtnTxt = '我的积分'
            }
            this.setData({ 
              upperBtnTxt: this.data.upperBtnTxt,
              lowerBtnTxt: this.data.lowerBtnTxt || '知道了',
              pointsCount: handselPoints,
              couponCount: this.data.couponCount,
              showDialog: true,
              hasNoGift: hasNoGift
            })
          }
        });
      } else {
        let msg = res.data.msg || '签到失败';
        this.messageComponent.applyActive(msg)
      }
    }).catch();
  },

  // 签到信息
  signConfig() {
    userApi.signConfig().then(res => {
      if (res.data.flag) {
        this.setData({
          signTimeList: res.data.signTimeList,
          day: res.data.signCount,
          score: res.data.allGetPoints,
          signed: res.data.todayIsSign == 'Y',
          rule: res.data.pointsSignedConfigDesc,
          todayPoints: res.data.todayPoints
        })
      } else {
        let msg = res.data.msg || '获取签到信息失败';
        this.messageComponent.applyActive(msg)
      }
    }).catch();
  },
  // 按钮—上  跳转
  toNextList() {
    let arr = [this.data.pointsCount, this.data.couponCount]
    let url = ''
    if (arr.every(item => item) || !arr[0] && arr[1]) { // 卡券
      url = '/pages/newPackage/myCouponNew/myCoupon'
    } else {
      url = '/pages/integral/integral'
    }
    wx.navigateTo({
      url
    })
  },
  // 按钮—下  跳转
  toNextPage() {
    if (this.data.pointsCount && this.data.couponCount) {
      wx.navigateTo({
        url: '/pages/integral/integral'
      })
    } else {
      this.handleCloseDialog()
    }
  },

  // 关闭送积分弹窗
  handleCloseDialog() {
    this.setData({
      showDialog: false
    })
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad() {
    // TODO: onLoad
    this.messageComponent = this.selectComponent('.messageTips')
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {
    // TODO: onReady
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {
    // TODO: onShow
    this.signConfig();
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {
    // TODO: onHide
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {
    // TODO: onUnload
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {
    // TODO: onPullDownRefresh
  }
});
