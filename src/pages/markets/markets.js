// 获取全局应用程序实例对象
const app = getApp()

import api from '../../api/markets.js';

// 创建页面实例对象
Page({
  /**
   * 页面的初始数据
   */
  data: {
    flag: false,
    activeIndex: 0,
    tabs: [],
    contentList: [],
    industryType: ''
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(opt) {
    wx.showShareMenu({ menus: ['shareAppMessage', 'shareTimeline']}); // 分享好友、朋友圈 
    let industryType = opt.industryType ? opt.industryType : '';
    this.setData({
      industryType: industryType
    })
    wx.getSystemInfo({
      success: (res) => {
        this.setData({
          deviceWidth: res.windowWidth,
          deviceHeight: res.windowHeight
        });
      }
    });
    /*
    * 获取城市列表
    */
    let t = this;
    api.fetchOrganizeList({ industryType }).then(res => {
      let data = res.data.data;
      let id = wx.getStorageSync('organizeId');
      this.setData({ tabs: data });
      if (!app.globalData.organizeList) {
        let list = [];
        data.forEach(city => {
          if (city.organizeList && city.organizeList.length > 0) {
            list.push(...city.organizeList);
          }
        });
        app.globalData.organizeList = list;
      }
      data.map((item, key) => {
        item.organizeList.length && item.organizeList.map((list) => {
          this.setData({
            activeIndex: key,
            contentList: item.organizeList
          });
          if (list.organizeId == id) {
            this.setData({
              activeIndex: key,
              contentList: item.organizeList
            });
          }
        })
      })
    })
  },
  /*
  * 选择商城
  */
  switchMal(e) {
    let { id, name, phone, dss, type } = e.currentTarget.dataset;
    // wx.setStorageSync('scene', '2001');
    // if (type == 'SuperMarket') {
    //   wx.setStorageSync('cs_id', id);
    // } else {
    //   wx.setStorageSync('sc_id', id);
    // }
    app.handleOrganizeInfo({
      organizeId: id,
      organizeName: name,
      organizePhone: phone,
      organizeAddress: dss,
      industryType: type
    });
    wx.navigateBack({
      delta: 1
    })
  },
  /*
  * 切换城市
  */
  changeTab(e) {
    let vm = this;
    this.setData({
      activeIndex: e.currentTarget.dataset.index,
      content: e.currentTarget.dataset.organizelist,
      contentList: e.currentTarget.dataset.organizelist,
      flag: true
    });
    setTimeout(function () {
      vm.setData({
        flag: false
      })
    }, 300);
  }
})
