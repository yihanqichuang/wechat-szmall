// 获取全局应用程序实例对象
const app = getApp();
const wechat = require('../../utils/wechat.js');
const api = require('../../api/login.js');

// 创建页面实例对象
Page({
  /**
   * 页面的初始数据
   */
  data: {
    title: 'start'
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad() {
    // TODO: onLoad
  },

  bindGetUserInfo(e) {
    console.log(e);
    let param = {};
    param.code = wx.getStorageSync('code');
    // online
    // param.appId = 'wxb5950e320cdc8ec2';
    // 开发环境
    // param.appId = 'wxd451d31ed096de10';
    // 测试环境
    param.appId = 'wx0f9facbac8391344';
    param.encryptedData = e.detail.encryptedData;
    param.ivStr = e.detail.iv;
    wx.setStorageSync('appId', param.appId);
    app.data.userInfo = e.detail.userInfo;
    if (e.detail.errMsg == 'getUserProfile:ok') {
      let memberPhone = wx.getStorageSync('memberPhone');
      if (!memberPhone) {
        wx.reLaunch({
          url: '/pages/author/author'
        })
      } else {
        wx.navigateBack({
          delta: 1
        })
      }
    }
    this.getHash(param);
  },
  /**
   * 获取hash
   */
  getHash(param) {
    api.login(param).then(res => {
      if (!res.data.flag) {
        wx.showModal({
          title: '提示',
          content: res.data.msg,
          success: function() {}
        })
      }
      // 会员类型
      this.data.memberType = res.data.result;
      // 域名
      this.data.hostName = res.data.hostName;
      wx.setStorageSync('hash', res.data.hash);
      this.getLocation();
    })
  },
  /**
   * 获取地理位置
   */
  getLocation() {
    wechat.getLocation().then((res) => {
      let lng = res.longitude;
      let lat = res.latitude;
      let industryType = 'GeneralMerchandise';
      this.getNearestOrganiz({ lng, lat , industryType });
    }).catch(() => {
      this.getNearestOrganiz({industryType});
    });
  },
  /**
   * 获取最近商场
   */
  getNearestOrganiz(param) {
    api.nearestOrganiz(param).then(res => {
      console.log(res);
      let id = res.data.data.organizeId;
      let name = res.data.data.organizeName;
      let localid = wx.getStorageSync('organizeId');
      wx.setStorageSync('organizeId', id);
      wx.setStorageSync('organizeName', name);
      if (localid) return;
      this.jumpToTargetpage();
    })
  },
  /**
   * 页面跳转
   */
  jumpToTargetpage() {
    let pages = getCurrentPages();
    let currentPage = pages[pages.length - 1];
    let url = currentPage.route;
    let options = currentPage.options;
    let param = Object.keys(options).map(key => {
      return key + '=' + options[key]
    }).join('&');
    console.log(param);
    if (options.organizeId)  {
      wx.setStorageSync('organizeId', options.organizeId);
      wx.setStorageSync('organizeName', options.organizeName);
    }
    wx.reLaunch({
      url: '/' + url + '?' + param
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {
    // TODO: onReady
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {
    // TODO: onShow
    wx.getSetting({
      success(res) {
        console.log(res);
        // 验证是否授权过
        if (res.authSetting['scope.userInfo']) {
          wx.getUserProfile({
            success: function(res) {
              console.log(res.userInfo)
            }
          });
          // let memberPhone = wx.getStorageSync('memberPhone');
          // if (memberPhone && memberPhone != '') {
          //   wx.switchTab({
          //     url: '/pages/homeMarket/home'
          //   })
          // } else {
          //   wx.navigateTo({
          //     url: '/pages/author/author'
          //   })
          // }
        }
      }
    });
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {
    // TODO: onHide
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {
    // TODO: onUnload
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {
    // TODO: onPullDownRefresh
  }
});
