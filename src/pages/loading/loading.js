// 获取全局应用程序实例对象
const app = getApp();
import wechat from '../../utils/wechat.js';
import api from '../../api/login.js';
import Tip from '../../utils/tip';
import query from '../../utils/query.js';

// 创建页面实例对象
Page({
  /**
   * 页面的初始数据
   */
  data: {
    title: 'loading',
    logoImg: ''
  },

  onLoad() {
  },

  onShow(options) {
    this.login();
  },

  // 登录
  async login() {
    const loginRes = await wechat.login();
    if (!loginRes.code) {
      Tip.toast('获取code失败', null, 'none');
      return;
    }
    let url = 'szmall/oauth/token/' + loginRes.code;
    let param = {
      appId: '${appId}',
      appType: 'smallApp'
    };
    wx.setStorageSync('code', loginRes.code);
    wx.setStorageSync('appId', param.appId);
    // 调用登陆
    const tokenRes = await api.login(url, param);
    if ("00000" === tokenRes.data.code) {
      wx.setStorageSync('token', tokenRes.data.data.access_token);
      const redirectPath = wx.getStorageSync('redirectPath') || '/pages/homeMarket/home';
      wx.reLaunch({
        url: redirectPath
      });
    } else {
      Tip.toast(tokenRes.data.msg, null, 'none');
    }
  }
})
