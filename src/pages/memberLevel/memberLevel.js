// 获取全局应用程序实例对象
// const app = getApp()
import userApi from '../../api/user.js';
import WxParse from '../../wxParse/wxParse.js'
// 创建页面实例对象
Page({
  /**
   * 页面的初始数据
   */
  data: {
    title: 'memberLevel',
    messageComponent: '',
    memberCardInfo: {},
    points: '',
    currentLevel: {},
    gradeNo: '',
    cardDesc: ''
  },

  // 获取会员卡说明
  getCardDesc() {
    userApi.getCardDesc().then(res => {
      if (res.data.flag) {
        if (res.data.data) {
          let _this = this;
          let article = res.data.data.cardDesc;
          this.setData({
            cardDesc: article
          });
          WxParse.wxParse('article', 'html', article, _this, 15);
        }
      } else {
        this.messageComponent.applyActive(res.data.msg)
      }
    }).catch(err => {
      console.log(err)
    })
  },

  // 获取会员等级
  getMemberLevel() {
    userApi.queryCardLevel().then(res => {
      console.log(res);
      if (res.data.flag) {
        this.setData({
          // 历史总积分
          points: res.data.data.stageCent,
          // 下一等级
          memberCardInfo: res.data.data.cardLevelDO,
          // 当前等级
          currentLevel: res.data.data.currentLevel,
          gradeNo: res.data.data.gradeNo
        })
      } else {
        // this.messageComponent.applyActive(res.data.msg || '获取数据失败')
      }
    }).catch(err => {
      console.log(err)
    })
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad() {
    // TODO: onLoad
    this.messageComponent = this.selectComponent('.messageTips')
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {
    // TODO: onReady
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {
    // TODO: onShow
    // this.getMemberLevel();
    this.getCardDesc()
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {
    // TODO: onHide
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {
    // TODO: onUnload
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {
    // TODO: onPullDownRefresh
  }
});
