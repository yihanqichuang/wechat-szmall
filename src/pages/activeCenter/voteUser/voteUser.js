// 获取全局应用程序实例对象
const app = getApp()
import voteApi from '../../../api/vote.js';
import WxParse from '../../../wxParse/wxParse.js';
import Tips from '../../../utils/tip.js';
import shareLog from '../../../api/shareLog.js';
import timeFormat from '../../../utils/timeFormat.js';
import query from '../../../utils/query.js';

// 创建页面实例对象
Page({
  /**
   * 页面的初始数据
   */
  data: {
    title: '',
    messageComponent: '',
    id: '',
    userId: '',
    rank: '',
    isOwn: true,
    voteDialog: false,
    voteSuccessText: '',
    gradeName: false,
    needMember: false,
    memberDialog: false,
    canVote: false,
    imageList: [],
    info: {},
    voteNeedMember: false
  },

  // 分享
  onShareAppMessage() {
    // 转发成功之后记录分享的次数
    shareLog.shareLogCommon(this.data.voteUser.voteActivityId, 'VOTE_ACTIVITY');
    let organizeId = wx.getStorageSync('organizeId'),  organizeName = wx.getStorageSync('organizeName');
    return {
      path: `/pages/activeCenter/voteUser/voteUser?id=${this.data.voteUser.voteActivityId}&userId=${this.data.voteUser.id}&organizeId=${organizeId}&organizeName=${organizeName}&share=true`
    }
  },

  // 重新报名
  handleReVote() {
    wx.navigateTo({
      url: `../voteEnter/voteEnter?id=${this.data.voteActivity.id}`
    })
  },

  // 返回
  handleBack() {
    wx.navigateBack({
      delta: 1
    })
  },

  // 跳转到我的报名
  handleJumpVote() {
    this.setData({
      userId: this.data.nowUser.id
    });
    this.getVoteUserDetail(this.data.id, this.data.userId);
  },

  // 给别人投票
  handleVoteForUser() {
    let memberType = wx.getStorageSync('memberType');
    if (this.data.voteNeedMember && memberType !== 'MEMBER') {
      wx.navigateTo({
        url: `/pages/memberSign/memberSign`
      })
      return;
    }
    let canVote = this.data.canVote;
    if (!canVote) {
      this.messageComponent.applyActive('当前活动不符合投票规则~');
      return;
    }
    Tips.loading('投票中');
    setTimeout(() => this.voteForUser(), 500);
  },

  // 给别人投票
  voteForUser() {
    let params = {
      openId: this.data.voteUser.openId,
      voteActivityId: this.data.voteUser.voteActivityId,
      voteTypeId: this.data.voteUser.voteTypeId,
      voteUserId: this.data.voteUser.id
    };
    voteApi.voteForUser(params).then(res => {
      Tips.loaded();
      if (res.data.flag) {
        this.setData({
          voteDialog: true,
          voteSuccessText: res.data.alertMsg
        });
      } else {
        this.messageComponent.applyActive(res.data.msg || '投票失败')
      }
    }).catch(err => {
      console.log(err)
    })
  },

  // 投票成功知道了 || 点击右上角关闭按钮
  handleCloseVote() {
    this.setData({
      voteDialog: false
    });
    // 重新加载数据、
    this.getVoteUserDetail(this.data.id, this.data.userId);
  },

  // 报名该投票活动
  handleJoinVote() {
    let memberType = wx.getStorageSync('memberType');
    if (memberType !== 'MEMBER') {
      wx.navigateTo({
        url: `/pages/memberSign/memberSign`
      })
      return;
    }
    if (this.data.needMember) {
      if (!this.data.isMember) {
        this.setData({
          memberDialog: true
        });
        return false;
      } else if (!this.data.gradeName) {
        this.messageComponent.applyActive('亲，您暂不符合参加报名条件!');
        return false;
      }
    }
    wx.navigateTo({
      url: `../voteEnter/voteEnter?id=${this.data.voteActivity.id}`
    })
  },

  // 关闭注册会员弹窗
  handleClose() {
    this.setData({
      memberDialog: false
    });
  },

  // 去注册
  handleSure() {
    wx.navigateTo({
      url: `../../memberSign/memberSign`
    });
    this.setData({
      memberDialog: false
    });
  },

  // 获取报名人的详情
  getVoteUserDetail(id, userId) {
    let params = {
      voteActivityId: id,
      userId: userId
    };
    voteApi.voteUserDetail(params).then(res => {
      console.log(res);
      Tips.loaded();
      let data = res.data;
      let _this = this;
      if (!res.data.flag) {
        let msg = res.data.msg || '请求数据失败';
        this.messageComponent.applyActive(msg);
        return
      }
      this.setData({
        voteActivity: data.voteActivity,
        voteType: data.voteType,
        voteUser: data.voteUser,
        rank: data.rank,
        nowUser: data.nowUser ? data.nowUser : null,
        canVote: data.canVote,
        voteNeedMember: data.needMember,
        // 判断打开的是自己的详情还是别人的详情
        isOwn: data.member.openId == data.voteUser.openId,
        // 自己是否会员
        isMember: data.isMember,
        // 会员等级
        moduleGradeList: data.moduleGradeList,
        // 活动是否有会员限制
        needMember: data.moduleGradeList.length > 0,
        enrollState: data.enrollState,
        voteState: data.voteState,
        info: {
          picturePath: data.voteActivity.picPath.split(',')[0],
          title: `我是"${data.voteUser.joinNum}号选手"正在参加`,
          name: data.voteActivity.name,
          time: timeFormat.timeToDay(data.voteActivity.voteStartTime) + ' ~ ' + timeFormat.timeToDay(data.voteActivity.voteEndTime),
          memberHead: data.memberHead,
          memberNickName: data.memberNickName,
          qrcodeUrl: data.qrcodeUrl,
          logoUrl: app.globalData.defaultConfig.defaultPic,
          organizeName: wx.getStorageSync('organizeName')
        }
      });

      // 设置页面标题
      wx.setNavigationBarTitle({
        title: this.data.isOwn ? '我的报名详情' : '报名详情'
      });

      // 判断是否达到报名的等级
      if (data.moduleGradeList && data.moduleGradeList.length > 0) {
        let gradeName = data.moduleGradeList.some(function(x) {
          return x.gradeName == data.gradeName;
        });
        console.log(gradeName);
        this.setData({
          gradeName: gradeName
        })
      }

      // 处理报名选项 图片处理放在数组末尾
      data.enrollList.forEach((item, index) => {
        if (item.columnType == 'image') {
          let arr = data.enrollList.splice(index, 1);
          data.enrollList.push(arr[0]);
        }
      });
      data.enrollList.forEach((item) => {
        if (item.columnType == 'image') {
          item.columnValue = item.columnValue.split(',');
          this.setData({
            imageList: item.columnValue
          })
        }
      });

      this.setData({
        enrollList: data.enrollList
      });

      // ==================== 富文本 ================== //
      let article = data.voteActivity.content;
      article && WxParse.wxParse('article', 'html', article, _this, 15);
      // ==================== 富文本 ================== //
    }).catch(err => {
      console.log(err)
    })
  },

  // 展示图片
  handlePreviewImage(e) {
    let index = e.target.dataset.index;
    let that = this;
    wx.previewImage({
      current: that.data.imageList[index], // 当前显示图片的http链接
      urls: that.data.imageList
    })
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(option) {
    this.messageComponent = this.selectComponent('.messageTips');
    if (option.scene) {
      let scene = decodeURIComponent(option.scene);
      let id = query.getQueryString('id', scene);
      let userId = query.getQueryString('userId', scene);
      this.setData({
        id: id,
        userId: userId || ''
      });
    } else {
      this.setData({
        id: option.id ? option.id : '',
        userId: option.userId ? option.userId : ''
      })
    }
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {
    Tips.loading();
    this.getVoteUserDetail(this.data.id, this.data.userId);
  }

})
