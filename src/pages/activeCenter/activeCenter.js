// import activityApi from '../../api/activity.js';
// import timeFormat from '../../utils/timeFormat.js';
import question from '../../api/question.js';
import vote from '../../api/vote.js';
import activityApi from '../../api/activity.js';
import timeFormat from '../../utils/timeFormat.js';


// 创建页面实例对象
Page({
  /**
   * 页面的初始数据
   */
  data: {
    bottomTipsFlag: true,
    tabs: [
      {
        name: '活动'
      },
      {
        name: '投票'
      },
      {
        name: '问卷'
      }
    ],
    activeIndex: 0,
    navSlider: {
      width: '40rpx',
      left: '106rpx',
      position: ['106rpx', '356rpx', '604rpx']
    },
    messageComponent: '',
    voteList: [],
    pageVote: {
      pageNum: 1,
      pageSize: 10,
      total: 1
    },
    questionList: [],
    pageQuestion: {
      pageNum: 1,
      pageSize: 10,
      total: 1
    },
    tagList: [],
    // 活动标签高亮index
    tagIndex: 0,
    tagId: '0',
    activityList: [],
    page: {
      pageNum: 1,
      pageSize: 10,
      total: 1
    },
    scrollLeft: 0,
    // 屏幕的宽度
    screenWidth: null,
    organizeId: ''
  },

  // tab切换
  handleTabSlider(data) {
    let current = data.detail;
    this.setData({
      activeIndex: current
    });
  },

  // nav-slider 的位置
  sliderPosition(index) {
    this.setData({
      'navSlider.left': this.data.navSlider.position[index]
    })
  },

  // 改变 swiper 手势滑动
  swiperChange(event) {
    let current = event.detail.current;
    this.setData({
      activeIndex: current
    });
    this.sliderPosition(current);
    if (current == 2) {
      this.getQuestionList(1)
    } else if (current == 1) {
      this.getVoteList(1)
    } else if (current == 0) {
      this.getActivityList(this.data.tagId, 1)
    }
  },

  // 获取投票列表数据
  getVoteList(pageNum) {
    let params = {
      pageNum: pageNum
    };
    if (pageNum === 1) this.setData({ voteList: [] });
    vote.voteActivityList(params).then(res => {
      if (res.data.flag) {
        let ret = res.data;
        console.log(ret);
        ret.data.forEach((item) => {
          item.voteStartTimeStr = item.voteStartTime.replace(/-/g, '.');
          item.voteEndTimeStr = item.voteEndTime.replace(/-/g, '.');
          item.picPathUrl = item.picPath.split(',')[0];
        });
        this.setData({
          voteList: [...this.data.voteList, ...ret.data],
          pageVote: ret.page
        });
        console.log(this.data.voteList)
      } else {
        this.messageComponent.applyActive(res.data.msg || '加载数据失败')
      }
    }).catch(err => {
      console.log(err)
    })
  },

  // 加载更多投票列表数据
  loadMoreVote(e) {
    this.getVoteList(e.detail)
  },

  // 获取问卷列表数据
  getQuestionList(pageNum) {
    let params = {
      pageNum: pageNum
    };
    if (pageNum === 1) this.setData({ questionList: [] });
    question.questionList(params).then(res => {
      if (res.data.flag) {
        let ret = res.data;
        console.log(ret);
        ret.data.forEach((item) => {
          item.quesStartTimeStr = item.startTime.replace(/-/g, '.');
          item.quesEndTimeStr = item.endTime.replace(/-/g, '.');
          item.picPathUrl = item.restfulUrl.split(',')[0];
        });
        this.setData({
          questionList: [...this.data.questionList, ...ret.data],
          pageQuestion: ret.page
        });
        console.log(this.data.questionList)
      } else {
        this.messageComponent.applyActive(res.data.msg || '加载数据失败')
      }
    }).catch(err => {
      console.log(err)
    })
  },

  // 加载更多问卷列表数据
  loadMoreQuestion(e) {
    this.getQuestionList(e.detail)
  },

  // 获取活动列表
  getActivityList(tagId, page) {
    let params = {
      tagId: tagId,
      pageNum: page
    };
    if (page === 1) this.setData({ activityList: [] });
    activityApi.activityList(params).then(res => {
      if (res.data.flag) {
        let ret = res.data;
        console.log(ret);
        this.handleTags(ret.tagList);
        ret.data.forEach((item) => {
          this.handleFormatData(item)
        });
        this.setData({
          activityList: [...this.data.activityList, ...ret.data],
          page: ret.page
        });
        console.log(this.data.activityList)
      } else {
        this.messageComponent.applyActive(res.data.msg || '加载数据失败')
      }
    }).catch(err => {
      console.log(err)
    })
  },

  // 活动上拉加载更多
  loadMore(e) {
    this.getActivityList(this.data.tagId, e.detail)
  },

  // 处理活动列表数据
  handleFormatData(item) {
    item.activityEndtimeStr = timeFormat.timeStr(item.activityEndtime, 'yyyy.MM.dd');
    item.activityStarttimeStr = timeFormat.timeStr(item.activityStarttime, 'yyyy.MM.dd');
    if (item.memberSignupState == '待报名') {
      item.type = '1';
      item.isDisabled = true
    } else if (item.memberSignupState == '去报名') {
      item.type = '2';
      item.isDisabled = false
    } else if (item.memberSignupState == '去参加') {
      item.type = '3';
      item.isDisabled = false
    } else if (item.memberSignupState == '已结束') {
      item.type = '4';
      item.isDisabled = true
    }
  },

  // 处理标签
  handleTags(data) {
    data.push({
      id: -1,
      tagName: '其他'
    });
    data.unshift({
      id: 0,
      tagName: '全部'
    });

    this.setData({
      tagList: data
    });
    console.log(this.data.tagList)
  },

  // 选择活动标签
  handleChooseTag(e) {
    let index = e.target.dataset.index;
    let tagId = e.target.dataset.id;
    let query = wx.createSelectorQuery();
    // 元素离最左边的距离
    let offsetLeft = e.target.offsetLeft;
    // 屏幕宽度
    let screenWidth = this.data.screenWidth;
    // 中间的位置
    let sliderCenter = screenWidth / 2;
    // 点击的标签的宽度
    let boxWidth = null;
    query.selectAll('.activity-tags .tag').boundingClientRect(function(res) {
      boxWidth = res[index].width;
      res.width
    }).exec()
    let y = offsetLeft % screenWidth;
    let n = Math.floor(offsetLeft / screenWidth);
    let scrollLeft = screenWidth * n + ((y - sliderCenter) + boxWidth / 2 + 30);
    this.setData({
      tagIndex: index,
      tagId: tagId,
      page: {
        pageNum: 1,
        pageSize: 10,
        pages: 1,
        total: 1
      },
      activityList: [],
      scrollLeft: scrollLeft
    });
    this.getActivityList(this.data.tagId, this.data.page.pageNum)
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad() {
    // TODO: onLoad
    this.messageComponent = this.selectComponent('.messageTips');
    this.getActivityList(this.data.tagId, 1)
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {
    // TODO: onReady
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {
    // TODO: onShow
    let organizeId =  wx.getStorageSync('organizeId');
    this.setData({
      screenWidth: wx.getSystemInfoSync().windowWidth,
      organizeId: organizeId
    });
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {
    // TODO: onHide
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {
    // TODO: onUnload
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {
    // TODO: onPullDownRefresh
  },

  /**
   * 页面相关分享
   */
  onShareAppMessage() {
    
  }
});
