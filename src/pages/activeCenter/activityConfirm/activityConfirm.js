// 获取全局应用程序实例对象
// const app = getApp()

import activityApi from '../../../api/activity.js';
import userApi from '../../../api/user.js';
import Tips from  '../../../utils/tip.js';
import benMath from '../../../utils/BenMath';
import timeFormat from '../../../utils/timeFormat.js';
import nBack from '../../../utils/wx_navigateback.js';
import wxtemplate from '../../../api/wxtemplate'

function check(obj) {
  var regStrs = [
    ['^0(\\d+)$', '$1'],
    ['[^\\d\\.]+$', ''],
    ['\\.(\\d?)\\.+', '.$1'],
    ['^(\\d+\\.\\d{2}).+', '$1']
  ];
  for (var i = 0; i < regStrs.length; i++) {
    var reg = new RegExp(regStrs[i][0]);
    obj.value = obj.value.replace(reg, regStrs[i][1]);
  }
}

// 创建页面实例对象
Page({
  /**
   * 页面的初始数据
   */
  data: {
    title: 'activityConfirm',
    inputFocus: false,
    showCenterDialog: false,
    activityId: '',
    activityDetail: {},
    messageComponent: '',
    accountBalance: '',
    // 手动输入金额
    inputAccountBalance: '',
    // 订单编号
    outTradeNo: '',
    // 还需微信付款的钱
    enrollFee: '',
    // 活动支付的现金
    orderFee: '',
    // 还需付的金额
    needFee: '',
    // 全额还是部分
    payType: '',
    transMoney: '',
    record: {},
    // 输入框是否能输入
    disabled: '',
    // 预付卡是否冻结成功
    isPay: '',
    isDisabled: '',
    formId: '',  // 用于推送模板消息
    activityIds: ''
  },

  // 金额输入框获得焦点
  handleInputFocus() {
    this.setData({
      inputFocus: true
    })
  },

  // 手动输入金额
  handleInputValue(e) {
    check(e.detail);
    console.log(e.detail.value)
    let inputAccountBalance = e.detail.value;
    let orderFee = this.data.orderFee;
    let accountBalance = this.data.accountBalance;
    if (accountBalance >= orderFee) {
      inputAccountBalance = Number(inputAccountBalance) > orderFee ? orderFee : inputAccountBalance;
    } else {
      inputAccountBalance = Number(inputAccountBalance) > accountBalance ? accountBalance : inputAccountBalance;
    }
    let needFee = benMath.accSub(this.data.orderFee, e.detail.value);
    this.setData({
      inputAccountBalance: inputAccountBalance,
      transMoney: inputAccountBalance,
      needFee: needFee >= 0 ? needFee : 0,
      enrollFee: needFee >= 0 ? needFee : 0
    })
  },

  // 金额输入框失去焦点
  handleInputBlur() {
    this.setData({
      inputFocus: false
    })

    // 校验输入的金额和账户的余额
    if (this.data.inputAccountBalance > this.data.accountBalance) {
      this.messageComponent.applyActive('余额不足~');
      this.setData({
        inputAccountBalance: ''
      })
    } else if (this.data.inputAccountBalance > this.data.orderFee) {
      this.messageComponent.applyActive('输入的余额过大~');
      this.setData({
        inputAccountBalance: this.data.orderFee
      })
    } else {
      this.setData({
        needFee: benMath.accSub(this.data.orderFee, this.data.inputAccountBalance)
      })
    }
  },

  // 点击去支付按钮
  handlePayDialog() {
    // 如果已经生成了订单
    if (this.data.orderData) {
      // 如果有长益的订单
      if (this.data.cyOrderData) {
        // 全额用预付卡
        if (this.data.cyOrderData.transMoney == this.data.orderFee) {
          // 没有冻结成功
          if (this.data.cyOrderData.state == null) {
            this.setData({
              showCenterDialog: true,
              payType: 'ALL'
            })
          } else if (this.data.cyOrderData.state == 'PREPAID') {
            // 支付回调
            let params = {
              orderNo: this.data.outTradeNo
            }
            activityApi.getPayDataSuccess(params).then(ress => {
              console.log(ress)
              if (ress.data.flag) {
                Tips.success('报名成功', 1000).then(() => {
                  nBack.setNavigateBackData({ paySuccess: true }, 1)
                })
                // wxtemplate.activityTemplate(this.data.formId, this.data.activityDetail.marketingActivitySignupId)
              } else {
                this.messageComponent.applyActive(ress.data.msg || '报名失败')
              }
            }).catch(errs => {
              console.log(errs)
            })
          }
          // 部分预付卡
        } else {
          if (this.data.cyOrderData.state == null) {
            this.setData({
              showCenterDialog: true,
              payType: 'PART'
            })
          } else if (this.data.cyOrderData.state == 'PREPAID') {
            wx.navigateTo({
              url: `../activityPay/activityPay?orderNo=${this.data.outTradeNo}&totalFee=${this.data.enrollFee}&payType=ACTIVITY&activityId=${this.data.activityIds}`
            })
          }
        }
      } else {
        wx.navigateTo({
          url: `../activityPay/activityPay?orderNo=${this.data.outTradeNo}&totalFee=${this.data.enrollFee}&payType=ACTIVITY&activityId=${this.data.activityIds}`
        })
      }
    } else {
      // 余额为0的时候，直接走微信支付渠道
      if (this.data.accountBalance == 0) {
        this.createActivityOrder().then(() => {
          wx.navigateTo({
            url: `../activityPay/activityPay?orderNo=${this.data.outTradeNo}&totalFee=${this.data.enrollFee}&payType=ACTIVITY&activityId=${this.data.activityIds}`
          })
        })
        // 余额不为0的时候
      } else {
        // 没有输入金额
        if (!this.data.inputAccountBalance || this.data.inputAccountBalance == 0) {
          this.createActivityOrder().then(() => {
            wx.navigateTo({
              url: `../activityPay/activityPay?orderNo=${this.data.outTradeNo}&totalFee=${this.data.enrollFee}&payType=ACTIVITY&activityId=${this.data.activityIds}`
            })
          })
          // 有输入余额
        } else {
          // 输入的余额大于等于订单的总价（全额付款）
          if (this.data.inputAccountBalance >= this.data.orderFee) {
            this.createActivityOrder().then(() => {
              this.setData({
                showCenterDialog: true,
                payType: 'ALL'
              })
            })
          } else {
            this.createActivityOrder().then(() => {
              this.setData({
                showCenterDialog: true,
                payType: 'PART'
              })
            })
          }
        }
      }
    }
  },

  // 输入完密码
  handlePayPassWord(e) {
    // 验证密码
    this.checkPassWord(e.detail.value, this.data.payType)
  },

  // 验证支付密码，type:ALL全额付款 PART 部分付款
  checkPassWord(password, type) {
    let params = {
      payPassword: password
    }
    userApi.validatePassword(params).then(res => {
      // 密码正确
      if (res.data.flag) {
        this.setData({
          showCenterDialog: false
        });
        // 冻结余额
        let param = {
          orderNo: this.data.outTradeNo,
          organizeId: wx.getStorageSync('organizeId'),
          enrollFee: this.data.transMoney
        }
        activityApi.preparePay(param).then(ret => {
          if (ret.data.flag) {
            this.setData({
              disabled: 1
            })
            if (type == 'PART') {
              wx.navigateTo({
                url: `../activityPay/activityPay?orderNo=${this.data.outTradeNo}&totalFee=${this.data.enrollFee}&payType=ACTIVITY&activityId=${this.data.activityIds}`
              })
            } else if (type == 'ALL') {
              // 支付回调
              let params = {
                orderNo: this.data.outTradeNo
              }
              activityApi.getPayDataSuccess(params).then(ress => {
                if (ress.data.flag) {
                  Tips.success('报名成功', 1000).then(() => {
                    nBack.setNavigateBackData({ paySuccess: true }, 1)
                  })
                  // wxtemplate.activityTemplate(this.data.formId, this.data.activityDetail.marketingActivitySignupId)
                } else {
                  this.messageComponent.applyActive(ress.data.msg || '报名失败,请重试')
                }
              }).catch(errs => {
                console.log(errs)
              })
            }
          } else {
            var that = this;
            wx.showModal({
              title: '提示',
              content: '预付卡在其他地方使用导致余额不足，请重新支付',
              showCancel: false,
              confirmText: '知道了',
              success: function(res) {
                if (res.confirm) {
                  console.log('用户点击确定')
                  that.getUserAmount();
                  that.getSignUpInfo();
                  that.setData({
                    inputAccountBalance: '',
                    transMoney: ''
                  })
                }
              }
            });
          }
        }).catch((err) => {
          console.log(err)
        });
      } else {
        this.messageComponent.applyActive(res.data.msg)
      }
    }).catch(err => {
      console.log(err)
    })
  },

  // 生成活动订单(没有余额，部分余额，全额付款)
  createActivityOrder() {
    return new Promise((resolve, reject) => {
      let activityId = wx.getStorageSync('activityId');
      let realname = wx.getStorageSync('realname');
      let mobile = wx.getStorageSync('mobile');
      let data = wx.getStorageSync('data');
      // 输入的金额(如果是空字符串则不会生成长益的订单)
      let enrollFee = this.data.inputAccountBalance
      let params = {
        id: activityId,
        realname: realname,
        mobile: mobile,
        enrollFee: enrollFee,
        data: data
      }
      activityApi.createActivityOrder(params).then(res => {
        console.log(res)
        if (res.data.flag) {
          this.setData({
            orderData: res.data.data.order,
            cyOrderData: res.data.data.cyOrder ? res.data.data.cyOrder : null,
            outTradeNo: res.data.data.order.tranNo,
            // 微信还需支付的金额
            enrollFee: res.data.data.order.enrollFee
          })

          // 直接生成了微信订单
          if (res.data.data.order && !res.data.data.cyOrder) {
            this.setData({
              disabled: 1
            })
          }
          resolve();
        } else {
          this.messageComponent.applyActive(res.data.msg);
          reject();
        }
      }).catch(err => {
        console.log(err)
      })
    });
  },

  // 点击弹窗关闭按钮
  handlePayClose() {
    console.log('guanbi')
  },

  // 点击忘记密码
  handleForgetPassword() {
    console.log('wangji')
  },

  // 获取报名信息
  getSignUpInfo() {
    let params = {
      id: this.data.activityId
    }
    Tips.loading()
    activityApi.signUpRecord(params).then(res => {
      console.log(res)
      if (res.data.flag) {
        let t = res.data.data.activity
        t.activityStarttimeStr = timeFormat.timeStr(t.activityStarttime, 'yyyy.MM.dd hh:mm')
        t.activityEndtimeStr = timeFormat.timeStr(t.activityEndtime, 'yyyy.MM.dd hh:mm')
        this.setData({
          activityDetail: res.data.data.activity,
          orderFee: res.data.data.activity.enrollFee,
          needFee: res.data.data.activity.enrollFee,
          record: res.data.data.record,
          activityIds: res.data.data.activity.marketingActivitySignupId
        })

        // 如果有订单(不一定会生成长益订单)
        if (res.data.data.order) {
          this.setData({
            orderData: res.data.data.order,
            cyOrderData: res.data.data.cyOrder ? res.data.data.cyOrder : null,
            outTradeNo: res.data.data.order.tranNo,
            // 微信还需支付的金额
            enrollFee: res.data.data.order.enrollFee
          })
          if (res.data.data.cyOrder) {
            if (res.data.data.cyOrder.state == 'PREPAID') {
              this.setData({
                disabled: 1,
                inputAccountBalance: res.data.data.cyOrder.transMoney,
                needFee: benMath.accSub(this.data.orderFee, res.data.data.cyOrder.transMoney)
              })
            } else if (res.data.data.cyOrder.state == null) {
              this.setData({
                inputAccountBalance: '',
                needFee: this.data.orderFee
              })
            }
          } else {
            this.setData({
              disabled: 1
            })
          }
        }
      } else {
        this.messageComponent.applyActive(res.data.msg)
      }
      Tips.loaded()
    }).catch((err) => {
      console.log(err)
      Tips.loaded()
    })
  },

  // 获取用户余额
  getUserAmount() {
    userApi.queryAmount().then(res => {
      console.log(res)
      if (res.data.flag) {
        this.setData({
          accountBalance: res.data.data.accountBalance
          // accountBalance: 300
        })
      } else {
        this.messageComponent.applyActive(res.data.msg)
      }
    }).catch(err => {
      console.log(err)
    })
  },
  // 提交表单获取formId
  formSubmit(e) {
    console.log('form发生了submit事件，携带数据为：', e)
    this.setData({ formId: e.detail.formId })
    wxtemplate.formSubmit(e).then(res => {
      this.setData({ formId: res })
    }).catch(() => {
      console.log('formId undefined')
    })
  },
  formReset() {
    console.log('form发生了reset事件')
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(e) {
    // TODO: onLoad
    this.messageComponent = this.selectComponent('.messageTips')
    this.setData({
      activityId: e.id
    })
    if (e.transMoney) {
      this.setData({
        transMoney: e.transMoney,
        payType: 'ALL',
        outTradeNo: e.outTradeNo
      })
    }
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {
    // TODO: onReady
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {
    // TODO: onShow
    this.getSignUpInfo()
    this.getUserAmount()
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {
    // TODO: onHide
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {
    // TODO: onUnload
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {
    // TODO: onPullDownRefresh
  }
})
