// 获取全局应用程序实例对象
const app = getApp()

import activityApi from '../../../api/activity.js';
import shareLog from '../../../api/shareLog.js';
import WxParse from '../../../wxParse/wxParse.js'
import timeFormat from '../../../utils/timeFormat.js';
import Tips from '../../../utils/tip';
import query from '../../../utils/query.js';
import GetPage from '../../../utils/getCurrentPageUrl'

// 创建页面实例对象
Page({
  /**
   * 页面的初始数据
   */
  data: {
    title: 'activityDetail',
    type: '',
    activityId: '',
    // 活动详情对象
    activityDetail: {},
    memberType: '',
    messageComponent: '',
    enrollFee: '',
    tranNo: '',
    orderData: null,
    cyOrderData: null,
    info: {}
  },

  // 去报名
  handleEnterActivity() {
    let type = this.data.type;
    let acivityType = this.data.activityDetail.acivityType;
    // 非会员
    if (this.data.memberType !== 'MEMBER') {
      wx.navigateTo({
        url: `../../memberSign/memberSign`
      })
    } else {
      if (type == '3') {
        this.handleCheckActivity();
      } else {
        if (type != '2' || acivityType == 'NONEED') return;
        wx.navigateTo({
          url: '../activityEnter/activityEnter?type=1&id=' + this.data.activityId + ''
        });
      }
    }
  },

  // 去查看报名信息
  handleCheckActivity() {
    wx.navigateTo({
      url: '../activityEnter/activityEnter?type=2&id=' + this.data.activityId + ''
    })
  },

  // 获取活动详情
  getActivityDetail() {
    let params = {
      id: this.data.activityId
    };
    Tips.loading();
    activityApi.activityDetails(params).then(res => {
      if (res.data.flag) {
        let t = res.data.data.signupDo;
        let m = res.data.data.member;
        t.activityStarttimeStr = timeFormat.timeStr(res.data.data.signupDo.activityStarttime, 'yyyy.MM.dd hh:mm');
        t.activityEndtimeStr = timeFormat.timeStr(res.data.data.signupDo.activityEndtime, 'yyyy.MM.dd hh:mm');
        t.signupStartTimeStr = timeFormat.timeStr(res.data.data.signupDo.startTime, 'yyyy.MM.dd hh:mm');
        t.signupEndTimeStr = timeFormat.timeStr(res.data.data.signupDo.endTime, 'yyyy.MM.dd hh:mm');
        this.setData({
          activityDetail: res.data.data.signupDo,
          memberType: res.data.data.memberType,
          orderData: res.data.data.order ? res.data.data.order : null,
          cyOrderData: res.data.data.cyOrder ? res.data.data.cyOrder : null,
          signupId: res.data.data.signupStatId ? res.data.data.signupStatId : '',
          info: {
            id: this.data.activityId,
            picturePath: t.activityPicture,
            commodityName: t.activityTitle,
            price: this.handlePrice(t),
            qrcodeUrl: m.qrcodeUrl,
            memberHead: m.memberHead,
            memberNickName: m.memberNickName,
            // logoUrl: app.globalData.defaultConfig.defaultPic,
            organizeName: wx.getStorageSync('organizeName'),
            pageUrl:`${GetPage.getCurrentPageUrl()}`
          }
        });
        let _this = this;
        let article = res.data.data.signupDo.activityIntroduce;
        WxParse.wxParse('article', 'html', article, _this, 15);
        if (res.data.data.order) {
          this.setData({
            tranNo: res.data.data.order.tranNo,
            enrollFee: res.data.data.order.enrollFee
          })
        }
        // 根据状态显示按钮
        this.handleFormatData(res.data.data.signupDo)
        app.updateLocalOrganize(this.data.activityDetail.organizeId)
      } else {
        this.messageComponent.applyActive(res.data.msg)
      }
      Tips.loaded()
    }).catch(() => {
      this.messageComponent.applyActive('获取数据异常!');
      Tips.loaded()
    })
  },

  // 处理价格
  handlePrice(data) {
    return data.acivityType == 'FREE'
      ? '免费' : data.acivityType == 'SCORE'
        ? `${data.enrollScore}积分` : data.acivityType == 'CASH'
          ? `￥${data.enrollFee}` : data.acivityType == 'MIXED'
            ? `${data.enrollScore}积分+${data.enrollFee}元` : `无需报名`
  },

  // 处理数据
  handleFormatData(item) {
    if (item.memberSignupState == '待报名') {
      this.setData({
        type: '1'
      })
    } else if (item.memberSignupState == '去报名') {
      this.setData({
        type: '2'
      })
    } else if (item.memberSignupState == '去参加') {
      this.setData({
        type: '3'
      })
    } else if (item.memberSignupState == '报名已结束') {
      this.setData({
        type: '4'
      })
    } else if (item.memberSignupState == '活动已结束') {
      this.setData({
        type: '6'
      })
    }

    // 名额已满 并且没有报名记录 活动没有下架
    if (item.signupLimitNumber != 0 && item.signupLimitNumber - item.signupNumber <= 0 && !this.data.signupId && item.signupState != 'OFFLINE') {
      this.setData({
        type: '5'
      })
    }

    // 活动已下架 并且没有报名记录
    if (item.signupState == 'OFFLINE' && !this.data.signupId) {
      this.setData({
        type: '7'
      })
    }

    this.setData({
      activityDetail: item
    });
  },

  // 分享
  onShareAppMessage() {
    shareLog.shareLogCommon(this.data.activityId, 'ACTIVITY_SIGNUP');
    let organizeId = wx.getStorageSync('organizeId'),  organizeName = wx.getStorageSync('organizeName');
    return {
      path: '/pages/activeCenter/activityDetail/activityDetail?id=' + this.data.activityId + '&organizeId=' + organizeId + '&organizeName=' + organizeName + '&share=true'
    }
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(e) {
    console.log('活动启动参数：', e);
    this.messageComponent = this.selectComponent('.messageTips');
    if (e.scene) {
      let scene = decodeURIComponent(e.scene);
      console.log('解析场景值：', scene);
      let id = query.getQueryString('id', scene);
      this.setData({
        activityId: id
      });
    } else {
      this.setData({
        activityId: e.id
      });
    }
    this.getActivityDetail();
  }
});
