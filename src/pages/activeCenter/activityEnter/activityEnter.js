// 获取全局应用程序实例对象
const app = getApp();
import activityApi from '../../../api/activity.js';
import { PHONE, IDCARD, EMAIL, AGE } from  '../../../utils/regular.js'
import Tips from  '../../../utils/tip.js'
import timeFormat from '../../../utils/timeFormat.js';
import nBack from '../../../utils/wx_navigateback.js';
import wxtemplate from '../../../api/wxtemplate';
import wxPay from '../../../utils/wxPay.js';

// 创建页面实例对象
Page({
  /**
   * 页面的初始数据
   */
  data: {
    title: 'activityEnter',
    // 状态1去报名2查看报名3名额不足4继续报名
    type: '',
    activityId: '',
    activityDetail: {},
    realName: '',
    phone: '',
    realNameTips: '',
    phoneTips: '',
    activityType: '',
    activityScore: '',
    messageComponent: '',
    btnDisabled: false,
    // 活动报名选项
    optionList: null,
    imgList: [],
    optionListForm: [],
    optionListFormData: '',
    // 活动记录id
    signupId: '',
    memberValues: [],
    tranNo: '',
    orderData: null,
    cyOrderData: null,
    // 订单编号
    outTradeNo: '',
    // 还需微信付款的钱
    enrollFee: '',
    // 活动支付的现金
    orderFee: '',
    // 还需付的金额
    needFee: '',
    // 全额还是部分
    payType: '',
    transMoney: '',
    formId: ''  // 用于推送模板消息
  },

  // 姓名
  handleInputName(e) {
    this.setData({
      realName: e.detail.value
    });
  },

  // 手机号
  handleInputPhone(e) {
    this.setData({
      phone: e.detail.value
    })
  },

  // 单行文本&多行文本输入
  singleInputValue(e) {
    let optionListForm = this.data.optionListForm;
    optionListForm[e.currentTarget.dataset.index].value = e.detail.value;
    this.setData({
      optionListForm: optionListForm
    });
  },

  // 单选框
  radioChange(e) {
    let optionListForm = this.data.optionListForm;
    optionListForm[e.currentTarget.dataset.index].value = e.detail.value;
    let currentItemList = optionListForm[e.currentTarget.dataset.index].itemList.find((value) => {
      return value.title == e.detail.value
    });
    optionListForm[e.currentTarget.dataset.index].optionValueIds = currentItemList.id;
    this.setData({
      optionListForm: optionListForm
    });
  },

  // 上传图片
  handleUpLoad(e) {
    let that = this;
    let hostName = wx.getStorageSync('hostName') || app.data.hostName;
    wx.chooseImage({
      count: 1, // 默认9
      sizeType: ['original', 'compressed'],
      sourceType: ['album', 'camera'],
      success: function(res) {
        console.log(res);
        res.tempFilePaths.forEach((item) => {
          wx.uploadFile({
            url: `${hostName}/wechat/wechatCommon/saveImgToOSS.json`,
            filePath: item,
            name: 'image',
            header: { 'Content-Type': 'multipart/form-data' },
            formData: {
              'appType': 'smallApp',
              'hash': wx.getStorageSync('hash')
            },
            success: function(res) {
              let ret = JSON.parse(res.data);
              if (ret.flag) {
                let imgList = that.data.imgList;
                let data = ret.data.substr(0, ret.data.length - 1);
                let optionListForm = that.data.optionListForm;
                optionListForm[e.currentTarget.dataset.index].value = data;
                imgList.push(data);
                that.setData({
                  imgList: imgList,
                  optionListForm: optionListForm
                });
                console.log(that.data.optionListForm);
              } else {
                wx.showModal({
                  title: '提示',
                  content: '上传失败',
                  showCancel: false
                });
              }
            },
            fail: function() {
              wx.showModal({
                title: '提示',
                content: '上传失败',
                showCancel: false
              })
            },
            complete: function() {
              wx.hideToast();
            }
          })
        })
      }
    })
  },

  // 删除图片
  handleCancelImg(e) {
    // 上传图片所在列的位置
    let index = e.currentTarget.dataset.index;
    // 删除的当前图片所在的位置
    let idx = e.currentTarget.dataset.idx;
    let imgList = this.data.imgList;
    let optionListForm = this.data.optionListForm;
    imgList.splice(idx, 1);
    optionListForm[index].value = '';
    this.setData({
      optionListForm: optionListForm,
      imgList: imgList
    });
  },

  // 选择生日
  bindDateChange(e) {
    let optionListForm = this.data.optionListForm;
    optionListForm[e.currentTarget.dataset.index].value = e.detail.value;
    this.setData({
      optionListForm: optionListForm
    });
  },

  // 获取报名记录
  signUpRecord(id) {
    activityApi.signUpRecord({ id: id }).then(res => {
      console.log(res);
      if (res.data.flag) {
        // this.setData({
        //   memberValues: res.data.data.memberValues
        // });
        // let memberValues = this.data.memberValues;
        // let optionListForm = this.data.optionListForm;
        // if (!memberValues) return;
        // memberValues.forEach((val, i) => {
        //   optionListForm.forEach((item, j) => {
        //     if (i == j) {
        //       val.itemList = item.itemList
        //     }
        //   })
        // });
        // this.setData({
        //   memberValues: memberValues
        // });
      } else {
        this.messageComponent.applyActive(res.data.msg)
      }
    })
  },

  // 保存报名选项optionList的值
  saveOptionList() {
    let data = '';
    this.data.optionListForm.forEach((item) => {
      data += `${item.optionValueIds}|${item.value}|${item.id}|${item.title};`
    });
    this.setData({
      optionListFormData: data.substr(0, data.length - 1)
    })
  },

  // 表单验证
  validate() {
    let optionListForm = this.data.optionListForm;
    // 姓名
    if (!this.data.realName) {
      this.setData({
        realNameTips: '请输入您的姓名'
      });
      return false
    } else {
      this.setData({
        realNameTips: ''
      })
    }

    // 手机号码
    if (!this.data.phone) {
      this.setData({
        phoneTips: '请输入您的手机号码'
      });
      return false
    } else if (!PHONE.test(this.data.phone)) {
      this.setData({
        phoneTips: '您输入的手机号码不正确'
      });
      return false
    } else {
      this.setData({
        phoneTips: ''
      })
    }

    if (optionListForm.length <= 0) return true;
    for (let i = 0; i < optionListForm.length; i++) {
      let itemType = optionListForm[i].type;
      let title = optionListForm[i].title;
      if (optionListForm[i].required) {
        if (itemType == 'RADIO') {
          if (optionListForm[i].value == '') {
            optionListForm[i].valueTips = `请选择${title}信息`;
            this.setData({
              optionListForm: optionListForm
            });
            return false;
          } else {
            optionListForm[i].valueTips = '';
            this.setData({
              optionListForm: optionListForm
            })
          }
        } else if (itemType == 'UPLOAD_IMAGE') {
          if (optionListForm[i].value == '') {
            optionListForm[i].valueTips = `请选择图片信息`;
            this.setData({
              optionListForm: optionListForm
            });
            return false;
          } else {
            optionListForm[i].valueTips = '';
            this.setData({
              optionListForm: optionListForm
            })
          }
        } else if (itemType == 'SINGLE' || itemType == 'MULTIPLE') {
          if (optionListForm[i].value == '') {
            optionListForm[i].valueTips = `请填写${title}信息`;
            this.setData({
              optionListForm: optionListForm
            });
            return false;
          } else {
            // 身份验证
            if (title == '身份证号码') {
              if (!IDCARD.test(optionListForm[i].value)) {
                optionListForm[i].valueTips = `请填写正确的身份证号码`;
                this.setData({
                  optionListForm: optionListForm
                });
                return false;
              }
            }
            // 邮箱验证
            if (title == '邮箱') {
              if (!EMAIL.test(optionListForm[i].value)) {
                optionListForm[i].valueTips = `请填写正确的邮箱地址`;
                this.setData({
                  optionListForm: optionListForm
                });
                return false;
              }
            }
            // AGE
            if (title == '年龄') {
              if (!AGE.test(optionListForm[i].value)) {
                optionListForm[i].valueTips = `请填写合法的年龄`;
                this.setData({
                  optionListForm: optionListForm
                });
                return false;
              }
            }

            optionListForm[i].valueTips = '';
            this.setData({
              optionListForm: optionListForm
            })
          }
        }
      }
    }
    return true;
  },

  // 提交表单获取formId
  formSubmit(e) {
    this.setData({ formId: e.detail.formId });
    wxtemplate.formSubmit(e).then(res => {
      this.setData({ formId: res });
      this.handleEnterActivity()
    }).catch(() => {
      this.handleEnterActivity()
    })
  },

  // 去报名
  handleEnterActivity() {
    // 表单验证
    if (!this.validate()) {
      return false
    }

    // 保存表单数据,得到optionListFormData数据
    this.saveOptionList();

    // 校验报名人数
    this.checkSignUpInfo().then(() => {
      let enrollScore = this.data.activityDetail.enrollScore;
      // 需要积分的
      if (enrollScore) {
        Tips.confirm(`您正在使用${enrollScore}积分参与报名，请确认是否立即支付？`, {}, '温馨提示', '再想想', '立即支付').then(() => {
          this.createActivityRecord().then(() => {
            // 积分
            if (this.data.activityType == 'SCORE') {
              Tips.success('报名成功', 1000).then(() => {
                Tips.confirm('您已报名成功，记得到场参加活动哦!', {}, '报名成功', '取消', '去查看').then(() => {
                  wx.setStorageSync('activityList', 'activityList');
                  wx.navigateTo({
                    url: '../../activityList/activityList'
                  })
                }).catch(() => {
                  this.signUpRecord(this.data.signupId);
                  this.setData({ type: 2 })
                })
              });
              // 混合支付
            } else if (this.data.activityType == 'MIXED') {
              this.createActivityOrder().then((res) => {
                this.wxPay(res.orderNo, res.enrollFee, 'ACTIVITY');
              })
            }
          })
        })
      } else {
        this.createActivityRecord().then(() => {
          // 免费
          if (this.data.activityType == 'FREE') {
            Tips.success('报名成功', 1000).then(() => {
              Tips.confirm('活动入场码已放置于‘我的-活动’，记得到场参加活动哦!', {}, '报名成功', '取消', '去查看').then(() => {
                wx.setStorageSync('activityList', 'activityList');
                wx.navigateTo({
                  url: '../../activityList/activityList'
                })
              }).catch(() => {
                this.signUpRecord(this.data.signupId);
                this.setData({ type: 2 })
              })
            });
            // 现金支付
          } else if (this.data.activityType == 'CASH') {
            this.createActivityOrder().then((res) => {
              this.wxPay(res.orderNo, res.enrollFee, 'ACTIVITY');
            })
          }
        })
      }
    });
  },

  // 生成活动记录
  createActivityRecord() {
    return new Promise((resolve, reject) => {
      let params = {
        realname: this.data.realName,
        mobile: this.data.phone,
        id: this.data.activityId,
        data: this.data.optionListFormData
      };
      Tips.loading('报名中');
      if (this.data.btnDisabled) return;
      this.setData({ btnDisabled: true });
      activityApi.signUpSubmit(params).then(res => {
        Tips.loaded();
        this.setData({
          btnDisabled: false
        });
        if (res.data.flag) {
          this.setData({
            signupId: res.data.data.data
          });
          resolve();
        } else {
          this.messageComponent.applyActive(res.data.msg);
          reject();
        }
      }).catch(err => {
        console.log(err);
        reject();
      });
    });
  },

  // 生成订单（用于现金报名）
  createActivityOrder() {
    return new Promise((resolve, reject) => {
      let activityId = this.data.activityId;
      let realname = this.data.realName;
      let mobile = this.data.phone;
      let data = this.data.optionListFormData;
      // 输入的金额(如果是空字符串则不会生成长益的订单)
      // let enrollFee = this.data.inputAccountBalance;
      let enrollFee = '';
      let params = {
        id: activityId,
        realname: realname,
        mobile: mobile,
        enrollFee: enrollFee,
        data: data
      };
      activityApi.createActivityOrder(params).then(res => {
        console.log(res);
        if (res.data.flag) {
          this.setData({
            orderData: res.data.data.order,
            cyOrderData: res.data.data.cyOrder ? res.data.data.cyOrder : null,
            outTradeNo: res.data.data.order.tranNo,
            // 微信还需支付的金额
            enrollFee: res.data.data.order.enrollFee
          });

          // 直接生成了微信订单
          if (res.data.data.order && !res.data.data.cyOrder) {
            this.setData({
              disabled: 1
            })
          }
          resolve({
            orderNo: res.data.data.order.tranNo,
            enrollFee: res.data.data.order.enrollFee
          });
        } else {
          this.messageComponent.applyActive(res.data.msg);
          reject();
        }
      }).catch(err => {
        console.log(err)
      })
    });
  },

  // 校验积分、余额、报名人数
  checkSignUpInfo() {
    return new Promise((resolve, reject) => {
      let params = {
        id: this.data.activityId
      };
      activityApi.activitySignUp(params).then(res => {
        if (res.data.flag) {
          let signupLimitNumber = res.data.data.signupActivity.signupLimitNumber;
          let signupNumber = res.data.data.signupActivity.signupNumber;
          // 校验报名人数
          if (signupLimitNumber && signupLimitNumber - signupNumber <= 0) {
            this.messageComponent.applyActive('真可惜，就慢了那么一点点~');
            this.setData({ type: '3' });
            reject();
          } 
        //   else if (res.data.data.member.balanceScore < res.data.data.signupActivity.enrollScore) {
        //     this.messageComponent.applyActive('您的积分不足，无法报名~');
        //     reject();
        //   } 
          else {
            resolve();
          }
        } else {
          this.messageComponent.applyActive(res.data.msg);
          reject();
        }
      }).catch(err => {
        console.log(err);
        reject();
      })
    })
  },

  // 获取报名信息
  getSignUpInfo(activityId) {
    return new Promise((resolve, reject) => {
      let params = {
        id: activityId
      };
      Tips.loading();
      activityApi.activitySignUp(params).then(res => {
        if (res.data.flag) {
          let t = res.data.data.signupActivity;
          t.activityStarttimeStr = timeFormat.timeStr(t.activityStarttime, 'yyyy.MM.dd hh:mm');
          t.activityEndtimeStr = timeFormat.timeStr(t.activityEndtime, 'yyyy.MM.dd hh:mm');
          this.setData({
            activityDetail: res.data.data.signupActivity,
            activityType: res.data.data.signupActivity.acivityType,
            activityScore: res.data.data.signupActivity.enrollScore,
            // balanceScore: res.data.data.member.balanceScore,
            phone: res.data.data.signupStatistics == null ? res.data.data.member.memberPhone : res.data.data.signupStatistics.phone,
            realName: res.data.data.signupStatistics != null ? res.data.data.signupStatistics.name : this.data.realName ? this.data.realName : '',
            signupId: res.data.data.signupId,
            orderData: res.data.data.order ? res.data.data.order : null,
            cyOrderData: res.data.data.cyOrder ? res.data.data.cyOrder : null
          });
  
          // 如果optionList有值并且optionListForm值为空（防止上传图片刷新）
          if (res.data.data.optionList && this.data.optionListForm.length == 0) {
            let optionListFormArr = [];
            res.data.data.optionList.forEach((item) => {
              let optionListForm = {};
              optionListForm.id = item.id;
              optionListForm.required = item.required;
              optionListForm.itemList = item.itemList;
              optionListForm.title = item.title;
              optionListForm.type = item.type;
              optionListForm.value = '';
              optionListForm.optionValueIds = item.id;
              optionListForm.valueTips = '';
              optionListFormArr.push(optionListForm)
            });
  
            this.setData({
              optionList: res.data.data.optionList,
              optionListForm: optionListFormArr
            });
            console.log(this.data.optionListForm)
          }
  
          // 有报名记录
          if (res.data.data.signupId) {
            this.signUpRecord(res.data.data.signupId);
            let memberSignupState = res.data.data.signupActivity.memberSignupState;
            // 有订单
            if (memberSignupState == '去参加') {
              this.setData({
                type: 2
              })
            } else if (memberSignupState == '去报名') {
              this.setData({
                type: 4
              })
            }
          }
  
          // 如果有订单(不一定会生成长益订单)
          if (res.data.data.order) {
            this.setData({
              orderData: res.data.data.order,
              cyOrderData: res.data.data.cyOrder ? res.data.data.cyOrder : null,
              outTradeNo: res.data.data.order.tranNo,
              // 微信还需支付的金额
              enrollFee: res.data.data.order.enrollFee
            })
          }
          resolve();
        } else {
          this.messageComponent.applyActive(res.data.msg)
          reject();
        }
        Tips.loaded()
        reject();
      }).catch((err) => {
        console.log(err);
        Tips.loaded()
        reject();
      })
    });
  },

  // 继续报名（防止用户取消支付操作）
  async handleContinueJoin() {
    // 重新获取一次活动报名状态 
    const res = await this.getSignUpInfo(this.data.activityId);
    // 若已关闭则提示相应信息
    if (!this.data.orderData || this.data.orderData.state != 0) {
      this.messageComponent.applyActive('订单状态异常，请刷新后重试！')
      return;
    }

    // 长益订单和微信订单
    if (this.data.cyOrderData && this.data.orderData) {
      let orderData = this.data.orderData;
      let orderNo = orderData.tranNo;
      let totalFee = orderData.enrollFee;
      let cyOrderData = this.data.cyOrderData;
      // 直接跳入微信支付(部分)
      if (orderData.state == 0 && orderData.enrollFee > 0 && cyOrderData.state == 'PREPAID') {
        this.wxPay(orderNo, totalFee, 'ACTIVITY');
      } else {
        wx.navigateTo({
          url: '../activityConfirm/activityConfirm?id=' + this.data.signupId + '&outTradeNo=' + orderNo + ''
        })
      }
    } else if (!this.data.cyOrderData && this.data.orderData) {
      let orderData = this.data.orderData;
      let orderNo = orderData.tranNo;
      let totalFee = orderData.enrollFee;
      // 直接跳入微信支付(部分)
      if (orderData.state == 0 && orderData.enrollFee > 0) {
        this.wxPay(orderNo, totalFee, 'ACTIVITY');
      }
    } else if (!this.data.orderData && !this.data.cyOrderData) {
      wx.navigateTo({
        url: '../activityConfirm/activityConfirm?type=1&id=' + this.data.signupId + ''
      })
    }
  },

  // 微信支付
  wxPay(orderNo, totalFee, orderType) {
    console.log('参数--->' + totalFee + '---' + orderType)
    wxPay.getNewWxPayConfig(orderNo).then(() => {
      this.getSignUpInfo(this.data.activityId);
      Tips.confirm(`可以在我的活动报名中查看`, '', '提示', '取消', '确认').then(() => {
        wx.setStorageSync('activityList', 'activityList');
        wx.navigateTo({
          url: `/pages/activityList/activityList`
        });
      }).catch(() => {
        console.log('取消查看')
      })
    }).catch(() => {
      this.getSignUpInfo(this.data.activityId);
      this.setData({ type: 4 })
    })
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(e) {
    // TODO: onLoad
    let activityId = e.id || wx.getStorageSync('activityId');
    this.messageComponent = this.selectComponent('.messageTips');
    this.setData({
      activityId: activityId,
      type: e.type
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {
    // TODO: onReady
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {
    // TODO: onShow
    this.getSignUpInfo(this.data.activityId);
    if (nBack.getNavigateBackData() && nBack.getNavigateBackData().paySuccess) nBack.paySuccess('activity')
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {
    // TODO: onHide
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {
    // TODO: onUnload
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {
    // TODO: onPullDownRefresh
  }
});
