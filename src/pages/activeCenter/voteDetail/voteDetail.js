// 获取全局应用程序实例对象
const app = getApp()
import voteApi from '../../../api/vote.js';
import WxParse from '../../../wxParse/wxParse.js';
import Tips from '../../../utils/tip.js';
import query from '../../../utils/query.js';
import timeFormat from '../../../utils/timeFormat.js';
import shareLog from '../../../api/shareLog.js';


// 创建页面实例对象
Page({
  /**
   * 页面的初始数据
   */
  data: {
    title: 'activityDetail',
    messageComponent: '',
    // 投票id
    id: '',
    tabs: [
      {
        name: '最新参赛',
        orderType: 1
      },
      {
        name: '投票排行',
        orderType: 2
      },
      {
        name: '最热排行',
        orderType: 3
      }
    ],
    timeText: '',
    countDownTimeText: '',
    activeIndex: 0,
    navSlider: {
      width: '40rpx',
      left: '110rpx',
      position: ['110rpx', '360rpx', '610rpx']
    },
    // 投票图片数组
    voteDetailImg: [],
    // 投票图片是否轮播
    autoPlay: false,
    // 是否显示轮播小点
    dotsIsShow: false,
    // 是否是会员
    isMember: false,
    // 活动状态
    enrollState: null,
    // 投票状态
    voteState: null,
    // 会员等级限制
    gradeName: false,
    keyword: '',
    voteActivity: {},
    voteUser: null,
    voteType: {},
    listNew: [],
    listVote: [],
    listHot: [],
    pageNew: {
      pageNum: 1,
      pageSize: 10,
      total: 1
    },
    pageHot: {
      pageNum: 1,
      pageSize: 10,
      total: 1
    },
    pageVote: {
      pageNum: 1,
      pageSize: 10,
      total: 1
    },
    emptyType: 'voteList',
    memberDialog: false,
    voteDialog: false,
    voteSuccessText: '',
    voteNeedMember: false
  },

  // 滑动tab
  handleTabSlider(data) {
    let current = data.detail;
    this.setData({
      activeIndex: current
    });
  },
  // nav-slider 的位置
  sliderPosition(index) {
    this.setData({
      'navSlider.left': this.data.navSlider.position[index]
    })
  },
  // 改变 swiper 手势滑动
  swiperChange(event) {
    let current = event.detail.current;
    this.setData({
      activeIndex: current
    });
    this.sliderPosition(current);
    // 滑动获取数据
    this.getMoreVoteUser(this.data.tabs[current].orderType, this.data.keyword, 1)
  },

  // 我要报名
  handleJoin() {
    let memberType = wx.getStorageSync('memberType');
    if (memberType !== 'MEMBER') {
      wx.navigateTo({
        url: `/pages/memberSign/memberSign`
      })
      return;
    }
    let params = {
      id: this.data.id
    };
    voteApi.voteActivityDetail(params).then(res => {
      let data = res.data;
      if (data.flag) {
        // 活动已结束
        if (data.voteActivity.enrollState == 2) {
          this.messageComponent.applyActive('该报名活动已结束!');
          return false;
        }
      } else {
        this.messageComponent.applyActive(data.msg || '请求数据失败！')
      }
    }).catch(err => {
      console.log(err)
    });

    if (this.data.needMember) {
      if (!this.data.isMember) {
        this.setData({
          memberDialog: true
        });
        return false;
      } else if (!this.data.gradeName) {
        this.messageComponent.applyActive('亲，您暂不符合参加报名条件!');
        return false;
      }
    }
    wx.navigateTo({
      url: `../voteEnter/voteEnter?id=${this.data.id}`
    })
  },

  // 关闭注册会员弹窗
  handleClose() {
    this.setData({
      memberDialog: false
    });
  },

  // 去注册
  handleSure() {
    wx.navigateTo({
      url: `../../memberSign/memberSign`
    });
    this.setData({
      memberDialog: false
    });
  },

  // 我的报名 跳转到该投票活动的报名人的详情
  handleJumpVoteUser() {
    wx.navigateTo({
      url: `../voteUser/voteUser?id=${this.data.id}`
    })
  },

  // 点击列表跳转到详情
  handleToVoteUser(e) {
    let item = e.currentTarget.dataset.item;
    // 列表中带的openId
    let openId = item.openId;
    // 自己的openId
    let openIdUser = this.data.member.openId;
    if (openId == openIdUser) {
      wx.navigateTo({
        url: `../voteUser/voteUser?id=${this.data.id}`
      })
    } else {
      wx.navigateTo({
        url: `../voteUser/voteUser?userId=${item.id}`
      })
    }
  },

  // 给他人投票
  handleVoteForUser(e) {
    let memberType = wx.getStorageSync('memberType');
    if (this.data.voteNeedMember && memberType !== 'MEMBER') {
      wx.navigateTo({
        url: `/pages/memberSign/memberSign`
      })
      return;
    }
    let item = e.currentTarget.dataset.item;
    let index = e.currentTarget.dataset.index;
    Tips.loading('投票中');
    setTimeout(() => this.voteForUser(item, index), 500);
  },

  // 给别人投票
  voteForUser(item, index) {
    // 列表中带的openId
    let params = {
      openId: item.openId,
      voteActivityId: item.voteActivityId,
      voteTypeId: item.voteTypeId,
      voteUserId: item.id
    };
    voteApi.voteForUser(params).then(res => {
      Tips.loaded();
      if (res.data.flag) {
        this.setData({
          voteDialog: true,
          voteSuccessText: res.data.alertMsg
        });
        this.changeItem(item, index, this.data.activeIndex, res.data.voteFlag);
      } else {
        this.messageComponent.applyActive(res.data.msg || '投票失败')
      }
    }).catch(err => {
      console.log(err)
    })
  },

  // 不能投票改变该列的状态
  changeListStatus() {
    let activeIndex = this.data.activeIndex;
    let list = [];
    if (activeIndex == 0) {
      list = this.data.listNew
    } else if (activeIndex == 1) {
      list = this.data.listVote
    } else {
      list = this.data.listHot
    }
    list.forEach((item) => {
      item.canVote = false
    });
    if (activeIndex == 0) {
      this.setData({
        listNew: list
      })
    } else if (activeIndex == 1) {
      this.setData({
        listVote: list
      })
    } else {
      this.setData({
        listHot: list
      })
    }
  },

  // 领取后 改变 该条 投票的状态
  changeItem(item, index, tabIndex, voteFlag) {
    item.voteNumber = item.voteNumber + 1;
    item.canVote = voteFlag == 1;
    let _voteNum = '';
    let _canVote = '';
    if (tabIndex == 0) {
      _voteNum = `listNew[${index}].voteNumber`;
      _canVote = `listNew[${index}].canVote`;
    } else if (tabIndex == 1) {
      _voteNum = `listVote[${index}].voteNumber`;
      _canVote = `listVote[${index}].canVote`;
    } else if (tabIndex == 2) {
      _voteNum = `listHot[${index}].voteNumber`;
      _canVote = `listHot[${index}].canVote`
    }
    this.setData({
      [_voteNum]: item.voteNumber,
      [_canVote]: item.canVote
    });

    if (voteFlag == 3) {
      this.changeListStatus();
    }
  },

  // 投票成功知道了 || 点击右上角关闭按钮
  handleCloseVote() {
    this.setData({
      voteDialog: false
    });
  },

  // 输入的搜索
  searchInput(e) {
    this.setData({
      keyword: e.detail.value
    })
  },

  selectByName() {
    // 滑动获取数据
    let orderType = this.data.tabs[this.data.activeIndex].orderType;
    this.getMoreVoteUser(orderType, this.data.keyword, 1)
  },

  // 倒计时
  countDownTime(times) {
    if (!times || times < 0) {
      return
    }
    let timeStamp = new Date(times.replace(/-/g, '/')).getTime();
    let timer = null;
    timer = setInterval(() => {
      let now = Date.now();
      let time = (timeStamp - now) / 1000;
      if (time <= 0) {
        this.setData({
          timeText: ''
        });
        // 刷新页面
        this.getVoteDetail(this.data.id);
        clearInterval(timer);
        return
      }
      let s = parseInt((time) % 60);
      let m = parseInt((time / 60) % 60);
      let h = parseInt((time / 60 / 60) % 24);
      let d = parseInt((time / 60 / 60) / 24);
      this.setData({
        timeText: `${d > 0 ? d + '天' : ''}${h < 10 ? '0' + h : h}:${m < 10 ? '0' + m : m}:${s < 10 ? '0' + s : s}`
      })
    }, 1000)
  },

  // 获取投票详情数据
  getVoteDetail(id) {
    let params = {
      id: id
    };
    voteApi.voteActivityDetail(params).then(res => {
      let data = res.data;
      let _this = this;
      if (data.flag) {
        this.setData({
          voteActivity: data.voteActivity,
          voteDetailImg: data.picPaths,
          voteType: data.voteType,
          autoPlay: data.picPaths.length > 1,
          voteState: data.voteState,
          voteUser: data.voteUser ? data.voteUser : null,
          member: data.member,
          // 自己是否会员
          isMember: data.isMember,
          voteNeedMember: data.needMember,
          // 会员等级
          moduleGradeList: data.moduleGradeList,
          // 活动是否有会员限制
          needMember: data.moduleGradeList.length > 0,
          enrollState: data.enrollState ? data.enrollState : null,
          // 海报图绘制需要的信息
          info: {
            picturePath: data.voteActivity.picPath.split(',')[0],
            name: data.voteActivity.name,
            time: timeFormat.timeToDay(data.voteStartTime) + ' ~ ' + timeFormat.timeToDay(data.voteEndTime),
            memberHead: data.memberHead,
            memberNickName: data.memberNickName,
            qrcodeUrl: data.qrcodeUrl,
            logoUrl: app.globalData.defaultConfig.defaultPic,
            organizeName: wx.getStorageSync('organizeName')
          }
        });
        app.updateLocalOrganize(this.data.voteActivity.organizeId);
        // 默认获取最新报名的数据
        this.getMoreVoteUser(this.data.tabs[this.data.activeIndex].orderType, this.data.keyword, 1);

        // 判断是否达到报名的等级
        if (data.moduleGradeList.length > 0) {
          let gradeName = data.moduleGradeList.some(function (x) {
            return x.gradeName == data.gradeName;
          });
          console.log(gradeName);
          this.setData({
            gradeName: gradeName
          })
        }

        // 可以报名
        if (data.voteActivity.needUserEnroll == 'Y') {
          if (data.voteState == 0 && data.enrollState == 0) {
            this.countDownTimes(data.voteActivity.enrollStartTime);
            this.setData({
              countDownTimeText: '后报名开始'
            })
          } else if (data.voteState == 0 && data.enrollState == 1) {
            this.countDownTimes(data.voteStartTime);
            this.setData({
              countDownTimeText: '后投票开始'
            })
          } else if (data.voteState == 1 && data.enrollState == 1) {
            this.countDownTimes(data.voteActivity.enrollEndTime);
            this.setData({
              countDownTimeText: '后报名结束'
            })
          } else if (data.voteState == 1 && data.enrollState == 2) {
            this.countDownTimes(data.voteEndTime);
            this.setData({
              countDownTimeText: '后投票结束'
            })
          } else if (data.voteState == 2 && data.enrollState == 2) {
            this.setData({
              countDownTimeText: '投票活动已结束'
            })
          }
        } else {
          if (data.voteState == 0) {
            this.countDownTimes(data.voteStartTime);
            this.setData({
              countDownTimeText: '后投票开始'
            })
          } else if (data.voteState == 1) {
            this.countDownTimes(data.voteEndTime);
            this.setData({
              countDownTimeText: '后投票结束'
            })
          } else {
            this.setData({
              countDownTimeText: '投票活动已结束'
            })
          }
        }
        // ==================== 富文本 ================== //
        let article = data.voteActivity.content;
        article && WxParse.wxParse('article', 'html', article, _this, 15);
        // ==================== 富文本 ================== //
      } else {
        this.messageComponent.applyActive(data.msg || '请求数据失败！')
      }
    }).catch(err => {
      console.log(err)
    })
  },

  countDownTimes(data) {
    let timeStamp = new Date(data.replace(/-/g, '/')).getTime();
    let now = Date.now();
    let time = (timeStamp - now) / 1000;
    if (time <= 0) {
      return false;
    } else {
      this.countDownTime(data);
    }
  },

  // 报名人员列表
  getMoreVoteUser(orderType, keyword, pageNum) {
    if (orderType == '1') {
      if (pageNum === 1) this.setData({listNew: []})
    } else if (orderType == '2') {
      if (pageNum === 1) this.setData({listVote: []})
    } else if (orderType == '3') {
      if (pageNum === 1) this.setData({listHot: []})
    }
    let params = {
      id: this.data.id,
      orderType: orderType,
      keyword: keyword,
      pageNum: pageNum
    };
    voteApi.getMoreVoteUser(params).then(res => {
      if (!res.data.flag) {
        let msg = res.data.msg || '请求数据失败';
        this.messageComponent.applyActive(msg);
        return
      }
      console.log(res.data.data);
      res.data.data.forEach((item) => {
        item.voteNumber = this.data.voteActivity.patJoinUserNum * item.voteNum + item.addNum
      });
      if (orderType == '1') {
        this.setData({
          listNew: [...this.data.listNew, ...res.data.data],
          pageNew: res.data.page
        })
      } else if (orderType == '2') {
        this.setData({
          listVote: [...this.data.listVote, ...res.data.data],
          pageVote: res.data.page
        })
      } else if (orderType == '3') {
        this.setData({
          listHot: [...this.data.listHot, ...res.data.data],
          pageHot: res.data.page
        })
      }
    }).catch(err => {
      console.log(err)
    })
  },

  // 加载更多
  loadMore(e) {
    let queryType = e.currentTarget.dataset.type;
    if (queryType == 'New') {
      this.getMoreVoteUser(1, this.data.keyword, e.detail)
    } else if (queryType == 'Vote') {
      this.getMoreVoteUser(2, this.data.keyword, e.detail)
    } else if (queryType == 'Hot') {
      this.getMoreVoteUser(3, this.data.keyword, e.detail)
    }
  },

  // 分享
  onShareAppMessage() {
    // 转发成功之后记录分享的次数
    shareLog.shareLogCommon(this.data.id, 'VOTE');
    let organizeId = wx.getStorageSync('organizeId'), organizeName = wx.getStorageSync('organizeName');
    return {
      path: '/pages/activeCenter/voteDetail/voteDetail?id=' + this.data.id + '&organizeId=' + organizeId + '&organizeName=' + organizeName + '&share=true'
    }
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(option) {
    this.messageComponent = this.selectComponent('.messageTips');
    if (option.scene) {
      let scene = decodeURIComponent(option.scene);
      console.log(scene);
      let id = query.getQueryString('id', scene);
      this.setData({
        id: id
      });
    } else {
      this.setData({
        id: option.id
      });
    }
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {
    this.getVoteDetail(this.data.id);
  }
})
