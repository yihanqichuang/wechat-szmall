// 获取全局应用程序实例对象
const app = getApp();
import voteApi from '../../../api/vote.js';
import WxParse from '../../../wxParse/wxParse.js';
import { PHONE, IDCARD, EMAIL, AGE } from  '../../../utils/regular.js';
import Tips from  '../../../utils/tip.js';

// 创建页面实例对象
Page({
  /**
   * 页面的初始数据
   */
  data: {
    title: 'activityDetail',
    messageComponent: '',
    // 投票图片数组
    voteDetailImg: [],
    // 投票图片是否轮播
    autoPlay: false,
    // 是否显示轮播小点
    dotsIsShow: false,
    // 投票活动的id
    id: '',
    // 报名选项
    columnList: [],
    // 报名选项前端需要用到
    optionListForm: [],
    // 保存的值
    optionListFormData: [],
    // 报名上传的图片
    imgList: [],
    voteDialog: false,
    voteSuccessDialog: false,
    radio: [
      {
        title: '男'
      },
      {
        title: '女'
      }
    ]
  },

  // 滑动tab
  handleTabSlider(data) {
    let current = data.detail;
    this.setData({
      activeIndex: current
    });
    this.sliderPosition(current)
  },
  // nav-slider 的位置
  sliderPosition(index) {
    this.setData({
      'navSlider.left': this.data.navSlider.position[index]
    })
  },
  // 改变 swiper 手势滑动
  swiperChange(event) {
    let current = event.detail.current;
    this.setData({
      activeIndex: current
    });
    this.sliderPosition(current);
  },

  // 获取报名数据
  getVoteDetail(id) {
    let params = {
      id: id
    };
    voteApi.toVoteEnroll(params).then(res => {
      console.log(res);
      let data = res.data;
      let _this = this;
      if (data.flag) {
        this.setData({
          voteActivity: data.voteActivity,
          voteDetailImg: data.picPaths,
          voteType: data.voteType,
          autoPlay: data.picPaths.length > 1,
          enrollStartTime: data.enrollStartTime.replace(/-/g, '.'),
          enrollEndTime: data.enrollEndTime.replace(/-/g, '.')
        });
        // 如果optionList有值并且optionListForm值为空（防止上传图片刷新）
        if (data.columnList && this.data.optionListForm.length == 0) {
          let optionListFormArr = [];
          data.columnList.forEach((item) => {
            let optionListForm = {};
            optionListForm.id = item.id;
            optionListForm.isMust = item.isMust;
            optionListForm.columnName = item.columnName;
            optionListForm.columnType = item.columnType;
            optionListForm.value = '';
            optionListForm.valueTips = '';
            optionListFormArr.push(optionListForm)
          });

          this.setData({
            columnList: data.columnList,
            optionListForm: optionListFormArr
          });
        }

        // ==================== 富文本 ================== //
        let article = data.voteActivity.note;
        article && WxParse.wxParse('article', 'html', article, _this, 15);
        // ==================== 富文本 ================== //
      } else {
        this.messageComponent.applyActive(data.msg || '请求数据失败！')
      }
    }).catch(err => {
      console.log(err)
    })
  },

  // 单行文本
  singleInputValue(e) {
    console.log(e)
    let optionListForm = this.data.optionListForm;
    optionListForm[e.currentTarget.dataset.index].value = e.detail.value;
    this.setData({
      optionListForm: optionListForm
    });
    console.log(this.data.optionListForm)
  },

  // 单选框
  radioChange(e) {
    let optionListForm = this.data.optionListForm;
    optionListForm[e.currentTarget.dataset.index].value = e.detail.value;
    // let currentItemList = optionListForm[e.currentTarget.dataset.index].itemList.find((value) => {
    //   return value.title == e.detail.value
    // });
    // optionListForm[e.currentTarget.dataset.index].optionValueIds = currentItemList.id;
    this.setData({
      optionListForm: optionListForm
    });
    console.log(this.data.optionListForm)
  },

  // 上传图片
  handleUpLoad(e) {
    let that = this;
    let hostName = wx.getStorageSync('hostName') || app.data.hostName;
    wx.chooseImage({
      count: 6 - that.data.imgList.length, // 默认9
      sizeType: ['original', 'compressed'], // 可以指定是原图还是压缩图，默认二者都有
      sourceType: ['album', 'camera'], // 可以指定来源是相册还是相机，默认二者都有
      success: function(res) {
        console.log(res)
        // 返回选定照片的本地文件路径列表，tempFilePath可以作为img标签的src属性显示图片
        // var tempFilePaths = res.tempFilePaths[0];
        res.tempFilePaths.forEach((item) => {
          wx.uploadFile({
            url: `${hostName}/wechat/wechatCommon/saveImgToOSS.json`,
            filePath: item,
            name: 'image',
            header: { 'Content-Type': 'multipart/form-data' },
            formData: {
              'appType': 'smallApp',
              'hash': wx.getStorageSync('hash')
            },
            success: function(res) {
              console.log(res);
              var ret = JSON.parse(res.data);
              console.log(ret);
              if (ret.flag) {
                let imgList = that.data.imgList;
                let data = ret.data.substr(0, ret.data.length - 1);
                let optionListForm = that.data.optionListForm;
                imgList.push(data);
                optionListForm[e.currentTarget.dataset.index].value = imgList.join(',');
                that.setData({
                  tempFilePaths: ret.data.substr(0, ret.data.length - 1),
                  optionListForm: optionListForm,
                  imgList: imgList
                });
                console.log(that.data.optionListForm);
              } else {
                wx.showModal({
                  title: '提示',
                  content: '上传失败',
                  showCancel: false
                })
                return;
              }
            },
            fail: function() {
              wx.showModal({
                title: '提示',
                content: '上传失败',
                showCancel: false
              })
            },
            complete: function() {
              wx.hideToast();
            }
          })
        })
      }
    })
  },

  // 删除图片
  handleCancelImg(e) {
    // 上传图片所在列的位置
    let index = e.currentTarget.dataset.index;
    // 删除的当前图片所在的位置
    let idx = e.currentTarget.dataset.idx;
    let imgList = this.data.imgList;
    let optionListForm = this.data.optionListForm;
    imgList.splice(idx, 1);
    optionListForm[index].value = imgList.join(',');
    this.setData({
      optionListForm: optionListForm,
      imgList: imgList
    });
    console.log(this.data.optionListForm)
  },

  // 保存报名选项optionList的值
  saveOptionList() {
    let data = [];
    this.data.optionListForm.forEach((item) => {
      let obj = {};
      obj.columnValue = item.value;
      obj.voteColumnId = item.id;
      obj.columnName = item.columnName;
      obj.columnType = item.columnType;
      data.push(obj);
    });
    this.setData({
      optionListFormData: data
    })
  },

  // 表单验证
  validate() {
    let optionListForm = this.data.optionListForm;
    if (optionListForm.length <= 0) return true;
    for (var i = 0; i < optionListForm.length; i++) {
      let itemType = optionListForm[i].columnType;
      let title = optionListForm[i].columnName;
      if (optionListForm[i].isMust == 'Y') {
        if (itemType == 'select') {
          if (optionListForm[i].value == '') {
            optionListForm[i].valueTips = `请选择${title}信息`;
            this.setData({
              optionListForm: optionListForm
            })
            return false;
          } else {
            optionListForm[i].valueTips = '';
            this.setData({
              optionListForm: optionListForm
            })
          }
        } else if (itemType == 'image') {
          if (optionListForm[i].value == '') {
            optionListForm[i].valueTips = `请选择图片信息`;
            this.setData({
              optionListForm: optionListForm
            });
            return false;
          } else {
            optionListForm[i].valueTips = '';
            this.setData({
              optionListForm: optionListForm
            })
          }
        } else if (itemType == 'text' || itemType == 'phone') {
          if (optionListForm[i].value == '') {
            optionListForm[i].valueTips = `请填写${title}信息`;
            this.setData({
              optionListForm: optionListForm
            });
            return false;
          } else {
            // 身份验证
            if (title == '身份证号码') {
              if (!IDCARD.test(optionListForm[i].value)) {
                optionListForm[i].valueTips = `请填写正确的身份证号码`;
                this.setData({
                  optionListForm: optionListForm
                })
                return false;
              }
            }
            // 邮箱验证
            if (title == '邮箱') {
              if (!EMAIL.test(optionListForm[i].value)) {
                optionListForm[i].valueTips = `请填写正确的邮箱地址`;
                this.setData({
                  optionListForm: optionListForm
                })
                return false;
              }
            }
            // AGE
            if (title == '年龄') {
              if (!AGE.test(optionListForm[i].value)) {
                optionListForm[i].valueTips = `请填写合法的年龄`;
                this.setData({
                  optionListForm: optionListForm
                });
                return false;
              }
            }

            // 手机号
            if (title == '手机号') {
              if (!PHONE.test(optionListForm[i].value)) {
                optionListForm[i].valueTips = `请填正确的手机号码`;
                this.setData({
                  optionListForm: optionListForm
                });
                return false;
              }
            }

            optionListForm[i].valueTips = '';
            this.setData({
              optionListForm: optionListForm
            })
          }
        } else if (itemType == 'image') {
          if (optionListForm[i].value == '') {
            optionListForm[i].valueTips = `请选择图片信息`;
            this.setData({
              optionListForm: optionListForm
            });
            return false;
          } else {
            optionListForm[i].valueTips = '';
            this.setData({
              optionListForm: optionListForm
            })
          }
        }
      }
    }
    return true;
  },

  // 提交数据
  handleToSubmit() {
    // 表单验证
    if (!this.validate()) {
      return false
    }
    this.setData({
      voteDialog: true
    });
  },

  // 取消报名
  handleCloseVote() {
    this.setData({
      voteDialog: false
    });
  },

  // 确认报名
  handleSureVote() {
    this.setData({
      voteDialog: false
    });
    Tips.loading('报名中');
    setTimeout(() => this.submit(), 500);
  },

  // 接口提交数据
  submit() {
    // 组装需要提交的数据
    this.saveOptionList();
    let params = {
      voteActivityId: this.data.voteActivity.id,
      voteTypeId: this.data.voteType.id,
      enrollList: JSON.stringify(this.data.optionListFormData)
    };
    console.log(params);
    voteApi.saveVoteEnroll(params).then(res => {
      console.log(res);
      Tips.loaded();
      if (res.data.flag) {
        this.setData({
          voteSuccessDialog: true
        });
      } else {
        this.messageComponent.applyActive(res.data.msg || '报名失败！')
      }
    }).catch(err => {
      console.log(err)
    });
  },

  // 关闭报名成功弹窗
  handleCloseVoteSuccess() {
    this.setData({
      voteSuccessDialog: false
    });
  },

  // 报名成功 知道了
  handleSureVoteSuccess() {
    this.setData({
      voteSuccessDialog: false
    });
    wx.redirectTo({
      url: `../voteUser/voteUser?id=${this.data.id}`
    })
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(e) {
    // TODO: onLoad
    this.messageComponent = this.selectComponent('.messageTips');
    this.setData({
      id: e.id
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {
    // TODO: onReady
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {
    // TODO: onShow
    this.getVoteDetail(this.data.id)
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {
    // TODO: onHide
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {
    // TODO: onUnload
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {
    // TODO: onPullDownRefresh
  }
})
