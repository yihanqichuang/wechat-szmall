// 获取全局应用程序实例对象
const app = getApp();
import CartApi from '../../api/shoppingcart'
import Tip from '../../utils/tip'
// 创建页面实例对象
Page({
  /**
   * 页面的初始数据
   */
  data: {
    checkAll: false,
    isEdit: false,
    cartList: null,
    cartList1: null,
    marketList: null,
    marketSecondList: null,// 第二件活动商品列表
    statusHeight: app.globalData.statusBarHeight,
    navHeight: app.globalData.navHeight,
    slideButtons: [{ type: 'warn', text: '移除' }],
    pageBackgroundColor: '#EFF3F8'
  },

  // 删除显示
  sliderShow(e) {
    console.log('删除显示', e)
    let { index, organizeId, shopId } = e.currentTarget.dataset;
    this.setData({
      [`cartList.${organizeId}.shops.${shopId}.skuList[${index}].enableClick`]: false
    })
  },

  // 删除隐藏
  sliderHide(e) {
    let { index, organizeId, shopId } = e.currentTarget.dataset;
    this.setData({
      [`cartList.${organizeId}.shops.${shopId}.skuList[${index}].enableClick`]: true
    })
  },

  // 进入商品详情
  enterProductDetail(e) {
    let sku = e.currentTarget.dataset.sku;
    if (sku.enableClick) {
      // 优惠套餐详情页
      if (sku.cartProductType == 'PACKAGE') {
        if (sku.packageMarketInfo.marketState == 'ING') {
          wx.navigateTo({
            url: '/pages/newPackage/packageDiscountDetail/packageDiscountDetail?marketCode=' + sku.marketCode
          })
        }
      } else {
        // 其他商品详情页
        wx.navigateTo({
          url: `/pages/newProductDetail/newProductDetail?productCode=${sku.productCode}`
        })
      }
    }
  },
  // 进入
  enterPackageDiscountSpuDetail(e) {
    let sku = e.currentTarget.dataset.sku;
    wx.navigateTo({
      url: `/pages/newPackage/newProductDetail/newProductDetail?productCode=${sku.productCode}`
    })
  },

  // 获取购物车列表
  getCartList() {
    Tip.loading();
    CartApi.fetchShoppingCartProducts().then(res => {
      Tip.loaded();
      if (res.data.flag) {
        this.groupCartList1(res.data.data.cartList);
        this.isFoldComputed()
        this.setData({
          marketList: res.data.data.marketFullList,
          marketSecondList: res.data.data.marketSecondList
        });
        if (this.data.cartList) {
            this.setData({
                pageBackgroundColor: '#EFF3F8',
            });
        } else {
            this.setData({
                pageBackgroundColor: '#FFFFFF',
            });
        }
      } else {
        Tip.toast(res.data, null, 'none');
      }
    })
  },
  // 查询该数据在对象中的位置，若无返回false
  judgeInArr(id, param, arr) {
    console.log('judgeInArr', id, param, arr)
    if (!arr) return
    if (param == 'organizeId') {
      let i = 0;
      for (let item in arr) {
        console.log('33333333', arr[item].organizeId + ',item:' + item);
        if (id == arr[item].organizeId) {
          console.log('找到了相同organizeId')
          return i
        }
        i++
      }
      return i
    }
    if (param == 'shopId') {
      let i = 0;
      for (let item in arr) {
        // console.log('222222', arr[item].shopId);
        if (id == arr[item].shopId) {
          return i
        }
        i++
      }
      return i
    }
  },

  // 将购物车商品列表组织成树形结构
  groupCartList1(list) {
    let t = this;
    if (!list || list.length <= 0) {
      this.setData({ cartList: null });
      return
    }
    let cartObj = {};
    list.forEach(product => {
      product['isCheck'] = false;
      product['right'] = 0;
      product['enableClick'] = true; // 控制左滑删除和点击查看详情冲突

      let shopId = 0;
      let organizeId = 0;

      organizeId = t.judgeInArr(product.organizeId, 'organizeId', cartObj);

      const organize = cartObj[organizeId];

      console.log('organize', organize)

      if (organize && organize.shops) {
        shopId = t.judgeInArr(product.shopId, 'shopId', organize.shops);
      }
      console.log('organizeId', organizeId)
      // console.log('shopId', shopId)


      if (organize) { // 判断该product中的商场id是否存在cartObj中
        if (organize.shops[shopId]) { // 判断product中的门店id是否存在cartObj下的该商场中的shops下
          organize.shops[shopId].skuList.push(product);
        } else {
          organize.shops[shopId] = {
            mdid: shopId,
            shopId: product.shopId,
            shopLogo: product.shopLogo,
            shopName: product.shopName,
            skuList: [product]
          }
        }
      } else {
        cartObj[organizeId] = {
          isCheck: false,
          zzid: organizeId,
          organizeId: product.organizeId,
          organizeName: product.organizeName,
          organizeType: product.organizeType,
          checkedCount: 0,
          reduceAmount: 0,
          totalAmount: 0,
          totalPoints: 0,
          actualPrice: 0,
          shops: {
            [shopId]: {
              isCheck: false,
              mdid: shopId,
              shopId: product.shopId,
              shopLogo: product.shopLogo,
              shopName: product.shopName,
              skuList: [product]
            }
          }
        }
      }
    });
    for (let item in cartObj) {
      console.log(cartObj[item]);
      if (cartObj[item].organizeType != 'SuperMarket') {
        for (let obj in cartObj[item].shops) {
          if (cartObj[item].shops[obj].skuList.length < 4) {
            cartObj[item].shops[obj].isShowAll = true;
          } else {
            cartObj[item].shops[obj].isShowAll = false;
          }
        }
      } else {
        // 超市商品 折叠/展开显示再最下层
        let skuTotal = 0
        for (let obj in cartObj[item].shops) {
          skuTotal += cartObj[item].shops[obj].skuList.length
        }

        if (skuTotal < 4) {
          cartObj[item].isShowAll = true;
        } else {
          cartObj[item].isShowAll = false;
        }
        cartObj[item].skuTotal = skuTotal
      }

    }
    console.log('------------------', cartObj);
    this.setData({ cartList: cartObj });
  },
  groupCartList(list) {
    if (!list || list.length <= 0) {
      this.setData({ cartList: null });
      return
    }
    let cartObj = {};
    list.forEach(product => {
      product['isCheck'] = false;
      product['right'] = 0;
      product['enableClick'] = true; // 控制左滑删除和点击查看详情冲突
      const organize = cartObj[product.organizeId];

      console.log(organize)
      if (organize) {
        if (organize.shops[product.shopId]) {
          organize.shops[product.shopId].skuList.push(product);
        } else {
          organize.shops[product.shopId] = {
            shopId: product.shopId,
            shopLogo: product.shopLogo,
            shopName: product.shopName,
            skuList: [product]
          }
        }
      } else {
        cartObj[product.organizeId] = {
          isCheck: false,
          organizeId: product.organizeId,
          organizeName: product.organizeName,
          checkedCount: 0,
          reduceAmount: 0,
          totalAmount: 0,
          totalPoints: 0,
          actualPrice: 0,
          shops: {
            [product.shopId]: {
              isCheck: false,
              shopId: product.shopId,
              shopLogo: product.shopLogo,
              shopName: product.shopName,
              skuList: [product]
            }
          }
        }
      }
    });

    // 计算是否展开/折叠
    for (let item in cartObj) {
      console.log(cartObj[item]);
      for (let obj in cartObj[item].shops) {
        if (cartObj[item].shops[obj].skuList.length < 4) {
          cartObj[item].shops[obj].isShowAll = true;
        } else {
          cartObj[item].shops[obj].isShowAll = false;
        }
      }
    }

    this.setData({ cartList: cartObj });
  },
  // 是否展开/折叠，以及折叠高度
  isFoldComputed() {
    let cartObj = this.data.cartList;
    for (let item in cartObj) {
      console.log(cartObj[item]);
      // organizeType 超市
      if (cartObj[item].organizeType == 'SuperMarket') {

        let totalSkusCount = 0
        // 计算超市下共有几个商品
        for (let obj in cartObj[item].shops) {
          console.log('obj:' + obj)
          totalSkusCount += cartObj[item].shops[obj].skuList.length;
        }
        console.log('totalSkusCount:' + totalSkusCount);
        if (totalSkusCount < 4) {
          cartObj[item].isShowAll = true;
          cartObj[item].showHeight = 'auto';
        } else {
          cartObj[item].isShowAll = false;
          cartObj[item].showHeight = '610rpx';
        }

      } else {
        // ShoppingMall 百货
        for (let obj in cartObj[item].shops) {

          if (cartObj[item].shops[obj].skuList.length < 4) {
            cartObj[item].shops[obj].isShowAll = true;
          } else {
            cartObj[item].shops[obj].isShowAll = false;
            cartObj[item].shops[obj].showHeight = '610rpx';
          }
        }
      }
    }

    this.setData({ cartList: cartObj });

  },
  // 查看更多按钮
  showMore(e) {
    let { organizeid, shopid } = e.currentTarget.dataset;
    console.log('showMore', organizeid, shopid)
    let flag = this.data.cartList[organizeid].shops[shopid].isShowAll;
    this.data.cartList[organizeid].shops[shopid].isShowAll = !flag;
    this.setData({ cartList: this.data.cartList });
  },
  // 超市商品查看更多按钮
  superMarketShowMore(e) {
    let { organizeid } = e.currentTarget.dataset;
    console.log('superMarketShowMore', organizeid)
    let flag = this.data.cartList[organizeid].isShowAll;
    this.data.cartList[organizeid].isShowAll = !flag;
    this.setData({ cartList: this.data.cartList });
  },
  // 超市-优惠套餐商品收起/展开
  superMarketPackageDiscountShowMore(e) {
    let { organizeid, shopid, skuindex } = e.currentTarget.dataset;
    console.log('superMarketPackageDiscountShowMore', organizeid)
    let flag = this.data.cartList[organizeid].shops[shopid].skuList[skuindex].isShowAll;
    this.data.cartList[organizeid].shops[shopid].skuList[skuindex].isShowAll = !flag;
    if (!flag) {
      this.data.cartList[organizeid].showHeight = 610 + this.data.cartList[organizeid].shops[shopid].skuList[skuindex].packageSkuInfoList.length * 210 + 'rpx'
    } else {
      this.data.cartList[organizeid].showHeight = '610rpx'
    }
    this.setData({ cartList: this.data.cartList });
  },
  // 百货-优惠套餐商品收起/展开
  superMallPackageDiscountShowMore(e) {
    let { organizeid, shopid, skuindex } = e.currentTarget.dataset;
    console.log('superMarketPackageDiscountShowMore', organizeid)
    let flag = this.data.cartList[organizeid].shops[shopid].skuList[skuindex].isShowAll;
    this.data.cartList[organizeid].shops[shopid].skuList[skuindex].isShowAll = !flag;
    if (!flag) {
      this.data.cartList[organizeid].shops[shopid].showHeight = 610 + this.data.cartList[organizeid].shops[shopid].skuList[skuindex].packageSkuInfoList.length * 210 + 'rpx';
    } else {
      this.data.cartList[organizeid].shops[shopid].showHeight = this.data.cartList[organizeid].shops[shopid].skuList.length * 210 + 'rpx';
    }
    this.setData({ cartList: this.data.cartList });
  },
  // 全选/全不选
  toggleAllCheck() {
    const cartList = this.data.cartList;
    if (!cartList) return
    const isCheck = !this.data.checkAll;
    Object.values(cartList).forEach(organize => {
      organize.isCheck = isCheck;
      if (Object.values(organize.shops).length) {
        Object.values(organize.shops).forEach(shop => {
          shop.isCheck = isCheck;
          if (shop.skuList && shop.skuList.length) {
            shop.skuList.forEach(sku => {
              // 预售商品不可选
              if (sku.skuInfo && sku.skuInfo.marketType == 'PRESALE') {
              } else if (sku.packageMarketInfo && sku.packageMarketInfo.marketState == 'END') {
                // 优惠套餐过期不可选
              } else if (sku.cartProductType == 'PACKAGE' && (!sku.packageSkuInfoList || sku.packageSkuInfoList.length == 0)) {
                // 优惠套餐商品变更不可选
              } else {
                sku.isCheck = isCheck;
              }
            })
          }
        })
      }
      this.getCartTotalData(organize);
    });
    this.setData({ checkAll: isCheck, cartList });
  },
  // 商场选择
  toggleOrganizeCheck(e) {
    console.log(e)
    const zzid = e.currentTarget.dataset.zzid;
    const organizeId = e.currentTarget.dataset.organizeid;
    const currentOrganize = this.data.cartList[zzid];
    console.log('currentOrganize', currentOrganize, this.data.cartList);
    currentOrganize.isCheck = !currentOrganize.isCheck;
    if (Object.values(currentOrganize.shops).length) {
      Object.values(currentOrganize.shops).forEach(shop => {
        shop.isCheck = currentOrganize.isCheck;
        if (shop.skuList && shop.skuList.length) {
          shop.skuList.forEach(sku => {
            // 预售商品不可选
            if (sku.skuInfo && sku.skuInfo.marketType == 'PRESALE') {
            } else if (sku.packageMarketInfo && sku.packageMarketInfo.marketState == 'END') {
              // 优惠套餐过期不可选
            } else if (sku.cartProductType == 'PACKAGE' && (!sku.packageSkuInfoList || sku.packageSkuInfoList.length == 0)) {
              // 优惠套餐商品变更不可选
            } else {
              sku.isCheck = currentOrganize.isCheck;
            }
          })
        }
      })
    }
    const key = `cartList.${zzid}`;
    this.setData({
      [key]: currentOrganize,
      checkAll: this.isAllCheck()
    });
    this.getCartTotalData(currentOrganize);
  },
  // 店铺选择
  toggleShopCheck(e) {
    const shopId = e.currentTarget.dataset.shopid;
    const organizeId = e.currentTarget.dataset.organizeid;
    const mdid = e.currentTarget.dataset.mdid;
    const zzid = e.currentTarget.dataset.zzid;
    const currentOrganize = this.data.cartList[zzid];
    const currentShop = currentOrganize.shops[mdid];
    currentShop.isCheck = !currentShop.isCheck;
    if (currentShop.skuList && currentShop.skuList.length) {
      currentShop.skuList.forEach(sku => {
        // 预售商品不可选
        if (sku.skuInfo && sku.skuInfo.marketType == 'PRESALE') {
        } else if (sku.packageMarketInfo && sku.packageMarketInfo.marketState == 'END') {
          // 优惠套餐过期不可选
        } else if (sku.cartProductType == 'PACKAGE' && (!sku.packageSkuInfoList || sku.packageSkuInfoList.length == 0)) {
          // 优惠套餐商品变更不可选
        } else {
          sku.isCheck = currentShop.isCheck
        }
      });
    }
    currentOrganize.isCheck = this.isOrganizeCheck(currentOrganize);
    const key = `cartList.${zzid}`;
    this.setData({
      [key]: currentOrganize,
      checkAll: this.isAllCheck()
    });
    this.getCartTotalData(currentOrganize);
  },
  // 商品选择
  toggleSkuCheck(e) {
    const skuIndex = e.currentTarget.dataset.index;
    const shopId = e.currentTarget.dataset.shopid;
    const organizeId = e.currentTarget.dataset.organizeid;
    const mdid = e.currentTarget.dataset.mdid;
    const zzid = e.currentTarget.dataset.zzid;
    const currentOrganize = this.data.cartList[zzid];
    const currentShop = currentOrganize.shops[mdid];
    const currentSku = currentShop.skuList[skuIndex];
    // 预售商品不可选
    if (currentSku.skuInfo && currentSku.skuInfo.marketType === 'PRESALE') return;
    // 优惠套餐过期不可选
    if (currentSku.packageMarketInfo && currentSku.packageMarketInfo.marketState == 'END') return;
    // 优惠套餐商品变更不可选
    if (currentSku.cartProductType == 'PACKAGE' && (!currentSku.packageSkuInfoList || currentSku.packageSkuInfoList.length == 0)) return;
    currentSku.isCheck = !currentSku.isCheck;
    currentShop.isCheck = this.isShopCheck(currentShop);
    currentOrganize.isCheck = this.isOrganizeCheck(currentOrganize);

    const key = `cartList.${zzid}`;
    this.setData({
      [key]: currentOrganize,
      checkAll: this.isAllCheck()
    });
    this.getCartTotalData(currentOrganize);
  },

  // 店铺是否选中
  isShopCheck(shop) {
    const checkedSkuCount = shop.skuList.filter(sku => sku.isCheck).length;
    return checkedSkuCount === 0 ? false : checkedSkuCount === shop.skuList.length;
  },
  // 商场是否选中
  isOrganizeCheck(organize) {
    const shopList = Object.values(organize.shops);
    const checkedShopCount = shopList.filter(shop => shop.isCheck).length;
    return checkedShopCount === 0 ? false : checkedShopCount === shopList.length;
  },
  // 是否全选
  isAllCheck() {
    const organizeList = Object.values(this.data.cartList);
    const checkedCount = organizeList.filter(ele => ele.isCheck).length;
    return checkedCount === 0 ? false : checkedCount === organizeList.length;
  },
  // 增减商品数量
  async changeNum(e) {
    Tip.loading();
    const num = Number(e.currentTarget.dataset.var);
    const skuIndex = e.currentTarget.dataset.index;
    const shopId = e.currentTarget.dataset.shopid;
    const organizeId = e.currentTarget.dataset.organizeid;
    const zzid = e.currentTarget.dataset.zzid;
    const mdid = e.currentTarget.dataset.mdid;
    const currentOrganize = this.data.cartList[zzid];
    const currentShop = currentOrganize.shops[mdid];
    const currentSku = currentShop.skuList[skuIndex];
    currentSku.skuCount += num;
    const res = await this.addToCart(currentSku);
    Tip.loaded();
    if (res.data.flag) {
      const key = `cartList.${zzid}.shops.${mdid}.skuList[${skuIndex}]`;
      this.setData({ [key]: currentSku });
      // 已选的才计算优惠
      if (currentSku.isCheck) {
        this.getCartTotalData(currentOrganize);
      }
    } else {
      Tip.toast(res.data.msg, null, 'none');
    }
  },
  // 同步数据库商品数量变化
  addToCart(selectedSkuData) {
    if (!selectedSkuData) return
    const params = {
      id: selectedSkuData.id,
      skuCount: selectedSkuData.skuCount
    };
    return CartApi.fetchUpdateCartCount(params);
  },
  // 计算
  getCartTotalData(currentOrganize) {
    const shopList = Object.values(currentOrganize.shops);
    const skuList = [];
    shopList.forEach(shop => {
      skuList.push(...shop.skuList);
    });
    const checkedSku = skuList.filter(sku => sku.isCheck);
    let totalAmount = 0;
    let totalPoints = 0;
    let reduceAmount = 0;
    if (checkedSku.length > 0) {
      checkedSku.forEach(sku => {
        if (sku.cartProductType == 'PACKAGE') {
          totalAmount += sku.packageMarketInfo.marketPrice * sku.skuCount;
        } else {
          if (sku.skuInfo.salePrice) {
            totalAmount += sku.skuInfo.salePrice * sku.skuCount;
          }
          if (sku.skuInfo.points) {
            totalPoints += sku.skuInfo.points * sku.skuCount;
          }
        }

      });
      reduceAmount = this.getDiscountFee(checkedSku);
    }
    const checkedCountKey = `cartList.${currentOrganize.zzid}.checkedCount`;
    const totalAmountKey = `cartList.${currentOrganize.zzid}.totalAmount`;
    const totalPointsKey = `cartList.${currentOrganize.zzid}.totalPoints`;
    const reduceAmountKey = `cartList.${currentOrganize.zzid}.reduceAmount`;
    const actualPriceKey = `cartList.${currentOrganize.zzid}.actualPrice`;
    this.setData({
      [checkedCountKey]: checkedSku.length,
      [totalAmountKey]: parseFloat(totalAmount.toFixed(2)),
      [totalPointsKey]: parseFloat(totalPoints.toFixed(2)),
      [reduceAmountKey]: parseFloat(reduceAmount.toFixed(2)),
      [actualPriceKey]: parseFloat((totalAmount - reduceAmount).toFixed(2))
    });
  },
  // 获取优惠金额
  getDiscountFee(skus) {
    let discountAmount = 0;

    // 满减商品优惠计算
    if (skus.length > 0 && this.data.marketList && this.data.marketList.length) {
      let obj = {};
      // 已选商品按marketCode划分算出总价
      skus.forEach(sku => {
        if (sku.skuInfo && sku.skuInfo.marketCode) {
          if (obj[sku.skuInfo.marketCode]) {
            obj[sku.skuInfo.marketCode].totalAmount += sku.skuCount * sku.skuInfo.salePrice;
            obj[sku.skuInfo.marketCode].totalCount += sku.skuCount;
          } else {
            obj[sku.skuInfo.marketCode] = {
              totalAmount: sku.skuCount * sku.skuInfo.salePrice,
              totalCount: sku.skuCount
            };
          }
        }
      });
      let marketCodes = Object.keys(obj);
      if (marketCodes.length > 0) {
        marketCodes.forEach(marketCode => {
          const currentMarket = this.data.marketList.find(ele => ele.marketCode === marketCode);
          // 如果满减会场存在，筛选出满足条件的阶梯
          let fullLadders = [];
          if (currentMarket) {
            fullLadders = currentMarket.ladders.filter(ladder => {
              if (currentMarket.fullType === 'FULL_MONEY') {
                return ladder.fullValue <= obj[marketCode].totalAmount;
              }
              if (currentMarket.fullType === 'FULL_COUNT') {
                return ladder.fullValue <= obj[marketCode].totalCount;
              }
            });
          }
          if (fullLadders.length > 0) {
            // 满足条件的阶梯降序排序，取第一个阶梯
            fullLadders.sort((ele1, ele2) => ele2.fullValue - ele1.fullValue);
            let ladderAmount = fullLadders[0].discountAmount;
            if (ladderAmount && ladderAmount > 0) {
              // 满元减情况下，如果减免金额大于商品总金额，减免商品总金额，防止出现负数
              if (ladderAmount > obj[marketCode].totalAmount) {
                ladderAmount = obj[marketCode].totalAmount
              }
              discountAmount += ladderAmount;
            }
          }
        })
      }
    }

    // 第二件活动商品优惠计算 marketSecondList
    if (skus.length > 0 && this.data.marketSecondList && this.data.marketSecondList.length) {
      let obj = {};
      // 已选商品按marketCode划分算出总价
      skus.forEach(sku => {
        if (sku.skuInfo && sku.skuInfo.marketCode) {
          if (obj[sku.skuInfo.marketCode]) {
            obj[sku.skuInfo.marketCode].totalAmount += sku.skuCount * sku.skuInfo.salePrice;
            obj[sku.skuInfo.marketCode].totalCount += sku.skuCount;
            obj[sku.skuInfo.marketCode].salePriceList.push(sku.skuInfo.salePrice);
            obj[sku.skuInfo.marketCode].skuList.push(sku);
          } else {
            obj[sku.skuInfo.marketCode] = {
              totalAmount: sku.skuCount * sku.skuInfo.salePrice,
              totalCount: sku.skuCount,
              salePriceList: [sku.skuInfo.salePrice],
              skuList: [sku]
            };
          }
        }
      });
      let marketCodes = Object.keys(obj);
      if (marketCodes.length > 0) {
        marketCodes.forEach(marketCode => {
          const currentMarket = this.data.marketSecondList.find(ele => ele.marketCode === marketCode);
          // 如果第二件活动存在且活动商品件数符合条件(大于1件)，计算优惠金额
          if (currentMarket && obj[marketCode].totalCount > 1) {
            obj[marketCode].salePriceList.sort((ele1, ele2) => ele1 - ele2);
            // 第二件0元商品计算
            if (currentMarket.secondType == 'ZERO') {
              // 优惠件数
              let zeroCount = Math.floor(obj[marketCode].totalCount / 2);
              while (zeroCount > 0) {
                let skuobj = obj[marketCode].skuList.find(ele => ele.skuInfo.salePrice === obj[marketCode].salePriceList[0])

                if (skuobj.skuCount < zeroCount) {
                  discountAmount += skuobj.skuCount * skuobj.skuInfo.salePrice
                  zeroCount -= skuobj.skuCount
                } else {
                  discountAmount += zeroCount * skuobj.skuInfo.salePrice
                  zeroCount = -1
                }
                obj[marketCode].salePriceList.splice(0, 1);
              }

            }

            // 第二件打折计算
            if (currentMarket.secondType == 'DISCOUNT') {
              // 优惠件数
              let discountCount = Math.floor(obj[marketCode].totalCount / 2);
              console.log('discountCount:' + discountCount);
              while (discountCount > 0) {
                let skuobj = obj[marketCode].skuList.find(ele => ele.skuInfo.salePrice === obj[marketCode].salePriceList[0])
                console.log(skuobj)

                console.log(discountAmount)
                if (skuobj.skuCount < discountCount) {
                  discountAmount += skuobj.skuCount * (Math.floor(skuobj.skuInfo.salePrice * (currentMarket.discountValue / 10) * 100) / 100)
                  discountCount -= skuobj.skuCount
                } else {
                  discountAmount += discountCount * (Math.floor(skuobj.skuInfo.salePrice * (currentMarket.discountValue / 10) * 100) / 100)
                  discountCount = -1
                }
                obj[marketCode].salePriceList.splice(0, 1);
              }
            }
          }

        })
      }
    }

    return discountAmount;
  },
  // 删除购物车商品
  removeCartProduct() {
    if (this.data.checkAll) {
      const ids = [];
      Tip.confirm('确定清空购物车?').then(res => {
        this.doRemove(ids);
      }).catch(() => {
        console.log('取消清空');
      });
    } else {
      const ids = [];
      if (this.data.cartList) {
        Object.values(this.data.cartList).forEach(organize => {
          Object.values(organize.shops).forEach(shop => {
            shop.skuList.forEach(sku => {
              if (sku.isCheck) {
                ids.push(sku.id);
              }
            })
          })
        })
      }
      if (ids.length <= 0) {
        Tip.toast('请选择商品', null, 'none');
        return
      }
      Tip.confirm(`确定删除已选的${ids.length}件商品?`).then(res => {
        this.doRemove(ids);
      }).catch(() => {
        console.log('取消清空');
      });
    }
  },
  // 删除购物车商品
  doRemove(params) {
    Tip.loading();
    CartApi.fetchCartDelete(params).then(res => {
      Tip.loaded();
      if (res.data.flag) {
        this.getCartList();
      }
    })
  },
  // 编辑删除
  showEdit() {
    this.setData({ isEdit: !this.data.isEdit });
  },
  // touch事件
  touchstart(e) {
    this.setData({
      startX: e.touches[0].clientX,
      startY: e.touches[0].clientY
    })
  },
  touchend(e) {
    let { shopid, organizeid, index } = e.currentTarget.dataset;
    let { clientX, clientY } = e.changedTouches[0];
    let distanceX = this.data.startX - clientX;
    let distanceY = Math.abs(this.data.startY - clientY);
    let isOpen = distanceX > 80 && distanceY < 100;
    console.log('isOpen', isOpen);
    this.setData({
      [`cartList.${organizeid}.shops.${shopid}.skuList[${index}].right`]: isOpen ? '54' : '0'
    })
  },
  // 单个删除
  slideButtonTap(e) {
    let id = e.currentTarget.dataset.id;
    this.doRemove([id]);
  },

  // 去结算
  goConfirmOrder(e) {
    const currentOrganizeId = wx.getStorageSync('organizeId');
    const zzid = e.currentTarget.dataset.zzid;
    const organizeId = e.currentTarget.dataset.organizeid;
    const organizeName = e.currentTarget.dataset.organizename;
    if (currentOrganizeId !== organizeId) {
      // 不是当前机构的商品，机构信息切换到选择商品所属机构的信息
      wx.setStorageSync('organizeId', organizeId);
      wx.setStorageSync('organizeName', organizeName);
    }
    const currentOrganize = this.data.cartList[zzid];
    const obj = {};
    Object.values(currentOrganize.shops).forEach(shop => {
      shop.skuList.forEach(sku => {
        if (sku.isCheck) {
          if (obj[shop.shopId]) {
            obj[shop.shopId].skuList.push({
              cartId: sku.id,
              dispatchType: sku.dispatchType,
              skuCount: sku.skuCount,
              skuInfo: sku.cartProductType == 'PACKAGE' ? sku.packageSkuInfoList : sku.skuInfo,
              marketCode: sku.cartProductType == 'PACKAGE' ? sku.marketCode : null,
              cartProductType: sku.cartProductType == 'PACKAGE' ? sku.cartProductType : null,
              packageProductCode: sku.cartProductType == 'PACKAGE' ? sku.packageProductCode : null
            });
          } else {
            obj[shop.shopId] = {
              shopId: shop.shopId,
              shopName: shop.shopName,
              shopLogo: shop.shopLogo,
              shopPhone: shop.shopPhone,
              organizeType: currentOrganize.organizeType, // 组织类型
              skuList: [{
                cartId: sku.id,
                dispatchType: sku.dispatchType,
                skuCount: sku.skuCount,
                skuInfo: sku.cartProductType == 'PACKAGE' ? sku.packageSkuInfoList : sku.skuInfo,
                marketCode: sku.cartProductType == 'PACKAGE' ? sku.marketCode : null,
                cartProductType: sku.cartProductType == 'PACKAGE' ? sku.cartProductType : null,
                packageProductCode: sku.cartProductType == 'PACKAGE' ? sku.packageProductCode : null
              }]
            }
          }
        }
      })
    })
    const checkedSkuShops = Object.values(obj);
    wx.setStorageSync('createOrderSkus', checkedSkuShops);
    // 判断下单的商品是否为超市商品
    // const isSuperMarket = checkedSkuShops.every(store => store.organizeType === 'SuperMarket')
    let url = '/pages/newPackage/createOrder/createOrder'
    // if (isSuperMarket) {
    //   // 超市商品去 超市确认订单页
    //   url = '/pages/newPackage/createMarkOrder/createMarkOrder'
    // }
    wx.navigateTo({ url })
  },
  onShow() {
    this.getCartList();
  }
})
