// 获取全局应用程序实例对象
const app = getApp();
// import { PHONE } from '../../utils/regular.js';
// import Tip from '../../utils/tip';
// import ProductApi from '../../api/commodity';
import {
	jdFetch
} from '../../../utils/fetch.js';
// 创建页面实例对象
Page({
	/**
	 * 页面的初始数据
	 */
	data: {
		__user_info: {
			nickname: '吴浩浩594',
			parent: '总店',
			is_distributor: '0'
		},
		form: {
			name: "",
			mobile: ""
		},
		img: "../../../images/icon/img-share-agree.png",
		agree: 0,
		show_modal: !1,
		share_name: '',
		jdShareInfo: {},
		fxOrganize: {},
		sqbg: "",
		dshbg: "",
		parent_name:""
	},
	onLoad() {
		let t = this;
		let jdShareInfo = wx.getStorageSync("jdShareInfo");
		console.log(jdShareInfo)
		t.getInfo();
		t.getShareSetting();
	},

	onShow() {
		var t = this;
		var a = "分销商";
		let jdShareInfo = wx.getStorageSync("jdShareInfo");
		let fxOrganize = wx.getStorageSync("fxOrganize");
		let parent_name = wx.getStorageSync("parent_name");
		console.log(wx.getStorageSync("organizeId" + fxOrganize.organizeId))
		jdShareInfo.is_distributor = wx.getStorageSync("organizeId" + fxOrganize.organizeId)
		t.setData({
			jdShareInfo: jdShareInfo,
			fxOrganize: fxOrganize,
			parent_name: parent_name
		});
		t.data.custom && t.data.custom.word && (a = t.data.custom.word.share_name.name), t.setData({
			share_name: a
		}), wx.setNavigationBarTitle({
			title: "申请成为" + a
		});
		
		// return
		wx.showLoading({
			title: "加载中"
		});
		jdFetch({
			url: 'share/check'
		}).then(res => {
			// return
			if (0 == res.code) {
				if (1 == res.data) {
					jdShareInfo.is_distributor = 1;
					t.setData({
						jdShareInfo: jdShareInfo
					});
					wx.setStorageSync("jdShareInfo", jdShareInfo);
					wx.redirectTo({
						url: "/pages/share/index/index"
					})
				}
			}
			wx.hideLoading();
		});
	},

	getShareSetting() {
		let t = this;
		jdFetch({
			url: 'default/store'
		}).then(res => {
			console.log(res);
			if (0 == res.code) {
				if (res.data.share_setting) var share_setting = res.data.share_setting;
				else var share_setting = res.data;
				wx.setStorageSync('SHARE_SETTING', share_setting), t.setData({
					share_setting: share_setting
				});
				t.setData({
					sqbg: share_setting.pic_url_1,
					dshbg: share_setting.pic_url_2
				})
			}
		})
	},
	getInfo() {
		let t = this;
		jdFetch({
			url: 'share/get-info'
		}).then(res => {
			console.log('getInfo', res);
			0 == res.code && (t.setData({
				custom: res.data.custom
			}), wx.setStorageSync('CUSTOM', res.data.custom))
		})
	},
	agree: function() {
		var e = this,
			t = e.data.agree;

		0 == t ? (t = 1, e.setData({
			img: "/images/img-share-agree.png",
			agree: t
		})) : 1 == t && (t = 0, e.setData({
			img: "/images/img-share-un.png",
			agree: t
		}));
	},
	formSubmit: function(e) {
		console.log('formSubmit', e);
		var t = this,
			a = t.data.jdShareInfo;
		if (t.data.form = e.detail.value, null != t.data.form.name && "" != t.data.form.name) {
			if (null != t.data.form.mobile && "" != t.data.form.mobile) {
				if (/^\+?\d[\d -]{8,12}\d/.test(t.data.form.mobile)) {
					var o = e.detail.value;
					o.form_id = e.detail.formId, 0 != t.data.agree ? (
						wx.showLoading({
							title: "正在提交",
							mask: !0
						}),
						jdFetch({
							url: 'share/join',
							data: o,
						}).then(res => {
							0 == res.code ? (a.is_distributor = res.data.is_distributor,
								t.setData({
									jdShareInfo: a
								}), wx.setStorageSync("jdShareInfo", a)) : wx.showToast({
								title: res.msg,
								duration: 2000,
								icon: "none"
							});
							if (res.data.is_distributor == 1) {
								wx.redirectTo({
									url: "/pages/share/index/index"
								})
							}
						})
					) : wx.showToast({
						title: "请先阅读并确认分销申请协议！！",
						duration: 2000,
						icon: "none"
					});
				} else wx.showModal({
					title: "提示",
					content: "手机号格式不正确",
					showCancel: !1
				});
			} else wx.showToast({
				title: "请填写联系方式！",
				duration: 2000,
				icon: "none"
			});
		} else wx.showToast({
			title: "请填写姓名！",
			duration: 2000,
			icon: "none"
		});
	},
	agreement: function() {
		this.setData({
			show_modal: !0
		});
	},
	close: function() {
		this.setData({
			show_modal: !1,
			img: "../../../images/icon/img-share-agree.png",
			agree: 1
		});
	},
	// getQueryMember 获取会员信息
	getQueryMember() {
		userApi.queryMember().then(res => {
			if (res.data.flag) {
				this.setData({
					memberInfo: res.data.data,
					memberType: res.data.data.memberType,
					isError: res.data.data.isError,
					memberPhone: res.data.data.memberPhone
				});
				wx.setStorageSync('memberId', res.data.data.memberId);
				app.globalData.cardNumber = res.data.data.cardNumber;
				app.globalData.memberPhone = res.data.data.memberPhone;
				this.isCanUseSign();
			} else {
				let msg = res.data.msg || '请求数据失败';
				this.messageComponent.applyActive(msg)
			}
		}).catch();
	},

	// 手机号码输入
	handlePhoneInput(e) {
		this.setData({
			phoneValue: e.detail.value
		});
		if (PHONE.test(this.data.phoneValue) && this.data.codeValue) {
			this.setData({
				isCanBind: true
			})
		} else {
			this.setData({
				isCanBind: false
			})
		}
	},

	// 绑定手机号
	handleBindPhone() {
		// 验证手机号
		if (!this.data.phoneValue) {
			this.messageComponent.applyActive('请输入手机号码');
			return false;
		} else if (!PHONE.test(this.data.phoneValue)) {
			this.messageComponent.applyActive('手机格式不正确');
			return false;
		}

		// 验证码校验
		if (!this.data.codeValue) {
			this.messageComponent.applyActive('请输入验证码');
			return false;
		}

		let params = {
			memberPhone: this.data.phoneValue,
			validateCode: this.data.codeValue
		};
		this.setData({
			isLoading: true,
			isCanBind: false
		});
		userApi.againBindMemberPhone(params).then(res => {
			if (res.data.flag) {
				app.globalData.memberPhone = this.data.phoneValue;
				wx.setStorageSync('memberPhone', this.data.phoneValue);
				this.handleCloseBindDialog();
				this.getQueryMember()
				this.messageComponent.applyActive('重新绑定成功！');
			} else {
				this.messageComponent.applyActive(res.data.msg || '重新绑定失败');
				this.setData({
					isLoading: false,
					isCanBind: true
				})
			}
		}).catch(err => {
			console.log(err);
			this.setData({
				isLoading: false,
				isCanBind: true
			})
		})
	}
});
