var app = getApp(),
    is_no_more = !1,
    is_loading = !1,
    p = 2;

import { jdFetch } from '../../../utils/fetch.js'
Page({
    data: {
        status: 1,
        first_count: 0,
        second_count: 0,
        third_count: 0,
        list: Array,
        no_more: !1
    },
    onLoad: function (t) {
        var shareSetting = wx.getStorageSync('SHARE_SETTING');
        this.setData({
            share_setting: shareSetting
        }),
            is_no_more = is_loading = !1,
            p = 2,
            this.GetList(t.status || 1);
    },
    changeTeamLevel(e) {
        
        let level =e.currentTarget.dataset.level;

        this.GetList(level)
    },
    GetList: function (level) {
        var t = this;
        is_loading || (is_loading = !0, t.setData({
            status: parseInt(level || 1)
        }),
            wx.showLoading({
                title: "正在加载",
                mask: !0
            }),
            jdFetch({
                url: 'share/get-team',
                data: {
                    status: t.data.status,
                    page: 1
                }
            }).then(res => {
                t.setData({
                    first_count: res.data.first,
                    second_count: res.data.second,
                    third_count: res.data.third,
                    list: res.data.list
                }), 0 == res.data.list.length && (is_no_more = !0, t.setData({
                    no_more: !0
                }));

                wx.hideLoading()
                is_loading = !1;
            }).catch(err => {
                wx.hideLoading()
                is_loading = !1;
            })
        );
    },
    onReachBottom: function () {
        is_no_more || this.loadData();
    },
    loadData: function () {
        if (!is_loading) {
            is_loading = !0;
            var t = this;
            wx.showLoading({
                title: "正在加载",
                mask: !0
            })
            jdFetch({
                url: 'share/get-team',
                data: {
                    status: t.data.status,
                    page: p
                }
            }).then(res => {
                t.setData({
                    first_count: res.data.first,
                    second_count: res.data.second,
                    third_count: res.data.third,
                    list: t.data.list.concat(res.data.list)
                })
                if(res.data.list.length == 0) {
                    is_no_more = !0;
                    t.setData({
                        no_more: !0
                    })
                }
                wx.hideLoading();
                is_loading = !1;
                p++;
            }).catch(err => {
               wx.hideLoading();
               is_loading = !1;
               p++;
            })
        }
    }
});