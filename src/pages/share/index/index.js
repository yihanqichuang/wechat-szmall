// 获取全局应用程序实例对象
const app = getApp();
// import { PHONE } from '../../utils/regular.js';
// import Tip from '../../utils/tip';
// import ProductApi from '../../api/commodity';
import {
	jdFetch
} from '../../../utils/fetch.js';
import {
	bindParent
} from '../../../utils/bindParent.js';
import {
	fetchOrganizeList
} from '../../../api/markets'
// 创建页面实例对象
Page({
	/** 
	 * 页面的初始数据    
	 */
	data: {
		total_price: 0,
		price: 0,
		cash_price: 0,
		total_cash: 0,
		team_count: 0,
		order_money: 0,
		organizeModel: 0,
		txModel: 0,
		organizeList: [],
		currentStore: {},
		yue: 0,
		allStoreYue: []
	},
	onLoad() {
		this.getShareSetting()
		let jdShareInfo = wx.getStorageSync("jdShareInfo");
		this.setData({
			jdShareInfo: jdShareInfo
		});
		this.getInfo();
		this.getOrganizeList();
		this.getCurrentStore();
	},
	getCurrentStore() {
		let t = this;
		let fxOrganize = wx.getStorageSync("fxOrganize");
		console.log("当前的store", fxOrganize);
		t.setData({
			currentStore: {
				organizeName: fxOrganize.organizeName
			}
		})
	},
	getCurOrgInfo() {
		let jdShareInfo = wx.getStorageSync("jdShareInfo");
		jdFetch({
			url: 'passport/share',
			data: {
				user_id: jdShareInfo.user_id
			},
			showLoading: true
		}).then(res => {
			console.log("获取当前商城的分销信息:", res)
			if (res.code == 0) {
				wx.setStorageSync("organizeId" + wx.getStorageSync("fxOrganize").organizeId, res.data.is_distributor);
				if (res.data.is_distributor !== 1) {
					wx.redirectTo({
						url: "/pages/share/add-share/index"
					})
				} else {
					this.getInfo()
				}
			}
		}).catch(err => {
			wx.removeStorageSync("fxOrganize")
		})
	},

	onShow() {},
	getInfo() {
		let fxOrganize = wx.getStorageSync("fxOrganize")
		let t = this;
		jdFetch({
			url: 'share/get-info'
		}).then(res => {
			console.log('getInfo', res);
			0 == res.code && (t.setData({
					total_price: res.data.price.total_price,
					price: res.data.price.price,
					cash_price: res.data.price.cash_price,
					total_cash: res.data.price.total_cash,
					team_count: res.data.team_count,
					order_money: res.data.order_money,
					custom: res.data.custom,
					order_money_un: res.data.order_money_un,
					parent_name: res.data.parent_name
				}),
				wx.setStorageSync('CUSTOM', res.data.custom))
			this.setData({
				custom: res.data.custom
			})
		})
	},
	getShareSetting() {
		jdFetch({
			url: 'default/store',
			showLoading: true
		}).then(res => {
			console.log('后台设置的分销相关的配置', res.data);
			wx.setStorageSync('SHARE_SETTING', res.data.share_setting);
		}).catch(err => {
			console.log('请求后台设置的分销配置失败', err)
		})
	},
	// 获取所有的组织列表
	getOrganizeList() {
		let params = {}
		fetchOrganizeList(params).then(res => {
			console.log("所有组织列表:", res);
			let organizeList = [];
			for (let i = 0; i < res.data.data.length; i++) {
				let item = res.data.data[i];
				console.log(item)
				organizeList = organizeList.concat(item.organizeList)
			}
			this.setData({
				organizeList: organizeList
			})
			this.getTotalMoney()
		})
	},
	getTotalMoney() {
		let t = this;
		let organizeList = t.data.organizeList;
		let idArr = []
		organizeList.forEach(item => {
			idArr.push(item.organizeId);
		})
		console.log(idArr)
		jdFetch({
			url: "share/user-price",
			data: {
				txOrganizeId: idArr,
				user_id: t.data.jdShareInfo.user_id
			}
		}).then(res => {
			console.log('所有商铺可提现余额:', res);
			let yue = 0
			res.data.forEach(item => {
				yue += parseFloat(item.price);
				item.price = (parseFloat(item.price) * 1000 / 1000).toFixed(2);
			})

			this.setData({
				yue: (yue * 1000 / 1000).toFixed(2),
				allStoreYue: res.data
			})
		})
	},
	showOrganizeModel() {
		this.setData({
			organizeModel: !0
		})
	},
	closeOrganizeModel() {
		this.setData({
			organizeModel: 0
		})
	},
	showTxModel() {
		this.setData({
			txModel: !0
		})
	},
	closeTxModel() {
		this.setData({
			txModel: 0
		})
	},
	chooseOrganize(e) {
		let curOrg = e.currentTarget.dataset.item;
		// 更新缓存里的存的店铺
		wx.setStorageSync('fxOrganize', curOrg);
		this.setData({
			currentStore: {
				organizeName: curOrg.organizeName
			}
		})
		this.getCurOrgInfo();
		this.closeOrganizeModel();
		this.getShareSetting();
	},
	chooseStore(e) {
		let curOrg = e.currentTarget.dataset.item;
		wx.navigateTo({
			url: "/pages/share/cash/cash?organizeId=" + curOrg.organizeId
		})
	},
	// JD:分销,获取后台设置的参数
	// getShareSetting(){
	//   let t=this;
	//   jdFetch({
	//     url:'share/index'
	//   }).then(res => {
	//     console.log(res);
	//     if (0 == res.code) {
	//       if (res.data.share_setting) var share_setting = res.data.share_setting; else var share_setting = res.data;
	//       wx.setStorageSync('SHARE_SETTING', share_setting), t.setData({
	//           share_setting: share_setting
	//       });
	//     }
	//   })
	// },
});
