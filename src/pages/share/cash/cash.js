var app = getApp();
// api = getApp().api;

function min(e, a) {
    return e = parseFloat(e), (a = parseFloat(a)) < e ? a : e;
}

import {jdFetch} from '../../../utils/fetch.js'

Page({
    data: {
        price: 0,
        cash_max_day: -1,
        selected: -1,
        custom:{},
        focus:true,
        txId:null
    },
    onLoad: function(e) {
        this.setData({
            txId:e.organizeId
        })
    },
    onShow: function() {
        let t = this, 
            shareSetting = wx.getStorageSync('SHARE_SETTING'), 
            custom = wx.getStorageSync('CUSTOM');
        t.setData({
            share_setting: shareSetting,
            custom: custom
        }), 
        jdFetch({
            url: 'share/get-price',
            data:{
                organizeId:t.data.txId
            }
        }).then(res => {
            console.log("可提现金额:",res)  ;
            if (0 == res.code) {
                var cash_last = res.data.cash_last, 
                    name = "", 
                    mobile = "", 
                    bank_name = "", 
                    check = "", 
                    selected = this.data.selected;
                cash_last && (name = cash_last.name, 
                    mobile = cash_last.mobile, 
                    bank_name = cash_last.bank_name, 
                    check = cash_last.type), 
                t.setData({
                    price: res.data.price.price,
                    cash_max_day: res.data.cash_max_day,
                    pay_type: res.data.pay_type,
                    bank: res.data.bank,
                    remaining_sum: res.data.remaining_sum,
                    name: name,
                    mobile: mobile,
                    bank_name: bank_name,
                    selected: selected,
                    check: check,
                    cash_service_charge: res.data.cash_service_charge,
                    service_content: res.data.service_content,
                    pay_type_list: res.data.pay_type_list
                });
            }
        })
    },
    onPullDownRefresh: function() {},
    formSubmit: function(e) {
        console.log(e.detail.formId)
        var t = this, 
            cash = parseFloat(parseFloat(e.detail.value.cash).toFixed(2)), 
            price = t.data.price;
        if (-1 != t.data.cash_max_day && (price = min(price, t.data.cash_max_day)), cash) if (price < cash) wx.showToast({
            title: "提现金额不能超过" + price + "元",
            duration:1000,
            icon:"none"
        }); else if (cash < parseFloat(t.data.share_setting.min_money)) wx.showToast({
            title: "提现金额不能低于" + t.data.share_setting.min_money + "元",
            duration:1000,
            icon:"none"
        }); else {
            var selected = t.data.selected;
            if (0 == selected || 1 == selected || 2 == selected || 3 == selected) {
                if (0 == selected || 1 == selected || 2 == selected) {
                    if (!(name = e.detail.value.name) || null == name) return void wx.showToast({
                        title: "姓名不能为空",
                        duration:2000,
                        icon:"none"
                    });
                    if (!(mobile = e.detail.value.mobile) || null == mobile) return void wx.showToast({
                        title: "账号不能为空",
                        duration:2000,
                        icon:"none"
                    });
                }
                if (2 == selected) {
                    if (!(bank_name = e.detail.value.bank_name) || null == bank_name) return void wx.showToast({
                        title: "开户行不能为空",
                        duration:2000,
                        icon:"none"
                    });
                } else var bank_name = "";
                if (3 == selected) {
                    bank_name = "";
                    var mobile = "", name = "";
                }
                wx.showLoading({
                    title: "正在提交",
                    mask: !0
                })
                jdFetch({
                    url: 'share/apply',
                    data: {
                        cash: cash,
                        name: name,
                        mobile: mobile,
                        bank_name: bank_name,
                        pay_type: selected,
                        scene: "CASH",
                        organizeId:t.data.txId
                    }
                }).then(res => {
                    wx.hideLoading();
                    wx.showModal({
                        title: "提示",
                        content: res.msg,
                        showCancel: !1,
                        success: function(resm) {
                            resm.confirm && 0 == res.code && wx.redirectTo({
                                url: "/pages/share/cash-detail/cash-detail"
                            });
                        }
                    });
                })
            } else wx.showToast({
                title: "请选择提现方式",
                duration:2000,
                icon:"none"
            });
        } else wx.showToast({
            title: "请输入提现金额",
            duration:2000,
            icon:"none"
        });
    },
    showCashMaxDetail: function() {
        wx.showModal({
            title: "提示",
            content: "今日剩余提现金额=平台每日可提现金额-今日所有用户提现金额"
        });
    },
    select: function(e) {
        var index = e.currentTarget.dataset.index;
        index != this.data.check && this.setData({
            name: "",
            mobile: "",
            bank_name: ""
        }), this.setData({
            selected: index
        });
    },
    loseFocus() {
        this.setData({
            focus:false
        })
    }
});