// 获取全局应用程序实例对象
const app = getApp();
// import { PHONE } from '../../utils/regular.js';
// import Tip from '../../utils/tip';
// import ProductApi from '../../api/commodity';
import { jdFetch } from '../../../utils/fetch.js';
// 创建页面实例对象
Page({
  /**
   * 页面的初始数据
   */
  data: {
    qrcode: ""
  },
  onLoad() {
    var t = this;
    wx.getStorageSync('SHARE_SETTING');
    wx.showLoading({
        title: "正在加载",
        mask: !0
    });
	
	var appid = wx.getStorageSync("appId")
    jdFetch({ 
      // hh:1,
      url:'share/get-qrcode',
      data:{
        memberId: wx.getStorageSync("memberId"),
		    appid: appid
      }
    }).then(res => { 
      0 == res.code ? t.setData({
        qrcode: res.data 
      }) : wx.showModal({
          title: "提示",
          content: res.msg,
          showCancel: !1
      });
      wx.hideLoading();
    });
  },
  click(){
    wx.previewImage({
      current: this.data.qrcode,
      urls: [ this.data.qrcode ]
    });
  },
  onShow() {
    console.log('app',app)
    
  }
});
