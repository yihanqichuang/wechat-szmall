// 获取全局应用程序实例对象
const app = getApp();
var is_no_more = !1, is_loading = !1, p = 2;
// import { PHONE } from '../../utils/regular.js';
// import Tip from '../../utils/tip';
// import ProductApi from '../../api/commodity';
import { jdFetch } from '../../../utils/fetch.js';
// 创建页面实例对象
Page({
  /**
   * 页面的初始数据
   */
  data: {
    status: -1,
    cash_list: [],
    show_no_data_tip: !1
  },
  onLoad(opt) {
    is_loading = is_no_more = !1, p = 2,
    this.LoadCashList(opt.status || -1);
  },

  onShow() { },
  changeStatus(e){
    let status=e.currentTarget.dataset.status;
    is_loading = is_no_more = !1, p = 2,
    this.LoadCashList(status || -1);
  },
  LoadCashList: function(opt) {
    var t = this;
    console.log('LoadCashList',opt)
    t.setData({
        status: parseInt(opt || -1)
    }),
    wx.showLoading({
        title: "正在加载",
        mask: !0
    }),
    jdFetch({
      url:'share/cash-detail',
      data:{
        status: t.data.status
      }
    }).then(res => {
      console.log(res);
      0 == res.code && t.setData({
        cash_list: res.data.list
      }), t.setData({
          show_no_data_tip: 0 == t.data.cash_list.length
      });
      wx.hideLoading();
    })
  },
  onReachBottom: function() {
    var t = this;
    is_loading || is_no_more || (is_loading = !0, 
      jdFetch({
        url:'share/cash-detail',
        data:{
          status: t.data.status,
          page: p
        }
      }).then(res => {
        console.log(res);
        if (0 == res.code) {
          var a = t.data.cash_list.concat(res.data.list);
          t.setData({
            cash_list: a
          }), 0 == res.data.list.length && (is_no_more = !0);
        }
        p++;
        is_loading = !1;
      })
    );
  }
});
