// 获取全局应用程序实例对象
// const app = getApp()

import categoryApi from '../../../api/category'
import {
	jdFetch
} from '../../../utils/fetch.js'
// 创建页面实例对象
Page({
	data: {
		list: [],
		tabs: [ //
			{
				name: '默认排序',
				type: ''
			},
			{
				name: '价格',
				type: 'PRICE'
			},
			{
				name: '佣金',
				type: 'PERCENT'
			}
		],
		sortType: '',
		priceType: '',
		searchKey: '',
		isShare: 0,
		p: 0,
		is_no_more: !1,
		is_loading: !1
	},
	onLoad(option) {
		this.setData({
			isShare: parseInt(wx.getStorageSync("SHARE_SETTING").is_share_price)
		})
		if (option.categoryName) {
			wx.setNavigationBarTitle({
				title: option.categoryName
			});
		}
		this.getList();
	},
	// 获取 列表数据
	getList() {
		let commodityList = [];
		let type = 'id',
			priceType = '';
		let t = this;
		if (this.data.sortType == 'PRICE') {
			type = 'price';
		} else if (this.data.sortType == 'PERCENT') {
			type = 'max_share_price'
		};
		if (this.data.priceType == 'ASC') {
			priceType = 'asc';
		} else if (this.data.priceType == 'DESC') {
			priceType = 'desc'
		};
		jdFetch({
			// hh:1,
			url: 'default/goods-share-list',
			data: {
				page: t.data.p,
				type: type,
				sort: priceType,
				keyword: this.data.searchKey
			}
		}).then(res => {
			if (res.code == '0') {
				console.log('获取分销商品列表', res);
				commodityList = [...this.data.list, ...res.data];
				this.setData({
					list: commodityList
				})
				t.setData({
					p: t.data.p + 1,
					is_loading: !1
				})
				if (p >= res.pages) {
					t.setData({
						is_no_more: !0
					})
				}
			}
		}).catch(err => {

		})
	},
	// 顶部搜索栏搜索方法
	handleConfirm(e) {
		let searchKey = e.detail.value;
		this.setData({
			searchKey: searchKey,
			list: []
		});
		this.setData({
			p: 0
		})
		this.getList();
	},

	// 排序选择
	handleSortType(e) {
		let sortType = e.currentTarget.dataset.type;
		let priceType = this.data.priceType;
		if (sortType == 'PRICE') {
			priceType = priceType == 'ASC' ? 'DESC' : 'ASC';
		} else if (sortType == 'PERCENT') {
			priceType = priceType == 'ASC' ? 'DESC' : 'ASC';
		} else {
			priceType = ''
		}
		this.setData({
			list: [],
			p:0
		})
		this.setData({
			sortType,
			priceType
		})
		
		this.getList();
	},
	// 顶部选择器展开切换方法
	switchSelect() {
		this.setData({
			showSelect: !this.data.showSelect
		})
	},
	onReachBottom: function() {
		var t = this;
		t.data.is_loading || t.data.is_no_more || (t.data.is_loading = !0,
			t.setData({
				is_loading: t.data.is_loading
			}),
			t.getList()
		);
	},
	onHide() {}
	/**
	 * 生命周期函数--监听页面加载
	 */

})
