var app = getApp(), 
    is_no_more = !1, 
    is_loading = !1, 
    p = 2;

    import {jdFetch} from '../../../utils/fetch.js'
Page({
    data: {
        status: -1,
        list: [],
        hidden: -1,
        is_no_more: !0,
        is_loading: !1
    },
    onLoad: function(opt) {
        is_loading = is_no_more = !1, 
        p = 2, 
        this.GetList(opt.status || -1);
    },
    changeOrderClass(e){
        let status = e.currentTarget.dataset.status;
        this.GetList(status);
    },
    GetList: function(status) {
        var t = this; 
        t.setData({
            status: parseInt(status || -1)
        })
        wx.showLoading({
            title: "正在加载",
            mask: !0
        })
        jdFetch({
            // hh:1,
            url: 'share/get-order',
            data: {
                status: t.data.status
            }
        }).then(res => {
            wx.hideLoading();
            if(res.code=='0'){
                t.setData({
                    list: res.data
                })
            };
            if(res.data.length == 0) {
                t.setData({
                    is_no_more: !0
                });
            }
        }).catch(err => {
            wx.hideLoading();
        })
    },
    onReady: function() {
    },
    onShow: function() {
    },
    onHide: function() {
    },
    onUnload: function() {
    },
    onPullDownRefresh: function() {},
    click: function(e) {
        var index = e.currentTarget.dataset.index;
        this.setData({
            hidden: this.data.hidden == index ? -1 : index
        });
    },
    onReachBottom: function() {
        var t = this;
        is_loading || is_no_more || (is_loading = !0, 
            t.setData({
                is_loading: is_loading
            }),
            jdFetch({
                // hh:1,
                url: 'share/get-order',
                data: {
                    status: t.data.status,
                    page: p
                }
            }).then(res => {
                if (0 == res.code) {
                    var list = t.data.list.concat(res.data);
                    t.setData({
                        list: list
                    }), 0 == res.data.length && (is_no_more = !0, t.setData({
                        is_no_more: is_no_more
                    }));
                }
                p++;

                is_loading = !1;
                t.setData({
                    is_loading: is_loading
                });
            })
        );
    }
});