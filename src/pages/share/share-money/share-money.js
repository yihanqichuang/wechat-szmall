// 获取全局应用程序实例对象
const app = getApp();
// import { PHONE } from '../../utils/regular.js';
// import Tip from '../../utils/tip';
// import ProductApi from '../../api/commodity';
import { jdFetch } from '../../../utils/fetch.js';
// 创建页面实例对象
Page({
  /**
   * 页面的初始数据
   */
  data: {
    block: !1,
    active: "",
    total_price: 0,
    price: 0,
    cash_price: 0,
    un_pay: 0
  },
  onLoad() {
  },

  onShow() {
    var t = this, 
    share_setting = wx.getStorageSync('SHARE_SETTING'), 
    custom = wx.getStorageSync('CUSTOM');
    t.setData({
        share_setting: share_setting,
        custom: custom
    }), 
    wx.showLoading({
        title: "正在加载",
        mask: !0
    }),
    jdFetch({
      url:'share/get-price'
    }).then(res => {
      0 == res.code && t.setData({
        total_price: res.data.price.total_price,
        price: res.data.price.price,
        cash_price: res.data.price.cash_price,
        un_pay: res.data.price.un_pay
      });
      wx.hideLoading();
    })
  },
  tapName: function(t) {
    var e = this, a = "";
    e.data.block || (a = "active"), e.setData({
        block: !e.data.block,
        active: a
    });
  }
});
