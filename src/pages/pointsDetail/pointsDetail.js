// 获取全局应用程序实例对象
const app = getApp()
const points = require('../../api/points.js');
import shareLog from '../../api/shareLog.js';
import Tips from '../../utils/tip.js';
import wxtemplate from '../../api/wxtemplate';
import benMath from '../../utils/BenMath';
import wxPay from '../../utils/wxPay'
import query from '../../utils/query'
import GetPage from '../../utils/getCurrentPageUrl'

// 创建页面实例对象
Page({
  /**
   * 页面的初始数据
   */
  data: {
    pointsRewardId: '',  // 礼品 ID
    info: {},
    exchangeNum: 1,  // 兑换数量
    summary: 0,    // 兑换需要的积分
    canExchange: false,  // 是否可兑换
    token: {
      name: '',
      token: ''
    },
    disableButton: {   // 禁用增减按钮
      plus: true,
      minus: true
    },
    orderInfo: {},
    isShow: false,
    messageComponent: '',
    pending: false,
    formId: ''
  },

  // 获取 礼品详情
  getPointsDetail(params) {
    points.fetchPointsDetail(params).then(res => {
      if (!res.data.flag) {
        Tips.loaded();
        return
      }
      let info = res.data.data;
      if (info.validityType == 'STABLE' || info.startTime) {
        info.startTime = info.startTime.substr(0, 16).replace(/-/g, '.');
        info.endTime = info.endTime.substr(0, 16).replace(/-/g, '.');
      }
      this.setData({
        info: info,
        exchangeNum: 1,
        'disableButton.plus': true,
        'disableButton.minus': true,
        posterInfo: {
          id: info.pointsRewardId,
          picturePath: info.picUrl,
          commodityName: info.relateName,
          qrcodeUrl: info.qrcodeUrl,
          memberHead: info.memberHead,
          memberNickName: info.memberNickName,
          price: info.accessType == 'MIXED' ? `${info.needScore}积分+${info.accessValue}元` : `${info.needScore}积分`,
          logoUrl: '',
          organizeName: wx.getStorageSync('organizeName'),
          pageUrl:`${GetPage.getCurrentPageUrl()}`
        }
      });
      // if (this.data.orderNo) {
      //   this.setData({
      //     exchangeNum: info.detailAccount,
      //     summary: info.price,
      //     points: info.points
      //   })
      // }
      if (info.state === 'CANEXCHANGE') {
        let plus = (info.limitGetNum && (info.limitGetNum === 1 || info.limitGetNum - info.singleGetNum <= 1) || info.balanceNum <= 1);
        this.setData({
          canExchange: true,
          'disableButton.plus': plus,
          'token.name': res.data.submit_tokens_name,
          'token.token': res.data[res.data.submit_tokens_name]
        })
      }
      this.setData({ isShow: true });
      this.sum();
      app.updateLocalOrganize(info.organizeId);
      Tips.loaded()
    })
  },
  // 增加或减少 兑换数量
  changeNum(event) {
    if (!this.data.canExchange) return; // 不满足兑换条件
    if (this.data.orderNo && this.data.info.detailAccount) return;      // 有订单
    let val = Number(event.currentTarget.dataset.val);  // plus-增加   minus-减少
    let exchangeNum = this.data.exchangeNum + val;
    if (exchangeNum > this.data.info.balanceNum) {
      // 大于剩余数量
      return this.messageComponent.applyActive('宝贝只有这么多啦')
    } else if (this.data.info.limitGetNum && exchangeNum > this.data.info.limitGetNum - this.data.info.singleGetNum) {
      // 大于限制数量
      return this.messageComponent.applyActive('只能购买这么多啦')
    } else if (exchangeNum < 1) {
      // 减少 至少为 1
      return this.messageComponent.applyActive('购买数量需大于0')
    } else {
      this.setData({ exchangeNum });
      this.sum()
    }
  },

  // 计算所需积分
  sum() {
    // 需要的金额
    let summary = benMath.accMul(this.data.exchangeNum, this.data.info.accessValue);
    // 需要的积分
    let points = benMath.accMul(this.data.exchangeNum, this.data.info.needScore);
    this.setData({ summary, points })
  },

  // 立即兑换
  submit() {
    let memberType = wx.getStorageSync('memberType');
    let state = this.data.info.state;
    if (this.data.pending) return;
    if (memberType !== 'MEMBER') {
      wx.navigateTo({
        url: `/pages/memberSign/memberSign`
      });
      return
    }

    // 积分不足
    if (state === 'NOPOINTS') {
      this.messageComponent.applyActive('您的积分不足，领取失败');
      return
    }

    // 如果是订单过来的直接唤起微信支付
    if (this.data.orderNo && this.data.info.detailAccount) {
      this.wxPay();
      return
    }

    // 只有CANEXCHANGE状态 && 上架状态 才可以点击
    if (state == 'CANEXCHANGE' && this.data.info.rewardState == 'ONLINE') {
      Tips.confirm(`您正在使用${this.data.points}积分兑换礼品，请确认是否立即支付？`, {}, '温馨提示', '再想想', '立即支付').then(() => {
        Tips.loading('购买中');
        this.setData({ pending: true });
        this.fetchExchange().then(() => {
          if (this.data.info.accessType == 'POINTS') {
            Tips.confirm('商品已放置于“我的兑换” ，记得到店核销兑换哟～', {}, '购买成功', '取消', '去看看').then(() => {
              this.toExchange()
            }).catch(() => {
              this.setData({ orderNo: '' });
              this.getPointsDetail({ pointsRewardId: this.data.pointsRewardId });
            })
          } else if (this.data.info.accessType == 'MIXED') {
            this.wxPay()
          }
        })
      })
    }
  },

  // 兑换(相当于下单接口) Promise
  fetchExchange() {
    return new Promise((resolve, reject) => {
      let params = {
        pointsRewardId: this.data.pointsRewardId,
        exchangeNum: this.data.exchangeNum
      };
      points.fetchPointsExchange(params).then(res => {
        this.setData({ pending: false });
        Tips.loaded();
        if (res.data.flag) {
          this.setData({
            orderNo: res.data.data.orderInfo ? res.data.data.orderInfo.orderNo : null
          });
          resolve();
        } else {
          this.messageComponent.applyActive(res.data.msg || '购买失败');
        }
      }, () => {
        this.setData({ pending: false });
        Tips.loaded();
        this.messageComponent.applyActive('购买失败!');
        reject();
      })
    })
  },

  // 微信支付
  wxPay() {
    wxPay.getNewWxPayConfig(this.data.orderNo).then(() => {
      this.setData({ orderNo: '' });
      this.getPointsDetail({ pointsRewardId: this.data.pointsRewardId });
    }).catch(err => {
      if (err === '取消支付') {
        Tips.confirm('您的订单在15分钟内未支付将被取消，请尽快完成支付', {}, '确认取消支付', '继续支付', '确认离开').then(() => {
          wx.redirectTo({
            url: '/pages/newPackage/mallOrders/mallOrders'
          })
        }).catch(() => {
          this.wxPay()
        })
      }
    })
  },

  // 跳转到 我的兑换
  toExchange() {
    wx.setStorageSync('exchangeList', 'exchangeList');
    wx.redirectTo({
      url: '../exchangeList/exchangeList'
    })
  },

  // 提交表单获取formId
  formSubmit(e) {
    this.setData({ formId: e.detail.formId });
    wxtemplate.formSubmit(e).then(res => {
      this.setData({ formId: res });
      this.submit()
    }).catch(() => {
      this.submit()
    })
  },

  // 分享
  onShareAppMessage() {
    // 转发成功之后记录分享的次数
    shareLog.shareLogCommon(this.data.pointsRewardId, 'POINTSREWARD');
    let organizeId = wx.getStorageSync('organizeId'),  organizeName = wx.getStorageSync('organizeName');
    return {
      path: '/pages/pointsDetail/pointsDetail?id=' + this.data.pointsRewardId + '&organizeId=' + organizeId + '&organizeName=' + organizeName + '&share=true'
    }
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(option) {
    this.messageComponent = this.selectComponent('.messageTips');
    if (option.scene) {
      let scene = decodeURIComponent(option.scene);
      let id = query.getQueryString('id', scene);
      this.setData({ pointsRewardId: id });
    } else {
      this.setData({
        pointsRewardId: option.id,
        orderNo: option.orderNo || ''
      });
    }
    if (this.data.orderNo) {
      wx.setNavigationBarTitle({
        title: '确认支付'
      })
    }
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {
    Tips.loading();
    this.setData({
      pending: false
    });
    this.getPointsDetail({ pointsRewardId: this.data.pointsRewardId })
  }
});
