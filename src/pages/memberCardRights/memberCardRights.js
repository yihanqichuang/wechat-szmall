// 获取全局应用程序实例对象
const app = getApp()
import userApi from '../../api/user.js'
import WxParse from '../../wxParse/wxParse.js'

// 创建页面实例对象
Page({
  /**
   * 页面的初始数据
   */
  data: {
    showRights: false,
    title: 'memberCardRights',
    memberCardList: [],
    activeIndex: 0,
    listCardLevel: [],
    listRights: [],
    messageComponent: '',
    festivalRights: 'N', // 纪念日营销权益显示flag
    memberRights: 'N',// 会员日营销权益显示flag
    birthdayRights: 'N',// 生日营销权益显示flag
    rightsIconList: [],// 所有会员等级的纪念日营销图标是否显示list
  },

  // 显示会员权益
  showRightsDesc(e) {
    this.setData({
      showRights: true
    })
    let privilegeid = e.currentTarget.dataset.privilegeid;
    this.data.listCardLevel.forEach(card => {
      card.listPrivilege.forEach(privilege => {
        if (privilege.privilegeId == privilegeid) {
          // list.push(card.gradeName);
          let article = privilege.privilegeDesc;
          article && WxParse.wxParse('article', 'html', article, this, 15);
        }
      })
    });
  },

  // 滑动img
  handleScrollImage(e) {
    let current = e.detail.current;
    this.setData({
      activeIndex: current,
      listRights: this.data.listCardLevel[current].listPrivilege,
      showRights: false
    })
    this.handleShowRightsIcon();
  },

  // 获取纪念日营销图标是否显示
  showLevelPowerByGradeId() {
    let gradeIdList = [];
    for (let i = 0; i < this.data.listCardLevel.length; i++) {
      gradeIdList.push(this.data.listCardLevel[i].gradeId);
    }
    let param = { gradeIdList: gradeIdList }
    userApi.showLevelPowerByGradeId(param).then(res => {
      if (res.flag && res.data && res.data.length > 0) {
        this.setData({
          rightsIconList: res.data
        })
        this.handleShowRightsIcon();
      }
    })
  },

  // 处理纪念日营销权益图标显示
  handleShowRightsIcon() {
    for (let i = 0; i < this.data.rightsIconList.length; i++) {
      if (this.data.rightsIconList[i].gradeId === this.data.listCardLevel[this.data.activeIndex].gradeId) {
        this.setData({
          festivalRights: this.data.rightsIconList[i].isShowFestival,
          memberRights: this.data.rightsIconList[i].isShowMemberDay,
          birthdayRights: this.data.rightsIconList[i].isShowBirthday,
        })
        break;
      }
    }
  },

  getData() {
    userApi.listCardLevel().then(res => {
      if (res.data.flag) {
        this.setData({
          listCardLevel: res.data.data,
          listRights: res.data.data[this.data.activeIndex].listPrivilege
        })
      } else {
        this.messageComponent.applyActive(res.data.msg || '获取数据失败')
      }
    }).catch(err => {
      console.log(err)
    })
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad() {
    // TODO: onLoad
    this.messageComponent = this.selectComponent('.messageTips')
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {
    // TODO: onReady
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {
    // TODO: onShow
    this.getData()
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {
    // TODO: onHide
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {
    // TODO: onUnload
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {
    // TODO: onPullDownRefresh
  }
})
