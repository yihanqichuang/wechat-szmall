import question from '../../api/question.js';

// 创建页面实例对象
Page({
  /**
   * 页面的初始数据
   */
  data: {
    bottomTipsFlag: true,
    messageComponent: '',
    questionList: [],
    pageQuestion: {
      pageNum: 1,
      pageSize: 10,
      total: 1
    },
    organizeId: '',
    activeIndex: 0,
    navSlider: {
      width: '40rpx',
      left: '50rpx',
      position: ['50rpx', '220rpx', '420rpx', '620rpx']
    },
    tabs: [  //
      {
        name: '全部',
        state: ''
      },
      {
        name: '未开始',
        state: 'UNSTART'
      },
      {
        name: '进行中',
        state: 'DOING'
      },
      {
        name: '已结束',
        state: 'END'
      }
    ]
  },
  // 改变 tab 点击头部
  changeIndex(data) {
    let current = data.detail;
    this.setData({
      activeIndex: current
    });
    console.log(data)
    this.sliderPosition(current)
    this.getQuestionList(1, this.data.tabs[current].state)
  },
  // nav-slider 的位置
  sliderPosition(index) {
    this.setData({
      'navSlider.left': this.data.navSlider.position[index]
    })
  },
  // 获取问卷列表数据
  getQuestionList(pageNum, state) {
    let params = {
      pageNum: pageNum,
      state: state
    };
    if (pageNum === 1) this.setData({ questionList: [] });
    question.questionList(params).then(res => {
      if (res.data.flag) {
        let ret = res.data;
        console.log(ret);
        ret.data.forEach((item) => {
          item.quesStartTimeStr = item.startTime.replace(/-/g, '.');
          item.quesEndTimeStr = item.endTime.replace(/-/g, '.');
        //   item.picPathUrl = item.restfulUrl.split(',')[0];
        });
        this.setData({
          questionList: [...this.data.questionList, ...ret.data],
          pageQuestion: ret.page
        });
      } else {
        this.messageComponent.applyActive(res.data.msg || '加载数据失败')
      }
    }).catch(err => {
      console.log(err)
    })
  },

  // 加载更多问卷列表数据
  loadMoreQuestion(e) {
    this.getQuestionList(e.detail)
  },


  /**
   * 生命周期函数--监听页面加载
   */
  onLoad() {
    this.messageComponent = this.selectComponent('.messageTips');
    this.getQuestionList(1)
  }
});
