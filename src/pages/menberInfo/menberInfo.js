import menberInfoApi from '../../api/menberInfo.js'

// 获取全局应用程序实例对象
// const app = getApp()

// 创建页面实例对象
Page({
  /**
   * 页面的初始数据
   */
  data: {
    companygrade: '',
    cardno: '',
    cardIssuer: '',
    cardShop: '',
    companypoint: '',
    company22point: '',
    couponNum: ''
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad() {
    // TODO: onLoad
    let params = {
      memberId: wx.getStorageSync('memberId')
    };
    menberInfoApi.getData(params).then((res) => {
      this.setData({
        companygrade: res.data.data.companygrade,
        cardno: res.data.data.cardno,
        cardIssuer: res.data.data.cardIssuer,
        cardShop: res.data.data.cardShop,
        companypoint: res.data.data.companypoint,
        company22point: res.data.data.company22point,
        couponNum: res.data.data.couponNum
      })
    }).catch(err => {
      console.log(err);
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {
    // TODO: onReady
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {
    // TODO: onShow
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {
    // TODO: onHide
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {
    // TODO: onUnload
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {
    // TODO: onPullDownRefresh
  }
});
