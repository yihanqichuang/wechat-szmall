// 获取全局应用程序实例对象
// const app = getApp()
const activityApi = require('../../api/activity.js');
import voteApi from '../../api/vote.js'
import {fetchOrganizeList} from '../../api/markets'
// 创建页面实例对象
Page({
  /**
   * 页面的初始数据
   */
  data: {
    tabs: [
      {
        name: '活动报名',
        type: '0'
      },
      {
        name: '投票报名',
        type: '1'
      }
    ],
    activeIndex: 0,
    navSlider: {
      width: '40rpx',
      left: '165rpx',
      position: ['165rpx', '540rpx']
    },

    subActivityIndex: 0,
    subActivityTabs: [
      {
        name: '待参加',
        type: '0'
      },
      {
        name: '全部活动',
        type: '1'
      }
    ],
    subVoteTabs: [
      {
        name: '全部',
        type: ''
      },
      {
        name: '报名中',
        type: 'ENROLLING'
      },
      {
        name: '报名失败',
        type: 'FAIL'
      },
      {
        name: '报名成功',
        type: 'SUCCESS'
      }
    ],
    subVoteIndex: 0,
    // 投票
    listVoteAll: [],
    pageVoteAll: {
      pageNum: 1,
      pageSize: 10,
      total: 1
    },

    // 活动
    listAll: [],
    pageAll: {
      pageNum: 1,
      pageSize: 10,
      total: 1
    },
    emptyType: 'activity',
    bottomTipsFlag: true,
    messageComponent: '',
    isMember: false
  },
  // 获取组织列表
  getOrganizeList() {
    let params = {
    };
    fetchOrganizeList(params).then(res => {
      console.log('获取组织列表', res)
      if (res.data.flag) {
        let organizeList = res.data.data;
        let list=[{
          organizeId:0,
          organizeName: '全部门店'
        }];
        organizeList.forEach(item => {
          console.log('item', item.organizeList);
          list.push(...item.organizeList);
        });
        if (organizeList && organizeList.length > 0) {
          this.setData({
            organizeList: list
          });
        }
      } else {
        this.messageComponent.applyActive(res.data.msg || '获取列表数据异常');
      }
    }).catch(err => {
      console.log(err)
    })
  },
  onChooseChanges(event) {
    console.log('获取列表',event);
    // this.setData({
    //   theOrganizeId: event.detail.organizeId
    // });
    // this.getList(1,event.detail.organizeId)
  },
  // 改变 tab 点击头部
  changeIndex(data) {
    let current = data.detail;
    this.setData({
      activeIndex: current
    });
    this.sliderPosition(current)
    if (current == 0) {
      this.getExchangeList(this.data.subActivityTabs[this.data.subActivityIndex].type, 1)
    } else if (current == 1) {
      this.getVoteList(this.data.subVoteTabs[this.data.subVoteIndex].type, 1)
    }
  },

  // nav-slider 的位置
  sliderPosition(index) {
    this.setData({
      'navSlider.left': this.data.navSlider.position[index]
    })
  },

  // 改变 swiper 手势滑动
  swiperChange(event) {
    let current = event.detail.current;
    this.setData({
      activeIndex: current
    });
    this.sliderPosition(current);
    if (current == 0) {
      this.getExchangeList(this.data.subActivityTabs[this.data.subActivityIndex].type, 1)
    } else if (current == 1) {
      this.getVoteList(this.data.subVoteTabs[this.data.subVoteIndex].type, 1)
    }
  },

  // 获取活动列表
  getExchangeList(queryType, pageNum) {
    if (pageNum === 1) this.setData({ listAll: [] });
    let params = {
      type: queryType,
      pageNum,
      memberId: wx.getStorageSync('memberId')
    };
    console.log(params);
    activityApi.activityList(params).then(res => {
      if (!res.data.flag) {
        let msg = res.data.msg || '请求数据失败';
        this.messageComponent.applyActive(msg);
        return
      }

      res.data.data.forEach((item) => {
        item.activityStarttimeStr = item.activityStarttime.substr(0, 10).replace(/-/g, '.');
        item.activityEndtimeStr = item.activityEndtime.substr(0, 10).replace(/-/g, '.');
        item.signupTime = item.signupTime.replace(/-/g, '.');
      });

      this.setData({
        listAll: [...this.data.listAll, ...res.data.data],
        pageAll: res.data.page
      })
    }, () => {
      this.messageComponent.applyActive('请求数据失败！');
      console.log('err')
    })
  },

  // 点击活动报名筛选
  handleActivityKind(e) {
    let dataset = e.currentTarget.dataset;
    let index = dataset.index;
    this.setData({
      subActivityIndex: index
    });
    this.getExchangeList(this.data.subActivityTabs[index].type, 1)
  },

  // 获取投票活动列表
  getVoteList(queryType, pageNum) {
    if (pageNum === 1) this.setData({ listVoteAll: [] });

    let params = {
      enrollState: queryType,
      pageNum: pageNum
    };
    console.log(params);
    voteApi.voteEnrollList(params).then(res => {
      if (!res.data.flag) {
        let msg = res.data.msg || '请求数据失败';
        this.messageComponent.applyActive(msg);
        return
      }
      res.data.data.forEach((item) => {
        item.picPath = item.picPath.split(',')[0];
      });

      this.setData({
        listVoteAll: [...this.data.listVoteAll, ...res.data.data],
        pageVoteAll: res.data.page
      });
    }, () => {
      this.messageComponent.applyActive('请求数据失败！');
      console.log('err')
    })
  },

  // 点击投票活动筛选
  handleVoteKind(e) {
    let dataset = e.currentTarget.dataset;
    let index = dataset.index;
    this.setData({
      subVoteIndex: index
    });
    this.getVoteList(this.data.subVoteTabs[index].type, 1)
  },

  // 报名加载更多
  loadActivityMore(e) {
    let queryType = this.data.subActivityTabs[this.data.subActivityIndex].type;
    this.getExchangeList(queryType, e.detail)
  },

  // 投票加载更多
  loadVoteMore(e) {
    let queryType = this.data.subVoteTabs[this.data.subVoteIndex].type;
    this.getExchangeList(queryType, e.detail)
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(e) {
    this.messageComponent = this.selectComponent('.messageTips');
    if (e.isFirst) {
      this.setData({
        isFirst: e.isFirst
      })
    }
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {
    // TODO: onShow
    let activityList = wx.getStorageSync('activityList');
    let isMember = wx.getStorageSync('memberType');
    if (isMember == 'MEMBER') {
      this.setData({
        isMember: true
      })
    } else {
      this.setData({
        isMember: false
      })
    }
    console.log(activityList);
    this.getExchangeList('0', 1);
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {
    // TODO: onHide
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {
    // TODO: onUnload
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {
    // TODO: onPullDownRefresh
  },

  /**
   * 页面相关分享
   */
  onShareAppMessage() {
    
  }
})
