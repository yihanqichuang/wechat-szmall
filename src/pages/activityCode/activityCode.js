// 获取全局应用程序实例对象
// const app = getApp()
const activityApi = require('../../api/activity.js');
const QR = require('../../utils/wxqrcode.min.js');
import timeFormat from '../../utils/timeFormat.js';
import Tips from '../../utils/tip.js';

// 创建页面实例对象
Page({
  /**
   * 页面的初始数据
   */
  data: {
    info: {},  // 礼品核销信息
    member: {},  // 会员信息
    avatarUrl: '',    // 头像
    imgErweima: '',   // 二维码
    messageComponent: '',
    gainId: '',
    isStart: false,
    signStateText: '',
    // 报名记录信息
    statistic: {},
    // 核销过期状态：1 去参加 2 已完成 3 已失效
    type: 0
  },
  // 获取活动信息
  getInfo(params) {
    activityApi.activityCode(params).then(res => {
      this.setData({
        isStart: true
      });
      Tips.loading();
      console.log(res);
      if (!res.data.flag) {
        let msg = res.data.msg || '请求数据失败';
        this.messageComponent.applyActive(msg);
        Tips.loaded();
        return
      }
      let t = res.data.data.activity;
      let statistic = res.data.data.statistic;
      t.activityStarttimeStr = timeFormat.timeStr(t.activityStarttime, 'yyyy.MM.dd');
      t.activityEndtimeStr = timeFormat.timeStr(t.activityEndtime, 'yyyy.MM.dd');
      statistic.signupTimeStr = statistic.signupTime.replace(/-/g, '.');
      t.verifyCode = res.data.data.activity.verifyCode + '2';
      this.setType(t.memberSignupState);
      this.setData({
        info: res.data.data.activity,
        signStateText: res.data.data.activity.signStateText,
        statistic: res.data.data.statistic
      });
      let imgErweima = QR.createQrCodeImg(res.data.data.activity.verifyCode);
      this.setData({
        imgErweima: imgErweima
      });
      Tips.loaded();
    }, () => {
      this.messageComponent.applyActive('请求数据失败！');
      Tips.loaded();
    })
  },
  setType(useState) {
    let type = '';
    if (useState === '去参加') {
      type = 1
    } else if (useState === '已完成') {
      type = 2
    } else if (useState === '已失效') {
      type = 3
    }
    this.setData({ type: type })
  },
  handleJumpDetail(e) {
    let gainId = e.currentTarget.dataset.id;
    if (this.data.signStateText == '已下架') {
      this.messageComponent.applyActive('sorry,活动已下架~')
    } else {
      wx.navigateTo({
        url: `../activeCenter/activityDetail/activityDetail?id=${gainId}`
      })
    }
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(option) {
    this.messageComponent = this.selectComponent('.messageTips');
    this.setData({
      gainId: option.activityId,
      type: option.type ? option.type : this.data.type
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {
    // TODO: onReady
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {
    // TODO: onShow
    let params = {
      id: this.data.gainId
    };
    this.getInfo(params)
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {
    // TODO: onHide
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {
    // TODO: onUnload
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {
    // TODO: onPullDownRefresh
  }
});
