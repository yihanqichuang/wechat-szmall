// 获取全局应用程序实例对象
// const app = getApp()
const exchange = require('../../api/exchange.js');
import { fetchOrganizeList } from '../../api/markets'
// 创建页面实例对象
Page({
  /**
   * 页面的初始数据
   */
  data: {
    tabs: [
      {
        name: '待核销',
        type: 'UNUSE'
      },
      {
        name: '全部',
        type: ''
      }
    ],
    activeIndex: 0,
    navSlider: {
      width: '40rpx',
      left: '175rpx',
      position: ['175rpx', '550rpx']
    },
    listUnuse: [],
    pageUnuse: {
      pageNum: 1,
      pageSize: 10
    },
    listAll: [],
    pageAll: {
      pageNum: 1,
      pageSize: 10
    },
    emptyType: 'exchange',
    messageComponent: '',
    organizeList: [],
    theOrganizeId: ''
  },
  // 获取组织列表
  getOrganizeList() {
    let params = {
    };
    fetchOrganizeList(params).then(res => {
      if (res.data.flag) {
        let organizeList = res.data.data;
        let list = [{
          organizeId: '',
          organizeName: '全部门店'
        }];
        organizeList.forEach(item => {
          list.push(...item.organizeList);
        });
        if (organizeList && organizeList.length > 0) {
          this.setData({
            organizeList: list
          });
        }
      } else {
        this.messageComponent.applyActive(res.data.msg || '获取列表数据异常');
      }
    }).catch(err => {
      console.log(err)
    })
  },
  onChooseChanges(event) {
    this.setData({
      theOrganizeId: event.detail.organizeId ? event.detail.organizeId : -1
    });
    this.getExchangeList(this.data.tabs[this.data.activeIndex].type, 1)
  },
  // 改变 tab 点击头部
  changeIndex(e) {
    let index = e.currentTarget.dataset.index;
    this.setData({ activeIndex: index });
  },
  // 改变 swiper 手势滑动
  swiperChange(event) {
    let index = event.detail.current;
    this.setData({ activeIndex: index });
    this.getExchangeList(this.data.tabs[index].type, 1)
  },
  // 获取 礼品列表
  getExchangeList(queryType, pageNum) {
    exchange.fetchExchangeList({
      queryType: queryType,
      rewardType: 'GIFT',
      organizeId: this.data.theOrganizeId,
      pageNum,
      pageSize: 10
    }).then(res => {
      if (!res.data.flag) {
        let msg = res.data.msg || '请求数据失败';
        this.messageComponent.applyActive(msg);
        return
      }
      res.data.data.forEach((item) => {
        if (item.endTime == null || item.startTime == null) return;
        item.endTimeStr = item.endTime.substr(0, 16).replace(/-/g, '.');
        item.startTimeStr = item.startTime.substr(0, 16).replace(/-/g, '.');
      });
      if (queryType === 'UNUSE') {
        if (pageNum === 1) this.setData({ listUnuse: [] });
        this.setData({
          listUnuse: [...this.data.listUnuse, ...res.data.data],
          pageUnuse: res.data.page
        })
      } else {
        if (pageNum === 1) this.setData({ listAll: [] });
        this.setData({
          listAll: [...this.data.listAll, ...res.data.data],
          pageAll: res.data.page
        })
      }
    }, () => {
      this.messageComponent.applyActive('请求数据失败！')
    })
  },

  // 跳转到详情
  toExchange(event) {
    let gainId = event.currentTarget.dataset.gainid;
    wx.navigateTo({
      url: '../exchange/exchange?gainId=' + gainId
    })
  },

  // 加载更多
  loadMore(e) {
    console.log('加载更多：', e);
    let queryType = e.currentTarget.dataset.type;
    this.getExchangeList(queryType, e.detail)
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(e) {
    this.messageComponent = this.selectComponent('.messageTips');
    this.getOrganizeList();
    if (e.isFirst) {
      this.setData({
        isFirst: e.isFirst
      })
    }
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {
    let exchangeList = wx.getStorageSync('exchangeList');
    if (exchangeList == 'exchangeList' || this.data.isFirst) {
      wx.removeStorageSync('exchangeList');
      this.getExchangeList('UNUSE', 1)
    }
  },

  /**
   * 页面相关分享
   */
  onShareAppMessage() {

  }
});
