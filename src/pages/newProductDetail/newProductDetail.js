// 获取全局应用程序实例对象
const app = getApp()
import ProductApi from '../../api/commodity';
import ShopApi from '../../api/findshop';
import CartApi from '../../api/shoppingcart';
import PreSaleApi from '../../api/preSale';
import Tip from '../../utils/tip';
import wechat from '../../utils/wechat.js';
import WxParse from '../../wxParse/wxParse.js'
import query from '../../utils/query.js'
import InviteApi from '../../api/invite';
import GroupApi from '../../api/group';
import LoginApi from '../../api/login';

import { bindParent } from '../../utils/bindParent.js';

// 创建页面实例对象
Page({
    /**
     * 页面的初始数据
     */
    data: {
        isCollect: false,
        productCode: null,
        couponList: '',
        fullMarketInfo: '',
        productData: '',
        shopData: '',
        mallData: '',
        floorData: '',
        sellState: true,
        btnText: '立即购买',
        shoppingCartCount: 0,
        showSkuPicker: false,
        chooseWay: 'addCart', // addCart:加入购物车 buyNow:立即购买 openGroup:我要开团 joinGroup:去参团
        shareData: {},
        organizeType: '',
        secondInfo: null, // 第二件活动信息：DISCOUNT：第2件打折,ZERO:第2件0元
        preSaleData: null,
        distributionData: null, // 分销信息
        packageDiscountList: null, // 优惠套餐list
        distributionData: null, // 分销信息
        groupInfoCode: null,// 选择参团的那个团的团编码
        g: null,// 分享二维码过来的团编码
        ingGroupInfoList: [],// 正在进行中的团list
        groupInfo: null,
        pictureList: [
            'http://img.alicdn.com/imgextra/i3/2210843787782/O1CN01cCGYXK27MCsdiAtPI_!!2210843787782.jpg_430x430q90.jpg',
            'https://img.alicdn.com/imgextra/i4/2210843787782/O1CN01BLPBEM27MCscgOwQG_!!2210843787782.jpg_430x430q90.jpg'
        ],
        swiperIndex: 1,
        outOfStock: false
    },
    //事件处理函数
    swiperChange(e) {
        this.setData({
            swiperIndex: e.detail.current + 1
        })
    },
    // 打开可参与的拼单弹窗
    openGroupActivity() {
        this.selectComponent('#groupActivity').showDialog(this.data.groupInfo.groupInfoVOList);
    },
    getGroupInfo() {

    },
    // 处理正在拼团的团信息，用于滚动播放用(将list拆成多个明细list（一个明细list包含两个对象）)
    handleGroupInfoList() {
        if (this.data.groupInfo.groupInfoVOList && this.data.groupInfo.groupInfoVOList.length > 0) {
            let detailList = [];
            for (let i = 0; i < this.data.groupInfo.groupInfoVOList.length; i++) {
                if ((i + 1) % 2 != 0) {
                    detailList.push(this.data.groupInfo.groupInfoVOList[i]);
                    if (i === this.data.groupInfo.groupInfoVOList.length - 1) {
                        this.data.ingGroupInfoList.push(detailList);
                        detailList = [];
                    }
                } else {
                    detailList.push(this.data.groupInfo.groupInfoVOList[i]);
                    this.data.ingGroupInfoList.push(detailList);
                    detailList = [];
                }
            }
        }
        this.setData({
            ingGroupInfoList: this.data.ingGroupInfoList
        })
    },
    // 前往导航
    goFloorLocation() {
        wx.navigateTo({
            url: '/pages/newPackage/floorLocation/floorLocation'
        })
    },

    // 前往包含此商品的优惠套餐列表
    openPackageDiscountList() {
        wx.navigateTo({
            url: '/pages/newPackage/packageDiscount/packageDiscount?productSpuCode=' + this.data.productCode
        })
    },

    // 分享获取佣金
    shareDistribution() {
        if (wx.getStorageSync('memberType') !== 'MEMBER') {
            wx.navigateTo({
                url: '/pages/memberSign/memberSign'
            });
            return
        }
        // 判断是否为分销商（是：则弹出分享海报 否：进入申请成为分销商页面）
        if (this.data.distributionData.isShopDistribution === 'Y') {
            // 弹出分享海报
            this.selectComponent('#btnGroups').goTransmit();
        } else {
            let fxOrganize = {};
            console.log(wx.getStorageSync('organizeId'))
            fxOrganize.organizeId = wx.getStorageSync('organizeId');
            fxOrganize.organizeName = wx.getStorageSync('organizeName');
            fxOrganize.organizePhone = wx.getStorageSync('organizePhone');
            fxOrganize.organizeAddress = wx.getStorageSync('organizeAddress');
            fxOrganize.organizeType = wx.getStorageSync('organizeType');
            wx.setStorageSync('fxOrganize', fxOrganize);
            // 进入申请成为分销商页面
            wx.navigateTo({
                url: '/pages/share/add-share/index'
            })
        }
    },

    // 获取商品详情
    async getProductDetail() {
        Tip.loading();
        const res = await ProductApi.fetchCommodityDetail({
            productCode: this.data.productCode
        });
        const resData = res.data;
        console.log('商品详情1：', res);
        Tip.loaded();
        if (resData.flag) {
            //   if (resData.data.isDistribution === 'Y') {
            //     // 获取分销佣金
            //     const resDistribution = await ProductApi.fetchGetDetailDistributionPrice({
            //       productCode: this.data.productCode
            //     });
            //     if (resDistribution.flag) {
            //       let disData = resDistribution.data;
            //       disData.distributionPrice = parseFloat(disData.distributionPrice);
            //       this.setData({
            //         distributionData: disData
            //       })
            //     }
            //   }
            this.setData({
                organizeType: wx.getStorageSync("organizeType") //resData.data.organizeType
            })
            // 解析图片
            if (resData.data.productPic) {
                resData.data['pictureList'] = resData.data.productPic.split(',');
            }
            // 解析商品详情
            if (resData.data.productDetails) {
                let _this = this;
                WxParse.wxParse('article', 'html', resData.data.productDetails, _this, 15);
            }
            //   // 获取拼团商品信息
            //   if(resData.data.marketType === 'GROUP' && resData.data.productCode) {
            //     // 调用接口获取拼团商品信息
            //     const groupRes = await GroupApi.getGroupByProductCode({ productCode: resData.data.productCode });
            //     if(groupRes.flag && groupRes.data.groupActivityVO && 'PUTSHELF' === groupRes.data.groupActivityVO.shelfStatus){
            //       this.setData({
            //         groupInfo:groupRes.data
            //       })
            //       this.handleGroupInfoList();
            //     }else{
            //       // 加入取不到数据则显示为正常售卖的商品
            //       resData.data.marketType = "";
            //     }
            //   }
            //   let preSaleData = null;
            //   if (resData.data.marketType === 'PRESALE' && resData.data.marketCode) {
            //     const preSaleRes = await PreSaleApi.fetchQueryPreSale({ productVO: { marketCode: resData.data.marketCode } });
            //     console.log('获取预售详情：', preSaleRes.data.data);
            //     if (preSaleRes.flag) {
            //       preSaleData = preSaleRes.data.data;
            //     }
            //   }
            // 商品是否可以购买
            // 库存为0不允许购买，已买数量大于等于限购数量不允许购买
            let sellState = true;
            let btnText = '立即购买';

            //   if (resData.data.marketType === 'PRESALE' && preSaleData.preSaleModel === 'DEPOSIT') {
            //     btnText = '支付定金';
            //   } else if(resData.data.marketType === 'GROUP') {
            //     if(this.data.g){
            //       // 扫描二维码进来参团时
            //       btnText = '去参团';
            //     }else{
            //       btnText = '我要开团';
            //     }
            //   }
            //   if (preSaleData && preSaleData.marketState === 'WAIT') {
            //     sellState = false;
            //   } 
            //   else if (preSaleData && preSaleData.marketState === 'END') {
            //     sellState = false;
            //     btnText = '预售结束';
            //   } else if (this.data.groupInfo && this.data.groupInfo.groupActivityVO.activityStatus === 'END') {
            //     sellState = false;
            //     btnText = '拼团结束';
            //   }
            
            if (resData.data.sumSaleStock <= 0) {
                sellState = false;
                btnText = '已售罄';
            } else if (resData.data.limitMemberGradeFlag === 'Y') {
                sellState = false;
                btnText = '会员专享';
            } else if (resData.data.limitMemberPortraitFlag === 'Y') {
                sellState = false;
                btnText = '特定人群专享';
            } else if (resData.data.productState === 'OFFLINE') {
                sellState = false;
                btnText = '已下架';
            }
            //   else if (preSaleData && preSaleData.limitBuyNum && preSaleData.limitBuyNum <= resData.data.memberBuyCount) {
            //     sellState = false;
            //     btnText = '已限购';
            //   }
            //    else if (this.data.groupInfo && this.data.groupInfo.groupActivityVO.perActivityLimitNum && this.data.groupInfo.groupActivityVO.perActivityLimitNum <= resData.data.memberBuyCount) {
            //     sellState = false;
            //     btnText = '已限购';
            //   } 
            else if (resData.data.marketType != 'PRESALE' && resData.data.marketType != 'GROUP' && resData.data.limitBuyNum && resData.data.limitBuyNum <= resData.data.memberBuyCount) {
                sellState = false;
                btnText = '已限购';
            } 
            // else if (resData.data.marketType === 'TIMER' && resData.data.marketState != 'ING') {
            //     sellState = false;
            // }

            //   // 获取优惠套餐列表
            //   const resPackageList = await ProductApi.selectMarketPackageBySpuCode({
            //     marketPackageDiscountVO: {productSpuCode:this.data.productCode}
            //   });
            //   if (resPackageList.flag) {
            //     this.setData({
            //       packageDiscountList:resPackageList.data.length > 2 ? resPackageList.data.slice(0,2) : resPackageList.data
            //     })
            //   }
            let newPrice = "";
            //   if (resData.data.marketType === 'GROUP'){
            //     newPrice = resData.data.saleType === 'ALLMONEY' ? `￥${this.data.groupInfo.groupActivityVO.displayPrice}` : resData.data.saleType === 'ALLPOINTS' ? `${this.data.groupInfo.groupActivityVO.displayPoint}积分` : resData.data.saleType === 'MIXED' ? `${this.data.groupInfo.groupActivityVO.displayPoint}积分+${this.data.groupInfo.groupActivityVO.displayPrice}元` : '免费';
            //   } else {
            newPrice = resData.data.saleType === 'ALLMONEY' ? `￥${resData.data.minSalePrice}` : resData.data.saleType === 'ALLPOINTS' ? `${resData.data.minPoints}积分` : resData.data.saleType === 'MIXED' ? `${resData.data.minPoints}积分+${resData.data.minSalePrice}元` : '免费';
            //   }
            // 活动结束时间转时间戳
            if (resData.data.marketType === 'TIMER') {
                resData.data.marketEndTime = new Date(resData.data.marketEndTime.replace(/-/g, '/')).getTime();
            }

            // 菜场判断是否有货，无货显示无货按钮
            if(resData.data.productType == 'MARKET' && resData.data.skuExtList.length && !resData.data.skuExtList[0].shopName) {
               this.setData({
                outOfStock: true
               })
            }

            this.setData({
                productData: resData.data,
                sellState: sellState,
                btnText: btnText,
                shareData: {
                    id: resData.data.productCode,
                    picturePath: resData.data.pictureList[0],
                    commodityName: resData.data.productName,
                    price: newPrice,
                    oldPrice: resData.data.minLinePrice
                },
                // preSaleData: preSaleData
            });

            //   this.queryInviteCourteous(resData.data.organizeId);
            //   this.getCollectState();
            // 百货查询商户
            if (resData.data.productType != "MARKET") {
                this.getShopData();
                this.getProductFullMarket();
            }
            // 获取商品第二件活动信息
            //   if (this.data.productData.marketType == 'SECOND') {
            //     this.getProductSecondInfo(resData.data.marketCode);
            //   }

            app.updateLocalOrganize(resData.data.organizeId);
        } else {
            Tip.toast(resData.data, null, 'none');
        }
    },

    // 查询是否有邀请有礼活动
    queryInviteCourteous(organizeId) {
        InviteApi.queryInviteCourteous({ organizeId }).then(res => {
            console.log("邀请有礼详情", res)
            if (res.flag) {
                let data = res.data.data;
                this.setData({
                    inviteActivityInfo: data,
                })
            }
        });
    },

    // 邀请有礼
    inviteFriends() {
        wx.navigateTo({ url: `/pages/newPackage/inviteCourtesy/inviteCourtesy?organizeId=` + this.data.productData.organizeId })
    },

    // 获取商铺详情
    getShopData() {
        ShopApi.fetchShopData({
            shopId: this.data.productData.shopId
        }).then(res => {
            if (res.data.flag) {
                this.setData({
                    shopData: res.data.shop,
                    mallData: res.data.mall,
                    floorData: res.data.floor
                });
            }
        })
    },

    // 获取收藏状态
    getCollectState() {
        const params = {
            type: 'PRODUCT',
            relateId: this.data.productData.id
        };
        ShopApi.fetchCollectStatus(params)
            .then(res => {
                console.log('收藏状态：', res);
                if (res.data.flag) {
                    this.setData({
                        isCollect: res.data.data === 'Y'
                    });
                }
            })
    },

    // 收藏/取消收藏
    toggleCollect() {
        this.setData({
            isCollect: !this.data.isCollect
        });
        const params = {
            type: 'PRODUCT',
            relateId: this.data.productData.id,
            isCollect: this.data.isCollect ? 'Y' : 'N'
        };
        ShopApi.fetchDoCollect(params)
            .then(res => {
                console.log('收藏结果：', res);
                if (res.data.flag) {
                    const txt = this.data.isCollect ? '已收藏' : '已取消';
                    Tip.toast(txt, null, 'none');
                } else {
                    Tip.toast(res.data.data, null, 'none');
                }
            })
    },

    // 获取商品相关优惠券
    getProductCoupons() {
        ProductApi.fetchProductCoupons({
            productCode: this.data.productCode
        })
            .then(res => {
                console.log('商品相关优惠券：', res);
                if (res.data.flag) {
                    this.setData({
                        couponList: res.data.data
                    });
                }
            })
    },

    // 获取商品相关满减信息
    getProductFullMarket() {
        if (this.data.productData.marketType !== 'FULL') return
        const marketCodeList = [];
        this.data.productData.skuExtList.forEach(sku => {
            if (sku.marketCode) {
                marketCodeList.push(sku.marketCode);
            }
        });
        ProductApi.fetchProductFullMarket({
            marketCodeList
        })
            .then(res => {
                console.log('商品相关满减信息：', res);
                if (res.flag) {
                    this.setData({
                        fullMarketInfo: res.data
                    });
                }
            })
    },

    // 获取商品第二件活动信息 
    getProductSecondInfo(marketCode) {
        ProductApi.secondMarketByCode({
            marketCode
        })
            .then(res => {
                console.log('商品相关满减信息：', res);
                if (res.flag) {
                    this.setData({
                        secondInfo: res.data
                    });
                }
            })
    },

    // 呼叫商户
    callShop() {
        if (this.data.mallData.industryType === 'SuperMarket') {
            const phone = this.data.productData.mallManagementConfig && this.data.productData.mallManagementConfig.phone
            if (!phone) {
                wx.showToast({
                    title: '该超市没有联系电话',
                    icon: 'none'
                });
            } else {
                wechat.makePhoneCall(phone).then(() => {
                    console.log('拨打成功')
                }).catch(() => {
                    console.log('拨打失败')
                })
            }
        } else {
            if (!this.data.shopData.shopPhone) {
                wx.showToast({
                    title: '该商户没有联系电话',
                    icon: 'none'
                });
            } else {
                wechat.makePhoneCall(this.data.shopData.shopPhone).then(() => {
                    console.log('拨打成功')
                }).catch(() => {
                    console.log('拨打失败')
                })
            }
        }
    },

    // 打开sku选择器
    async openSkuPicker(e) {
        this.setData({
            chooseWay: e.currentTarget.dataset.chooseway,
            groupInfoCode: e.currentTarget.dataset.groupinfocode
        });
        // 判断打开skuPicker是否是从可拼团列表选择后调用打开的
        if (e.detail.chooseway) {
            this.setData({
                chooseWay: e.detail.chooseway,
                groupInfoCode: e.detail.groupinfocode
            });
        }
        // console.log("chooseway",this.data.chooseWay);
        // console.log("groupInfoCode",this.data.groupInfoCode);
        // 老带新拼团活动-判断当前登陆用户是否是新用户
        if (this.data.groupInfoCode && 'OLDNEW' === this.data.groupInfo.groupActivityVO.groupMode) {
            let res = await GroupApi.isGroupMarketNewMember({ marketCode: this.data.productData.marketCode, shopId: this.data.productData.shopId });
            if (!res.data) {
                Tip.toast('您不是新用户，不能参团', null, 'none');
                return;
            }
        }
        if (this.data.organizeType === 'SuperMarket' && this.data.chooseWay == 'addCart') {
            // 判断该商品是否只能自提，只能自提，则不能加入购物车
            let findDispatchTypePickUp = this.data.productData.dispatchTypeList.find(item => item.dispatchType == 'PICK_UP')
            if (findDispatchTypePickUp && this.data.productData.dispatchTypeList.length == 1) {
                // 商品只能自提，提示不能加入购物车
                wx.showModal({
                    title: '提示',
                    content: '当前商品仅支持自提，请直接购买',
                    showCancel: false
                });
                return
            }
        }

        if (wx.getStorageSync('memberType') !== 'MEMBER') {
            wx.navigateTo({
                url: '/pages/memberSign/memberSign'
            });
            return
        }
        if (this.data.productData.productType == 'MARKET') {
            this.selectComponent('#marketSkuPicker').showPicker();
        } else {
            this.selectComponent('#skuPicker').showPicker();
        }

    },
    // 保存选择的sku
    saveSelectedSku(event) {
        // console.log('选择的商品：', event);
        let dispaWay = null
        // if (this.data.organizeType === 'SuperMarket') {
        //     // 判断该商品是否只能自提，只能自提，则不能加入购物车
        //     let findDispatchTypePickUp = this.data.productData.dispatchTypeList.find(item => item.dispatchType == 'PICK_UP')
        //     if (findDispatchTypePickUp && this.data.productData.dispatchTypeList.length == 1) {
        //         // 商品只能自提，
        //         dispaWay = 'PICK_UP'
        //         wx.setStorageSync('isPickUp', true);// 在createMarkOrder页面用于判断该商品是否只能自提
        //     } else {
        //         wx.setStorageSync('isPickUp', false);// 在createMarkOrder页面用于判断该商品是否只能自提
        //     }
        // }
        const selectedSkuData = {
            origin: 'detail',
            shopId: this.data.productData.shopId ? this.data.productData.shopId : event.detail.selectedSku.shopId,
            shopName: this.data.shopData.shopName ? this.data.shopData.shopName : event.detail.selectedSku.shopName,
            shopLogo: this.data.shopData.shopLogo ? this.data.shopData.shopLogo : event.detail.selectedSku.shopLogo,
            shopPhone: this.data.shopData.shopPhone,
            dispatchType: dispaWay != null ? dispaWay : event.detail.dispatchWay,
            skuCount: event.detail.productCount,
            skuInfo: event.detail.selectedSku
        };
        wx.setStorageSync('createOrderSkus', selectedSkuData);
        // // 将团信息传入下个页面
        // let noMarketStock = event.detail.noMarketStock;// 没有活动库存标志位,如果没有，则走正常下单流程
        // // 参团时将团code参数传入下个页面
        // if (this.data.chooseWay === 'joinGroup' && !noMarketStock) {
        //     wx.setStorageSync('createOrderGroupInfoCode', this.data.groupInfoCode);
        // } else {
        //     wx.setStorageSync('createOrderGroupInfoCode', '');
        // }
        // // 将是否是拼团传入下个页面
        // if ((this.data.chooseWay === 'openGroup' || this.data.chooseWay === 'joinGroup') && !noMarketStock) {
        //     // 是否是拼团或者参团
        //     wx.setStorageSync('createOrderIsGroup', 'Y');
        // } else {
        //     wx.setStorageSync('createOrderIsGroup', '');
        // }
        // // 如果该商品是拼团商品时，将分享的对象、拼团活动对象传入下个页面
        // if (this.data.productData.marketType === 'GROUP' && !noMarketStock) {
        //     wx.setStorageSync('createOrderShareData', this.data.shareData);
        //     wx.setStorageSync('createOrderGroupActivity', this.data.groupInfo.groupActivityVO);
        // }
        // if (this.data.productData.marketType === 'PRESALE' && this.data.mallData.industryType === 'SuperMarket') {
        //     // 保存可选的组织ID
        //     wx.setStorageSync('selectedOrganizeIds', this.data.preSaleData.selectedOrganize);
        // }
        if (this.data.chooseWay === 'addCart') {
            // 加入购物车
            this.addToCart(selectedSkuData);
        } else {
            let url = '/pages/newPackage/createOrder/createOrder'
            // // 立即购买，跳往下单页面
            // if (this.data.organizeType === 'SuperMarket') {
            //     // 超市商品
            //     url = '/pages/newPackage/createMarkOrder/createMarkOrder'
            // }
            wx.navigateTo({
                url: url
            })
        }
    },
    // 保存菜场选择的sku
    saveSelectedMakertSku(event) {
        console.log('选择的商品：', event);
    },
    // 将商品加入购物车
    addToCart(selectedSkuData) {
        if (!selectedSkuData) return
        const params = {
            productCode: selectedSkuData.skuInfo.productCode,
            productSkuCode: selectedSkuData.skuInfo.productSkuCode,
            shopId: selectedSkuData.shopId,
            shopName: selectedSkuData.shopName,
            shopLogo: selectedSkuData.shopLogo,
            shopPhone: selectedSkuData.shopPhone,
            skuCount: selectedSkuData.skuCount,
            skuPrice: selectedSkuData.skuInfo.salePrice,
            skuPoints: selectedSkuData.skuInfo.points,
            dispatchType: selectedSkuData.dispatchType.value,
            organizeType: this.data.organizeType,
            organizeName: wx.getStorageSync('organizeName') || ''
        }
        CartApi.fetchAddProductToCart(params).then(res => {
            if (res.data.flag) {
                Tip.toast('添加成功', null, 'none');
                this.getCartCount();
            } else {
                Tip.toast(res.data, null, 'none');
            }
        })
    },
    // 打开优惠券领取
    openCouponPicker() {
        this.selectComponent('#couponPicker').showPicker();
    },
    // 打开送礼明细
    openGiftsPicker() {
        this.selectComponent('#giftPopup').showPicker();
    },
    // 进入购物车页面
    toShoppingCart() {
        if (wx.getStorageSync('memberType') !== 'MEMBER') {
            wx.navigateTo({
                url: `/pages/memberSign/memberSign`
            })
            return false
        }
        wx.switchTab({
            url: '/pages/cart/cart'
        });
    },
    // 获取购物车数量
    getCartCount() {
        if (wx.getStorageSync('memberType') !== 'MEMBER') return
        CartApi.fetchShoppingCartCount({}).then(res => {
            if (res.data.flag) {
                this.setData({
                    shoppingCartCount: res.data.data || 0
                });
            }
        })
    },
    // 第2件活动页面跳转
    gotoSecondList(e) {
        const secondInfo = e.currentTarget.dataset.secondinfo;
        if (secondInfo.secondType == 'DISCOUNT') {
            wx.navigateTo({ url: `/pages/newPackage/secondDiscount/secondDiscount` })
        } else {
            wx.navigateTo({ url: `/pages/newPackage/secondZero/secondZero` })
        }
    },

    // 获取二维码分享过来的团编码
    async getParamGroupInfoCode(scene) {
        let pStr = query.getQueryString('p', scene);
        if (pStr) {
            // 因为分销传参过长，需要通过p调用后台接口取得传参的数据
            let res = await LoginApi.getQrcodeParamById({ id: pStr });
            let param = "";
            if (res.flag) {
                param = res.data;
            }
            let groupInfoCode = query.getQueryString('g', param);
            if (groupInfoCode) {
                this.setData({
                    groupInfoCode: groupInfoCode,
                    g: groupInfoCode,
                    chooseWay: 'joinGroup'
                });
            }
        }
    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad(option) {
        let t = this;
        if (option.scene) {
            let scene = decodeURIComponent(option.scene);
            let id = query.getQueryString('id', scene);
            t.setData({
                productCode: id,
            });
            // 拼团参数获取(通过p获取参数)
            this.getParamGroupInfoCode(scene);
        } else {
            t.setData({
                productCode: option.productCode
            });
            // 获取拼团分享过来的团码
            let groupInfoCode = option.g;
            if (groupInfoCode) {
                this.setData({
                    groupInfoCode: groupInfoCode,
                    g: groupInfoCode,
                    chooseWay: 'joinGroup'
                });
            }
        }
        this.getCartCount();
        this.getProductDetail();
        this.getProductCoupons();
    },

    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady() { },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow() { },

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide() { },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload() { },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh() { },
    /**
     * 页面相关分享
     */
    onShareAppMessage() {
        const organizeId = wx.getStorageSync('organizeId');
        const organizeName = wx.getStorageSync('organizeName');
        const memberId = wx.getStorageSync('memberId');
        const jdShareInfo = wx.getStorageSync('jdShareInfo');
        const userId = jdShareInfo ? jdShareInfo.user_id : false;
        let shareUrl = '';
        if (userId) {
            shareUrl = `/pages/newProductDetail/newProductDetail?productCode=${this.data.productCode}&organizeId=${organizeId}&organizeName=${organizeName}&m=${memberId}&o=${organizeId}&userId=${userId}`
        } else {
            shareUrl = `/pages/newProductDetail/newProductDetail?productCode=${this.data.productCode}&organizeId=${organizeId}&organizeName=${organizeName}&m=${memberId}&o=${organizeId}`
        }
        return {
            path: shareUrl
        }
    }
})