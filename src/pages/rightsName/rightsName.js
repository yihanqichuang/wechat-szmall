// 获取全局应用程序实例对象
// const app = getApp()
import userApi from '../../api/user.js'
import WxParse from '../../wxParse/wxParse.js'
// 创建页面实例对象
Page({
  /**
   * 页面的初始数据
   */
  data: {
    title: 'rightsName',
    privilegeId: '',
    listCardLevel: [],
    list: []
  },
  getData() {
    userApi.listCardLevel().then(res => {
      if (res.data.flag) {
        this.setLevels(res.data.data)
      } else {
        this.messageComponent.applyActive(res.data.msg || '获取数据失败')
      }
    }).catch(err => {
      console.log(err)
    })
  },
  setLevels(data) {
    let list = [];
    data.forEach(card => {
      card.listPrivilege.forEach(privilege => {
        if (privilege.privilegeId == this.data.privilegeId) {
          list.push(card.gradeName);
          let article = privilege.privilegeDesc;
          article && WxParse.wxParse('article', 'html', article, this, 15);
        }
      })
    });
    this.setData({ list })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(option) {
    this.getData();
    this.setData({ privilegeId: option.id });
    this.messageComponent = this.selectComponent('.messageTips')
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {
    // TODO: onReady
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {
    // TODO: onShow
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {
    // TODO: onHide
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {
    // TODO: onUnload
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {
    // TODO: onPullDownRefresh
  }
});
