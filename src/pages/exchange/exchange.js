// 获取全局应用程序实例对象
// const app = getApp()
const exchange = require('../../api/exchange.js');
const QR = require('../../utils/wxqrcode.min.js');
import tip from '../../utils/tip'
import CheckStatus from '../../api/checkState.js';

Page({
  data: {
    info: {},  // 礼品核销信息
    member: {},  // 会员信息
    avatarUrl: '',    // 头像
    imgErweima: '',   // 二维码
    messageComponent: '',
    isShow: true
  },

  // 获取 礼品核销信息
  getInfo(params) {
    exchange.fetchExchangeInfo(params).then(res => {
      if (!res.data.flag) {
        let msg = res.data.msg || '请求数据失败';
        this.messageComponent.applyActive(msg);
        tip.loaded();
        return
      }
      if (res.data.data.verifyCode) {
        res.data.data.verifyCode = `${res.data.data.verifyCode}`;
        this.getErweima(res.data.data.verifyCode)
      } else {
        // this.messageComponent.applyActive('获取核销码失败！')
      }

      let info = res.data.data;
      // if (info.startTime != null) {
      //   info.startTimeStr = info.startTime.substr(0, 16).replace(/-/g, '.');
      // }
      // if (info.endTime != null) {
      //   info.endTimeStr = info.endTime.substr(0, 16).replace(/-/g, '.');
      // }
      this.setData({
        info: info,
        member: res.data.member,
        isShow: false
      });
      tip.loaded();
      // 检查核销状态 (只有未使用才检查)
      // if (info.useState == 'UNUSE') {
      //   CheckStatus.verifyInterVal({ verifyCode: res.data.data.verifyCode }).then((r) => {
      //     if (r) {
      //       wx.setStorageSync('exchangeList', 'exchangeList');
      //       this.getInfo(params)
      //     }
      //   }).catch(err => {
      //     console.log(err)
      //   })
      // }
    }, () => {
      this.messageComponent.applyActive('请求数据失败！');
      tip.loaded()
    })
  },

  // 获取 二维码
  getErweima(code) {
    let imgErweima = QR.createQrCodeImg(code);
    this.setData({ imgErweima })
  },

  // 退礼品
  handleBackGift() {
    tip.confirm(`确认退回？`, '', '提示', '取消', '确认').then(() => {
      CheckStatus.clearVerifyInterValInterval();
      let params = {
        pointsExchangeId: this.data.info.gainId
      };
      exchange.refundGift(params).then(res => {
        if (res.data.flag) {
          tip.success('退礼品成功');
          wx.setStorageSync('exchangeList', 'exchangeList');
          wx.navigateBack({
            delta: 1
          })
        } else {
          this.messageComponent.applyActive(res.data.msg || '退券失败');
        }
      }).catch(() => {
        console.log('退礼品失败')
      })
    }).catch(() => {
      console.log('取消退礼品')
    })
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(option) {
    tip.loading();
    this.messageComponent = this.selectComponent('.messageTips');
    let params = {
      queryType: 'GIFT',
      gainId: option.gainId
    };
    this.setData({ gainId: option.gainId });
    this.getErweima(params);
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {
    let params = {
      queryType: 'GIFT',
      gainId: this.data.gainId
    };
    this.getInfo(params);
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {
    CheckStatus.clearVerifyInterValInterval();
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {
    // 清除定时器
    CheckStatus.clearVerifyInterValInterval();
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {
  }
});
