// 获取全局应用程序实例对象
// const app = getApp()
import userApi from '../../api/user.js'
import { fetchOrganizeList } from '../../api/markets'
import TimeFormat from '../../utils/timeFormat.js';
// 创建页面实例对象
Page({
  /**
   * 页面的初始数据
   */
  data: {
    marketType: '', //节日营销类型
    gradeId: '',// 会员卡id
    gradeName: '',// 会员卡名称
    organizeList: [],// 组织列表
    theOrganizeId: '',// 当前组织id
    theOrganizeName: '',// 当前组织名称
    rightsDetail: null,// 权益详情对象
    marketTime: '',// 权益有效期
  },
  // 获取组织列表
  getOrganizeList() {
    let params = {
    };
    fetchOrganizeList(params).then(res => {
      if (res.data.flag) {
        let organizeList = res.data.data;
        let list = [];
        organizeList.forEach(item => {
          console.log('item', item.organizeList);
          list.push(...item.organizeList);
        });
        if (organizeList && organizeList.length > 0) {
          this.setData({
            organizeList: list
          });
        }
      } else {
        this.messageComponent.applyActive(res.data.msg || '获取列表数据异常');
      }
    }).catch(err => {
      console.log(err)
    })
  },
  onChooseChanges(event) {
    this.setData({
      theOrganizeId: event.detail.organizeId ? event.detail.organizeId : ''
    });
    this.getData();
  },
  // 获取权益详情
  getData() {
    let param = {};
    param.marketType = this.data.marketType;
    param.organizeId = this.data.theOrganizeId;
    param.gradeId = this.data.gradeId;
    // 获取权益详情
    userApi.getLevelPowerByGradeId(param).then(res => {
      if (res.flag) {
        if (res.data) {
          this.setData({
            rightsDetail: res.data
          })
          this.handleMarketTime();
        } else {
          this.setData({
            rightsDetail: null
          })
        }
      }
    })
  },
  // 处理权益有效期显示
  handleMarketTime() {
    if ('FESTIVAL' === this.data.marketType) {
      let startDate = TimeFormat.dateFormatFilter(this.data.rightsDetail.startDate, 'yyyy-MM-dd hh:mm:ss');
      let endDate = TimeFormat.dateFormatFilter(this.data.rightsDetail.endDate, 'yyyy-MM-dd hh:mm:ss');
      this.setData({
        marketTime: startDate + '至' + endDate
      });
    } else if ('MEMBER_DAY' === this.data.marketType) {
      if ('DAY' === this.data.rightsDetail.repeatType) {
        this.setData({
          marketTime: '每天' + this.data.rightsDetail.memberDayStart + '至' + this.data.rightsDetail.memberDayEnd + '日'
        });
      } else if ('WEEK' === this.data.rightsDetail.repeatType) {
        let weekStr = '';
        for (let i = 0; i < this.data.rightsDetail.memberWeekList.length; i++) {
          if ('1' === this.data.rightsDetail.memberWeekList[i] || 1 === this.data.rightsDetail.memberWeekList[i]) {
            weekStr = weekStr + '周一 '
          } else if ('2' === this.data.rightsDetail.memberWeekList[i] || 2 === this.data.rightsDetail.memberWeekList[i]) {
            weekStr = weekStr + '周二 '
          } else if ('3' === this.data.rightsDetail.memberWeekList[i] || 3 === this.data.rightsDetail.memberWeekList[i]) {
            weekStr = weekStr + '周三 '
          } else if ('4' === this.data.rightsDetail.memberWeekList[i] || 4 === this.data.rightsDetail.memberWeekList[i]) {
            weekStr = weekStr + '周四 '
          } else if ('5' === this.data.rightsDetail.memberWeekList[i] || 5 === this.data.rightsDetail.memberWeekList[i]) {
            weekStr = weekStr + '周五 '
          } else if ('6' === this.data.rightsDetail.memberWeekList[i] || 6 === this.data.rightsDetail.memberWeekList[i]) {
            weekStr = weekStr + '周六 '
          } else if ('7' === this.data.rightsDetail.memberWeekList[i] || 7 === this.data.rightsDetail.memberWeekList[i]) {
            weekStr = weekStr + '周日 '
          }
        }
        this.setData({
          marketTime: weekStr
        });
      } else if ('MONTH' === this.data.rightsDetail.repeatType) {
        this.setData({
          marketTime: '每月' + this.data.rightsDetail.memberMonthStart + '日至' + this.data.rightsDetail.memberMonthEnd + '日'
        });
      }
    } else if ('BIRTHDAY' === this.data.marketType) {
      if ('DAY' === this.data.rightsDetail.repeatType) {
        this.setData({
          marketTime: '生日当天'
        });
      } else if ('WEEK' === this.data.rightsDetail.repeatType) {
        this.setData({
          marketTime: '生日当周'
        });
      } else if ('MONTH' === this.data.rightsDetail.repeatType) {
        this.setData({
          marketTime: '生日当月'
        });
      }
    }
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(option) {
    let organizeId = wx.getStorageSync('organizeId');
    let organizeName = wx.getStorageSync('organizeName');
    this.setData({
      gradeId: option.gradeId,
      gradeName: option.gradeName,
      marketType: option.marketType,
      theOrganizeId: organizeId,
      theOrganizeName: organizeName
    })
    this.getOrganizeList();
    this.getData();
    this.messageComponent = this.selectComponent('.messageTips')
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {
    // TODO: onReady
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {
    // TODO: onShow
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {
    // TODO: onHide
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {
    // TODO: onUnload
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {
    // TODO: onPullDownRefresh
  }
});
