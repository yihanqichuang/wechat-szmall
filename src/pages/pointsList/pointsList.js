// 获取全局应用程序实例对象
const app = getApp()
const points = require('../../api/points.js');
import coupon from '../../api/coupon';
import debounce from '../../utils/debounce.js';

// 创建页面实例对象
Page({
  /**
   * 页面的初始数据
   */
  data: {
    title: 'pointsList',
    mainTabs: [
      {
        name: '积分兑券'
      },
      {
        name: '积分兑礼'
      }
    ],
    tabs: [  //
      {
        name: '默认排序',
        type: 'DEFAULT'
      },
      {
        name: '积分最高',
        type: 'DESC'
      },
      {
        name: '积分最低',
        type: 'ASC'
      }
    ],
    activeIndex: 0,
    mainActiveIndex: 0,
    navSlider: {
      width: '40rpx',
      left: '94rpx',
      position: ['94rpx', '360rpx', '600rpx']
    },
    navMainSlider: {
      width: '40rpx',
      left: '150rpx',
      position: ['150rpx', '520rpx']
    },
    list: [],  // 列表数据
    listCoupon: [],
    page: {   // 分页
      pageNum: 1,
      pageSize: 10
    },
    pageCoupon: {
      pageNum: 1,
      pageSize: 10
    },
    emptyType: 'pointsList',
    params: {  // 请求列表的参数
      pointsRewardType: 'GIFT',
      sortType: 'DEFAULT'  // 排序   DEFAULT-默认 ASC-从低到高 DESC-从高到低
    },
    bottomTipsFlag: true,
    messageComponent: '',
    inputValue: ''
  },

  // 积分兑券和积分兑换切换（点击触发）
  changeMainIndex(data) {
    let current = data.detail;
    this.setData({
      mainActiveIndex: current
    });
    if (current === 1 && !this.data.list.length) {
      this.getPointsList({
        pointsRewardType: 'GIFT',
        sortType: this.data.params.sortType,
        pageNum: 1,
        pageSize: 10
      })
    } else if (current === 0 && !this.data.listCoupon.length) {
      this.setData({
        inputValue: ''
      })
      this.getList(1)
    }
  },

  // 积分兑券和积分兑换切换（滑动触发）
  swiperChange(event) {
    let current = event.detail.current;
    this.setData({
      mainActiveIndex: current
    });
    this.sliderMainPosition(current);
    if (current === 1 && !this.data.list.length) {
      this.getPointsList({
        pointsRewardType: 'GIFT',
        sortType: this.data.params.sortType,
        pageNum: 1,
        pageSize: 10
      })
    } else if (current === 0 && !this.data.listCoupon.length) {
      this.setData({
        inputValue: ''
      })
      this.getList(1)
    }
  },

  // 搜索
  search(e) {
    this.setData({
      listCoupon: [],
      pageCoupon: {
        pageNum: 1,
        pageSize: 10
      }
    });
    debounce.debounceDelay(this.getList(this.data.pageCoupon.pageNum, e.detail.value.trim()), 100)
  },

  // 积分兑券和积分兑换切换
  sliderMainPosition(index) {
    this.setData({
      'navMainSlider.left': this.data.navMainSlider.position[index]
    })
  },

  // nav-slider 的位置
  sliderPosition(index) {
    this.setData({
      'navSlider.left': this.data.navSlider.position[index]
    })
  },

  // 改变排序 tab 点击头部
  changeIndex(data) {
    let current = data.detail;
    let type = this.data.tabs[current].type;
    this.setData({
      activeIndex: current,
      'params.sortType': type
    });
    this.setData({
      list: [],  // 列表数据
      page: {   // 分页
        pageNum: 1,
        pageSize: 10
      }
    });
    this.getPointsList({
      pointsRewardType: 'GIFT',
      sortType: type,
      pageNum: 1,
      pageSize: this.data.page.pageSize
    });
    this.sliderPosition(current)
  },

  // 积分兑换排序
  sort(event) {
    let sortType = event.currentTarget.dataset.type;
    if (sortType === this.data.params.sortType) return;
    this.setData({
      'params.sortType': sortType,
      list: [],  // 列表数据
      page: {   // 分页
        pageNum: 1,
        pageSize: 10
      }
    });
    this.getPointsList({
      pointsRewardType: 'GIFT',
      sortType,
      pageNum: 1,
      pageSize: this.data.page.pageSize
    })
  },

  // 获取优惠券列表accessType：POINT 筛选出积分兑换的优惠券
  // getList(pageNum, key = '') {
  getList(pageNum) {
    // let params = {
    //   key,
    //   pageNum,
    //   accessType: 'POINT',
    //   pageSize: 10
    // };
    if (pageNum == 1) {
      this.setData({
        listCoupon: []
      })
    }
    // Object.keys(this.data.query).forEach(item => params[item] = this.data.query[item]);
    // coupon.fetchCouponList(params).then(res => {
    coupon.fetchCouponListNew({
      query: { accessType: 'POINT' },
      page: {
        pageNo: pageNum,
        pageSize: 10
      }
    }).then(res => {
      console.log('newCoupon:', res)
      if (!res.flag) {
        let msg = res.msg || '请求数据失败';
        this.messageComponent.applyActive(msg);
        return
      }
      let data = res.data;
      // let tokenKey = res.data.submit_tokens_name;
      // let tokenValue = res.data[tokenKey];
      // if (data && data.length > 0) {
      //   data.forEach(item => {
      //     item.tokenKey = tokenKey;
      //     item.tokenValue = tokenValue
      //   })
      // }
      this.setData({
        // tokenKey: tokenKey,
        // tokenValue: tokenValue,
        listCoupon: [...this.data.listCoupon, ...data],
        pageCoupon: res.data.page
      })
    })
  },

  // 获取点击的优惠券列表索引
  handleGetIndex(e) {
    let refreshIndex = e.currentTarget.dataset.index;
    this.setData({ refreshIndex })
  },

  // 获取 礼品列表
  getPointsList(params) {
    points.fetchPointsList(params).then(res => {
      if (!res.data.flag) {
        let msg = res.data.msg || '请求数据失败';
        this.messageComponent.applyActive(msg)
      }
      (!res.data.data || res.data.data.length <= 0) && (res.data.data = []);
      this.setData({
        list: [...this.data.list, ...res.data.data],
        page: res.data.page
      })
    })
  },
  // 跳转到 详情
  toPointsDetail(event) {
    let id = event.currentTarget.dataset.id;
    wx.navigateTo({
      url: '../pointsDetail/pointsDetail?id=' + id
    })
  },

  // 加载更多礼品
  loadMore(e) {
    this.getPointsList({
      pointsRewardType: 'GIFT',
      sortType: this.data.params.sortType,
      pageNum: e.detail,
      pageSize: this.data.page.pageSize
    })
  },

  // 加载更多优惠券
  loadMoreCoupon(e) {
    this.getList(e.detail, this.data.inputValue)
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad() {
    this.messageComponent = this.selectComponent('.messageTips');

    // 获取优惠券数据
    this.getList(1);
    app.pubSub.on('refreshCouponItem', (state, balanceNum) => {
      console.log(state)
      console.log(balanceNum)
      let item = this.data.listCoupon[this.data.refreshIndex]
      state && (item.state = state)
      item.balanceNum = balanceNum
      this.setData({
        [`listCoupon[${this.data.refreshIndex}]`]: item
      })
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {
    // TODO: onHide
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {
    app.changeInitDiscover(0)
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {
    // TODO: onHide
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {
    // TODO: onUnload
    app.pubSub.off('refreshCouponItem', null)
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {
    // TODO: onPullDownRefresh
  },

  /**
   * 页面相关分享
   */
  onShareAppMessage() {
    
  }
});
