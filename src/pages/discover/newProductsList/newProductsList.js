// 获取全局应用程序实例对象
// const app = getApp()
import commodity from '../../../api/commodity'
// 创建页面实例对象
Page({
  /**
   * 页面的初始数据
   */
  data: {
    params: {
      commodityType: 'FIRSTLOOK',
      shopId: '',
      pageNum: 1,
      pageSize: 10
    },
    list: [],
    page: {},
    emptyType: 'commodity'
  },
  // 获取 列表数据
  getList(params) {
    commodity.fetchCommodityList(params).then(res => {
      console.log(res)
      if (!res.data.flag) {
        return
      }
      this.setData({
        list: [...this.data.list, ...res.data.data],
        page: res.data.page
      })
    })
  },
  loadMore(data) {
    console.log(111)
    this.setData({
      'params.pageNum': data.detail
    })
    this.getList(this.data.params)
  },
  // 初始化页面
  initPage(shopId = '') {
    this.setData({
      list: [],
      page: {},
      params: {
        commodityType: 'FIRSTLOOK',
        shopId: shopId,
        pageNum: 1,
        pageSize: 10
      }
    })
    this.getList(this.data.params)
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(option) {
    this.initPage(option.shopId)
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {
  
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {
  
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {
    // TODO: onHide
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {
    // TODO: onUnload
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {
    // TODO: onPullDownRefresh
  },

  /**
   * 页面相关分享
   */
  onShareAppMessage() {
    
  }
})
