const app = getApp();
const commodity = require('../../../api/commodity');
let _interval = null;
import WxParse from '../../../wxParse/wxParse.js'
import tip from '../../../utils/tip'
import nBack from '../../../utils/wx_navigateback'
import shareLog from '../../../api/shareLog.js';
import query from '../../../utils/query.js';

// 创建页面实例对象
Page({
  /**
   * 页面的初始数据
   */
  data: {
    commodityId: '',
    info: {},
    state: '',  // 团购状态 un 未开始 ed 已结束 ing 进行中
    btnState: '',  // 按钮状态 sell 已抢光 join 已参团
    startTime: '',  // 团购开始时间
    endTime: '',   // 团购结束时间
    date: {
      d: 0,
      h: '00',
      m: '00',
      s: '00'
    },
    isShow: false,
    messageComponent: '',
    isRender1st: true,
    pending: false
  },

  // 获取详情数据
  getInfo(params) {
    commodity.fetchCommodityDetail(params).then(res => {
      if (!res.data.flag) {
        tip.loaded();
        return
      }
      let localId = wx.getStorageSync('organizeId');
      if (!localId) {
        wx.setStorageSync('organizeId', res.data.data.organizeId);
        wx.setStorageSync('organizeName', res.data.data.organizeName);
      }
      let _this = this;
      let data = res.data.data;
      this.setData({
        info: res.data.data,
        startTime: new Date(res.data.data.startTime.replace(/-/g, '/')).getTime(),
        endTime: new Date(res.data.data.endTime.replace(/-/g, '/')).getTime()
      });
      if (this.data.isRender1st) {
        let article = res.data.data.commodityIntro;
        article && WxParse.wxParse('article', 'html', article, _this, 15);
        this.setData({ isRender1st: false })
      }
      this.setData({
        isShow: true,
        btnState: res.data.data.isJoin === 'Y' ? 'join' : res.data.data.commodityNum <= 0 ? 'sellout' : '',
        [`info.price`]: data.payType == 'ALLMONEY' ? `￥${data.nowPrice}` : data.payType == 'ALLPOINTS' ? `${data.points}积分` : data.payType == 'MIXED' ? `${data.points}积分+${data.nowPrice}元` : '免费',
        [`info.organizeName`]: wx.getStorageSync('organizeName'),
        [`info.logoUrl`]: app.globalData.defaultConfig.defaultPic
      });
      this.decideState();
      tip.loaded()
      if (this.data.state === 'ed') return;
      _interval = setInterval(function() {
        _this.decideState()
      }, 1000)
    })
  },

  decideState() {
    let now = Date.now();
    if (now < this.data.startTime) {  // 未开始
      this.setData({
        state: 'un'
      });
      this.computedTime(this.data.startTime - now)
    } else if (now < this.data.endTime) {
      this.setData({
        state: 'ing'
      });
      this.computedTime(this.data.endTime - now)
    } else {
      this.setData({
        state: 'ed'
      });
      clearInterval(_interval)
    }
  },
  computedTime(time) {
    if (!time || time < 0) {
      this.messageComponent.applyActive('Error when computed time');
      return
    }
    let s = parseInt((time / 1000) % 60)
    let m = parseInt((time / 1000 / 60) % 60)
    let h = parseInt((time / 1000 / 60 / 60) % 24)
    let d = parseInt(time / 1000 / 60 / 60 / 24)
    this.setData({
      date: {
        s: s < 10 ? '0' + s : s,
        m: m < 10 ? '0' + m : m,
        h: h < 10 ? '0' + h : h,
        d: d
      }
    })
  },

  // 提交订单
  submit() {
    let memberType = wx.getStorageSync('memberType');
    if (this.data.pending) return;
    this.setData({ pending: true });
    if (memberType !== 'MEMBER') {
      wx.navigateTo({
        url: `/pages/memberSign/memberSign`
      });
      return
    }
    if (this.data.state !== 'ing' || this.data.btnState || this.data.info.commodityState != 'ONLINE') return;
    commodity.fetchCommodityDetail({
      commodityId: this.data.commodityId
    }).then(res => {
      if (!res.data.flag) {
        this.messageComponent.applyActive(res.data.msg);
        this.setData({ pending: false });
        return
      }
      if (res.data.data.commodityNum <= 0) {
        this.setData({
          btnState: 'sellout'
        });
        this.messageComponent.applyActive('已抢光');
        return
      }
      wx.navigateTo({
        url: '../submitOrder/submitOrder?id=' + this.data.commodityId + '&commodityType=' + this.data.info.commodityType + '&room_id=' + this.data.roomId
      });
      this.setData({ pending: false })
    }).catch(() => {
      this.setData({ pending: false })
    })
  },

  // 分享
  onShareAppMessage() {
    // 转发成功之后记录分享的次数
    shareLog.shareLogCommon(this.data.commodityId, 'SECKILL');
    let organizeId = wx.getStorageSync('organizeId'),  organizeName = wx.getStorageSync('organizeName');
    return {
      path: '/pages/discover/secKillDetail/secKillDetail?id=' + this.data.commodityId + '&organizeId=' + organizeId + '&organizeName=' + organizeName + '&share=true'
    }
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(option) {
    console.log('页面参数：', option);
    if (option.scene) {
      let scene = decodeURIComponent(option.scene);
      let id = query.getQueryString('commodityId', scene);
      this.setData({
        commodityId: id
      });
    } else {
      this.setData({
        commodityId: option.id,
        roomId: option.room_id
      });
    }
    this.messageComponent = this.selectComponent('.messageTips');
  },
  
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {
  
  },
  
  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {
    wx.removeStorageSync('skuObj')
    tip.loading()
    this.getInfo({
      commodityId: this.data.commodityId
    });
    this.setData({
      pending: false
    });
    if (nBack.getNavigateBackData() && nBack.getNavigateBackData().paySuccess) nBack.paySuccess('secKill')
  },
  
  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {
    clearInterval(_interval)
  },
  
  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {
    clearInterval(_interval);
    commodity.fetchCommodityDetail({
      commodityId: this.data.commodityId
    }).then(res => {
      if (!res.data.flag) return false;
      let data = res.data.data;
      app.pubSub.emit('refreshSecKillItem', data.commodityNum, data.saleNum, data.isJoin, data.commodityState)
    })
  },
  
  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {
    // TODO: onPullDownRefresh
  }
});
