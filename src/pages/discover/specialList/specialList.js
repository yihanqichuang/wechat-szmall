// 获取全局应用程序实例对象
// const app = getApp()
import categoryApi from '../../../api/category'
// 创建页面实例对象
Page({
  /**
   * 页面的初始数据
   */
  data: {
    params: {
      commodityType: 'SPECIAL',
      needPage: true,
      pageNum: 1,
      pageSize: 10
    },
    list: [],
    page: {},
    emptyType: 'commodity',
    showTag: true,
    showLeft: false,
    activeIndex: 0,
    // 业态分类
    categoryList: [],
    showSelect: false,
    level: 3,
    tabs: [  //
      {
        name: '默认排序',
        type: ''
      },
      // {
      //   name: '销量',
      //   type: 'SALENUM'
      // },
      {
        name: '价格',
        type: 'PRICE'
      }
    ],
    sortType: '',
    priceType: ''
  },
  // 获取 列表数据
  getList(params, type) {
    let commodityList = [];
    // commodity.fetchCommodityList(params).then(res => {
    categoryApi.fetchGoodsByCategory(params).then(res => {
      if (!res.data.flag) {
        return
      }

      if (type == 'list') {
        commodityList = [...this.data.list, ...res.data.data];
      } else {
        commodityList = [...res.data.data];
      }

      this.setData({
        list: commodityList,
        page: res.data.page
      })
    })
  },
  loadMore(data) {
    this.setData({
      'params.pageNum': data.detail
    })
    this.getList(this.data.params, 'list')
  },
  // 初始化页面
  initPage() {
    this.setData({
      list: [],
      page: {},
      params: {
        commodityType: 'SPECIAL',
        needPage: true,
        pageNum: 1,
        pageSize: 10,
        parentCategoryId: this.data.activeIndex == 0 ? this.data.parentCategoryId : this.data.activeIndex,
        searchKey: this.data.searchKey ? this.data.searchKey : '',
        sortField: this.data.sortType,
        sortType: this.data.priceType
      }
    })
    // 从商铺详情页或发现页面进入
    if (this.data.shopId) {
      this.data.params.shopId = this.data.shopId;
      this.data.params.parentCategoryId = 0;
      let params = this.data.params
      this.setData({ params })
    }
    this.getList(this.data.params, 'list')
  },

  // 初始化业态分类
  initIndustryType() {
    let categoryList = [];
    categoryList.push({
      categoryId: 0,
      parentCategoryId: this.data.parentCategoryId ? this.data.parentCategoryId : 0,
      name: '全部'
    });

    let params = {};
    
    if (this.data.shopId) {
      params = {
        commodityType: 'SPECIAL',
        shopId: this.data.shopId
      };

      categoryApi.fetchCategoryByShop(params).then(res => {
        if (!res.data.flag) return;
        let shopCategory = res.data.data;
        shopCategory.forEach(list => {
          list.parentCategoryId = 0;
          list.name = list.categoryName;
        })
        this.setData({
          categoryList: [...categoryList, ...shopCategory]
        });
      });
    } else {
      params = {
        parentCategoryId: this.data.parentCategoryId ? this.data.parentCategoryId : 0,
        name: '',
        level: this.data.level
      };

      categoryApi.fetchCategoryList(params).then(res => {
        if (!res.data.flag) return;
        this.setData({
          categoryList: [...categoryList, ...res.data.data]
        });
      });
    }
  },

  // 左侧分类导航显示隐藏
  toggleLeft() {
    this.setData({
      showLeft: !this.data.showLeft,
      showTag: !this.data.showTag
    });
  },

  chooseCategory(e) {
    let item = e.currentTarget.dataset.item;
    let params = this.data.params;
    params.pageNum = 1;
    params.pageSize = 10;
    params.needPage = true;
    params.parentCategoryId = item.categoryId == 0 ? item.parentCategoryId : item.categoryId;

    this.getList(this.data.params, 'category');

    this.setData({
      activeIndex: item.categoryId
      // 选择完了之后的操作
      // showLeft: !this.data.showLeft,
      // showTag: !this.data.showTag
    })
  },

  // 顶部选择器展开切换方法
  switchSelect() {
    this.setData({ showSelect: !this.data.showSelect })
  },

  // 顶部搜索栏搜索方法
  handleConfirm(e) {
    let searchKey = e.detail.value;
    this.setData({ searchKey });
    this.initPage();
  },

  // 排序选择
  handleSortType(e) {
    let sortType = e.currentTarget.dataset.type;
    let priceType = this.data.priceType;
    if (sortType == 'PRICE') {
      priceType = priceType == 'ASC' ? 'DESC' : 'ASC'
    } else if (sortType == 'SALENUM') {
      priceType = 'DESC'
    } else {
      priceType = ''
    }
    this.setData({ sortType, priceType })
    this.initPage();
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(option) {
    if (option.categoryName) {
      wx.setNavigationBarTitle({ title: option.categoryName });
    }
    
    this.setData({
      shopId: option.shopId || '',
      parentCategoryId: option.parentCategoryId ? option.parentCategoryId : 0,
      activeIndex: option.categoryId ? option.categoryId : 0,
      level: option.level ? option.level : 3,
      searchKey: option.searchKey
    })
    this.initPage();
    this.initIndustryType();
  }
})
