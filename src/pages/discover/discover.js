const app = getApp();
import commodity from '../../api/commodity.js';
// import timeFormat from '../../utils/timeFormat.js';
// 创建页面实例对象
Page({
  /**
   * 页面的初始数据
   */
  data: {
    bottomTipsFlag: true,
    tabs: [
      {
        name: '潮品汇'
      },
      {
        name: '团购秒杀'
      }
    ],
    activeIndex: 0,
    navSlider: {
      width: '40rpx',
      left: '170rpx',
      position: ['170rpx', '540rpx']
    },
    messageComponent: '',
    // 潮品汇
    newCommodityList: [],
    newCommodityPage: {
      pageNum: 1,
      pageSize: 10,
      total: 1
    },
    // 商品
    commodityTypeList: ['GROUPON', 'SPECIAL', 'FIRSTLOOK', 'SECKILL'],
    commodityLists: {
      GROUPON: [],
      SPECIAL: [],
      FIRSTLOOK: [],
      SECKILL: []
    },
    emptyCommodity: false,
    scrollLeft: 0,
    // 屏幕的宽度
    screenWidth: null
  },

  /*
   * tab切换
   * */
  handleTabSlider(data) {
    this.setTabSliderData(data.detail)
  },
  setTabSliderData(current) {
    this.setData({
      activeIndex: current
    });
  },
  // nav-slider 的位置
  sliderPosition(index) {
    this.setData({
      'navSlider.left': this.data.navSlider.position[index]
    })
  },
  // 改变 swiper 手势滑动
  swiperChange(event) {
    let current = event.detail.current;
    this.setData({
      activeIndex: current
    });
    this.sliderPosition(current);
    if (current === 1) {
      this.goodsChange()
    } else if (current === 0 && !this.data.newCommodityList.length) {
      this.getNewCommodityList()
    }
  },

  // 商品切换
  goodsChange() {
    let promises = this.data.commodityTypeList.map(commodityType => {
      return this.getCommodityData({ commodityType })
    })
    Promise.all([...promises]).then(res => {
      console.log(res)
      let emptyCommodity = true
      res.forEach(item => { if (item) emptyCommodity = false })
      this.setData({ emptyCommodity })
    }).catch(err => console.log(err))
  },

  /*
   * 商品加载
   * */
  goodsLoadMore() {
    this.setData({
      bottomTipsFlag: false
    })
  },
  // 获取潮品汇列表
  getNewCommodityList(pageNum = 1) {
    if (this.data.newListFlag) return
    this.setData({ newListFlag: true })
    if (pageNum === 1) {
      this.setData({
        'newCommodityPage.pageNum': 1,
        newCommodityList: []
      })
    }
    commodity.fetchCommodityList({
      commodityType: 'FIRSTLOOK',
      pageNum,
      pageSize: 10
    }).then(res => {
      this.setData({ newListFlag: false })
      if (!res.data.flag) {
        return this.messageComponent.applyActive(res.data.msg)
      }
      this.setData({
        newCommodityList: [...this.data.newCommodityList, ...res.data.data],
        newCommodityPage: res.data.page
      })
    }).catch(() => {
      this.setData({ newListFlag: false })
    })
  },
  // 潮品汇上拉加载更多
  loadMore(e) {
    this.getNewCommodityList(e.detail)
  },
  // 获取商品信息
  getCommodityData(params) {
    return new Promise((resolve, reject) => {
      commodity.fetchCommodityList(params).then(res => {
        if (!res.data.flag) {
          this.messageComponent.applyActive(res.data.msg)
          resolve(false)
          return
        }
        if (res.data.data.length <= 0) {
          this.setData({
            [`commodityLists.${params.commodityType}`]: []
          })
          resolve(false)
          return
        }
        resolve(true)
        let data = res.data.data.length <= 4 ? res.data.data : res.data.data.slice(0, 4)
        /* if (params.commodityType === 'GROUPON') {
          data.forEach(item => {
            item.cState = item.isJoin === 'Y' ? 'join' : item.commodityNum <= 0 ? 'sellout' : ''
            item.state = this.decideState(item.startTime, item.endTime)
          })
        } */
        this.setData({
          [`commodityLists.${params.commodityType}`]: data
        })
      }, () => reject())
    })
  },
  
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad() {
    this.messageComponent = this.selectComponent('.messageTips');
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {
    // TODO: onReady
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {
    if (app.globalData.initDiscover === 0) {
      this.setTabSliderData(0)
      this.getNewCommodityList()
      app.changeInitDiscover(0)
    } else if (this.data.activeIndex === 1 || app.globalData.initDiscover === 1) {
      this.setTabSliderData(1)
      this.goodsChange()
    }
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {
  
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {
    // TODO: onUnload
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {
    // TODO: onPullDownRefresh
  },

  /**
   * 页面相关分享
   */
  onShareAppMessage() {
    
  }
})
