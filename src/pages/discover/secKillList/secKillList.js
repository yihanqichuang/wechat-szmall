const app = getApp()
import commodity from '../../../api/commodity'

Page({
  data: {
    params: {
      commodityType: 'SECKILL',
      pageNum: 1,
      pageSize: 10
    },
    list: [],
    page: {},
    emptyType: 'commodity',
    refreshIndex: null,
    showTag: true,
    showLeft: false,
    activeIndex: 0,
    // 业态分类
    categoryList: []
  },
  // 获取 列表数据
  getList(params, type) {
    let commodityList = [];
    commodity.fetchCommodityList(params).then(res => {
      if (!res.data.flag) return

      if (type == 'list') {
        commodityList = [...this.data.list, ...res.data.data];
      } else {
        commodityList = [...res.data.data];
      }

      this.setData({
        list: commodityList,
        page: res.data.page
      })
    })
  },
  loadMore(data) {
    this.setData({
      'params.pageNum': data.detail
    })
    this.getList(this.data.params, 'list')
  },
  // 初始化页面
  initPage() {
    this.setData({
      list: [],
      page: {},
      params: {
        commodityType: 'SECKILL',
        pageNum: 1,
        pageSize: 10
      }
    })
    if (this.data.shopId) {
      this.data.params.shopId = this.data.shopId
      let params = this.data.params
      this.setData({
        params
      })
    }
    this.getList(this.data.params, 'list')
  },

  // 初始化业态分类
  initIndustryType() {
    let params = this.data.params;

    commodity.fetchIndustryList(params).then(res => {
      if (!res.data.flag) return;
      this.setData({
        categoryList: res.data.data
      });
    });
  },

  // 左侧分类导航显示隐藏
  toggleLeft() {
    this.setData({
      showLeft: !this.data.showLeft,
      showTag: !this.data.showTag
    });
  },

  chooseCategory(e) {
    let industryId = e.currentTarget.dataset.item.industryId;
    // let item = e.currentTarget.dataset.item;
    let params = this.data.params;
    params.pageNum = 1;
    params.industryId = industryId;

    this.getList(this.data.params, 'industry');

    this.setData({
      activeIndex: industryId,
      // 选择完了之后的操作
      showLeft: !this.data.showLeft,
      showTag: !this.data.showTag
    })
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(option) {
    this.setData({
      shopId: option.shopId || ''
    })
    this.initPage();
    this.initIndustryType();
    app.pubSub.on('refreshSecKillItem', (commodityNum, saleNum, isJoin, commodityState) => {
      let item = this.data.list[this.data.refreshIndex]
      item.commodityNum = commodityNum
      item.saleNum = saleNum
      item.isJoin = isJoin
      item.commodityState = commodityState
      this.setData({
        [`list[${this.data.refreshIndex}]`]: item
      })
    })
  },
  // 点击的索引，从详情后退时刷新
  refreshIndex(e) {
    let refreshIndex = e.currentTarget.dataset.index
    console.log(refreshIndex)
    this.setData({ refreshIndex })
  },
  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {
  
  },
  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {
    // TODO: onHide
  },
  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {
    app.pubSub.off('refreshSecKillItem', null)
  },

  /**
   * 页面相关分享
   */
  onShareAppMessage() {
    
  }
})
