// 获取全局应用程序实例对象
// const app = getApp()
import WxParse from '../../../wxParse/wxParse.js'
import commodity from '../../../api/commodity'
import tip from '../../../utils/tip'
// 创建页面实例对象
Page({
  /**
   * 页面的初始数据
   */
  data: {
    title: 'newProductsList',
    info: {},
    article: '',
    messageComponent: ''
  },
  getInfo(params) {
    commodity.fetchCommodityDetail(params).then(res => {
      if (!res.data.flag) {
        let msg = res.data.msg || '请求数据失败！'
        this.messageComponent.applyActive(msg)
        tip.loaded()
        return
      }
      let localId = wx.getStorageSync('organizeId');
      if (!localId) {
        wx.setStorageSync('organizeId', res.data.data.organizeId);
        wx.setStorageSync('organizeName', res.data.data.organizeName);
      }
      res.data.data.onlineTime = res.data.data.onlineTime.replace(/-/g, '.')
      this.setData({
        info: res.data.data
      })
      let article = res.data.data.commodityIntro
      let _this = this;
      article && WxParse.wxParse('article', 'html', article, _this, 15);
      tip.loaded()
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(option) {
    tip.loading()
    this.getInfo({
      commodityId: option.id
    })
    this.messageComponent = this.selectComponent('.messageTips')
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {
    // TODO: onReady
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {
    wx.removeStorageSync('skuObj')
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {
    // TODO: onHide
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {
    // TODO: onUnload
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {
    // TODO: onPullDownRefresh
  }
})
