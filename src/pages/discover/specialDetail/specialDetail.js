const app = getApp()
const commodity = require('../../../api/commodity');
import shareLog from '../../../api/shareLog.js';
import cart from '../../../api/shoppingcart';
import WxParse from '../../../wxParse/wxParse.js'
import tip from '../../../utils/tip'
import nBack from '../../../utils/wx_navigateback'
import query from '../../../utils/query.js';

// 创建页面实例对象
Page({
  /**
   * 页面的初始数据
   */
  data: {
    commodityId: '',
    info: {},
    state: '',
    isShow: false,
    messageComponent: '',
    isRender1st: true,
    pending: false,
    shoppingCartCount: 0,  // 购物车商品数量
    visible: false,
    actions: [
      {
        name: '到店自提',
        value: 'PICKUP'
      },
      {
        name: '快递配送',
        value: 'DELIVERY'
      }
    ],
    showCenterDialog: false,  // SKU选择插件
    skuStockId: '',
    chooseWay: ''      // 进入入口，判断sku组件由哪种情况触发  BUY-立即购买  CART-加入购物车
  },

  getInfo(params, submit) {
    commodity.fetchCommodityDetail(params).then(res => {
      console.log('商品详情：', res);
      if (!res.flag) {
        tip.loaded();
        this.setData({ pending: false });
        return
      }
      let localId = wx.getStorageSync('organizeId');
      if (!localId) {
        wx.setStorageSync('organizeId', res.data.organizeId);
        wx.setStorageSync('organizeName', res.data.organizeName);
      }
      let picturePath = res.data.data.picturePath;
      res.data.data.picturePathArr = picturePath.split(',')
      let data = res.data.data;
      this.setData({
        info: data
      });

      if (data.isConfigSpecs == 'Y') { // 判断是否有SKU        
        this.setData({
          skuList: data.skuStockExtDOS
        });
      }

      this.setData({
        [`info.organizeName`]: wx.getStorageSync('organizeName'),
        [`info.logoUrl`]: app.globalData.defaultConfig.defaultPic,
        [`info.picturePath`]: data.picturePathArr[0],
        [`info.price`]: data.payType == 'ALLMONEY' ? `￥${data.nowPrice}` : data.payType == 'ALLPOINTS' ? `${data.points}积分` : data.payType == 'MIXED' ? `${data.points}积分+${data.nowPrice}元` : '免费'
      });
      let state = data.commodityNum < 1 ? 'sellout' : data.limitPerDayNum && data.buyNumToday >= data.limitPerDayNum ? 'join' : data.buyLimitNum && data.buyNum >= data.buyLimitNum ? 'join' : 'selling'
      this.setData({ state })
      if (submit && state === 'selling') {
        wx.navigateTo({
          url: '../submitOrder/submitOrder?id=' + this.data.commodityId + '&commodityType=' + this.data.info.commodityType + '&room_id=' + this.data.roomId
        })
        this.setData({ pending: false })
      }
      if (!submit && data.commodityIntro && this.data.isRender1st) {
        let article = data.commodityIntro;
        let _this = this;
        WxParse.wxParse('article', 'html', article, _this, 15);
        this.setData({ isRender1st: false })
      }
      this.setData({ isShow: true })
      tip.loaded()
    }).catch(() => {
      this.setData({ pending: false })
    })
  },

  // 提交
  submit() {
    let memberType = wx.getStorageSync('memberType');
    if (this.data.pending) return;
    this.setData({ 
      pending: true,
      chooseWay: 'BUY'
    });
    if (memberType !== 'MEMBER') {
      wx.navigateTo({
        url: `/pages/memberSign/memberSign`
      });
      return
    }

    if (this.data.state === 'sellout' || this.data.state === 'join' || this.data.info.commodityState != 'ONLINE') return;

    // 如果有商品sku,且没选sku
    if (this.data.info.isConfigSpecs == 'Y' && !this.data.skuObj) {
      this.setData({
        showCenterDialog: true,
        pending: false
      })
      return false
    }

    this.getInfo({ productCode: this.data.commodityId }, true)
  },

  // 查询购物车商品数量
  fetchShoppingCartCount() {
    cart.fetchShoppingCartCount().then(res => {
      let shoppingCartCount = this.data.shoppingCartCount + res.data.data;
      this.setData({ shoppingCartCount: shoppingCartCount })
    })
  },

  // 加入购物车
  addShoppingCart(e) {
    // 每次加购物车前先清空之前选择的规格属性
    this.setData({
      skuObj: null,
      dispatchingWay: ''
    });
    let memberType = wx.getStorageSync('memberType');
    if (memberType !== 'MEMBER') {
      wx.navigateTo({
        url: `/pages/memberSign/memberSign`
      })
      return
    }

    this.setData({ chooseWay: 'CART' });

    if (e.currentTarget.dataset.disabled) return false
    let item = e.currentTarget.dataset.item;
    if (this.data.shoppingCartCount >= 200) {
      return this.messageComponent.applyActive('购物车上限200条商品')
    // } else if (item.commodityNum - item.commodityCount < 1) {
    //   return this.messageComponent.applyActive('宝贝只有这么多啦')
    } else if (item.limitPerDayNum && (item.limitPerDayNum <= item.buyNumToday + item.commodityCount)) {
      return this.messageComponent.applyActive('已达到每人每天限购数量')
    } else if (item.buyLimitNum && (item.buyLimitNum <= item.buyNum + item.commodityCount)) {
      return this.messageComponent.applyActive('已达到每人限购数量')
    }

    // 如果有商品sku,且没选sku或者商品需要配送，显示skuDialog组件
    if ((this.data.info.isConfigSpecs == 'Y' && !this.data.skuObj && this.data.state == 'selling') || (this.data.info.isDelivery == 'Y' && this.data.state == 'selling')) {
      this.setData({
        showCenterDialog: true,
        pending: false
      })
      return false
    }

    this.addShoppingCartForDispatch(this.data.dispatchingWay);

    // 如果支持配送
    // if (this.data.info.isDelivery == 'Y') {
    //   this.setData({
    //     visible: true
    //   })
    // } else {
    //   this.addShoppingCartForDispatch()
    // }
  },

  // 选择配送方式之后
  addShoppingCartForDispatch(dispatchingWay = 'PICKUP') {
    tip.loading()
    cart.addShoppingCart({
      commodityId: this.data.commodityId,
      shopId: this.data.info.shopId,
      commodityCount: 1,
      dispatchingWay: dispatchingWay,
      skuStockId: this.data.skuObj ? this.data.skuObj.skuStockId : ''
    }).then(res => {
      tip.loaded()
      if (!res.data.flag) return this.messageComponent.applyActive(res.data.msg || '加入购物车失败')
      tip.success('加入购物车成功')
      let shoppingCartCount = this.data.shoppingCartCount + 1
      let commodityCount = this.data.info.commodityCount + 1
      let skuCartCount = this.data.skuCartCount + 1
      this.setData({
        shoppingCartCount: shoppingCartCount,
        'info.commodityCount': commodityCount,
        skuCartCount: skuCartCount
      })
    }, () => {
      tip.loaded()
      this.messageComponent.applyActive('加入购物车失败')
    })
  },

  // 关闭选择配送方式
  handleCancel() {
    this.setData({
      visible: false
    });
  },

  // 选择配送方式之后的操作
  handleClickItem({ detail }) {
    const index = detail.index;
    this.setData({
      dispatchingWay: this.data.actions[index].value,
      visible: false
    })
    this.addShoppingCartForDispatch(this.data.actions[index].value)
  },

  // 分享
  onShareAppMessage() {
    // 转发成功之后记录分享的次数
    shareLog.shareLogCommon(this.data.commodityId, 'SPECIAL');
    let organizeId = wx.getStorageSync('organizeId'),  organizeName = wx.getStorageSync('organizeName');
    return {
      path: '/pages/discover/specialDetail/specialDetail?id=' + this.data.commodityId + '&organizeId=' + organizeId + '&organizeName=' + organizeName + '&share=true'
    }
  },

  // 跳转到购物车
  toShoppingCart() {
    // ../shoppingCart/shoppingCart
    let pages = getCurrentPages().map(item => item ? item.route : null)
    let index = pages.indexOf('pages/discover/shoppingCart/shoppingCart')
    if (index === -1) {
      wx.navigateTo({
        url: '/pages/discover/shoppingCart/shoppingCart?room_id=' + this.data.roomId
      })
    } else {
      wx.navigateBack({
        delta: pages.length - 1 - index
      })
    }
  },

  // 选择sku 
  handleChooseSku(e) {
    this.setData({
      skuObj: e.detail,
      dispatchingWay: e.detail.dispatchingWay,
      showCenterDialog: false,
      skuCartCount: this.data.skuCartCount ? this.data.skuCartCount : e.detail.skuCartCount
    })
    wx.setStorageSync('skuObj', JSON.stringify(e.detail));

    if (this.data.chooseWay == 'CART') {
      // 判断加入购物车的商品规格数量是否已超过剩余数量
      if (this.data.skuObj && this.data.skuObj.skuStockNum - this.data.skuCartCount < 1) {
        return this.messageComponent.applyActive('宝贝只有那么多啦！')
      }
      this.addShoppingCartForDispatch(this.data.dispatchingWay);
    } else {
      wx.navigateTo({
        url: '../submitOrder/submitOrder?id=' + this.data.commodityId + '&commodityType=' + this.data.info.commodityType + '&room_id=' + this.data.roomId
      });
    }
  },

  // 关闭sku弹窗
  handleCloseSku() {
    this.setData({
      showCenterDialog: false
    })
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(option) {
    if (option.scene) {
      let scene = decodeURIComponent(option.scene);
      let id = query.getQueryString('commodityId', scene);
      this.setData({
        commodityId: id
      });
    } else {
      this.setData({
        commodityId: option.id,
        roomId: option.room_id
      });
    }
    this.messageComponent = this.selectComponent('.messageTips')
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {},

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {
    this.fetchShoppingCartCount();
    tip.loading()
    wx.removeStorageSync('skuObj');
    this.setData({
      pending: false,
      skuObj: null
    });
    this.getInfo({ productCode: this.data.commodityId }, false)
    if (nBack.getNavigateBackData() && nBack.getNavigateBackData().paySuccess) nBack.paySuccess('special')
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {
    this.setData({
      shoppingCartCount: 0
    })
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {
  }
})
