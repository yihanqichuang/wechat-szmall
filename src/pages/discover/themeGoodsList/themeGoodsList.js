// 获取全局应用程序实例对象
// const app = getApp()
import categoryApi from '../../../api/category'
// 创建页面实例对象
Page({
  /**
   * 页面的初始数据
   */
  data: {
    params: {
      commodityType: 'SPECIAL',
      needPage: true,
      pageNum: 1,
      pageSize: 10
    },
    list: [],
    page: {},
    emptyType: 'commodity',
    showTag: true,
    showLeft: false,
    activeIndex: 0,
    // 业态分类
    categoryList: [],
    showSelect: false,
    navbarInitTop: 0,   // 导航栏初始化距顶部的距离
    isFixedTop: false,  // 是否固定顶部
    themePicUrl: '',    // 主题图片
    activePosition: ''  // 选中定位
  },
  // 获取 列表数据
  getList(params, type) {
    let commodityList = [];
    // commodity.fetchCommodityList(params).then(res => {
    categoryApi.fetchThemeDetail(params).then(res => {
      let data = res.data.data;
      if (!res.data.flag) {
        return
      }

      if (type == 'list') {
        commodityList = [...this.data.list, ...data[0].mallCommodityDOList];
      } else {
        commodityList = [...data[0].mallCommodityDOList];
      }

      let categoryList = [];
      categoryList.push({
        categoryId: 0,
        parentCategoryId: this.data.parentCategoryId ? this.data.parentCategoryId : 0,
        name: '全部'
      });

      // 判断到底是品类还是品牌
      if (data[0].themeType == 'CATEGORY') {
        categoryList = [...categoryList, ...data[0].goodsCategoryForeDOList]
      } else {
        let shopList = data[0].mallShopDOList;
        shopList.forEach(list => {
          list.categoryId = list.shopId;
          list.name = list.shopName;
        })
        categoryList = [...categoryList, ...shopList]
      }

      wx.setNavigationBarTitle({ title: data[0].themeName });

      this.setData({
        categoryList: categoryList,
        list: commodityList,
        themePicUrl: data[0].themePicUrl,
        page: res.data.page
      }, function() {
        this.setData({ activePosition: this.data.shopId ? 'item' + this.data.shopId : '' });
      })
    })
  },
  loadMore(data) {
    this.setData({
      'params.pageNum': data.detail
    })
    this.getList(this.data.params, 'list')
  },
  // 初始化页面
  initPage() {
    this.setData({
      list: [],
      page: {},
      params: {
        commodityType: 'SPECIAL',
        needPage: true,
        pageNum: 1,
        pageSize: 10,
        themeId: this.data.themeId,
        categroyOrShopId: this.data.shopId ? this.data.shopId : 0
      },
      activeIndex: this.data.shopId ? this.data.shopId : 0
    })
    this.getList(this.data.params, 'list')
  },

  chooseCategory(e) {
    let item = e.currentTarget.dataset.item;
    let params = this.data.params;
    params.pageNum = 1;
    params.pageSize = 10;
    params.needPage = true;
    params.categroyOrShopId = item.categoryId == 0 ? item.parentCategoryId : item.categoryId;

    this.getList(this.data.params, 'category');

    this.setData({
      activeIndex: item.categoryId,
      activePosition: 'item' + item.categoryId,
      showSelect: false
    })
  },

  // 顶部选择器展开切换方法
  switchSelect() {
    this.setData({ showSelect: !this.data.showSelect })
  },

  // 滚动事件
  scroll(e) {
    let that = this;
    let scrollTop = parseInt(e.detail.scrollTop); // 滚动条距离顶部高度

    // 判断'滚动条'滚动的距离 和 '元素在初始时'距顶部的距离进行判断
    let isSatisfy = scrollTop >= that.data.navbarInitTop;
    // 为了防止不停的setData, 这儿做了一个等式判断。 只有处于吸顶的临界值才会不相等
    if (that.data.isFixedTop === isSatisfy) {
      return false;
    }

    that.setData({
      isFixedTop: isSatisfy
    });
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(option) {
    this.setData({
      shopId: option.shopId || '',
      themeId: option.themeId
    })
    this.initPage();
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {
    
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {
    let that = this;

    if (that.data.navbarInitTop == 0) {
      // 获取节点距离顶部的距离
      wx.createSelectorQuery().select('#navbar').boundingClientRect(function(rect) {
        if (rect && rect.top > 0) {
          let navbarInitTop = parseInt(rect.top);
          that.setData({
            navbarInitTop: navbarInitTop
          });
        }
      }).exec();
    }
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {
    // TODO: onHide
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {
    // TODO: onUnload
  },

  /**
   * 监听页面滑动事件
   */
  onPageScroll: function(e) {
    console.log(e.scrollTop);
    let that = this;
    let scrollTop = parseInt(e.scrollTop); // 滚动条距离顶部高度

    // 判断'滚动条'滚动的距离 和 '元素在初始时'距顶部的距离进行判断
    let isSatisfy = scrollTop >= that.data.navbarInitTop;
    // 为了防止不停的setData, 这儿做了一个等式判断。 只有处于吸顶的临界值才会不相等
    if (that.data.isFixedTop === isSatisfy) {
      return false;
    }

    that.setData({
      isFixedTop: isSatisfy
    });
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {
    // TODO: onPullDownRefresh
  },

  /**
   * 页面相关分享
   */
  onShareAppMessage() {
    
  }
})
