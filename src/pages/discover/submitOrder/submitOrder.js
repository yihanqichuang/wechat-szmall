// 获取全局应用程序实例对象
const app = getApp();
import commodity from '../../../api/commodity'
import tip from '../../../utils/tip'
import benMath from '../../../utils/BenMath'
import wxPay from '../../../utils/wxPay'
import nBack from '../../../utils/wx_navigateback'
import cart from '../../../api/shoppingcart'
import userApi from '../../../api/user.js'
import memberAddr from '../../../api/addressApi'


// 创建页面实例对象
Page({
  /**
   * 页面的初始数据
   */
  data: {
    source: '',        // commodity 商品   order 订单  cart 购物车
    commodityId: '',
    memberOrderShopId: '',
    orderNo: '',
    info: {},
    count: 0,          // 商品数量
    summary: 0,        // 商品总价
    points: 0,         // 商品总积分
    disableButton: {   // 禁用增减按钮
      plus: true,
      minus: true
    },
    cartOrderData: null,  // 购物车数据 用于页面展示
    cartDatas: null,    // 购物车数据 用于提交信息
    messageComponent: '',
    pending: false,
    showErrorDialog: false, // 处理异常商品的弹窗
    isEnoughScore: true,    // 用户积分是否够提交订单
    errorInformation: null,  // 异常商品的信息
    array: ['到店自提', '快递配送'],
    arraySign: ['PICKUP', 'DELIVERY'],
    wayIndex: -1,
    isSelect: false,
    addressId: '',
    name: '',
    phone: '',
    province: '',
    city: '',
    county: '',
    address: '',
    isDefault: '',
    isSubmit: false,           // 控制已提交订单无法再次选择地址
    showPickDialog: false,     // 控制自提商品弹窗
    pickUpList: null,          // 自提商品信息
    skuStockId: '',            // 商品sku型号ID
    useCouponList: '',         // 可用优惠券数组
    maxUsableCoupon: '',       // 最优可用优惠券
    discountFee: 0,            // 优惠金额，默认为0
    freightFee: 0,             // 快递运费，默认为0
    showCenterDialog: false,   // 优惠券选择插件
    memberCouponId: ''         // 选中的优惠券ID
  },

  // 获取订单信息
  getInfo(params) {
    commodity.fetchOrderView(params).then(res => {
      if (!res.data.flag) {
        this.messageComponent.applyActive(res.data.msg);
        if (res.data.code === 4004) {
          wx.redirectTo({
            url: '../../memberSign/memberSign'
          })
        }
        return
      }
      let data = res.data.data;
      data.canBuyNum = data.buyLimitNum ? data.buyLimitNum - data.buyNum > 0 ? data.buyLimitNum - data.buyNum : 0 : 'noLimit';
      let isSubmit = data.isDelivery == 'N'
      if (this.data.source === 'commodity') {
        let wayIndex = -1;
        // 临时改动，秒杀和团购上SKU功能时需要调整
        if (!this.data.dispatchingWay) {
          wayIndex = data.isDelivery == 'N' ? 0 : -1
        } else {
          wayIndex = this.data.dispatchingWay == 'PICKUP' ? 0 : 1
        }

        if (wayIndex == 1) { // 获取默认送货地址
          this.getAddressList(wayIndex);
        }
        this.setData({
          info: data,
          isEnoughScore: benMath.accSub(data.memberBalanceScore, data.points) >= 0,
          count: data.commodityNum > 0 ? 1 : 0,
          'disableButton.plus': data.commodityNum < 2 || data.canBuyNum < 2,
          isSubmit: isSubmit,
          wayIndex: wayIndex
        })

        if (this.data.skuPrice || this.data.skuPrice === 0) { // 当前选中sku价格
          this.setData({ [`info.nowPrice`]: this.data.skuPrice })
        }
        
        if (this.data.skuPoints || this.data.skuPoints === 0) { // 当前选中sku积分
          this.setData({ [`info.points`]: this.data.skuPoints })
        }

        if (this.data.skuStockNum || this.data.skuStockNum === 0) { // 当前选中sku剩余数量
          this.setData({ [`info.commodityNum`]: this.data.skuStockNum })
        }

        if (this.data.skuPayType) { // 当前选中sku的支付类型
          this.setData({ [`info.payType`]: this.data.skuPayType })
        }
      } else if (this.data.source === 'order') {
        let order = data.orderDtlList[0];
        let obj = {
          shopLogo: data.shopLogo,
          shopName: data.relationName,
          picturePath: order.detailPicturepath,
          commodityName: order.detailName,
          nowPrice: order.detailPrice,
          oldPrice: order.originalPrice,
          getTimeLimit: order.getTimeLimit,
          commodityType: data.orderType,
          payType: order.payType,
          points: order.detailPoints,
          memberBalanceScore: data.memberBalanceScore,
          skuValue: order.orderSpecs
        };
        let wayIndex = data.dispatchingWay == 'PICKUP' ? 0 : 1;
        this.setData({
          info: obj,
          orderNo: data.orderShopNumber,
          count: order.detailAccount,
          disableButton: {
            plus: true,
            minus: true
          },
          summary: data.price,
          points: data.points,
          isEnoughScore: benMath.accSub(data.memberBalanceScore, data.points) >= 0,
          wayIndex: wayIndex,
          name: data.harvestName,
          phone: data.harvestPhone,
          province: data.harvestProvince,
          city: data.harvestCity,
          county: data.harvestCounty,
          address: data.harvestAddress,
          isSelect: true,
          isSubmit: true,
          discountFee: data.discountAmount ? data.discountAmount : 0,
          freightFee: data.freight ? data.freight : 0
        })
      }
      this.sum()
    })
  },

  // 提交订单信息
  submit1(e) {
    // 订单已生成
    if (this.data.orderNo) return this.data.summary !== 0 ? this.wxPay(this.data.orderNo, this.data.summary, 'COMMODITY') : false;
    this.setData({ pending: true });
    commodity.fetchSubmitOrder({
      commodityId: this.data.commodityId,
      commodityType: this.data.info.commodityType,
      count: this.data.count,
      useAmount: 0,
      payment: this.data.summary,
      formid: e.detail.formId
    }).then(res => {
      if (!res.data.flag) {
        let msg = res.data.msg || '请求失败！';
        this.setData({ pending: false });
        return this.messageComponent.applyActive(msg)
      }
      this.setData({
        orderNo: res.data.data.orderNumber
      });
      if (this.data.summary !== 0) {
        this.wxPay(this.data.orderNo, this.data.summary, 'COMMODITY')
      } else {
        tip.success('支付成功', 1000).then(() => {
          wx.setStorageSync('myOrderList', 'myOrderList');
          nBack.setNavigateBackData({ paySuccess: true }, 1)
        })
      }
      this.setData({ pending: false })
    }).catch(() => {
      this.setData({ pending: false })
    })
  },

  // 提交订单
  submit(e) {
    let index = this.data.wayIndex;
    // 判断当前配送方式, 0-到店自提, 1-快递配送
    if (index == -1) {
      this.messageComponent.applyActive('请选择配送方式！')
      return
    }

    if (index == 1 && !this.data.isSelect) {
      this.messageComponent.applyActive('请选择收货地址！')
      return
    }

    let payType = this.data.info.payType;
    let formId = e.detail.formId;

    // 若已生产过订单
    if (this.data.orderNo) {
      if (this.data.summary !== 0) {
        this.wxPay(this.data.orderNo, this.data.summary, 'COMMODITY')
      } else {
        tip.success('支付成功', 1000).then(() => {
          wx.setStorageSync('myOrderList', 'myOrderList');
          nBack.setNavigateBackData({ paySuccess: true }, 1)
        })
      }
      return false;
    }

    // 有积分的需要提示所有积分
    if (payType != 'ALLMONEY') {
      if (this.data.points && this.data.points > 0) {
        tip.confirm(`您正在使用${this.data.points}积分购买商品，请确认是否立即支付？`, {}, '温馨提示', '再想想', '立即支付').then(() => {
          this.submitOrder(formId).then(() => {
            if (payType == 'ALLPOINTS') {
              tip.success('支付成功', 1000).then(() => {
                wx.setStorageSync('myOrderList', 'myOrderList');
                nBack.setNavigateBackData({ paySuccess: true }, 1)
              })
            } else {
              if (this.data.summary !== 0) {
                this.wxPay(this.data.orderNo, this.data.summary, 'COMMODITY')
              } else {
                tip.success('支付成功', 1000).then(() => {
                  wx.setStorageSync('myOrderList', 'myOrderList');
                  nBack.setNavigateBackData({ paySuccess: true }, 1)
                })
              }
            }
          })
        })
      } else {
        this.submitOrder(formId).then(() => {
          if (payType == 'ALLPOINTS') {
            tip.success('支付成功', 1000).then(() => {
              wx.setStorageSync('myOrderList', 'myOrderList');
              nBack.setNavigateBackData({ paySuccess: true }, 1)
            })
          } else {
            if (this.data.summary !== 0) {
              this.wxPay(this.data.orderNo, this.data.summary, 'COMMODITY')
            } else {
              tip.success('支付成功', 1000).then(() => {
                wx.setStorageSync('myOrderList', 'myOrderList');
                nBack.setNavigateBackData({ paySuccess: true }, 1)
              })
            }
          }
        })
      }
    } else {
      this.submitOrder(formId).then(() => {
        if (this.data.summary !== 0) {
          this.wxPay(this.data.orderNo, this.data.summary, 'COMMODITY')
        } else {
          tip.success('支付成功', 1000).then(() => {
            wx.setStorageSync('myOrderList', 'myOrderList');
            nBack.setNavigateBackData({ paySuccess: true }, 1)
          })
        }
      })
    }
  },

  // 下单接口
  submitOrder(formId) {
    this.setData({ pending: true });
    return new Promise((resolve, reject) => {
      let params = {}
      let productInfo = {
        productId: this.data.commodityId,
        skuStockId: this.data.skuStockId ? this.data.skuStockId : null,
        buyCount: this.data.count ? this.data.count : 1 
      };
      let index = this.data.wayIndex; 
      let orderParams = {
        formid: formId,
        products: [productInfo],
        dispatchingWay: this.data.arraySign[index],
        memberCouponId: this.data.memberCouponId ? this.data.memberCouponId : null
      };
      // 判断当前配送方式, 0-到店自提, 1-快递配送
      if (index == 1 && this.data.isSelect) {
        orderParams.addressId = this.data.addressId
      }
      // 将参数转为json
      params.param = JSON.stringify(orderParams);
      commodity.fetchSubmitOrder(params).then(res => {
        if (!res.data.flag) {
          let msg = res.data.msg || '请求失败！';
          this.setData({ pending: false });
          reject();
          return this.messageComponent.applyActive(msg);
        }
        this.setData({
          orderNo: res.data.data.orderNumber
        });
        resolve();
        this.setData({ pending: false })
      }).catch(() => {
        this.setData({ pending: false });
        reject();
      })
    })
  },
  
  // 微信支付
  wxPay() {
    wxPay.getWxConfig(...arguments).then(() => {}).catch(err => {
      if (err === '取消支付') {
        tip.confirm('您的订单在15分钟内未支付将被取消，请尽快完成支付', {}, '确认取消支付', '继续支付', '确认离开').then(() => {
          wx.redirectTo({
            url: '/pages/orderList/orderList?isFirst=true'
          })
        }).catch(() => {
          this.wxPay(this.data.orderNo, this.data.summary, 'COMMODITY')
        })
      } else {
        this.messageComponent.applyActive(err)
      }
    })
  },

  // 增加或减少数量
  changeNum(event) {
    if (this.data.orderNo) return console.log('订单已生成' + this.data.orderNo);
    let n = Number(event.currentTarget.dataset.var);
    let count = this.data.count + n;
    if (count > this.data.info.commodityNum) {
      // 大于剩余数量
      return this.messageComponent.applyActive('宝贝只有这么多啦')
    } else if (this.data.info.limitPerDayNum && count > this.data.info.limitPerDayNum - this.data.info.buyNumToday) {
      // 大于限制数量
      return this.messageComponent.applyActive('每人每天限购' + this.data.info.limitPerDayNum + '件')
    } else if (this.data.info.buyLimitNum && count > this.data.info.buyLimitNum - this.data.info.buyNum) {
      // 大于限制数量
      return this.messageComponent.applyActive('每人限购' + this.data.info.buyLimitNum + '件')
    } else if (count < 1) {
      // 减少 至少为 1
      return this.messageComponent.applyActive('购买数量需大于0')
    } else {
      this.setData({ count });
      // 重新获取可用券列表并计算总金额
      this.getUseCouponList().then(() => this.sum());
    }
  },

  // 计算总金额
  sum() {
    // 优惠金额-默认为0
    let discountFee = this.data.discountFee;
    // 需要的金额
    let summary = benMath.accMul(this.data.count, this.data.info.nowPrice);
    // 需要的积分
    let points = benMath.accMul(this.data.count, this.data.info.points);
    // 计算运费逻辑
    // if (this.data.freightFee) {
    //   summary += this.data.freightFee;
    // }
    // 计算优惠后的总金额
    summary = benMath.accSub(summary, discountFee) * 1;
    // summary = summary.toFixed(2) > 0.00 ? summary.toFixed(2) : 0;
    this.setData({ summary, points })
  },

  // 购物车结算
  settlement() {
    // 是否勾选了地址
    let index = this.data.wayIndex;
    // 判断当前配送方式, 0-到店自提, 1-快递配送
    if (index == 1 && !this.data.isSelect) {
      return this.messageComponent.applyActive('请选择收货地址！')
    }

    // 是否有自提的商品
    let cartOrderData = this.data.cartOrderData;
    let pickUpList = [];
    cartOrderData.forEach(item1 => {
      item1.orderDtls.forEach(item2 => {
        if (item2.dispatchingWay == 'PICKUP') {
          pickUpList.push(item2)
        }
      })
    })

    // 有自提商品并且勾选了地址
    if (pickUpList && pickUpList.length > 0 && index == 1) {
      this.setData({
        pickUpList,
        showPickDialog: true
      })
    } else {
      this.settlementConfirm()
    }
  },

  closePickDialog() {
    this.setData({
      showPickDialog: false
    })
  },

  handleConfirmSettlement() {
    this.setData({
      showPickDialog: false
    })
    this.settlementConfirm();
  },

  settlementConfirm() {
    let payType = this.data.payType;
    // 有积分支付
    if (payType != 'ALLMONEY') {
      if (this.data.points && this.data.points > 0) {
        tip.confirm(`您正在使用${this.data.points}积分购买商品，请确认是否立即支付？`, {}, '温馨提示', '再想想', '立即支付').then(() => {
          this.settlementOrder().then(() => {
            if (payType == 'ALLPOINTS') {
              tip.success('支付成功', 1000).then(() => {
                this.backShoppingCart();
              })
            } else {
              if (this.data.summary !== 0) {
                this.wxPay(this.data.orderNo, this.data.summary, 'COMMODITY')
              } else {
                tip.success('支付成功', 1000).then(() => {
                  this.backShoppingCart();
                })
              }
            }
          })
        }).catch(() => {
          this.setData({ pending: false })
        })
      } else {
        this.settlementOrder().then(() => {
          if (payType == 'ALLPOINTS') {
            tip.success('支付成功', 1000).then(() => {
              this.backShoppingCart();
            })
          } else {
            if (this.data.summary !== 0) {
              this.wxPay(this.data.orderNo, this.data.summary, 'COMMODITY')
            } else {
              tip.success('支付成功', 1000).then(() => {
                this.backShoppingCart();
              })
            }
          }
        })
      }  
    } else {
      this.settlementOrder().then(() => {
        if (this.data.summary !== 0) {
          this.wxPay(this.data.orderNo, this.data.summary, 'COMMODITY')
        } else {
          tip.success('支付成功', 1000).then(() => {
            this.backShoppingCart();
          })
        }
      })
    }
  },

  // 购物车订单结算API
  settlementOrder() {
    this.setData({ pending: true });
    return new Promise((resolve, reject) => {
      let cartDatas = this.data.cartDatas;
      let shopCartIds = [];
      cartDatas.forEach(item1 => {
        item1.orderDtls.forEach(item2 => {
          item2.addressId = this.data.addressId;
          shopCartIds.push(item2.memberShoppingchartId);
        })
      })

      let param = {};
      param.shopCars = shopCartIds;
      param.addressId = this.data.addressId;
      param.memberCouponId = this.data.memberCouponId ? this.data.memberCouponId : null;
      
      commodity.fetchSubmitOrder({ param: JSON.stringify(param) }).then(res => {
        this.setData({ pending: false });
        if (!res.data.flag) {
          reject();
          return !res.data.data || !res.data.data.length
            ? this.messageComponent.applyActive(res.data.msg || '请求失败！')
            : this.handleSettlementError(res.data.data)
        } else {
          this.setData({
            orderNo: res.data.data.orderNumber
          });
          resolve();
        }
      }).catch(() => {
        reject();
        this.setData({ pending: false })
      })
    })
  },

  // 处理购物车商品数据
  settlementData(cartOrderData) {
    let count = 0, summary = 0, points = 0, payType = '', dispatchingWay = '';
    let cartDatas = [];
    cartOrderData.forEach(item1 => {
      if (item1.isDelete) return;
      item1.count = 0;
      item1.summary = 0;
      item1.points = 0;
      item1.payType = item1.orderDtls.every(item => item.payType == 'ALLMONEY') ? 'ALLMONEY' : item1.orderDtls.every(item => item.payType == 'ALLPOINTS') ? 'ALLPOINTS' : 'MIXED';
      item1.dispatchingWay = item1.orderDtls.some(item => item.dispatchingWay == 'DELIVERY') ? 'DELIVERY' : 'PICKUP'
      let orderDtls = [];
      item1.orderDtls.forEach(item2 => {
        if (item2.isDelete) return;
        item1.count += item2.count;
        item1.summary = benMath.accAdd(item1.summary, benMath.accMul(item2.count, item2.nowPrice));
        item1.points = benMath.accAdd(item1.points, benMath.accMul(item2.count, item2.points));
        orderDtls.push({
          goodsId: item2.goodsId,
          count: item2.count,
          memberShoppingchartId: item2.memberShoppingchartId,
          dispatchingWay: item2.dispatchingWay,
          skuSpecs: item2.skuSpecs
        })
      });
      count += item1.count;
      summary = benMath.accAdd(summary, item1.summary);
      points = benMath.accAdd(points, item1.points);
      cartDatas.push({
        shopId: item1.shopId,
        shopName: item1.shopName,
        orderDtls: orderDtls
      })
    });
    summary = benMath.accSub(summary, this.data.discountFee) * 1
    payType = cartOrderData.every(item => item.payType == 'ALLMONEY') ? 'ALLMONEY' : cartOrderData.every(item => item.payType == 'ALLPOINTS') ? 'ALLPOINTS' : 'MIXED';
    dispatchingWay = cartOrderData.some(item => item.dispatchingWay == 'DELIVERY') ? 'DELIVERY' : 'PICKUP'
    if (points) {
      this.getQueryMember().then(() => {
        this.setData({
          isEnoughScore: benMath.accSub(this.data.memberBalanceScore, points) >= 0
        })
      })
    }
    this.setData({
      cartOrderData,
      cartDatas,
      count,
      summary,
      points,
      payType,
      dispatchingWay,
      wayIndex: dispatchingWay == 'DELIVERY' ? 1 : 0
    })

    // 如果购物车中有一个配送的，需要拉取默认地址
    this.getAddressList(this.data.wayIndex)
  },

  // 异常状态商品
  handleSettlementError(data) {
    let errorInformation = {
      lists: {  // 各状态的异常商品
        OFFLINE: {
          errorText: '已下架',
          list: []
        },
        LOW_STOCKS: {
          errorText: '库存不足',
          list: []
        },
        EXCEED_QUANTITY: {
          errorText: '超出限购数量',
          list: []
        }
      },
      deleteIds: [],  // 需要删除的id
      changeItems: [] // 需要更改数量的
    };
    data.forEach(item => {
      if (item.availableNum) {
        errorInformation.changeItems.push({
          memberShoppingchartId: item.memberShoppingchartId,
          availableNum: item.availableNum
        })
      } else {
        errorInformation.deleteIds.push(item.memberShoppingchartId)
      }
      for (let key in errorInformation.lists) {
        if (item.errorType === key) {
          errorInformation.lists[key].list.push(item);
          break
        }
      }
    });
    this.setData({
      errorInformation,
      showErrorDialog: true
    })
  },

  // 关闭弹窗
  closeErrorDialog() {
    this.setData({
      showErrorDialog: false
    })
  },

  // 返回购物车
  backShoppingCart() {
    wx.navigateBack({
      delta: 1
    })
  },

  // 移除异常商品
  deletes() {
    tip.loading('移除中...');
    let cartOrderData = this.data.cartOrderData;
    let { deleteIds, changeItems } = this.data.errorInformation;
    cartOrderData.forEach((item1) => {
      item1.orderDtls.forEach((item2) => {
        if (deleteIds.indexOf(item2.memberShoppingchartId) !== -1) {
          cart.fetchDelete({ dataIds: item2.memberShoppingchartId })
          item2.isDelete = true
        } else {
          changeItems.forEach(item => {
            if (item.memberShoppingchartId === item2.memberShoppingchartId) {
              item2.count = item.availableNum
            }
          })
        }
      });
      if (item1.orderDtls.every(item => item.isDelete)) item1.isDelete = true
    });
    this.settlementData(cartOrderData);
    wx.nextTick(() => {
      tip.loaded();
      this.closeErrorDialog()
    })
  },
  // 后退
  goBack() {
    wx.navigateBack({
      delta: 1
    })
  },

  // getQueryMember 获取会员信息
  getQueryMember() {
    return new Promise((resolve, reject) => {
      userApi.queryMember().then(res => {
        if (res.data.flag) {
          this.setData({
            memberBalanceScore: res.data.data.points
          });
          resolve();
        } else {
          reject();
        }
      }).catch(() => {
        reject();
      });
    })
  },

  // 配送方式选择器
  bindPickerChange(e) {
    if (e.detail.value == 0) {
      this.setData({
        wayIndex: e.detail.value,
        freightFee: 0
      })
      // this.sum();
      return
    }
    this.getAddressList(e.detail.value);
    // this.sum();
  },

  // 获取配送地址
  getAddressList(wayIndex) {
    memberAddr.fetchAddressList({ isDefault: 'Y' }).then(res => {
      if (!res.data.flag) {
        let msg = res.data.msg || '请求数据失败';
        this.messageComponent.applyActive(msg);
        return
      }
      let data = res.data.data
      // 判断是否存在默认地址
      if (data == null) {
        this.setData({
          wayIndex: wayIndex
        })
        return
      }
      // 根据选择地址查询运费
      // this.getFreightInfo(data[0].province);

      this.setData({
        addressId: data[0].id,
        name: data[0].name,
        phone: data[0].phone,
        province: data[0].province,
        city: data[0].city,
        county: data[0].county,
        address: data[0].address,
        isDefault: data[0].isDefault,
        isSelect: true,
        wayIndex: wayIndex
      })
    })
  },

  // 跳转至地址选择界面
  toSelectAddress() {
    let isSubmit = this.data.isSubmit
    if (isSubmit) {
      return
    }

    let params = {
      canEdit: 0
    }

    // 有的手机报错不兼容Object.entries
    if (!Object.entries) {
      Object.entries = function(obj) {
        let ownProps = Object.keys(obj),
          i = ownProps.length,
          resArray = new Array(i); // preallocate the Array
        while (i--) {
          resArray[i] = [ownProps[i], obj[ownProps[i]]];
        }
        return resArray;
      }
    }

    wx.navigateTo({
      url: '/pages/memberAddress/memberAddress?' + Object.entries(params).map(item => `${item[0]}=${item[1]}`).join('&')
    })
  },

  // 获取可用优惠券列表
  getUseCouponList(cartOrderData) {
    return new Promise((resolve, reject) => {
      let source = this.data.source;
      let skuObj = wx.getStorageSync('skuObj');
      let params = {};

      if (source == 'commodity') { // 商品单个提交
        let productInfo = {
          productId: this.data.commodityId,
          skuStockId: skuObj ? JSON.parse(skuObj).skuStockId : null,
          buyCount: this.data.count ? this.data.count : 1 
        };
        let productParam = {
          products: [productInfo]
        };
        params.param = JSON.stringify(productParam);
      } else { // 购物车提交
        let cartDatas = cartOrderData;
        let shopCartIds = [];
        cartDatas.forEach(item1 => {
          item1.orderDtls.forEach(item2 => {
            item2.addressId = this.data.addressId;
            shopCartIds.push(item2.memberShoppingchartId);
          })
        })
        let shopCar = {
          shopCars: shopCartIds 
        };
        params.param = JSON.stringify(shopCar);
      }
      commodity.fetchUseCouponList(params).then(res => {
        if (!res.data.flag) {
          reject();
          let msg = res.data.msg || '请求数据失败';
          this.messageComponent.applyActive(msg);
          return
        }
        let data = res.data.data
        this.setData({
          discountFee: data.maxUsableCoupon ? data.maxUsableCoupon.discountsAmt : 0,
          memberCouponId: data.maxUsableCoupon ? data.maxUsableCoupon.memberCouponId : 0,
          maxUsableCoupon: data.maxUsableCoupon,
          useCouponList: data.usableCoupons
        })
        resolve();
      }).catch(() => {
        reject();
      })
    })
  },

  // 根据省份获取运费信息
  getFreightInfo(province) {
    commodity.fetchFreightInfo({ province: province }).then(res => {
      if (!res.data.flag) {
        let msg = res.data.msg || '请求数据失败';
        this.messageComponent.applyActive(msg);
        return
      }
      this.setData({
        freightFee: res.data.data ? res.data.data : 0
      })
      this.sum();
    });
  },

  // 选择优惠券弹窗
  showCouponDialog() {
    let useCouponList = this.data.useCouponList;
    if (!useCouponList || useCouponList.length == 0) {
      return
    }
    this.setData({
      showCenterDialog: true
    })
  },

  // 选择优惠券提交方法
  handleChooseCoupon(e) {
    this.setData({
      memberCouponId: e.detail.memberCouponId,
      discountFee: e.detail.discountsAmt >= 0 ? e.detail.discountsAmt : this.data.discountFee,
      couponTypeKind: e.detail.couponTypeKind,
      couponValue: e.detail.couponValue,
      showCenterDialog: false
    })
    if (this.data.source == 'commodity') {
      this.sum();
    } else {
      this.settlementData(app.globalData.cartOrderData);
    }
  },

  // 关闭优惠券弹窗
  handleCloseCoupon() {
    this.setData({
      showCenterDialog: false
    })
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(option) {
    if (option.id) {
      this.setData({
        source: 'commodity',
        commodityId: option.id,
        commodityType: option.commodityType
      });
      // 判断当前商品是否为单品
      if (option.commodityType && option.commodityType == 'SPECIAL') {
        // 获取可用券列表
        this.getUseCouponList().then(() => this.getInfo({ commodityId: option.id }, this.data.source));
      } else {
        this.getInfo({ commodityId: option.id }, this.data.source)
      }
    } else if (option.memberOrderShopId) {
      this.setData({
        source: 'order',
        memberOrderShopId: option.memberOrderShopId
      });
      this.getInfo({ memberOrderShopId: option.memberOrderShopId }, this.data.source)
    } else if (option.source === 'cart') {
      this.setData({
        source: 'cart'
      });
      // 获取可用券列表
      let cartOrderData = app.globalData.cartOrderData;
      this.getUseCouponList(cartOrderData).then(() => this.settlementData(cartOrderData));
    }
    this.messageComponent = this.selectComponent('.messageTips')
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {
    let skuObj = wx.getStorageSync('skuObj');
    if (skuObj) {
      let skuValue = JSON.parse(skuObj).skuValu;
      let skuId = JSON.parse(skuObj).skuId;
      let skuStockId = JSON.parse(skuObj).skuStockId;
      let dispatchingWay = JSON.parse(skuObj).dispatchingWay;
      let skuPrice = JSON.parse(skuObj).skuPrice;
      let skuPoints = JSON.parse(skuObj).skuPoints;
      let skuStockNum = JSON.parse(skuObj).skuStockNum;
      let skuPayType = JSON.parse(skuObj).payType;
      let skuImageUrl = JSON.parse(skuObj).skuImageUrl;
      this.setData({
        skuValue,
        skuId,
        skuStockId,
        dispatchingWay,
        skuPrice,
        skuPoints,
        skuStockNum,
        skuPayType,
        skuImageUrl
      })
    }

    // 进入页面重新获取一下运费
    // if (this.data.source == 'commodity') {
    //   this.getFreightInfo(this.data.province);
    //   this.sum();
    // }
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {
  
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {
    tip.loaded()
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {
    // TODO: onPullDownRefresh
  }
})
