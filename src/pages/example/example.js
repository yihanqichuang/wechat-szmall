// /* eslint-disable */
// // 获取全局应用程序实例对象
// // const app = getApp()
// const livePlayer = requirePlugin('live-player-plugin')
// import liveMedia from '../../api/liveMedia.js'

// Page({
//   data: {
//     list: [],
//     liveList: [],
//     activeIndex: 'ss',
//     page: {
//       pageNum: 1,
//       pageSize: 10,
//       total: 1
//     }
//   },

//   // 首次获取立马返回直播状态，往后间隔1分钟或更慢的频率去轮询获取直播状态
//   getLiveStatusById(item) {
//     let liveStatusObj = {};
//     let liveStatus = ''
//     livePlayer.getLiveStatus({ room_id: item.roomid }).then(res => {
//       // 101: 直播中, 102: 未开始, 103: 已结束, 104: 禁播, 105: 暂停中, 106: 异常，107：已过期
//       liveStatus = res.liveStatus
//       console.log('get live status', liveStatus)
//     }).catch(err => {
//       console.log('get live status', err)
//     })
//     item.status = liveStatus;
//     liveStatusObj['room_' + item.roomid] = item;
//     this.setData(liveStatusObj);
//   },

//   // 获取直播列表
//   getLiveList(pageNum) {
//     let params = {
//       liveSatus: this.data.activeIndex,
//       isNeedPage: true,
//       pageNum: pageNum,
//       pageSize: this.data.page.pageSize
//     };
//     liveMedia.fetchLiveList(params).then(res => {
//       if (res.data.flag) {
//         if (pageNum === 1) this.setData({ liveList: [] });
//         let statusStr = '';
//         let liveList = [...this.data.liveList, ...res.data.data];
//         liveList.forEach(item => { // 遍历一下列表中每个直播的状态
//           let goodsArr = this.switchJsonObj(item.goods);
//           item.goods = goodsArr;
//           let curTime = new Date().getTime();
//           let startTime = new Date(item.startTime.replace(/-/g, '/')).getTime();
//           let endTime = new Date(item.endTime.replace(/-/g, '/')).getTime();
//           if (curTime >= startTime && curTime <= endTime) {
//             item.status = '101';
//           } else if (curTime < startTime) {
//             item.status = '102';
//           } else if (curTime > endTime) {
//             item.status = '103';
//           }
//           // 拼装直播列表状态字符串
//           statusStr += item.id + ':' + item.status + ','
//         })
//         // 去掉末尾逗号
//         statusStr = statusStr.substring(0, statusStr.length - 1);
//         // 调用更新直播列表状态接口
//         if (statusStr != '') {
//           this.updateLiveStatus(statusStr, pageNum);
//         }

//         this.setData({
//           liveList: liveList,
//           page: res.data.page
//         })
//       } else {
//         this.messageComponent.applyActive(res.data.msg || '获取banner列表异常~');
//       }
//     }).catch((err) => {
//       console.log(err)
//     })
//   },

//   // 批量更新直播列表状态
//   updateLiveStatus(statusStr, pageNum) {
//     let params = {
//       strSatus: statusStr,
//       pageNum: pageNum,
//       status: this.data.activeIndex
//     };
//     liveMedia.fetchUpdateLiveStatus(params).then(res => {
//       if (res.data.flag) {
//         console.log('更新成功')
//       } else {
//         console.log(res.data.msg)
//       }
//     }).catch((err) => {
//       console.log(err)
//     })
//   },

//   // 判断是否为json, 处理
//   switchJsonObj(goodObj) {
//     try {
//       if (typeof JSON.parse(goodObj) == 'object') {
//         return JSON.parse(goodObj);
//       }
//     } catch(e) {
//       return goodObj;
//     }
//   },

//   // 分页
//   loadMore(e) {
//     this.getLiveList(e.detail)
//   },

//   onLoad: function() {
//     this.refreshScroll = this.selectComponent('#refreshScroll')
//     for (let i = 0; i < 10; i++) {
//       this.data.list.push(i)
//     }
//     this.setData({
//       list: this.data.list
//     })
//     this.getLiveList(1)
//   },

//   onPageScroll(e) {
//     this.refreshScroll.onPageScroll(e)
//   },
//   onReachBottom() {
//     this.refreshScroll.onReachBottom()
//   },
//   refresh: function() {
//     this.setData({
//       activeIndex: ''
//     }, () => {
//       this.getLiveList(1)
//     })
//     setTimeout(() => {
//       this.refreshScroll.stopRefresh()
//     }, 1000)
//   }
// })
