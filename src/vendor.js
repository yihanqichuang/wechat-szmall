

// 停车
export const PARKING_PROJECT = '${parkingProject}';
// 停车缴费支付方式
export const PARKINGPAY = '${parkingPay}';
//  注册信息 是否只需要手机号
export const REGISTERMESSAGE = '${registerMessage}';
// 是否显示订单中心
export const SHOWORDERCENTER = '${showOrderCenter}';
// 是否显示积分明细
export const SHOWPOINTDETAILS = '${showPointDetails}';


