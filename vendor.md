### appId 文件配置说明

- appId 打包发布appId,必填
- projectName 项目名称，必填
- hostName 项目接口访问地址，必填
- provinceDefault 默认车牌的省份，非必填，不填则为'浙'
- memberCardPath 会员卡页面， 非必填，不填则为默认会员页面
  - 宁波杉杉  /pages/memberCardBinBin/memberCardBinBin
  - 木渎影视城  /pages/memberCardMuDu/memberCard
- parkingProject 停车缴费页面接口请求地址，非必填，不填则为默认请求地址
  - 义乌绣湖里 xhl 
- parkingPay 支付方式，非必填，不填则默认为PARKINGPAY
  - 义乌绣湖里 PARKINGXHL
- registerMessage 会员注册是否需要填写姓名，性别，生日；非必填，不填则默认都需要。
  - 宁波杉杉 只需要手机号，registerMessage给一个为true的布尔值。


# 文档说明

### 安装项目`NPM`依赖

```shell
npm install
```

### 开发阶段

执行如下命令

```shell
# 启动监视
npm run watch
```
通过`微信Web开放者工具`打开项目根目录下`dist`文件夹，预览~

- 打开`微信Web开放者工具`，选择`添加项目`，填写或选择相应信息
  + AppID：点击右下角`无AppID`（个人用户可以申请）
  + 项目名称：随便填写，因为不涉及到部署，所以无所谓
  + 项目目录：选择项目根目录下`dist`文件夹
  + 点击`添加项目`
- 可以通过任意开发工具完成`src`下的编码，`gulp`会监视项目根目录下`src`文件夹，当文件变化自动编译
- 注意在微信公众平台后台添加域名白名单设置或者关闭开发阶段对请求域名安全的校验

#### 创建新页面

执行如下命令

```shell
# 启动生成器
$ npm run add
? Input the page name (index) [page-name]
? Do you need a configuration file (y/N) N
? Select a style framework (Use arrow keys)
> sass
# 自动生成...
```

### 生产阶段

执行如下命令

```shell
# 启动编译
$ npm run build
```

生产阶段的代码会经过压缩处理，最终输出到`dist`下。


### 发布阶段

执行如下命令

```shell
# 启动编译
$ npm run release
```

``` bash
├── appId                      // 配置相关
├── config                     // 动态基础配置
│   ├── hostname               // 配置开发和线上hostname
│   ├── dev                    // 开发环境小程序配置项
│   ├── prod                   // 线上环境小程序配置项
├── dist                       // 目标文件
├── src                        // 源代码
│   ├── api                    // 所有请求资源API
│   ├── images                 // images 等静态资源
│   ├── components             // 全局公用组件
│   ├── pages                  // 页面文件
│   ├── utils                  // 公用的方法
│   ├── style                  // 公用的样式
│   ├── wxParse                // 富文本解析
│   ├── app.json               // 主入口配置
```

